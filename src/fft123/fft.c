#include <stdlib.h>
#include <memory.h>
#include <config.h>

/* This is dummy file for VAC++. */
void dummy( void )
{}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    _CRT_term();
  }
  return 1UL;
}
#endif
