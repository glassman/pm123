/*
* SACD Decoder plugin
* Copyright (c) 2011-2020 Maxim V.Anisiutkin <maxim.anisiutkin@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with FFmpeg; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#define FIRCOEFS

#include <stdio.h>
#include <debuglog.h>
#include "DSDPCMConverterEngine.h"

#define LOG_ERROR   ("Error: ")
#define LOG_WARNING ("Warning: ")
#define LOG_INFO    ("Info: ")
#define LOG(p1, p2) DEBUGLOG(("%s%s", p1, p2))

DSDPCMConverterEngine::DSDPCMConverterEngine() {
  channels = 0;
  dsd_samples = 0;
  dsd_samplerate = 0;
  pcm_samplerate = 0;
  dB_gain = 0.0f;
  conv_delay = 0.0f;
  conv_type = DSDPCM_CONV_MULTISTAGE;
  conv_need_reinit = false;
}

DSDPCMConverterEngine::~DSDPCMConverterEngine() {
  clean();
  conv_delay = 0.0f;
}

float DSDPCMConverterEngine::get_delay() {
  return conv_delay;
}

void DSDPCMConverterEngine::set_gain(float dB_gain) {
  if (this->dB_gain != dB_gain) {
    conv_need_reinit = true;
    this->dB_gain = dB_gain;
  }
}

int DSDPCMConverterEngine::init(int channels, int dsd_samples, int dsd_samplerate, int pcm_samplerate, int conv_type, bool conv_fp64, double* fir_coefs, int fir_length) {
  if (!conv_need_reinit &&
       this->channels == channels &&
       this->dsd_samples == dsd_samples &&
       this->dsd_samplerate == dsd_samplerate &&
       this->pcm_samplerate == pcm_samplerate &&
       this->conv_type == conv_type &&
       this->conv_fp64 == conv_fp64)
  {
    return 1;
  }
  if (conv_type == DSDPCM_CONV_USER) {
    if (!(fir_coefs && fir_length > 0)) {
      return -2;
    }
  }
  this->channels = channels;
  this->dsd_samples = dsd_samples;
  this->dsd_samplerate = dsd_samplerate;
  this->pcm_samplerate = pcm_samplerate;
  this->conv_type = conv_type;
  this->conv_fp64 = conv_fp64;
  if (conv_fp64) {
    fltSetup_fp64.set_gain(dB_gain);
    fltSetup_fp64.set_fir1_64_coefs(fir_coefs, fir_length);
    convSlots_fp64 = new DSDPCMConverterSlot<double>[channels];
    for (int i = 0; i < channels; i++ ) {
      convSlots_fp64[i].init(fltSetup_fp64, dsd_samples,
                             ceil((double)pcm_samplerate / dsd_samplerate * dsd_samples * 8),
                             conv_type, dsd_samplerate / pcm_samplerate);
    }
    conv_delay = convSlots_fp64[0].converter->get_delay();
  }
  else {
    fltSetup_fp32.set_gain(dB_gain);
    fltSetup_fp32.set_fir1_64_coefs(fir_coefs, fir_length);
    convSlots_fp32 = new DSDPCMConverterSlot<float>[channels];
    for (int i = 0; i < channels; i++ ) {
      convSlots_fp32[i].init(fltSetup_fp32, dsd_samples,
                             ceil((double)pcm_samplerate / dsd_samplerate * dsd_samples * 8),
                             conv_type, dsd_samplerate / pcm_samplerate);
    }
    conv_delay = convSlots_fp32[0].converter->get_delay();
  }
  conv_need_reinit = false;
  return 0;
}

int DSDPCMConverterEngine::reset() {
  if (this->conv_fp64) {
    for (int i = 0; i < channels; i++ ) {
      convSlots_fp64[i].converter->init(fltSetup_fp64, dsd_samples);
    }
  }
  else {
    for (int i = 0; i < channels; i++ ) {
      convSlots_fp32[i].converter->init(fltSetup_fp32, dsd_samples);
    }
  }
  return 0;
}

int DSDPCMConverterEngine::clean() {
  if (conv_fp64) {
    for (int i = 0; i < channels; i++ ) {
      DSDPCMConverterSlot<double>& slot = convSlots_fp64[i];
      slot.run_slot = false;
      slot.dsd_semaphore.notify(); // Release worker (decoding) thread for exit
      slot.run_thread.join();      // Wait until worker (decoding) thread exit
      delete slot.converter;
      DSDPCMUtil::mem_free(slot.dsd_data);
      DSDPCMUtil::mem_free(slot.pcm_data);
    }
    delete[] convSlots_fp64;
  }
  else {
    for (int i = 0; i < channels; i++ ) {
      DSDPCMConverterSlot<float>& slot = convSlots_fp32[i];
      slot.run_slot = false;
      slot.dsd_semaphore.notify(); // Release worker (decoding) thread for exit
      slot.run_thread.join();      // Wait until worker (decoding) thread exit
      delete slot.converter;
      DSDPCMUtil::mem_free(slot.dsd_data);
      DSDPCMUtil::mem_free(slot.pcm_data);
    }
    delete[] convSlots_fp32;
  }
  return 0;
}

int DSDPCMConverterEngine::convert(uint8_t* dsd_data, int dsd_samples, float* pcm_data) {
  int pcm_samples;
  if (conv_fp64) {
    pcm_samples = ::convert(convSlots_fp64, channels, dsd_data, dsd_samples, pcm_data);
  } else {
    pcm_samples = ::convert(convSlots_fp32, channels, dsd_data, dsd_samples, pcm_data);
  }
  return pcm_samples;
}

template<class real_t>
int convert(DSDPCMConverterSlot<real_t>* convSlots, int channels, uint8_t* dsd_data, int dsd_samples, float* pcm_data) {
  int pcm_samples = 0;
  int i;

  for (i = 0; i < channels; i++ ) {
    DSDPCMConverterSlot<real_t>& slot = convSlots[i];
    if (!dsd_data) {
      for (int sample = 0; sample < slot.dsd_samples / 2; sample++) {
        uint8_t temp = slot.dsd_data[slot.dsd_samples - 1 - sample];
        slot.dsd_data[slot.dsd_samples - 1 - sample] = slot.dsd_data[sample];
        slot.dsd_data[sample] = temp;
      }
    } else {
      slot.dsd_samples = dsd_samples / channels;
      for (int sample = 0; sample < slot.dsd_samples; sample++) {
        slot.dsd_data[sample] = dsd_data[sample * channels + i];
      }
    }
    slot.dsd_semaphore.notify(); // Release worker (decoding) thread on the loaded slot
  }
  for (i = 0; i < channels; i++ ) {
    DSDPCMConverterSlot<real_t>& slot = convSlots[i];
    slot.pcm_semaphore.wait();   // Wait until worker (decoding) thread is complete
    for (int sample = 0; sample < slot.pcm_samples; sample++) {
      pcm_data[sample * channels + i] = (float)slot.pcm_data[sample];
    }
    pcm_samples += slot.pcm_samples;
  }
  return pcm_samples;
}

template<class real_t>
bool DSDPCMConverterSlot<real_t>::init(DSDPCMFilterSetup<real_t>& fltSetup, int dsd_samples, int pcm_samples, int conv_type, int decimation)
{
  this->dsd_data = (uint8_t*)DSDPCMUtil::mem_alloc(dsd_samples * sizeof(uint8_t));
  this->dsd_samples = dsd_samples;
  this->pcm_data = (real_t*)DSDPCMUtil::mem_alloc(pcm_samples * sizeof(real_t));
  this->pcm_samples = 0;
  switch (conv_type) {
    case DSDPCM_CONV_MULTISTAGE:
    {
      DSDPCMConverterMultistage<real_t>* pConv = NULL;
      switch (decimation) {
        case 1024:
          pConv = new DSDPCMConverterMultistage_x1024<real_t>();
          break;
        case 512:
          pConv = new DSDPCMConverterMultistage_x512<real_t>();
          break;
        case 256:
          pConv = new DSDPCMConverterMultistage_x256<real_t>();
          break;
        case 128:
          pConv = new DSDPCMConverterMultistage_x128<real_t>();
          break;
        case 64:
          pConv = new DSDPCMConverterMultistage_x64<real_t>();
          break;
        case 32:
          pConv = new DSDPCMConverterMultistage_x32<real_t>();
          break;
        case 16:
          pConv = new DSDPCMConverterMultistage_x16<real_t>();
          break;
        case 8:
          pConv = new DSDPCMConverterMultistage_x8<real_t>();
          break;
      }
      pConv->init(fltSetup, dsd_samples);
      converter = pConv;
      break;
    }
    case DSDPCM_CONV_DIRECT:
    case DSDPCM_CONV_USER:
    {
      DSDPCMConverterDirect<real_t>* pConv = NULL;
      switch (decimation) {
        case 1024:
          pConv = new DSDPCMConverterDirect_x1024<real_t>();
          break;
        case 512:
          pConv = new DSDPCMConverterDirect_x512<real_t>();
          break;
        case 256:
          pConv = new DSDPCMConverterDirect_x256<real_t>();
          break;
        case 128:
          pConv = new DSDPCMConverterDirect_x128<real_t>();
          break;
        case 64:
          pConv = new DSDPCMConverterDirect_x64<real_t>();
          break;
        case 32:
          pConv = new DSDPCMConverterDirect_x32<real_t>();
          break;
        case 16:
          pConv = new DSDPCMConverterDirect_x16<real_t>();
          break;
        case 8:
          pConv = new DSDPCMConverterDirect_x8<real_t>();
          break;
      }
      pConv->init(fltSetup, dsd_samples);
      converter = pConv;
      break;
    }
    default:
      break;
  }
  run_slot = true;
  run_thread = thread((void(TFNENTRYP)(void*))DSDPCMConverterSlot<real_t>::converter_thread, this);
  if (!run_thread.joinable()) {
    LOG(LOG_ERROR, ("Could not start decoder thread"));
    return false;
  }
  return true;
}
