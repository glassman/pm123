/*
* SACD Decoder plugin
* Copyright (c) 2011-2020 Maxim V.Anisiutkin <maxim.anisiutkin@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with FFmpeg; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef DSDPCMCONSTANTS_H
#define DSDPCMCONSTANTS_H

#include <config.h>
#include <inttypes.h>

#define DSDPCM_CONV_MULTISTAGE  0
#define DSDPCM_CONV_DIRECT      1
#define DSDPCM_CONV_USER        2

#define DSD_SILENCE_BYTE  0x69

#define CTABLES(fir_length) ((fir_length + 7) / 8)

#define DSDFIR1_8_LENGTH  80
#define DSDFIR1_16_LENGTH 160
#define DSDFIR1_64_LENGTH 641
#define PCMFIR2_2_LENGTH  27
#define PCMFIR3_2_LENGTH  151
#define PCMFIR_OFFSET     0x7fffffff
#define PCMFIR_SCALE      31

extern const double DSDFIR1_8_COEFS[DSDFIR1_8_LENGTH];
extern const double DSDFIR1_16_COEFS[DSDFIR1_16_LENGTH];
extern const double DSDFIR1_64_COEFS[DSDFIR1_64_LENGTH];
extern const double PCMFIR2_2_COEFS[PCMFIR2_2_LENGTH];
extern const double PCMFIR3_2_COEFS[PCMFIR3_2_LENGTH];

#endif
