/*
* SACD Decoder plugin
* Copyright (c) 2011-2016 Maxim V.Anisiutkin <maxim.anisiutkin@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with FFmpeg; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef DSDPCMCONVERTERENGINE_H
#define DSDPCMCONVERTERENGINE_H

#include <math.h>
#include "semaphore.h"
#include "thread.h"
#include "DSDPCMConverterMultistage.h"
#include "DSDPCMConverterDirect.h"

template<class real_t>
class DSDPCMConverterSlot {
public:
  uint8_t*  dsd_data;
  int       dsd_samples;
  real_t*   pcm_data;
  int       pcm_samples;
  semaphore dsd_semaphore;
  semaphore pcm_semaphore;
  bool      run_slot;
  thread    run_thread;
  DSDPCMConverter<real_t>* converter;
  DSDPCMConverterSlot() {
    run_slot = false;
    dsd_data = NULL;
    dsd_samples = 0;
    pcm_data = NULL;
    pcm_samples = 0;
    converter = NULL;
  }

  static void TFNENTRY converter_thread(DSDPCMConverterSlot<real_t>* slot) {
    while (slot->run_slot) {
      slot->dsd_semaphore.wait();
      if (slot->run_slot) {
        slot->pcm_samples = slot->converter->convert(slot->dsd_data, slot->pcm_data, slot->dsd_samples);
      }
      else {
        slot->pcm_samples = 0;
      }
      slot->pcm_semaphore.notify();
    }
    _endthread();
  }

  DSDPCMConverterSlot(const DSDPCMConverterSlot<real_t>& slot) {}

public:

  bool init(DSDPCMFilterSetup<real_t>& fltSetup, int dsd_samples, int pcm_samples, int conv_type, int decimation);

};

class DSDPCMConverterEngine {
  int   channels;
  int   dsd_samples;
  int   dsd_samplerate;
  int   pcm_samplerate;
  float dB_gain;
  float conv_delay;
  int   conv_type;
  bool  conv_fp64;
  bool  conv_need_reinit;
  DSDPCMConverterSlot<float>*  convSlots_fp32;
  DSDPCMFilterSetup<float>     fltSetup_fp32;
  DSDPCMConverterSlot<double>* convSlots_fp64;
  DSDPCMFilterSetup<double>    fltSetup_fp64;
public:
  DSDPCMConverterEngine();
  ~DSDPCMConverterEngine();
  float get_delay();
  void set_gain(float dB_gain);
  int init(int channels, int dsd_samples, int dsd_samplerate, int pcm_samplerate, int conv_type, bool conv_fp64, double* fir_coefs, int fir_length);
  int reset();
  int clean();
  int convert(uint8_t* dsd_data, int dsd_samples, float* pcm_data);
};

template<class real_t>
int convert(DSDPCMConverterSlot<real_t>* convSlots, int channels, uint8_t* dsd_data, int dsd_samples, float* pcm_data);

#endif
