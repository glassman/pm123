/*
* SACD Decoder plugin
* Copyright (c) 2020 Dmitry Steklenev <dmitry@5nets.ru>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with FFmpeg; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef THREAD_H
#define THREAD_H

#define  INCL_BASE
#include <os2.h>
#include <process.h>
#include <stdlib.h>
#include <config.h>

class thread {
  TID tid;
public:
  thread() {
    tid = -1;
  }
  thread( void(TFNENTRYP f)(void*), void* arg) {
    tid = _beginthread(f, NULL, 131070, arg);
  }
  bool joinable() {
    return tid != (TID)-1;
  }
  void join() {
    DosWaitThread(&tid, DCWW_WAIT );
  }
};

#endif
