/*
* SACD Decoder plugin
* Copyright (c) 2011-2019 Maxim V.Anisiutkin <maxim.anisiutkin@gmail.com>
* Copyright (c) 2020 Dmitry Steklenev <dmitry@5nets.ru>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with FFmpeg; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#define  INCL_BASE
#include <os2.h>

class semaphore {
  HEV hev;
public:
  semaphore() {
    DosCreateEventSem(NULL, &hev, DCE_POSTONE, 0);
  }
 ~semaphore() {
    DosCloseEventSem(hev);
  }
  void notify() {
    DosPostEventSem(hev);
  }
  void wait() {
    DosWaitEventSem(hev, SEM_INDEFINITE_WAIT);
  }
};

#endif
