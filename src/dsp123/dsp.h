/*
 * Copyright 2020 Dmitry Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DSP_H
#define DSP_H

#include <format.h>

#ifdef __cplusplus
extern "C" {
#endif

#define  DSP_DITHER_NONE        0x00000000UL
#define  DSP_DITHER_RECTANGULAR 0x00000001UL
#define  DSP_DITHER_TRIANGULAR  0x00000002UL
#define  DSP_DITHER_SHAPED      0x00000003UL
#define  DSP_DITHER             0x00000003UL
#define  DSP_RESAMPLE_HQ        0x00000004UL
#define  DSP_DSDPCM_MULTISTAGE  0x00000010UL
#define  DSP_DSDPCM_DIRECT      0x00000020UL
#define  DSP_DSDPCM_FP64        0x00000040UL

typedef struct _DSP_CONV {

  int         options;        /* See DSP_*                                     */
  FORMAT_INFO fmt_in;         /* Format of the input stream.                   */
  FORMAT_INFO fmt_out;        /* Format of the output stream.                  */
  int         samples;        /* The number of samples of one data block.      */
  float       scale;          /* Scale factor.                                 */

  void*       dsdtopcm;       /* Handle of the DSD to PCM conversion routine.  */
  uint8_t*    buff_dsd;       /* Contains interleaved 8-bit DSD stream.        */
  float*      buff_pcm;       /* Contains result of the DSD to PCM convertion. */

  void**      resample;       /* Array of handles of the resampling routines.  */
  double      resample_ratio; /* Resample ratio.                               */
  float**     buff_separated; /* Contains PCM data separated by channels.      */
  int         buff_used;      /* This field is used if several unprocessed     */
                              /* samples remain after the previous             */
                              /* resampling.                                   */
  int         max_buff;       /* The number of samples that can be buffered.   */
  float**     buff_resampled; /* Resampled PCM data separated by channels.     */
  int         max_out;        /* The number of samples that can be produced.   */
  void**      dither;         /* Array of handles of the dithering routines.   */

} DSP_CONV;

DSP_CONV* DLLENTRY dsp_conv_init( const FORMAT_INFO* fmt_in, const FORMAT_INFO* fmt_out,
                                  int samples, float scale, int options );
void DLLENTRY dsp_conv_cleanup( DSP_CONV* cnv );
int  DLLENTRY dsp_convert( DSP_CONV* cnv, void* in, void* out, int samples );
void DLLENTRY dsp_conv_reset( DSP_CONV* cnv );

#ifdef __cplusplus
}
#endif
#endif /* DSP_H */
