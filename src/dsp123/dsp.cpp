/*
 * Copyright 2020 Dmitry Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <stdlib.h>
#include <debuglog.h>
#include <utilfct.h>
#include "dsp.h"
#include <DSDPCMConverterEngine.h>
#include <libresample.h>
#include <gdither.h>

/* Initializes conversion routines. */
DSP_CONV* DLLENTRY
dsp_conv_init( const FORMAT_INFO* fmt_in, const FORMAT_INFO* fmt_out, int samples, float scale, int options )
{
  DSP_CONV* cnv;
  GDitherType dither_type;
  ULONG i;

  DEBUGLOG(( "dsp: input  format: samplerate: %d, channels: %d, bits: %d, id: 0x%04X\n",
      fmt_in->samplerate, fmt_in->channels, fmt_in->bits, fmt_in->format ));

  DEBUGLOG(( "dsp: output format: samplerate: %d, channels: %d, bits: %d, id: 0x%04X\n",
      fmt_out->samplerate, fmt_out->channels, fmt_out->bits, fmt_out->format ));

  // Current version of the library can produce 8, 16, 24 or 32 bit
  // PCM stream and can't change a number of channels.
  if( fmt_out->format   != WAVE_FORMAT_PCM  ||
      fmt_out->channels != fmt_in->channels ||
      fmt_out->bits > 32                    ||
      fmt_out->bits % 8                     )
  {
    return NULL;
  }

  // Can receive only 32 bit DSD stream.
  if( fmt_in->format == WAVE_FORMAT_DSD && fmt_in->bits != 32 ) {
    return NULL;
  }

  // Or 8, 16, 24 and 32 bit PCM stream.
  if( fmt_in->format == WAVE_FORMAT_PCM && ( fmt_in->bits > 32 || fmt_in->bits % 8 )) {
    return NULL;
  }

  if(( cnv = (DSP_CONV*)calloc( sizeof( DSP_CONV ), 1 )) == NULL ) {
    DEBUGLOG(( "dsp: not enough memory.\n" ));
    return NULL;
  }

  cnv->options = options;
  cnv->fmt_in  = *fmt_in;
  cnv->fmt_out = *fmt_out;
  cnv->samples = samples;
  cnv->scale   = scale;

  if( fmt_in->format == WAVE_FORMAT_DSD  )
  {
    int dsdtopcm_samplerate;
    int dsd_samples;
    int buff_dsd_size;
    int buff_pcm_size;

    // Select the samplerate that is equal or above target
    // and supported by DSD conversion engine.
    if( fmt_out->samplerate > 176400 ) {
      dsdtopcm_samplerate = 352800;
    } else if( fmt_out->samplerate > 88200 ) {
      dsdtopcm_samplerate = 176400;
    } else if( fmt_out->samplerate > 44100 ) {
      dsdtopcm_samplerate = 88200;
    } else {
      dsdtopcm_samplerate = 44100;
    }

    dsd_samples = samples * fmt_in->bits / 8 * fmt_in->channels;

    DEBUGLOG(( "dsp: init DSDPCMConverterEngine to %d channels, dsd_samples=%d, samplerate %d -> %d\n",
                fmt_in->channels, dsd_samples, fmt_in->samplerate * fmt_in->bits,
                dsdtopcm_samplerate ));

    // Initialize DSD conversion engine.
    cnv->dsdtopcm = new DSDPCMConverterEngine();

    if( !cnv->dsdtopcm ) {
      DEBUGLOG(( "dsp: not enough memory.\n" ));
      dsp_conv_cleanup( cnv );
      return NULL;
    }

    ((DSDPCMConverterEngine*)cnv->dsdtopcm)->init(
        fmt_in->channels, dsd_samples, fmt_in->samplerate * fmt_in->bits, dsdtopcm_samplerate,
        options & DSP_DSDPCM_DIRECT ? DSDPCM_CONV_DIRECT : DSDPCM_CONV_MULTISTAGE,
        options & DSP_DSDPCM_FP64, NULL, 0 );

    buff_dsd_size = samples * fmt_in->channels * fmt_in->bits / 8;
    buff_pcm_size = ceil((double)dsdtopcm_samplerate / fmt_in->samplerate / fmt_in->bits * dsd_samples * 8);
    cnv->buff_dsd = (uint8_t*)calloc( buff_dsd_size, sizeof( uint8_t ));
    cnv->buff_pcm = (float*)calloc( buff_pcm_size, sizeof( float ));

    DEBUGLOG(( "dsp: DSD buffer size is %d 8-bit samples (total).\n", buff_dsd_size ));
    DEBUGLOG(( "dsp: PCM buffer size is %d floating-point samples (total).\n", buff_pcm_size ));

    if( !cnv->buff_dsd || !cnv->buff_pcm ) {
      DEBUGLOG(( "dsp: not enough memory.\n" ));
      dsp_conv_cleanup( cnv );
      return NULL;
    }

    // From libresample README.txt:
    // - Given a resampling factor f > 1, and a number of input
    //   samples n, the number of output samples should be between
    //   floor(n - f) and ceil(n + f). In other words, if you
    //   resample 1000 samples at a factor of 8, the number of
    //   output samples might be between 7992 and 8008. Do not
    //   assume that it will be exactly 8000. If you need exactly
    //   8000 outputs, pad the input with extra zeros as necessary.

    cnv->resample_ratio = (double)fmt_out->samplerate / dsdtopcm_samplerate;
    cnv->max_buff = buff_pcm_size / fmt_in->channels;
    cnv->max_out  = ceil(( cnv->max_buff + 1 ) * cnv->resample_ratio );
  } else if( fmt_in->samplerate != fmt_out->samplerate ) {
    cnv->resample_ratio = (double)fmt_out->samplerate / fmt_in->samplerate;
    cnv->max_buff = samples;
    cnv->max_out  = ceil(( samples + 1 ) * cnv->resample_ratio );
  } else {
    cnv->resample_ratio = 1.0;
    cnv->max_out  = samples;
    cnv->max_buff = samples;
  }

  // It is not known how much samples may remain unused from the
  // previous sampling rate conversion cycle. I think a double
  // buffer is enough to receive a new chunk of data.
  cnv->max_buff *= 2;

  DEBUGLOG(( "dsp: maximum separated and output samples: %d/%d\n", cnv->max_buff, cnv->max_out ));

  // Prepare buffers for resampled PCM data and resampling routines.
  if( cnv->resample_ratio != 1.0 )
  {
    DEBUGLOG(( "dsp: prepare of resampling with a ratio of %0.5f\n", cnv->resample_ratio ));

    cnv->buff_resampled = (float**)calloc( fmt_in->channels, sizeof( *cnv->buff_resampled ));
    cnv->resample = (void**)calloc( fmt_in->channels, sizeof( *cnv->resample ));

    if( !cnv->buff_resampled || !cnv->resample ) {
      DEBUGLOG(( "dsp: not enough memory.\n" ));
      dsp_conv_cleanup( cnv );
      return NULL;
    }

    for( i = 0; i < fmt_in->channels; i++ )
    {
      cnv->buff_resampled[i] = (float*)calloc( cnv->max_out, sizeof( float ));
      cnv->resample[i] = resample_open( cnv->options & DSP_RESAMPLE_HQ, cnv->resample_ratio, cnv->resample_ratio );

      if( !cnv->resample[i] || !cnv->buff_resampled[i] ) {
        DEBUGLOG(( "dsp: not enough memory.\n" ));
        dsp_conv_cleanup( cnv );
        return NULL;
      }
    }
  }

  // Prepare buffers for scaling and dithering.
  if(( fmt_in->format == WAVE_FORMAT_PCM && fmt_in->bits == fmt_out->bits ) ||
     ( fmt_in->format == WAVE_FORMAT_DSD && fmt_out->bits >= 24 ))
  {
    DEBUGLOG(( "dsp: disable dithering as unnecessary.\n" ));
    dither_type = GDitherNone;
  } else {
    switch( options & DSP_DITHER ) {
      case DSP_DITHER_RECTANGULAR:
        DEBUGLOG(( "dsp: use rectangular dithering.\n" ));
        dither_type = GDitherRect;
        break;
      case DSP_DITHER_TRIANGULAR:
        DEBUGLOG(( "dsp: use triangular dithering.\n" ));
        dither_type = GDitherTri;
        break;
      case DSP_DITHER_SHAPED:
        DEBUGLOG(( "dsp: apply shaped dithering.\n" ));
        dither_type = GDitherShaped;
        break;
      default:
        DEBUGLOG(( "dsp: disable dithering.\n" ));
        dither_type = GDitherNone;
        break;
    }
  }

  // Prepare buffers for separated PCM data and dither routines.
  cnv->buff_separated = (float**)calloc( fmt_in->channels, sizeof( *cnv->buff_separated ));
  cnv->dither = (void**)calloc( fmt_in->channels, sizeof( *cnv->dither ));

  if( !cnv->buff_separated || !cnv->dither ) {
    DEBUGLOG(( "dsp: not enough memory.\n" ));
    dsp_conv_cleanup( cnv );
    return NULL;
  }

  for( i = 0; i < fmt_in->channels; i++ )
  {
    cnv->buff_separated[i] = (float*)calloc( cnv->max_buff, sizeof( float ));
    cnv->dither[i] = gdither_new( dither_type, fmt_out->channels, GDitherSize( fmt_out->bits ), fmt_out->bits );

    if( !cnv->dither[i] || !cnv->buff_separated[i] ) {
      DEBUGLOG(( "dsp: not enough memory.\n" ));
      dsp_conv_cleanup( cnv );
      return NULL;
    }
  }

  return cnv;
}

/* Cleanups conversion routines. */
void DLLENTRY
dsp_conv_cleanup( DSP_CONV* cnv )
{
  if( cnv )
  {
    ULONG i;

    DEBUGLOG(( "dsp: cleanup conversion engine.\n" ));

    for( i = 0; i < cnv->fmt_in.channels; i++ )
    {
      if( cnv->buff_separated ) {
        free( cnv->buff_separated[i] );
      }
      if( cnv->buff_resampled ) {
        free( cnv->buff_resampled[i] );
      }
      if( cnv->dither && cnv->dither[i] ) {
        gdither_free( cnv->dither[i] );
      }
      if( cnv->resample && cnv->resample[i] ) {
        resample_close( cnv->resample[i] );
      }
    }

    free( cnv->buff_dsd );
    free( cnv->buff_pcm );
    free( cnv->buff_separated );
    free( cnv->buff_resampled );
    free( cnv->dither );
    free( cnv->resample );

    delete (DSDPCMConverterEngine*)cnv->dsdtopcm;

    free( cnv );
  }
}

/* Separates an interleaved unsigned 8-bit PCM stream into
   floating point sequential streams, one for each channel. */
static void
pcm_separate_08bit( uint8_t* in, float** out, int samples, int channels, int used )
{
  int i, ch;

  for( i = used, samples += used; i < samples; i++ ) {
    for( ch = 0; ch < channels; ch++, in++ ) {
      out[ch][i] = GETSMPL8U(in) / SMPL8U_MAX;
    }
  }
}

/* Separates an interleaved signed 16-bit PCM stream into
   floating point sequential streams, one for each channel. */
static void
pcm_separate_16bit( int16_t* in, float** out, int samples, int channels, int used )
{
  int i, ch;

  for( i = used, samples += used; i < samples; i++ ) {
    for( ch = 0; ch < channels; ch++, in++ ) {
      out[ch][i] = GETSMPL16(in) / SMPL16_MAX;
    }
  }
}

/* Separates an interleaved signed 24-bit PCM stream into
   floating point sequential streams, one for each channel. */
static void
pcm_separate_24bit( uint8_t* in, float** out, int samples, int channels, int used )
{
  int i, ch;

  for( i = used, samples += used; i < samples; i++ ) {
    for( ch = 0; ch < channels; ch++, in += 3 ) {
      out[ch][i] = GETSMPL24(in) / SMPL24_MAX;
    }
  }
}

/* Separates an interleaved signed 24-bit PCM stream into
   floating point sequential streams, one for each channel. */
static void
pcm_separate_32bit( int32_t* in, float** out, int samples, int channels, int used )
{
  int i, ch;

  for( i = used, samples += used; i < samples; i++ ) {
    for( ch = 0; ch < channels; ch++, in++ ) {
      out[ch][i] = GETSMPL32(in) / SMPL32_MAX;
    }
  }
}

/* Separates an interleaved floating point PCM stream into
   sequential streams, one for each channel. */
static void
pcm_separate_float( float* in, float** out, int samples, int channels, int used )
{
  int i, ch;

  for( i = used, samples += used; i < samples; i++ ) {
    for( ch = 0; ch < channels; ch++ ) {
      out[ch][i] = *in++;
    }
  }
}

/* Resampling. */
static int
dsp_resample( DSP_CONV* cnv, float** in, float** out, int samples, int eos )
{
  int   out_samples[2] = {0}, used[2] = {0};
  ULONG i;

  for( i = 0; i < cnv->fmt_in.channels; i++ ) {
    out_samples[0] = resample_process( cnv->resample[i], cnv->resample_ratio, in[i],
                                       samples + cnv->buff_used, eos, &used[0],
                                       out[i], cnv->max_out );
    if( i == 0 ) {
      out_samples[1] = out_samples[0];
      used[1] = used[0];
    }
    else if( out_samples[1] != out_samples[0] ||
             used[1] != used[0] )
    {
      DEBUGLOG(( "dsp: channels out of sync.\n" ));
      return -1;
    }

    if( used[0] != samples ) {
      DEBUGLOG(( "dsp: used %d samples of %d\n", used[0], samples ));
      memcpy( &in[i][0], &in[i][used[0]], ( samples - used[0] ) * sizeof( float ));
    }
  }

  cnv->buff_used = samples - used[0];
  return out_samples[0];
}

/* Scales samples according to the current scale factor. */
static void
dsp_scale_float( DSP_CONV* cnv, float* in, int samples )
{
  float scale = cnv->scale, z;
  int   i;

  if( scale != 1.0f ) {
    for( i = 0; i < samples; i++ )
    {
      z = in[i] * scale;
      z = limit2( z, -1.0f, 1.0f );
      in[i] = z;
    }
  }
}

/* Convert multi-byte DSD stream to an 8-bit stream required
   by DSDPCMConverterEngine. */
static void
dsd_convert_dsd_to_8bit( uint8_t* in, uint8_t* out, int samples, int channels, int bits )
{
  uint8_t* pin = (uint8_t*)in;
  uint8_t* p8b = (uint8_t*)out;

  int i, k, ch;
  int bps = bits / 8;

  for( i = 0; i < samples; i++ ) {
    for( ch = 0; ch < channels; ch++ ) {
      for( k = 0; k < bps; k++ ) {
        p8b[k*channels+ch] = *pin++;
      }
    }
    p8b += bps * channels;
  }
}

/* Converts samples. */
int DLLENTRY
dsp_convert( DSP_CONV* cnv, void* in, void* out, int samples )
{
  ULONG   i;
  float** buff_separated;
  int     eos = samples < cnv->samples;

  DEBUGLOG2(( "dsp: convert %d samples.\n", samples ));

  if( cnv->dsdtopcm ) {
    // Convert the DSD stream to the separated PCM streams.
    dsd_convert_dsd_to_8bit((uint8_t*)in, cnv->buff_dsd, samples,
                             cnv->fmt_in.channels, cnv->fmt_in.bits );

    samples = ((DSDPCMConverterEngine*)cnv->dsdtopcm)->convert(
                  cnv->buff_dsd, samples * cnv->fmt_in.channels * cnv->fmt_in.bits / 8,
                  cnv->buff_pcm ) / cnv->fmt_in.channels;

    DEBUGLOG2(( "dsp: DSDPCMConverterEngine produce %d samples.\n", samples ));

    if( samples > cnv->max_buff - cnv->buff_used ) {
      DEBUGLOG(( "dsp: buffer overflow.\n" ));
      samples = cnv->max_buff - cnv->buff_used;
    }

    pcm_separate_float( cnv->buff_pcm, cnv->buff_separated,
                        samples, cnv->fmt_in.channels, cnv->buff_used );
  } else {
    // Separate PCM stream.
    if( samples > cnv->max_buff - cnv->buff_used ) {
      DEBUGLOG(( "dsp: buffer overflow.\n" ));
      samples = cnv->max_buff - cnv->buff_used;
    }

    switch( cnv->fmt_in.bits ) {
      case 8:
        pcm_separate_08bit((uint8_t*)in, cnv->buff_separated, samples,
                                         cnv->fmt_in.channels, cnv->buff_used );
        break;
      case 16:
        pcm_separate_16bit((int16_t*)in, cnv->buff_separated, samples,
                                         cnv->fmt_in.channels, cnv->buff_used );
        break;
      case 24:
        pcm_separate_24bit((uint8_t*)in, cnv->buff_separated, samples,
                                         cnv->fmt_in.channels, cnv->buff_used );
        break;
      case 32:
        pcm_separate_32bit((int32_t*)in, cnv->buff_separated, samples,
                                         cnv->fmt_in.channels, cnv->buff_used );
        break;
    }
  }

  if( cnv->resample ) {
    samples = dsp_resample( cnv, cnv->buff_separated, cnv->buff_resampled, samples + cnv->buff_used, eos );
    buff_separated = cnv->buff_resampled;
    DEBUGLOG2(( "dsp: resampling routines produces %d samples.\n", samples ));
  } else {
    buff_separated = cnv->buff_separated;
  }

  for( i = 0; i < cnv->fmt_in.channels; i++ ) {
    dsp_scale_float( cnv, buff_separated[i], samples );
    gdither_runf( cnv->dither[i], i, samples, buff_separated[i], out );
  }

  return samples;
}

/* Resets conversion routines. */
void DLLENTRY
dsp_conv_reset( DSP_CONV* cnv )
{
  ULONG i;

  if( cnv->dsdtopcm ) {
    ((DSDPCMConverterEngine*)cnv->dsdtopcm)->reset();
  }

  cnv->buff_used = 0;

  for( i = 0; i < cnv->fmt_in.channels; i++ ) {
    gdither_reset( cnv->dither[i] );
  }
}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    __ctordtorInit();
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    __ctordtorTerm();
    _CRT_term();
  }
  return 1UL;
}
#endif
