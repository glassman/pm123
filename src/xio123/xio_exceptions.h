/*
 * Copyright 2020 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef XIO_EXCEPTIONS_H
#define XIO_EXCEPTIONS_H

#include <setjmp.h>

#pragma pack(4)
typedef struct _XIO_EXCEPTION
{
  EXCEPTIONREGISTRATIONRECORD exreg;
  jmp_buf env;

} XIO_EXCEPTION;
#pragma pack()

#ifdef __cplusplus
extern "C" {
#endif

ULONG EXPENTRY ExceptionHandler( PEXCEPTIONREPORTRECORD,
                                 PEXCEPTIONREGISTRATIONRECORD,
                                 PCONTEXTRECORD,
                                 PVOID );
#ifdef __cplusplus
}
#endif

#define XIO_BEGINSEQUENCE                           \
  {                                                 \
    XIO_EXCEPTION ex;                               \
    if( setjmp( ex.env ) == 0 ) {                   \
      ex.exreg.ExceptionHandler = ExceptionHandler; \
      DosSetExceptionHandler( &ex.exreg );

#define XIO_RECOVER                                 \
      DosUnsetExceptionHandler( &ex.exreg );        \
    } else {

#define XIO_END                                     \
    }                                               \
  }

#endif
