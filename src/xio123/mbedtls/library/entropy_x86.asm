;
;  OS/2-specific entropy polling functions
;
;  Copyright Dmitry Steklenev
;  SPDX-License-Identifier: Apache-2.0
;
;  Licensed under the Apache License, Version 2.0 (the "License"); you may
;  not use this file except in compliance with the License.
;  You may obtain a copy of the License at
;
;  http://www.apache.org/licenses/LICENSE-2.0
;
;  Unless required by applicable law or agreed to in writing, software
;  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
;  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;  See the License for the specific language governing permissions and
;  limitations under the License.
;

bits 32

%ifdef OBJ_FORMAT_aout
  %idefine code_section section .text
  %idefine data_section section .data
  %idefine bss_section  section .bss
%else
  %idefine code_section segment .text class=CODE flat use32
  %idefine data_section segment .data class=DATA flat use32
  %idefine bss_section  segment .bss  class=DATA flat use32
%endif

%idefine MBEDTLS_ERR_ENTROPY_SOURCE_FAILED -003Ch

  code_section

; Acquire entropy using Intel-specific cpu instructions
; Uses the RDRAND instruction if available.

global _mbedtls_rdrand_bytes
_mbedtls_rdrand_bytes:

  push ebx
  push ecx
  push edx
  push edi

  ; [esp + 20]  unsigned char *output
  ; [esp + 24]  size_t len

  mov  eax, 1
  xor  ecx, ecx
  cpuid
  shr  ecx, 30
  test ecx, 1
  jnz  .rdrand_begin
  mov  eax, MBEDTLS_ERR_ENTROPY_SOURCE_FAILED
  jmp  .rdrand_ret

.rdrand_begin:

  mov   edi, [esp + 20];
  mov   ecx, [esp + 24];

.rdrand_loop:

  rdrand eax
  jnc  .rdrand_loop

  cmp  ecx, 4
  jb   .rdrand_tail
  mov  [edi], eax
  add  edi, 4
  sub  ecx, 4
  jmp  .rdrand_loop

.rdrand_tail:

  or   ecx, ecx
  jz   .rdrand_done
  mov  [edi], al
  inc  edi
  dec  ecx
  shr  eax, 8

.rdrand_done:

  xor  eax, eax

.rdrand_ret:

  pop  edi
  pop  edx
  pop  ecx
  pop  ebx
  ret

; Acquire entropy from high-speed clock
;
; Since we get some randomness from the low-order bits of the
; high-speed clock, it can help.

global _mbedtls_rdtsc_bytes
_mbedtls_rdtsc_bytes:

  push ebx
  push ecx
  push edx
  push edi

  ; [esp + 20]  unsigned char *output
  ; [esp + 24]  size_t len

  mov  eax, 1
  cpuid
  test edx, 10h
  jnz  .rdtsc_begin
  mov  eax, MBEDTLS_ERR_ENTROPY_SOURCE_FAILED
  jmp  .rdtsc_ret

.rdtsc_begin:

  mov   edi, [esp + 20];
  mov   ecx, [esp + 24];

.rdtsc_loop:

  or   ecx, ecx
  jz   .rdtsc_done
  rdtsc
  mov  [edi], al
  dec  ecx
  inc  edi
  jmp  .rdtsc_loop

.rdtsc_done:

  xor  eax, eax

.rdtsc_ret:

  pop  edi
  pop  edx
  pop  ecx
  pop  ebx
  ret
