/*
 * Copyright 2006-2022 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_ERRORS
#include <os2.h>
#include <config.h>
#include <string.h>
#include <errno.h>
#include <nerrno.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ctype.h>

#ifndef  TCPV40HDRS
#include <unistd.h>
#endif

#include <errorstr.h>
#include <decoder_plug.h>
#include <debuglog.h>
#include <minmax.h>

#include "xio.h"
#include "xio_protocol.h"
#include "xio_buffer.h"
#include "xio_file.h"
#include "xio_ftp.h"
#include "xio_http.h"
#include "xio_cddb.h"
#include "xio_socket.h"
#include "xio_url.h"
#include "xio_tls.h"

#ifdef  XIO_SERIALIZE_DISK_IO
extern  HMTX serialize;
#endif

#define XIO_SERIAL 0x41290837

static char   http_proxy_host[XIO_MAX_HOSTNAME];
static int    http_proxy_port;
static char   http_proxy_user[XIO_MAX_USERNAME];
static char   http_proxy_pass[XIO_MAX_PASSWORD];
static u_long http_proxy_addr;
static int    http_proxy_bypass_local;

static char
  http_proxy_bypass[XIO_MAX_HOSTLIST] = "localhost,127.0.0.1";

static size_t buffer_size    = 32768;
static int    buffer_wait    = 0;
static int    buffer_fill    = 30;
static size_t buffer_preload = 0;
static int    socket_timeout = 30;

/* Serializes access to the library's global data. */
static HMTX mutex;

/* Cleanups the file structure. */
static void
xio_terminate( XFILE* x )
{
  x->serial = 0;

  if( x->buffer ) {
    // At end the buffer itself will clear all others.
    buffer_terminate( x );
  } else {
    if( x->protocol ) {
      x->protocol->clean( x );
    }
    free( x );
  }
}

/* Returns 1 if specified hostname is a member
   of the domain for which no proxy is used. Returns
   0 otherwise or if an error occurs. */
int DLLENTRY
xio_bypass_proxy( char* host )
{
  char*  domains = malloc( XIO_MAX_HOSTLIST   );
  char*  rhost   = strdup( host );
  int    rc      = 0;
  char*  mask;
  size_t mlen;


  // First checks if the specified hostname is a member of the domain
  // for which no proxy is used.
  if( domains && host ) {
    xio_no_proxy( domains, XIO_MAX_HOSTLIST );
    strrev( rhost );

    for( mask = strtok( domains, "," ); mask; mask = strtok( NULL, "," )) {
      while( *mask && isspace( *mask )) {
        ++mask;
      }
      strrev( mask );
      while( *mask && isspace( *mask )) {
        ++mask;
      }

      mlen = strlen( mask );

      if( mlen ) {
        DEBUGLOG2(( "xio: compare %s with %s\n", rhost, mask ));

        if( strnicmp( mask, rhost, mlen ) == 0 ) {
          if( mask[mlen-1] == '.' || rhost[mlen] == '.' || rhost[mlen] == 0 ) {
            DEBUGLOG2(( "xio: is a member of.\n" ));
            rc = 1;
            break;
          }
        }
      }
    }
  }

  free( rhost   );
  free( domains );

  if( rc ) {
    return rc;
  }

  // Next checks if the specified hostname is a local address.
  if( http_proxy_bypass_local )
  {
    char buf[ 1024 ];
    u_long address = so_get_address( host );

    if( address != -1 )
    {
      int s = socket( AF_INET, SOCK_RAW, 0 );
      address = htonl( address );

      DEBUGLOG(( "xio: check %08X as local address\n", address ));

      if( s != -1 ) {
        if( os2_ioctl( s, SIOSTATAT, buf, sizeof( buf )) == 0 )
        {
          u_short i, count = *(u_short*)buf;
          struct statatreq* p = (struct statatreq*)( buf + 2 );

          for( i = 0; i < count; i++ )
          {
            DEBUGLOG2(( "xio: check %08X and %08X/%08X\n", address, htonl( p[i].addr ), p[i].mask ));

            if(( address & p[i].mask ) == ( htonl( p[i].addr ) & p[i].mask )) {
              DEBUGLOG2(( "xio: this is the local address.\n" ));
              rc = 1;
              break;
            }
          }
        }
        soclose( s );
      }
    }
  }

  return rc;
}

/* Open file. Returns a pointer to a file structure that can be used
   to access the open file. A NULL pointer return value indicates an
   error. If there is not enough memory to create a read-ahead buffer,
   the file is opened in direct access mode and errno variable is
   set to ENOMEM. */
XFILE* DLLENTRY
xio_fopen( const char* filename, const char* mode )
{
  XFILE* x;

  if( !filename || !*filename ) {
    errno = ENOENT;
    return NULL;
  }
  if( !( x = calloc( 1, sizeof( XFILE )))) {
    return NULL;
  }

  if( strnicmp( filename, "http:", 5 ) == 0 )
  {
    x->scheme   = XIO_HTTP;
    x->protocol = http_initialize( x );
  }
  else if( strnicmp( filename, "https:", 6 ) == 0 )
  {
    x->scheme   = XIO_HTTPS;
    x->protocol = http_initialize( x );
  }
  else if( strnicmp( filename, "ftp:", 4 ) == 0 )
  {
    XURL* url = url_allocate( filename );

    if( *http_proxy_host && !xio_bypass_proxy( url->host )) {
      x->scheme   = XIO_HTTP;
      x->protocol = http_initialize( x );
    } else {
      x->scheme   = XIO_FTP;
      x->protocol = ftp_initialize( x );
    }

    url_free( url );
  }
  else if( strnicmp( filename, "cddbp:", 6 ) == 0 )
  {
    x->scheme   = XIO_CDDB;
    x->protocol = cddb_initialize( x );
  } else {
    x->scheme   = XIO_FILE;
    x->protocol = file_initialize( x );
  }

  if( !x->protocol ) {
    xio_terminate( x );
    return NULL;
  }

  if( strchr( mode, 'r' )) {
    if( strchr( mode, '+' )) {
      x->oflags = XO_READ | XO_WRITE;
    } else {
      x->oflags = XO_READ;
    }
  } else if( strchr( mode, 'w' )) {
    if( strchr( mode, '+' )) {
      x->oflags = XO_WRITE | XO_CREATE | XO_TRUNCATE | XO_READ;
    } else {
      x->oflags = XO_WRITE | XO_CREATE | XO_TRUNCATE;
    }
  } else if( strchr( mode, 'a' )) {
    if( strchr( mode, '+' )) {
      x->oflags = XO_WRITE | XO_CREATE | XO_APPEND | XO_READ;
    } else {
      x->oflags = XO_WRITE | XO_CREATE | XO_APPEND;
    }
  } else {
    xio_terminate( x );
    errno = EINVAL;
    return NULL;
  }

  if((( x->oflags & XO_READ   ) &&
      ( x->oflags & XO_WRITE  ) && !( x->protocol->supports & XS_CAN_READWRITE )) ||
     (( x->oflags & XO_READ   ) && !( x->protocol->supports & XS_CAN_READ      )) ||
     (( x->oflags & XO_WRITE  ) && !( x->protocol->supports & XS_CAN_WRITE     )) ||
     (( x->oflags & XO_CREATE ) && !( x->protocol->supports & XS_CAN_CREATE    ))  )
  {
    xio_terminate( x );
    errno = EINVAL;
    return NULL;
  }

  if( x->protocol->open( x, filename, x->oflags ) != 0 ) {
    xio_terminate( x );
    return NULL;
  }

  errno = 0;
  buffer_initialize( x );
  x->protocol->ungetc = EOF;
  x->serial = XIO_SERIAL;
  return x;
}

/* Waits the read-ahead buffer filling according to current
   library setting. */
void DLLENTRY
xio_fwait( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fwait] file handle %08X is not valid.\n", x ));
    errno = EBADF;
  } else {
    buffer_wait_for_filling( x );
  }
}

/* Reads specified chunk of the data and notifies an attached
   observer about streaming metadata. Returns the number of
   bytes placed in result. The return value 0 indicates an attempt
   to read at end-of-file. A return value -1 indicates an error. */
static int
xio_read_and_notify( XFILE* x, char* result, unsigned int count )
{
  int read_size;
  int read_done = 0;
  int done;
  int i;

  unsigned char metahead;
  int           metasize;
  char*         metabuff;
  char*         titlepos;

  // Reads byte that is pushed onto stream before via xio_ungetc.

  if( count && x->protocol->ungetc != EOF ) {
    DosRequestMutexSem( x->protocol->mtx_access, SEM_INDEFINITE_WAIT );
    if( x->protocol->ungetc != EOF ) {
      *result = (char)x->protocol->ungetc;
      x->protocol->ungetc = EOF;
      read_done++;
    }
    DosReleaseMutexSem( x->protocol->mtx_access );
  }

  // Note: x->protocol->s_metaint and x->protocol->s_metapos
  // do not changed by a read-ahead thread.

  if( !x->protocol->s_metaint ) {
    return buffer_read( x, result + read_done, count - read_done ) + read_done;
  }

  while( read_done < count ) {
    if( x->protocol->s_metapos == 0 )
    {
      // Time to read metadata from a input stream.
      metahead = 0;
      done = buffer_read( x, (char*)&metahead, 1 );

      if( done ) {
        if( metahead ) {
          metasize = metahead * 16;

          if(( metabuff = malloc( metasize + 1 )) == NULL ) {
            return -1;
          }
          if(( done = buffer_read( x, metabuff, metasize )) != metasize ) {
            return -1;
          }

          metabuff[done] = 0;
          DosRequestMutexSem( x->protocol->mtx_access, SEM_INDEFINITE_WAIT );

          if(( titlepos = strstr( metabuff, "StreamTitle='" )) != NULL )
          {
            titlepos += 13;
            for( i = 0; i < sizeof( x->protocol->s_title ) - 1 && *titlepos
                        && ( titlepos[0] != '\'' || titlepos[1] != ';' ); i++ )
            {
              x->protocol->s_title[i] = *titlepos++;
            }

            x->protocol->s_title[i] = 0;
          }

          if( x->protocol->s_observer &&
              x->protocol->s_metabuff )
          {
            strlcpy( x->protocol->s_metabuff, metabuff, x->protocol->s_metasize );
            WinPostMsg( x->protocol->s_observer, WM_METADATA,
                        MPFROMP( x->protocol->s_metabuff ), 0 );
          }

          if( x->protocol->supports & XS_USE_SPOS ) {
            x->protocol->s_pos -= ( metasize + 1 );
          }

          DosReleaseMutexSem( x->protocol->mtx_access );
        }
        x->protocol->s_metapos = x->protocol->s_metaint;
      }
    }

    // Determines the maximum size of the data chunk for reading.
    read_size = count - read_done;
    read_size = min( read_size, x->protocol->s_metapos );

    done = buffer_read( x, result + read_done, read_size );

    if( !done || done == -1 ) {
      break;
    }

    read_done += done;
    x->protocol->s_metapos -= done;
  }

  return read_done;
}

/* Reads up to count items of size length from the input file and
   stores them in the given buffer. The position in the file increases
   by the number of bytes read. Returns the number of full items
   successfully read, which can be less than count if an error occurs
   or if the end-of-file is met before reaching count. */
size_t DLLENTRY
xio_fread( void* buffer, size_t size, size_t count, XFILE* x )
{
  int done;
  int read = count * size;
  int rc   = 0;

  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fread] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return 0;
  }

  DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );

  if(!( x->oflags & XO_READ )) {
    errno = EINVAL;
    x->protocol->error = 1;
  } else {
    done = xio_read_and_notify( x, buffer, read );

    if( done == read ) {
      rc = count;
    } else if( done >= 0 ) {
      int remain = done % size;
      if( remain ) {
        buffer_seek( x, -remain, XIO_SEEK_CUR );
      }
      rc = done / size;
      x->protocol->eof   = 1;
    } else if( x->protocol->abort ) {
      done = 0;
      x->protocol->eof   = 1;
    } else {
      x->protocol->error = 1;
    }
  }

  DosReleaseMutexSem( x->protocol->mtx_file );
  return rc;
}

/* Writes up to count items, each of size bytes in length, from buffer
   to the output file. Returns the number of full items successfully
   written, which can be fewer than count if an error occurs. */
size_t DLLENTRY
xio_fwrite( const void* buffer, size_t size, size_t count, XFILE* x )
{
  int write = count * size;
  int rc    = 0;
  int done;

  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fwrite] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return 0;
  }

  DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );

  if(!( x->oflags & XO_WRITE )) {
    errno = EBADF;
    x->protocol->error = 1;
  } else {
    done = buffer_write( x, buffer, write );
    x->protocol->ungetc = EOF;

    if( done == write ) {
      rc = count;
    } else if( done >= 0 ) {
      int remain = done % size;
      if( remain ) {
        buffer_seek( x, -remain, XIO_SEEK_CUR );
      }
      rc = done / size;
    } else {
      x->protocol->error = 1;
    }
  }

  DosReleaseMutexSem( x->protocol->mtx_file );
  return rc;
}

/* Closes a file pointed to by x. Returns 0 if it successfully closes
   the file, or -1 if any errors were detected. */
int DLLENTRY
xio_fclose( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fclose] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  }

  DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );

  if( x->protocol->abort || x->protocol->close( x ) == 0 ) {
    DosReleaseMutexSem( x->protocol->mtx_file );
    xio_terminate( x );
    return 0;
  }

  DosReleaseMutexSem( x->protocol->mtx_file );
  xio_terminate( x );
  return -1;
}

/* Causes an abnormal termination of all current read/write
   operations of the file. All current and subsequent calls can
   raise an error. */
void DLLENTRY
xio_fabort( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fabort] file handle %08X is not valid.\n", x ));
    errno = EBADF;
  } else {
    if( !x->protocol->abort ) {
      x->protocol->abort = 1;
      x->protocol->close( x );
    }
  }
}

/* Determines the file handle currently associated with stream.
   If the function fails, the return value is -1. */
int DLLENTRY
xio_fileno( XFILE*  x )
{
  if( !x || x->serial != XIO_SERIAL || x->scheme != XIO_FILE ) {
    DEBUGLOG(( "xio: [xio_fileno] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  } else {
    return x->protocol->s_handle;
  }
}

/* Finds the current position of the file. Returns the current file
   position. On error, returns -1 and sets errno to a nonzero value. */
long long DLLENTRY
xio_ftell64( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_ftell] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  } else {
    long long pos = buffer_tell( x );
    if( x->protocol->ungetc != EOF && pos > 0 ) {
      --pos;
    }
    return pos;
  }
}

long DLLENTRY
xio_ftell( XFILE* x )
{
  long long curpos = xio_ftell64( x );

  if( curpos > XIO_2GB_LIMIT ) {
    errno = EFBIG;
    return -1;
  } else {
    return curpos;
  }
}

/* Stores the current position of the file pointer associated with stream
   into the object pointed to by pos. The value pointed to by pos can be
   used later in a call to xio_fsetpos to reposition the stream.
   Returns 0 if successful. On error, returns nonzero and sets errno to a
   nonzero value. */
int DLLENTRY
xio_fgetpos64( XFILE* x, xio_fpos64_t* pos )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fgetpos] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  } else {
    *pos = xio_ftell64( x );
    return 0;
  }
}

int DLLENTRY
xio_fgetpos( XFILE* x, xio_fpos_t* pos )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fgetpos] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  }
  else
  {
    long long curpos = xio_ftell64( x );

    if( curpos > XIO_2GB_LIMIT ) {
      errno = EFBIG;
      return -1;
    } else {
     *pos = curpos;
      return 0;
    }
  }
}

/* Changes the current file position to a new location within the file.
   Returns 0 if it successfully moves the pointer. A nonzero return
   value indicates an error. On devices that cannot seek the return
   value is nonzero. */
int DLLENTRY
xio_fseek64( XFILE* x, long long offset, int origin )
{
  int rc = -1;

  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fseek] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  }

  DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );

  if( x->protocol->s_metaint ) {
    errno = EINVAL;
    x->protocol->error  = 1;
  } else if( buffer_seek( x, offset, origin ) != -1 ) {
    x->protocol->error  = 0;
    x->protocol->eof    = 0;
    x->protocol->ungetc = EOF;
    rc = 0;
  } else {
    x->protocol->error  = 1;
  }

  DosReleaseMutexSem( x->protocol->mtx_file );
  return rc;
}

int DLLENTRY
xio_fseek( XFILE* x, long offset, int origin ) {
  return xio_fseek64( x, offset, origin );
}

/* Moves any file position associated with stream to a new location within
   the file according to the value pointed to by pos. The value of pos
   was obtained by a previous call to the xio_fgetpos library function.
   If successfully changes the current position of the file, it returns 0.
   A nonzero return value indicates an error. */
int DLLENTRY
xio_fsetpos64( XFILE* x, const xio_fpos64_t* pos )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fsetpos] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  } else {
    return xio_fseek64( x, *pos, XIO_SEEK_SET );
  }
}

int DLLENTRY
xio_fsetpos( XFILE* x, const xio_fpos_t* pos )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fsetpos] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  } else {
    return xio_fseek64( x, *pos, XIO_SEEK_SET );
  }
}

/* Repositions the file pointer associated with stream to the beginning
   of the file. A call to xio_rewind is the same as:
   (void)xio_fseek( x, 0, XIO_SEEK_SET )
   except that xio_rewind also clears the error indicator for
   the stream. */
void DLLENTRY
xio_rewind( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_rewind] file handle %08X is not valid.\n", x ));
    errno = EBADF;
  } else {
    if( !x->protocol->s_metaint ) {
      xio_fseek64( x, 0, XIO_SEEK_SET );
    }
    x->protocol->error  = 0;
    x->protocol->eof    = 0;
    x->protocol->ungetc = EOF;
    errno = 0;
  }
}

/* Returns the size of the file. A return value of -1 indicates an
   error or an unknown size. */
long long DLLENTRY
xio_fsize64( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fsize] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  } else {
    return buffer_filesize( x );
  }
}

long DLLENTRY
xio_fsize( XFILE* x )
{
  long long cursize = xio_fsize64( x );

  if( cursize > XIO_2GB_LIMIT ) {
    errno = EFBIG;
    return -1;
  } else {
    return cursize;
  }
}

/* Lengthens or cuts off the file to the length specified by size.
   You must open the file in a mode that permits writing. Adds null
   characters when it lengthens the file. When cuts off the file, it
   erases all data from the end of the shortened file to the end
   of the original file. Returns the value 0 if it successfully
   changes the file size. A return value of -1 shows an error. */
int DLLENTRY
xio_ftruncate64( XFILE* x, long long size )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_ftruncate] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  } else {
    return buffer_truncate( x, size );
  }
}

int DLLENTRY
xio_ftruncate( XFILE* x, long size ) {
  return xio_ftruncate64( x, size );
}

/* Reads bytes from the current file position up to and including the
   first new-line character (\n), up to the end of the file, or until
   the number of bytes read is equal to n-1, whichever comes first.
   Stores the result in string and adds a null character (\0) to the
   end of the string. The string includes the new-line character, if
   read. If n is equal to 1, the string is empty. Returns a pointer
   to the string buffer if successful. A NULL return value indicates
   an error or an end-of-file condition. */
char* DLLENTRY
xio_fgets( char* string, int n, XFILE* x )
{
  int done = 0;
  int read;

  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fgets] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return NULL;
  }

  DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );

  if(!( x->oflags & XO_READ )) {
    errno = EINVAL;
    x->protocol->error = 1;
  } else {
    while( done < n - 1 ) {
      if(( read = xio_read_and_notify( x, string, 1 )) == 1 ) {
        if( *string == '\r' ) {
          continue;
        } else if( *string == '\n' ) {
          ++string;
          ++done;
          break;
        } else {
          ++string;
          ++done;
        }
      } else if( read == 0 ) {
        x->protocol->eof   = 1;
        break;
      } else {
        x->protocol->error = 1;
        break;
      }
    }
  }

  DosReleaseMutexSem( x->protocol->mtx_file );

  *string = 0;
  return done ? string : NULL;
}

/* Reads a single byte from the current stream position and advances
   the stream position to the next byte. Returns the value read. A
   return value of EOF indicates an error or end-of-file condition.
   Use xio_ferror or xio_feof to determine whether an error or an
   end-of-file condition occurred. */
int DLLENTRY
xio_getc( XFILE* x )
{
  int  read = -1;
  char ch;

  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fgetc] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return EOF;
  }

  DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );

  if(!( x->oflags & XO_READ )) {
    errno = EINVAL;
  } else {
    read = xio_read_and_notify( x, &ch, 1 );
  }

  DosReleaseMutexSem( x->protocol->mtx_file );

  if( read == 1 ) {
    return ch;
  } else if( read == 0 ) {
    x->protocol->eof = 1;
  } else {
    x->protocol->error = 1;
  }

  return EOF;
}

/* Pushes the byte c back onto the given input stream. However, only
   one sequential byte is guaranteed to be pushed back onto the input
   stream if you call ungetc consecutively. The stream must be open
   for reading. A subsequent read operation on the stream starts with
   c. The byte c cannot be the EOF character. Bytes placed on the
   stream by xio_ungetc will be erased if xio_fseek, xio_fsetpos,
   xio_rewind is called before the byte is read from the stream.
   Returns the integer argument c or EOF if c cannot be pushed back. */
int DLLENTRY
xio_ungetc( int c, XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_ungetc] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return EOF;
  }
  if(!( x->oflags & XO_READ )) {
    errno = EINVAL;
    x->protocol->error = 1;
    return EOF;
  }

  DosRequestMutexSem( x->protocol->mtx_access, SEM_INDEFINITE_WAIT );

  if( x->protocol->ungetc == EOF ) {
    x->protocol->ungetc = c;
  } else {
    c = EOF;
  }

  DosReleaseMutexSem( x->protocol->mtx_access );
  return c;
}

/* Copies string to the output file at the current position.
   It does not copy the null character (\0) at the end of the string.
   Returns -1 if an error occurs; otherwise, it returns a non-negative
   value. */
int DLLENTRY
xio_fputs( const char* string, XFILE* x )
{
  char* cr = "\r";
  int   rc = 0;

  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_fputs] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return -1;
  }

  DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );

  while( *string ) {
    if( *string == '\n' ) {
      if( buffer_write( x, cr, 1 ) != 1 ) {
        x->protocol->error = 1;
        rc = -1;
        break;
      }
    }
    if( buffer_write( x, string++, 1 ) != 1 ) {
      x->protocol->error = 1;
      rc = -1;
      break;
    }
  }

  x->protocol->ungetc = EOF;
  DosReleaseMutexSem( x->protocol->mtx_file );
  return rc;
}

/* Indicates whether the end-of-file flag is set for the given stream.
   The end-of-file flag is set by several functions to indicate the
   end of the file. The end-of-file flag is cleared by calling xio_rewind,
   xio_fseek, or xio_clearerr for this stream. */
int DLLENTRY
xio_feof( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_feof] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return 1;
  } else {
    return x->protocol->eof;
  }
}

/* Tests for an error in reading from or writing to the given stream.
   If an error occurs, the error indicator for the stream remains set
   until you close stream, call xio_rewind, or call xio_clearerr. */
int DLLENTRY
xio_ferror( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_ferror] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    return 1;
  } else {
    return x->protocol->error;
  }
}

/* Resets the error indicator and end-of-file indicator for the
   specified stream. Once set, the indicators for a specified stream
   remain set until your program calls xio_clearerr or xio_rewind.
   xio_fseek also clears the end-of-file indicator. */
void DLLENTRY
xio_clearerr( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_clearerr] file handle %08X is not valid.\n", x ));
    errno = EBADF;
  } else {
    DosRequestMutexSem( x->protocol->mtx_file, SEM_INDEFINITE_WAIT );
    x->protocol->error = 0;
    x->protocol->eof   = 0;
    DosReleaseMutexSem( x->protocol->mtx_file );
  }
}

/* Returns the last error code set by a library call in the current
   thread. Subsequent calls do not reset this error code. */
int DLLENTRY
xio_errno( void ) {
  return errno;
}

/* Maps the error number in errnum to an error message string. */
const char* DLLENTRY
xio_strerror( int errnum )
{
  if( errnum >= TLSBASEERR ) {
    errnum -= TLSBASEERR;
    return errnum & 0xFF80 ?
      mbedtls_high_level_strerr( errnum ) :
      mbedtls_low_level_strerr ( errnum );
  } else if( errnum >= CDDBBASEERR ) {
    return cddb_strerror( errnum );
  } else if( errnum >= FTPBASEERR ) {
    return ftp_strerror( errnum );
  } else if( errnum >= HTTPBASEERR ) {
    return http_strerror( errnum );
  } else if( errnum >= HBASEERR ) {
    return h_strerror( errnum - HBASEERR );
  } else if( errnum >= FILEBASEERR ) {
    return file_strerror( errnum - FILEBASEERR );
  #ifdef SOCBASEERR
  } else if( errnum >= SOCBASEERR ) {
    return sock_strerror( errnum );
  #endif
  } else {
    return strerror( errnum );
  }
}

/* Sets a handle of a window that are to be notified of changes
   in the state of the library. */
void DLLENTRY
xio_set_observer( XFILE* x, unsigned long window,
                            char* buffer, int buffer_size )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_set_observer] file handle %08X is not valid.\n", x ));
    errno = EBADF;
  } else {
    DosRequestMutexSem( x->protocol->mtx_access, SEM_INDEFINITE_WAIT );
    x->protocol->s_observer = window;
    x->protocol->s_metabuff = buffer;
    x->protocol->s_metasize = buffer_size;
    DosReleaseMutexSem( x->protocol->mtx_access );
  }
}

/* Returns a specified meta information if it is
   provided by associated stream. */
char* DLLENTRY
xio_get_metainfo( XFILE* x, int type, char* result, int size )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_get_metainfo] file handle %08X is not valid.\n", x ));
    errno = EBADF;
    *result = 0;
  } else {
    DosRequestMutexSem( x->protocol->mtx_access, SEM_INDEFINITE_WAIT );

    switch( type ) {
      case XIO_META_GENRE : strlcpy( result, x->protocol->s_genre, size ); break;
      case XIO_META_NAME  : strlcpy( result, x->protocol->s_name , size ); break;
      case XIO_META_TITLE : strlcpy( result, x->protocol->s_title, size ); break;
      case XIO_META_TYPE  : strlcpy( result, x->protocol->s_type,  size ); break;
      default:
        *result = 0;
    }
    DosReleaseMutexSem( x->protocol->mtx_access );
  }
  return result;
}

/* Returns XIO_NOT_SEEK (0) on streams incapable of seeking,
   XIO_CAN_SEEK (1) on streams capable of seeking and returns
   XIO_CAN_SEEK_FAST (2) on streams capable of fast seeking. */
int DLLENTRY
xio_can_seek( XFILE* x )
{
  if( !x || x->serial != XIO_SERIAL ) {
    DEBUGLOG(( "xio: [xio_can_seek] file handle %08X is not valid.\n", x ));
    errno = EBADF;
  } else {
    if( !x->protocol->s_metaint ) {
      if( x->protocol->supports & XS_CAN_SEEK_FAST ) {
        return XIO_CAN_SEEK_FAST;
      } else if( x->protocol->supports & XS_CAN_SEEK ) {
        return XIO_CAN_SEEK;
      }
    }
  }
  return XIO_NOT_SEEK;
}

/* Returns the read-ahead buffer size. */
size_t DLLENTRY
xio_buffer_size( void ) {
  return buffer_size;
}

/* Returns fills the buffer before reading state. */
int DLLENTRY
xio_buffer_wait( void ) {
  return buffer_wait;
}

/* Returns value of prefilling of the buffer. */
int DLLENTRY
xio_buffer_fill( void ) {
  return buffer_fill;
}

/* Returns number of bytes that must be preloaded to buffer. */
size_t DLLENTRY
xio_buffer_preload( void ) {
  return buffer_preload;
}

/* Sets the read-ahead buffer size. */
void DLLENTRY
xio_set_buffer_size( size_t size ) {
  buffer_size = size;
}

/* Sets fills the buffer before reading state. */
void DLLENTRY
xio_set_buffer_wait( int wait ) {
  buffer_wait = wait;
}

/* Sets value of prefilling of the buffer. */
void DLLENTRY
xio_set_buffer_fill( int percent )
{
  if( percent > 0 && percent <= 100 ) {
    buffer_fill = percent;
  }
}

/* Sets number of bytes that must be preloaded to buffer. */
void DLLENTRY
xio_set_buffer_preload( size_t preload ) {
  buffer_preload = preload;
}

/* Returns the name of the proxy server. */
char* DLLENTRY
xio_http_proxy_host( char* hostname, int size )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( hostname, http_proxy_host, size );
  DosReleaseMutexSem( mutex );
  return hostname;
}

/* Returns the port number of the proxy server. */
int DLLENTRY
xio_http_proxy_port( void ) {
  return http_proxy_port;
}

/* Returns the user name of the proxy server. */
char* DLLENTRY
xio_http_proxy_user( char* username, int size )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( username, http_proxy_user, size );
  DosReleaseMutexSem( mutex );
  return username;
}

/* Returns the user password of the proxy server. */
char* DLLENTRY
xio_http_proxy_pass( char* password, int size )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( password, http_proxy_pass, size );
  DosReleaseMutexSem( mutex );
  return password;
}

/* Sets the name of the proxy server. */
void DLLENTRY
xio_set_http_proxy_host( const char* hostname )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );

  // Addition a HTTP scheme to the proxy hostname is a common mistake.
  if( strnicmp( hostname, "http://", 7 ) == 0 ) {
    strlcpy( http_proxy_host, hostname + 7, sizeof( http_proxy_host ));
  } else {
    strlcpy( http_proxy_host, hostname, sizeof( http_proxy_host ));
  }

  http_proxy_addr = 0;
  DosReleaseMutexSem( mutex );
}

/* Sets the port number of the proxy server. */
void DLLENTRY
xio_set_http_proxy_port( int port ) {
  http_proxy_port = port;
}

/* Sets the user name of the proxy server. */
void DLLENTRY
xio_set_http_proxy_user( const char* username )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( http_proxy_user, username, sizeof( http_proxy_user ));
  DosReleaseMutexSem( mutex );
}

/* Sets the user password of the proxy server. */
void DLLENTRY
xio_set_http_proxy_pass( const char* password )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( http_proxy_pass, password, sizeof( http_proxy_pass ));
  DosReleaseMutexSem( mutex );
}

/* Returns an internet address of the proxy server.
   Returns 0 if the proxy server is not defined or -1 if
   an error occurs */
unsigned long DLLENTRY
xio_http_proxy_addr( void )
{
  char host[XIO_MAX_HOSTNAME];
  u_long address;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( host, http_proxy_host, sizeof( host ));
  address = http_proxy_addr;
  DosReleaseMutexSem( mutex );

  if( address != 0 && address != -1 ) {
    return address;
  }
  if( !*host ) {
    return 0;
  }

  address = so_get_address( host );

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  http_proxy_addr = address;
  DosReleaseMutexSem( mutex );
  return address;
}

/* Returns the TCP/IP connection timeout. */
int DLLENTRY
xio_connect_timeout( void ) {
  return socket_timeout;
}

/* Sets the TCP/IP connection timeout. */
void DLLENTRY
xio_set_connect_timeout( int seconds ) {
  socket_timeout = seconds;
}

/* Sets the domains for which no proxy is used. */
void DLLENTRY
xio_set_no_proxy( const char* domains )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( http_proxy_bypass, domains, sizeof( http_proxy_bypass ));
  DosReleaseMutexSem( mutex );
}

/* Returns the domains for which no proxy is used. */
char* DLLENTRY
xio_no_proxy( char* domains, int size )
{
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  strlcpy( domains, http_proxy_bypass, size );
  DosReleaseMutexSem( mutex );
  return domains;
}

/* Returns 1 if proxy server bypass local addresses. */
int DLLENTRY
xio_bypass_local( void ) {
  return http_proxy_bypass_local;
}

/* Tells proxy server to bypass local addresses. */
void DLLENTRY
xio_set_bypass_local( int bypass ) {
  http_proxy_bypass_local = bypass;
}

int INIT_ATTRIBUTE __dll_initialize( void )
{
  if( DosCreateMutexSem( NULL, &mutex,     0, FALSE ) != NO_ERROR ) {
    return 0;
  }
  #ifdef XIO_SERIALIZE_DISK_IO
  if( DosCreateMutexSem( NULL, &serialize, 0, FALSE ) != NO_ERROR ) {
    return 0;
  }
  #endif

  return 1;
}

int TERM_ATTRIBUTE __dll_terminate( void )
{
  DosCloseMutexSem( mutex     );
  #ifdef XIO_SERIALIZE_DISK_IO
  DosCloseMutexSem( serialize );
  #endif
  return 1;
}

#if defined(__IBMC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag       )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    return __dll_initialize();
  } else if( flag == DLL_PROCESS_DETACH ) {
    #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
    #endif
    __dll_terminate();
    _CRT_term();
  }
  return 1UL;
}
#endif

