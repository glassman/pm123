/*
 * Copyright 2022 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_ERRORS
#include <os2.h>
#include <stdlib.h>
#include <string.h>
#include <debuglog.h>
#include <fileutil.h>

#include "xio_tls.h"
#include "xio_socket.h"

static char* drbg_personalized_str = "XIO/1.0 (OS/2)";

#if DEBUG

/* Logs the TLS connection. */
static void
tls_debuglog( void* ctx, int level, const char* file,
                              int line, const char* str )
{
  char basename[_MAX_PATH];
  sfnameext( basename, file, sizeof( basename ));
  DEBUGLOG(( "xio: [tls] %s:%d: [%d] %s", basename, line, level, str ));
}

#endif

/* Perform the SSL handshake */
int
tls_ssl_handshake( XFILE* x )
{
  XTLS* tls = x->protocol->s_tls;
  int   rc;

  if( tls )
  {
    DosRequestMutexSem( tls->mutex, SEM_INDEFINITE_WAIT );
    mbedtls_ssl_set_bio( &tls->ssl, (void*)x->protocol->s_handle,
                                    (mbedtls_ssl_send_t*)so_write,
                                    (mbedtls_ssl_recv_t*)so_read, NULL );

    if(( rc = mbedtls_ssl_handshake( &tls->ssl )) == 0 ) {
      tls->active = TRUE;
    }
    DosReleaseMutexSem( tls->mutex );
  } else {
    DEBUGLOG(( "xio: [tls] try to use uninitialized TLS connection\n" ));
    rc = MBEDTLS_ERR_SSL_INTERNAL_ERROR;
  }

  if( rc != 0 ) {
    errno = TLSBASEERR + abs( rc );
  }

  return rc;
}

/* Reset an already initialized SSL context for re-use while
   retaining application-set variables, function pointers and data. */
int
tls_session_reset( XFILE* x )
{
  XTLS* tls = x->protocol->s_tls;
  int   rc  = 0;

  if( tls ) {
    DosRequestMutexSem( tls->mutex, SEM_INDEFINITE_WAIT );
    mbedtls_ssl_close_notify( &tls->ssl );
    rc = mbedtls_ssl_session_reset( &tls->ssl );
    tls->active = FALSE;
    DosReleaseMutexSem( tls->mutex );
  }

  if( rc != 0 ) {
    errno = TLSBASEERR + abs( rc );
  }

  return rc;
}


/* Receive application data decrypted from the SSL layer and stores
   it in the buffer. */
int
tls_read( XFILE* x, char* buffer, int size )
{
  XTLS* tls = x->protocol->s_tls;

  if( tls && tls->active )
  {
    int read = 0;
    int done;

    while( read < size )
    {
      DosRequestMutexSem( tls->mutex, SEM_INDEFINITE_WAIT );
      done = mbedtls_ssl_read( &tls->ssl, (unsigned char*)buffer + read, size - read );
      DosReleaseMutexSem( tls->mutex );

      if( done < 0 ) {
        errno = TLSBASEERR + abs( done );
        break;
      } else if( done == 0 ) {
        break;
      } else {
        read += done;
      }
    }

    return read;
  } else {
    return so_read( x->protocol->s_handle, buffer, size );
  }
}

/* Receive application data decrypted from the SSL layer up to the
   first new-line character (\n) or until the number of bytes received is
   equal to n-1, whichever comes first. */
char*
tls_readline( XFILE* x, char* buffer, int size )
{
  XTLS* tls = x->protocol->s_tls;

  if( tls && tls->active )
  {
    int done = 0;
    char* p  = buffer;

    while( done < size - 1 ) {
      if( tls_read( x, p, 1 ) == 1 ) {
        if( *p == '\r' ) {
          continue;
        } else if( *p == '\n' ) {
          break;
        } else {
          ++p;
          ++done;
        }
      } else {
        if( !done ) {
          return NULL;
        } else {
          break;
        }
      }
    }

    *p = 0;
    return buffer;
  } else {
    return so_readline( x->protocol->s_handle, buffer, size );
  }
}

/* Send application data to be encrypted by the SSL layer,
   taking care of max fragment length and buffer size. */
int
tls_write( XFILE* x, const char* buffer, int size )
{
  XTLS* tls = x->protocol->s_tls;

  if( tls && tls->active )
  {
    int write = size;
    int done;

    while( write )
    {
      DosRequestMutexSem( tls->mutex, SEM_INDEFINITE_WAIT );
      done = mbedtls_ssl_write( &tls->ssl, (const unsigned char*)buffer, write );
      DosReleaseMutexSem( tls->mutex );

      if( done <= 0 ) {
        errno = TLSBASEERR + abs( done );
        return done;
      }

      buffer += done;
      write  -= done;
    }

    return size;
  } else {
    return so_write( x->protocol->s_handle, buffer, size );
  }
}

/* Cleanups the TLS connection. */
void
tls_terminate( XTLS* tls )
{
  if( tls ) {
    mbedtls_entropy_free( &tls->entropy );
    mbedtls_ctr_drbg_free( &tls->ctr_drbg );
    mbedtls_ssl_free( &tls->ssl );
    mbedtls_ssl_config_free( &tls->config );

    if( tls->mutex ) {
      DosCloseMutexSem( tls->mutex );
    }

    free( tls );
  }
}

/* Initializes the TLS connection. */
XTLS*
tls_initialize( XFILE* x )
{
  XTLS* tls = calloc( 1, sizeof( XTLS ));
  int   rc;

  if( tls )
  {
    if( DosCreateMutexSem( NULL, &tls->mutex, 0, FALSE ) != NO_ERROR )
    {
      free( tls );
      return NULL;
    }

    mbedtls_entropy_init( &tls->entropy );
    mbedtls_ctr_drbg_init( &tls->ctr_drbg );
    mbedtls_ssl_init( &tls->ssl );
    mbedtls_ssl_config_init( &tls->config );

    #if DEBUG
      mbedtls_ssl_conf_dbg( &tls->config, tls_debuglog, NULL );
      mbedtls_debug_set_threshold( TLS_DEBUG_LEVEL );
    #endif

    rc = mbedtls_ctr_drbg_seed( &tls->ctr_drbg, mbedtls_entropy_func, &tls->entropy,
            (const unsigned char*)drbg_personalized_str,
            strlen( drbg_personalized_str ) + 1 );

    if( rc != 0 ) {
      errno = TLSBASEERR + abs( rc );
      tls_terminate( tls );
      return NULL;
    }

    rc = mbedtls_ssl_config_defaults( &tls->config, MBEDTLS_SSL_IS_CLIENT,
                                      MBEDTLS_SSL_TRANSPORT_STREAM,
                                      MBEDTLS_SSL_PRESET_DEFAULT );
    if( rc != 0 ) {
      errno = TLSBASEERR + abs( rc );
      tls_terminate( tls );
      return NULL;
    }

    mbedtls_ssl_conf_rng( &tls->config, mbedtls_ctr_drbg_random, &tls->ctr_drbg );
    mbedtls_ssl_conf_authmode( &tls->config, MBEDTLS_SSL_VERIFY_NONE );

    rc = mbedtls_ssl_setup( &tls->ssl, &tls->config );

    if( rc != 0 ) {
      errno = TLSBASEERR + abs( rc );
      tls_terminate( tls );
      return NULL;
    }
  }

  return tls;
}

