/*
 * Copyright 2011 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef XSTDIO_H
#define XSTDIO_H

#include <config.h>
#include <xio.h>

#ifdef __cplusplus
extern "C" {
#endif

#undef  SEEK_SET
#undef  SEEK_CUR
#undef  SEEK_END

#define SEEK_SET   XIO_SEEK_SET
#define SEEK_CUR   XIO_SEEK_CUR
#define SEEK_END   XIO_SEEK_END

typedef XFILE      FILE;
typedef xio_fpos_t fpos_t;

#define fopen      xio_fopen
#define fread      xio_fread
#define fwrite     xio_fwrite
#define fclose     xio_fclose
#define ftell      xio_ftell
#define fseek      xio_fseek
#define rewind     xio_rewind
#define fgets      xio_fgets
#define fputs      xio_fputs
#define feof       xio_feof
#define ferror     xio_ferror
#define clearerr   xio_clearerr
#define fileno     xio_fileno
#define fgetpos    xio_fgetpos
#define fsetpos    xio_fsetpos

#if !defined(__GNUC__)
#define ftruncate chsize
#endif

#undef  errno
#undef  strerror
#define errno      xio_errno()
#define strerror   xio_strerror

#ifdef  __WATCOMC__
#define CRTLINK _WCRTLINK
#else
#define CRTLINK
#endif

CRTLINK extern int sprintf( char *, const char *, ... );

#ifdef __cplusplus
}
#endif
#endif /* XSTDIO_H */

