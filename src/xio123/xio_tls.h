/*
 * Copyright 2022 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef XIO_TLS_H
#define XIO_TLS_H

#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/error.h>
#include <mbedtls/ssl.h>
#include <mbedtls/debug.h>

#include "xio.h"
#include "xio_protocol.h"

#ifndef TLSBASEERR
#define TLSBASEERR 65536
#endif

/* Change to a number between 1 and 4 to debug the TLS connection. */
#define TLS_DEBUG_LEVEL 1

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _XTLS {

  HMTX                     mutex;     /* Serializes all operations.                   */
  mbedtls_entropy_context  entropy;   /* Entropy context used to seed the DRBG to use */
                                      /* in the TLS connection.                       */
  mbedtls_ctr_drbg_context ctr_drbg;  /* The DRBG used throughout the TLS connection. */
  mbedtls_ssl_context      ssl;       /* The TLS context.                             */
  mbedtls_ssl_config       config;    /* The TLS configuration in use.                */
  BOOL                     active;    /* The TLS connection is active.                */

} XTLS;

/* Initializes the TLS connection. */
XTLS* tls_initialize( XFILE* x );
/* Cleanup the TLS connection. */
void tls_terminate( XTLS* tls );

/* Perform the SSL handshake. If this function returns something
   other than zero, you must stop using the TSL context for
   reading or writing, and either cleanup it or call tls_session_reset()
   on it before re-using it for a new connection; the current
   connection must be closed. */
int tls_ssl_handshake( XFILE* x );

/* Reset an already initialized SSL context for re-use while
   retaining application-set variables, function pointers and data. */
int tls_session_reset( XFILE* x );

/* Receive application data decrypted from the SSL layer and stores it in
   the buffer. When successful, the number of bytes of data received
   into the buffer is returned. The value 0 indicates that the connection
   is closed. The negative value indicates an error. If the XFILE
   structure does not contain an initialized TLS connection, then the
   data is read from the s_handle socket. */
int tls_read( XFILE* x, char* buffer, int size );

/* Receive application data decrypted from the SSL layer up to the
   first new-line character (\n) or until the number of bytes received is
   equal to n-1, whichever comes first. Stores the result in string and
   adds a null character (\0) to the end of the string. If n is equal to 1,
   the string is empty. Returns a pointer to the string buffer if successful.
   A NULL return value indicates an error or that the connection is
   closed. If the XFILE structure does not contain an initialized
   TLS connection, then the data is read from the s_handle socket. */
char* tls_readline( XFILE* x, char* buffer, int size );

/* Send application data to be encrypted by the SSL layer,
   taking care of max fragment length and buffer size. If the XFILE
   structure does not contain an initialized TLS connection, then the
   data is send to the s_handle socket. */
int tls_write( XFILE* x, const char* buffer, int size );

#ifdef __cplusplus
}
#endif
#endif /* XIO_TLS_H */

