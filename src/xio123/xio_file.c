/*
 * Copyright 2006-2022 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_ERRORS
#define  INCL_LONGLONG
#include <os2.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <io.h>
#include <share.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <ctype.h>

#include "xio_file.h"
#include "xio_url.h"
#include "xio_exceptions.h"

#include <debuglog.h>

#ifdef XIO_SERIALIZE_DISK_IO

  HMTX serialize;

  // Serializes all read and write disk operations. This is
  // improve performance of poorly implemented filesystems (OS/2 version
  // of FAT32 for example).

  #define FILE_REQUEST_DISK( x ) if( x->protocol->s_serialized ) { DosRequestMutexSem( serialize, SEM_INDEFINITE_WAIT );}
  #define FILE_RELEASE_DISK( x ) if( x->protocol->s_serialized ) { DosReleaseMutexSem( serialize ); }
#else
  #define FILE_REQUEST_DISK( x )
  #define FILE_RELEASE_DISK( x )
#endif

#ifdef XIO_SERIALIZE_DISK_IO
static int
is_singletasking_fs( const char* filename )
{
  if( isalpha( filename[0] ) && filename[1] == ':' )
  {
    // Return-data buffer should be large enough to hold FSQBUFFER2
    // and the maximum data for szName, szFSDName, and rgFSAData
    // Typically, the data isn't that large.

    BYTE fsqBuffer[ sizeof( FSQBUFFER2 ) + ( 3 * CCHMAXPATH )] = { 0 };
    PFSQBUFFER2 pfsqBuffer = (PFSQBUFFER2)fsqBuffer;

    ULONG  cbBuffer        = sizeof( fsqBuffer );
    PCHAR  pszFSDName      = NULL;
    CHAR   szDeviceName[3] = "x:";
    APIRET rc;

    szDeviceName[0] = filename[0];

    rc = DosQueryFSAttach( szDeviceName,    // Logical drive of attached FS
                           0,               // Ignored for FSAIL_QUERYNAME
                           FSAIL_QUERYNAME, // Return data for a Drive or Device
                           pfsqBuffer,      // Returned data
                          &cbBuffer );      // Returned data length

    // On successful return, the fsqBuffer structure contains
    // a set of information describing the specified attached
    // file system and the DataBufferLen variable contains
    // the size of information within the structure.

    if( rc == NO_ERROR ) {
      pszFSDName = (PCHAR)pfsqBuffer->szName + pfsqBuffer->cbName + 1;
      if( stricmp( pszFSDName, "FAT32" ) == 0 ) {
        DEBUGLOG(( "xio: detected %s filesystem for file %s, serialize access.\n", pszFSDName, filename ));
        return 1;
      } else {
        DEBUGLOG(( "xio: detected %s filesystem for file %s.\n", pszFSDName, filename ));
      }
    }
  }
  return 0;
}
#endif

/* Opens the file specified by filename. Returns 0 if it
   successfully opens the file. A return value of -1 shows an error. */
static int
file_open( XFILE* x, const char* filename, int oflags )
{
  ULONG    flags = 0;
  ULONG    omode = OPEN_FLAGS_SEQUENTIAL | OPEN_FLAGS_NOINHERIT;
  char*    pname = (char*)filename;
  XURL*    url   = NULL;
  ULONG    action;
  LONGLONG actual;
  APIRET   rc;

  if(( oflags & XO_WRITE ) && ( oflags & XO_READ )) {
    omode |= OPEN_ACCESS_READWRITE | OPEN_SHARE_DENYWRITE;
  } else if( oflags & XO_WRITE ) {
    omode |= OPEN_ACCESS_WRITEONLY | OPEN_SHARE_DENYWRITE;
  } else if( oflags & XO_READ ) {
    omode |= OPEN_ACCESS_READONLY  | OPEN_SHARE_DENYNONE;
  }

  if( oflags & XO_CREATE ) {
    flags |= OPEN_ACTION_CREATE_IF_NEW;
  }
  if( oflags & XO_TRUNCATE ) {
    flags |= OPEN_ACTION_REPLACE_IF_EXISTS;
  } else {
    flags |= OPEN_ACTION_OPEN_IF_EXISTS;
  }

  if( strnicmp( filename, "file:", 5 ) == 0 )
  {
    url = url_allocate( filename );

    if( !url->path ) {
      url_free( url );
      errno = FILEBASEERR + ERROR_FILE_NOT_FOUND;
      return -1;
    }

    // Converts leading drive letters of the form C| to C:
    // and if a drive letter is present or we have UNC path
    // strips off the slash that precedes path. Otherwise,
    // the leading slash is used.

    for( pname = url->path; *pname; pname++ ) {
      if( *pname == '/'  ) {
          *pname = '\\';
      }
    }

    pname = url->path;

    if( isalpha( pname[1] ) && ( pname[2] == '|' || pname[2] == ':' )) {
      pname[2] = ':';
      ++pname;
    } else if ( pname[1] == '\\' && pname[2] == '\\' ) {
      ++pname;
    }
  }

  #ifdef XIO_SERIALIZE_DISK_IO
  x->protocol->s_serialized = is_singletasking_fs( pname );
  #endif

  FILE_REQUEST_DISK(x);
  rc = DosOpenL( pname, &x->protocol->s_handle, &action, 0, FILE_NORMAL, flags, omode, NULL );

  if( rc == NO_ERROR && ( oflags & XO_APPEND )) {
    rc = DosSetFilePtrL( x->protocol->s_handle, 0, FILE_END, &actual );
  }
  FILE_RELEASE_DISK(x);
  url_free( url );

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return  0;
  }
}

/* Reads count bytes from the file into buffer. Returns the number
   of bytes placed in result. The return value 0 indicates an attempt
   to read at end-of-file. A return value -1 indicates an error.     */
static int
file_read( XFILE* x, char* result, unsigned int count )
{
  APIRET rc;
  ULONG  actual;

  FILE_REQUEST_DISK(x);

  XIO_BEGINSEQUENCE {
    rc = DosRead( x->protocol->s_handle, result, count, &actual );
  } XIO_RECOVER {
    DEBUGLOG(( "xio: access violation exception occurred\n" ));
    rc = ERROR_PROTECTION_VIOLATION;
  } XIO_END;

  FILE_RELEASE_DISK(x);

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return actual;
  }
}

/* Writes count bytes from source into the file. Returns the number
   of bytes moved from the source to the file. The return value may
   be positive but less than count. A return value of -1 indicates an
   error */
static int
file_write( XFILE* x, const char* source, unsigned int count )
{
  APIRET rc;
  ULONG  actual;

  FILE_REQUEST_DISK(x);
  rc = DosWrite( x->protocol->s_handle, (PVOID)source, count, &actual );
  FILE_RELEASE_DISK(x);

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return actual;
  }
}

/* Closes the file. Returns 0 if it successfully closes the file. A
   return value of -1 shows an error. */
static int
file_close( XFILE* x )
{
  APIRET rc;

  FILE_REQUEST_DISK(x);
  rc = DosClose( x->protocol->s_handle );
  FILE_RELEASE_DISK(x);

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return 0;
  }
}

/* Returns the current position of the file pointer. The position is
   the number of bytes from the beginning of the file. On devices
   incapable of seeking, the return value is -1. */
static long long
file_tell( XFILE* x )
{
  APIRET   rc;
  LONGLONG actual;

  FILE_REQUEST_DISK(x);
  rc = DosSetFilePtrL( x->protocol->s_handle, 0, FILE_CURRENT, &actual );
  FILE_RELEASE_DISK(x);

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return actual;
  }
}

/* Moves any file pointer to a new location that is offset bytes from
   the origin. Returns the offset, in bytes, of the new position from
   the beginning of the file. A return value of -1 indicates an
   error. */
static long long
file_seek( XFILE* x, long long offset, int origin )
{
  ULONG    mode;
  APIRET   rc;
  LONGLONG actual;

  switch( origin ) {
    case XIO_SEEK_SET: mode = FILE_BEGIN;   break;
    case XIO_SEEK_CUR: mode = FILE_CURRENT; break;
    case XIO_SEEK_END: mode = FILE_END;     break;
    default:
      errno = FILEBASEERR + ERROR_INVALID_FUNCTION;
      return -1;
  }

  FILE_REQUEST_DISK(x);

  rc = DosSetFilePtrL( x->protocol->s_handle, offset, mode, &actual );

  // Here's a trick required for seeking on large FAT32 files.
  if(( rc == ERROR_NEGATIVE_SEEK ) &&
     ( origin == XIO_SEEK_SET ) &&
     ( offset > XIO_2GB_LIMIT ))
  {
    LONGLONG step_offset = XIO_2GB_LIMIT;
    ULONG    step_mode = FILE_BEGIN;

    DEBUGLOG(( "xio: DosSetFilePtrL return ERROR_NEGATIVE_SEEK, try seek-by-seek.\n" ));

    while( offset ) {
      rc = DosSetFilePtrL( x->protocol->s_handle, step_offset, step_mode, &actual );

      if( rc == NO_ERROR ) {
        offset -= step_offset;
        step_offset = offset > XIO_2GB_LIMIT ? XIO_2GB_LIMIT : offset;
        step_mode = FILE_CURRENT;
      } else {
        break;
      }
    }
  }

  FILE_RELEASE_DISK(x);

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return actual;
  }
}

/* Returns the size of the file. A return value of -1 indicates an
   error or an unknown size. */
static long long
file_size( XFILE* x )
{
  FILESTATUS3L fs;
  APIRET       rc;

  FILE_REQUEST_DISK(x);
  rc = DosQueryFileInfo( x->protocol->s_handle, FIL_STANDARDL, &fs, sizeof( fs ));
  FILE_RELEASE_DISK(x);

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return fs.cbFile;
  }
}

/* Lengthens or cuts off the file to the length specified by size.
   You must open the file in a mode that permits writing. Adds null
   characters when it lengthens the file. When cuts off the file, it
   erases all data from the end of the shortened file to the end
   of the original file. */
static int
file_truncate( XFILE* x, long long size )
{
  APIRET rc;

  FILE_REQUEST_DISK(x);
  rc = DosSetFileSizeL( x->protocol->s_handle, size );
  FILE_RELEASE_DISK(x);

  if( rc != NO_ERROR ) {
    errno = FILEBASEERR + rc;
    return -1;
  } else {
    return 0;
  }
}

/* Terminates the file protocol. */
static void
file_terminate( XPROTOCOL* xp )
{
  if( xp ) {
    if( xp->mtx_access ) {
      DosCloseMutexSem( xp->mtx_access );
    }
    if( xp->mtx_file ) {
      DosCloseMutexSem( xp->mtx_file );
    }
    free( xp );
  }
}

/* Cleanups the protocol. */
static void
file_clean( XFILE* x ) {
  file_terminate( x->protocol );
}

/* Initializes the file protocol. */
XPROTOCOL*
file_initialize( XFILE* x )
{
  XPROTOCOL* xp = calloc( 1, sizeof( XPROTOCOL ));

  if( xp ) {
    if( DosCreateMutexSem( NULL, &xp->mtx_access, 0, FALSE ) != NO_ERROR ||
        DosCreateMutexSem( NULL, &xp->mtx_file  , 0, FALSE ) != NO_ERROR )
    {
      file_terminate( xp );
      return NULL;
    }

    xp->supports =
      XS_CAN_READ   | XS_CAN_WRITE | XS_CAN_READWRITE |
      XS_CAN_CREATE | XS_CAN_SEEK  | XS_CAN_SEEK_FAST;

    xp->open   = file_open;
    xp->read   = file_read;
    xp->write  = file_write;
    xp->close  = file_close;
    xp->tell   = file_tell;
    xp->seek   = file_seek;
    xp->chsize = file_truncate;
    xp->size   = file_size;
    xp->clean  = file_clean;
  }

  return xp;
}

/* Maps the error number in errnum to an error message string. */
const char* file_strerror( int errnum )
{
  switch( errnum )
  {
    case ERROR_INVALID_FUNCTION         /*   1 */ : return "Incorrect function.";
    case ERROR_FILE_NOT_FOUND           /*   2 */ : return "File not found.";
    case ERROR_PATH_NOT_FOUND           /*   3 */ : return "Path not found.";
    case ERROR_TOO_MANY_OPEN_FILES      /*   4 */ : return "The system cannot open the file.";
    case ERROR_ACCESS_DENIED            /*   5 */ : return "Access is denied.";
    case ERROR_INVALID_HANDLE           /*   6 */ : return "Incorrect internal file identifier.";
    case ERROR_INVALID_ACCESS           /*  12 */ : return "The access code is invalid.";
    case ERROR_WRITE_PROTECT            /*  19 */ : return "The drive is currently write-protected.";
    case ERROR_NOT_DOS_DISK             /*  26 */ : return "The specified disk or diskette cannot be accessed.";
    case ERROR_WRITE_FAULT              /*  29 */ : return "The system cannot write to the specified device.";
    case ERROR_READ_FAULT               /*  30 */ : return "The system cannot read from the specified device.";
    case ERROR_SHARING_VIOLATION        /*  32 */ : return "The process cannot access the file because it is being used by another process.";
    case ERROR_LOCK_VIOLATION           /*  33 */ : return "The process cannot access the file because another process has locked a portion of the file.";
    case ERROR_SHARING_BUFFER_EXCEEDED  /*  36 */ : return "The system has detected an overflow in the sharing buffer.";
    case ERROR_CANNOT_MAKE              /*  82 */ : return "The directory or file cannot be created.";
    case ERROR_INVALID_PARAMETER        /*  87 */ : return "The parameter is incorrect.";
    case ERROR_DEVICE_IN_USE            /*  99 */ : return "The device is already in use by another application.";
    case ERROR_DRIVE_LOCKED             /* 108 */ : return "The disk is in use or locked by another process.";
    case ERROR_BROKEN_PIPE              /* 109 */ : return "The pipe has been ended.";
    case ERROR_OPEN_FAILED              /* 110 */ : return "The system cannot open the file specified.";
    case ERROR_BUFFER_OVERFLOW          /* 111 */ : return "The file name is too long.";
    case ERROR_DISK_FULL                /* 112 */ : return "There is not enough space on the disk.";
    case ERROR_PROTECTION_VIOLATION     /* 115 */ : return "General protection fault.";
    case ERROR_INVALID_LEVEL            /* 124 */ : return "The system call level is incorrect.";
    case ERROR_NEGATIVE_SEEK            /* 131 */ : return "Attempting seek to negative offset.";
    case ERROR_SEEK_ON_DEVICE           /* 132 */ : return "Application trying to seek on device or pipe.";
    case ERROR_DISCARDED                /* 157 */ : return "Segment is discarded.";
    case ERROR_PIPE_BUSY                /* 231 */ : return "Pipe is busy.";
    case ERROR_NO_DATA                  /* 232 */ : return "No data available on non-blocking read.";
    case ERROR_MORE_DATA                /* 234 */ : return "More data is available.";

    default:
      return "Unexpected file error.";
  }
}
