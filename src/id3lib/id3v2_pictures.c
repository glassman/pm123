/*
 * Copyright (C) 2014-2017 Dmitry Steklenev
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <config.h>
#include <string.h>

#include "id3v2.h"
#include "id3v2_header.h"

/* Is the specified frame an attached picture frame. */
static int
id3v2_is_picture_frame( ID3V2_FRAME* frame )
{
  if( frame && ( frame->fr_desc->fd_id == ID3V2_APIC )) {
    return 1;
  } else {
    return 0;
  }
}

/* Return picture type. */
char
id3v2_picture_type( ID3V2_FRAME* frame )
{
  // Do we even have data for this frame.
  if( !frame->fr_data ) {
    return 0;
  }

  // Type check.
  if( !id3v2_is_picture_frame( frame )) {
    return 0;
  }

  // Check if frame is compressed.
  if( id3v2_decompress_frame( frame ) == -1 ) {
    return 0;
  }

  // Text encoding  $xx
  // MIME type      <text string> $00
  // Picture type   $xx
  // Description    <text string according to encoding> $00 (00)
  // Picture data   <binary data>

  return *( ID3V2_TEXT_FRAME_PTR( frame ) +
            strlen( ID3V2_TEXT_FRAME_PTR( frame )) + 1 );
}

/* Get picture MIME type. */
char*
id3v2_get_mime_type( ID3V2_FRAME* frame, char* result, int size )
{
  // Do we even have data for this frame.
  if( !frame->fr_data ) {
    return NULL;
  }

  // Type check.
  if( !id3v2_is_picture_frame( frame )) {
    return NULL;
  }

  // Check if frame is compressed.
  if( id3v2_decompress_frame( frame ) == -1 ) {
    return NULL;
  }

  // Text encoding  $xx
  // MIME type      <text string> $00
  // Picture type   $xx
  // Description    <text string according to encoding> $00 (00)
  // Picture data   <binary data>

  strlcpy( result, ID3V2_TEXT_FRAME_PTR( frame ), size );
  return result;
}


/* Return picture type. */
int
id3v2_picture_size( ID3V2_FRAME* frame )
{
  int mime_size;
  int desc_size;

  // Do we even have data for this frame.
  if( !frame->fr_data ) {
    return 0;
  }

  // Type check.
  if( !id3v2_is_picture_frame( frame )) {
    return 0;
  }

  // Check if frame is compressed.
  if( id3v2_decompress_frame( frame ) == -1 ) {
    return 0;
  }

  // Text encoding  $xx
  // MIME type      <text string> $00
  // Picture type   $xx
  // Description    <text string according to encoding> $00 (00)
  // Picture data   <binary data>

  mime_size = strlen( ID3V2_TEXT_FRAME_PTR( frame )) + 1;
  desc_size = id3v2_string_size( ID3V2_TEXT_FRAME_ENCODING( frame ),
                                 ID3V2_TEXT_FRAME_PTR( frame ) + mime_size + 1 );

  return frame->fr_size - 1 - mime_size - 1 - desc_size;
}

/* Return pointer to picture data. */
void*
id3v2_picture_data_ptr( ID3V2_FRAME* frame )
{
  // Do we even have data for this frame.
  if( !frame->fr_data ) {
    return 0;
  }

  // Type check.
  if( !id3v2_is_picture_frame( frame )) {
    return 0;
  }

  // Check if frame is compressed.
  if( id3v2_decompress_frame( frame ) == -1 ) {
    return 0;
  }

  return (char*)frame->fr_data +
         frame->fr_size - id3v2_picture_size( frame );
}


/* Set attached picture. Return 0 upon
   success, or -1 if an error occured. */
int
id3v2_set_picture( ID3V2_FRAME* frame, char type, char* mime, char* desc, void* data, int size )
{
  char*  encoded  = NULL;
  int8_t encoding = id3v2_get_save_encoding();
  int    rc;

  // Type check.
  if( !id3v2_is_picture_frame( frame )) {
    return -1;
  }

  // Release memory occupied by previous data.
  id3v2_clean_frame( frame );

  if(( encoded = id3v2_string_encode( encoding, desc )) != NULL )
  {
    int mime_size = strlen( mime ) + 1;
    int desc_size = id3v2_string_size( encoding, encoded );

    frame->fr_raw_size = 1 + mime_size + 1 + desc_size + size;
    frame->fr_raw_data = calloc( 1, frame->fr_raw_size );

    // Copy contents:
    // Text encoding  $xx
    // MIME type      <text string> $00
    // Picture type   $xx
    // Description    <text string according to encoding> $00 (00)
    // Picture data   <binary data>

    *((int8_t*)frame->fr_raw_data ) = encoding;
    strcpy((char*)frame->fr_raw_data + 1, mime );
    *((int8_t*)frame->fr_raw_data + 1 + mime_size ) = type;
    memcpy((char*)frame->fr_raw_data + 2 + mime_size, encoded, desc_size );
    memcpy((char*)frame->fr_raw_data + 2 + mime_size + desc_size,  data, size );

    frame->fr_altered = 1;
    frame->fr_data    = frame->fr_raw_data;
    frame->fr_size    = frame->fr_raw_size;

    frame->fr_owner->id3_altered = 1;
    rc =  0;
  } else {
    rc = -1;
  }

  free( encoded );
  return rc;
}

