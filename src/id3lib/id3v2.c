/*
 * Copyright (C) 1998, 1999, 2002 Espen Skoglund
 * Copyright (C) 2000-2004 Haavard Kvaalen
 * Copyright (C) 2007-2021 Dmitry Steklenev
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <io.h>

#include <charset.h>
#include <debuglog.h>
#include <minmax.h>

#include "id3v2.h"
#include "id3v2_header.h"

static int8_t id3v2_save_encoding = ID3V2_ENCODING_UTF16;
static int    id3v2_read_charset  = CH_DEFAULT;
static int    id3v2_save_charset  = CH_DEFAULT;

/* Sets the writing charcters set for plain text frames of the ID3 tag. */
void
id3v2_set_save_charset( int charset )
{
  id3v2_save_charset = charset;

  switch( charset ) {
    case CH_UTF_8:
      id3v2_save_encoding = ID3V2_ENCODING_UTF8;
      break;
    case CH_UCS_2BE:
      id3v2_save_encoding = ID3V2_ENCODING_UTF16;
      break;
    case CH_UCS_2BOM:
      id3v2_save_encoding = ID3V2_ENCODING_UTF16_BOM;
      break;
    default:
      id3v2_save_encoding = ID3V2_ENCODING_ISO_8859_1;
      break;
  }
}

/* Gets the writing charcters set for plain text frames of the ID3 tag. */
int
id3v2_get_save_charset( void ) {
  return id3v2_save_charset;
}

/* Gets the writing encoding of the ID3 tag. */
int8_t
id3v2_get_save_encoding( void ) {
  return id3v2_save_encoding;
}

/* Sets the reading characters set for plain text frames of the ID3 tag. */
void
id3v2_set_read_charset( int charset ) {
  id3v2_read_charset = charset;
}

/* Gets the reading characters set for plain text frames of the ID3 tag. */
int
id3v2_get_read_charset( void ) {
  return id3v2_read_charset;
}

/* Seek `offset' bytes forward in the indicated ID3-tag. Return 0
   upon success, or -1 if an error occured. */
static int
id3v2_seek( ID3V2_TAG* id3, int offset )
{
  // Check boundary.
  if(( id3->id3_pos + offset > id3->id3_totalsize ) ||
     ( id3->id3_pos + offset < 0 )) {
    return -1;
  }

  if( offset >= 0 )
  {
    // If offset is positive, we use fread() instead of fseek(). This
    // is more robust with respect to streams.

    char buf[64];
    int  r, remain = offset;

    while( remain > 0 )
    {
      int size = min( 64, remain );
      r = xio_fread( buf, 1, size, id3->id3_file );
      if( r == 0 ) {
        id3->id3_error_msg = "Read failed.";
        return -1;
      }
      remain -= r;
    }
  }
  else
  {
    // If offset is negative, we have to use fseek(). Let us hope
    // that it works.

    if( xio_fseek( id3->id3_file, offset, XIO_SEEK_CUR ) == -1 ) {
      id3->id3_error_msg = "Seeking beyond tag boundary.";
      return -1;
    }
  }
  id3->id3_pos += offset;
  return 0;
}

/* Read `size' bytes from indicated ID3-tag. If `buf' is non-NULL,
   read into that buffer. Return a pointer to the data which was
   read, or NULL upon error. */
static void*
id3v2_read( ID3V2_TAG* id3, void* buf, int size )
{
  int ret;

  // Check boundary.
  if( id3->id3_pos + size > id3->id3_totalsize ) {
    size = id3->id3_totalsize - id3->id3_pos;
  }

  // If buffer is NULL, we use the default buffer.
  if( buf == NULL ) {
    if( size > ID3V2_FILE_BUFSIZE ) {
      return NULL;
    }
    buf = id3->id3_filedata;
  }

  // Try reading from file.
  ret = xio_fread( buf, 1, size, id3->id3_file );
  if( ret != size ) {
    id3->id3_error_msg = "Read failed.";
    return NULL;
  }

  id3->id3_pos += ret;
  return buf;
}

/* Initialize an empty ID3 tag. */
static void
id3v2_init_tag( ID3V2_TAG* id3 )
{
  // Initialize header.
  id3->id3_version   = ID3V2_VERSION;
  id3->id3_revision  = ID3V2_REVISION;
  id3->id3_flags     = ID3V2_THFLAG_USYNC | ID3V2_THFLAG_EXP;
  id3->id3_size      = 0;
  id3->id3_totalsize = 0;
  id3->id3_altered   = 1;
  id3->id3_newtag    = 1;
  id3->id3_pos       = 0;
  id3->id3_started   = 0;

  // Initialize frames.
  id3->id3_frames = NULL;
  id3->id3_frames_count = 0;
}

/* Creates a new ID3 tag structure. Useful for creating
   a new tag. */
ID3V2_TAG*
id3v2_new_tag( void )
{
  ID3V2_TAG* id3;

  // Allocate ID3 structure. */
  id3 = calloc( 1, sizeof( ID3V2_TAG ));

  if( id3 != NULL ) {
    id3v2_init_tag( id3 );
  }

  return id3;
}

/* Read the ID3 tag from the input stream. The start of the tag
   must be positioned in the next tag in the stream.  Return 0 upon
   success, or -1 if an error occured. */
static int
id3v2_read_tag( ID3V2_TAG* id3 )
{
  char*   buf;
  uint8_t padding;

  // We know that the tag will be at least this big.
  id3->id3_totalsize = ID3V2_TAGHDR_SIZE + 3;

  if( !( id3->id3_oflags & ID3V2_GET_NOCHECK )) {
    // Check if we have a valid ID3 tag.
    char* id = id3->id3_read( id3, NULL, 3 );
    if( id == NULL ) {
      return -1;
    }

    if(( id[0] != 'I' ) || ( id[1] != 'D' ) || ( id[2] != '3' )) {
      // ID3 tag was not detected.
      id3->id3_seek( id3, -3 );
      return -1;
    }
  }

  // Read ID3 tag-header.
  buf = id3->id3_read( id3, NULL, ID3V2_TAGHDR_SIZE );
  if( buf == NULL ) {
    return -1;
  }

  id3->id3_version    = buf[0];
  id3->id3_revision   = buf[1];
  id3->id3_flags      = buf[2];
  id3->id3_size       = ID3_GET_SIZE28( buf[3], buf[4], buf[5], buf[6] );
  id3->id3_totalsize += id3->id3_size;
  id3->id3_newtag     = 0;

  if( id3->id3_flags & ID3V2_THFLAG_FOOTER ) {
    id3->id3_totalsize += 10;
  }

  DEBUGLOG(( "id3v2: found ID3V2 tag version 2.%d.%d\n",
                     id3->id3_version, id3->id3_revision ));

  if(( id3->id3_version < 2 ) || ( id3->id3_version > 4 )) {
    DEBUGLOG(( "id3v2: this version of the ID3V2 tag is unsupported.\n" ));
    return -1;
  }

  // Parse extended header.
  if( id3->id3_flags & ID3V2_THFLAG_EXT ) {
    buf = id3->id3_read( id3, NULL, ID3V2_EXTHDR_SIZE );
    if( buf == NULL ) {
      return -1;
    }
  }

  // Parse frames.
  while( id3->id3_pos < id3->id3_size ) {
    if( id3v2_read_frame( id3 ) == -1 ) {
      return -1;
    }
  }

  // Like id3lib, we try to find unstandard padding (not within
  // the tag size). This is important to know when we strip
  // the tag or replace it. Another option might be looking for
  // an MPEG sync, but we don't do it.

  id3->id3_seek( id3, id3->id3_totalsize - id3->id3_pos );

  // Temporarily increase totalsize, to try reading beyong the boundary.
  ++id3->id3_totalsize;

  while( id3->id3_read( id3, &padding, sizeof( padding )) != NULL )
  {
    if( padding == 0 ) {
      ++id3->id3_totalsize;
    } else {
//    id3->id3_seek( id3, -1 );
      xio_ungetc( padding, id3->id3_file );
      break;
    }
  }

  // Decrease totalsize after we temporarily increased it.
  --id3->id3_totalsize;

  return 0;
}

/* Read an ID3 tag using a file pointer. Return a pointer to a
   structure describing the ID3 tag, or NULL if an error occured. */
ID3V2_TAG*
id3v2_get_tag( XFILE* file, int flags )
{
  ID3V2_TAG* id3;

  // Allocate ID3 structure.
  id3 = calloc( 1, sizeof( ID3V2_TAG ));

  // Initialize access pointers.
  id3->id3_seek    = id3v2_seek;
  id3->id3_read    = id3v2_read;
  id3->id3_oflags  = flags;
  id3->id3_pos     = 0;
  id3->id3_file    = file;
  id3->id3_started = xio_ftell64( file );

  // Allocate buffer to hold read data.
  id3->id3_filedata = malloc( ID3V2_FILE_BUFSIZE );

  // Try reading ID3 tag.
  if( id3v2_read_tag( id3 ) == -1 ) {
    if( ~flags & ID3V2_GET_CREATE )
    {
      free( id3->id3_filedata );
      free( id3 );
      return NULL;
    }
    id3v2_init_tag( id3 );
  }

  return id3;
}

/* Free all resources associated with the ID3 tag. */
void
id3v2_free_tag( ID3V2_TAG* id3 )
{
  free( id3->id3_filedata );
  id3v2_delete_all_frames( id3 );
  free( id3 );
}

/* When altering a file, some ID3 tags should be discarded. As the ID3
   library has no means of knowing when a file has been altered
   outside of the library, this function must be called manually
   whenever the file is altered. */
int
id3v2_alter_tag( ID3V2_TAG* id3 )
{
  // List of frame classes that should be discarded whenever the
  // file is altered.

  static uint32_t discard_list[] = {
    ID3V2_ETCO, ID3V2_EQUA, ID3V2_MLLT, ID3V2_POSS, ID3V2_SYLT,
    ID3V2_SYTC, ID3V2_RVAD, ID3V2_TENC, ID3V2_TLEN, ID3V2_TSIZ,
    0
  };

  ID3V2_FRAME* fr;
  uint32_t id, i = 0;

  // Go through list of frame types that should be discarded.
  while(( id = discard_list[i++] ) != 0 )
  {
    // Discard all frames of that type.
    while(( fr = id3v2_get_frame( id3, id, 1 )) != NULL ) {
      id3v2_delete_frame( fr );
    }
  }

  return ID3V2_OK;
}

/* Remove the ID3 tag from the file using a file pointer.
   Takes care of resizing the file, if needed. Returns ID3V2_OK upon
   success, returns ID3V2_COPIED if the file data is saved to savename,
   ID3V2_ERROR or ID3V2_BADPOS if an error occured. */
int
id3v2_wipe_tag( XFILE* file, const char* savename )
{
  ID3V2_TAG* old_id3;
  int        old_totalsize;
  int64_t    old_started;
  int64_t    filesize;
  XFILE*     save;
  char*      buffer;
  int        bytes;
  int        rc = ID3V2_OK;

  // Figure out how large the current tag is.
  old_id3 = id3v2_get_tag( file, 0 );

  if( old_id3 ) {
    // We use max to make sure an erroneous tag doesn't confuse us.
    old_totalsize = max( old_id3->id3_totalsize, 0 );
    old_started   = old_id3->id3_started;
    id3v2_free_tag( old_id3 );
  } else {
    return ID3V2_OK;
  }

  filesize = xio_fsize64( file );
  if( old_started + old_totalsize == filesize ) {
    // If the ID3 tag is placed at end of the file, the file can be truncated simple.
    if( xio_ftruncate64( file, filesize - old_totalsize ) == 0 ) {
      return ID3V2_OK;
    } else {
      DEBUGLOG(( "id3v2: file truncation error when wiping tag\n" ));
      return ID3V2_ERROR;
    }
  }

  if(( buffer = calloc( 1, ID3V2_FILE_BUFSIZE )) == NULL ) {
    return ID3V2_ERROR;
  }

  if(( save = xio_fopen( savename, "wb" )) == NULL ) {
    free( buffer );
    return ID3V2_ERROR;
  }

  // And now, copy all the data to the new file.
  if( xio_fseek( file, old_totalsize, XIO_SEEK_SET ) == 0 ) {
    while(( bytes = xio_fread( buffer, 1, ID3V2_FILE_BUFSIZE, file )) > 0 ) {
      if( xio_fwrite( buffer, 1, bytes, save ) != bytes ) {
        break;
      }
    }
    rc = xio_ferror( save ) ? ID3V2_ERROR : ID3V2_COPIED;
  } else {
    rc = ID3V2_ERROR;
  }

  xio_fclose( save );
  free( buffer );
  return rc;
}

/* Write the ID3 tag to the indicated file descriptor. Return ID3V2_OK
   upon success, or ID3V2_ERROR if an error occured. */
static int
id3v2_write_tag( XFILE* file, ID3V2_TAG* id3 )
{
  int  i;
  char buf[ID3V2_TAGHDR_SIZE];

  ID3V2_FRAME* fr;

  // Write tag header.

  buf[0] = ID3V2_VERSION;
  buf[1] = ID3V2_REVISION;
  buf[2] = id3->id3_flags;

  ID3_SET_SIZE28( id3->id3_size, buf[3], buf[4], buf[5], buf[6] );

  if( xio_fwrite( "ID3", 1, 3, file ) != 3 ) {
    return ID3V2_ERROR;
  }
  if( xio_fwrite( buf, 1, ID3V2_TAGHDR_SIZE, file ) != ID3V2_TAGHDR_SIZE ) {
    return ID3V2_ERROR;
  }

  // TODO: Write extended header.

#if 0
  if( id3->id3_flags & ID3_THFLAG_EXT ) {
    id3_exthdr_t exthdr;
  }
#endif

  for( i = 0; i < id3->id3_frames_count; i++ )
  {
    char fhdr[ID3V2_FRAMEHDR_SIZE];

    fr = id3->id3_frames[i];

    // TODO: Support compressed headers, encoded
    // headers, and grouping info.

    fhdr[0] = fr->fr_desc->fd_idstr[0];
    fhdr[1] = fr->fr_desc->fd_idstr[1];
    fhdr[2] = fr->fr_desc->fd_idstr[2];
    fhdr[3] = fr->fr_desc->fd_idstr[3];

    #if ID3V2_VERSION < 4
      fhdr[4] = ( fr->fr_raw_size >> 24 ) & 0xFF;
      fhdr[5] = ( fr->fr_raw_size >> 16 ) & 0xFF;
      fhdr[6] = ( fr->fr_raw_size >> 8  ) & 0xFF;
      fhdr[7] = ( fr->fr_raw_size       ) & 0xFF;
    #else
      ID3_SET_SIZE28( fr->fr_raw_size, fhdr[4], fhdr[5], fhdr[6], fhdr[7] )
    #endif

    fhdr[8] = ( fr->fr_flags    >> 8  ) & 0xFF;
    fhdr[9] = ( fr->fr_flags          ) & 0xFF;

    if( xio_fwrite( fhdr, 1, sizeof( fhdr ), file ) != sizeof( fhdr )) {
      return ID3V2_ERROR;
    }

    if( xio_fwrite( fr->fr_raw_data, 1, fr->fr_raw_size, file ) != fr->fr_raw_size ) {
      return ID3V2_ERROR;
    }
  }
  return ID3V2_OK;
}

/* Writes the ID3 tag to the file. Returns ID3V2_OK upon success,
   returns ID3V2_COPIED if the ID3 tag is saved to savename or
   returns ID3V2_ERROR or ID3V2_BADPOS if an error occured. */
int
id3v2_set_tag( XFILE* file, ID3V2_TAG* id3, const char* savename )
{
  ID3V2_TAG* old_id3;
  int        old_totalsize;
  int        new_totalsize;
  int64_t    old_startdata;
  int64_t    old_started;
  int64_t    filesize;
  int        tag_at_end;
  int        i;
  XFILE*     save = NULL;
  char*      buffer;
  size_t     remaining;
  int        rc = ID3V2_OK;

  if(( buffer = calloc( 1, ID3V2_FILE_BUFSIZE )) == NULL ) {
    return ID3V2_ERROR;
  }

  // Figure out how large the current tag is.
  old_id3 = id3v2_get_tag( file, 0 );

  if( old_id3 ) {
    // We use max to make sure an erroneous tag doesn't confuse us.
    old_totalsize = max( old_id3->id3_totalsize, 0 );
    old_started   = old_id3->id3_started;
    old_startdata = old_started + old_totalsize;
    id3v2_free_tag( old_id3 );
  } else {
    old_totalsize = 0;
    old_started   = xio_ftell64( file );
    old_startdata = old_started;
  }

  // Try to determine position of the ID3 tag in the file.
  filesize = xio_fsize64( file );

  if( old_started + old_totalsize == filesize ) {
    DEBUGLOG(( "id3v2: old tag is placed at end of file.\n" ));
    tag_at_end = 1;
  } else {
    char data[4];

    DEBUGLOG(( "id3v2: old tag is placed at begin of file.\n" ));
    tag_at_end = 0;

    if( xio_fread( data, 1, 4, file ) == 4 ) {
      if( data[0] == 'R' && data[1] == 'I' && data[2] == 'F' && data[3] == 'F' ) {
        while( xio_fread( data + 3, 1, 1, file ) == 1 ) {
          if( data[0] == 'd' && data[1] == 'a' && data[2] == 't' && data[3] == 'a' ) {
            old_startdata = xio_ftell64( file );
            DEBUGLOG(( "id3v2: skipped RIFF header up to %lld.\n", old_startdata ));
            break;
          }
          data[0] = data[1];
          data[1] = data[2];
          data[2] = data[3];
        }
      }
    }
  }

  // Figure out how large the new tag will be.
  new_totalsize = ID3V2_TAGHDR_SIZE + 3;

  for( i = 0; i < id3->id3_frames_count; i++ ) {
    new_totalsize += id3->id3_frames[i]->fr_raw_size + ID3V2_FRAMEHDR_SIZE;
  }

  new_totalsize += 0;   // no extended header, no footer, no padding.
  id3->id3_flags = 0;

  DEBUGLOG(( "id3v2: new tag size is %d, old size is %d\n",
                     new_totalsize, old_totalsize ));

  // Determine whether we need to rewrite the file to make place for the new tag.
  if( !tag_at_end ) {
    if(( new_totalsize > old_totalsize ) || ( old_totalsize - new_totalsize > 4096 )) {
      if( savename )
      {
        DEBUGLOG(( "id3v2: save tag to the new copy of file %s\n", savename ));

        if(( save = xio_fopen( savename, "wb" )) == NULL ) {
          free( buffer );
          return ID3V2_ERROR;
        }
        // Use padding.
        new_totalsize += 1024;
        old_started = 0;
      } else {
        DEBUGLOG(( "id3v2: the tag can not be updated, file must be copied\n" ));
        return ID3V2_BADPOS;
      }
    } else {
      new_totalsize = old_totalsize;
      save = file;
    }
  } else {
    save = file;
  }

  // Zero-out the ID3v2 tag area.
  xio_fseek64( save, old_started, XIO_SEEK_SET );
  for( remaining = new_totalsize; remaining > 0; remaining -= min( remaining, ID3V2_FILE_BUFSIZE )) {
    xio_fwrite( buffer, 1, min( remaining, ID3V2_FILE_BUFSIZE ), save );
  }

  // Write the new tag.
  id3->id3_size = new_totalsize - ID3V2_TAGHDR_SIZE - 3;
  id3->id3_totalsize = new_totalsize;
  xio_fseek64( save, old_started, XIO_SEEK_SET );

  if( id3v2_write_tag( save, id3 ) == ID3V2_ERROR ) {
    DEBUGLOG(( "id3v2: error at write tag data\n" ));
    rc = ID3V2_ERROR;
  }

  if( rc == ID3V2_OK ) {
    if( save != file )
    {
      int bytes;

      DEBUGLOG(( "id3v2: copy data from %lld to %d\n", old_startdata, new_totalsize ));

      // And now, copy all the data to the new file.
      if( xio_fseek64( file, old_startdata, XIO_SEEK_SET ) == 0 ) {
        if( xio_fseek64( save, new_totalsize, XIO_SEEK_SET ) == 0 ) {
          while(( bytes = xio_fread( buffer, 1, ID3V2_FILE_BUFSIZE, file )) > 0 ) {
            if( xio_fwrite( buffer, 1, bytes, save ) != bytes ) {
              break;
            }
          }
        }
        rc = xio_ferror( save ) ? ID3V2_ERROR : ID3V2_COPIED;
      } else {
        rc = ID3V2_ERROR;
      }
    } else if( tag_at_end ) {
      DEBUGLOG(( "id3v2: truncate file to %lld\n", xio_ftell64( file )));
      xio_ftruncate64( file, xio_ftell64( file ));
    }
  }

  if( save != file ) {
    xio_fclose( save );
  }

  free( buffer );
  return rc;
}

