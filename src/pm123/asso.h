/*
 * Copyright 2009-2021 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_ASSO_H
#define PM123_ASSO_H

#define ID_TYPE_AUDIO 100
#define ID_TYPE_AIF   101
#define ID_TYPE_AU    102
#define ID_TYPE_AVR   103
#define ID_TYPE_CAF   104
#define ID_TYPE_IFF   105
#define ID_TYPE_LST   106
#define ID_TYPE_M3U   107
#define ID_TYPE_M3U8  108
#define ID_TYPE_MAT   109
#define ID_TYPE_MP1   110
#define ID_TYPE_MP2   111
#define ID_TYPE_MP3   112
#define ID_TYPE_MPL   113
#define ID_TYPE_OGG   114
#define ID_TYPE_PAF   115
#define ID_TYPE_PLS   116
#define ID_TYPE_PVF   117
#define ID_TYPE_SD2   118
#define ID_TYPE_SDS   119
#define ID_TYPE_SF    120
#define ID_TYPE_VOC   121
#define ID_TYPE_W64   122
#define ID_TYPE_WAV   123
#define ID_TYPE_XI    124
#define ID_TYPE_OGA   125
#define ID_TYPE_FLAC  126
#define ID_TYPE_APE   127
#define ID_TYPE_MPC   128
#define ID_TYPE_MPLUS 129
#define ID_TYPE_TTA   130
#define ID_TYPE_WV    131
#define ID_TYPE_CUE   132
#define ID_TYPE_DSF   133
#define ID_TYPE_DFF   134
#define ID_TYPE_AAC   135
#define ID_TYPE_MP4   136
#define ID_TYPE_M4A   137
#define ID_TYPE_M4B   138

typedef struct _ASSOFILE
{
  char      ext [16];
  char      type[64];
  int       resid;
  HPOINTER  hicon;

} ASSOFILE;

typedef struct _ASSOLIST
{
  ULONG size;
  PCHAR ps;

} ASSOLIST;

typedef struct _ASSORECORD
{
  RECORDCORE rc;
  ASSOFILE*  file;

} ASSORECORD;

#ifdef __cplusplus
extern "C" {
#endif

/* Initializes association container.
 */

BOOL asso_init( HWND hcontainer );

/* Terminates association container.
 */

BOOL asso_term( HWND hcontainer );

/* Associates/desssociates program with specified file.
 */

BOOL asso_create( ASSOFILE* file );
BOOL asso_remove( ASSOFILE* file );
BOOL asso_create_all( void );
BOOL asso_remove_all( void );

/* Returns TRUE if the specified extension is
 * associated with PM123.
 */

BOOL asso_is_registered( const char* ext );

/* Returns TRUE if PM123 can be integrated to folder context menu. */
BOOL asso_can_integrated( void );
/* Returns TRUE if PM123 is integrated to folder context menu. */
BOOL asso_is_integrated( void );
/* Integrates PM123 to folder context menu. */
BOOL asso_integrate( BOOL state );

#ifdef __cplusplus
}
#endif
#endif /* PM123_ASSO_H */

