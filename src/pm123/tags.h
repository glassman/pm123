/*
 * Copyright 2007-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_TAGS_H
#define PM123_TAGS_H

#define DLG_FILEINFO     2022
#define NB_FILEINFO      FID_CLIENT

#define DLG_SONGINFO     2012
#define ST_TAG_TITLE      101
#define CB_TAG_TITLE      102
#define ST_TAG_ARTIST     104
#define CB_TAG_ARTIST     105
#define ST_TAG_ALBUM      107
#define CB_TAG_ALBUM      108
#define ST_TAG_COMMENT    110
#define CB_TAG_COMMENT    111
#define ST_TAG_COPYRIGHT  113
#define CB_TAG_COPYRIGHT  114
#define ST_TAG_GENRE      116
#define CB_TAG_GENRE      117
#define ST_TAG_TRACK      119
#define CB_TAG_TRACK      120
#define ST_TAG_YEAR       122
#define CB_TAG_YEAR       123
#define ST_TAG_DISC       124
#define CB_TAG_DISC       125
#define PC_COVER          126
#define PB_PIC_NEXT       127
#define PB_PIC_PREV       128
#define ST_PIC_INFO       129
#define EN_PIC_INFO       130
#define ST_PIC_TYPE       131
#define CB_PIC_TYPE       132
#define ST_PIC_DESC       133
#define EN_PIC_DESC       134
#define PB_PIC_ADD        135
#define PB_PIC_CLEAR      136
#define PB_PIC_EXTRACT    137
#define ST_PIC_SELECTED   138

#define DLG_TECHINFO     2013
#define ST_FILENAME       101
#define EN_FILENAME       102
#define ST_DECODER        103
#define EN_DECODER        104
#define ST_INFO           105
#define EN_INFO           106
#define ST_MPEGINFO       107
#define EN_MPEGINFO       108
#define ST_SONGLENGTH     109
#define EN_SONGLENGTH     110
#define ST_FILESIZE       111
#define EN_FILESIZE       112
#define ST_STARTED        113
#define EN_STARTED        114
#define ST_ENDED          115
#define EN_ENDED          116
#define ST_TRACK_GAIN     117
#define EN_TRACK_GAIN     118
#define ST_TRACK_PEAK     119
#define EN_TRACK_PEAK     120
#define ST_ALBUM_GAIN     121
#define EN_ALBUM_GAIN     122
#define ST_ALBUM_PEAK     123
#define EN_ALBUM_PEAK     124

/* Edits a meta information of the specified file.
   Must be called from the main thread. */
void tag_edit( HWND owner, AMP_FILE* tags, int count );
/* Reads the picture. */
APIC* tag_read_picture( const char* filename );

/* Initializes of the tagging module. */
void tag_init( void );
/* Terminates  of the tagging module. */
void tag_term( void );

#ifdef __cplusplus
}
#endif
#endif /* PM123_TAGS_H */

