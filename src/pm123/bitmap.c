/*
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_PM
#define  INCL_BASE
#include <os2.h>

#include <gbm.h>
#include <gbmscale.h>
#include <debuglog.h>

#include <fcntl.h>
#include <io.h>
#include <errno.h>
#include <sys/stat.h>

#include "bitmap.h"
#include "pm123.h"
#include "assertions.h"

// Now trick GBM and use own versions of open, create, close, lseek, read and
// write routines, that allow parsing bitmap already loaded into memory.

typedef struct _BMPSTORAGE
{
  int   fd;
  void* memptr;
  int   memsize;
  int   mempos;

} BMPSTORAGE, *PBMPSTORAGE;

static int
io_get_checked_internal_open_mode( int mode )
{
  int internal_mode = O_BINARY;

  if( mode & GBM_O_RDONLY ) {
    internal_mode |= O_RDONLY;
  } else if( mode & GBM_O_WRONLY ) {
    internal_mode |= O_WRONLY;
  } else if( mode & GBM_O_RDWR ) {
    internal_mode |= O_RDWR;
  } else {
    return -1;
  }

  if( mode & GBM_O_EXCL ) {
    internal_mode |= O_EXCL;
  }

  return internal_mode;
}

static int GBMENTRY
io_open( const char* fn, int mode )
{
  int internal_mode = io_get_checked_internal_open_mode( mode );
  int fd;

  if(( fd = open( fn, internal_mode )) != -1 )
  {
    PBMPSTORAGE ps = calloc( sizeof( BMPSTORAGE ), 1 );

    if( ps ) {
      ps->fd = fd;
      return (int)ps;
    }
  }

  return -1;
}

static int GBMENTRY
io_create( const char* fn, int mode )
{
  int internal_mode = io_get_checked_internal_open_mode( mode );
  int fd;

  if(( fd = open( fn, O_CREAT | O_TRUNC | internal_mode, S_IREAD | S_IWRITE )) != -1 )
  {
    PBMPSTORAGE ps = calloc( sizeof( BMPSTORAGE ), 1 );

    if( ps ) {
      ps->fd = fd;
      return (int)ps;
    }
  }

  return -1;
}

static int GBMENTRY
io_access( void* memptr, int size )
{
  PBMPSTORAGE ps = calloc( sizeof( BMPSTORAGE ), 1 );

  if( ps ) {
    ps->memptr  = memptr;
    ps->memsize = size;
    return (int)ps;
  }

  return -1;
}

static void GBMENTRY
io_close( int fd )
{
  PBMPSTORAGE ps = (PBMPSTORAGE)fd;

  if( ps->fd ) {
    close( ps->fd );
  }

  free( ps );
}

static long GBMENTRY
io_lseek( int fd, long pos, int whence )
{
  PBMPSTORAGE ps = (PBMPSTORAGE)fd;

  if( ps->fd )
  {
    int internal_whence = -1;

    switch( whence ) {
      case GBM_SEEK_SET:
        internal_whence = SEEK_SET;
        break;

      case GBM_SEEK_CUR:
        internal_whence = SEEK_CUR;
        break;

      case GBM_SEEK_END:
        internal_whence = SEEK_END;
        break;
    }

    return lseek( ps->fd, pos, internal_whence );
  }
  else
  {
    int internal_pos = ps->mempos;

    switch( whence ) {
      case GBM_SEEK_SET:
        internal_pos = pos;
        break;

      case GBM_SEEK_CUR:
        internal_pos += pos;
        break;

      case GBM_SEEK_END:
        internal_pos = ps->memsize - pos;
        break;
    }

    if( internal_pos >= 0 && internal_pos <= ps->memsize ) {
      ps->mempos = internal_pos;
      return internal_pos;
    }

    return -1;
  }
}

static int GBMENTRY
io_read( int fd, void* buf, int len )
{
  PBMPSTORAGE ps = (PBMPSTORAGE)fd;

  if( ps->fd ) {
    return read( ps->fd, buf, len );
  } else {
    if( ps->memsize - ps->mempos < len ) {
      len = ps->memsize - ps->mempos;
    }

    memcpy( buf, (char*)ps->memptr + ps->mempos, len );
    ps->mempos += len;
    return len;
  }
}

static int GBMENTRY
io_write( int fd, const void *buf, int len )
{
  PBMPSTORAGE ps = (PBMPSTORAGE)fd;

  if( ps->fd ) {
    return write( ps->fd, buf, len );
  } else {
    errno = EBADF;
    return -1;
  }
}

/* Reads and parse a bitmap file. */
static BOOL
read_bitmap( const char* filename, int fd, GBM* gbm, GBMRGB* gbmrgb, gbm_u8** ppbData )
{
  int   filetype;
  int   cb;
  char* opt = "";

  if( gbm_guess_filetype( filename, &filetype ) != GBM_ERR_OK ) {
    amp_show_error( "Unable deduce bitmap format from file extension:\n%s\n", filename );
    return FALSE;
  }

  if( gbm_read_header( filename, fd, filetype, gbm, opt ) != GBM_ERR_OK ) {
    amp_show_error( "Unable read bitmap file header:\n%s\n", filename );
    return FALSE;
  }

  if( gbm_read_palette( fd, filetype, gbm, gbmrgb ) != GBM_ERR_OK ) {
    amp_show_error( "Unable read bitmap file palette:\n%s\n", filename );
    return FALSE;
  }

  cb = (( gbm->w * gbm->bpp + 31 ) / 32 ) * 4 * gbm->h;

  if(( *ppbData = malloc( cb )) == NULL ) {
    amp_show_error( "Out of memory" );
    return FALSE;
  }

  if( gbm_read_data( fd, filetype, gbm, *ppbData ) != GBM_ERR_OK )
  {
    free( *ppbData );
    amp_show_error( "Unable read bitmap file data:\n%s\n", filename );
    return FALSE;
  }

  return TRUE;
}

/* Creates a native OS/2 bitmap from specified data. */
static BOOL
make_bitmap( HWND hwnd, GBM* gbm, GBMRGB* gbmrgb, gbm_u8* pbData, HBITMAP* phbm )
{
  HAB hab = WinQueryAnchorBlock( hwnd );

  USHORT cRGB, usCol;
  SIZEL  sizl;
  HDC    hdc;
  HPS    hps;

  struct {
    BITMAPINFOHEADER2 bmp2;
    RGB2 argb2Color[0x100];
  } bm;

  // Got the data in memory, now make bitmap.
  memset( &bm, 0, sizeof( bm ));

  bm.bmp2.cbFix     = sizeof( BITMAPINFOHEADER2 );
  bm.bmp2.cx        = gbm->w;
  bm.bmp2.cy        = gbm->h;
  bm.bmp2.cBitCount = gbm->bpp;
  bm.bmp2.cPlanes   = 1;

  cRGB = (( 1 << gbm->bpp ) & 0x1FF );
  // 1 -> 2, 4 -> 16, 8 -> 256, 24 -> 0

  for( usCol = 0; usCol < cRGB; usCol++ )
  {
    bm.argb2Color[ usCol ].bRed   = gbmrgb[ usCol ].r;
    bm.argb2Color[ usCol ].bGreen = gbmrgb[ usCol ].g;
    bm.argb2Color[ usCol ].bBlue  = gbmrgb[ usCol ].b;
  }

  if(( hdc = DevOpenDC( hab, OD_MEMORY, "*", 0L, (PDEVOPENDATA)NULL, (HDC)NULL )) == (HDC)NULL )
  {
    return FALSE;
  }

  sizl.cx = bm.bmp2.cx;
  sizl.cy = bm.bmp2.cy;

  if(( hps = GpiCreatePS( hab, hdc, &sizl,
                          PU_PELS | GPIF_DEFAULT | GPIT_MICRO | GPIA_ASSOC )) == (HPS)NULL )
  {
    DevCloseDC( hdc );
    return FALSE;
  }

  if( cRGB == 2 )
  {
    static RGB2 argb2Black = { 0x00, 0x00, 0x00 };
    static RGB2 argb2White = { 0xff, 0xff, 0xff };

    bm.argb2Color[0] = argb2Black; /* Contrast */
    bm.argb2Color[1] = argb2White; /* Reset    */
  }

  if(( *phbm = GpiCreateBitmap( hps, &(bm.bmp2), CBM_INIT,
                               (BYTE*)pbData, (BITMAPINFO2*)&bm.bmp2 )) == (HBITMAP)NULL )
  {
    GpiDestroyPS( hps );
    DevCloseDC  ( hdc );
    return FALSE;
  }

  GpiSetBitmap( hps, (HBITMAP)NULL );
  GpiDestroyPS( hps );
  DevCloseDC  ( hdc );
  return TRUE;
}

static BOOL
is_grayscale_palette( const GBMRGB* gbmrgb, const int entries )
{
  if(( entries > 0 ) && ( entries <= 0x100 ))
  {
    int  i;
    for( i = 0; i < entries; i++ )
    {
      if(( gbmrgb[i].r != gbmrgb[i].g ) ||
         ( gbmrgb[i].r != gbmrgb[i].b ) ||
         ( gbmrgb[i].g != gbmrgb[i].b ))
      {
        return FALSE;
      }
    }
    return TRUE;
  }

  return FALSE;
}

/* Loads a bitmap from a file and returns the bitmap handle. */
HBITMAP
bmp_load_bitmap( const char* filename )
{
  GBM     gbm;
  GBMRGB  gbmrgb[0x100];
  gbm_u8* pbData;
  HBITMAP hbmBmp = NULLHANDLE;
  int     fd;

  fd = gbm_io_open( filename, GBM_O_RDONLY );

  if( fd == -1 ) {
    amp_show_error( "Unable open bitmap file:\n%s\n", filename );
  } else if( read_bitmap( filename, fd, &gbm, gbmrgb, &pbData )) {
    make_bitmap( HWND_DESKTOP, &gbm, gbmrgb, pbData, &hbmBmp );
    gbm_io_close( fd );
    free( pbData );
  }

  return hbmBmp;
}

/* Makes a bitmap from a file loaded to memory and returns the bitmap handle. */
HBITMAP
bmp_make_bitmap( const char* filename, void* memptr, int size )
{
  GBM     gbm;
  GBMRGB  gbmrgb[0x100];
  gbm_u8* pbData;
  HBITMAP hbmBmp = NULLHANDLE;
  int     fd;

  fd = io_access( memptr, size );

  if( fd == -1 ) {
    amp_show_error( "Out of memory" );
  } else if( read_bitmap( filename, fd, &gbm, gbmrgb, &pbData )) {
    make_bitmap( HWND_DESKTOP, &gbm, gbmrgb, pbData, &hbmBmp );
    gbm_io_close( fd );
    free( pbData );
  }

  return hbmBmp;
}

/* Makes a bitmap from a data loaded to memory, scale it to specified size
   and returns the bitmap handle. */
HBITMAP
bmp_make_scaled_bitmap( const char* mimetype, void* memptr, int size, int w, int h )
{
  GBM     gbm1;
  gbm_u8* pbData1 = NULL;
  GBM     gbm2;
  gbm_u8* pbData2 = NULL;
  GBMRGB  gbmrgb[0x100];
  HBITMAP hbmBmp = NULLHANDLE;
  int     fd;
  char    type[64];
  char*   p;
  BOOL    is_grayscale = FALSE;
  int     cb;
  GBM_ERR rc;

  fd = io_access( memptr, size );

  if( fd == -1 ) {
    amp_show_error( "Out of memory" );
    return NULLHANDLE;
  }

  strlcpy( type, mimetype, sizeof( type ));
  if(( p = strpbrk( type, "/")) != NULL ) {
    *p = '.';
  }
  if( !read_bitmap( type, fd, &gbm1, gbmrgb, &pbData1 )) {
    goto exit;
  }

  // Check for color depth supported by algorithms.
  switch( gbm1.bpp )
  {
    case 64:
    case 48:
    case 32:
    case 24:
    case 8:
    case 4:
    case 1:
      break;

    default:
      DEBUGLOG(( "pm123: bmp_make_scaled_bitmap doesn't support %d bpp", gbm1.bpp ));
      goto exit;
  }

  if(((float)w/gbm1.w) <= ((float)h/gbm1.h)) {
    h = ( gbm1.h * w ) / gbm1.w;
  } else {
    w = ( gbm1.w * h ) / gbm1.h;
  }

  if( w == 0 || h == 0 ) {
    DEBUGLOG(( "pm123: bmp_make_scaled_bitmap doesn't support scale to %dx%d", w, h ));
    goto exit;
  }

  gbm2   = gbm1;
  gbm2.w = w;
  gbm2.h = h;

  if( gbm1.bpp <= 8 ) {
    is_grayscale = is_grayscale_palette( gbmrgb, 1 << gbm1.bpp );
  }
  if( gbm1.bpp <= 8 && !is_grayscale ) {
    DEBUGLOG(( "pm123: can't use filter 'GBM_SCALE_FILTER_BILINEAR' for colour palette images\n" ));
    goto exit;
  }
  if( is_grayscale ) {
    gbm2.bpp = 8;
  }

  cb = (( gbm2.w * gbm2.bpp + 31 ) / 32 ) * 4 * gbm2.h;

  if(( pbData2 = malloc( cb )) == NULL ) {
    amp_show_error( "Out of memory" );
    goto exit;
  }

  if( is_grayscale ) {
    rc = gbm_quality_scale_gray( pbData1, gbm1.w, gbm1.h, gbm1.bpp, gbmrgb,
                                 pbData2, gbm2.w, gbm2.h, gbmrgb, GBM_SCALE_FILTER_BILINEAR );
  } else {
    rc = gbm_quality_scale_bgra( pbData1, gbm1.w, gbm1.h,
                                 pbData2, gbm2.w, gbm2.h, gbm2.bpp, GBM_SCALE_FILTER_BILINEAR );
  }

  if( rc == GBM_ERR_OK ) {
    make_bitmap( HWND_DESKTOP, &gbm2, gbmrgb, pbData2, &hbmBmp );
  } else {
    DEBUGLOG(( "pm123: gbm_quality_scale return %d for %d bpp\n", rc, gbm1.bpp ));
  }

exit:

  free( pbData1 );
  free( pbData2 );
  gbm_io_close( fd );
  return hbmBmp;
}

/* Returns the size of a bitmap loaded into memory. */
BOOL
bmp_query_bitmap_size( const char* mimetype, void* memptr, int size, int* w, int* h )
{
  int     fd;
  char    type[64];
  char*   p;
  int     filetype;
  GBM     gbm;
  BOOL    rc;

  fd = io_access( memptr, size );

  if( fd == -1 ) {
    amp_show_error( "Out of memory" );
    return FALSE;
  }

  strlcpy( type, mimetype, sizeof( type ));
  if(( p = strpbrk( type, "/")) != NULL ) {
    *p = '.';
  }

  if( gbm_guess_filetype( type, &filetype ) == GBM_ERR_OK &&
      gbm_read_header( type, fd, filetype, &gbm, "" ) == GBM_ERR_OK )
  {
    *w = gbm.w;
    *h = gbm.h;
    rc = TRUE;
  } else {
    rc = FALSE;
  }

  gbm_io_close( fd );
  return rc;
}

/* Initializes the bitmap management. Must be called
   from the main thread. */
void
bmp_init( void )
{
  ASSERT_IS_MAIN_THREAD;
  gbm_init();
  gbm_io_setup( io_open, io_create, io_close, io_lseek, io_read, io_write );
}

/* Terminates the command management. Must be called
   from the main thread. */
void
bmp_term( void )
{
  ASSERT_IS_MAIN_THREAD;
  gbm_deinit();
}

