/*
 * Copyright 2018-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_UPNP_H
#define PM123_UPNP_H

#include <upnp.h>
#include <upnptools.h>

/* Flags used in UPNP_STATE_VARIABLE structure. */
#define UPNP_VAR_CHANGED    0x0001UL
#define UPNP_VAR_ALLOCATED  0x0002UL

/** Structure that defines one state variable. */
typedef struct _UPNP_STATE_VARIABLE
{
  char* name;
  char* strval;
  ULONG flags;

} UPNP_STATE_VARIABLE;

#define UPNP_RenderingControl              0
#define UPNP_ConnectionManager             1
#define UPNP_AVTransport                   2

#define UPNP_LastChange                    0
#define UPNP_A_ARG_TYPE_InstanceID         1
#define UPNP_A_ARG_TYPE_Channel            2
#define UPNP_A_ARG_TYPE_PresetName         3
#define UPNP_PresetNameList                4
#define UPNP_Volume                        5
#define UPNP_VolumeDB                      6
#define UPNP_Mute                          7

#define UPNP_LastChange                    0
#define UPNP_A_ARG_TYPE_InstanceID         1
#define UPNP_A_ARG_TYPE_SeekMode           2
#define UPNP_A_ARG_TYPE_SeekTarget         3
#define UPNP_AbsoluteCounterPosition       4
#define UPNP_AbsoluteTimePosition          5
#define UPNP_AVTransportURI                6
#define UPNP_AVTransportURIMetaData        7
#define UPNP_CurrentMediaCategory          8
#define UPNP_CurrentMediaDuration          9
#define UPNP_CurrentPlayMode              10
#define UPNP_CurrentRecordQualityMode     11
#define UPNP_CurrentTrack                 12
#define UPNP_CurrentTrackDuration         13
#define UPNP_CurrentTrackMetaData         14
#define UPNP_CurrentTrackURI              15
#define UPNP_CurrentTransportActions      16
#define UPNP_NextAVTransportURI           17
#define UPNP_NextAVTransportURIMetaData   18
#define UPNP_NumberOfTracks               19
#define UPNP_PlaybackStorageMedium        20
#define UPNP_PossiblePlaybackStorageMedia 21
#define UPNP_PossibleRecordQualityModes   22
#define UPNP_PossibleRecordStorageMedia   23
#define UPNP_RecordMediumWriteStatus      24
#define UPNP_RecordStorageMedium          25
#define UPNP_RelativeCounterPosition      26
#define UPNP_RelativeTimePosition         27
#define UPNP_TransportPlaySpeed           28
#define UPNP_TransportState               29
#define UPNP_TransportStatus              30

#define UPNP_A_ARG_TYPE_AVTransportID      0
#define UPNP_A_ARG_TYPE_ConnectionID       1
#define UPNP_A_ARG_TYPE_ConnectionManager  2
#define UPNP_A_ARG_TYPE_ConnectionStatus   3
#define UPNP_A_ARG_TYPE_Direction          4
#define UPNP_A_ARG_TYPE_ProtocolInfo       5
#define UPNP_A_ARG_TYPE_RcsID              6
#define UPNP_CurrentConnectionIDs          7
#define UPNP_FeatureList                   8
#define UPNP_SinkProtocolInfo              9
#define UPNP_SourceProtocolInfo           10

#ifdef __cplusplus
extern "C" {
#endif

/* Allowed values for upnp_notify_state_change. */

#define UPNP_N_VOLUME         0x00000001UL  /* Volume changed. */
#define UPNP_N_PLAYERSTATE    0x00000002UL  /* Player state changed: play, stop, pause, fast forward, rewind. */
#define UPNP_N_TRACKS         0x00000004UL  /* Number of tracks is changed. */
#define UPNP_N_PLAYERMODE     0x00000008UL  /* Switch between playlist or single file mode, repeat, shuffle. */
#define UPNP_N_FILELOADED     0x00000010UL  /* Single file or playlist record is loaded to player. */
#define UPNP_N_TIMERS         0x00000020UL  /* Current playing position is changed. */
#define UPNP_N_PLUGIN         0x00000040UL  /* Plug-in status is changed. */
#define UPNP_N_ERROR          0x80000000UL  /* Error occured. */
#define UPNP_N_NOTIFY         0x40000000UL  /* Allows notify all event subscribers. */
#define UPNP_N_ALL            0x0000007FUL

/** Initializes of the UPNP subsystem. */
void upnp_init( void );
/** Terminates of the UPNP subsystem. */
void upnp_term( void );
/* Notifies UPNP subsystem about player state change. */
void upnp_notify_state_change( int event );

#ifdef __cplusplus
}
#endif
#endif /* PM123_UPNP_H */

