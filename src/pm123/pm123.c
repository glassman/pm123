/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_WIN
#define  INCL_GPI
#define  INCL_DOS
#define  INCL_DOSERRORS
#define  INCL_WINSTDDRAG
#include <os2.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <math.h>
#include <float.h>
#include <process.h>
#include <direct.h>
#include <io.h>
#include <utilfct.h>
#include <pipe.h>
#include <debuglog.h>

#include "pm123.h"
#include "bookmark.h"
#include "button95.h"
#include "pfreq.h"
#include "docking.h"
#include "messages.h"
#include "assertions.h"
#include "playlist.h"
#include "tags.h"
#include "filedlg.h"
#include "hotkeys.h"
#include "asso.h"
#include "mmsgbox.h"
#include "skin.h"
#include "iniman.h"
#include "sliders.h"
#include "bitmap.h"
#include "picture.h"
#include "banner.h"
#include "upnp.h"

#define  AMP_PAINT              ( WM_USER + 1001 ) /* options,   0                            */
#define  AMP_PB_STOP            ( WM_USER + 1002 ) /* 0,         0                            */
#define  AMP_PB_PLAY            ( WM_USER + 1003 ) /* pos,       0                            */
#define  AMP_PB_RESET           ( WM_USER + 1004 ) /* 0,         0                            */
#define  AMP_PB_PAUSE           ( WM_USER + 1005 ) /* 0,         0                            */
#define  AMP_PB_LOAD_SINGLEFILE ( WM_USER + 1006 ) /* filename,  options                      */
#define  AMP_PB_LOAD_URL        ( WM_USER + 1007 ) /* hwnd,      options                      */
#define  AMP_PB_LOAD_TRACK      ( WM_USER + 1008 ) /* hwnd,      options                      */
#define  AMP_PB_USE             ( WM_USER + 1009 ) /* use,       0                            */
#define  AMP_PB_VOLUME          ( WM_USER + 1010 ) /* volume,    0                            */
#define  AMP_PB_SEEK            ( WM_USER + 1011 ) /* pos,       0                            */
#define  AMP_DISPLAY_MESSAGE    ( WM_USER + 1013 ) /* message,   TRUE (info) or FALSE (error) */
#define  AMP_DISPLAY_MODE       ( WM_USER + 1014 ) /* 0,         0                            */
#define  AMP_QUERY_STRING       ( WM_USER + 1015 ) /* buffer,    size and type                */
#define  AMP_PB_FORWARD         ( WM_USER + 1017 ) /* 0,         0                            */
#define  AMP_PB_REWIND          ( WM_USER + 1018 ) /* 0,         0                            */
#define  AMP_PB_LOAD_RECORD     ( WM_USER + 1019 ) /* index,     0                            */
#define  AMP_PB_NEXT            ( WM_USER + 1020 ) /* 0,         0                            */
#define  AMP_PB_PREVIOUS        ( WM_USER + 1021 ) /* 0,         0                            */
#define  AMP_PB_SHUFFLE         ( WM_USER + 1022 ) /* enable,    0                            */
#define  AMP_PB_REPEAT          ( WM_USER + 1023 ) /* enable,    0                            */
#define  AMP_PB_MUTE            ( WM_USER + 1024 ) /* mute,      0                            */

int       amp_playmode = AMP_NOFILE;
HPOINTER  mp3;      /* Song file icon   */
HPOINTER  mp3play;  /* Played file icon */
HPOINTER  mp3gray;  /* Broken file icon */
HMODULE   hmodule;

/* Contains startup path of the program without its name.  */
char startpath[_MAX_PATH];
/* Contains a name of the currently loaded file. */
char current_filename[_MAX_URL];
/* Contains a information about of the currently loaded file. */
DECODER_INFO current_info;
/* Other parameters of the currently loaded file. */
char current_decoder[_MAX_MODULE_NAME];

static HAB    hab        = NULLHANDLE;
static HWND   heq        = NULLHANDLE;
static HWND   hframe     = NULLHANDLE;
static HWND   hplayer    = NULLHANDLE;
static HWND   hhelp      = NULLHANDLE;
static HWND   hmenu      = NULLHANDLE;
static HWND   hpopup     = NULLHANDLE;

static TID    pipe_tid   = -1;
static PQUEUE load_queue = NULL;
static TID    load_tid   = -1;

static BOOL   is_have_focus  = FALSE;
static BOOL   is_volume_drag = FALSE;
static BOOL   is_seeking     = FALSE;
static BOOL   is_slider_drag = FALSE;
static BOOL   is_arg_shuffle = FALSE;
static BOOL   is_terminate   = FALSE;
static BOOL   is_stopping    = FALSE;

/* Current seeking time. Valid if is_seeking == TRUE. */
static int    seeking_pos = 0;
static int    upd_options = 0;

typedef struct {

  USHORT count;
  USHORT first;

} CD_INFO;

void DLLENTRY
pm123_control( int type, void* param )
{
  switch( type )
  {
    case CONTROL_NEXTMODE:
      WinSendMsg( hplayer, AMP_DISPLAY_MODE, 0, 0 );
      break;
  }
}

int DLLENTRY
pm123_getstring( int type, int subtype, int size, char* buffer )
{
  WinSendMsg( hplayer, AMP_QUERY_STRING,
              MPFROMP( buffer ), MPFROM2SHORT( size, type ));
  return 0;
}

void DLLENTRY
pm123_display_info( char* info ) {
  WinPostMsg( hplayer, AMP_DISPLAY_MESSAGE, MPFROMP( strdup( info )), MPFROMLONG( FALSE ));
}

void DLLENTRY
pm123_display_error( char* info ) {
  WinPostMsg( hplayer, AMP_DISPLAY_MESSAGE, MPFROMP( strdup( info )), MPFROMLONG( TRUE  ));
}

/* Returns the information about installed CD drivers. */
static USHORT
amp_pb_cd_info( CD_INFO* info )
{
  HFILE hcdrom;
  ULONG action;
  ULONG len = sizeof( *info );

  memset( info, 0, sizeof( *info ));

  if( DosOpen( "\\DEV\\CD-ROM2$", &hcdrom, &action, 0,
               FILE_NORMAL, OPEN_ACTION_OPEN_IF_EXISTS,
               OPEN_SHARE_DENYNONE | OPEN_ACCESS_READONLY, NULL ) == NO_ERROR )
  {
    DosDevIOCtl( hcdrom, 0x82, 0x60, NULL, 0, NULL, info, len, &len );
    DosClose( hcdrom );
  }

  return info->count;
}

/* Adjusts audio volume to level accordingly current playing mode.
   Must be called from the main thread. */
static void
amp_pb_volume_adjust( void )
{
  if( cfg.mute ) {
    out_set_volume( 0 );
  } else if( is_forward() || is_rewind()) {
    out_set_volume( cfg.defaultvol * 0.7 );
  } else {
    out_set_volume( cfg.defaultvol );
  }
}

/* Sets the audio volume to the specified level.
   Must be called from the main thread. */
static BOOL
amp_pb_set_volume( int volume )
{
  ASSERT_IS_MAIN_THREAD;

  cfg.defaultvol = limit2( volume, 0, 100 );
  amp_pb_volume_adjust();
  amp_invalidate( UPD_VOLUME );
  upnp_notify_state_change( UPNP_N_VOLUME );
  return TRUE;
}

/* Mute audio.
   Must be called from the main thread. */
static BOOL
amp_pb_mute( BOOL mute )
{
  ASSERT_IS_MAIN_THREAD;

  cfg.mute = mute;
  amp_pb_volume_adjust();
  upnp_notify_state_change( UPNP_N_VOLUME );
  return TRUE;
}

/* Extracts specified part of the path and file name. */
char*
amp_parse_filename( char* result, const char* filename, int options, int size )
{
  char*  p;
  size_t len;

  if(( options & PFN_PRESCHEME ) && is_url( filename ))
  {
    strlcpy( result, "[" , size );
    scheme( result + 1, filename, size - 1 );
    if(( p = strchr( result, ':' )) != NULL ) {
      *p = 0;
    }
    strlcat( result, "] ", size );

    len   = strlen( result );
    p     = result + len;
    size -= len;
  } else {
    p     = result;
  }

  switch( options & PFN_MASK ) {
    case PFN_NAME:
      sfname( p, filename, size );
      break;

    case PFN_NAMEEXT:
      sfnameext( p, filename, size );
      break;
  }

  if( is_url( filename )) {
    sdecode( p, p, size );
    if( cfg.tags_charset != CH_DEFAULT ) {
      ch_convert( cfg.tags_charset, p, CH_DEFAULT, p, size );
    }
  }

  return result;
}

/* Constructs a string of the displayable text from the file information.
   Must be called from the main thread. */
static char*
amp_pb_construct_tag_string( char* result, const DECODER_INFO* info, const char* filename, int size )
{
  ASSERT_IS_MAIN_THREAD;

  *result = 0;

  if( *info->artist ) {
    strlcat( result, info->artist, size );
    strlcat( result, ": ", size );
  }

  if( *info->title ) {
    strlcat( result, info->title, size );
  } else {
    char songname[_MAX_URL];
    amp_title_from_filename( songname, filename, sizeof( songname ));
    strlcat( result, songname, size );
  }

  if( *info->album && *info->year )
  {
    strlcat( result, " (", size );
    strlcat( result, info->album, size );
    strlcat( result, ", ", size );
    strlcat( result, info->year,  size );
    strlcat( result, ")",  size );
  }
  else
  {
    if( *info->album && !*info->year )
    {
      strlcat( result, " (", size );
      strlcat( result, info->album, size );
      strlcat( result, ")",  size );
    }
    if( !*info->album && *info->year )
    {
      strlcat( result, " (", size );
      strlcat( result, info->year, size );
      strlcat( result, ")",  size );
    }
  }

  if( *info->comment )
  {
    strlcat( result, " -- ", size );
    strlcat( result, info->comment, size );
  }

  return control_strip( result );
}

/* Constructs a information text for currently loaded file and selects
   it for displaying. Must be called from the main thread. */
static void
amp_pb_display_filename( void )
{
  char display[ 512 ];
  ASSERT_IS_MAIN_THREAD;

  if( amp_playmode == AMP_NOFILE ) {
    bmp_set_text( "No file loaded" );
    return;
  }

  switch( cfg.viewmode )
  {
    case CFG_DISP_ID3TAG:
      amp_pb_construct_tag_string( display, &current_info, current_filename, sizeof( display ));

      if( *display ) {
        bmp_set_text( display );
        break;
      }

      // if tag is empty - use filename instead of it.

    case CFG_DISP_FILENAME:
      if( *current_filename ) {
        amp_parse_filename( display, current_filename, PFN_NAME | PFN_PRESCHEME, sizeof( display ));
        bmp_set_text( display );
      } else {
        bmp_set_text( "This is a bug!" );
      }
      break;

    case CFG_DISP_FILEINFO:
      bmp_set_text( current_info.tech_info );
      break;
  }
}

/* Draws all player timers and the position slider.
   Must be called from the main thread. */
static void
amp_pb_paint_timers( HPS hps )
{
  int play_time = 0;
  int play_left = current_info.songlength / 1000;
  int list_left = 0;

  ASSERT_IS_MAIN_THREAD;

  if( cfg.mode == CFG_MODE_REGULAR )
  {
    if( decoder_playing()) {
      if( !is_seeking ) {
        play_time = time_played();
      } else {
        play_time = seeking_pos;
      }
    }

    if( play_left > 0 ) {
      play_left -= play_time;
    }

    if( amp_playmode == AMP_PLAYLIST && !cfg.rpt ) {
      list_left = pl_playleft() - play_time;
    }

    bmp_draw_slider( hps, play_time, time_total());
    bmp_draw_timer ( hps, play_time );

    bmp_draw_tiny_timer( hps, POS_TIME_LEFT, play_left );
    bmp_draw_tiny_timer( hps, POS_PL_LEFT,   list_left );
  }
}

/* Draws the position slider. Must be called
   from the main thread. */
static void
amp_pb_paint_slider( HPS hps )
{
  int play_time = 0;

  ASSERT_IS_MAIN_THREAD;

  if( cfg.mode == CFG_MODE_REGULAR )
  {
    if( decoder_playing()) {
      if( !is_seeking ) {
        play_time = time_played();
      } else {
        play_time = seeking_pos;
      }
    }

    bmp_draw_slider( hps, play_time, time_total());
    bmp_draw_timer ( hps, play_time );
  }
}

/* Draws all attributes of the currently loaded file.
   Must be called from the main thread. */
static void
amp_pb_paint_fileinfo( HPS hps )
{
  ASSERT_IS_MAIN_THREAD;

  if( amp_playmode == AMP_PLAYLIST ) {
    bmp_draw_plind( hps, pl_loaded_index(), pl_size());
  } else {
    bmp_draw_plind( hps, 0, 0 );
  }

  bmp_draw_plmode  ( hps );
  bmp_draw_timeleft( hps );
  bmp_draw_rate    ( hps, current_info.bitrate );
  bmp_draw_channels( hps, current_info.mode );
  bmp_draw_text    ( hps );
}

/* Marks the player window as needed of redrawing. */
void
amp_invalidate( int options )
{
  if( options & UPD_WINDOW ) {
    WinInvalidateRect( hplayer, NULL, 1 );
    options &= ~UPD_WINDOW;
  }
  if( options & UPD_DELAYED || hplayer == NULLHANDLE ) {
    upd_options |= ( options & ~UPD_DELAYED );
  } else if( options ) {
    WinPostMsg( hplayer, AMP_PAINT, MPFROMLONG( options ), 0 );
  }
}

/* Returns the handle of the player window. */
HWND
amp_player_window( void ) {
  return hplayer;
}

/* Posts a command to the message queue associated with the player window. */
BOOL
amp_post_command( USHORT id ) {
  return WinPostMsg( hplayer, WM_COMMAND, MPFROMSHORT( id ), 0 );
}

/* Returns the anchor-block handle. */
HAB
amp_player_hab( void ) {
  return hab;
}

/* Updates title of the player window.
   Must be called from the main thread. */
static void
amp_pb_update_play_title( void )
{
  char caption[_MAX_URL];
  ASSERT_IS_MAIN_THREAD;

  sprintf( caption, "%s - ", AMP_FULLNAME );

  if( *current_info.title ) {
    strlcat( caption, current_info.title, sizeof( caption ));
  } else {
    char songname[_MAX_URL];
    amp_title_from_filename( songname, current_filename, sizeof( songname ));
    strlcat( caption, songname, sizeof( caption ));
  }

  WinSetWindowText( hframe, caption );
}

/* Hides and destroys the popup message window.
   Must be called from the main thread. */
static void
amp_pb_hide_popup( void )
{
  if( hpopup ) {
    WinDestroyWindow( hpopup );
    hpopup = NULLHANDLE;
  }
}

/* Creates and displays the popup message window.
   Must be called from the main thread. */
static BOOL
amp_pb_show_popup( void )
{
  FORMAT_INFO info;

  if( out_playing_samples( &info, NULL, 0 ) == PLUGIN_OK )
  {
    char popup[128];

    if( info.format == WAVE_FORMAT_DSD ) {
      snprintf( popup, sizeof( popup ), "Playing as DSD %.1f MHz, %ld channel(s)",
                info.samplerate * info.bits / 1000000.0, info.channels );
    } else {
      snprintf( popup, sizeof( popup ), "Playing as PCM %ld-bit %.1f kHz, %ld channel(s)",
                info.bits, info.samplerate / 1000.0, info.channels );
    }

    hpopup = CreateBubble( popup );
    return TRUE;
  }

  return FALSE;
}

/* Makes the copy of the meta information of the currently
   loaded file. Must be called from the main thread. */
static void
amp_pb_current_file( AMP_FILE* file )
{
  ASSERT_IS_MAIN_THREAD;

  dec_copyinfo( &file->info, &current_info );
  strcpy( file->filename, current_filename );
  strcpy( file->decoder, current_decoder );
}

/* Copies file information from one data structure to another. Also duplicates
   of the array of loaded attached pictures. */
void
amp_copyinfo( AMP_FILE* dst, const AMP_FILE* src )
{
  dec_copyinfo( &dst->info, &src->info );
  strcpy( dst->filename, src->filename );
  strcpy( dst->decoder, src->decoder );
}

/* Resets the file information data structure and frees memory
   allocated by previous functions. */
void
amp_cleaninfo( AMP_FILE* file )
{
  if( file ) {
    dec_cleaninfo( &file->info );
  }
}

/* Begins playback of the currently loaded file from the specified
   position. Must be called from the main thread. */
static BOOL
amp_pb_play_ex( int pos, int options )
{
  QMSG qms;
  BOOL rc = FALSE;
  ASSERT_IS_MAIN_THREAD;

  if( amp_playmode == AMP_NOFILE ) {
    WinSendDlgItemMsg( hplayer, BMP_PLAY, WM_DEPRESS, 0, 0 );
    return FALSE;
  }

  if( msg_play( pos, options ))
  {
    AMP_FILE file = { "", "", sizeof( DECODER_INFO )};

    amp_pb_volume_adjust();
    WinSendDlgItemMsg( hplayer, BMP_PLAY, WM_SETHELP, MPFROMP( "Stops playback" ), 0 );

    if( cfg.continuous ) {
      // If continuous playback is active and played file is too short,
      // the player can receive some WM_OUTPUT_OUTOFDATA messages during
      // start of such file. These messages must be ignored even if
      // playback is not continuous because of this.
      if( decoder_playing()) {
        while( WinPeekMsg( hab, &qms, hplayer, WM_OUTPUT_OUTOFDATA, WM_OUTPUT_OUTOFDATA, PM_REMOVE )) {
          DEBUGLOG(( "pm123: discard WM_OUTPUT_OUTOFDATA message.\n" ));
        }
      }
    }

    if( amp_playmode == AMP_PLAYLIST ) {
      pl_mark_as_play();
    }

    if( hpopup ) {
      amp_pb_hide_popup();
      amp_pb_show_popup();
    }

    amp_pb_update_play_title();
    amp_pb_current_file( &file );
    bn_show( &file );
    amp_cleaninfo( &file );
    WinSendDlgItemMsg( hplayer, BMP_PLAY, WM_PRESS  , 0, 0 );
    amp_invalidate( UPD_TIMERS );
    rc = TRUE;
  } else {
    WinSendDlgItemMsg( hplayer, BMP_PLAY, WM_DEPRESS, 0, 0 );
    amp_pb_hide_popup();
  }

  if( !is_forward()) { WinSendDlgItemMsg( hplayer, BMP_FWD,   WM_DEPRESS, 0, 0 ); }
  if( !is_rewind ()) { WinSendDlgItemMsg( hplayer, BMP_REW,   WM_DEPRESS, 0, 0 ); }
  if( !is_paused ()) { WinSendDlgItemMsg( hplayer, BMP_PAUSE, WM_DEPRESS, 0, 0 ); }

  return rc;
}

/* Begins playback of the currently loaded file.
   Must be called from the main thread. */
static BOOL
amp_pb_play( void ) {
  return amp_pb_play_ex( 0, 0 );
}

/* Toggles fast forwarding of the currently played file.
   Must be called from the main thread. */
static BOOL
amp_pb_forward( void )
{
  if( decoder_playing()) {
    WinSendDlgItemMsg( hplayer, BMP_REW, WM_DEPRESS, 0, 0 );
    msg_forward();
    WinSendDlgItemMsg( hplayer, BMP_FWD, is_forward() ? WM_PRESS : WM_DEPRESS, 0, 0 );
    amp_pb_volume_adjust();
  } else {
    WinSendDlgItemMsg( hplayer, BMP_FWD, WM_DEPRESS, 0, 0 );
    return FALSE;
  }

  return TRUE;
}

/* Toggles rewinding of the currently played file.
   Must be called from the main thread. */
static BOOL
amp_pb_rewind( void )
{
  if( decoder_playing()) {
    WinSendDlgItemMsg( hplayer, BMP_FWD, WM_DEPRESS, 0, 0 );
    msg_rewind();
    WinSendDlgItemMsg( hplayer, BMP_REW, is_rewind() ? WM_PRESS : WM_DEPRESS, 0, 0 );
    amp_pb_volume_adjust();
  } else {
    WinSendDlgItemMsg( hplayer, BMP_REW, WM_DEPRESS, 0, 0 );
    return FALSE;
  }

  return TRUE;
}

/* Stops the playing of the current file. Must be called
   from the main thread. */
static BOOL
amp_pb_stop_ex( int options )
{
  QMSG qms;
  ASSERT_IS_MAIN_THREAD;

  is_stopping = TRUE;

  DEBUGLOG(( "pm123: receive a stopping request.\n" ));

  if( msg_stop( options )) {
    DEBUGLOG(( "pm123: player is stopped.\n" ));
    WinSendDlgItemMsg( hplayer, BMP_PLAY,  WM_SETHELP, MPFROMP( "Starts playing" ), 0 );
    WinSendDlgItemMsg( hplayer, BMP_PLAY,  WM_DEPRESS, 0, 0 );
    WinSendDlgItemMsg( hplayer, BMP_PAUSE, WM_DEPRESS, 0, 0 );
    WinSendDlgItemMsg( hplayer, BMP_FWD,   WM_DEPRESS, 0, 0 );
    WinSendDlgItemMsg( hplayer, BMP_REW,   WM_DEPRESS, 0, 0 );
    WinSetWindowText ( hframe,  AMP_FULLNAME );
    amp_pb_hide_popup();

    if( amp_playmode == AMP_PLAYLIST ) {
      pl_mark_as_stop();
    }

    while( WinPeekMsg( hab, &qms, hplayer, WM_PLAYSTOP, WM_PLAYSTOP, PM_REMOVE )) {
      DEBUGLOG(( "pm123: discard WM_PLAYSTOP message.\n" ));
    }
    while( WinPeekMsg( hab, &qms, hplayer, WM_OUTPUT_OUTOFDATA, WM_OUTPUT_OUTOFDATA, PM_REMOVE )) {
      DEBUGLOG(( "pm123: discard WM_OUTPUT_OUTOFDATA message.\n" ));
    }
    while( WinPeekMsg( hab, &qms, hplayer, WM_PLAYERROR, WM_PLAYERROR, PM_REMOVE )) {
      DEBUGLOG(( "pm123: discard WM_PLAYERROR message.\n" ));
    }

    amp_invalidate( UPD_TIMERS );
    is_stopping = FALSE;
    is_seeking  = FALSE;
    bn_hide();
    return TRUE;
  }

  DEBUGLOG(( "pm123: unable to stop a player.\n" ));
  is_stopping = FALSE;
  return FALSE;
}

/* Stops the playing of the current file. Must be called
   from the main thread. */
static BOOL
amp_pb_stop( void ) {
  return amp_pb_stop_ex( 0 );
}

/* Stops playing and resets the player to its default state.
   Must be called from the main thread. */
static BOOL
amp_pb_reset( void )
{
  ASSERT_IS_MAIN_THREAD;

  if( !is_busy() && (!decoder_playing() || amp_pb_stop()))
  {
    amp_playmode = AMP_NOFILE;
    current_filename[0] = 0;
    current_decoder [0] = 0;

    dec_cleaninfo( &current_info );
    pl_refresh_status();
    amp_invalidate( UPD_FILENAME | UPD_FILEINFO | UPD_TIMERS );
    upnp_notify_state_change( UPNP_N_ALL );
    return TRUE;
  }

  return FALSE;
}

/* Suspends or resumes playback of the currently played file.
   Must be called from the main thread. */
static BOOL
amp_pb_pause( void )
{
  ASSERT_IS_MAIN_THREAD;

  if( decoder_playing())
  {
    msg_pause();

    if( is_paused()) {
      WinSendDlgItemMsg( hplayer, BMP_PAUSE, WM_PRESS, 0, 0 );
      return TRUE;
    }
  }

  WinSendDlgItemMsg( hplayer, BMP_PAUSE, WM_DEPRESS, 0, 0 );
  return TRUE;
}

/* Activates or deactivates the current playlist. Must be
   called from the main thread. */
static BOOL
amp_pb_use( BOOL use )
{
  ASSERT_IS_MAIN_THREAD;

  if( !is_busy()) {
    if( use ) {
      if( pl_size()) {
        amp_playmode = AMP_PLAYLIST;
        pl_refresh_status();

        if( pl_load_file_record( current_filename )) {
          if( decoder_playing()) {
            pl_mark_as_play();
          } else if( cfg.playonuse ) {
            amp_pb_play();
          }
        } else {
          if( pl_load_first_record() && cfg.playonuse ) {
            if( !cfg.continuous || is_always_hungry()) {
              amp_pb_stop();
            }
            amp_pb_play();
          } else {
            amp_pb_stop();
          }
        }
      }
    } else {
      if( amp_playmode != AMP_SINGLE )
      {
        amp_playmode = AMP_SINGLE;
        pl_refresh_status();
        pl_mark_as_stop  ();
        pl_clean_shuffle ();
      }
    }

    amp_invalidate( UPD_FILEINFO | UPD_TIMERS );
    upnp_notify_state_change( UPNP_N_PLAYERMODE | UPNP_N_TRACKS | UPNP_N_TIMERS );
    return TRUE;
  }

  return FALSE;
}

/* Changes the current playing position of the currently
   played file. Must be called from the main thread. */
static BOOL
amp_pb_seek( int pos )
{
  BOOL rc;
  ASSERT_IS_MAIN_THREAD;

  pos = max( pos, 0 );
  seeking_pos = pos / 1000;
  is_seeking = TRUE;

  if(( rc = msg_seek( pos )) != TRUE ) {
    // If msg_seek() is success, is_seeking will be reset at WM_SEEKSTOP processing.
    is_seeking = FALSE;
  }

  amp_invalidate( UPD_TIMERS );
  return rc;
}

/* Advances to the next playlist record.
   Must be called from the main thread. */
static BOOL
amp_pb_next( void )
{
  if( amp_playmode == AMP_PLAYLIST )
  {
    BOOL decoder_was_playing = decoder_playing();

    if( decoder_was_playing ) {
      if( !cfg.continuous || is_always_hungry()) {
        if( !amp_pb_stop()) {
          return FALSE;
        }
      }
    }

    pl_load_next_record();

    if( decoder_was_playing ) {
      return amp_pb_play();
    }
  }

  return FALSE;
}

/* Advances to the previous playlist record.
   Must be called from the main thread. */
static BOOL
amp_pb_previous( void )
{
  if( amp_playmode == AMP_PLAYLIST )
  {
    BOOL decoder_was_playing = decoder_playing();

    if( decoder_was_playing ) {
      if( !cfg.continuous || is_always_hungry()) {
        if( !amp_pb_stop()) {
          return FALSE;
        }
      }
    }

    pl_load_prev_record();

    if( decoder_was_playing ) {
      return amp_pb_play();
    }
  }

  return FALSE;
}

/* Enables the shuffle mode.
   Must be called from the main thread. */
static BOOL
amp_pb_shuffle( BOOL enable )
{
  cfg.shf = enable;

  if( amp_playmode == AMP_PLAYLIST ) {
    pl_clean_shuffle();
    if( cfg.shf && decoder_playing()) {
      pl_mark_as_play();
    }
  }
  if( cfg.shf ) {
    WinSendDlgItemMsg( hplayer, BMP_SHUFFLE, WM_PRESS,   0, 0 );
  } else {
    WinSendDlgItemMsg( hplayer, BMP_SHUFFLE, WM_DEPRESS, 0, 0 );
  }

  amp_invalidate( UPD_FILEINFO | UPD_TIMERS );
  upnp_notify_state_change( UPNP_N_PLAYERMODE | UPNP_N_TIMERS );
  return TRUE;
}

/* Enables the repeat mode.
   Must be called from the main thread. */
static BOOL
amp_pb_repeat( BOOL enable )
{
  cfg.rpt = enable;

  if( cfg.rpt ) {
    WinSendDlgItemMsg( hplayer, BMP_REPEAT, WM_PRESS,   0, 0 );
  } else {
    WinSendDlgItemMsg( hplayer, BMP_REPEAT, WM_DEPRESS, 0, 0 );
  }

  amp_invalidate( UPD_FILEINFO | UPD_TIMERS );
  upnp_notify_state_change( UPNP_N_PLAYERMODE | UPNP_N_TIMERS );
  return TRUE;
}

/* Loads the specified playlist record to the player and
   plays it if this is specified in the player properties.
   Must be called from the main thread. */
static BOOL
amp_pb_load_record( ULONG index, int options )
{
  BOOL decoder_was_playing = decoder_playing();

  if( is_busy()) {
    return FALSE;
  }

  if( decoder_was_playing ) {
    if( !cfg.continuous || is_always_hungry()) {
      if( !amp_pb_stop()) {
        return FALSE;
      }
    }
  }

  if( !pl_load_record( index )) {
    return FALSE;
  }

  if(!( options & AMP_LOAD_NOT_PLAY )) {
    if( cfg.playonload || decoder_was_playing ) {
      if( !amp_pb_play()) {
        return FALSE;
      }
      pl_mark_as_play();
    }
  }

  return TRUE;
}

/* Loads a standalone file or CD track to the player and
   plays it if this is specified in the player properties.
   Both AMP_LOAD_* and PL_LOAD_* options can be used.
   Must be called from the main thread. */
static BOOL
amp_pb_load_singlefile( const char* filename, int options )
{
  int    i;
  ULONG  rc;
  char   module_name[_MAX_FNAME] = "";
  BOOL   decoder_was_playing = decoder_playing();
  struct stat fi;

  ASSERT_IS_MAIN_THREAD;

  if( is_busy()) {
    return FALSE;
  }
  if( is_regular_file( filename ) && stat( filename, &fi ) != 0 ) {
    amp_show_error( "Unable load file:\n%s\n%s", filename, strerror(errno));
    return FALSE;
  }
  if( is_playlist( filename )) {
    return pl_load( filename, options | PL_LOAD_CLEAR );
  }

  if( decoder_was_playing ) {
    if( !cfg.continuous || is_always_hungry()) {
      if( !amp_pb_stop()) {
        return FALSE;
      }
    }
  }

  amp_pb_use( FALSE );
  rc = msg_fileinfo((char*)filename, &current_info, module_name );

  if( rc != PLUGIN_OK )
  {
    amp_pb_reset();

    if( rc == PLUGIN_NO_READ ) {
      amp_show_error( "The file %s could not be read.", filename );
    } else if( rc == PLUGIN_NO_PLAY ) {
      amp_show_error( "The file %s cannot be played by PM123. "
                      "The file might be corrupted or the necessary plug-in is "
                      "not loaded or enabled.", filename );
    } else {
      amp_show_error( "%s: Error occurred: %s", filename, xio_strerror( rc ));
    }

    return FALSE;
  }

  strcpy( current_filename, filename );
  strcpy( current_decoder, module_name );
  amp_invalidate( UPD_FILENAME | UPD_FILEINFO );
  upnp_notify_state_change( UPNP_N_FILELOADED | UPNP_N_TRACKS | UPNP_N_TIMERS );

  if( !( options & AMP_LOAD_NOT_RECALL ) && !is_track( filename )) {
    for( i = 0; i < MAX_RECALL; i++ ) {
      if( nlstricmp( cfg.last_file[i], filename ) == 0 ) {
        while( ++i < MAX_RECALL ) {
          strcpy( cfg.last_file[i-1], cfg.last_file[i] );
        }
        break;
      }
    }

    for( i = MAX_RECALL - 2; i >= 0; i-- ) {
      strcpy( cfg.last_file[i + 1], cfg.last_file[i] );
    }

    strcpy( cfg.last_file[0], filename );
  }

  if(!( options & AMP_LOAD_NOT_PLAY )) {
    if( cfg.playonload || decoder_was_playing || ( options & AMP_LOAD_PLAY )) {
      return amp_pb_play();
    }
  }

  return TRUE;
}

/* Opens folder containing specified file. */
BOOL amp_open_containing_folder( const char* filename )
{
  if( is_file( filename ))
  {
    char    pathname[_MAX_URL];
    int     pathsize;
    HOBJECT hobject;

    sdrivedir( pathname, filename, sizeof( pathname ));
    pathsize = strlen( pathname );
    if( pathsize && pathname[ pathsize - 1 ] == '\\' && !is_root( pathname )) {
      pathname[ pathsize - 1 ] = 0;
    }

    hobject = WinQueryObject( pathname );

    if( hobject != NULLHANDLE ) {
      if( WinSetObjectData( hobject, "OPEN=DEFAULT;" )) {
        // Uses the function WinSetObjectData twice to open an object and
        // run it in the foreground.
        WinSetObjectData( hobject, "OPEN=DEFAULT;" );
      }
      return TRUE;
    }
  }

  return FALSE;
}

/* Activates or deactivates the current playlist. */
BOOL
amp_pl_use( BOOL use ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_USE, MPFROMLONG( use ), 0 ));
}

/* Loads a standalone file or CD track to the player and
   plays it if this is specified in the player properties.
   Both AMP_LOAD_* and PL_LOAD_* options can be used. */
BOOL
amp_load_singlefile( const char* filename, int options )
{
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_LOAD_SINGLEFILE,
                                 MPFROMP( filename ), MPFROMLONG( options )));
}

/* Loads the specified playlist record to the player and
   plays it if this is specified in the player properties. */
BOOL
amp_load_record( ULONG index, int options )
{
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_LOAD_RECORD,
                                 MPFROMLONG( index ),  MPFROMLONG( options )));
}

/* Begins playback of the currently loaded file from
   the specified position. */
BOOL
amp_play( int pos ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_PLAY, MPFROMLONG( pos ), 0 ));
}

/* Toggles fast forwarding of the currently played file. */
BOOL
amp_forward( void ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_FORWARD, 0, 0 ));
}

/* Toggles fast forwarding of the currently played file. */
BOOL
amp_rewind( void ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_REWIND, 0, 0 ));
}

/* Changes the current playing position of the currently played file. */
BOOL
amp_seek( int pos ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_SEEK, MPFROMLONG( pos ), 0 ));
}

/* Advances to the next playlist record. */
BOOL
amp_next() {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_NEXT, 0, 0 ));
}

/* Advances to the previous playlist record. */
BOOL
amp_previous() {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_PREVIOUS, 0, 0 ));
}

/* Enables the shuffle mode. */
BOOL
amp_shuffle( BOOL enable ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_SHUFFLE, MPFROMLONG( enable ), 0 ));
}

/* Enables the repeat mode. */
BOOL
amp_repeat( BOOL enable ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_REPEAT, MPFROMLONG( enable ), 0 ));
}

/* Stops playback of the currently played file. */
BOOL
amp_stop( void ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_STOP, 0, 0 ));
}

/* Stops playing and resets the player to its default state. */
BOOL
amp_reset( void ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_RESET, 0, 0 ));
}

/* Suspends or resumes playback of the currently played file. */
BOOL
amp_pause( void ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_PAUSE, 0, 0 ));
}

/* Sets the audio volume to the specified level. */
BOOL
amp_set_volume( int volume ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_VOLUME, MPFROMLONG( volume ), 0 ));
}

/* Mute audio. */
BOOL
amp_mute( BOOL mute ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_MUTE, MPFROMLONG( mute ), 0 ));
}

/* Shows the context menu of the playlist. Must be called from
   the main thread. */
static void
amp_pb_show_context_menu( HWND parent )
{
  POINTL   pos;
  SWP      swp;
  char     file[_MAX_URL];
  HWND     mh;
  int      i;
  int      count;
  CD_INFO  cdinfo;

  ASSERT_IS_MAIN_THREAD;

  if( hmenu == NULLHANDLE ) {
    hmenu = WinLoadMenu( HWND_OBJECT, hmodule, MNU_MAIN );
    mn_set_default( mn_get_submenu( hmenu, IDM_M_LOAD_MENU ), IDM_M_LOAD_FILE );
  }

  WinQueryPointerPos( HWND_DESKTOP, &pos );
  WinMapWindowPoints( HWND_DESKTOP, parent, &pos, 1 );

  if( WinWindowFromPoint( parent, &pos, TRUE ) == NULLHANDLE )
  {
    // The context menu is probably activated from the keyboard.
    WinQueryWindowPos( parent, &swp );
    pos.x = swp.cx / 2;
    pos.y = swp.cy / 2;
  }

  // Update CDs menu.
  mh    = mn_get_submenu( hmenu, IDM_M_DISCS );
  count = mn_count( mh );

  for( i = 0; i < count; i++ ) {
    mn_remove_item( mh, mn_item_id( mh, 0 ));
  }

  if( amp_pb_cd_info( &cdinfo ) == 0 ) {
    mn_enable_item( hmenu, IDM_M_DISCS, FALSE );
  } else {
    char drive[3] = "X:";
    LONG defcd    = IDM_M_DISCS + 1 + cdinfo.first;

    for( i = 0; i < cdinfo.count; i++ )
    {
      drive[0] = 'A' + cdinfo.first + i;
      mn_add_item( mh, IDM_M_DISCS  + i + 1 + cdinfo.first, MPFROMP( drive ), TRUE, FALSE, NULL );

      if( stricmp( cfg.cddrive, drive ) == 0 ) {
        defcd = IDM_M_DISCS + i + 1 + cdinfo.first;
      }
    }

    mn_set_default( mh, defcd );
    mn_enable_item( hmenu, IDM_M_DISCS, TRUE );
  }

  // Update load menu.
  mh = mn_get_submenu( hmenu, IDM_M_LOAD_MENU );
  count = mn_count( mh );

  // Remove all items from the load menu except of three first
  // intended for a choice of object of loading.
  for( i = 3; i < count; i++ ) {
    mn_remove_item( mh, mn_item_id( mh, 3 ));
  }

  // Fill the recall list.
  if( *cfg.last_file[0] ) {
    mn_add_separator( mh, 0 );

    for( i = 0; i < MAX_RECALL; i++ ) {
      if( *cfg.last_file[i] )
      {
        sprintf( file, "~%u ", i + 1 );
        amp_parse_filename( file + strlen( file ), cfg.last_file[i],
                            PFN_NAMEEXT | PFN_PRESCHEME, sizeof( file ) - strlen( file ) );
        mn_add_item( mh, IDM_M_LOAD_LAST + i + 1, file, TRUE, FALSE, NULL );
      }
    }

    mn_add_separator( mh, 0 );
    mn_add_item( mh, IDM_M_LOAD_CLEAR, "~Clear history", TRUE, FALSE, NULL );
  }


  // Update bookmarks and plug-ins submenus.
  bm_add_bookmarks_to_menu( mn_get_submenu( hmenu, IDM_M_BOOKMARKS ));
  pg_prepare_plugin_menu( mn_get_submenu( hmenu, IDM_M_PLUGINS ));

  // Update status
  mn_enable_item( hmenu, IDM_M_LOAD_MENU,   !is_busy());
  mn_enable_item( hmenu, IDM_M_BOOKMARKS,   !is_busy());
  mn_enable_item( hmenu, IDM_M_PLUGINS,     !is_busy());
  mn_enable_item( hmenu, IDM_M_DISCS,       !is_busy());
  mn_enable_item( hmenu, IDM_M_EDIT,        !is_busy() && *current_filename );
  mn_enable_item( hmenu, IDM_M_PLAYBACK,    !is_busy());
  mn_enable_item( hmenu, IDM_M_PROPERTIES,  !is_busy());
  mn_enable_item( hmenu, IDM_M_SAVE_STREAM, !is_busy());
  mn_enable_item( hmenu, IDM_M_OPEN_FOLDER,  is_file( current_filename ));
  mn_enable_item( hmenu, IDM_M_SIZE_SMALL,  bmp_is_mode_supported( CFG_MODE_SMALL   ));
  mn_enable_item( hmenu, IDM_M_SIZE_NORMAL, bmp_is_mode_supported( CFG_MODE_REGULAR ));
  mn_enable_item( hmenu, IDM_M_SIZE_TINY,   bmp_is_mode_supported( CFG_MODE_TINY    ));
  mn_enable_item( hmenu, IDM_M_FONT_MENU,   cfg.font_skinned );
  mn_enable_item( hmenu, IDM_M_FONT_SET1,   bmp_is_font_supported( 0 ));
  mn_enable_item( hmenu, IDM_M_FONT_SET2,   bmp_is_font_supported( 1 ));
  mn_enable_item( hmenu, IDM_M_BM_ADD,      amp_playmode != AMP_NOFILE );

  mn_check_item( hmenu, IDM_M_FLOAT,        cfg.floatontop   );
  mn_check_item( hmenu, IDM_M_SAVE_STREAM,  is_stream_saved());
  mn_check_item( hmenu, IDM_M_FONT_SET1,    cfg.font == 0    );
  mn_check_item( hmenu, IDM_M_FONT_SET2,    cfg.font == 1    );
  mn_check_item( hmenu, IDM_M_SIZE_SMALL,   cfg.mode == CFG_MODE_SMALL   );
  mn_check_item( hmenu, IDM_M_SIZE_NORMAL,  cfg.mode == CFG_MODE_REGULAR );
  mn_check_item( hmenu, IDM_M_SIZE_TINY,    cfg.mode == CFG_MODE_TINY    );
  mn_check_item( hmenu, IDM_M_PAUSE,        is_paused ());
  mn_check_item( hmenu, IDM_M_FWD,          is_forward());
  mn_check_item( hmenu, IDM_M_REW,          is_rewind ());
  mn_check_item( hmenu, IDM_M_MUTE,         cfg.mute );

  WinPopupMenu( parent, parent, hmenu, pos.x, pos.y, 0,
                PU_HCONSTRAIN   | PU_VCONSTRAIN |
                PU_MOUSEBUTTON1 | PU_MOUSEBUTTON2 | PU_KEYBOARD   );
}

/* Adds URL to the playlist or load it to the player.
   Must be called from the main thread. */
static BOOL
amp_pb_load_url( HWND owner, int options )
{
  char url[_MAX_URL];
  HWND hwnd;
  int  i;

  hwnd = WinLoadDlg( HWND_DESKTOP, owner, WinDefDlgProc, hmodule, DLG_URL, 0 );
  ASSERT_IS_MAIN_THREAD;

  if( hwnd == NULLHANDLE ) {
    return FALSE;
  }

  do_warpsans( hwnd );
  WinSendDlgItemMsg( hwnd, ENT_URL, EM_SETTEXTLIMIT, MPFROMSHORT( sizeof( url ) - 1 ), 0 );

  for( i = 0; i < MAX_RECALL; i++ ) {
    if( *cfg.last_urls[i] ) {
      // The OS/2 combination box control incorrectly set entry field in case if
      // list items have length above 256 bytes. At this moment I simple ignore
      // such URLs. Maybe later a better solution will appear.
      if( strlen( cfg.last_urls[i] ) < 256 ) {
        lb_add_item( hwnd, ENT_URL, cfg.last_urls[i] );
      }
    }
  }

  if( options & URL_ADD_TO_LIST ) {
    WinSetWindowText( hwnd, "Add URL" );
  } else {
    WinSetWindowText( hwnd, "Load URL" );
  }

  if( WinProcessDlg( hwnd ) == DID_OK ) {
    WinQueryDlgItemText( hwnd, ENT_URL, sizeof( url ), url );
    if( *url ) {
      for( i = 0; i < MAX_RECALL; i++ ) {
        if( nlstricmp( cfg.last_urls[i], url ) == 0 ) {
          while( ++i < MAX_RECALL ) {
            strcpy( cfg.last_urls[i-1], cfg.last_urls[i] );
          }
          break;
        }
      }

      for( i = MAX_RECALL - 2; i >= 0; i-- ) {
        strcpy( cfg.last_urls[i + 1], cfg.last_urls[i] );
      }

      strcpy( cfg.last_urls[0], url );

      if( options & URL_ADD_TO_LIST ) {
        if( is_playlist( url )) {
          pl_load( url, 0 );
        } else {
          pl_add_file( url, NULL, 0 );
        }
      } else {
        amp_pb_load_singlefile( url, 0 );
      }
    }
  }
  WinDestroyWindow( hwnd );
  return TRUE;
}

/* Adds URL to the playlist or load it to the player. */
BOOL
amp_load_url( HWND owner, int options ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_LOAD_URL,
                                 MPFROMHWND( owner ), MPFROMLONG( options )));
}

/* Processes messages of the dialog of addition of CD tracks.
   Must be called from the main thread. */
static MRESULT EXPENTRY
amp_pb_load_track_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_CONTROL:
      if( SHORT1FROMMP(mp1) == CB_DRIVE && SHORT2FROMMP(mp1) == CBN_EFCHANGE ) {
        WinPostMsg( hwnd, WM_COMMAND,
                    MPFROMSHORT( PB_REFRESH ), MPFROM2SHORT( CMDSRC_OTHER, FALSE ));
      }
      break;

    case WM_COMMAND:
      if( COMMANDMSG(&msg)->cmd == PB_REFRESH )
      {
        char drive[3];
        char track[32];
        int  options = WinQueryWindowULong( hwnd, QWL_USER );
        int  i;

        DECODER_CDINFO cdinfo = { 0 };

        WinQueryDlgItemText( hwnd, CB_DRIVE, sizeof( drive ), drive );
        lb_remove_all( hwnd, LB_TRACKS );

        if( dec_cdinfo( drive, &cdinfo ) == 0 ) {
          if( cdinfo.firsttrack ) {
            for( i = cdinfo.firsttrack; i <= cdinfo.lasttrack; i++ ) {
              sprintf( track,"Track %02d", i );
              lb_add_item( hwnd, LB_TRACKS, track );
            }
            if( options & TRK_ADD_TO_LIST ) {
              for( i = cdinfo.firsttrack; i <= cdinfo.lasttrack; i++ ) {
                lb_select( hwnd, LB_TRACKS, i - cdinfo.firsttrack );
              }
            } else {
              lb_select( hwnd, LB_TRACKS, 0 );
            }
          }
        } else {
          amp_show_error( "Cannot find decoder that supports CD tracks." );
        }

        return 0;
      }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Adds CD tracks to the playlist or load one to the player.
   Must be called from the main thread. */
static BOOL
amp_pb_load_track( HWND owner, int options )
{
  HFILE hcdrom;
  ULONG action;
  HWND  hwnd;

  ASSERT_IS_MAIN_THREAD;
  hwnd = WinLoadDlg( HWND_DESKTOP, owner,
                     amp_pb_load_track_dlg_proc, hmodule, DLG_TRACK, 0 );

  if( hwnd == NULLHANDLE ) {
    return FALSE;
  }

  WinSetWindowULong( hwnd, QWL_USER, options );
  do_warpsans( hwnd );

  if( options & TRK_ADD_TO_LIST ) {
    WinSetWindowBits( WinWindowFromID( hwnd, LB_TRACKS ), QWL_STYLE, 1, LS_MULTIPLESEL );
    WinSetWindowBits( WinWindowFromID( hwnd, LB_TRACKS ), QWL_STYLE, 1, LS_EXTENDEDSEL );
    WinSetWindowText( hwnd, "Add Tracks" );
  } else {
    WinSetWindowText( hwnd, "Load Track" );
  }

  if( DosOpen( "\\DEV\\CD-ROM2$", &hcdrom, &action, 0,
               FILE_NORMAL, OPEN_ACTION_OPEN_IF_EXISTS,
               OPEN_SHARE_DENYNONE | OPEN_ACCESS_READONLY, NULL ) == NO_ERROR )
  {
    CD_INFO cdinfo;
    char    drive[3] = "X:";
    ULONG   i;

    if( amp_pb_cd_info( &cdinfo ) > 0 ) {
      for( i = 0; i < cdinfo.count; i++ ) {
        drive[0] = 'A' + cdinfo.first + i;
        lb_add_item( hwnd, CB_DRIVE, drive );
      }
    }

    if( *cfg.cddrive ) {
      WinSetDlgItemText( hwnd, CB_DRIVE, cfg.cddrive );
    } else {
      lb_select( hwnd, CB_DRIVE, 0 );
    }
  }

  if( WinProcessDlg( hwnd ) == DID_OK ) {
    SHORT selected = lb_selected( hwnd, LB_TRACKS, LIT_FIRST );
    WinQueryDlgItemText( hwnd, CB_DRIVE, sizeof( cfg.cddrive ), cfg.cddrive );

    if( selected != LIT_NONE ) {
      if( options & TRK_ADD_TO_LIST ) {
        char track[32];
        char cdurl[64];

        while( selected != LIT_NONE ) {
          lb_get_item( hwnd, LB_TRACKS, selected, track, sizeof(track));
          sprintf( cdurl, "cd:///%s\\%s", cfg.cddrive, track );
          pl_add_file( cdurl, NULL, 0 );
          selected = lb_selected( hwnd, LB_TRACKS, selected );
        }
        pl_completed();
      } else {
        char track[32];
        char cdurl[64];

        lb_get_item( hwnd, LB_TRACKS, selected, track, sizeof(track));
        sprintf( cdurl, "cd:///%s\\%s", cfg.cddrive, track );
        amp_load_singlefile( cdurl, 0 );
      }
    }
  }
  WinDestroyWindow( hwnd );
  return TRUE;
}

/* Adds all CD tracks to the playlist.
   Must be called from the main thread. */
static BOOL
amp_pb_load_disc( int drive_no )
{
  char drive[ 3] = "X:";
  char cdurl[64];
  int  i;

  DECODER_CDINFO cdinfo = { 0 };
  ASSERT_IS_MAIN_THREAD;

  if( !is_busy()) {
    drive[0] = 'A' + drive_no;

    if( dec_cdinfo( drive, &cdinfo ) == 0 ) {
      if( cdinfo.firsttrack ) {
        pl_clear();
        for( i = cdinfo.firsttrack; i <= cdinfo.lasttrack; i++ ) {
          sprintf( cdurl, "cd:///%s\\Track %02d", drive, i );
          pl_add_file( cdurl, NULL, 0 );
        }
        pl_completed();
        strcpy( cfg.cddrive, drive );
      }
      return TRUE;
    }
  }

  return FALSE;
}

/* Adds CD tracks to the playlist or load one to the player. */
BOOL
amp_load_track( HWND owner, int options ) {
  return LONGFROMMR( WinSendMsg( hplayer, AMP_PB_LOAD_TRACK,
                                 MPFROMHWND( owner ), MPFROMLONG( options )));
}

/* Reads url from the specified file. */
char*
amp_url_from_file( char* result, const char* filename, int size )
{
  FILE* file = fopen( filename, "r" );

  if( file ) {
    if( fgets( result, size, file )) {
        blank_strip( result );
    }
    fclose( file );
  } else {
    *result = 0;
  }

  return result;
}

/* Extracts song title from the specified file name. */
char*
amp_title_from_filename( char* result, const char* filename, int size )
{
  char* p;

  if( result && filename ) {
    amp_parse_filename( result, filename, PFN_NAME, size );
    for( p = result; *p; p++ ) {
      if( *p == '_' ) {
          *p =  ' ';
      }
    }
  }

  return result;
}

/* Prepares the player to the drop operation. Must be
   called from the main thread. */
static MRESULT
amp_pb_drag_over( HWND hwnd, PDRAGINFO pdinfo )
{
  PDRAGITEM pditem;
  int       i;
  USHORT    drag_op = 0;
  USHORT    drag    = DOR_NEVERDROP;

  if( is_busy()) {
    return MPFROM2SHORT( DOR_NODROPOP, DO_UNKNOWN );
  }
  if( !DrgAccessDraginfo( pdinfo )) {
    return MRFROM2SHORT( DOR_NEVERDROP, 0 );
  }

  for( i = 0; i < pdinfo->cditem; i++ )
  {
    #ifdef DEBUG
      char info[2048];
      pditem = DrgQueryDragitemPtr( pdinfo, i );

      DEBUGLOG(( "pm123: DnD info (%02d) hwndSource: %08X\n", i, pdinfo->hwndSource ));
      DEBUGLOG(( "pm123: DnD info (%02d) hwndItem:   %08X\n", i, pditem->hwndItem   ));
      DEBUGLOG(( "pm123: DnD info (%02d) ulItemID:   %lu\n",  i, pditem->ulItemID   ));
      DEBUGLOG(( "pm123: DnD info (%02d) fsControl:  %08X\n", i, pditem->fsControl  ));

      DrgQueryStrName( pditem->hstrType, sizeof( info ), info );
      DEBUGLOG(( "pm123: DnD info (%02d) hstrType: %s\n", i, info ));
      DrgQueryStrName( pditem->hstrRMF,  sizeof( info ), info );
      DEBUGLOG(( "pm123: DnD info (%02d) hstrRMF: %s\n", i, info ));
      DrgQueryStrName( pditem->hstrContainerName, sizeof( info ), info );
      DEBUGLOG(( "pm123: DnD info (%02d) hstrContainerName: %s\n", i, info ));
      DrgQueryStrName( pditem->hstrSourceName, sizeof( info ), info );
      DEBUGLOG(( "pm123: DnD info (%02d) hstrSourceName: %s\n", i, info ));
      DrgQueryStrName( pditem->hstrTargetName, sizeof( info ), info );
      DEBUGLOG(( "pm123: DnD info (%02d) hstrTargetName: %s\n", i, info ));
    #else
      pditem = DrgQueryDragitemPtr( pdinfo, i );
    #endif

    if( DrgVerifyRMF( pditem, "DRM_123FILE", NULL )) {
      if( pdinfo->usOperation == DO_DEFAULT ) {
        drag    = DOR_DROP;
        drag_op = DO_COPY;
      }
      else if( pdinfo->usOperation == DO_COPY ||
               pdinfo->usOperation == DO_MOVE ||
               pdinfo->usOperation == DO_LINK )
      {
        drag    = DOR_DROP;
        drag_op = pdinfo->usOperation;
      } else {
        drag    = DOR_NODROPOP;
        drag_op = DO_UNKNOWN;
        break;
      }
    } else if( DrgVerifyRMF( pditem, "DRM_OS2FILE", NULL )) {
      if( pdinfo->usOperation == DO_DEFAULT &&
          pditem->fsSupportedOps & DO_COPYABLE )
      {
        drag    = DOR_DROP;
        drag_op = DO_COPY;
      }
      else if( pdinfo->usOperation == DO_LINK &&
               pditem->fsSupportedOps & DO_LINKABLE )
      {
        drag    = DOR_DROP;
        drag_op = pdinfo->usOperation;
      } else {
        drag    = DOR_NODROPOP;
        drag_op = DO_UNKNOWN;
        break;
      }
    } else {
      drag    = DOR_NEVERDROP;
      drag_op = DO_UNKNOWN;
      break;
    }
  }

  DrgFreeDraginfo( pdinfo );
  return MPFROM2SHORT( drag, drag_op );
}

/* Returns TRUE if the specified window is belong to
   the current process. */
static BOOL
amp_pb_is_app_window( HWND hwnd )
{
  PID pid1, pid2;
  TID tid1, tid2;

  if( WinQueryWindowProcess( hplayer, &pid1, &tid1 ) &&
      WinQueryWindowProcess( hwnd,    &pid2, &tid2 ))
  {
    return pid1 == pid2;
  }

  return FALSE;
}

/* Receives dropped files or playlist records. Must be
   called from the main thread. */
static MRESULT
amp_pb_drag_drop( HWND hwnd, PDRAGINFO pdinfo )
{
  PDRAGITEM pditem;

  char pathname[_MAX_URL];
  char filename[_MAX_URL];
  char fullname[_MAX_URL];
  int  i;

  if( !DrgAccessDraginfo( pdinfo )) {
    return 0;
  }

  for( i = 0; i < pdinfo->cditem; i++ )
  {
    pditem = DrgQueryDragitemPtr( pdinfo, i );

    DrgQueryStrName( pditem->hstrSourceName,    sizeof( filename ), filename );
    DrgQueryStrName( pditem->hstrContainerName, sizeof( pathname ), pathname );
    strcpy( fullname, pathname );
    strcat( fullname, filename );

    if( DrgVerifyRMF( pditem, "DRM_123FILE", NULL ))
    {
      if( pdinfo->cditem == 1 ) {
        if( amp_pb_is_app_window( pdinfo->hwndSource )) {
          WinSendMsg( pdinfo->hwndSource, WM_123FILE_LOAD,
                      MPFROMLONG( pditem->ulItemID ), 0 );
        }
        else
        {
          PID   pid;
          TID   tid;
          PCHAR sharedstr;

          // Because DrgAddStrHandle limits the length of the string to 255
          // characters, here used shared memory and a special message to get
          // an URL the length of which can exceed these limits.
          if( WinQueryWindowProcess( pdinfo->hwndSource, &pid, &tid )) {
            if( DosAllocSharedMem((PVOID*)&sharedstr, NULL, _MAX_URL,
                                   PAG_READ | OBJ_GIVEABLE | PAG_COMMIT ) == NO_ERROR )
            {
              if( DosGiveSharedMem( sharedstr, pid, PAG_WRITE ) == NO_ERROR ) {
                if( WinSendMsg( pdinfo->hwndSource, WM_123FILE_PATHNAME,
                                MPFROMLONG( pditem->ulItemID ), MPFROMP( sharedstr )))
                {
                  strlcpy( fullname, sharedstr, sizeof( fullname ));
                }
              }
              DosFreeMem( sharedstr );
            }
          }

          amp_pb_load_singlefile( fullname, 0 );
        }
        if( pdinfo->usOperation == DO_MOVE ) {
          WinSendMsg( pdinfo->hwndSource, WM_123FILE_REMOVE, MPFROMLONG( pditem->ulItemID ), 0 );
        }
      } else {
        pl_add_file( fullname, NULL, 0 );

        if( pdinfo->usOperation == DO_MOVE ) {
          WinSendMsg( pdinfo->hwndSource, WM_123FILE_REMOVE, MPFROMLONG( pditem->ulItemID ), 0 );
        }
      }
    }
    else  if( DrgVerifyRMF( pditem, "DRM_OS2FILE", NULL ))
    {
      if( pditem->hstrContainerName && pditem->hstrSourceName ) {
        // Have full qualified file name.
        if( DrgVerifyType( pditem, "UniformResourceLocator" )) {
          amp_url_from_file( fullname, fullname, sizeof( fullname ));
        }
        if( is_dir( fullname )) {
          pl_add_directory( fullname, PL_DIR_RECURSIVE );
        } else if( is_playlist( fullname )) {
          pl_load( fullname, 0 );
        } else if( pdinfo->cditem == 1 ) {
          amp_pb_load_singlefile( fullname, 0 );
        } else {
          pl_add_file( fullname, NULL, 0 );
        }

        if( pditem->hwndItem ) {
          // Tell the source you're done.
          DrgSendTransferMsg( pditem->hwndItem, DM_ENDCONVERSATION, (MPARAM)pditem->ulItemID,
                                                                    (MPARAM)DMFL_TARGETSUCCESSFUL );
        }
      }
      else if( pditem->hwndItem &&
               DrgVerifyType( pditem, "UniformResourceLocator" ))
      {
        // The droped item must be rendered.
        PDRAGTRANSFER pdtrans  = DrgAllocDragtransfer(1);
        AMP_DROPINFO* pdsource = (AMP_DROPINFO*)malloc( sizeof( AMP_DROPINFO ));
        char renderto[_MAX_URL];

        if( !pdtrans || !pdsource ) {
          return 0;
        }

        pdsource->cditem   = pdinfo->cditem;
        pdsource->hwndItem = pditem->hwndItem;
        pdsource->ulItemID = pditem->ulItemID;

        pdtrans->cb               = sizeof( DRAGTRANSFER );
        pdtrans->hwndClient       = hwnd;
        pdtrans->pditem           = pditem;
        pdtrans->hstrSelectedRMF  = DrgAddStrHandle( "<DRM_OS2FILE,DRF_TEXT>" );
        pdtrans->hstrRenderToName = 0;
        pdtrans->ulTargetInfo     = (ULONG)pdsource;
        pdtrans->fsReply          = 0;
        pdtrans->usOperation      = pdinfo->usOperation;

        // Send the message before setting a render-to name.
        if( pditem->fsControl & DC_PREPAREITEM ) {
          DrgSendTransferMsg( pditem->hwndItem, DM_RENDERPREPARE, (MPARAM)pdtrans, 0 );
        }

        strlcpy( renderto, startpath , sizeof( renderto ));
        strlcat( renderto, "pm123.dd", sizeof( renderto ));

        pdtrans->hstrRenderToName = DrgAddStrHandle( renderto );

        // Send the message after setting a render-to name.
        if(( pditem->fsControl & ( DC_PREPARE | DC_PREPAREITEM )) == DC_PREPARE ) {
          DrgSendTransferMsg( pditem->hwndItem, DM_RENDERPREPARE, (MPARAM)pdtrans, 0 );
        }

        // Ask the source to render the selected item.
        DrgSendTransferMsg( pditem->hwndItem, DM_RENDER, (MPARAM)pdtrans, 0 );
      }
    }
  }

  DrgDeleteDraginfoStrHandles( pdinfo );
  DrgFreeDraginfo( pdinfo );
  return 0;
}

/* Receives dropped and rendered files and urls. Must be
   called from the main thread. */
static MRESULT
amp_pb_drag_render_done( HWND hwnd, PDRAGTRANSFER pdtrans, USHORT rc )
{
  char rendered[_MAX_URL];
  char fullname[_MAX_URL];

  AMP_DROPINFO* pdsource = (AMP_DROPINFO*)pdtrans->ulTargetInfo;

  // If the rendering was successful, use the file, then delete it.
  if(( rc & DMFL_RENDEROK ) && pdsource &&
       DrgQueryStrName( pdtrans->hstrRenderToName, sizeof( rendered ), rendered ))
  {
    amp_url_from_file( fullname, rendered, sizeof( fullname ));
    DosDelete( rendered );

    if( is_playlist( fullname )) {
      pl_load( fullname, 0 );
    } else if( pdsource->cditem == 1 ) {
      amp_pb_load_singlefile( fullname, 0 );
    } else {
      pl_add_file( fullname, NULL, 0 );
    }
  }

  // Tell the source you're done.
  DrgSendTransferMsg( pdsource->hwndItem, DM_ENDCONVERSATION,
                     (MPARAM)pdsource->ulItemID, (MPARAM)DMFL_TARGETSUCCESSFUL );

  DrgDeleteStrHandle ( pdtrans->hstrSelectedRMF );
  DrgDeleteStrHandle ( pdtrans->hstrRenderToName );
  DrgFreeDragtransfer( pdtrans );
  return 0;
}

/* It is called after successful saving of the meta information.
   Must be called from the main thread. */
static void
amp_pb_refresh_file( AMP_FILE* file )
{
  ASSERT_IS_MAIN_THREAD;

  pl_refresh_file( file->filename, &file->info );

  if( nlstricmp( current_filename, file->filename ) == 0 ) {
    dec_copyinfo( &current_info, &file->info );
    amp_pb_update_play_title();
    amp_invalidate( UPD_FILENAME | UPD_FILEINFO );
  }
}

/* Dispatches requests received from the load queue. */
static void TFNENTRY
amp_load_thread( void* scrap )
{
  ULONG single = TRUE;
  HAB   hab    = WinInitialize( 0 );
  HMQ   hmq    = WinCreateMsgQueue( hab, 0 );

  ULONG request;
  PVOID data;
  ULONG repeat;

  while( qu_read( load_queue, &request, &data ) &&
         !is_terminate )
  {
    char* pathname = data;

    if( single ) {
      if( is_dir( pathname )) {
        pl_clear();
        pl_add_directory( pathname, PL_DIR_RECURSIVE );
      } else {
        amp_load_singlefile( pathname, 0 );
      }
      for( repeat = 0; repeat < 15 && qu_empty( load_queue ); repeat++ ) {
        DosSleep( 32 );
      }
      if( qu_empty( load_queue )) {
        continue;
      }
      if( !is_playlist( pathname ) && !is_dir( pathname )) {
        pl_clear();
        pl_add_file( pathname, NULL, 0 );
      }
      single = FALSE;
    } else {
      if( is_playlist( pathname )) {
        pl_load( pathname, PL_LOAD_NOT_RECALL | PL_LOAD_NOT_COMPLETE );
      } else {
        pl_add_file( pathname, NULL, 0 );
      }
      if( qu_empty( load_queue )) {
        pl_completed();
      }
      for( repeat = 0; repeat < 15 && qu_empty( load_queue ); repeat++ ) {
        DosSleep(32);
      }
      if( qu_empty( load_queue )) {
        single = TRUE;
      }
    }
    free( pathname );
  }

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
  _endthread();
}

/* Write file meta information to pipe. */
static void
amp_write_meta_to_pipe( HPIPE hpipe, const char* filename )
{
  AMP_FILE file = { "", "", sizeof( DECODER_INFO )};
  char* reply;
  int size;

  if( !filename ) {
    WinSendMsg( hplayer, WM_123FILE_CURRENT, MPFROMP( &file ), 0 );
  } else {
    dec_fileinfo( filename, &file.info, NULL, 0 );
  }

  size = ( *file.info.title     ? ( strlen( file.info.title     ) + 7  ) : 0 ) +
         ( *file.info.artist    ? ( strlen( file.info.artist    ) + 8  ) : 0 ) +
         ( *file.info.album     ? ( strlen( file.info.album     ) + 7  ) : 0 ) +
         ( *file.info.year      ? ( strlen( file.info.year      ) + 6  ) : 0 ) +
         ( *file.info.comment   ? ( strlen( file.info.comment   ) + 9  ) : 0 ) +
         ( *file.info.genre     ? ( strlen( file.info.genre     ) + 7  ) : 0 ) +
         ( *file.info.track     ? ( strlen( file.info.track     ) + 7  ) : 0 ) +
         ( *file.info.disc      ? ( strlen( file.info.disc      ) + 6  ) : 0 ) +
         ( *file.info.copyright ? ( strlen( file.info.copyright ) + 11 ) : 0 ) + 128;

  if(( reply = malloc( size )) == NULL ) {
    pipe_write( hpipe, "\n" );
  } else {
   *reply = 0;

    #define ADDTAG( tag, var ) \
      if( *var ) { strlcat( reply, tag,  size ); \
                   strlcat( reply, var, size  ); \
                   strlcat( reply, "\n", size ); }

    ADDTAG( "TITLE=",     file.info.title     );
    ADDTAG( "ARTIST=",    file.info.artist    );
    ADDTAG( "ALBUM=",     file.info.album     );
    ADDTAG( "YEAR=",      file.info.year      );
    ADDTAG( "COMMENT=",   file.info.comment   );
    ADDTAG( "GENRE=",     file.info.genre     );
    ADDTAG( "TRACK=",     file.info.track     );
    ADDTAG( "DISC=",      file.info.disc      );
    ADDTAG( "COPYRIGHT=", file.info.copyright );

    if( file.info.album_gain || file.info.album_peak || file.info.track_gain || file.info.track_peak )
    {
      char buffer[32];

      strlcat( reply, "REPLAYGAIN=", size );

      if( file.info.track_gain ) {
        sprintf( buffer, "%.2f", file.info.track_gain );
        strlcat( reply, buffer, size );
      }
      strlcat( reply, ",", size );

      if( file.info.track_peak ) {
        sprintf( buffer, "%.6f", file.info.track_peak );
        strlcat( reply, buffer, size );
      }
      strlcat( reply, ",", size );

      if( file.info.album_gain ) {
        sprintf( buffer, "%.2f", file.info.album_gain );
        strlcat( reply, buffer, size );
      }
      strlcat( reply, ",", size );

      if( file.info.album_peak ) {
        sprintf( buffer, "%.6f", file.info.album_peak );
        strlcat( reply, buffer, size );
      }
    }

    strlcat( reply, "\n", size );
    pipe_write( hpipe, reply );
    free( reply );
  }

  amp_cleaninfo( &file );
}

/* Write file technical information to pipe. */
static void
amp_write_format_to_pipe( HPIPE hpipe, const char* filename )
{
  AMP_FILE file = { "", "", sizeof( DECODER_INFO )};
  char* reply;
  int size = 1024;

  if( !filename ) {
    WinSendMsg( hplayer, WM_123FILE_CURRENT, MPFROMP( &file ), NULL );
  } else {
    dec_fileinfo( filename, &file.info, file.decoder, 0 );
  }

  if( !*file.decoder || ( reply = malloc( size )) == NULL ) {
    pipe_write( hpipe, "\n" );
  } else {
    snprintf( reply, size, "FILESIZE=%llu (bytes)\n"
                           "SAMPLERATE=%ld\n"
                           "CHANNELS=%ld\n"
                           "TECHINFO=%s\n"
                           "DECODER=%s\n"
                           "SONGLENGTH=%.4f (seconds)\n"
                           "BITRATE=%d (bits/seconds)\n"
                           "\n",
              file.info.filesize,
              file.info.format.samplerate,
              file.info.format.channels,
              file.info.tech_info,
              file.decoder,
              file.info.songlength / 1000.0,
              file.info.bitrate * 1000 );

    pipe_write( hpipe, reply );
    free( reply );
  }

  amp_cleaninfo( &file );
}

/* Dispatches requests received from the pipe. */
static void TFNENTRY
amp_pipe_thread( void* arg )
{
  char  buffer [1024];
  char  command[1024];
  char  result [1024];
  char* zork;
  char* dork;
  HAB   hab   = WinInitialize( 0 );
  HMQ   hmq   = WinCreateMsgQueue( hab, 0 );
  HPIPE hpipe = (HPIPE)arg;

  while( !is_terminate ) {
    DosConnectNPipe( hpipe );

    while( pipe_read( hpipe, buffer, sizeof( buffer )) &&
           !is_terminate )
    {
      blank_strip( buffer );
      DEBUGLOG(( "pm123: receive from pipe %08X command %s.\n", hpipe, buffer ));

      if( *buffer && *buffer != '*' )
      {
        // if( is_dir( buffer )) {
        //   pl_clear();
        //   pl_add_directory( buffer, PL_DIR_RECURSIVE );
        // } else {
        //   amp_load_singlefile( buffer, 0 );
        // }
        qu_write( load_queue, 0, strdup( buffer ));
      }
      else if( *buffer == '*' )
      {
        strcpy( command, buffer + 1 ); // Strip the '*' character.
        blank_strip( command );

        zork = strtok( command, " " );
        dork = strtok( NULL,    ""  );

        if( zork ) {
          if( stricmp( zork, "status" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "file" ) == 0 ) {
                WinSendMsg( hplayer, AMP_QUERY_STRING,
                            MPFROMP( result ), MPFROM2SHORT( sizeof( result ), STR_FILENAME     ));
              } else if( stricmp( dork, "tag"  ) == 0 ) {
                WinSendMsg( hplayer, AMP_QUERY_STRING,
                            MPFROMP( result ), MPFROM2SHORT( sizeof( result ), STR_DISPLAY_TAG  ));
              } else if( stricmp( dork, "info" ) == 0 ) {
                WinSendMsg( hplayer, AMP_QUERY_STRING,
                            MPFROMP( result ), MPFROM2SHORT( sizeof( result ), STR_DISPLAY_INFO ));
              } else if( stricmp( dork, "pos" ) == 0 ) {
                ltoa( time_total(),  result, 10 );
                strcat( result, " " );
                ltoa( time_played(), result + strlen( result ), 10 );
              } else {
                *result = 0;
              }
            } else {
              if( decoder_playing()) {
                if( is_paused()) {
                  strlcpy( result, "paused", sizeof( result ));
                } else {
                  strlcpy( result, "playing", sizeof( result ));
                }
              } else {
                strlcpy( result, "stopped", sizeof( result ));
              }
              if( is_forward()) {
                strlcat( result, " forwarding", sizeof( result ));
              }
              if( is_rewind()) {
                strlcat( result, " rewinding", sizeof( result ));
              }
            }

            pipe_write( hpipe, result );
          }
          else if( stricmp( zork, "size" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "regular" ) == 0 ||
                  stricmp( dork, "0"       ) == 0 ||
                  stricmp( dork, "normal"  ) == 0  )
              {
                WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_SIZE_NORMAL ), 0 );
              }
              if( stricmp( dork, "small"   ) == 0 ||
                  stricmp( dork, "1"       ) == 0  )
              {
                WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_SIZE_SMALL  ), 0 );
              }
              if( stricmp( dork, "tiny"    ) == 0 ||
                  stricmp( dork, "2"       ) == 0  )
              {
                WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_SIZE_TINY   ), 0 );
              }
            }
          }
          else if( stricmp( zork, "rdir" ) == 0 )
          {
            if( dork ) {
              pl_add_directory( dork, PL_DIR_RECURSIVE );
            }
          }
          else if( stricmp( zork, "dir"  ) == 0 )
          {
            if( dork ) {
              pl_add_directory( dork, 0 );
            }
          }
          else if( stricmp( zork, "font" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "1" ) == 0 ) {
                WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_FONT_SET1 ), 0 );
              }
              if( stricmp( dork, "2" ) == 0 ) {
                WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_FONT_SET2 ), 0 );
              }
            }
          }
          else if( stricmp( zork, "add" ) == 0 )
          {
            char* file;

            if( dork ) {
              while( *dork ) {
                file = dork;
                while( *dork && *dork != ';' ) {
                  ++dork;
                }
                if( *dork == ';' ) {
                  *dork++ = 0;
                }
                if( is_playlist( file )) {
                  pl_load( file, PL_LOAD_NOT_RECALL | PL_LOAD_NOT_COMPLETE );
                } else {
                  pl_add_file( file, NULL, 0 );
                }
              }
            }
          }
          else if( stricmp( zork, "load" ) == 0 )
          {
            if( dork ) {
              amp_load_singlefile( dork, 0 );
            }
          }
          else if( stricmp( zork, "hide"  ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_MINIMIZE ), 0 );
          }
          else if( stricmp( zork, "float" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "off" ) == 0 || stricmp( dork, "0" ) == 0 ) {
                if( cfg.floatontop ) {
                  WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_FLOAT ), 0 );
                }
              }
              if( stricmp( dork, "on"  ) == 0 || stricmp( dork, "1" ) == 0 ) {
                if( !cfg.floatontop ) {
                  WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( IDM_M_FLOAT ), 0 );
                }
              }
            }
          }
          else if( stricmp( zork, "use" ) == 0 )
          {
            amp_pl_use( TRUE );
          }
          else if( stricmp( zork, "clear" ) == 0 )
          {
            pl_clear();
          }
          else if( stricmp( zork, "next" ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_NEXT ), 0 );
          }
          else if( stricmp( zork, "previous" ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_PREV ), 0 );
          }
          else if( stricmp( zork, "remove" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "dead" ) == 0 ) {
                pl_remove_records( PL_REMOVE_DEAD );
              } else if( stricmp( dork, "duplicate" ) == 0 ) {
                pl_remove_records( PL_REMOVE_DUPLICATE );
              }
            } else {
              pl_remove_records( PL_REMOVE_LOADED );
            }
          }
          else if( stricmp( zork, "forward" ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_FWD  ), 0 );
          }
          else if( stricmp( zork, "rewind" ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_REW  ), 0 );
          }
          else if( stricmp( zork, "stop" ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_STOP ), 0 );
          }
          else if( stricmp( zork, "jump" ) == 0 )
          {
            if( dork && decoder_playing() && time_total() > 0 ) {
              WinSendMsg( hplayer, AMP_PB_SEEK, MPFROMLONG( atoi( dork ) * 1000 ), 0 );
            }
          }
          else if( stricmp( zork, "play" ) == 0 )
          {
            if( dork ) {
              amp_load_singlefile( dork, AMP_LOAD_PLAY );
            } else if( !decoder_playing()) {
              WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_PLAY ), 0 );
            }
          }
          else if( stricmp( zork, "pause" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "off" ) == 0 || stricmp( dork, "0" ) == 0 ) {
                if( is_paused()) {
                  WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_PAUSE ), 0 );
                }
              }
              if( stricmp( dork, "on"  ) == 0 || stricmp( dork, "1" ) == 0 ) {
                if( !is_paused()) {
                  WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_PAUSE ), 0 );
                }
              }
            } else {
              WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_PAUSE ), 0 );
            }
          }
          else if( stricmp( zork, "playonload" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "off" ) == 0 || stricmp( dork, "0" ) == 0 ) {
                cfg.playonload = FALSE;
              }
              if( stricmp( dork, "on"  ) == 0 || stricmp( dork, "1" ) == 0 ) {
                cfg.playonload = TRUE;
              }
            }
          }
          else if( stricmp( zork, "autouse" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "off" ) == 0 || stricmp( dork, "0" ) == 0 ) {
                cfg.autouse = FALSE;
              }
              if( stricmp( dork, "on"  ) == 0 || stricmp( dork, "1" ) == 0 ) {
                cfg.autouse = TRUE;
              }
            }
          }
          else if( stricmp( zork, "playonuse" ) == 0 )
          {
            if( dork ) {
              if( stricmp( dork, "off" ) == 0 || stricmp( dork, "0" ) == 0 ) {
                cfg.playonuse = FALSE;
              }
              if( stricmp( dork, "on"  ) == 0 || stricmp( dork, "1" ) == 0 ) {
                cfg.playonuse = TRUE;
              }
            }
          }
          else if( stricmp( zork, "repeat"  ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_REPEAT  ), 0 );
          }
          else if( stricmp( zork, "shuffle" ) == 0 )
          {
            WinSendMsg( hplayer, WM_COMMAND, MPFROMSHORT( BMP_SHUFFLE ), 0 );
          }
          else if( stricmp( zork, "volume" ) == 0 )
          {
            if( dork )
            {
              int volume = cfg.defaultvol;

              if( *dork == '+' ) {
                volume += atoi( dork + 1 );
              } else if( *dork == '-' ) {
                volume -= atoi( dork + 1 );
              } else {
                volume  = atoi( dork );
              }

              WinSendMsg( hplayer, AMP_PB_VOLUME, MPFROMLONG( volume ), 0 );
            }
            pipe_write( hpipe, itoa( cfg.defaultvol, result, 10 ));
          }
          else if( stricmp( zork, "info" ) == 0 )
          {
            if( dork )
            {
              char* filename;

              dork     = strtok( dork, " " );
              filename = strtok( NULL, ""  );

              if( stricmp( dork, "meta" ) == 0 ) {
                amp_write_meta_to_pipe( hpipe, filename );
              } else if( stricmp( dork, "format" ) == 0 ) {
                amp_write_format_to_pipe( hpipe, filename );
              }
            }
          }
        }
      }
    }
    DosDisConnectNPipe( hpipe );
  }

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
  _endthread();
}

/* Loads a file selected by the user to the player. Must be
   called from the main thread. */
static void
amp_pb_load_file( HWND owner )
{
  FILEDLG filedialog;

  char  type_audio[2048];
  char  type_all  [2048];
  APSZ  types[4] = {{ 0 }};

  ASSERT_IS_MAIN_THREAD;

  types[0][0] = type_audio;
  types[1][0] = FDT_PLAYLIST;
  types[2][0] = type_all;

  memset( &filedialog, 0, sizeof( FILEDLG ));
  filedialog.cbSize     = sizeof( FILEDLG );
  filedialog.fl         = FDS_CENTER | FDS_OPEN_DIALOG;
  filedialog.pszTitle   = "Load file";

  // WinFileDlg returns error if a length of the pszIType string is above
  // 255 characters. Therefore the small part from the full filter is used
  // as initial extended-attribute type filter. This part has enough to
  // find the full filter in the papszITypeList.
  filedialog.papszITypeList = types;
  filedialog.pszIType       = FDT_AUDIO_ALL;

  strcpy( type_audio, FDT_AUDIO     );
  dec_fill_types( type_audio + strlen( type_audio ),
                  sizeof( type_audio ) - strlen( type_audio ) - 1 );
  strcat( type_audio, ")" );

  strcpy( type_all, FDT_AUDIO_ALL );
  dec_fill_types( type_all + strlen( type_all ),
                  sizeof( type_all ) - strlen( type_all ) - 1 );
  strcat( type_all, ")" );

  DEBUGLOG(( "pm123: %s\n", type_audio ));
  DEBUGLOG(( "pm123: %s\n", type_all   ));

  strcpy( filedialog.szFullFile, cfg.filedir );
  amp_file_dlg( HWND_DESKTOP, owner, &filedialog );

  if( filedialog.lReturn == DID_OK ) {
    sdrivedir( cfg.filedir, filedialog.szFullFile, sizeof( cfg.filedir ));
    amp_pb_load_singlefile( filedialog.szFullFile, 0 );
  }
}

/* Loads a skin selected by the user. Must be called from
   the main thread. */
static void
amp_pb_load_skin( HAB hab, HWND owner, HPS hps )
{
  FILEDLG filedialog;
  APSZ types[] = {{ FDT_SKIN }, { 0 }};

  ASSERT_IS_MAIN_THREAD;
  memset( &filedialog, 0, sizeof( FILEDLG ));

  filedialog.cbSize         = sizeof( FILEDLG );
  filedialog.fl             = FDS_CENTER | FDS_OPEN_DIALOG;
  filedialog.pszTitle       = "Load PM123 skin";
  filedialog.papszITypeList = types;
  filedialog.pszIType       = FDT_SKIN;

  sdrivedir( filedialog.szFullFile, cfg.defskin, sizeof( filedialog.szFullFile ));
  amp_file_dlg( HWND_DESKTOP, HWND_DESKTOP, &filedialog );

  if( filedialog.lReturn == DID_OK ) {
    bmp_load_skin( filedialog.szFullFile, hab, owner, hps );
    strcpy( cfg.defskin, filedialog.szFullFile );
  }
}

/* Returns TRUE if the save stream feature has been enabled.
   Must be called from the main thread. */
static BOOL
amp_pb_save_stream( HWND hwnd, BOOL enable )
{
  ASSERT_IS_MAIN_THREAD;

  if( enable )
  {
    FILEDLG filedialog;

    memset( &filedialog, 0, sizeof( FILEDLG ));
    filedialog.cbSize     = sizeof( FILEDLG );
    filedialog.fl         = FDS_CENTER | FDS_SAVEAS_DIALOG | FDS_ENABLEFILELB;
    filedialog.pszTitle   = "Save stream as";

    strcpy( filedialog.szFullFile, cfg.savedir );
    amp_file_dlg( HWND_DESKTOP, hwnd, &filedialog );

    if( filedialog.lReturn == DID_OK ) {
      if( amp_warn_if_overwrite( hwnd, filedialog.szFullFile ))
      {
        msg_savestream( filedialog.szFullFile );
        sdrivedir( cfg.savedir, filedialog.szFullFile, sizeof( cfg.savedir ));
        return TRUE;
      }
    }
  } else {
    msg_savestream( NULL );
  }

  return FALSE;
}

/* Starts playing a next file or stops the player if all files
   already played. Must be called from the main thread. */
static void
amp_pb_playstop( void )
{
  BOOL play = FALSE;
  ASSERT_IS_MAIN_THREAD;

  if( !cfg.continuous || is_always_hungry()) {
    amp_pb_stop();
  }

  if( amp_playmode == AMP_SINGLE && cfg.rpt ) {
    play = TRUE;
  } else if ( amp_playmode == AMP_PLAYLIST  ) {
    if( pl_load_next_record()) {
      play = TRUE;
    } else {
      pl_clean_shuffle();
      if( pl_load_first_record() && cfg.rpt ) {
        play = TRUE;
      }
    }
  }

  if( cfg.continuous ) {
    if( play ) {
      amp_pb_play_ex( 0, MSG_PLAY_CONTINUOUS );
    } else {
      amp_pb_stop_ex( MSG_STOP_AFTER_END );
    }
  } else {
    if( play ) {
      amp_pb_play();
    }
  }
}

/* Stops the playing of the current file or continue play a
   next file. Must be called from the main thread. */
static void
amp_pb_playerror( void )
{
  BOOL play = FALSE;
  ASSERT_IS_MAIN_THREAD;

  if( !cfg.continuous || is_always_hungry()) {
    amp_pb_stop();
  }

  if( amp_playmode == AMP_PLAYLIST ) {
    pl_mark_as_grey();
    if( cfg.skip_badfiles ) {
      if( pl_load_next_record()) {
        play = TRUE;
      } else {
        pl_clean_shuffle();
        if( pl_load_first_record() && cfg.rpt ) {
          play = TRUE;
        }
      }
    }
  }

  if( cfg.continuous ) {
    if( play ) {
      amp_pb_play_ex( 0, MSG_PLAY_CONTINUOUS );
    } else {
      amp_pb_stop_ex( MSG_STOP_AFTER_END );
    }
  } else {
    if( play ) {
      amp_pb_play();
    }
  }
}

/* Creates and displays an error message window. Use the player
   window as message window owner. Must be called from the
   main thread. */
static void
amp_pb_show_error( const char* message )
{
  ASSERT_IS_MAIN_THREAD;
  multimsgbox( hframe, message, MB_ERROR );
}

/* Creates and displays an information message window. Use the player
   window as message window owner. Must be called from the
   main thread. */
static void
amp_pb_show_info( const char* message )
{
  ASSERT_IS_MAIN_THREAD;
  multimsgbox( hframe, message, MB_INFORMATION );
}

/* Processes messages of the player client window.
   Must be called from the main thread. */
static MRESULT EXPENTRY
amp_pb_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case WM_FOCUSCHANGE:
    {
      HPS hps = WinGetPS( hwnd );
      is_have_focus = SHORT1FROMMP( mp2 );
      bmp_draw_led( hps, is_have_focus  );
      WinReleasePS( hps );
      break;
    }

    case 0x041E:
      if( !is_have_focus ) {
        HPS hps = WinGetPS( hwnd );
        bmp_draw_led( hps, TRUE);
        WinReleasePS( hps );
      }
      return 0;

    case 0x041F:
      if( !is_have_focus ) {
        HPS hps = WinGetPS( hwnd );
        bmp_draw_led( hps, FALSE );
        WinReleasePS( hps );
      }
      if( hpopup ) {
        amp_pb_hide_popup();
      } else {
        WinStopTimer( hab, hwnd, TID_POPUP );
      }
      return 0;

    case AMP_PB_STOP:
      return MRFROMLONG( amp_pb_stop());
    case AMP_PB_PLAY:
      return MRFROMLONG( amp_pb_play_ex( LONGFROMMP( mp1 ), 0 ));
    case AMP_PB_FORWARD:
      return MRFROMLONG( amp_pb_forward());
    case AMP_PB_REWIND:
      return MRFROMLONG( amp_pb_rewind());
    case AMP_PB_RESET:
      return MRFROMLONG( amp_pb_reset());
    case AMP_PB_PAUSE:
      return MRFROMLONG( amp_pb_pause());
    case AMP_PB_USE:
      return MRFROMLONG( amp_pb_use( LONGFROMMP( mp1 )));
    case AMP_PB_LOAD_SINGLEFILE:
      return MRFROMLONG( amp_pb_load_singlefile( mp1, LONGFROMMP( mp2 )));
    case AMP_PB_LOAD_URL:
      return MRFROMLONG( amp_pb_load_url( HWNDFROMMP( mp1 ), LONGFROMMP( mp2 )));
    case AMP_PB_LOAD_TRACK:
      return MRFROMLONG( amp_pb_load_track( HWNDFROMMP( mp1 ), LONGFROMMP( mp2 )));
    case AMP_PB_LOAD_RECORD:
      return MRFROMLONG( amp_pb_load_record( LONGFROMMP( mp1 ), LONGFROMMP( mp2 )));
    case AMP_PB_VOLUME:
      return MRFROMLONG( amp_pb_set_volume( LONGFROMMP( mp1 )));
    case AMP_PB_MUTE:
      return MRFROMLONG( amp_pb_mute( LONGFROMMP( mp1 )));
    case AMP_PB_SEEK:
      return MRFROMLONG( amp_pb_seek( LONGFROMMP( mp1 )));
    case AMP_PB_NEXT:
      return MRFROMLONG( amp_pb_next());
    case AMP_PB_PREVIOUS:
      return MRFROMLONG( amp_pb_previous());
    case AMP_PB_REPEAT:
      return MRFROMLONG( amp_pb_repeat( LONGFROMMP( mp1  )));
    case AMP_PB_SHUFFLE:
      return MRFROMLONG( amp_pb_shuffle( LONGFROMMP( mp1  )));

    case AMP_DISPLAY_MESSAGE:
      if( mp1 ) {
        if( mp2 ) {
          amp_pb_show_error( mp1 );
        } else {
          amp_pb_show_info ( mp1 );
        }
        free( mp1 );
      }
      return 0;

    case AMP_DISPLAY_MODE:
      if( cfg.viewmode == CFG_DISP_FILEINFO ) {
        cfg.viewmode = CFG_DISP_FILENAME;
      } else {
        cfg.viewmode++;
      }

      amp_invalidate( UPD_FILEINFO | UPD_FILENAME );
      return 0;

    case AMP_QUERY_STRING:
    {
      char*  buffer = (char*)mp1;
      USHORT size   = SHORT1FROMMP(mp2);
      USHORT type   = SHORT2FROMMP(mp2);

      switch( type )
      {
        case STR_NULL:
         *buffer = 0;
          break;
        case STR_VERSION:
          strlcpy( buffer, AMP_FULLNAME, size );
          break;
        case STR_DISPLAY_TEXT:
          strlcpy( buffer, bmp_query_text(), size );
          break;
        case STR_DISPLAY_TAG:
          amp_pb_construct_tag_string( buffer, &current_info, current_filename, size );
          break;
        case STR_DISPLAY_INFO:
          strlcpy( buffer, current_info.tech_info, size );
          break;
        case STR_FILENAME:
          strlcpy( buffer, current_filename, size );
          break;
        default:
          break;
      }
      return 0;
    }

    case WM_123FILE_REFRESH:
      amp_pb_refresh_file( mp1 );
      return 0;

    case WM_123FILE_CURRENT:
      amp_pb_current_file( mp1 );
      return 0;

    case WM_CONTEXTMENU:
      amp_pb_show_context_menu( hwnd );
      return 0;

    case WM_SEEKSTOP:
      DEBUGLOG(( "pm123: receive WM_SEEKSTOP message.\n" ));
      if( is_seeking ) {
        msg_trash_buffers( seeking_pos * 1000 );
      }
      is_seeking = FALSE;
      return 0;

    case WM_CHANGEBR:
      if( mp1 ) {
        current_info.bitrate = (int)mp1;
      }
      return 0;

    // Posted by decoder
    case WM_PLAYERROR:
      if( !is_stopping ) {
        DEBUGLOG(( "pm123: receive WM_PLAYERROR message.\n" ));
        if( dec_status() == DECODER_STOPPED ||
            dec_status() == DECODER_ERROR   || !out_playing_data())
        {
          amp_pb_playerror();
        }
      } else {
        DEBUGLOG(( "pm123: discard WM_PLAYERROR message.\n" ));
      }
      return 0;

    // Posted by decoder
    case WM_PLAYSTOP:
      // The decoder has finished the work, but we should wait until output
      // buffers will become empty.
      if( !is_stopping )
      {
        QMSG qms;

        if( !WinPeekMsg( hab, &qms, hplayer, WM_PLAYERROR, WM_PLAYERROR, PM_NOREMOVE )) {
          DEBUGLOG(( "pm123: receive WM_PLAYSTOP message.\n" ));

          // If output is always hungry, WM_OUTPUT_OUTOFDATA will not be
          // posted again so we go there by our selves.
          if( is_always_hungry()) {
            amp_pb_playstop();
            return 0;
          }
        }
      }
      DEBUGLOG(( "pm123: discard WM_PLAYSTOP message.\n" ));
      return 0;

    // Posted by output
    case WM_OUTPUT_OUTOFDATA:
      if( !is_stopping )
      {
        ULONG status = dec_status();
        QMSG qms;

        if( !WinPeekMsg( hab, &qms, hplayer, WM_PLAYERROR, WM_PLAYERROR, PM_NOREMOVE )) {
          DEBUGLOG(( "pm123: receive WM_OUTPUT_OUTOFDATA [%d] message, dec_status=%d, out_playing_data=%d\n",
                      LONGFROMMP(mp1), status, out_playing_data()));

          // The fade plug-in always returns TRUE as result of the
          // out_playing_data. Added special case for it.
          if( status == DECODER_STOPPED &&
            ( cfg.continuous || !out_playing_data() || out_is_active( "fade" )))
          {
            amp_pb_playstop();
            return 0;
          } else if( status == DECODER_ERROR ) {
            amp_pb_playerror();
            return 0;
          }
        }
      }
      DEBUGLOG(( "pm123: discard WM_OUTPUT_OUTOFDATA message.\n" ));
      return 0;

    case DM_DRAGOVER:
      return amp_pb_drag_over( hwnd, (PDRAGINFO)mp1 );
    case DM_DROP:
      return amp_pb_drag_drop( hwnd, (PDRAGINFO)mp1 );
    case DM_RENDERCOMPLETE:
      return amp_pb_drag_render_done( hwnd, (PDRAGTRANSFER)mp1, SHORT1FROMMP( mp2 ));

    case WM_TIMER:
      if( LONGFROMMP(mp1) == TID_ONTOP ) {
        WinSetWindowPos( hframe, HWND_TOP, 0, 0, 0, 0, SWP_ZORDER );
      }

      if( LONGFROMMP(mp1) == TID_UPDATE_PLAYER )
      {
        HPS hps = WinGetPS( hwnd );

        if( bmp_scroll_text()) {
          bmp_draw_text( hps );
        }

        if( upd_options ) {
          WinPostMsg( hwnd, AMP_PAINT, MPFROMLONG( upd_options ), 0 );
          upd_options = 0;
        }

        WinReleasePS( hps );
      }

      if( LONGFROMMP(mp1) == TID_UPDATE_TIMERS && decoder_playing()) {
        if( time_played() && cfg.mode == CFG_MODE_REGULAR )
        {
          HPS hps = WinGetPS( hwnd );
          amp_pb_paint_timers( hps );
          WinReleasePS( hps );
        }

        upnp_notify_state_change( UPNP_N_TIMERS );
      }

      if( LONGFROMMP(mp1) == TID_UPDATE_BITRATE && decoder_playing())
      {
        static int bitrate = 0;
        if( bitrate != current_info.bitrate )
        {
          HPS hps = WinGetPS( hwnd );
          bitrate = current_info.bitrate;
          bmp_draw_rate( hps, bitrate );
          WinReleasePS( hps );
        }
      }

      if( LONGFROMMP(mp1) == TID_NOTIFY_UPNP ) {
        upnp_notify_state_change( UPNP_N_NOTIFY );
      }

      if( LONGFROMMP(mp1) == TID_POPUP )
      {
        WinStopTimer( hab, hwnd, TID_POPUP );
        if( !amp_pb_show_popup()) {
          WinStartTimer( hab, hwnd, TID_POPUP, 2000 );
        }
      }

      return 0;

    case WM_ERASEBACKGROUND:
      return 0;

    case WM_REALIZEPALETTE:
      vis_broadcast( msg, mp1, mp2 );
      break;

    case AMP_PAINT:
    {
      HPS hps     = WinGetPS( hwnd );
      int options = LONGFROMMP( mp1 );

      if( options & UPD_SCROLLSPEED ) {
        WinStartTimer( hab, hwnd, TID_UPDATE_PLAYER,  cfg.double_speed ? 25 : 50 );
      }
      if( options & UPD_FILENAME ) {
        amp_pb_display_filename();
      }
      if( options & UPD_TIMERS   ) {
        amp_pb_paint_timers( hps );
      }
      if( options & UPD_FILEINFO ) {
        amp_pb_paint_fileinfo( hps );
      }
      if( options & UPD_VOLUME   ) {
        bmp_draw_volume( hps, cfg.defaultvol );
      }
      if( options & UPD_SLIDER   ) {
        amp_pb_paint_slider( hps );
      }

      WinReleasePS( hps );
      return 0;
    }

    case WM_PAINT:
    {
      HPS hps = WinBeginPaint( hwnd, NULLHANDLE, NULL );
      bmp_draw_background( hps, hwnd );
      amp_pb_paint_fileinfo( hps );
      amp_pb_paint_timers( hps );
      bmp_draw_led( hps, is_have_focus );
      bmp_draw_volume( hps, cfg.defaultvol );

      WinEndPaint( hps );
      return 0;
    }

    case WM_METADATA:
    {
      char* metadata = PVOIDFROMMP(mp1);
      char* pos;
      int   flg = 0;
      int   i;
      char  teststring[512] = "";
      int   codepage;

      DEBUGLOG(( "pm123: receive WM_METADATA message: '%s'\n", metadata ));

      if( metadata && !is_cuesheet( current_filename )) {
        if(( pos = strstr( metadata, "StreamTitle='" )) != NULL )
        {
          pos += 13;
          flg |= DECODER_HAVE_TITLE;

          for( i = 0; i < sizeof( current_info.title ) - 1 && *pos
                          && ( pos[0] != '\'' || pos[1] != ';' ); i++ )
          {
            current_info.title[i] = *pos++;
          }

          current_info.title[i] = 0;
          strlcat( teststring, current_info.title, sizeof( teststring ));
        }
        if(( pos = strstr( metadata, "StreamAlbum='" )) != NULL )
        {
          pos += 13;
          flg |= DECODER_HAVE_ALBUM;

          for( i = 0; i < sizeof( current_info.album ) - 1 && *pos
                      && ( pos[0] != '\'' || pos[1] != ';' ); i++ )
          {
            current_info.album[i] = *pos++;
          }

          current_info.album[i] = 0;
          strlcat( teststring, current_info.album, sizeof( teststring ));
        }
        if(( pos = strstr( metadata, "StreamArtist='" )) != NULL )
        {
          pos += 14;
          flg |= DECODER_HAVE_ARTIST;

          for( i = 0; i < sizeof( current_info.artist ) - 1 && *pos
                      && ( pos[0] != '\'' || pos[1] != ';' ); i++ )
          {
            current_info.artist[i] = *pos++;
          }

          current_info.artist[i] = 0;
          strlcat( teststring, current_info.artist, sizeof( teststring ));
        }

        codepage = ch_detect( cfg.tags_charset, teststring );
        if( codepage != ch_default()) {
          if( flg & DECODER_HAVE_TITLE  ) {
            ch_convert( codepage, current_info.title,  CH_DEFAULT,
                                  current_info.title,  sizeof( current_info.title  ));
          }
          if( flg & DECODER_HAVE_ARTIST ) {
            ch_convert( codepage, current_info.artist, CH_DEFAULT,
                                  current_info.artist, sizeof( current_info.artist ));
          }
          if( flg & DECODER_HAVE_ALBUM  ) {
            ch_convert( codepage, current_info.album,  CH_DEFAULT,
                                  current_info.album,  sizeof( current_info.album  ));
          }
        }
        if( flg )
        {
          AMP_FILE file = { "", "", sizeof( DECODER_INFO )};

          pl_refresh_file( current_filename, &current_info );
          amp_invalidate( UPD_FILEINFO | UPD_FILENAME );
          amp_pb_update_play_title();
          amp_pb_current_file( &file );
          bn_show( &file );
          amp_cleaninfo( &file );
        }
      }
      return 0;
    }

    case WM_COMMAND:
      if( COMMANDMSG(&msg)->source == CMDSRC_MENU ) {
        if( COMMANDMSG(&msg)->cmd > IDM_M_DISCS ) {
          amp_pb_load_disc( COMMANDMSG(&msg)->cmd - IDM_M_DISCS - 1 );
          return 0;
        }
        if( COMMANDMSG(&msg)->cmd > IDM_M_PLUGINS ) {
          pg_process_plugin_menu( hwnd, mn_get_submenu( hmenu, IDM_M_PLUGINS  ),
                                                        COMMANDMSG(&msg)->cmd );
          return 0;
        }
        if( COMMANDMSG(&msg)->cmd > IDM_M_BOOKMARKS ) {
          bm_use_bookmark_from_menu( COMMANDMSG(&msg)->cmd );
          return 0;
        }
        if( COMMANDMSG(&msg)->cmd > IDM_M_LOAD_LAST )
        {
          char filename[_MAX_URL];
          int  i = COMMANDMSG(&msg)->cmd - IDM_M_LOAD_LAST - 1;

          strcpy( filename, cfg.last_file[i] );
          amp_pb_load_singlefile( filename, 0 );
          return 0;
        }
      }

      switch( COMMANDMSG(&msg)->cmd ) {
        case IDM_M_MANAGER:
          pm_show( TRUE );
          return 0;

        case IDM_M_BM_ADD:
          if( !is_busy() && *current_filename )
          {
            AMP_FILE file = { "", "", sizeof( DECODER_INFO )};

            amp_pb_current_file( &file );
            bm_add_bookmark( hwnd, &file, time_total() ? out_playing_pos() : 0 );
            amp_cleaninfo( &file );
          }
          return 0;

        case IDM_M_BM_EDIT:
          bm_show( TRUE );
          return 0;

        case IDM_M_EDIT:
          if( !is_busy())
          {
            AMP_FILE file = { "", "", sizeof( DECODER_INFO )};

            amp_pb_current_file( &file );
            tag_edit( hwnd, &file, 1 );
            amp_cleaninfo( &file );
          }
          return 0;

        case IDM_M_OPEN_FOLDER:
          amp_open_containing_folder( current_filename );
          return 0;

        case IDM_M_SAVE_STREAM:
          if( !is_busy()) {
            amp_pb_save_stream( hwnd, !is_stream_saved());
          }
          return 0;

        case IDM_M_EQUALIZE:
          eq_show( TRUE );
          return 0;

        case BMP_EQ:
          eq_show( !eq_is_visible());
          return 0;

        case IDM_M_FLOAT:
          cfg.floatontop = !cfg.floatontop;

          if( !cfg.floatontop ) {
            WinStopTimer ( hab, hwnd, TID_ONTOP );
          } else {
            WinStartTimer( hab, hwnd, TID_ONTOP, 100 );
          }
          return 0;

        case IDM_M_LOAD_URL:
          if( !is_busy()) {
            amp_pb_load_url( hwnd, URL_ADD_TO_PLAYER );
          }
          return 0;

        case IDM_M_LOAD_TRACK:
          if( !is_busy()) {
            amp_pb_load_track( hwnd, TRK_ADD_TO_PLAYER );
          }
          return 0;

        case IDM_M_LOAD_DISC:
          if( !is_busy()) {
            if( *cfg.cddrive ) {
              amp_pb_load_disc( toupper( cfg.cddrive[0] ) - 'A' );
            } else {
              CD_INFO cdinfo;
              if( amp_pb_cd_info( &cdinfo ) > 0 ) {
                amp_pb_load_disc( cdinfo.first );
              }
            }
          }
          return 0;

        case IDM_M_FONT_SET1:
          cfg.font = 0;
          bmp_refresh_font();
          amp_invalidate( UPD_FILEINFO );
          return 0;

        case IDM_M_FONT_SET2:
          cfg.font = 1;
          bmp_refresh_font();
          amp_invalidate( UPD_FILEINFO );
          return 0;

        case IDM_M_SIZE_TINY:
          cfg.mode = CFG_MODE_TINY;
          bmp_reflow_and_resize( hframe );
          return 0;

        case IDM_M_SIZE_NORMAL:
          cfg.mode = CFG_MODE_REGULAR;
          bmp_reflow_and_resize( hframe );
          return 0;

        case IDM_M_SIZE_SMALL:
          cfg.mode = CFG_MODE_SMALL;
          bmp_reflow_and_resize( hframe );
          return 0;

        case IDM_M_MINIMIZE:
        case BMP_HIDE:
          WinSetWindowPos( hframe, HWND_DESKTOP, 0, 0, 0, 0, SWP_HIDE );
          WinSetActiveWindow( HWND_DESKTOP, WinQueryWindow( hwnd, QW_NEXTTOP ));
          return 0;

        case IDM_M_LOAD_SKIN:
        {
          HPS hps = WinGetPS( hwnd );
          amp_pb_load_skin( hab, hwnd, hps );
          WinReleasePS( hps );
          return 0;
        }

        case IDM_M_LOAD_FILE:
          if( !is_busy()) {
            amp_pb_load_file( hwnd );
          }
          return 0;

        case IDM_M_LOAD_CLEAR:
        {
          int i;
          for( i = 0; i < MAX_RECALL; i++ ) {
            cfg.last_file[i][0] = 0;
          }
          return 0;
        }

        case IDM_M_PROPERTIES:
          if( !is_busy()) {
            cfg_properties( hwnd );

            if( cfg.dock_windows ) {
              dk_arrange( hframe );
            } else {
              dk_cleanup( hframe );
            }
          }
          return 0;

        case IDM_M_PLAYLIST:
          pl_show( TRUE );
          return 0;

        case IDM_M_VOL_RAISE:
          amp_pb_set_volume( cfg.defaultvol + 5 );
          return 0;

        case IDM_M_VOL_LOWER:
          amp_pb_set_volume( cfg.defaultvol - 5 );
          return 0;

        case IDM_M_MUTE:
          amp_pb_mute( !cfg.mute );
          return 0;

        case BMP_PL:
          pl_show( !pl_is_visible());
          return 0;

        case BMP_REPEAT:
          amp_pb_repeat( !cfg.rpt );
          return 0;

        case BMP_SHUFFLE:
          amp_pb_shuffle( !cfg.shf );
          return 0;

        case IDM_M_QUIT:
        case BMP_POWER:
          WinPostMsg( hwnd, WM_QUIT, 0, 0 );
          return 0;

        case IDM_M_PLAY:
        case BMP_PLAY:
          if( !is_busy()) {
            if( !decoder_playing()) {
              amp_pb_play();
            } else {
              WinSendMsg( hwnd, WM_COMMAND, MPFROMSHORT( BMP_STOP ), mp2 );
            }
          }
          return 0;

        case IDM_M_PAUSE:
        case BMP_PAUSE:
          if( !is_busy()) {
            amp_pb_pause();
          }
          return 0;

        case BMP_FLOAD:
          if( !is_busy()) {
            amp_pb_load_file( hwnd );
          }
          return 0;

        case BMP_STOP:
          if( !is_busy()) {
            if( amp_pb_stop()) {
              pl_clean_shuffle();
            }
          }
          return 0;

        case IDM_M_NEXT:
        case BMP_NEXT:
          if( !is_busy()) {
            amp_pb_next();
          }
          return 0;

        case IDM_M_PREV:
        case BMP_PREV:
          if( !is_busy()) {
            amp_pb_previous();
          }
          return 0;

        case IDM_M_FWD:
        case BMP_FWD:
          if( !is_busy()) {
            amp_pb_forward();
          }
          return 0;

        case IDM_M_REW:
        case BMP_REW:
          if( !is_busy()) {
            amp_pb_rewind();
          }
          return 0;

        case IDM_M_FWD_5SEC:
          amp_pb_seek( out_playing_pos() + 5000 );
          return 0;

        case IDM_M_REW_5SEC:
          amp_pb_seek( out_playing_pos() - 5000 );
          return 0;
      }
      return 0;

    case WM_CREATE:
    {
      HPS hps = WinGetPS( hwnd );
      bmp_load_skin( cfg.defskin, hab, hwnd, hps );
      WinReleasePS( hps );

      if( cfg.floatontop ) {
        WinStartTimer( hab, hwnd, TID_ONTOP, 100 );
      }

      if( cfg.rpt ) {
        WinSendDlgItemMsg( hwnd, BMP_REPEAT,  WM_PRESS, 0, 0 );
      }
      if( cfg.shf || is_arg_shuffle ) {
        WinSendDlgItemMsg( hwnd, BMP_SHUFFLE, WM_PRESS, 0, 0 );
        cfg.shf = TRUE;
      }

      WinStartTimer( hab, hwnd, TID_UPDATE_TIMERS,  100 );
      WinStartTimer( hab, hwnd, TID_UPDATE_PLAYER,  cfg.double_speed ? 25 : 50 );
      WinStartTimer( hab, hwnd, TID_UPDATE_BITRATE, 200 );
      WinStartTimer( hab, hwnd, TID_NOTIFY_UPNP,    500 );
      break;
    }

    case WM_BUTTON1DOWN:
    {
      POINTL pos;

      pos.x = (short)SHORT1FROMMP(mp1);
      pos.y = (short)SHORT2FROMMP(mp1);

      if( bmp_pt_in_volume( pos )) {
        is_volume_drag = TRUE;
        WinSetCapture( HWND_DESKTOP, hwnd );
        amp_pb_set_volume( bmp_calc_volume( pos ));
      } else if( bmp_pt_in_slider( pos ) && decoder_playing() && time_total() > 0 ) {
        is_slider_drag = TRUE;
        is_seeking     = TRUE;
        WinSetCapture( HWND_DESKTOP, hwnd );
        seeking_pos = bmp_calc_time( pos, time_total());
        amp_invalidate( UPD_SLIDER );
      } else if( !is_busy() && amp_playmode != AMP_NOFILE && bmp_pt_in_text( pos )) {
        WinPostMsg( hwnd, AMP_DISPLAY_MODE, 0, 0 );
      }

      break;
    }

    case WM_MOUSEMOVE:
    {
      POINTL pos;
      static POINTL last_pos;

      pos.x = (short)SHORT1FROMMP(mp1);
      pos.y = (short)SHORT2FROMMP(mp1);

      if( pos.x != last_pos.x || pos.y != last_pos.y )
      {
        last_pos = pos;

        if( is_volume_drag ) {
          amp_pb_set_volume( bmp_calc_volume( pos ));
        }
        if( is_slider_drag ) {
          seeking_pos = bmp_calc_time( pos, time_total());
          amp_invalidate( UPD_SLIDER );
        }
        if( hpopup ) {
          amp_pb_hide_popup();
        } else if( bmp_pt_in_volume( pos ) || bmp_pt_in_slider( pos )) {
          WinStopTimer ( hab, hwnd, TID_POPUP );
        } else {
          WinStartTimer( hab, hwnd, TID_POPUP, 2000 );
        }
      }
      break;
    }

    case WM_BUTTON1UP:
      if( is_volume_drag ) {
        is_volume_drag = FALSE;
        WinSetCapture( HWND_DESKTOP, NULLHANDLE );
      }
      if( is_slider_drag ) {
        msg_seek( seeking_pos * 1000 );
        is_slider_drag = FALSE;
        WinSetCapture( HWND_DESKTOP, NULLHANDLE );
      }
      break;

    case WM_BUTTON1MOTIONSTART:
    case WM_BUTTON2MOTIONSTART:
      WinSendMsg( hframe, WM_TRACKFRAME, MPFROMSHORT( TF_MOVE | TF_STANDARD ), 0 );
      WinQueryWindowPos( hframe, &cfg.main );
      return 0;

    case WM_TRANSLATEACCEL:
      if( hk_translate( HKW_PLAYER, mp1, mp2 )) {
        return MRFROMLONG( TRUE );
      }
      break;

    case WM_CHAR:
      if(!( SHORT1FROMMP(mp1) & KC_KEYUP ) && ( SHORT1FROMMP(mp1) & KC_VIRTUALKEY )) {
        if( SHORT2FROMMP(mp2) == VK_ESC ) {
          if( is_slider_drag ) {
            WinSetCapture( HWND_DESKTOP, NULLHANDLE );
            is_slider_drag = FALSE;
            is_seeking     = FALSE;
          }
        }
      }
      break;

    case WM_VSCROLL:
      if( SHORT2FROMMP( mp2 ) == SB_LINEUP ) {
        amp_pb_set_volume( cfg.defaultvol + 1 );
        return 0;
      } else if( SHORT2FROMMP( mp2 ) == SB_LINEDOWN ) {
        amp_pb_set_volume( cfg.defaultvol - 1 );
        return 0;
      }
      break;
  }

  return WinDefWindowProc( hwnd, msg, mp1, mp2 );
}

/* Creates and displays an error message window.
   Use the player window as message window owner. */
void
amp_show_error( const char* format, ... )
{
  char message[2048];
  va_list args;
  va_start( args, format );

  vsnprintf ( message, sizeof( message ), format, args );
  WinPostMsg( hplayer, AMP_DISPLAY_MESSAGE, MPFROMP( strdup( message )), MPFROMLONG( TRUE  ));
  upnp_notify_state_change( UPNP_N_ERROR );
}

/* Creates and displays an information message window.
   Use the player window as message window owner. */
void
amp_show_info( const char* format, ... )
{
  char message[2048];
  va_list args;
  va_start( args, format );

  vsnprintf ( message, sizeof( message ), format, args );
  WinPostMsg( hplayer, AMP_DISPLAY_MESSAGE, MPFROMP( strdup( message )), MPFROMLONG( FALSE ));
}

static USHORT
amp_message_box( HWND owner, const char* title,
                             const char* message, ULONG style  )
{
  char padded_title[60];
  sprintf( padded_title, "%-59s", title );

  if( owner == NULLHANDLE )
  {
    owner  =  HWND_DESKTOP;
    style &= ~MB_APPLMODAL;
  } else {
    style |=  MB_APPLMODAL;
  }

  return WinMessageBox( HWND_DESKTOP, owner, (PSZ)message,
                                      padded_title, 0, style );
}

/* Requests the user about specified action. Returns
   TRUE at confirmation or FALSE in other case. */
BOOL
amp_query( HWND owner, const char* format, ... )
{
  char message[2048];
  va_list args;

  va_start( args, format );
  vsnprintf( message, sizeof( message ), format, args );

  return amp_message_box( owner, "PM123 Query", message,
                          MB_QUERY | MB_YESNO | MB_MOVEABLE ) == MBID_YES;
}

/* Requests the user about overwriting a file. Returns
   TRUE at confirmation or at absence of a file. */
BOOL
amp_warn_if_overwrite( HWND owner, const char* filename )
{
  struct stat fi;

  if( stat( filename, &fi ) == 0 ) {
    return amp_query( owner, "File %s already exists. Overwrite it?", filename );
  } else {
    return TRUE;
  }
}

/* Tells the help manager to display a specific help window. */
void
amp_show_help( SHORT resid )
{
  WinSendMsg( hhelp, HM_DISPLAY_HELP, MPFROMLONG( MAKELONG( resid, NULL )),
                                      MPFROMSHORT( HM_RESOURCEID ));
}

/* Parses and processes a commandline arguments. Must be
   called from the main thread. */
static void
amp_pb_process_file_arguments( int files, int argc, char *argv[] )
{
  char curpath[_MAX_PATH];
  char file[_MAX_URL];
  int  i;

  ASSERT_IS_MAIN_THREAD;
  getcwd( curpath, sizeof( curpath ));

  for( i = 1; i < argc; i++ ) {
    if( *argv[i] != '/' && *argv[i] != '-' ) {
      if( !rel2abs( curpath, argv[i], file, sizeof(file))) {
        strcpy( file, argv[i] );
      }
      qu_write( load_queue, 0, strdup( file ));
    }
  }
}

/* The main PM123 entry point. */
int DLLENTRY
dll_main( int argc, char *argv[], int files,
          const char* pipename, HPIPE hpipe, int options )
{
  HMQ    hmq;
  char   exename[_MAX_PATH];
  char   bundle [_MAX_PATH];
  char   infname[_MAX_PATH];
  char   module [_MAX_PATH];
  QMSG   qmsg;
  struct stat fi;

  HELPINIT hinit     = { 0 };
  ULONG    flCtlData = FCF_TASKLIST | FCF_NOBYTEALIGN | FCF_ICON;

  hab = WinInitialize( 0 );
  hmq = WinCreateMsgQueue( hab, 0 );

  getExeName( exename, sizeof( exename ));
  getModule( &hmodule, module, sizeof( module ));
  sdrivedir( startpath, exename, sizeof( startpath ));

  pg_init();
  srand((unsigned long)time( NULL ));
  load_ini();

  if( options & ( ARG_ASSO_ALL | ARG_ASSO_CLEAR | ARG_ASSO_ON | ARG_ASSO_OFF ))
  {
    if( options & ARG_ASSO_ALL ) {
      asso_create_all();
    } else if( options & ARG_ASSO_CLEAR ) {
      asso_remove_all();
    } else if( options & ARG_ASSO_ON ) {
      asso_integrate( TRUE );
    } else if( options & ARG_ASSO_OFF ) {
      asso_integrate( FALSE );
    }

    pg_term();
    return 0;
  }
  if( options & ARG_SHUFFLE ) {
    is_arg_shuffle = TRUE;
  }

  if(( load_queue = qu_create()) == NULL ||
     ( load_tid = _beginthread( amp_load_thread, NULL, 65535, NULL )) == -1 )
  {
    amp_show_error( "Unable start the service thread." );
  }

  xio_set_http_proxy_host( cfg.proxy_host );
  xio_set_http_proxy_port( cfg.proxy_port );
  xio_set_http_proxy_user( cfg.proxy_user );
  xio_set_http_proxy_pass( cfg.proxy_pass );
  xio_set_bypass_local( cfg.proxy_no_local );
  xio_set_no_proxy( cfg.no_proxy );
  xio_set_buffer_size( cfg.buff_size * 1024 );
  xio_set_buffer_wait( cfg.buff_wait );
  xio_set_buffer_preload( cfg.buff_preload * 1024 );
  xio_set_connect_timeout( cfg.conn_timeout );

  bmp_init();

  InitButton ( hab );
  InitPicture( hab );
  InitSlider ( hab );

  WinRegisterClass( hab, "PM123", amp_pb_dlg_proc, CS_SIZEREDRAW, 0 );

  hframe = WinCreateStdWindow( HWND_DESKTOP, 0, &flCtlData, "PM123",
                               AMP_FULLNAME, 0, hmodule, WIN_MAIN, &hplayer );
  do_warpsans( hframe  );
  do_warpsans( hplayer );

  dk_init();
  dk_add_window( hframe, DK_IS_MASTER );

  mp3     = WinLoadPointer( HWND_DESKTOP, hmodule, ICO_MP3     );
  mp3play = WinLoadPointer( HWND_DESKTOP, hmodule, ICO_MP3PLAY );
  mp3gray = WinLoadPointer( HWND_DESKTOP, hmodule, ICO_MP3GRAY );

  pm_create();
  bm_create();
  pl_create();
  bn_init();

  strlcpy( infname, startpath,   sizeof( infname ));
  strlcat( infname, "pm123.inf", sizeof( infname ));

  if( stat( infname, &fi ) != 0  ) {
    // If the file of the help does not placed together with the program,
    // we shall give to the help manager to find it.
    strcpy( infname, "pm123.inf" );
  }

  hinit.cb = sizeof( hinit );
  hinit.phtHelpTable = (PHELPTABLE)MAKELONG( HLP_MAIN, 0xFFFF );
  hinit.hmodHelpTableModule = hmodule;
  hinit.pszHelpWindowTitle = "PM123 Help";
  hinit.fShowPanelId = CMIC_SHOW_PANEL_ID;
  hinit.pszHelpLibraryName = infname;

  if(( hhelp = WinCreateHelpInstance( hab, &hinit )) == NULLHANDLE ) {
    amp_show_error( "Error create help instance: %s", infname );
  } else {
    WinAssociateHelpInstance( hhelp, hframe );
  }

  msg_init();

  strlcpy( bundle, startpath,   sizeof( bundle ));
  strlcat( bundle, "pm123.lst", sizeof( bundle ));

  if( files > 0 ) {
    amp_pb_process_file_arguments( files, argc, argv );
  } else {
    amp_pb_display_filename();
    if( stat( bundle, &fi ) == 0 ) {
      pl_load_bundle( bundle, 0 );
    }
  }

  WinSetWindowPos( hframe, HWND_TOP,
                   cfg.main.x, cfg.main.y, 0, 0, SWP_ACTIVATE | SWP_MOVE | SWP_SHOW );

  if( cfg.dock_windows ) {
    dk_arrange( hframe );
  }

  if(( pipe_tid = _beginthread( amp_pipe_thread, NULL, 65535, (void*)hpipe )) == -1 ) {
    amp_show_error( "Unable start the named pipe service thread." );
  }

  vis_initialize_all( hplayer, FALSE );
  tag_init();
  hk_register( HKW_PLAYER, hplayer );

  if( cfg.upnp ) {
    upnp_init();
  }

  amp_pb_set_volume( cfg.defaultvol );

  while( WinGetMsg( hab, &qmsg, (HWND)0, 0, 0 )) {
    WinDispatchMsg( hab, &qmsg );
  }

  amp_pb_stop();
  is_terminate = TRUE;

  if( cfg.upnp ) {
    upnp_term();
  }

  if( pipe_tid != -1 ) {
    pipe_open_and_write( pipename, "*status" );
    wait_thread( pipe_tid, 2000 );
    pipe_close( hpipe );
  }
  if( load_tid != -1 ) {
    qu_write( load_queue, 0, NULL );
    wait_thread( load_tid, 2000 );
    qu_close( load_queue );
  }

  pl_save_bundle( bundle, 0 );

  pl_destroy();
  bm_destroy();
  pm_destroy();
  eq_destroy();
  bn_term();

  // The PM123 configuration must be saved after destroying of all child windows
  // because some important parameters are updated during destroying.
  save_ini();
  tag_term();
  msg_term();
  bmp_clean_skin();

  dk_term();

  if( hmenu ) {
    pg_cleanup_plugin_menu( mn_get_submenu( hmenu, IDM_M_PLUGINS ));
    WinDestroyWindow( hmenu );
  }

  pg_term();

  if( heq != NULLHANDLE ) {
    WinDestroyWindow( heq );
  }

  WinDestroyPointer( mp3     );
  WinDestroyPointer( mp3play );
  WinDestroyPointer( mp3gray );
  WinDestroyWindow ( hframe  );

  bmp_term();

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );

  #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
  #endif
  return 0;
}
