/*
 * Copyright 2018-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_BANNER_H
#define PM123_BANNER_H

#define DLG_BANNER     1100
#define ST_COVER        100
#define ST_ARTIST       101
#define ST_TITLE        102
#define ST_ALBUM        103
#define ST_TECH         104

/* Position of the banner */
#define BPOS_UPPER_RIGHT  0
#define BPOS_UPPER_LEFT   1
#define BPOS_BOTTOM_RIGHT 2
#define BPOS_BOTTOM_LEFT  3
#define BPOS_LEFT         1
#define BPOS_BOTTOM       2
#define BPOS_HIDE         4

/* Slide of the banner */
#define BSLIDE_NONE       0
#define BSLIDE_VERTICAL   1
#define BSLIDE_HORIZONTAL 2

#ifdef __cplusplus
extern "C" {
#endif

/* Initializes banner subsystem. */
BOOL bn_init( void );
/* Terminates  banner subsystem. */
BOOL bn_term( void );

/* Sets banner presentation parameters. */
void bn_set_parameters( int pos, int slide, int delay );
/* Show banner. */
void bn_show( const AMP_FILE* file );
/* Hide banner. */
void bn_hide( void );
/* Changes the banner colors. */
BOOL bn_set_colors( ULONG, ULONG );

#ifdef __cplusplus
}
#endif
#endif /* PM123_BANNER_H */
