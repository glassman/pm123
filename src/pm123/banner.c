/*
 * Copyright 2018-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <stdlib.h>
#include <stdio.h>
#include <process.h>
#include <debuglog.h>

#include "pm123.h"
#include "banner.h"
#include "assertions.h"
#include "picture.h"
#include "skin.h"
#include "iniman.h"

static HWND   hbanner    = NULLHANDLE;
static ULONG  pos        = BPOS_UPPER_RIGHT;
static ULONG  slide      = BSLIDE_HORIZONTAL;
static int    slide_step = 0;
static BOOL   slide_show = FALSE;
static ULONG  delay      = 5;

static TID    broker_tid   = 0;
static PQUEUE broker_queue = NULL;

static AMP_FILE* showed_file;

#define BN_SHOW      0
#define BN_TERMINATE 1

#define BN_MSG_SHOW  ( WM_USER + 1500  )
#define BN_MSG_SETUP ( WM_USER + 1501  )
#define TID_SLIDE    ( TID_USERMAX - 1 )
#define TID_HIDE     ( TID_USERMAX - 2 )

#define SLIDE_STEPS     10
#define SLIDE_STEP_TIME 25

/* Changes the banner colors. */
BOOL
bn_set_colors( ULONG fgcolor, ULONG bgcolor )
{
  HWND htitle = WinWindowFromID( hbanner, ST_TITLE );

  WinSetPresParam( hbanner, PP_FOREGROUNDCOLOR, sizeof(fgcolor), &fgcolor );
  WinSetPresParam( hbanner, PP_BACKGROUNDCOLOR, sizeof(bgcolor), &bgcolor );
  WinSetPresParam( htitle,  PP_FOREGROUNDCOLOR, sizeof(fgcolor), &fgcolor );
  WinSetPresParam( htitle,  PP_BACKGROUNDCOLOR, sizeof(bgcolor), &bgcolor );
  WinSendMsg( hbanner, WM_SKIN_CHANGED, 0, 0 );
  return TRUE;
}

/* Formats the banner window. Must be called
   from the main thread. */
static void
bn_m_frame_format( HWND hwnd )
{
  RECTL rect;
  HPS   hps = WinGetPS( hwnd );
  HDC   hdc = GpiQueryDevice( hps );
  LONG  dpi = 96;
  LONG  cy  = 15;

  ASSERT_IS_MAIN_THREAD;

  DevQueryCaps( hdc, CAPS_HORIZONTAL_FONT_RES, 1L, &dpi );
  WinReleasePS( hps );

  if( WinQueryWindowRect( hwnd, &rect ))
  {
    WinSendMsg( hwnd, WM_CALCFRAMERECT, MPFROMP(&rect), MPFROMSHORT(TRUE));

    WinSetWindowPos( WinWindowFromID( hwnd, ST_COVER ), 0,
                     rect.xLeft + 3,
                     rect.yBottom + 3,
                     rect.yTop - rect.yBottom - 6,
                     rect.yTop - rect.yBottom - 6,
                     SWP_MOVE | SWP_SIZE );

    cy = cy * dpi / 96;

    WinSetWindowPos( WinWindowFromID( hwnd, ST_ARTIST ), 0,
                     rect.xLeft + 3 + rect.yTop - rect.yBottom,
                     rect.yTop - cy - 3,
                     rect.xRight - rect.xLeft - 9 - rect.yTop + rect.yBottom,
                     cy,
                     SWP_MOVE | SWP_SIZE );

    WinSetWindowPos( WinWindowFromID( hwnd, ST_TITLE ), 0,
                     rect.xLeft + 3 + rect.yTop - rect.yBottom,
                     rect.yTop - 2 * cy - 3,
                     rect.xRight - rect.xLeft - 9 - rect.yTop + rect.yBottom,
                     cy,
                     SWP_MOVE | SWP_SIZE );

    WinSetWindowPos( WinWindowFromID( hwnd, ST_ALBUM ), 0,
                     rect.xLeft + 3 + rect.yTop - rect.yBottom,
                     rect.yTop - 3 * cy - 3,
                     rect.xRight - rect.xLeft - 9 - rect.yTop + rect.yBottom,
                     cy,
                     SWP_MOVE | SWP_SIZE );

    WinSetWindowPos( WinWindowFromID( hwnd, ST_TECH ), 0,
                     rect.xLeft + 3 + rect.yTop - rect.yBottom,
                     rect.yTop - 4 * cy - 3,
                     rect.xRight - rect.xLeft - 9 - rect.yTop + rect.yBottom,
                     cy,
                     SWP_MOVE | SWP_SIZE );
  }
}

/* Positioning the window according to the current step of the animation.
   Must be called from the main thread. */
static void
bn_m_slide( int step )
{
  RECTL win_rect;
  RECTL desktop_rect;
  SWP   max_pos;
  LONG  x, y;
  float diff;

  ASSERT_IS_MAIN_THREAD;

  WinQueryWindowRect( hbanner, &win_rect );
  WinQueryWindowRect( HWND_DESKTOP, &desktop_rect );
  WinGetMaxPosition( hbanner, &max_pos );

  if( max_pos.x > 0 ) {
    desktop_rect.xLeft = max_pos.x;
  }
  if( max_pos.x + max_pos.cx + 1 < desktop_rect.xRight ) {
    desktop_rect.xRight = max_pos.x + max_pos.cx + 1;
  }
  if( max_pos.y > 0 ) {
    desktop_rect.yBottom = max_pos.y;
  }
  if( max_pos.y + max_pos.cy - 1 < desktop_rect.yTop ) {
    desktop_rect.yTop = max_pos.y + max_pos.cy - 1;
  }

  switch( pos ) {
    case BPOS_BOTTOM_LEFT:
      x = 0;
      y = desktop_rect.yBottom;
      break;
    case BPOS_BOTTOM_RIGHT:
      x = desktop_rect.xRight - win_rect.xRight;
      y = desktop_rect.yBottom;
      break;
    case BPOS_UPPER_LEFT:
      x = desktop_rect.xLeft;
      y = desktop_rect.yTop - win_rect.yTop;
      break;
    default:
      x = desktop_rect.xRight - win_rect.xRight;
      y = desktop_rect.yTop - win_rect.yTop;
      break;
  }

  if( slide_show ) {
    diff = (float)slide_step * slide_step / SLIDE_STEPS / SLIDE_STEPS;
  } else {
    diff = (float)(SLIDE_STEPS - slide_step) * (SLIDE_STEPS - slide_step) / SLIDE_STEPS / SLIDE_STEPS;
  }

  if( slide == BSLIDE_HORIZONTAL ) {
    if( pos & BPOS_LEFT ) {
      x -= win_rect.xRight * diff;
    } else {
      x += win_rect.xRight * diff;
    }
  } else if( slide == BSLIDE_VERTICAL ) {
    if( pos & BPOS_BOTTOM ) {
      y -= win_rect.yTop * diff;
    } else {
      y += win_rect.yTop * diff;
    }
  }

  WinSetWindowPos( hbanner, 0, x, y, 0, 0, SWP_MOVE );

  if( slide_show || !slide_step ) {
    WinShowWindow( hbanner, slide_show );
  }
}

/* Processes messages of the banner presentation window. */
static MRESULT EXPENTRY
bn_m_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
      bn_m_frame_format( hwnd );
      break;

    case WM_FORMATFRAME:
    {
      MRESULT rc = bmp_frame_wnd_proc( hwnd, msg, mp1, mp2 );
      bn_m_frame_format( hwnd );
      return rc;
    }

    case WM_SYSCOMMAND:
      switch( SHORT1FROMMP(mp1)) {
        case SC_CLOSE:
        case SC_HIDE:
          stop_timer( hbanner, TID_HIDE  );
          stop_timer( hbanner, TID_SLIDE );
          slide_step = 0;
          slide_show = FALSE;
          break;
      }
      break;

    case WM_TIMER:
      switch( SHORT1FROMMP(mp1)) {
        case TID_SLIDE:
          bn_m_slide( slide_step );
          if( slide_step ) {
            slide_step--;
          } else {
            stop_timer( hbanner, TID_SLIDE );
          }
          break;

        case TID_HIDE:
          stop_timer( hbanner, TID_HIDE );
          slide_step = SLIDE_STEPS;
          slide_show = FALSE;
          start_timer( hbanner, TID_SLIDE, SLIDE_STEP_TIME );
          break;
      }
      break;

    case WM_SKIN_CHANGED:
      bn_m_frame_format( hwnd );
      break;

    case BN_MSG_SHOW:
    {
      AMP_FILE* file = (AMP_FILE*)mp1;
      char buf[512];

      WinSetDlgItemText( hwnd, ST_ARTIST, file->info.artist );

      if( *file->info.title ) {
        WinSetDlgItemText( hwnd, ST_TITLE, file->info.title );
      } else {
        char songname[_MAX_URL];
        amp_title_from_filename( songname, file->filename, sizeof( songname ));
        WinSetDlgItemText( hwnd, ST_TITLE, songname );
      }

      if( *file->info.album && *file->info.year ) {
        snprintf( buf, sizeof( buf ), "%s (%s)", file->info.album, file->info.year );
        WinSetDlgItemText( hwnd, ST_ALBUM, buf );
      } else {
        WinSetDlgItemText( hwnd, ST_ALBUM, file->info.album );
      }

      snprintf( buf, sizeof( buf ), "%02u:%02u, %s", file->info.songlength / 60000,
                file->info.songlength / 1000 % 60, file->info.tech_info );
      WinSetDlgItemText( hwnd, ST_TECH,  buf );

      WinSendDlgItemMsg( hwnd, ST_COVER, WM_PIC_SETPICS,
                         MPFROMP( file->info.pics ), MPFROMLONG( file->info.pics_count ));

      amp_cleaninfo( showed_file );
      free( showed_file );
      showed_file = file;

      stop_timer ( hbanner, TID_HIDE );
      start_timer( hbanner, TID_HIDE, delay * 1000 );

      if( !slide_show ) {
        slide_step = slide_step ? ( SLIDE_STEPS - slide_step ) : SLIDE_STEPS;
        slide_show = TRUE;
        start_timer( hbanner, TID_SLIDE, SLIDE_STEP_TIME );
        WinSendMsg( hbanner, WM_TIMER, MPFROMSHORT( TID_SLIDE ), 0 );
      }

      WinSetWindowPos( hbanner, HWND_TOP, 0, 0, 0, 0, SWP_ZORDER );
      return 0;
    }

    case BN_MSG_SETUP:
    {
      int  new_pos   = SHORT1FROMMP(mp1);
      int  new_slide = SHORT2FROMMP(mp1);
      int  new_delay = SHORT1FROMMP(mp2);
      BOOL refresh   = ( new_pos != pos || new_slide != slide );

      pos   = new_pos;
      slide = new_slide;
      delay = new_delay;

      if( refresh ) {
        if( pos == BPOS_HIDE ) {
          WinSendMsg( hbanner, WM_CLOSE, 0, 0 );
        } else {
          bn_m_slide( slide_step );
        }
      }

      return 0;
    }
  }

  return bmp_frame_wnd_proc( hwnd, msg, mp1, mp2 );
}

/* Show banner. */
static void
bn_m_show( AMP_FILE* file )
{
  int options = 0;

  if( hbanner ) {
    if( file->info.pics_count ) {
      if( !file->info.pics ) {
        options = DECODER_EXTPICS | DECODER_LOADPICS;
      }
    } else {
      options = DECODER_EXTPICS;
    }

    if( options ) {
      #if DEBUG
      int rc =
      #endif
          dec_loadpics( file->filename, &file->info, file->decoder, options );

      DEBUGLOG(( "pm123: dec_loadpics returns %d, pictures found=%d\n",
                                           rc, file->info.pics_count ));
    }

    WinSendMsg( hbanner, BN_MSG_SHOW, MPFROMP( file ), 0 );
  } else {
    amp_cleaninfo( file );
    free( file );
  }
}

/* Dispatches the banner subsytem management requests. */
static void TFNENTRY
bn_broker( void* dummy )
{
  ULONG     request;
  AMP_FILE* file;
  HAB       hab = WinInitialize( 0 );
  HMQ       hmq = WinCreateMsgQueue( hab, 0 );

  while( qu_read( broker_queue, &request, (PPVOID)&file ))
  {
    if( request == BN_TERMINATE ) {
      break;
    }

    switch( request ) {
      case BN_TERMINATE:
        break;

      case BN_SHOW:
        if( pos != BPOS_HIDE ) {
          bn_m_show( file );
        }
        break;
    }
  }

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
  _endthread();
}

/* Show banner. */
void
bn_show( const AMP_FILE* file )
{
  AMP_FILE* data = (AMP_FILE*)calloc( sizeof( AMP_FILE ), 1 );

  if( data ) {
    amp_copyinfo( data, file );
    qu_write( broker_queue, BN_SHOW, data );
  }
}

/* Hide banner. */
void bn_hide( void )
{
  if( hbanner && WinIsWindowVisible( hbanner )) {
    WinSendMsg( hbanner, WM_TIMER, MPFROMSHORT( TID_HIDE ), 0 );
  }
}

/* Sets banner presentation parameters. */
void bn_set_parameters( int new_pos, int new_slide, int new_delay )
{
  if( hbanner ) {
    WinSendMsg( hbanner, BN_MSG_SETUP, MPFROM2SHORT( new_pos, new_slide ), MPFROMSHORT( new_delay ));
  }
}

/* Initializes banner subsystem. */
BOOL
bn_init()
{
  ASSERT_IS_MAIN_THREAD;

  hbanner = WinLoadDlg( HWND_DESKTOP, NULLHANDLE, bn_m_dlg_proc,
                        hmodule, DLG_BANNER, NULL );
  if( hbanner ) {
    // Because the title text is not set during creating of the skinned
    // window this must be set manually here.
    WinSetWindowText( hbanner, "PM123" );
    do_warpsans( hbanner );
    do_warpsans_bold( WinWindowFromID( hbanner, ST_TITLE ));

    if( !rest_window_pos( hbanner, WIN_MAP_POINTS )) {
      bn_set_colors( DEF_FG_COLOR, DEF_BG_COLOR );
    }
  } else {
    amp_show_error( "Unable create the banner window." );
    return FALSE;
  }

  broker_queue = qu_create();

  if( !broker_queue ) {
    amp_show_error( "Unable create banner subsytem service queue." );
    return FALSE;
  } else {
    if(( broker_tid = _beginthread( bn_broker, NULL, 2048000, NULL )) == -1 ) {
      amp_show_error( "Unable create the banner subsytem service thread." );
      return FALSE;
    }
  }

  bn_set_parameters( cfg.bn_pos, cfg.bn_slide, cfg.bn_delay );
  return TRUE;
}

/* Terminates banner subsystem. */
BOOL
bn_term()
{
  ULONG     request;
  AMP_FILE* file;

  ASSERT_IS_MAIN_THREAD;

  while( !qu_empty( broker_queue )) {
    qu_read( broker_queue, &request, (PPVOID)&file );
    amp_cleaninfo( file );
    free( file );
  }

  qu_push( broker_queue, BN_TERMINATE, NULL );
  wait_thread( broker_tid, 2000 );
  qu_close( broker_queue );

  WinShowWindow( hbanner, FALSE );
  save_window_pos( hbanner, WIN_MAP_POINTS );
  WinDestroyWindow( hbanner );

  amp_cleaninfo( showed_file );
  free( showed_file );
  return TRUE;
}
