/*
 * Copyright 2009 Dmitry A.Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>

#include "mmsgbox.h"
#include "pm123.h"

static HWND hmmsgbox   = NULLHANDLE;
static HWND hcontainer = NULLHANDLE;

/* This call destroys a multi message box window.
   Must be called from the main thread. */
static void
dismiss_mmsgbox( void )
{
  MSGRECORD* rec = (MSGRECORD*)WinSendMsg( hcontainer, CM_QUERYRECORD, MPFROMP( NULL ),
                                           MPFROM2SHORT( CMA_FIRST, CMA_ITEMORDER ));
  while( rec )
  {
    free( rec->rc.pszIcon );
    free( rec->pszMessage );

    rec = (MSGRECORD*)WinSendMsg( hcontainer, CM_QUERYRECORD, MPFROMP( rec ),
                                  MPFROM2SHORT( CMA_NEXT, CMA_ITEMORDER ));
  }

  WinSendMsg( hcontainer, CM_REMOVERECORD, NULL, MPFROM2SHORT( 0, CMA_FREE ));
  WinSendMsg( hcontainer, CM_REMOVEDETAILFIELDINFO, NULL, MPFROM2SHORT( 0, CMA_FREE ));
  WinDestroyWindow( hmmsgbox );
}

/* Processes messages of the multi message box.
   Must be called from the main thread. */
static MRESULT EXPENTRY
mmsgbox_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case DID_OK:
          dismiss_mmsgbox();
          break;
      }
      return 0;

    case WM_SYSCOMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case SC_CLOSE:
          dismiss_mmsgbox();
          return 0;
      }
      break;

    case WM_DESTROY:
      hmmsgbox = NULLHANDLE;
      break;
  }

  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Reformats a message. Inserts additional new-line characters
   if necessary. Must be called from the main thread. */
static void
reformat_message( MSGRECORD* rec )
{
  HPS   hps;
  LONG  msglen    = strlen( rec->pszMessage );
  char* formatted = malloc( msglen * 2 );
  LONG  done      = 0;
  RECTL msgrect;

  if( !formatted ) {
    free( rec->rc.pszIcon );
    rec->rc.pszIcon = NULL;
    return;
  } else {
    strcpy( formatted, "\n" );
  }

  WinSendMsg( hcontainer, CM_QUERYVIEWPORTRECT,
              MPFROMP( &msgrect ), MPFROM2SHORT( CMA_WINDOW, FALSE ));

  msgrect.xRight -= ( WinQuerySysValue( HWND_DESKTOP, SV_CXICON    ) +
                      WinQuerySysValue( HWND_DESKTOP, SV_CXVSCROLL ) + 10 ) ;

  hps = WinGetPS( hcontainer );
  while( done < msglen )
  {
    RECTL rect  = msgrect;
    char* p     = rec->pszMessage + done;
    LONG  chars = WinDrawText( hps, -1, p, &rect, 0, 0, DT_QUERYEXTENT | DT_WORDBREAK );

    strlcpy( formatted + strlen( formatted ), p, chars + 1 );

    if( formatted[ strlen( formatted ) - 1 ] != '\n' ) {
      strcat ( formatted, "\n" );
    }

    done += chars;
  }
  WinReleasePS( hps );

  strcat( formatted, "\n" );

  free( rec->rc.pszIcon );
  rec->rc.pszIcon = formatted;
}

/* Creates and initialized the multi message box.
   Must be called from the main thread. */
static BOOL
mmsgbox_create( HWND howner )
{
  SWP        swp;
  SWP        swp_screen;
  FIELDINFO* first;
  FIELDINFO* field;
  ULONG      color;

  FIELDINFOINSERT insert;
  CNRINFO cnrinfo;

  hmmsgbox = WinLoadDlg( HWND_DESKTOP, howner, mmsgbox_dlg_proc,
                         hmodule, DLG_MMSGBOX, NULL );
  if( !hmmsgbox ) {
    return FALSE;
  }

  hcontainer = WinWindowFromID( hmmsgbox, CNR_MMSGBOX );
  do_warpsans( hmmsgbox );

  if( WinQueryWindowPos( hmmsgbox, &swp ) &&
      WinQueryWindowPos( HWND_DESKTOP, &swp_screen ))
  {
    swp.x = ( swp_screen.cx - swp.cx ) / 2;
    swp.y = ( swp_screen.cy - swp.cy ) / 2;
    WinSetWindowPos( hmmsgbox, 0, swp.x, swp.y, 0, 0, SWP_MOVE );
  }

  first = (FIELDINFO*)WinSendMsg( hcontainer, CM_ALLOCDETAILFIELDINFO, MPFROMSHORT(2), 0 );
  field = first;

  field->flData    = CFA_BITMAPORICON | CFA_VCENTER | CFA_CENTER;
  field->offStruct = FIELDOFFSET( MSGRECORD, rc.hptrIcon);

  field = field->pNextFieldInfo;

  field->flData    = CFA_STRING;
  field->offStruct = FIELDOFFSET( MSGRECORD, rc.pszIcon );

  insert.cb = sizeof( FIELDINFOINSERT );
  insert.pFieldInfoOrder = (PFIELDINFO)CMA_FIRST;
  insert.fInvalidateFieldInfo = TRUE;
  insert.cFieldInfoInsert = 2;

  WinSendDlgItemMsg( hmmsgbox, CNR_MMSGBOX, CM_INSERTDETAILFIELDINFO,
                     MPFROMP( first ), MPFROMP( &insert ));

  cnrinfo.cb             = sizeof(cnrinfo);
  cnrinfo.pFieldInfoLast = first->pNextFieldInfo;
  cnrinfo.flWindowAttr   = CV_DETAIL | CA_DRAWICON;

  WinSendMsg( hcontainer, CM_SETCNRINFO, MPFROMP( &cnrinfo ),
              MPFROMLONG( CMA_PFIELDINFOLAST | CMA_FLWINDOWATTR ));

  color = SYSCLR_DIALOGBACKGROUND;
  WinSetPresParam( hcontainer, PP_BACKGROUNDCOLORINDEX, sizeof(ULONG), &color );
  WinSetPresParam( hcontainer, PP_HILITEBACKGROUNDCOLORINDEX, sizeof(ULONG), &color );
  color = SYSCLR_ICONTEXT;
  WinSetPresParam( hcontainer, PP_HILITEFOREGROUNDCOLORINDEX, sizeof(ULONG), &color );
  return TRUE;
}

/* Scrolls the message box so that the specified
   record became visible. Must be called from the main thread. */
static void
scroll_to_record( MSGRECORD* rec )
{
  QUERYRECORDRECT prcItem;
  RECTL rclRecord;
  RECTL rclContainer;

  prcItem.cb       = sizeof(prcItem);
  prcItem.pRecord  = (PRECORDCORE)rec;
  prcItem.fsExtent = CMA_ICON | CMA_TEXT;

  if( WinSendMsg( hcontainer, CM_QUERYRECORDRECT, &rclRecord, &prcItem )) {
    if( WinSendMsg( hcontainer, CM_QUERYVIEWPORTRECT, &rclContainer,
                                MPFROM2SHORT( CMA_WINDOW, FALSE )))
    {
      if( rclRecord.yBottom < rclContainer.yBottom )  {
        WinPostMsg( hcontainer, CM_SCROLLWINDOW, (MPARAM)CMA_VERTICAL,
                    (MPARAM)(rclContainer.yBottom - rclRecord.yBottom ));
      } else if( rclRecord.yTop > rclContainer.yTop ) {
        WinPostMsg( hcontainer, CM_SCROLLWINDOW, (MPARAM)CMA_VERTICAL,
                    (MPARAM)(rclContainer.yTop - rclRecord.yTop ));
      }
    }
  }
}

/* This function creates, displays, and operates a message box window.
   Must be called from the main thread. */
ULONG
multimsgbox( HWND howner, const char* message, int options )
{
  MSGRECORD*   rec;
  RECORDINSERT ins;
  LONG         beep_style;
  LONG         icon_style;

  if( !hmmsgbox ) {
    if( !mmsgbox_create( howner )) {
      return MBID_ERROR;
    }
  }

  if( options & MB_WARNING ) {
    icon_style = SPTR_ICONWARNING;
    beep_style = WA_WARNING;
  } else if( options & MB_ERROR ) {
    icon_style = SPTR_ICONERROR;
    beep_style = WA_ERROR;
  } else {
    icon_style = SPTR_ICONINFORMATION;
    beep_style = WA_NOTE;
  }

  rec = (MSGRECORD*)WinSendMsg( hcontainer, CM_ALLOCRECORD,
                                MPFROMLONG( sizeof( MSGRECORD ) - sizeof( RECORDCORE )),
                                MPFROMLONG( 1 ));
  if( !rec ) {
    return MBID_ERROR;
  }

  rec->rc.cb       = sizeof( RECORDCORE );
  rec->pszMessage  = strdup( message );
  rec->rc.hptrIcon = WinQuerySysPointer( HWND_DESKTOP, icon_style, FALSE );

  reformat_message( rec );

  ins.cb                = sizeof(RECORDINSERT);
  ins.pRecordOrder      = (PRECORDCORE)CMA_FIRST;
  ins.pRecordParent     = NULL;
  ins.fInvalidateRecord = TRUE;
  ins.zOrder            = CMA_TOP;
  ins.cRecordsInsert    = 1;

  WinSendMsg( hcontainer, CM_INSERTRECORD, MPFROMP( rec ), MPFROMP( &ins ));
  WinSendMsg( hcontainer, CM_SETRECORDEMPHASIS, MPFROMP( rec ), MPFROM2SHORT( TRUE, CRA_SELECTED ));
  scroll_to_record( rec );

  WinSetWindowPos( hmmsgbox, HWND_TOP, 0, 0, 0, 0, SWP_SHOW | SWP_ZORDER | SWP_ACTIVATE );
  WinAlarm( HWND_DESKTOP, beep_style );
  return MBID_OK;
}

