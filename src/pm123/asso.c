/*
 * Copyright 2009-2021 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <utilfct.h>

#include "pm123.h"
#include "asso.h"
#include "plugman.h"

static ULONG     types_count  = 0;
static ASSOFILE* types_list   = NULL;

static ASSOFILE  well_known[] =
  {{ "AIF" , "AIF" , ID_TYPE_AIF   },
   { "AIFF", "AIF" , ID_TYPE_AIF   },
   { "AU"  , "AU"  , ID_TYPE_AU    },
   { "AVR" , "AVR" , ID_TYPE_AVR   },
   { "CAF" , "CAF" , ID_TYPE_CAF   },
   { "IFF" , "IFF" , ID_TYPE_IFF   },
   { "LST" , "LST" , ID_TYPE_LST   },
   { "M3U" , "M3U" , ID_TYPE_M3U   },
   { "M3U8", "M3U8", ID_TYPE_M3U8  },
   { "MAT" , "MAT" , ID_TYPE_MAT   },
   { "MP1" , "MP1" , ID_TYPE_MP1   },
   { "MP2" , "MP2" , ID_TYPE_MP2   },
   { "MP3" , "MP3" , ID_TYPE_MP3   },
   { "MPL" , "MPL" , ID_TYPE_MPL   },
   { "OGG" , "OGGS", ID_TYPE_OGG   },
   { "PAF" , "PAF" , ID_TYPE_PAF   },
   { "PLS" , "PLS" , ID_TYPE_PLS   },
   { "PVF" , "PVF" , ID_TYPE_PVF   },
   { "SD2" , "SD2" , ID_TYPE_SD2   },
   { "SDS" , "SDS" , ID_TYPE_SDS   },
   { "SF"  , "SF"  , ID_TYPE_SF    },
   { "VOC" , "VOC" , ID_TYPE_VOC   },
   { "W64" , "W64" , ID_TYPE_W64   },
   { "WAV" , "WAV" , ID_TYPE_WAV   },
   { "XI"  , "XI"  , ID_TYPE_XI    },
   { "OGA" , "OGA" , ID_TYPE_OGA   },
   { "FLAC", "FLAC", ID_TYPE_FLAC  },
   { "APE" , "APE" , ID_TYPE_APE   },
   { "MPC" , "MPC" , ID_TYPE_MPC   },
   { "MP+" , "MP+" , ID_TYPE_MPLUS },
   { "TTA" , "TTA" , ID_TYPE_TTA   },
   { "WV"  , "WV"  , ID_TYPE_WV    },
   { "CUE" , "CUE" , ID_TYPE_CUE   },
   { "DSF" , "DSF" , ID_TYPE_DSF   },
   { "DFF" , "DFF" , ID_TYPE_DFF   },
   { "AAC" , "AAC" , ID_TYPE_AAC   },
   { "MP4" , "MP4" , ID_TYPE_MP4   },
   { "M4A" , "M4A" , ID_TYPE_M4A   },
   { "M4B" , "M4B" , ID_TYPE_M4B   }};

static char* pmwp_assoc_filter = "PMWP_ASSOC_FILTER";
static char* pmwp_assoc_type   = "PMWP_ASSOC_TYPE";

/* Returns a handle of the program object associated with
 * the specified extension.
 */

static HOBJECT
asso_query_object_handle( const char* ext )
{
  char id[128];

  snprintf( id, sizeof( id ), "<PM123_%s>", ext );
  return WinQueryObject( id );
}

/* Returns a list of object handles associated with
 * specified extension.
 */

static ASSOLIST*
asso_query_filter_list( ASSOLIST* list, const char* ext )
{
  char key[64];

  snprintf( key, sizeof( key ), "*.%s", ext );
  if( PrfQueryProfileSize( HINI_USERPROFILE, pmwp_assoc_filter, key, &list->size )) {
    if(( list->ps = malloc( list->size + 1 )) != NULL ) {
      if( PrfQueryProfileData( HINI_USERPROFILE, pmwp_assoc_filter, key, list->ps, &list->size )) {
        list->ps[list->size] = 0;
        return list;
      } else {
        free( list->ps );
      }
    }
  }

  list->ps   = NULL;
  list->size = 0;
  return NULL;
}

/* Returns a list of object handles associated with
 * specified type.
 */

static ASSOLIST*
asso_query_type_list( ASSOLIST* list, const char* type )
{
  if( PrfQueryProfileSize( HINI_USERPROFILE, pmwp_assoc_type, (PSZ)type, &list->size )) {
    if(( list->ps = malloc( list->size + 1 )) != NULL ) {
      if( PrfQueryProfileData( HINI_USERPROFILE, pmwp_assoc_type, (PSZ)type, list->ps, &list->size )) {
        list->ps[list->size] = 0;
        return list;
      } else {
        free( list->ps );
      }
    }
  }

  list->ps   = NULL;
  list->size = 0;
  return NULL;
}

/* Returns TRUE if the specified handle of the program object
 * found in association list.
 */

static BOOL
asso_in_list( ASSOLIST* list, HOBJECT hobject )
{
  char  shandle[64];
  PCHAR p;

  ltoa( hobject, shandle, 10 );

  if( list->size ) {
    for( p = list->ps; *p; p += strlen( p ) + 1 ) {
      if( strcmp( p, shandle ) == 0 ) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

/* Associates the specified handle of the program object
 * with the specified type.
 */

static BOOL
asso_insert_to_type_list( const char* type, HOBJECT hobject )
{
  char     shandle[64];
  ASSOLIST old_list;
  ASSOLIST new_list;
  BOOL     rc = FALSE;

  ltoa( hobject, shandle, 10 );

  if( !asso_query_type_list( &old_list, type )) {
    new_list.size = strlen( shandle ) + 1;
    new_list.ps   = strdup( shandle );
  } else {
    new_list.size = strlen( shandle ) + 1;
    new_list.ps   = malloc( old_list.size + new_list.size );

    if( new_list.ps )
    {
      PCHAR p;

      strcpy( new_list.ps, shandle );
      for( p = old_list.ps; *p; p += strlen( p ) + 1 ) {
        if( strcmp( p, shandle ) != 0 ) {
          strcpy( new_list.ps + new_list.size, p );
          new_list.size += strlen( p ) + 1;
        }
      }
    }
  }

  if( new_list.ps ) {
    rc = PrfWriteProfileData( HINI_USERPROFILE, pmwp_assoc_type, (PSZ)type, new_list.ps, new_list.size );
  }

  free( old_list.ps );
  free( new_list.ps );
  return rc;
}

/* Associates the specified handle of the program object
 * with the specified extension.
 */

BOOL
asso_insert_to_filter_list( const char* ext, HOBJECT hobject )
{
  char     shandle[64];
  ASSOLIST old_list;
  ASSOLIST new_list;
  BOOL     rc = FALSE;
  char     key[64];

  ltoa( hobject, shandle, 10 );

  if( !asso_query_filter_list( &old_list, ext )) {
    new_list.size = strlen( shandle ) + 1;
    new_list.ps   = strdup( shandle );
  } else {
    new_list.size = strlen( shandle ) + 1;
    new_list.ps   = malloc( old_list.size + new_list.size );

    if( new_list.ps )
    {
      PCHAR p;

      strcpy( new_list.ps, shandle );
      for( p = old_list.ps; *p; p += strlen( p ) + 1 ) {
        if( strcmp( p, shandle ) != 0 ) {
          strcpy( new_list.ps + new_list.size, p );
          new_list.size += strlen( p ) + 1;
        }
      }
    }
  }

  if( new_list.ps ) {
    snprintf( key, sizeof( key ), "*.%s", ext );
    rc = PrfWriteProfileData( HINI_USERPROFILE, pmwp_assoc_filter, key, new_list.ps, new_list.size );
  }

  free( old_list.ps );
  free( new_list.ps );
  return rc;
}

/* Sets the default view for a class of files.
 */

static BOOL
asso_set_default_view( const char* ext, const char* view )
{
  char*   temp = getenv( "TEMP" );
  char    filename[_MAX_PATH];
  char    setup[512];
  FILE*   file;
  HOBJECT hobject;

  if( temp ) {
    snprintf( filename, sizeof( filename ), "%s\\%s.%s", temp, "PM123$$", ext );
  } else {
    snprintf( filename, sizeof( filename ), "%s.%s", "PM123$$", ext );
  }

  if(( file = fopen( filename, "wb" )) != NULL ) {
    fclose( file );
    snprintf( setup, sizeof( setup ), "CLASSDEFAULTVIEW=%s", view );
    hobject = WinQueryObject( filename );

    if( hobject ) {
      WinSetObjectData( hobject, setup );
      WinDestroyObject( hobject );
      return TRUE;
    }
  }

  return FALSE;
}

/* Clears association with specified file.
 */

BOOL
asso_remove( ASSOFILE* file )
{
  HOBJECT hobject = asso_query_object_handle( file->ext );

  if( hobject ) {
    WinDestroyObject( hobject );
    asso_set_default_view( file->ext, "P~lay" );
  }

  return TRUE;
}

/* Clears association with all files.
 */

BOOL
asso_remove_all( void )
{
  BOOL    need_term = FALSE;
  HOBJECT hobject;
  int     i;

  if( !types_list ) {
    asso_init( NULLHANDLE );
    need_term = TRUE;
  }

  if(( hobject = WinQueryObject( "<PM123ASSOTIATIONS>" )) != NULLHANDLE ) {
    WinDestroyObject( hobject );
  }

  for( i = 0; i < types_count; i++ ) {
    asso_remove( types_list + i );
  }

  if( need_term ) {
    asso_term( NULLHANDLE );
  }

  return TRUE;
}

/* Associates program with specified file.
 */

BOOL
asso_create( ASSOFILE* file )
{
  HOBJECT hobject;
  char    setup[2048];
  char    title[  64];

  snprintf( title, sizeof( title ), "PM123[%s]", file->ext );
  snprintf( setup, sizeof( setup ),
    "PROGTYPE=PM;EXENAME=%sPM123.EXE;OBJECTID=<PM123_%s>;ICONRESOURCE=%d,%sPM123.DLL;STARTUPDIR=%s",
     startpath, file->ext, file->resid, startpath, startpath );

  hobject = WinCreateObject( "WPProgram", title, setup, "<WP_NOWHERE>", CO_REPLACEIFEXISTS );

  if( !hobject ) {
    return FALSE;
  }

  asso_insert_to_filter_list( file->ext, hobject );
  asso_insert_to_type_list( file->type, hobject );
  asso_set_default_view( file->ext, title );
  return TRUE;
}

/* Associates program with all files.
 */

BOOL
asso_create_all( void )
{
  BOOL    need_term = FALSE;
  HOBJECT hobject;
  int     i;

  if( !types_list ) {
    asso_init( NULLHANDLE );
    need_term = TRUE;
  }

  if(( hobject = WinQueryObject( "<PM123ASSOTIATIONS>" )) != NULLHANDLE ) {
    WinDestroyObject( hobject );
  }

  for( i = 0; i < types_count; i++ ) {
    asso_create( types_list + i );
  }

  if( need_term ) {
    asso_term( NULLHANDLE );
  }

  return TRUE;
}


/* Returns TRUE if the specified extension is
 * associated with PM123.
 */

BOOL
asso_is_registered( const char* ext )
{
  HOBJECT  hobject = asso_query_object_handle( ext );
  BOOL     rc = FALSE;
  ASSOLIST list;

  if( hobject ) {
    if( asso_query_filter_list( &list, ext ) != NULL ) {
      rc = asso_in_list( &list, hobject );
      free( list.ps );
    }
  }

  return rc;
}

/* Adds new type to the type list.
 */

static char*
ass_add_type( char* type )
{
  char  ext[_MAX_EXT];
  char* ps = type;
  char* pd = ext;
  int   i;

  if( type[0] != '*' || type[1] != '.' || !type[2] ) {
    while( *ps && *ps != ';' ) {
      ps++;
    }
    return ps;
  }

  ps += 2;
  while( *ps && *ps != ';' ) {
    *pd++ = *ps++;
  }
  *pd = 0;
  strupr( ext );

  for( i = 0; i < types_count; i++ ) {
    if( strcmp( types_list[i].ext, ext ) == 0 ) {
      return ps;
    }
  }

  types_list = realloc( types_list, sizeof( ASSOFILE ) * ( types_count + 1 ));
  if( !types_list ) {
    return NULL;
  }

  strlcpy( types_list[types_count].ext,  ext, sizeof( types_list[types_count].ext  ));
  strlcpy( types_list[types_count].type, ext, sizeof( types_list[types_count].type ));

  types_list[types_count].resid = ID_TYPE_AUDIO;
  types_list[types_count].hicon = NULLHANDLE;

  for( i = 0; i < sizeof( well_known ) / sizeof( well_known[0] ); i++ ) {
    if( stricmp( well_known[i].ext, ext ) == 0 ) {
      strlcpy( types_list[types_count].type, well_known[i].type, sizeof( types_list[types_count].type ));
      types_list[types_count].resid = well_known[i].resid;
      break;
    }
  }

  types_list[types_count].hicon = WinLoadPointer( HWND_DESKTOP, hmodule, types_list[types_count].resid );
  types_count++;

  return ps;
}

/* Initializes association container.
 */

BOOL
asso_init( HWND hcontainer )
{
  char  types[2048];
  char* p;

  asso_term( hcontainer );

  ass_add_type( "*.LST"  );
  ass_add_type( "*.M3U"  );
  ass_add_type( "*.M3U8" );
  ass_add_type( "*.MPL"  );
  ass_add_type( "*.PLS"  );
  ass_add_type( "*.CUE"  );

  dec_fill_types( types, sizeof( types ));

  for( p = types; p && *p; p++ ) {
    p = ass_add_type( p );
  }

  if( hcontainer )
  {
    ASSORECORD*  rec;
    RECORDINSERT ins;
    CNRINFO cnrinfo = {0};
    int i;

    cnrinfo.cb = sizeof(cnrinfo);
    cnrinfo.flWindowAttr = CV_ICON | CV_GRID | CA_DRAWICON;

    WinSendMsg( hcontainer, CM_SETCNRINFO, MPFROMP( &cnrinfo ), MPFROMLONG( CMA_FLWINDOWATTR ));

    ins.cb                = sizeof(RECORDINSERT);
    ins.pRecordOrder      = (PRECORDCORE)CMA_END;
    ins.pRecordParent     = NULL;
    ins.fInvalidateRecord = TRUE;
    ins.zOrder            = CMA_TOP;
    ins.cRecordsInsert    = 1;

    for( i = 0; i < types_count; i++ )
    {
      rec = (ASSORECORD*)WinSendMsg( hcontainer, CM_ALLOCRECORD,
                                     MPFROMLONG( sizeof( ASSORECORD ) - sizeof( RECORDCORE )),
                                     MPFROMLONG( 1 ));
      if( rec ) {
        rec->rc.pszIcon  = types_list[i].ext;
        rec->rc.hptrIcon = types_list[i].hicon;
        rec->file        = types_list + i;

        if( asso_is_registered( types_list[i].ext )) {
          rec->rc.flRecordAttr = CRA_INUSE;
        }
      }

      WinSendMsg( hcontainer, CM_INSERTRECORD, MPFROMP( rec ), MPFROMP( &ins ));
    }
  }

  return TRUE;
}

/* Terminates association container.
 */

BOOL
asso_term( HWND hcontainer )
{
  int i;

  if( hcontainer ) {
    WinSendMsg( hcontainer, CM_REMOVERECORD, NULL, MPFROM2SHORT( 0, CMA_FREE | CMA_INVALIDATE ));
  }

  for( i = 0; i < types_count; i++ ) {
    WinDestroyPointer( types_list[i].hicon );
  }

  free( types_list );
  types_list  = NULL;
  types_count = 0;

  return TRUE;
}

/* Returns TRUE if PM123 can be integrated to folder context menu.
 */

BOOL asso_can_integrated() {
  return ( WinQueryObject( "<XWP_CONFIG>" ) != NULLHANDLE );
}

/* Returns TRUE if PM123 is integrated to folder context menu.
 */

BOOL asso_is_integrated() {
  return ( WinQueryObject( "<PM123_PLAY_FOLDER>" ) != NULLHANDLE );
}

/* Integrates PM123 to folder context menu.
 */

BOOL asso_integrate( BOOL state )
{
  HOBJECT hobj = WinQueryObject( "<PM123_PLAY_FOLDER>" );
  char startup[CCHMAXPATH];

  strlcpy( startup, startpath, sizeof( startup ));
  if( *startup && !is_root( startup )) {
    startup[strlen(startup)-1] = 0;
  }

  if( state && !hobj )
  {
    char setup[2048];

    snprintf( setup, sizeof( setup ),
      "PROGTYPE=PM;EXENAME=%spm123.exe;OBJECTID=<PM123_PLAY_FOLDER>;CCVIEW=YES;STARTUPDIR=%s",
       startpath, startpath );

    hobj = WinCreateObject( "WPProgram", "Play in PM123", setup, "<XWP_CONFIG>", CO_REPLACEIFEXISTS );

    return ( hobj != NULLHANDLE );
  } else if( !state && hobj ) {
    return WinDestroyObject( hobj );
  }

  return TRUE;
}

