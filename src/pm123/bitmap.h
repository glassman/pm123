/*
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_BITMAP_H
#define PM123_BITMAP_H

#ifdef __cplusplus
extern "C" {
#endif

void bmp_init( void );
void bmp_term( void );

/* Loads a bitmap from a file, and returns the bitmap handle. */
HBITMAP bmp_load_bitmap( const char* filename );
/* Makes a bitmap from a file loaded to memory and returns the bitmap handle. */
HBITMAP bmp_make_bitmap( const char* filename, void* memptr, int size );
/* Makes a bitmap from a data loaded to memory, scale it to specified size and returns the bitmap handle. */
HBITMAP bmp_make_scaled_bitmap( const char* mimetype, void* memptr, int size, int w, int h );
/* Returns the size of a bitmap loaded into memory. */
BOOL bmp_query_bitmap_size( const char* mimetype, void* memptr, int size, int* w, int* h );

#ifdef __cplusplus
}
#endif
#endif /* PM123_BITMAP_H */
