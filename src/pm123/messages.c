/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2006-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_WIN
#define  INCL_GPI
#include <os2.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <string.h>
#include <debuglog.h>
#include <utilfct.h>

#include "pm123.h"
#include "plugman.h"
#include "messages.h"
#include "assertions.h"
#include "equalizer.h"
#include "skin.h"
#include "upnp.h"

static TID    broker_tid = 0;
static PQUEUE queue      = NULL;
static ULONG  rc         = 0;
static BOOL   busy       = FALSE;
static char   display[_MAX_URL + 128];

#define MSG_PLAY        0
#define MSG_STOP        1
#define MSG_PAUSE       2
#define MSG_FORWARD     3
#define MSG_REWIND      4
#define MSG_SEEK        5
#define MSG_SAVESTREAM  6
#define MSG_FILEINFO    7
#define MSG_TERMINATE   8

static OUTPUT_PARAMS  out_params = { sizeof( OUTPUT_PARAMS  )};
static DECODER_PARAMS dec_params = { sizeof( DECODER_PARAMS )};

static BOOL paused     = FALSE;
static BOOL forwarding = FALSE;
static BOOL rewinding  = FALSE;
static BOOL stopped    = TRUE;

static char proxyurl[_MAX_URL];
static char httpauth[_MAX_URL];
static char metadata[256];
static char savename[_MAX_URL];
static char filename[_MAX_URL];

static int  mp_pos;
static char mp_filename[_MAX_URL];
static char mp_decoder [_MAX_MODULE_NAME];
static int  mp_options;

static DECODER_INFO mp_fileinfo = { sizeof( DECODER_INFO )};

/* Returns TRUE if the player is paused. */
BOOL
is_paused( void ) {
  return paused;
}

/* Returns TRUE if the player is fast forwarding. */
BOOL
is_forward( void ) {
  return forwarding;
}

/* Returns TRUE if the player is rewinding. */
BOOL
is_rewind( void ) {
  return rewinding;
}

/* Returns TRUE if the output is always hungry. */
BOOL
is_always_hungry( void ) {
  return out_params.always_hungry;
}

/* Returns TRUE if the currently played
   stream is saved. */
BOOL
is_stream_saved( void ) {
  return *savename;
}

/* Returns TRUE if the engine already dispatch a message. */
BOOL
is_busy( void ) {
  return busy;
}

/* Returns TRUE if the player is completely stopped.
   It is not an antonym of the decoder_playing function. The
   completely stopping of a player means that any operations
   on plug-ins are safe. */
BOOL
is_stopped( void ) {
  return stopped;
}

/* Suspends or resumes playback of the currently played file. */
static BOOL
msg_m_pause( void )
{
  if( decoder_playing())
  {
    DEBUGLOG(( "pm123: initiating pause sequence.\n" ));
    out_params.pause = paused = !paused;

    if( paused ) {
      out_command( OUTPUT_PAUSE, &out_params );
    } else {
      out_command( OUTPUT_PAUSE, &out_params );
    }

    DEBUGLOG(( "pm123: completing pause sequence.\n" ));
  }
  return TRUE;
}

/* Toggles a fast forward of the currently played file. */
static BOOL
msg_m_forward( void )
{
  if( decoder_playing()) {
    if( rewinding ) {
      // Stop rewinding anyway.
      dec_params.rew = rewinding = FALSE;
      dec_command( DECODER_REW, &dec_params );
    }

    dec_params.ffwd = forwarding = !forwarding;

    if( dec_command( DECODER_FFWD, &dec_params ) != PLUGIN_OK ) {
      forwarding = FALSE;
    } else {
      // Going back in the stream to what is currently playing.
      dec_params.jumpto = out_playing_pos();
      dec_command( DECODER_JUMPTO, &dec_params );
      out_params.temp_playingpos = dec_params.jumpto;
      out_command( OUTPUT_TRASH_BUFFERS, &out_params );
    }
  }
  return TRUE;
}

/* Toggles a rewind of the currently played file. */
static BOOL
msg_m_rewind( void )
{
  if( decoder_playing()) {
    if( forwarding ) {
      // Stop forwarding anyway.
      dec_params.ffwd = forwarding = FALSE;
      dec_command( DECODER_FFWD, &dec_params );
    }

    dec_params.rew = rewinding = !rewinding;

    if( dec_command( DECODER_REW, &dec_params ) != PLUGIN_OK ) {
      rewinding = FALSE;
    } else {
      // Going back in the stream to what is currently playing.
      dec_params.jumpto = out_playing_pos();
      dec_command( DECODER_JUMPTO, &dec_params );
      out_params.temp_playingpos = dec_params.jumpto;
      out_command( OUTPUT_TRASH_BUFFERS, &out_params );
    }
  }
  return TRUE;
}

/* Stops decoding of the currently played file. */
static BOOL
msg_m_stop_decoder( BOOL synchronous )
{
  ULONG status = dec_status();
  ULONG rc;

  if( paused ) {
    out_set_volume( 0 );
    msg_m_pause();
  }

  if( status == DECODER_PLAYING  ||
      status == DECODER_STARTING ||
      status == DECODER_PAUSED   )
  {
    if( forwarding ) {
      dec_params.ffwd = forwarding = FALSE;
      dec_command( DECODER_FFWD, &dec_params );
    }
    if( rewinding ) {
      dec_params.rew = rewinding = FALSE;
      dec_command( DECODER_REW, &dec_params );
    }

    rc = dec_command( DECODER_STOP, &dec_params );

    if( rc != PLUGIN_OK && rc != PLUGIN_GO_ALREADY ) {
      return FALSE;
    }

    if( synchronous ) {
      status = dec_status();
      while( status == DECODER_PLAYING  ||
             status == DECODER_STARTING ||
             status == DECODER_PAUSED   )
      {
        DEBUGLOG(( "pm123: wait decoder stopping, dec_status=%d\n", status ));
        DosSleep(1);
        status = dec_status();
      }
    }
  } else {
    forwarding = FALSE;
    rewinding  = FALSE;
    paused     = FALSE;
  }

  return TRUE;
}

/* Stops playback of the currently played file. */
static BOOL
msg_m_stop( void )
{
  BOOL rc = FALSE;

  if( msg_m_stop_decoder( /*synchronous*/ FALSE )) {
    if( mp_options & MSG_STOP_AFTER_END ) {
      while( out_playing_data()) {
        DEBUGLOG(( "pm123: wait end of song, out_playing_data=%d\n",
                    out_playing_data()));
        DosSleep(1);
      }
    }

    if( out_command( OUTPUT_CLOSE, &out_params ) == PLUGIN_OK ) {
      rc = TRUE;
    }

    while( decoder_playing()) {
      DEBUGLOG(( "pm123: wait stopping, dec_status=%d, out_playing_data=%d\n",
                  dec_status(), out_playing_data()));
      DosSleep(1);
    }

    dec_set_filters( NULL );
    stopped = TRUE;
  }
  return rc;
}

/* Begins playback of the specified file. */
static BOOL
msg_m_play( void )
{
  char  cd_drive[3] = "";
  char  errorbuf[_MAX_URL+128];
  ULONG rc;

  if( !dec_is_active( current_decoder ) ||
       memcmp( &out_params.formatinfo, &current_info.format, sizeof( FORMAT_INFO )) != 0 )
  {
    if( !is_stopped()) {
      if( mp_options & MSG_PLAY_CONTINUOUS ) {
        while( out_playing_data()) {
          DEBUGLOG(( "pm123: wait output finishing, out_playing_data=%d\n",
                      out_playing_data()));
          DosSleep(1);
        }
      }

      msg_m_stop();
    }
  } else {
    if( !msg_m_stop_decoder( /*synchronous*/ TRUE )) {
      return FALSE;
    }
  }

  if( is_cuesheet( current_filename )) {
    dec_cuesheetinfo( current_filename, filename, NULL, NULL, 0 );
  } else {
    strlcpy( filename, current_filename, sizeof(filename));
  }

  if( is_stopped())
  {
    DEBUGLOG(( "pm123: activate new decoder: %s\n", current_decoder ));

    if( dec_set_active( current_decoder ) == PLUGIN_FAILED )
    {
      strlcpy( errorbuf, "Decoder module ", sizeof( errorbuf ));
      strlcat( errorbuf, current_decoder, sizeof( errorbuf ));
      strlcat( errorbuf, " needed to play ", sizeof( errorbuf ));
      strlcat( errorbuf, filename, sizeof( errorbuf ));
      strlcat( errorbuf, " is not loaded or enabled.", sizeof( errorbuf ));

      pm123_display_error( errorbuf );
      WinPostMsg( amp_player_window(), WM_PLAYERROR, 0, 0 );
      return FALSE;
    }
  } else if(!( mp_options & MSG_PLAY_CONTINUOUS )) {
    out_params.temp_playingpos = mp_pos;
    out_command( OUTPUT_TRASH_BUFFERS, &out_params );
  }

  out_params.size             = sizeof( out_params );
  out_params.hwnd             = amp_player_window();
  out_params.boostclass       = PRTYC_TIMECRITICAL;
  out_params.normalclass      = PRTYC_REGULAR;
  out_params.boostdelta       = 0;
  out_params.normaldelta      = 31;
  out_params.error_display    = pm123_display_error;
  out_params.info_display     = pm123_display_info;
  out_params.formatinfo       = current_info.format;
  out_params.info             = &current_info;
  out_params.buffersize       = 32784; // Must be an integer product of the 1, 2, 3 or 4 bytes.

  out_params.equalize_samples = equalize_samples;
  out_params.temp_playingpos  = mp_pos;

  rc = out_command( OUTPUT_SETUP, &out_params );

  if( rc == PLUGIN_NO_USABLE ) {
    pm123_display_error( "There is no any active output." );
    return FALSE;
  } else if( rc != PLUGIN_OK ) {
    return FALSE;
  }

  // Output plugin needs the original filename to correctly process
  // of the cues sheet tracks.
  out_params.filename = current_filename;

  if( out_command( OUTPUT_OPEN, &out_params ) != 0 ) {
    return FALSE;
  }

  if( *cfg.proxy_host ) {
    strlcpy( proxyurl, cfg.proxy_host, sizeof( proxyurl ));
    strlcat( proxyurl, ":", sizeof( proxyurl ));
    strlcat( proxyurl, ltoa( cfg.proxy_port, errorbuf, 10 ), sizeof( proxyurl ));
  } else {
    *proxyurl = 0;
  }

  if( *cfg.proxy_user || *cfg.proxy_pass ) {
    strlcpy( httpauth, cfg.proxy_user, sizeof( httpauth ));
    strlcat( httpauth, ":", sizeof( httpauth ));
    strlcat( httpauth, cfg.proxy_pass, sizeof( httpauth ));
  } else {
    *httpauth = 0;
  }

  dec_params.filename   = filename;
  dec_params.hwnd       = amp_player_window();
  dec_params.buffersize = cfg.buff_size * 1024;
  dec_params.bufferwait = cfg.buff_wait;
  dec_params.proxyurl   = proxyurl;
  dec_params.httpauth   = httpauth;
  dec_params.drive      = cd_drive;

  if( is_track( filename )) {
    sdrive( cd_drive, filename, sizeof( cd_drive ));
    dec_params.track = strack( filename );
  } else {
    dec_params.track = 0;
  }

  dec_params.audio_buffersize = out_params.buffersize;
  dec_params.error_display    = pm123_display_error;
  dec_params.info_display     = pm123_display_info;
  dec_params.metadata_buffer  = metadata;
  dec_params.metadata_size    = sizeof( metadata );

  if( is_stopped()) {
    dec_set_filters( &dec_params );
  }

  dec_command( DECODER_SETUP, &dec_params );

  dec_params.save_filename = is_stream_saved() ? savename : NULL;
  if( dec_command( DECODER_SAVEDATA, &dec_params ) != PLUGIN_OK && is_stream_saved()) {
    pm123_display_error( "The current active decoder don't support saving of a stream.\n" );
    *savename = 0;
  }

  dec_params.start  = current_info.start;
  dec_params.end    = current_info.end;
  dec_params.jumpto = mp_pos;
  rc = dec_command( DECODER_PLAY, &dec_params );

  if( rc != PLUGIN_OK && rc != PLUGIN_GO_ALREADY ) {
    msg_m_stop();
    return FALSE;
  }

  out_set_volume( cfg.defaultvol );
  stopped = FALSE;

  while( dec_status() == DECODER_STARTING ) {
    DosSleep(1);
  }

  return ( dec_status() != DECODER_ERROR );
}

/* Changes the current playing position of the currently
   played file. */
static BOOL
msg_m_seek( void )
{
  if( decoder_playing()) {
    dec_params.jumpto = mp_pos;
    dec_command( DECODER_JUMPTO, &dec_params );
  }
  return TRUE;
}

/* Toggles a saving of the currently played stream. */
static BOOL
msg_m_savestream( void )
{
  if( *savename ) {
    dec_params.save_filename = savename;
  } else {
    dec_params.save_filename = NULL;
  }

  if( decoder_playing()) {
    if( dec_command( DECODER_SAVEDATA, &dec_params ) != PLUGIN_OK && *savename ) {
      pm123_display_error( "The current active decoder doesn't support saving of a stream.\n" );
      *savename = 0;
    }
  }
  return TRUE;
}

/* Returns the information about the specified file. */
static ULONG
msg_m_fileinfo( void ) {
  return dec_fileinfo( mp_filename, &mp_fileinfo, mp_decoder, 0 );
}

/* Dispatches the player management requests. */
static void TFNENTRY
msg_m_broker( void* dummy )
{
  ULONG request;
  PVOID data;
  HAB   hab = WinInitialize( 0 );
  HMQ   hmq = WinCreateMsgQueue( hab, 0 );

  while( qu_read( queue, &request, &data ))
  {
    switch( request ) {
      case MSG_TERMINATE:
        break;
      case MSG_PLAY:       rc = msg_m_play      (); break;
      case MSG_STOP:       rc = msg_m_stop      (); break;
      case MSG_PAUSE:      rc = msg_m_pause     (); break;
      case MSG_FORWARD:    rc = msg_m_forward   (); break;
      case MSG_REWIND:     rc = msg_m_rewind    (); break;
      case MSG_SEEK:       rc = msg_m_seek      (); break;
      case MSG_SAVESTREAM: rc = msg_m_savestream(); break;
      case MSG_FILEINFO:   rc = msg_m_fileinfo  (); break;
    }

    busy = FALSE;

    if( request != MSG_SEEK       &&
        request != MSG_SAVESTREAM &&
        request != MSG_FILEINFO   &&
        request != MSG_TERMINATE  )
    {
      upnp_notify_state_change( UPNP_N_PLAYERSTATE | UPNP_N_TIMERS );
    }

    if( request == MSG_TERMINATE ) {
      break;
    }
  }

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
  _endthread();
}

static BOOL
msg_m_wait( void )
{
  QMSG qmsg;
  HWND hwnd     = amp_player_window();
  HAB  hab      = amp_player_hab();
  BOOL longwait = FALSE;

  char saved[_MAX_URL+128] = "";

  BOOL has_quit      = FALSE;
  BOOL has_outofdata = FALSE;
  BOOL has_playerror = FALSE;
  BOOL has_playstop  = FALSE;

  ASSERT_IS_MAIN_THREAD;
  WinStartTimer( hab, hwnd, TID_LONGWAIT, 1000 );

  DEBUGLOG(( "pm123: begin of waiting of `%s'\n", display ));

  while( busy ) {
    WinGetMsg( hab, &qmsg, 0, 0, 0 );
    switch( qmsg.msg ) {
      case WM_QUIT:
        has_quit = TRUE;
        continue;

      case WM_TIMER:
        if( qmsg.hwnd == hwnd && SHORT1FROMMP( qmsg.mp1 ) == TID_LONGWAIT ) {
          WinStopTimer( hab, hwnd, TID_LONGWAIT );
          strlcpy( saved, bmp_query_text(), sizeof( saved ));
          bmp_set_text( display );
          amp_invalidate( UPD_FILEINFO );
          longwait = TRUE;
          continue;
        }
        break;

      case WM_PLAYERROR:
        if( qmsg.hwnd == hwnd ) {
          has_playerror = TRUE;
          continue;
        }
        break;

      case WM_OUTPUT_OUTOFDATA:
        if( qmsg.hwnd == hwnd ) {
          DEBUGLOG(( "pm123: keep of WM_OUTPUT_OUTOFDATA [%d] message\n", LONGFROMMP(qmsg.mp1)));
          has_outofdata = TRUE;
          continue;
        }
        break;

      case WM_PLAYSTOP:
        if( qmsg.hwnd == hwnd ) {
          has_playstop = TRUE;
          continue;
        }
        break;
    }
    WinDispatchMsg( hab, &qmsg );
  }

  if( longwait ) {
    // If the displayed text is changed outside this waiting procedure
    // it is not necessary to restore previous text.
    if( strcmp( display, bmp_query_text()) == 0 ) {
      bmp_set_text( saved );
      amp_invalidate( UPD_FILEINFO );
    }
  } else {
    WinStopTimer( hab, hwnd, TID_LONGWAIT );
  }

  DEBUGLOG(( "pm123: end of `%s', rc=%d (%s)\n", display, rc, rc ? "TRUE" : "FALSE" ));

  if( has_quit ) {
    DEBUGLOG(( "pm123: reposts WM_QUIT message.\n" ));
    WinPostMsg( NULLHANDLE, WM_QUIT, 0, 0 );
  }
  if( has_playerror ) {
    DEBUGLOG(( "pm123: reposts WM_PLAYERROR message.\n" ));
    WinPostMsg( hwnd, WM_PLAYERROR, 0, 0 );
  }
  if( has_playstop ) {
    DEBUGLOG(( "pm123: reposts WM_PLAYSTOP message.\n" ));
    WinPostMsg( hwnd, WM_PLAYSTOP, 0, 0 );
  }
  if( has_outofdata ) {
    DEBUGLOG(( "pm123: reposts WM_OUTPUT_OUTOFDATA message.\n" ));
    WinPostMsg( hwnd, WM_OUTPUT_OUTOFDATA, 0, 0 );
  }

  return rc;
}

/* Begins playback of the currently loaded file.
   Must be called from the main thread. */
BOOL
msg_play( int pos, int options )
{
  ASSERT_IS_MAIN_THREAD;

  if( !busy ) {
    snprintf( display, sizeof( display ), "Starting %s...", current_filename );
    mp_pos = pos;
    mp_options = options;
    busy = TRUE;

    if( qu_write( queue, MSG_PLAY, NULL )) {
      return msg_m_wait();
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Stops playback of the currently played file.
   Must be called from the main thread. */
BOOL
msg_stop( int options )
{
  ASSERT_IS_MAIN_THREAD;

  if( !busy ) {
    snprintf( display, sizeof( display ), "Stopping %s...", current_filename );
    mp_options = options;
    busy = TRUE;

    if( qu_write( queue, MSG_STOP, NULL )) {
      return msg_m_wait();
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Suspends or resumes playback of the currently played file.
   Must be called from the main thread. */
BOOL
msg_pause( void )
{
  ASSERT_IS_MAIN_THREAD;

  if( !busy ) {
    snprintf( display, sizeof( display ), "Pausing %s...", current_filename );
    busy = TRUE;

    if( qu_write( queue, MSG_PAUSE, NULL )) {
      return msg_m_wait();
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Toggles a fast forward of the currently played file.
   Must be called from the main thread. */
BOOL
msg_forward( void )
{
  ASSERT_IS_MAIN_THREAD;

  if( !busy ) {
    snprintf( display, sizeof( display ), "Forwarding %s...", current_filename );
    busy = TRUE;

    if( qu_write( queue, MSG_FORWARD, NULL )) {
      return msg_m_wait();
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Toggles a rewind of the currently played file.
   Must be called from the main thread. */
BOOL
msg_rewind( void )
{
  ASSERT_IS_MAIN_THREAD;

  if( !busy ) {
    snprintf( display, sizeof( display ), "Rewinding %s...", current_filename );
    busy = TRUE;

    if( qu_write( queue, MSG_REWIND, NULL )) {
      return msg_m_wait();
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Changes the current playing position of the currently played file.
   Must be called from the main thread. */
BOOL
msg_seek( int pos )
{
  ASSERT_IS_MAIN_THREAD;

  if( !busy ) {
    snprintf( display, sizeof( display ), "Seeking %s...", current_filename );
    mp_pos = pos;
    busy = TRUE;

    if( qu_write( queue, MSG_SEEK, NULL )) {
      return msg_m_wait();
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Trashes all audio data received till this time. */
BOOL
msg_trash_buffers( int pos )
{
  ASSERT_IS_MAIN_THREAD;

  if( decoder_playing()) {
    out_params.temp_playingpos = pos;
    out_command( OUTPUT_TRASH_BUFFERS, &out_params );
  }
  return TRUE;
}

/* Toggles a saving of the currently played stream.
   Must be called from the main thread. */
BOOL
msg_savestream( const char* filename )
{
  ASSERT_IS_MAIN_THREAD;

  if( !busy ) {
    if( filename ) {
      strlcpy( savename, filename, sizeof( savename ));
    } else {
      *savename = 0;
    }

    snprintf( display, sizeof( display ), "Saving %s...", filename );
    busy = TRUE;

    if( qu_write( queue, MSG_SAVESTREAM, NULL )) {
      return msg_m_wait();
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Returns the information about the specified file. If the decoder
   name is not filled, also returns the decoder name that can play
   this file. Returns PLUGIN_OK if it successfully completes operation,
   returns PLUGIN_NO_PLAY if nothing can play that, or returns an error code
   returned by decoder module. Must be called from the main thread. */
ULONG
msg_fileinfo( const char* pathname, DECODER_INFO* info, char* name )
{
  ULONG rc = PLUGIN_NO_PLAY;
  ASSERT_IS_MAIN_THREAD;

  if( !busy )
  {
    strlcpy( mp_filename, pathname, sizeof( mp_filename ));
    strlcpy( mp_decoder, name, sizeof( mp_decoder ));
    // Because this structure is not used anywhere else, we simply reset
    // it to prevent the use of dec_cleaninfo inside plugin manager.
    memset( &mp_fileinfo, 0, sizeof( mp_fileinfo ));
    mp_fileinfo.size = sizeof( mp_fileinfo );

    snprintf( display, sizeof( display ), "Loading %s...", pathname );
    busy = TRUE;

    if( qu_write( queue, MSG_FILEINFO, NULL )) {
      rc = msg_m_wait();
      strcpy( name, mp_decoder );
      // Because this structure is not used anywhere else, we simply transfer
      // all data. The caller must be responsible for memory cleanup.
     *info = mp_fileinfo;
      return rc;
    } else {
      busy = FALSE;
    }
  }

  return FALSE;
}

/* Initializes the command management. Must be called
   from the main thread. */
void
msg_init( void )
{
  ASSERT_IS_MAIN_THREAD;
  queue = qu_create();

  if( !queue ) {
    amp_show_error( "Unable create command service queue." );
  } else {
    if(( broker_tid = _beginthread( msg_m_broker, NULL, 2048000, NULL )) == -1 ) {
      amp_show_error( "Unable to create the command service thread." );
    }
  }
}

/* Terminates the command management. Must be called
   from the main thread. */
void
msg_term( void )
{
  ASSERT_IS_MAIN_THREAD;

  qu_push( queue, MSG_TERMINATE, NULL );
  wait_thread( broker_tid, 2000 );
  qu_close( queue );
}

