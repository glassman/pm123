/*
 * Copyright 2009-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <ctype.h>

#include "hotkeys.h"
#include "pm123.h"
#include "playlist.h"
#include "pfreq.h"
#include "bookmark.h"

typedef struct  {
  USHORT context;
  USHORT uscode;
  USHORT fsflags;
  USHORT target;
  USHORT command;
} HOTKEY;

#define KC_VKEY KC_VIRTUALKEY
#define KC_SCAN KC_SCANCODE

static HOTKEY hotkeys[] =

  {{ HKW_PLAYER,    'O',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_LOAD_FILE    },
   { HKW_PLAYER,    'S',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_SAVE_STREAM  },
   { HKW_PLAYER,    VK_ENTER,     KC_VKEY,               HKW_PLAYER,    IDM_M_PLAY         },
   { HKW_PLAYER,    VK_F4,        KC_VKEY | KC_ALT,      HKW_PLAYER,    IDM_M_QUIT         },
   { HKW_PLAYER,    VK_F9,        KC_VKEY | KC_ALT,      HKW_PLAYER,    IDM_M_MINIMIZE     },
   { HKW_PLAYER,    VK_INSERT,    KC_VKEY,               HKW_PLAYLIST,  IDM_PL_ADD_FILES   },
   { HKW_PLAYER,    VK_NEWLINE,   KC_VKEY,               HKW_PLAYER,    IDM_M_PLAY         },
   { HKW_PLAYER,    VK_LEFT,      KC_VKEY | KC_PREVDOWN, HKW_PLAYER,    IDM_M_REW_5SEC     },
   { HKW_PLAYER,    VK_RIGHT,     KC_VKEY | KC_PREVDOWN, HKW_PLAYER,    IDM_M_FWD_5SEC     },

   { HKW_PLAYLIST,  'C',          KC_CHAR | KC_CTRL,     HKW_PLAYLIST,  IDM_PL_CLEAR       },
   { HKW_PLAYLIST,  'E',          KC_CHAR | KC_CTRL,     HKW_PLAYLIST,  IDM_RC_EDIT        },
   { HKW_PLAYLIST,  'O',          KC_CHAR | KC_CTRL,     HKW_PLAYLIST,  IDM_PL_OPEN_LIST   },
   { HKW_PLAYLIST,  'S',          KC_CHAR | KC_CTRL,     HKW_PLAYLIST,  IDM_PL_SAVE_LIST   },
   { HKW_PLAYLIST,  'T',          KC_CHAR | KC_ALT,      HKW_PLAYLIST,  IDM_PL_ADD_TRACKS  },
   { HKW_PLAYLIST,  'U',          KC_CHAR | KC_ALT,      HKW_PLAYLIST,  IDM_PL_ADD_URL     },
   { HKW_PLAYLIST,  VK_DELETE,    KC_VKEY,               HKW_PLAYLIST,  IDM_RC_REMOVE      },
   { HKW_PLAYLIST,  VK_DELETE,    KC_VKEY | KC_CTRL,     HKW_PLAYLIST,  IDM_RC_DELETE      },
   { HKW_PLAYLIST,  VK_ESC,       KC_VKEY,               HKW_PLAYLIST,  IDM_PL_CLOSE       },
   { HKW_PLAYLIST,  VK_INSERT,    KC_VKEY,               HKW_PLAYLIST,  IDM_PL_ADD_FILES   },
   { HKW_PLAYLIST,  'F',          KC_CHAR | KC_CTRL,     HKW_PLAYLIST,  IDM_RC_OPEN_FOLDER },

   { HKW_MANAGER,   'S',          KC_CHAR | KC_CTRL,     HKW_MANAGER,   IDM_PM_CALC        },
   { HKW_MANAGER,   VK_ESC,       KC_VKEY,               HKW_MANAGER,   IDM_PM_CLOSE       },
   { HKW_MANAGER,   VK_DELETE,    KC_VKEY,               HKW_MANAGER,   IDM_PM_L_REMOVE    },
   { HKW_MANAGER,   VK_DELETE,    KC_VKEY | KC_CTRL,     HKW_MANAGER,   IDM_PM_L_DELETE    },
   { HKW_MANAGER,   VK_INSERT,    KC_VKEY,               HKW_MANAGER,   IDM_PM_ADD         },

   { HKW_BOOKMARKS, 'N',          KC_CHAR | KC_CTRL,     HKW_BOOKMARKS, IDM_BM_RENAME      },
   { HKW_BOOKMARKS, 'A',          KC_CHAR | KC_CTRL,     HKW_BOOKMARKS, IDM_BM_ADDTOPL     },
   { HKW_BOOKMARKS, 'C',          KC_CHAR | KC_CTRL,     HKW_BOOKMARKS, IDM_BM_CLEAR       },
   { HKW_BOOKMARKS, 'B',          KC_CHAR | KC_CTRL,     HKW_BOOKMARKS, IDM_BM_ADD         },
   { HKW_BOOKMARKS, VK_DELETE,    KC_VKEY,               HKW_BOOKMARKS, IDM_BM_REMOVE      },
   { HKW_BOOKMARKS, VK_ESC,       KC_VKEY,               HKW_BOOKMARKS, IDM_BM_CLOSE       },

   { HKW_GLOBAL,    'B',          KC_CHAR | KC_ALT,      HKW_PLAYER,    IDM_M_BM_EDIT      },
   { HKW_GLOBAL,    'B',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_BM_ADD       },
   { HKW_GLOBAL,    'C',          KC_CHAR | KC_ALT,      HKW_PLAYER,    IDM_M_LOAD_DISC    },
   { HKW_GLOBAL,    'E',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_EDIT         },
   { HKW_GLOBAL,    'F',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_OPEN_FOLDER  },
   { HKW_GLOBAL,    'M',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_MANAGER      },
   { HKW_GLOBAL,    'P',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_PLAYLIST     },
   { HKW_GLOBAL,    'T',          KC_CHAR | KC_ALT,      HKW_PLAYER,    IDM_M_LOAD_TRACK   },
   { HKW_GLOBAL,    'Q',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_EQUALIZE     },
   { HKW_GLOBAL,    'U',          KC_CHAR | KC_ALT,      HKW_PLAYER,    IDM_M_LOAD_URL     },
   { HKW_GLOBAL,    'U',          KC_CHAR | KC_CTRL,     HKW_PLAYLIST,  IDM_PL_USE         },
   { HKW_GLOBAL,    '+',          KC_CHAR | KC_PREVDOWN, HKW_PLAYER,    IDM_M_VOL_RAISE    },
   { HKW_GLOBAL,    '=',          KC_CHAR | KC_PREVDOWN, HKW_PLAYER,    IDM_M_VOL_RAISE    },
   { HKW_GLOBAL,    '-',          KC_CHAR | KC_PREVDOWN, HKW_PLAYER,    IDM_M_VOL_LOWER    },
   { HKW_GLOBAL,    '-',          KC_CHAR | KC_CTRL,     HKW_PLAYER,    IDM_M_MUTE         },
   { HKW_GLOBAL,    VK_DOWN,      KC_VKEY | KC_CTRL,     HKW_PLAYER,    IDM_M_NEXT         },
   { HKW_GLOBAL,    VK_LEFT,      KC_VKEY | KC_CTRL,     HKW_PLAYER,    IDM_M_REW          },
   { HKW_GLOBAL,    VK_RIGHT,     KC_VKEY | KC_CTRL,     HKW_PLAYER,    IDM_M_FWD          },
   { HKW_GLOBAL,    VK_SPACE,     KC_VKEY,               HKW_PLAYER,    IDM_M_PAUSE        },
   { HKW_GLOBAL,    VK_UP,        KC_VKEY | KC_CTRL,     HKW_PLAYER,    IDM_M_PREV         },
   { HKW_GLOBAL,    VK_BACKSPACE, KC_VKEY | KC_PREVDOWN, HKW_PLAYER,    IDM_M_REW_5SEC     }
  };

static HWND windows[ HKW_MAX ];

/* Registers the specified window. */
void
hk_register( USHORT type, HWND hwnd )
{
  if( type < HKW_MAX ) {
    windows[ type ] = hwnd;
  }
}

/* Processes the WM_TRANSLATEACCEL message of the specified window. */
MRESULT
hk_translate( USHORT context, MPARAM mp1, MPARAM mp2 )
{
  PQMSG  pqmsg      = (PQMSG)mp1;
  USHORT fsflags    = SHORT1FROMMP( pqmsg->mp1 );
  USHORT usscancode = SHORT2FROMMP( pqmsg->mp1 ) >> 8;
  USHORT usch       = toupper( SHORT1FROMMP( pqmsg->mp2 ));
  USHORT usvk       = SHORT2FROMMP( pqmsg->mp2 );
  int    i;

  if( fsflags & ( KC_KEYUP | KC_DEADKEY | KC_INVALIDCOMP )) {
    return MRFROMLONG( FALSE );
  }

  fsflags &= ( KC_SHIFT      |
               KC_ALT        |
               KC_CTRL       |
               KC_CHAR       |
               KC_SCANCODE   |
               KC_PREVDOWN   |
               KC_VIRTUALKEY );

  for( i = 0; i < sizeof( hotkeys ) / sizeof( hotkeys[0] ); i++ ) {
    if( hotkeys[i].context == HKW_GLOBAL || hotkeys[i].context == context ) {
      if(( hotkeys[i].fsflags & ( KC_SHIFT | KC_ALT | KC_CTRL )) ==
                    ( fsflags & ( KC_SHIFT | KC_ALT | KC_CTRL )))
      {
        if( !( fsflags & KC_PREVDOWN ) || ( hotkeys[i].fsflags & KC_PREVDOWN ))
        {
          if((( hotkeys[i].fsflags & KC_SCANCODE   ) && ( hotkeys[i].uscode == usscancode )) ||
             (( hotkeys[i].fsflags & KC_VIRTUALKEY ) && ( hotkeys[i].uscode == usvk       )) ||
             (( hotkeys[i].fsflags & KC_CHAR       ) && ( hotkeys[i].uscode == usch       )))
          {
            pqmsg->hwnd = windows[ hotkeys[i].target ];
            pqmsg->msg  = WM_COMMAND;
            pqmsg->mp1  = MPFROMSHORT( hotkeys[i].command );
            pqmsg->mp2  = MPFROM2SHORT( CMDSRC_ACCELERATOR, FALSE );
            return MRFROMLONG( TRUE );
          }
        }
      }
    }
  }

  return MRFROMLONG( FALSE );
}

