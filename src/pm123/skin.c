/*
 * Copyright 1997-2003 Samuel Audet  <guardia@step.polymtl.ca>
 *                     Taneli Lepp�  <rosmo@sektori.com>
 *
 * Copyright 2004-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <memory.h>
#include <io.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "skin.h"
#include "pm123.h"
#include "plugman.h"
#include "playlist.h"
#include "pfreq.h"
#include "bookmark.h"
#include "button95.h"
#include "iniman.h"
#include "bitmap.h"
#include "banner.h"

HBITMAP bmp_cache[8000];
POINTL  bmp_pos  [1024];
ULONG   bmp_ulong[1024];

#define LCID_FONT 1L

static SKIN skin;

/* New characters needed: "[],%" 128 - 255 */
static char AMP_CHARSET[] = "abcdefghijklmnopqrstuvwxyz-_&.0123456789 ()��:,+%[]";
static int  LEN_CHARSET   = sizeof( AMP_CHARSET ) - 1;

/* The set of variables used during a text scroll. */
static char    s_display[512];
static int     s_len;
static int     s_sep_len;
static char*   s_sep    = " - ";
static int     s_offset;
static int     s_inc;
static int     s_pause;
static BOOL    s_done;
static HDC     s_dc     = NULLHANDLE;
static HPS     s_buffer = NULLHANDLE;
static HBITMAP s_bitmap = NULLHANDLE;

/* Buttons */
BMPBUTTON btn_play    = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_PLAY,       /* Pressed state bitmap for regular mode.         */
                          BMP_N_PLAY,     /* Release state bitmap for regular mode.         */
                          POS_R_PLAY,     /* Button position for regular mode.              */
                          BMP_S_PLAY,     /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_PLAY,    /* Release state bitmap for small and tiny modes. */
                          POS_S_PLAY,     /* Button position for small mode.                */
                          POS_T_PLAY,     /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          TRUE,           /* Is this a sticky button.                       */
                          "Starts playing" };

BMPBUTTON btn_stop    = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_STOP,       /* Pressed state bitmap for regular mode.         */
                          BMP_N_STOP,     /* Release state bitmap for regular mode.         */
                          POS_R_STOP,     /* Button position for regular mode.              */
                          BMP_S_STOP,     /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_STOP,    /* Release state bitmap for small and tiny modes. */
                          POS_S_STOP,     /* Button position for small mode.                */
                          POS_T_STOP,     /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Stops playing" };

BMPBUTTON btn_pause   = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_PAUSE,      /* Pressed state bitmap for regular mode.         */
                          BMP_N_PAUSE,    /* Release state bitmap for regular mode.         */
                          POS_R_PAUSE,    /* Button position for regular mode.              */
                          BMP_S_PAUSE,    /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_PAUSE,   /* Release state bitmap for small and tiny modes. */
                          POS_S_PAUSE,    /* Button position for small mode.                */
                          POS_T_PAUSE,    /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          TRUE,           /* Is this a sticky button.                       */
                          "Pauses the playback" };

BMPBUTTON btn_rew     = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_REW,        /* Pressed state bitmap for regular mode.         */
                          BMP_N_REW,      /* Release state bitmap for regular mode.         */
                          POS_R_REW,      /* Button position for regular mode.              */
                          BMP_S_REW,      /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_REW,     /* Release state bitmap for small and tiny modes. */
                          POS_S_REW,      /* Button position for small mode.                */
                          POS_T_REW,      /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          TRUE,           /* Is this a sticky button.                       */
                          "Seeks current song backward" };

BMPBUTTON btn_fwd     = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_FWD,        /* Pressed state bitmap for regular mode.         */
                          BMP_N_FWD,      /* Release state bitmap for regular mode.         */
                          POS_R_FWD,      /* Button position for regular mode.              */
                          BMP_S_FWD,      /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_FWD,     /* Release state bitmap for small and tiny modes. */
                          POS_S_FWD,      /* Button position for small mode.                */
                          POS_T_FWD,      /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          TRUE,           /* Is this a sticky button.                       */
                          "Seeks current song forward" };

BMPBUTTON btn_power   = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_POWER,      /* Pressed state bitmap for regular mode.         */
                          BMP_N_POWER,    /* Release state bitmap for regular mode.         */
                          POS_R_POWER,    /* Button position for regular mode.              */
                          BMP_S_POWER,    /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_POWER,   /* Release state bitmap for small and tiny modes. */
                          POS_S_POWER,    /* Button position for small mode.                */
                          POS_T_POWER,    /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Quits PM123" };

BMPBUTTON btn_prev    = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_PREV,       /* Pressed state bitmap for regular mode.         */
                          BMP_N_PREV,     /* Release state bitmap for regular mode.         */
                          POS_R_PREV,     /* Button position for regular mode.              */
                          BMP_S_PREV,     /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_PREV,    /* Release state bitmap for small and tiny modes. */
                          POS_S_PREV,     /* Button position for small mode.                */
                          POS_T_PREV,     /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Selects previous song in playlist" };

BMPBUTTON btn_next    = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_NEXT,       /* Pressed state bitmap for regular mode.         */
                          BMP_N_NEXT,     /* Release state bitmap for regular mode.         */
                          POS_R_NEXT,     /* Button position for regular mode.              */
                          BMP_S_NEXT,     /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_NEXT,    /* Release state bitmap for small and tiny modes. */
                          POS_S_NEXT,     /* Button position for small mode.                */
                          POS_T_NEXT,     /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Selects next song in playlist" };

BMPBUTTON btn_shuffle = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_SHUFFLE,    /* Pressed state bitmap for regular mode.         */
                          BMP_N_SHUFFLE,  /* Release state bitmap for regular mode.         */
                          POS_R_SHUFFLE,  /* Button position for regular mode.              */
                          BMP_S_SHUFFLE,  /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_SHUFFLE, /* Release state bitmap for small and tiny modes. */
                          POS_S_SHUFFLE,  /* Button position for small mode.                */
                          POS_T_SHUFFLE,  /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          TRUE,           /* Is this a sticky button.                       */
                          "Plays randomly from the playlist" };

BMPBUTTON btn_repeat  = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_REPEAT,     /* Pressed state bitmap for regular mode.         */
                          BMP_N_REPEAT,   /* Release state bitmap for regular mode.         */
                          POS_R_REPEAT,   /* Button position for regular mode.              */
                          BMP_S_REPEAT,   /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_REPEAT,  /* Release state bitmap for small and tiny modes. */
                          POS_S_REPEAT,   /* Button position for small mode.                */
                          POS_T_REPEAT,   /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          TRUE,           /* Is this a sticky button.                       */
                          "Toggles playlist/song repeat" };

BMPBUTTON btn_pl      = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_PL,         /* Pressed state bitmap for regular mode.         */
                          BMP_N_PL,       /* Release state bitmap for regular mode.         */
                          POS_R_PL,       /* Button position for regular mode.              */
                          BMP_S_PL,       /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_PL,      /* Release state bitmap for small and tiny modes. */
                          POS_S_PL,       /* Button position for small mode.                */
                          POS_T_PL,       /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Opens/closes playlist window" };

BMPBUTTON btn_fload   = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_FLOAD,      /* Pressed state bitmap for regular mode.         */
                          BMP_N_FLOAD,    /* Release state bitmap for regular mode.         */
                          POS_R_FLOAD,    /* Button position for regular mode.              */
                          BMP_S_FLOAD,    /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_FLOAD,   /* Release state bitmap for small and tiny modes. */
                          POS_S_FLOAD,    /* Button position for small mode.                */
                          POS_T_FLOAD,    /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Load a file" };

BMPBUTTON btn_hide    = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_HIDE,       /* Pressed state bitmap for regular mode.         */
                          BMP_N_HIDE,     /* Release state bitmap for regular mode.         */
                          POS_R_HIDE,     /* Button position for regular mode.              */
                          BMP_S_HIDE,     /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_HIDE,    /* Release state bitmap for small and tiny modes. */
                          POS_S_HIDE,     /* Button position for small mode.                */
                          POS_T_HIDE,     /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Hides PM123" };

BMPBUTTON btn_eq      = { NULLHANDLE,     /* Button window handle.                          */
                          BMP_EQ,         /* Pressed state bitmap for regular mode.         */
                          BMP_N_EQ,       /* Release state bitmap for regular mode.         */
                          POS_R_EQ,       /* Button position for regular mode.              */
                          BMP_S_EQ,       /* Pressed state bitmap for small and tiny modes. */
                          BMP_SN_EQ,      /* Release state bitmap for small and tiny modes. */
                          POS_S_EQ,       /* Button position for small mode.                */
                          POS_T_EQ,       /* Button position for tiny mode.                 */
                          0,              /* Button state.                                  */
                          FALSE,          /* Is this a sticky button.                       */
                          "Shows/hides sound equalizer" };

typedef struct _BUNDLEHDR
{
  int   resource;
  char  filename[128];
  ULONG length;

} BUNDLEHDR;

/* Converts time to two integer suitable for display by the timer. */
void
sec2num( long seconds, int* major, int* minor )
{
  int mi = seconds % 60;
  int ma = seconds / 60;

  if( ma > 99 ) {
    mi = ma % 60; // minutes
    ma = ma / 60; // hours
  }

  if( ma > 99 ) {
    mi = ma % 24; // hours
    ma = ma / 24; // days
  }

  *major = ma;
  *minor = mi;
}

/* Draws a bitmap using the current image colors and mixes. */
void
bmp_draw_bitmap( HPS hps, int x, int y, int res )
{
  POINTL aptlPoints[3];

  aptlPoints[0].x = x;
  aptlPoints[0].y = y;
  aptlPoints[1].x = x + bmp_cx(res) - 1;
  aptlPoints[1].y = y + bmp_cy(res) - 1;
  aptlPoints[2].x = 0;
  aptlPoints[2].y = 0;

  GpiWCBitBlt( hps, bmp_cache[res], 3, aptlPoints, ROP_SRCCOPY, BBO_IGNORE );
}

/* Draws a specified part of bitmap using the current image colors and mixes. */
void
bmp_draw_part_bitmap_to( HPS hps, int x1_1, int y1_1, int x1_2, int y1_2,
                                  int x2_1, int y2_1, int res )
{
  POINTL aptlPoints[3];

  aptlPoints[0].x = x1_1;
  aptlPoints[0].y = y1_1;
  aptlPoints[1].x = x1_2;
  aptlPoints[1].y = y1_2;
  aptlPoints[2].x = x2_1;
  aptlPoints[2].y = y2_1;

  GpiWCBitBlt( hps, bmp_cache[res], 3, aptlPoints, ROP_SRCCOPY, BBO_IGNORE );
}

/* Draws the player background. */
void
bmp_draw_background( HPS hps, HWND hwnd )
{
  RECTL  rcl;
  POINTL pos;

  WinQueryWindowRect( hwnd, &rcl );

  if( cfg.mode == CFG_MODE_SMALL && bmp_cache[BMP_S_BGROUND] ) {
    bmp_draw_bitmap( hps, 0, 0, BMP_S_BGROUND );
  } else if( cfg.mode == CFG_MODE_TINY && bmp_cache[BMP_T_BGROUND] ) {
    bmp_draw_bitmap( hps, 0, 0, BMP_T_BGROUND );
  } else {
    bmp_draw_bitmap( hps, 0, 0, BMP_R_BGROUND );
  }

  GpiCreateLogColorTable( hps, 0, LCOLF_RGB, 0, 0, 0 );

  if( bmp_ulong[ UL_SHADE_PLAYER ] )
  {
    bmp_draw_shade_out( hps, rcl.xLeft,
                             rcl.yBottom,
                             rcl.xRight - rcl.xLeft   - 1,
                             rcl.yTop   - rcl.yBottom - 1 );
  }

  if( cfg.mode == CFG_MODE_REGULAR && bmp_ulong[ UL_SHADE_VOLUME ] )
  {
    bmp_draw_shade_in( hps, 7, 36, 13, 50 );
  }

  if( cfg.mode == CFG_MODE_REGULAR && bmp_ulong[ UL_SHADE_STAT ] )
  {
    rcl.yBottom += 36;
    rcl.xLeft   += 26;
    rcl.xRight  -=  6;
    rcl.yTop    -=  6;

    bmp_draw_shade_in( hps, rcl.xLeft,
                            rcl.yBottom,
                            rcl.xRight - rcl.xLeft   - 1,
                            rcl.yTop   - rcl.yBottom - 1 );
  }

  if( cfg.mode == CFG_MODE_SMALL && bmp_ulong[ UL_SHADE_STAT ] )
  {
    rcl.yBottom += 36;
    rcl.xLeft   +=  6;
    rcl.xRight  -=  6;
    rcl.yTop    -=  6;

    bmp_draw_shade_in( hps, rcl.xLeft,
                            rcl.yBottom,
                            rcl.xRight - rcl.xLeft   - 1,
                            rcl.yTop   - rcl.yBottom - 1 );
  }

  if( cfg.mode == CFG_MODE_REGULAR && bmp_ulong[ UL_SHADE_SLIDER ] )
  {
    pos.x  = bmp_pos[ POS_SLIDER ].x - 1;
    pos.y  = bmp_pos[ POS_SLIDER ].y - 1;
    GpiMove( hps, &pos );
    pos.x += bmp_ulong[ UL_SLIDER_WIDTH ] + bmp_cx( BMP_SLIDER ) + 1;
    pos.y += bmp_cy( BMP_SLIDER ) + 1;
    GpiSetColor( hps, bmp_ulong[ UL_SLIDER_COLOR ]);
    GpiBox( hps, DRO_OUTLINE, &pos, 0, 0 );
  }
}

/* Draws the specified part of the player background. */
static void
bmp_draw_part_bg_to( HPS hps, int x1_1, int y1_1, int x1_2, int y1_2,
                              int x2_1, int y2_1 )
{
  POINTL aptlPoints[3];
  int    bg_id = BMP_R_BGROUND;

  if( cfg.mode == CFG_MODE_SMALL && bmp_cache[ BMP_S_BGROUND ] ) {
    bg_id = BMP_S_BGROUND;
  } else if( cfg.mode == CFG_MODE_TINY && bmp_cache[ BMP_T_BGROUND ] ) {
    bg_id = BMP_T_BGROUND;
  }

  aptlPoints[0].x = x1_1;
  aptlPoints[0].y = y1_1;
  aptlPoints[1].x = x1_2;
  aptlPoints[1].y = y1_2;
  aptlPoints[2].x = x2_1;
  aptlPoints[2].y = y2_1;

  GpiWCBitBlt( hps, bmp_cache[bg_id], 3, (PPOINTL)&aptlPoints, ROP_SRCCOPY, BBO_IGNORE );
}

/* Draws the specified part of the player background. */
void
bmp_draw_part_bg( HPS hps, int x1, int y1, int x2, int y2 ) {
  bmp_draw_part_bg_to( hps, x1, y1, x2, y2, x1, y1 );
}

/* Returns a width of the specified bitmap. */
int
bmp_cx( int id )
{
  BITMAPINFOHEADER2 hdr;

  hdr.cbFix = sizeof( BITMAPINFOHEADER2 );
  GpiQueryBitmapInfoHeader( bmp_cache[id], &hdr );
  return hdr.cx;
}

/* Returns a height of the specified bitmap. */
int
bmp_cy( int id )
{
  BITMAPINFOHEADER2 hdr;

  hdr.cbFix = sizeof( BITMAPINFOHEADER2 );
  GpiQueryBitmapInfoHeader( bmp_cache[id], &hdr );
  return hdr.cy;
}

/* Draws a activation led. */
void
bmp_draw_led( HPS hps, int active )
{
  if( cfg.mode != CFG_MODE_TINY )
  {
    if( active ) {
      if( bmp_pos[ POS_LED ].x != POS_UNDEF &&
          bmp_pos[ POS_LED ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_LED ].x,
                              bmp_pos[ POS_LED ].y, BMP_LED );
      }
    } else {
      if( bmp_pos[ POS_N_LED ].x != POS_UNDEF &&
          bmp_pos[ POS_N_LED ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_N_LED ].x,
                              bmp_pos[ POS_N_LED ].y, BMP_N_LED );
      }
    }
  }
}

/* Returns a resource id of the first symbol of current the selected font. */
static int
bmp_font( void )
{
  if( cfg.font == 0 ) {
    return BMP_FONT1;
  } else {
    return BMP_FONT2;
  }
}

/* Returns a width of the first symbol of the current selected font. */
static int
bmp_char_cx( void ) {
  return bmp_cx( bmp_font());
}

/* Returns a height of the first symbol of the current selected font. */
static int
bmp_char_cy( void ) {
  return bmp_cy( bmp_font());
}

/* Returns a bitmap identifier of the specified character
   of the current selected font. */
static int
bmp_char_id( char ch )
{
  int id;
  ch = tolower( ch );
  id = strchr ( AMP_CHARSET, ch ) - AMP_CHARSET;

  // Support for 127..255.
  if( id < 0 && ch > 126 ) {
      id = LEN_CHARSET + ch - 126 + 1;
  }

  if( id < 0 || bmp_cache[ bmp_font() + id ] == 0 ) {
      id = 40;
  }

  return bmp_font() + id;
}

/* Calculates a length of the current displayed text
   using the current selected font. */
static size_t
bmp_text_length( const char* string )
{
  int len = 0;

  if( cfg.font_skinned ) {
    while( *string ) {
      len += bmp_cx( bmp_char_id( *string++ ));
    }
  } else {
    RECTL rect = { 0, 0, 32000, 32000 };
    WinDrawText( s_buffer, -1, (PSZ)string, &rect, 0, 0, DT_LEFT | DT_VCENTER | DT_QUERYEXTENT );
    len = rect.xRight - rect.xLeft + 1;
  }

  return len;
}

/* Calculates a rectangle of the current displayed text
   using the current selected font. */
static RECTL
bmp_text_rect( void )
{
  RECTL rect;

  switch( cfg.mode ) {
    case CFG_MODE_REGULAR:

      rect.xLeft   = bmp_pos[ POS_R_TEXT ].x;
      rect.yBottom = bmp_pos[ POS_R_TEXT ].y;

      if( bmp_ulong[ UL_IN_PIXELS ] ) {
        rect.xRight = rect.xLeft   + bmp_ulong[ UL_R_MSG_LEN    ] - 1;
        rect.yTop   = rect.yBottom + bmp_ulong[ UL_R_MSG_HEIGHT ] - 1;
      } else {
        rect.xRight = rect.xLeft   + bmp_ulong[ UL_R_MSG_LEN ] * bmp_char_cx() - 1;
        rect.yTop   = rect.yBottom + bmp_char_cy() - 1;
      }
      break;

   case CFG_MODE_SMALL:

      rect.xLeft   = bmp_pos[ POS_S_TEXT ].x;
      rect.yBottom = bmp_pos[ POS_S_TEXT ].y;

      if( bmp_ulong[ UL_IN_PIXELS ] ) {
        rect.xRight = rect.xLeft   + bmp_ulong[ UL_S_MSG_LEN    ] - 1;
        rect.yTop   = rect.yBottom + bmp_ulong[ UL_S_MSG_HEIGHT ] - 1;
      } else {
        rect.xRight = rect.xLeft   + bmp_ulong[ UL_S_MSG_LEN ] * bmp_char_cx() - 1;
        rect.yTop   = rect.yBottom + bmp_char_cy() - 1;
      }
      break;

   default:
      rect.xLeft  = POS_UNDEF;
      rect.xRight = POS_UNDEF;
      break;
  }

  return rect;
}

/* Draws a current displayed text using the current selected font. */
void
bmp_draw_text( HPS hps )
{
  SIZEL  size;
  POINTL pos[3];
  RECTL  rect = bmp_text_rect();

  pos[0].x = rect.xLeft;
  pos[0].y = rect.yBottom;
  pos[1].x = rect.xRight + 1;
  pos[1].y = rect.yTop   + 1;
  pos[2].x = 0;
  pos[2].y = 0;

  // Someone might want to use their own scroller.
  if( rect.xLeft == POS_UNDEF && rect.yBottom == POS_UNDEF ) {
    return;
  }

  if( !GpiQueryBitmapDimension( s_bitmap, &size )) {
    return;
  }

  bmp_draw_part_bg_to( s_buffer, 0, 0, size.cx - 1, size.cy - 1,
                       rect.xLeft, rect.yBottom );

  if( cfg.font_skinned )
  {
    char* p =  s_display;
    int   x =  s_offset;
    int   y = (rect.yTop - rect.yBottom - bmp_char_cy()) / 2;
    int   i;

    while( *p ) {
      i = bmp_char_id( *p++ );
      bmp_draw_bitmap( s_buffer, x, y, i );
      x += bmp_cx( i );
    }

    if( !cfg.hither_thither && s_offset )
    {
      p = s_sep;
      while( *p ) {
        i = bmp_char_id( *p++ );
        bmp_draw_bitmap( s_buffer, x, y, i );
        x += bmp_cx( i );
      }
      p =  s_display;
      while( *p ) {
        i = bmp_char_id( *p++ );
        bmp_draw_bitmap( s_buffer, x, y, i );
        x += bmp_cx( i );
      }
    }

    i = bmp_char_id( ' ' );

    while( x < size.cx ) {
      bmp_draw_bitmap( s_buffer, x, y, i );
      x += bmp_cx( i );
    }
  }
  else
  {
    RECTL rect;

    rect.xLeft   = s_offset;
    rect.yBottom = 0;
    rect.xRight  = size.cx;
    rect.yTop    = size.cy;

    WinDrawText( s_buffer, -1, s_display, &rect, 0, 0,
                               DT_TEXTATTRS | DT_LEFT | DT_VCENTER );

    if( !cfg.hither_thither && s_offset ) {
      WinDrawText( s_buffer, -1, s_display, &rect, 0, 0,
                                 DT_TEXTATTRS | DT_QUERYEXTENT | DT_LEFT | DT_VCENTER );
      if( rect.xRight < size.cx ) {
        rect.xLeft   = rect.xRight + 1;
        rect.yBottom = 0;
        rect.xRight  = size.cx;
        rect.yTop    = size.cy;

        WinDrawText( s_buffer, -1, s_sep, &rect, 0, 0,
                                   DT_TEXTATTRS | DT_LEFT | DT_VCENTER );
        WinDrawText( s_buffer, -1, s_sep, &rect, 0, 0,
                                   DT_TEXTATTRS | DT_QUERYEXTENT | DT_LEFT | DT_VCENTER );
      }
      if( rect.xRight < size.cx ) {
        rect.xLeft   = rect.xRight + 1;
        rect.yBottom = 0;
        rect.xRight  = size.cx;
        rect.yTop    = size.cy;

        WinDrawText( s_buffer, -1, s_display, &rect, 0, 0,
                                   DT_TEXTATTRS | DT_LEFT | DT_VCENTER );
      }
    }
  }

  GpiBitBlt( hps, s_buffer, 3, pos, ROP_SRCCOPY, BBO_IGNORE );
}

/* Deletes a memory buffer used for drawing of the displayed text. */
static void
bmp_delete_text_buffer( void )
{
  if( s_buffer != NULLHANDLE ) {
    if( GpiQueryCharSet( s_buffer ) > 0 )
    {
      GpiSetCharSet ( s_buffer, LCID_DEFAULT );
      GpiDeleteSetId( s_buffer, LCID_FONT );
    }
  }
  if( s_bitmap != NULLHANDLE ) {
    GpiSetBitmap( s_buffer, NULLHANDLE );
    GpiDeleteBitmap( s_bitmap );
  }
  if( s_buffer != NULLHANDLE ) {
    GpiDestroyPS( s_buffer );
  }
  if( s_dc != NULLHANDLE ) {
    DevCloseDC( s_dc );
  }

  s_bitmap = NULLHANDLE;
  s_buffer = NULLHANDLE;
  s_dc     = NULLHANDLE;
}

/* Creates a memory buffer used for drawing of the displayed text. */
static void
bmp_create_text_buffer( void )
{
  RECTL rect = bmp_text_rect();
  SIZEL size;

  BITMAPINFOHEADER2 bmp_header;

  bmp_delete_text_buffer();

  size.cx = rect.xRight - rect.xLeft + 1;
  size.cy = rect.yTop - rect.yBottom + 1;

  s_dc = DevOpenDC( amp_player_hab(), OD_MEMORY, "*", 0, NULL, NULLHANDLE );
  s_buffer = GpiCreatePS( amp_player_hab(), s_dc, &size,
                          PU_PELS | GPIT_MICRO | GPIA_ASSOC );

  memset( &bmp_header, 0, sizeof( bmp_header ));
  bmp_header.cbFix      = sizeof( bmp_header );
  bmp_header.cx         = size.cx;
  bmp_header.cy         = size.cy;
  bmp_header.cPlanes    = 1;
  bmp_header.cBitCount  = 8;

  s_bitmap = GpiCreateBitmap( s_buffer, &bmp_header, 0, NULL, NULL );
  GpiSetBitmapDimension( s_bitmap, &size );
  GpiSetBitmap( s_buffer, s_bitmap );
  GpiCreateLogColorTable( s_buffer, 0, LCOLF_RGB, 0, 0, 0 );
}

/* Refresh the displayed text presentation attributes. */
void
bmp_refresh_font( void )
{
  s_offset =  0;
  s_inc    = -1;
  s_pause  = 20;
  s_done   = FALSE;

  if( cfg.mode != CFG_MODE_TINY ) {
    if( !s_buffer ) {
      bmp_create_text_buffer();
    }
    if( GpiQueryCharSet( s_buffer ) > 0 )
    {
      GpiSetCharSet ( s_buffer, LCID_DEFAULT );
      GpiDeleteSetId( s_buffer, LCID_FONT );
    }
    if( !cfg.font_skinned )
    {
      FATTRS attrs;
      SIZEF  charbox;
      LONG   size;

      fontname_to_attrs( cfg.fontname, &attrs, &size, &charbox );

      GpiSetColor( s_buffer, bmp_ulong[ UL_FG_MSG_COLOR ]);
      GpiSetBackMix( s_buffer, BM_LEAVEALONE );
      GpiCreateLogFont( s_buffer, NULL, LCID_FONT, &attrs );
      GpiSetCharSet( s_buffer, LCID_FONT );

      if( attrs.fsFontUse & FATTR_FONTUSE_OUTLINE ) {
        GpiSetCharBox( s_buffer, &charbox );
      }
    }
    s_len     = bmp_text_length( s_display );
    s_sep_len = bmp_text_length( s_sep );
  } else {
    s_len     = 0;
    s_sep_len = 0;
  }
}

/* Sets the new displayed text.
   The NULL can be used as argument for reset of a scroll status. */
void
bmp_set_text( const char* string )
{
  if( string && string != s_display ) {
    if( strcmp( s_display, string ) == 0 ) {
      return;
    } else {
      strlcpy( s_display, string, sizeof( s_display ));
    }
  }

  bmp_refresh_font();
  vis_broadcast( WM_PLUGIN_CONTROL, MPFROMLONG( PN_TEXTCHANGED ), 0 );
}

/* Returns a pointer to the current selected text. */
const char*
bmp_query_text( void ) {
  return s_display;
}

/* Returns a pointer to the recommended font name. */
const char*
bmp_default_fontname( void ) {
  return skin.fontname;
}

/* Scrolls the current selected text. */
BOOL
bmp_scroll_text( void )
{
  RECTL  s_rec = bmp_text_rect();
  size_t s_max = s_rec.xRight - s_rec.xLeft + 1;

  if( cfg.scroll == CFG_SCROLL_NONE ||
      cfg.mode   == CFG_MODE_TINY   || s_len < s_max )
  {
    if( s_offset != 0 ) {
      s_offset = 0;
      return TRUE;
    }

    return FALSE;
  }

  if( cfg.scroll == CFG_SCROLL_ONCE && s_done ) {
    return FALSE;
  } else {
    s_done = FALSE;
  }

  if( s_pause > 0 ) {
    --s_pause;
    return FALSE;
  }

  if( cfg.hither_thither ) {
    if( s_inc < 0 && s_len + s_offset <= s_max ) {
      s_inc   =  1;
      s_pause = 16;
      return FALSE;
    }

    if( s_inc > 0 && s_offset == 0 ) {
      if( cfg.scroll == CFG_SCROLL_ONCE ) {
        s_done  = TRUE;
      }
      s_inc   = -1;
      s_pause = 16;
      return FALSE;
    }

    s_offset += s_inc;
  } else {
    if( s_len + s_sep_len + s_offset <= 0 ) {
      if( cfg.scroll == CFG_SCROLL_ONCE ) {
        s_done   = TRUE;
        s_offset = 0;
      } else {
        s_offset = -1;
      }
    } else {
      s_offset -= 1;
    }
  }

  return TRUE;
}

/* Queries whether a point lies within a current displayed text rectangle. */
BOOL
bmp_pt_in_text( POINTL pos )
{
  RECTL rect = bmp_text_rect();

  if( pos.y >= rect.yBottom && pos.y <= rect.yTop &&
      pos.x >= rect.xLeft   && pos.x <= rect.xRight )
  {
    return TRUE;
  } else {
    return FALSE;
  }
}

/* Draws a specified digit using the specified size. */
void
bmp_draw_digit( HPS hps, int x, int y, int digit, int size ) {
  bmp_draw_bitmap( hps, x, y, size + digit );
}

static void
bmp_draw_box3d( HPS hps, int x, int y, int cx, int cy, long clr1, long clr2 )
{
  POINTL pos;

  pos.x = x;
  pos.y = y;
  GpiMove( hps, &pos );
  GpiSetColor( hps, clr2 );
  pos.x = x + cx;
  GpiLine( hps, &pos );
  GpiMove( hps, &pos );
  pos.y = y + cy;
  GpiLine( hps, &pos );
  pos.x = x;
  GpiSetColor( hps, clr1 );
  GpiLine( hps, &pos );
  GpiMove( hps, &pos );
  pos.y = y;
  GpiLine( hps, &pos );
}

void
bmp_draw_shade_in( HPS hps, int x, int y, int cx, int cy )
{
  bmp_draw_box3d( hps, x, y, cx, cy,
                  bmp_ulong[ UL_SHADE_DARK ], bmp_ulong[ UL_SHADE_BRIGHT ] );

  if( bmp_ulong[ UL_SHADE_BRIGHT2 ] == 0xFFFFFFFFUL ||
      bmp_ulong[ UL_SHADE_DARK2   ] == 0xFFFFFFFFUL )
  {
    bmp_draw_box3d( hps, x + 1, y + 1, cx - 2, cy - 2,
                    bmp_ulong[ UL_SHADE_DARK  ], bmp_ulong[ UL_SHADE_BRIGHT  ] );
  } else {
    bmp_draw_box3d( hps, x + 1, y + 1, cx - 2, cy - 2,
                    bmp_ulong[ UL_SHADE_DARK2 ], bmp_ulong[ UL_SHADE_BRIGHT2 ] );
  }
}

void
bmp_draw_shade_out( HPS hps, int x, int y, int cx, int cy )
{
  bmp_draw_box3d( hps, x + 1, y + 1, cx - 2, cy - 2,
                  bmp_ulong[ UL_SHADE_BRIGHT ], bmp_ulong[ UL_SHADE_DARK ] );
  if( bmp_ulong[ UL_SHADE_BRIGHT2 ] == 0xFFFFFFFFUL ||
      bmp_ulong[ UL_SHADE_DARK2   ] == 0xFFFFFFFFUL )
  {
    bmp_draw_box3d( hps, x, y, cx, cy,
                    bmp_ulong[ UL_SHADE_BRIGHT  ], bmp_ulong[ UL_SHADE_DARK  ] );
  } else {
    bmp_draw_box3d( hps, x, y, cx, cy,
                    bmp_ulong[ UL_SHADE_BRIGHT2 ], bmp_ulong[ UL_SHADE_DARK2 ] );
  }
}

/* Draws the main player timer. */
void
bmp_draw_timer( HPS hps, long time )
{
  int major;
  int minor;
  int x = bmp_pos[ POS_TIMER ].x;
  int y = bmp_pos[ POS_TIMER ].y;

  sec2num( time, &major, &minor );

  if( x != POS_UNDEF && y != POS_UNDEF )
  {
    bmp_draw_digit( hps, x, y, major / 10, DIG_BIG );
    x += bmp_cx( major / 10 + DIG_BIG ) + bmp_ulong[ UL_TIMER_SPACE ];
    bmp_draw_digit( hps, x, y, major % 10, DIG_BIG );
    x += bmp_cx( major % 10 + DIG_BIG ) + bmp_ulong[ UL_TIMER_SPACE ];

    if( bmp_ulong[ UL_TIMER_SEPARATE ] )
    {
      if( time % 2 == 0 ) {
        bmp_draw_digit( hps, x, y, 10, DIG_BIG );
        x += bmp_cx( 10 + DIG_BIG ) + bmp_ulong[ UL_TIMER_SPACE ];
      } else {
        bmp_draw_digit( hps, x, y, 11, DIG_BIG );
        x += bmp_cx( 11 + DIG_BIG ) + bmp_ulong[ UL_TIMER_SPACE ];
      }
    } else {
      x += max( bmp_ulong[ UL_TIMER_SPACE ], bmp_ulong[ UL_TIMER_SPACE ] * 2 );
    }

    bmp_draw_digit( hps, x, y, minor / 10, DIG_BIG );
    x += bmp_cx( minor / 10 + DIG_BIG ) + bmp_ulong[ UL_TIMER_SPACE ];
    bmp_draw_digit( hps, x, y, minor % 10, DIG_BIG );
  }
}

/* Draws the tiny player timer. */
void
bmp_draw_tiny_timer( HPS hps, int pos_id, long time )
{
  int major;
  int minor;
  int x = bmp_pos[pos_id].x;
  int y = bmp_pos[pos_id].y;

  sec2num( time, &major, &minor );

  if( x != POS_UNDEF && y != POS_UNDEF )
  {
    if( time > 0 )
    {
      bmp_draw_digit( hps, x, y, major / 10, DIG_TINY );
      x += bmp_cx( DIG_TINY + major / 10 );
      bmp_draw_digit( hps, x, y, major % 10, DIG_TINY );
      x += bmp_cx( DIG_TINY + major % 10 );
      bmp_draw_digit( hps, x, y, 10, DIG_TINY );
      x += bmp_cx( DIG_TINY + 10 );
      bmp_draw_digit( hps, x, y, minor / 10, DIG_TINY );
      x += bmp_cx( DIG_TINY + minor / 10 );
      bmp_draw_digit( hps, x, y, minor % 10, DIG_TINY );
    }
    else
    {
      bmp_draw_digit( hps, x, y, 11, DIG_TINY );
      x += bmp_cx( DIG_TINY + 11 );
      bmp_draw_digit( hps, x, y, 11, DIG_TINY );
      x += bmp_cx( DIG_TINY + 11 );
      bmp_draw_digit( hps, x, y, 12, DIG_TINY );
      x += bmp_cx( DIG_TINY + 12 );
      bmp_draw_digit( hps, x, y, 11, DIG_TINY );
      x += bmp_cx( DIG_TINY + 11 );
      bmp_draw_digit( hps, x, y, 11, DIG_TINY );
    }
  }
}

/* Draws the channels indicator. */
void
bmp_draw_channels( HPS hps, int channels )
{
  if( cfg.mode != CFG_MODE_REGULAR ) {
    return;
  }

  if( bmp_pos[ POS_NO_CHANNELS ].x != POS_UNDEF &&
      bmp_pos[ POS_NO_CHANNELS ].y != POS_UNDEF )
  {
    bmp_draw_part_bg( hps, bmp_pos[ POS_NO_CHANNELS ].x,
                           bmp_pos[ POS_NO_CHANNELS ].y,
                           bmp_pos[ POS_NO_CHANNELS ].x + bmp_cx( BMP_NO_CHANNELS ),
                           bmp_pos[ POS_NO_CHANNELS ].y + bmp_cy( BMP_NO_CHANNELS ));
  }
  if( bmp_pos[ POS_MONO ].x != POS_UNDEF &&
      bmp_pos[ POS_MONO ].y != POS_UNDEF )
  {
    bmp_draw_part_bg( hps, bmp_pos[ POS_MONO ].x,
                           bmp_pos[ POS_MONO ].y,
                           bmp_pos[ POS_MONO ].x + bmp_cx( BMP_MONO ),
                           bmp_pos[ POS_MONO ].y + bmp_cy( BMP_MONO ));
  }
  if( bmp_pos[ POS_STEREO ].x != POS_UNDEF &&
      bmp_pos[ POS_STEREO ].y != POS_UNDEF )
  {
    bmp_draw_part_bg( hps, bmp_pos[ POS_STEREO ].x,
                           bmp_pos[ POS_STEREO ].y,
                           bmp_pos[ POS_STEREO ].x + bmp_cx( BMP_STEREO ),
                           bmp_pos[ POS_STEREO ].y + bmp_cy( BMP_STEREO ));
  }

  switch( channels )
  {
    case -1:
      if( bmp_pos[ POS_NO_CHANNELS ].x != POS_UNDEF &&
          bmp_pos[ POS_NO_CHANNELS ].y != POS_UNDEF )
      {
        bmp_draw_bitmap ( hps, bmp_pos[ POS_NO_CHANNELS ].x,
                               bmp_pos[ POS_NO_CHANNELS ].y, BMP_NO_CHANNELS );
      }
      break;

    case  3:
      if( bmp_pos[ POS_MONO ].x != POS_UNDEF &&
          bmp_pos[ POS_MONO ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_MONO ].x,
                              bmp_pos[ POS_MONO ].y, BMP_MONO );
      }
      break;

    default:
      if( bmp_pos[ POS_STEREO ].x != POS_UNDEF &&
          bmp_pos[ POS_STEREO ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_STEREO ].x,
                              bmp_pos[ POS_STEREO ].y, BMP_STEREO );
      }
      break;
  }
}

/* Draws the volume bar and volume slider. */
void
bmp_draw_volume( HPS hps, int volume )
{
  int x = bmp_pos[ POS_VOLBAR ].x;
  int y = bmp_pos[ POS_VOLBAR ].y;
  int xo, yo, cx, cy;

  if( cfg.mode != CFG_MODE_REGULAR ) {
    return;
  }

  volume = limit2( volume, 0, 100 );
  cx     = bmp_cx( BMP_VOLBAR );
  cy     = bmp_cy( BMP_VOLBAR );

  if( bmp_ulong[ UL_VOLUME_BGS ] ) {
    bmp_draw_bitmap( hps, x, y, BMP_VOLBG + bmp_ulong[ UL_VOLUME_BGS ] * volume / 101 );
  } else {
    if( !bmp_ulong[ UL_VOLUME_SLIDER ] ) {
      if( bmp_ulong[ UL_VOLUME_HRZ ] ) {
        xo = volume * cx / 100;
        bmp_draw_part_bitmap_to( hps, x, y, x + xo - 1, y + cy - 1, 0, 0, BMP_VOLBAR );
        if( xo != cx ) {
          bmp_draw_part_bg( hps, x + xo, y, x + cx - 1, y + cy - 1 );
        }
      } else {
        yo = volume * cy / 100;
        bmp_draw_part_bitmap_to( hps, x, y, x + cx - 1, y + yo - 1, 0, 0, BMP_VOLBAR );
        if( yo != cy ) {
          bmp_draw_part_bg( hps, x, y + yo, x + cx - 1, y + cy - 1 );
        }
      }
    } else {
      bmp_draw_bitmap( hps, x, y, BMP_VOLBAR );
    }
  }

  if( bmp_ulong[ UL_VOLUME_SLIDER ] ) {
    if( !bmp_ulong[ UL_VOLUME_HRZ ] )
    {
      yo = volume * ( cy - bmp_cy( BMP_VOLSLIDER )) / 100;

      bmp_draw_bitmap( hps, x + bmp_pos[ POS_VOLSLIDER ].x,
                            y + bmp_pos[ POS_VOLSLIDER ].y + yo, BMP_VOLSLIDER );
    } else {
      xo = volume * ( cx - bmp_cx( BMP_VOLSLIDER )) / 100;

      bmp_draw_bitmap( hps, x + bmp_pos[ POS_VOLSLIDER ].x + xo,
                            y + bmp_pos[ POS_VOLSLIDER ].y, BMP_VOLSLIDER );
    }
  }
}

/* Queries whether a point lies within a volume bar rectangle. */
BOOL
bmp_pt_in_volume( POINTL pos )
{
  if( cfg.mode != CFG_MODE_REGULAR ) {
    return FALSE;
  }

  if( pos.x >= bmp_pos[ POS_VOLBAR ].x &&
      pos.x <= bmp_pos[ POS_VOLBAR ].x + bmp_cx( BMP_VOLBAR ) &&
      pos.y >= bmp_pos[ POS_VOLBAR ].y &&
      pos.y <= bmp_pos[ POS_VOLBAR ].y + bmp_cy( BMP_VOLBAR ))
  {
    return TRUE;
  } else {
    return FALSE;
  }
}

/* Calculates a volume level on the basis of position of the
   mouse pointer concerning the volume bar. */
int
bmp_calc_volume( POINTL pos )
{
  int volume;

  if( bmp_ulong[ UL_VOLUME_HRZ ]) {
    if( bmp_ulong[ UL_VOLUME_SLIDER ]) {
      volume = ( pos.x - bmp_pos[ POS_VOLBAR ].x - bmp_cx( BMP_VOLSLIDER ) / 2 + 1 ) * 100 / ( bmp_cx( BMP_VOLBAR ) - bmp_cx( BMP_VOLSLIDER ));
    } else {
      volume = ( pos.x - bmp_pos[ POS_VOLBAR ].x + 1 ) * 100 / bmp_cx( BMP_VOLBAR );
    }
  } else {
    if( bmp_ulong[ UL_VOLUME_SLIDER ]) {
      pos.y -= bmp_cy( BMP_VOLSLIDER ) / 2;
      volume = ( pos.y - bmp_pos[ POS_VOLBAR ].y - bmp_cy( BMP_VOLSLIDER ) / 2 + 1 ) * 100 / ( bmp_cy( BMP_VOLBAR ) - bmp_cy( BMP_VOLSLIDER ));
    } else {
      volume = ( pos.y - bmp_pos[ POS_VOLBAR ].y + 1 ) * 100 / bmp_cy( BMP_VOLBAR );
    }
  }

  if( volume > 100 ) {
      volume = 100;
  }
  if( volume < 0   ) {
      volume = 0;
  }

  return volume;
}

/* Draws the file bitrate. */
void
bmp_draw_rate( HPS hps, int rate )
{
  int  rate_index[] = { 0, 32, 48, 56, 64, 80, 96, 112, 128, 144, 160, 176, 192, 224, 256, 320 };
  int  i, index = 0;
  char buf[32];
  int  x = bmp_pos[ POS_BPS ].x;
  int  y = bmp_pos[ POS_BPS ].y;

  if( cfg.mode != CFG_MODE_REGULAR ) {
    return;
  }

  for( i = 0; i < sizeof( rate_index ) / sizeof( int ); i++ ) {
    if( rate_index[i] == rate ) {
      index = i;
      break;
    }
  }

  if( !bmp_ulong[ UL_BPS_DIGITS ]) {
    bmp_draw_bitmap( hps, x, y, BMP_BPS + index );
  }
  else
  {
    if( bmp_cache[ DIG_BPS + 10 ] ) {
      bmp_draw_part_bg( hps, x, y,
                             x + bmp_cx( DIG_BPS ) * 3,
                             y + bmp_cy( DIG_BPS ));
    } else {
      bmp_draw_part_bg( hps, x - bmp_cx( DIG_BPS ) * 2,
                             y,
                             x + bmp_cx( DIG_BPS ) * 3,
                             y + bmp_cy( DIG_BPS ));
    }

    if( rate > 0 )
    {
      x += bmp_cx( DIG_BPS ) * 2;

      if( rate > 999 && bmp_cache[ DIG_BPS + 10 ] ) {
        sprintf( buf, "%u", rate / 100 );
        bmp_draw_digit( hps, x, y, 10, DIG_BPS );
        x -= bmp_cx( DIG_BPS );
      } else {
        sprintf( buf, "%u", rate );
      }

      for( i = strlen( buf ) - 1; i >= 0; i-- )
      {
        if( buf[i] != ' ' ) {
          bmp_draw_digit( hps, x, y, buf[i] - 48, DIG_BPS );
          x -= bmp_cx( DIG_BPS );
        }
      }
    }
  }
}

/* Draws the current playlist mode. */
void
bmp_draw_plmode( HPS hps )
{
  if( cfg.mode == CFG_MODE_REGULAR && bmp_pos[ POS_PL_MODE ].x != POS_UNDEF &&
                                      bmp_pos[ POS_PL_MODE ].y != POS_UNDEF )
  {
    switch( amp_playmode ) {
      case AMP_PLAYLIST:
        bmp_draw_bitmap( hps, bmp_pos[ POS_PL_MODE ].x,
                              bmp_pos[ POS_PL_MODE ].y, BMP_LISTPLAY );
        break;

     case AMP_SINGLE:
        bmp_draw_bitmap( hps, bmp_pos[ POS_PL_MODE ].x,
                              bmp_pos[ POS_PL_MODE ].y, BMP_SINGLEPLAY );
        break;

      case AMP_NOFILE:
        bmp_draw_bitmap( hps, bmp_pos[ POS_PL_MODE ].x,
                              bmp_pos[ POS_PL_MODE ].y, BMP_NOFILE );
        break;
    }
  }
}

/* Draws the current playlist index. */
void
bmp_draw_plind( HPS hps, int index, int total )
{
  int  i, x, y;
  char buf[64];

  if( cfg.mode != CFG_MODE_REGULAR ) {
    return;
  }

  if( bmp_ulong[ UL_PL_INDEX ] )
  {
    if( bmp_pos[ POS_PL_INDEX ].x != POS_UNDEF &&
        bmp_pos[ POS_PL_INDEX ].y != POS_UNDEF )
    {
      y = bmp_pos[ POS_PL_INDEX ].y;
      x = bmp_pos[ POS_PL_INDEX ].x;

      bmp_draw_part_bg( hps, x - bmp_cx( DIG_PL_INDEX ),
                             y,
                             x + bmp_cx( DIG_PL_INDEX ) * 3,
                             y + bmp_cy( DIG_PL_INDEX ));

      if( amp_playmode == AMP_PLAYLIST && index > 0 )
      {
        sprintf( buf, "%3u", index );
        x += bmp_cx( DIG_PL_INDEX ) * 2;

        for( i = strlen( buf ) - 1; i >= 0; i-- )
        {
          if( buf[i] != ' ' ) {
            bmp_draw_digit( hps, x, y, buf[i] - 48, DIG_PL_INDEX );
          }
          x -= bmp_cx( DIG_PL_INDEX );
        }
      }
    }

    if( bmp_pos[ POS_PL_TOTAL ].x != POS_UNDEF &&
        bmp_pos[ POS_PL_TOTAL ].y != POS_UNDEF )
    {
      y = bmp_pos[ POS_PL_TOTAL ].y;
      x = bmp_pos[ POS_PL_TOTAL ].x;

      bmp_draw_part_bg( hps, x - bmp_cx( DIG_PL_INDEX ),
                             y,
                             x + bmp_cx( DIG_PL_INDEX ) * 3,
                             y + bmp_cy( DIG_PL_INDEX ));

      if( amp_playmode == AMP_PLAYLIST )
      {
        sprintf( buf, "%3u", total );
        x += bmp_cx( DIG_PL_INDEX ) * 2;

        for( i = strlen( buf ) - 1; i >= 0; i-- )
        {
          if( buf[i] != ' ' ) {
            bmp_draw_digit( hps, x, y, buf[i] - 48, DIG_PL_INDEX );
          }
          x -= bmp_cx( DIG_PL_INDEX );
        }
      }
    }
  }
  else
  {
    POINTL pos = { 138, 50 };

    sprintf( buf, "%02u of %02u", index, total );
    bmp_draw_part_bg( hps, pos.x, pos.y, pos.x + 50, pos.y + 20 );

    if( amp_playmode == AMP_PLAYLIST )
    {
      GpiCreateLogColorTable( hps, 0, LCOLF_RGB, 0, 0, 0 );
      GpiSetColor( hps, bmp_ulong[ UL_PL_COLOR ]);
      GpiSetBackColor( hps, 0x00000000UL );
      GpiMove( hps, &pos );
      GpiCharString( hps, strlen( buf ), buf );
    }
  }
}

/* Draws the current position slider. */
void
bmp_draw_slider( HPS hps, int played, int total )
{
  if( cfg.mode == CFG_MODE_REGULAR )
  {
    int pos;

    if( total ) {
      pos = (((float)played / (float)total ) * bmp_ulong[UL_SLIDER_WIDTH] );

      if( pos < 0 ) {
          pos = 0;
      }
      if( pos > bmp_ulong[ UL_SLIDER_WIDTH ] ) {
          pos = bmp_ulong[ UL_SLIDER_WIDTH ];
      }
    } else {
      pos = 0;
    }

    if( bmp_cache[ BMP_SLIDER_SHAFT ]   != 0 &&
        bmp_pos  [ POS_SLIDER_SHAFT ].x != POS_UNDEF &&
        bmp_pos  [ POS_SLIDER_SHAFT ].y != POS_UNDEF )
    {
      if( total > 0 && !bmp_ulong[ UL_NO_SEEK_SLIDER ] ) {
        if( pos > 0 ) {
          bmp_draw_part_bitmap_to( hps, bmp_pos[ POS_SLIDER_SHAFT ].x,
                                        bmp_pos[ POS_SLIDER_SHAFT ].y,
                                        bmp_pos[ POS_SLIDER_SHAFT ].x + pos - 1,
                                        bmp_pos[ POS_SLIDER_SHAFT ].y + bmp_cy( BMP_SLIDER_SHAFT ) - 1,
                                        0, 0, BMP_SLIDER_SHAFT );
        }

        bmp_draw_bitmap( hps, bmp_pos[ POS_SLIDER ].x + pos,
                              bmp_pos[ POS_SLIDER ].y, BMP_SLIDER );

        if( pos < bmp_ulong[ UL_SLIDER_WIDTH ] ) {
          bmp_draw_part_bitmap_to( hps, bmp_pos[ POS_SLIDER_SHAFT ].x + pos + bmp_cx( BMP_SLIDER ),
                                        bmp_pos[ POS_SLIDER_SHAFT ].y,
                                        bmp_pos[ POS_SLIDER_SHAFT ].x + bmp_cx( BMP_SLIDER_SHAFT ) - 1,
                                        bmp_pos[ POS_SLIDER_SHAFT ].y + bmp_cy( BMP_SLIDER_SHAFT ) - 1,
                                        pos + bmp_cx( BMP_SLIDER ), 0, BMP_SLIDER_SHAFT );
        }
      } else {
        bmp_draw_bitmap( hps, bmp_pos[ POS_SLIDER_SHAFT ].x,
                              bmp_pos[ POS_SLIDER_SHAFT ].y, BMP_SLIDER_SHAFT );
      }
    }
    else
    {
      if( total > 0 && !bmp_ulong[ UL_NO_SEEK_SLIDER ] ) {
        if( pos > 0 ) {
          bmp_draw_part_bg( hps, bmp_pos[ POS_SLIDER ].x,
                                 bmp_pos[ POS_SLIDER ].y,
                                 bmp_pos[ POS_SLIDER ].x + pos - 1,
                                 bmp_pos[ POS_SLIDER ].y + bmp_cy( BMP_SLIDER ) - 1 );
        }

        bmp_draw_bitmap( hps, bmp_pos[ POS_SLIDER ].x + pos,
                              bmp_pos[ POS_SLIDER ].y, BMP_SLIDER );

        if( pos < bmp_ulong[ UL_SLIDER_WIDTH ] ) {
          bmp_draw_part_bg( hps, bmp_pos[ POS_SLIDER ].x + pos + bmp_cx( BMP_SLIDER ),
                                 bmp_pos[ POS_SLIDER ].y,
                                 bmp_pos[ POS_SLIDER ].x + bmp_ulong[ UL_SLIDER_WIDTH ] + bmp_cx( BMP_SLIDER ) - 1,
                                 bmp_pos[ POS_SLIDER ].y + bmp_cy( BMP_SLIDER ) - 1 );
        }
      } else {
        bmp_draw_part_bg( hps, bmp_pos[ POS_SLIDER ].x,
                               bmp_pos[ POS_SLIDER ].y,
                               bmp_pos[ POS_SLIDER ].x + bmp_ulong[ UL_SLIDER_WIDTH ] + bmp_cx( BMP_SLIDER ) - 1,
                               bmp_pos[ POS_SLIDER ].y + bmp_cy( BMP_SLIDER ) - 1 );
      }
    }

  }
}

/* Calculates a current seeking time on the basis of position of the
   mouse pointer concerning the position slider. */
int
bmp_calc_time( POINTL pos, int total )
{
  int time = 0;

  pos.x -= bmp_cx( BMP_SLIDER ) / 2;

  if( bmp_ulong[ UL_SLIDER_WIDTH ] && pos.x >= bmp_pos[ POS_SLIDER ].x )
  {
    time = total * ( pos.x - bmp_pos[ POS_SLIDER ].x ) / bmp_ulong[ UL_SLIDER_WIDTH ];

    if( time < 0 ) {
      time = 0;
    }
    if( time > total ) {
      time = total;
    }
  }

  return time;
}

/* Queries whether a point lies within a position slider rectangle. */
BOOL
bmp_pt_in_slider( POINTL pos )
{
  if( cfg.mode != CFG_MODE_REGULAR || bmp_ulong[ UL_NO_SEEK_SLIDER ] ) {
    return FALSE;
  }

  if( pos.x >= bmp_pos[ POS_SLIDER ].x &&
      pos.x <= bmp_pos[ POS_SLIDER ].x + bmp_ulong[ UL_SLIDER_WIDTH ] + bmp_cx( BMP_SLIDER ) &&
      pos.y >= bmp_pos[ POS_SLIDER ].y - ( bmp_ulong[ UL_SHADE_SLIDER ] ? 1 : 0 ) &&
      pos.y <= bmp_pos[ POS_SLIDER ].y + ( bmp_ulong[ UL_SHADE_SLIDER ] ? 1 : 0 ) + bmp_cy( BMP_SLIDER ))
  {
    return TRUE;
  } else {
    return FALSE;
  }
}

/* Draws the time left and playlist left labels. */
void
bmp_draw_timeleft( HPS hps )
{
  if( cfg.mode == CFG_MODE_REGULAR )
  {
    if( amp_playmode == AMP_NOFILE ) {
      if( bmp_pos[ POS_NOTL ].x != POS_UNDEF &&
          bmp_pos[ POS_NOTL ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_NOTL ].x,
                              bmp_pos[ POS_NOTL ].y, BMP_NOTL );
      }
    } else {
      if( bmp_pos[ POS_TL ].x != POS_UNDEF &&
          bmp_pos[ POS_TL ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_TL   ].x,
                              bmp_pos[ POS_TL   ].y, BMP_TL );
      }
    }

    if( amp_playmode == AMP_PLAYLIST && !cfg.rpt ) {
      if( bmp_pos[ POS_PLIST   ].x != POS_UNDEF &&
          bmp_pos[ POS_PLIST   ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_PLIST   ].x,
                              bmp_pos[ POS_PLIST   ].y, BMP_PLIST );
      }
    } else {
      if( bmp_pos[ POS_NOPLIST ].x != POS_UNDEF &&
          bmp_pos[ POS_NOPLIST ].y != POS_UNDEF )
      {
        bmp_draw_bitmap( hps, bmp_pos[ POS_NOPLIST ].x,
                              bmp_pos[ POS_NOPLIST ].y, BMP_NOPLIST );
      }
    }
  }
}

/* Loads specified bitmap to the bitmap cache if it is
   not loaded before. */
static void
bmp_load_default( HPS hps, int id )
{
  if( bmp_cache[ id ] == 0 )
  {
    HBITMAP hbitmap = GpiLoadBitmap( hps, hmodule, id, 0, 0 );

    if( hbitmap != GPI_ERROR ) {
      bmp_cache[ id ] = hbitmap;
    }
  }
}

/* Loads default bitmaps to the bitmap cache. */
static void
bmp_init_skins_bitmaps( HPS hps )
{
  int i;

  for( i = DIG_SMALL; i < DIG_SMALL + 10; i++ ) {
    bmp_load_default( hps, i );
  }
  for( i = DIG_BIG; i < DIG_BIG + 12; i++ ) {
    bmp_load_default( hps, i );
  }
  for( i = DIG_TINY; i < DIG_TINY + 13; i++ ) {
    bmp_load_default( hps, i );
  }
  for( i = DIG_PL_INDEX; i <   DIG_PL_INDEX + 10; i++ ) {
    bmp_load_default( hps, i );
  }
  for( i = DIG_BPS; i < DIG_BPS + 10; i++ ) {
    bmp_load_default( hps, i );
  }

  bmp_load_default( hps, BMP_PLAY       );
  bmp_load_default( hps, BMP_PAUSE      );
  bmp_load_default( hps, BMP_REW        );
  bmp_load_default( hps, BMP_FWD        );
  bmp_load_default( hps, BMP_POWER      );
  bmp_load_default( hps, BMP_PREV       );
  bmp_load_default( hps, BMP_NEXT       );
  bmp_load_default( hps, BMP_SHUFFLE    );
  bmp_load_default( hps, BMP_REPEAT     );
  bmp_load_default( hps, BMP_PL         );
  bmp_load_default( hps, BMP_FLOAD      );
  bmp_load_default( hps, BMP_HIDE       );
  bmp_load_default( hps, BMP_EQ         );

  bmp_load_default( hps, BMP_N_PLAY     );
  bmp_load_default( hps, BMP_N_PAUSE    );
  bmp_load_default( hps, BMP_N_REW      );
  bmp_load_default( hps, BMP_N_FWD      );
  bmp_load_default( hps, BMP_N_POWER    );
  bmp_load_default( hps, BMP_N_PREV     );
  bmp_load_default( hps, BMP_N_NEXT     );
  bmp_load_default( hps, BMP_N_SHUFFLE  );
  bmp_load_default( hps, BMP_N_REPEAT   );
  bmp_load_default( hps, BMP_N_PL       );
  bmp_load_default( hps, BMP_N_FLOAD    );
  bmp_load_default( hps, BMP_N_HIDE     );
  bmp_load_default( hps, BMP_N_EQ       );

  for( i = BMP_FONT1; i < BMP_FONT1 + 45; i++ ) {
    bmp_load_default( hps, i );
  }

  for( i = BMP_FONT2; i < BMP_FONT2 + 45; i++ ) {
    bmp_load_default( hps, i );
  }

  bmp_load_default( hps, BMP_STEREO       );
  bmp_load_default( hps, BMP_MONO         );
  bmp_load_default( hps, BMP_NO_CHANNELS  );
  bmp_load_default( hps, BMP_SLIDER       );
  bmp_load_default( hps, BMP_S_BGROUND    );
  bmp_load_default( hps, BMP_T_BGROUND    );
  bmp_load_default( hps, BMP_VOLSLIDER    );
  bmp_load_default( hps, BMP_VOLBAR       );
  bmp_load_default( hps, BMP_SINGLEPLAY   );
  bmp_load_default( hps, BMP_LISTPLAY     );
  bmp_load_default( hps, BMP_NOFILE       );
  bmp_load_default( hps, BMP_LED          );
  bmp_load_default( hps, BMP_N_LED        );

  for( i = BMP_BPS; i < BMP_BPS + 16; i++ ) {
    bmp_load_default( hps, i );
  }

  bmp_load_default( hps, BMP_R_BGROUND    );
  bmp_load_default( hps, BMP_NOTL         );
  bmp_load_default( hps, BMP_TL           );
  bmp_load_default( hps, BMP_NOPLIST      );
  bmp_load_default( hps, BMP_PLIST        );

  bmp_load_default( hps, BMP_TLFT_CRN_INA );
  bmp_load_default( hps, BMP_TLFT_INA     );
  bmp_load_default( hps, BMP_LFT_TTL_INA  );
  bmp_load_default( hps, BMP_TTL_INA      );
  bmp_load_default( hps, BMP_RGT_TTL_INA  );
  bmp_load_default( hps, BMP_TRGT_INA     );
  bmp_load_default( hps, BMP_TRGT_CRN_INA );
  bmp_load_default( hps, BMP_TLFT_CRN_ACT );
  bmp_load_default( hps, BMP_TLFT_ACT     );
  bmp_load_default( hps, BMP_LFT_TTL_ACT  );
  bmp_load_default( hps, BMP_TTL_ACT      );
  bmp_load_default( hps, BMP_RGT_TTL_ACT  );
  bmp_load_default( hps, BMP_TRGT_ACT     );
  bmp_load_default( hps, BMP_TRGT_CRN_ACT );
  bmp_load_default( hps, BMP_BLFT_CRN     );
  bmp_load_default( hps, BMP_BOTTOM       );
  bmp_load_default( hps, BMP_BRGT_CRN     );
  bmp_load_default( hps, BMP_BLFT         );
  bmp_load_default( hps, BMP_LEFT         );
  bmp_load_default( hps, BMP_BRGT         );
  bmp_load_default( hps, BMP_RIGHT        );
  bmp_load_default( hps, BMP_MINIMIZE     );
  bmp_load_default( hps, BMP_N_MINIMIZE   );
  bmp_load_default( hps, BMP_CLOSE        );
  bmp_load_default( hps, BMP_N_CLOSE      );
}

/* Initializes default bitmap positions. */
static void
bmp_init_skin_positions( void )
{
  bmp_pos[ POS_TIMER       ].x =       228; bmp_pos[ POS_TIMER       ].y =        48;
  bmp_pos[ POS_R_SIZE      ].x =       300; bmp_pos[ POS_R_SIZE      ].y =       110;
  bmp_pos[ POS_R_PLAY      ].x =         6; bmp_pos[ POS_R_PLAY      ].y =         8;
  bmp_pos[ POS_R_PAUSE     ].x =        29; bmp_pos[ POS_R_PAUSE     ].y =         8;
  bmp_pos[ POS_R_REW       ].x =        52; bmp_pos[ POS_R_REW       ].y =         8;
  bmp_pos[ POS_R_FWD       ].x =        75; bmp_pos[ POS_R_FWD       ].y =         8;
  bmp_pos[ POS_R_PL        ].x =       108; bmp_pos[ POS_R_PL        ].y =         8;
  bmp_pos[ POS_R_REPEAT    ].x =       165; bmp_pos[ POS_R_REPEAT    ].y =         8;
  bmp_pos[ POS_R_SHUFFLE   ].x =       188; bmp_pos[ POS_R_SHUFFLE   ].y =         8;
  bmp_pos[ POS_R_PREV      ].x =       211; bmp_pos[ POS_R_PREV      ].y =         8;
  bmp_pos[ POS_R_NEXT      ].x =       234; bmp_pos[ POS_R_NEXT      ].y =         8;
  bmp_pos[ POS_R_POWER     ].x =       270; bmp_pos[ POS_R_POWER     ].y =         8;
  bmp_pos[ POS_R_STOP      ].x = POS_UNDEF; bmp_pos[ POS_R_STOP      ].y = POS_UNDEF;
  bmp_pos[ POS_R_FLOAD     ].x = POS_UNDEF; bmp_pos[ POS_R_FLOAD     ].y = POS_UNDEF;
  bmp_pos[ POS_R_HIDE      ].x = POS_UNDEF; bmp_pos[ POS_R_HIDE      ].y = POS_UNDEF;
  bmp_pos[ POS_R_EQ        ].x = POS_UNDEF; bmp_pos[ POS_R_EQ        ].y = POS_UNDEF;
  bmp_pos[ POS_R_TEXT      ].x =        32; bmp_pos[ POS_R_TEXT      ].y =        84;
  bmp_pos[ POS_S_TEXT      ].x =        10; bmp_pos[ POS_S_TEXT      ].y =        41;
  bmp_pos[ POS_NOTL        ].x =       178; bmp_pos[ POS_NOTL        ].y =        62;
  bmp_pos[ POS_TL          ].x =       178; bmp_pos[ POS_TL          ].y =        62;
  bmp_pos[ POS_NOPLIST     ].x =       178; bmp_pos[ POS_NOPLIST     ].y =        47;
  bmp_pos[ POS_PLIST       ].x =       178; bmp_pos[ POS_PLIST       ].y =        47;
  bmp_pos[ POS_TIME_LEFT   ].x =       188; bmp_pos[ POS_TIME_LEFT   ].y =        62;
  bmp_pos[ POS_PL_LEFT     ].x =       188; bmp_pos[ POS_PL_LEFT     ].y =        47;
  bmp_pos[ POS_PL_MODE     ].x = POS_UNDEF; bmp_pos[ POS_PL_MODE     ].y = POS_UNDEF;
  bmp_pos[ POS_LED         ].x =         9; bmp_pos[ POS_LED         ].y =        92;
  bmp_pos[ POS_N_LED       ].x =         9; bmp_pos[ POS_N_LED       ].y =        92;
  bmp_pos[ POS_SLIDER      ].x =        32; bmp_pos[ POS_SLIDER      ].y =        40;
  bmp_pos[ POS_VOLBAR      ].x =         9; bmp_pos[ POS_VOLBAR      ].y =        38;
  bmp_pos[ POS_NO_CHANNELS ].x =       240; bmp_pos[ POS_NO_CHANNELS ].y =        72;
  bmp_pos[ POS_MONO        ].x =       240; bmp_pos[ POS_MONO        ].y =        72;
  bmp_pos[ POS_STEREO      ].x =       240; bmp_pos[ POS_STEREO      ].y =        72;
  bmp_pos[ POS_BPS         ].x =       259; bmp_pos[ POS_BPS         ].y =        72;
  bmp_pos[ POS_S_SIZE      ].x = POS_UNDEF; bmp_pos[ POS_S_SIZE      ].y = POS_UNDEF;
  bmp_pos[ POS_T_SIZE      ].x = POS_UNDEF; bmp_pos[ POS_T_SIZE      ].y = POS_UNDEF;
  bmp_pos[ POS_S_PLAY      ].x =         6; bmp_pos[ POS_S_PLAY      ].y =         8;
  bmp_pos[ POS_S_PAUSE     ].x =        29; bmp_pos[ POS_S_PAUSE     ].y =         8;
  bmp_pos[ POS_S_REW       ].x =        52; bmp_pos[ POS_S_REW       ].y =         8;
  bmp_pos[ POS_S_FWD       ].x =        75; bmp_pos[ POS_S_FWD       ].y =         8;
  bmp_pos[ POS_S_PL        ].x =       108; bmp_pos[ POS_S_PL        ].y =         8;
  bmp_pos[ POS_S_REPEAT    ].x =       165; bmp_pos[ POS_S_REPEAT    ].y =         8;
  bmp_pos[ POS_S_SHUFFLE   ].x =       188; bmp_pos[ POS_S_SHUFFLE   ].y =         8;
  bmp_pos[ POS_S_PREV      ].x =       211; bmp_pos[ POS_S_PREV      ].y =         8;
  bmp_pos[ POS_S_NEXT      ].x =       234; bmp_pos[ POS_S_NEXT      ].y =         8;
  bmp_pos[ POS_S_POWER     ].x =       270; bmp_pos[ POS_S_POWER     ].y =         8;
  bmp_pos[ POS_S_STOP      ].x = POS_UNDEF; bmp_pos[ POS_S_STOP      ].y = POS_UNDEF;
  bmp_pos[ POS_S_FLOAD     ].x = POS_UNDEF; bmp_pos[ POS_S_FLOAD     ].y = POS_UNDEF;
  bmp_pos[ POS_S_HIDE      ].x = POS_UNDEF; bmp_pos[ POS_S_HIDE      ].y = POS_UNDEF;
  bmp_pos[ POS_S_EQ        ].x = POS_UNDEF; bmp_pos[ POS_S_EQ        ].y = POS_UNDEF;
  bmp_pos[ POS_T_PLAY      ].x =         6; bmp_pos[ POS_T_PLAY      ].y =         8;
  bmp_pos[ POS_T_PAUSE     ].x =        29; bmp_pos[ POS_T_PAUSE     ].y =         8;
  bmp_pos[ POS_T_REW       ].x =        52; bmp_pos[ POS_T_REW       ].y =         8;
  bmp_pos[ POS_T_FWD       ].x =        75; bmp_pos[ POS_T_FWD       ].y =         8;
  bmp_pos[ POS_T_PL        ].x =       108; bmp_pos[ POS_T_PL        ].y =         8;
  bmp_pos[ POS_T_REPEAT    ].x =       165; bmp_pos[ POS_T_REPEAT    ].y =         8;
  bmp_pos[ POS_T_SHUFFLE   ].x =       188; bmp_pos[ POS_T_SHUFFLE   ].y =         8;
  bmp_pos[ POS_T_PREV      ].x =       211; bmp_pos[ POS_T_PREV      ].y =         8;
  bmp_pos[ POS_T_NEXT      ].x =       234; bmp_pos[ POS_T_NEXT      ].y =         8;
  bmp_pos[ POS_T_POWER     ].x =       270; bmp_pos[ POS_T_POWER     ].y =         8;
  bmp_pos[ POS_T_STOP      ].x = POS_UNDEF; bmp_pos[ POS_T_STOP      ].y = POS_UNDEF;
  bmp_pos[ POS_T_FLOAD     ].x = POS_UNDEF; bmp_pos[ POS_T_FLOAD     ].y = POS_UNDEF;
  bmp_pos[ POS_T_HIDE      ].x = POS_UNDEF; bmp_pos[ POS_T_HIDE      ].y = POS_UNDEF;
  bmp_pos[ POS_T_EQ        ].x = POS_UNDEF; bmp_pos[ POS_T_EQ        ].y = POS_UNDEF;
  bmp_pos[ POS_PL_INDEX    ].x =       152; bmp_pos[ POS_PL_INDEX    ].y =        62;
  bmp_pos[ POS_PL_TOTAL    ].x =       152; bmp_pos[ POS_PL_TOTAL    ].y =        47;
  bmp_pos[ POS_SLIDER_SHAFT].x = POS_UNDEF; bmp_pos[ POS_SLIDER_SHAFT].y = POS_UNDEF;
  bmp_pos[ POS_MINIMIZE    ].x = POS_UNDEF; bmp_pos[ POS_MINIMIZE    ].y = POS_UNDEF;
  bmp_pos[ POS_CLOSE       ].x = POS_UNDEF; bmp_pos[ POS_CLOSE       ].y = POS_UNDEF;
}

/* Loads default PM123 skin. */
static void
bmp_init_default_skin( HPS hps )
{
  VISUAL visual;
  int    i;

  GpiDeleteBitmap( bmp_cache[ BMP_R_BGROUND ] );
  GpiDeleteBitmap( bmp_cache[ BMP_S_BGROUND ] );
  bmp_cache[ BMP_R_BGROUND ] = GpiLoadBitmap( hps, hmodule, BMP_R_BGROUNDEF, 0, 0 );
  bmp_cache[ BMP_S_BGROUND ] = GpiLoadBitmap( hps, hmodule, BMP_S_BGROUNDEF, 0, 0 );

  bmp_pos[ POS_R_PLAY      ].x =   6; bmp_pos[ POS_R_PLAY      ].y =   8;
  bmp_pos[ POS_R_PAUSE     ].x =  28; bmp_pos[ POS_R_PAUSE     ].y =   8;
  bmp_pos[ POS_R_REW       ].x =  50; bmp_pos[ POS_R_REW       ].y =   8;
  bmp_pos[ POS_R_FWD       ].x =  72; bmp_pos[ POS_R_FWD       ].y =   8;
  bmp_pos[ POS_R_PL        ].x = 102; bmp_pos[ POS_R_PL        ].y =   8;
  bmp_pos[ POS_R_EQ        ].x = 124; bmp_pos[ POS_R_EQ        ].y =   8;
  bmp_pos[ POS_R_FLOAD     ].x = 146; bmp_pos[ POS_R_FLOAD     ].y =   8;
  bmp_pos[ POS_R_REPEAT    ].x = 176; bmp_pos[ POS_R_REPEAT    ].y =   8;
  bmp_pos[ POS_R_SHUFFLE   ].x = 198; bmp_pos[ POS_R_SHUFFLE   ].y =   8;
  bmp_pos[ POS_R_PREV      ].x = 220; bmp_pos[ POS_R_PREV      ].y =   8;
  bmp_pos[ POS_R_NEXT      ].x = 242; bmp_pos[ POS_R_NEXT      ].y =   8;
  bmp_pos[ POS_R_POWER     ].x = 272; bmp_pos[ POS_R_POWER     ].y =   8;
  bmp_pos[ POS_S_PLAY      ].x =   6; bmp_pos[ POS_S_PLAY      ].y =   8;
  bmp_pos[ POS_S_PAUSE     ].x =  28; bmp_pos[ POS_S_PAUSE     ].y =   8;
  bmp_pos[ POS_S_REW       ].x =  50; bmp_pos[ POS_S_REW       ].y =   8;
  bmp_pos[ POS_S_FWD       ].x =  72; bmp_pos[ POS_S_FWD       ].y =   8;
  bmp_pos[ POS_S_PL        ].x = 102; bmp_pos[ POS_S_PL        ].y =   8;
  bmp_pos[ POS_S_EQ        ].x = 124; bmp_pos[ POS_S_EQ        ].y =   8;
  bmp_pos[ POS_S_FLOAD     ].x = 146; bmp_pos[ POS_S_FLOAD     ].y =   8;
  bmp_pos[ POS_S_REPEAT    ].x = 176; bmp_pos[ POS_S_REPEAT    ].y =   8;
  bmp_pos[ POS_S_SHUFFLE   ].x = 198; bmp_pos[ POS_S_SHUFFLE   ].y =   8;
  bmp_pos[ POS_S_PREV      ].x = 220; bmp_pos[ POS_S_PREV      ].y =   8;
  bmp_pos[ POS_S_NEXT      ].x = 242; bmp_pos[ POS_S_NEXT      ].y =   8;
  bmp_pos[ POS_S_POWER     ].x = 272; bmp_pos[ POS_S_POWER     ].y =   8;
  bmp_pos[ POS_T_PLAY      ].x =   6; bmp_pos[ POS_T_PLAY      ].y =   8;
  bmp_pos[ POS_T_PAUSE     ].x =  28; bmp_pos[ POS_T_PAUSE     ].y =   8;
  bmp_pos[ POS_T_REW       ].x =  50; bmp_pos[ POS_T_REW       ].y =   8;
  bmp_pos[ POS_T_FWD       ].x =  72; bmp_pos[ POS_T_FWD       ].y =   8;
  bmp_pos[ POS_T_PL        ].x = 102; bmp_pos[ POS_T_PL        ].y =   8;
  bmp_pos[ POS_T_EQ        ].x = 124; bmp_pos[ POS_T_EQ        ].y =   8;
  bmp_pos[ POS_T_FLOAD     ].x = 146; bmp_pos[ POS_T_FLOAD     ].y =   8;
  bmp_pos[ POS_T_REPEAT    ].x = 176; bmp_pos[ POS_T_REPEAT    ].y =   8;
  bmp_pos[ POS_T_SHUFFLE   ].x = 198; bmp_pos[ POS_T_SHUFFLE   ].y =   8;
  bmp_pos[ POS_T_PREV      ].x = 220; bmp_pos[ POS_T_PREV      ].y =   8;
  bmp_pos[ POS_T_NEXT      ].x = 242; bmp_pos[ POS_T_NEXT      ].y =   8;
  bmp_pos[ POS_T_POWER     ].x = 272; bmp_pos[ POS_T_POWER     ].y =   8;
  bmp_pos[ POS_R_HIDE      ].x = 285; bmp_pos[ POS_R_HIDE      ].y = 104;
  bmp_pos[ POS_S_HIDE      ].x = 285; bmp_pos[ POS_S_HIDE      ].y =  64;
  bmp_pos[ POS_T_HIDE      ].x = 288; bmp_pos[ POS_T_HIDE      ].y =  31;
  bmp_pos[ POS_S_SIZE      ].x = 300; bmp_pos[ POS_S_SIZE      ].y =  70;
  bmp_pos[ POS_T_SIZE      ].x = 300; bmp_pos[ POS_T_SIZE      ].y =  37;
  bmp_pos[ POS_NO_CHANNELS ].x = 229; bmp_pos[ POS_NO_CHANNELS ].y =  72;
  bmp_pos[ POS_MONO        ].x = 229; bmp_pos[ POS_MONO        ].y =  72;
  bmp_pos[ POS_STEREO      ].x = 229; bmp_pos[ POS_STEREO      ].y =  72;
  bmp_pos[ POS_PL_MODE     ].x = 132; bmp_pos[ POS_PL_MODE     ].y =  47;
  bmp_pos[ POS_MINIMIZE    ].x =  28; bmp_pos[ POS_MINIMIZE    ].y =  17;
  bmp_pos[ POS_CLOSE       ].x =  16; bmp_pos[ POS_CLOSE       ].y =  17;

  for( i = BMP_FONT1 + 45; i < BMP_FONT1 + 51 + 127; i++ ) {
    bmp_load_default( hps, i );
  }

  for( i = BMP_FONT2 + 45; i < BMP_FONT2 + 51 + 127; i++ ) {
    bmp_load_default( hps, i );
  }

  bmp_ulong[ UL_PL_INDEX     ] = TRUE;
  bmp_ulong[ UL_IN_PIXELS    ] = TRUE;
  bmp_ulong[ UL_R_MSG_LEN    ] = 256;
  bmp_ulong[ UL_R_MSG_HEIGHT ] = 16;
  bmp_ulong[ UL_S_MSG_LEN    ] = 276;
  bmp_ulong[ UL_S_MSG_HEIGHT ] = 16;
  bmp_ulong[ UL_FG_MSG_COLOR ] = DEF_FG_MSG_COLOR;
  bmp_ulong[ UL_FG_COLOR     ] = DEF_FG_COLOR;
  bmp_ulong[ UL_BG_COLOR     ] = DEF_BG_COLOR;
  bmp_ulong[ UL_HI_FG_COLOR  ] = DEF_HI_FG_COLOR;
  bmp_ulong[ UL_HI_BG_COLOR  ] = DEF_HI_BG_COLOR;;
  bmp_ulong[ UL_BPS_DIGITS   ] = TRUE;
  bmp_ulong[ UL_SHADE_BRIGHT ] = 0x00FFFFFFUL;
  bmp_ulong[ UL_SHADE_DARK   ] = 0x00808080UL;
  bmp_ulong[ UL_SHADE_BRIGHT2] = 0x00808080UL;
  bmp_ulong[ UL_SHADE_DARK2  ] = 0x00404040UL;
  bmp_ulong[ UL_TITLE_FG     ] = DEF_TITLE_FG;
  bmp_ulong[ UL_ACT_TITLE_FG ] = DEF_ACT_TITLE_FG;
  bmp_ulong[ UL_TITLE_HEIGHT ] = 16;
  bmp_ulong[ UL_SB_FG_COLOR  ] = DEF_SB_FG_COLOR;
  bmp_ulong[ UL_SB_BG_COLOR  ] = DEF_SB_BG_COLOR;;

  strcpy( skin.fontname, "9.WarpSans Bold" );

  strcpy( visual.pc.file, startpath );
  strcat( visual.pc.file, "visplug\\analyzer.dll" );
  strcpy( visual.param, "" );

  visual.skin = TRUE;
  visual.x    = 32;
  visual.y    = 49;
  visual.cx   = 95;
  visual.cy   = 30;

  pg_load_plugin( visual.pc.file, &visual );
}

static void
bmp_init_colors( void )
{
  pl_set_colors( bmp_ulong[ UL_FG_COLOR    ], bmp_ulong[ UL_BG_COLOR    ],
                 bmp_ulong[ UL_HI_FG_COLOR ], bmp_ulong[ UL_HI_BG_COLOR ],
                 bmp_ulong[ UL_SB_FG_COLOR ], bmp_ulong[ UL_SB_BG_COLOR ] );
  pm_set_colors( bmp_ulong[ UL_FG_COLOR    ], bmp_ulong[ UL_BG_COLOR    ],
                 bmp_ulong[ UL_HI_FG_COLOR ], bmp_ulong[ UL_HI_BG_COLOR ],
                 bmp_ulong[ UL_SB_FG_COLOR ], bmp_ulong[ UL_SB_BG_COLOR ] );
  bm_set_colors( bmp_ulong[ UL_FG_COLOR    ], bmp_ulong[ UL_BG_COLOR    ],
                 bmp_ulong[ UL_HI_FG_COLOR ], bmp_ulong[ UL_HI_BG_COLOR ],
                 bmp_ulong[ UL_SB_FG_COLOR ], bmp_ulong[ UL_SB_BG_COLOR ] );
  eq_set_colors( bmp_ulong[ UL_FG_COLOR    ], bmp_ulong[ UL_BG_COLOR    ],
                 bmp_ulong[ UL_HI_FG_COLOR ], bmp_ulong[ UL_HI_BG_COLOR ],
                 bmp_ulong[ UL_SB_FG_COLOR ], bmp_ulong[ UL_SB_BG_COLOR ] );
  bn_set_colors( bmp_ulong[ UL_FG_COLOR    ], bmp_ulong[ UL_BG_COLOR    ] );
}

/* Returns the color that is lighter than the original by the specified value. */
RGB rgb_lighten( RGB rgb, int power )
{
  RGB res;

  int red   = (UCHAR)rgb.bRed   + power;
  int green = (UCHAR)rgb.bGreen + power;
  int blue  = (UCHAR)rgb.bBlue  + power;

  if( power > 0 ) {
    res.bRed   = red   > 255 ? 255 : red;
    res.bGreen = green > 255 ? 255 : green;
    res.bBlue  = blue  > 255 ? 255 : blue;
  } else {
    res.bRed   = red   < 0 ? 0 : red;
    res.bGreen = green < 0 ? 0 : green;
    res.bBlue  = blue  < 0 ? 0 : blue;
  }

  return res;
}

/* Converts the unsigned long value to RGB. */
RGB ultorgb( ULONG color )
{
  RGB rgb;

  rgb.bBlue  = (color & 0x0000FFUL);
  rgb.bGreen = (color & 0x00FF00UL) >> 8;
  rgb.bRed   = (color & 0xFF0000UL) >> 16;

  return rgb;
}

/* Returns TRUE if specified mode supported by current skin. */
BOOL
bmp_is_mode_supported( int mode )
{
  switch( mode )
  {
    case CFG_MODE_REGULAR:
      return bmp_pos[ POS_R_SIZE ].x != POS_UNDEF && bmp_pos[ POS_R_SIZE ].y != POS_UNDEF;
    case CFG_MODE_SMALL:
      return bmp_pos[ POS_S_SIZE ].x != POS_UNDEF && bmp_pos[ POS_S_SIZE ].y != POS_UNDEF;
    case CFG_MODE_TINY:
      return bmp_pos[ POS_T_SIZE ].x != POS_UNDEF && bmp_pos[ POS_T_SIZE ].y != POS_UNDEF;
  }

  return FALSE;
}

/* Returns TRUE if specified font supported by current skin. */
BOOL
bmp_is_font_supported( int font ) {
  return ( font < bmp_ulong[ UL_FONTS ] );
}

/* Loads specified bitmaps bundle. */
static BOOL
bmp_load_packfile( char *filename )
{
  FILE*     pack;
  char*     fbuf;
  int       cb;
  BUNDLEHDR hdr;

  pack = fopen( filename, "rb" );
  if( pack == NULL ) {
    amp_show_error( "The bitmap bundle file, %s, for this skin was not found. "
                    "Skin display as incomplete.", filename );
    return FALSE;
  }

  while( !feof( pack ))
  {
    if( fread( &hdr, 1, sizeof( BUNDLEHDR ), pack ) == sizeof( BUNDLEHDR ) &&
        hdr.length > 0 )
    {
      if(( fbuf = malloc( hdr.length )) != NULL )
      {
        cb = fread( fbuf, 1, hdr.length, pack );
        bmp_cache[ hdr.resource ] = bmp_make_bitmap( hdr.filename, fbuf, cb );
        free( fbuf );
      } else {
        fclose( pack );
        amp_show_error( "Out of memory" );
        return FALSE;
      }
    }
  }

  fclose( pack );
  return TRUE;
}

/* Deallocates all resources used by current loaded skin. */
void
bmp_clean_skin( void )
{
  int i;

  for( i = 0; i < sizeof( bmp_cache ) / sizeof( HBITMAP ); i++ ) {
    if( bmp_cache[i] != NULLHANDLE )
    {
      GpiDeleteBitmap( bmp_cache[i] );
      bmp_cache[i] = NULLHANDLE;
    }
  }

  memset( &bmp_pos,   0, sizeof( bmp_pos   ));
  memset( &bmp_ulong, 0, sizeof( bmp_ulong ));

  bmp_delete_text_buffer();

  if( *skin.name ) {
    save_skin_data( &skin );
  }
}

/* Loads specified skin. */
BOOL
bmp_load_skin( const char *filename, HAB hab, HWND hplayer, HPS hps )
{
  int   i;
  FILE* file   = NULL;
  char  line[256];
  char  path[_MAX_PATH];
  int   errors = 0;
  BOOL  empty  = TRUE;

  sdrivedir( path, filename, sizeof( path ));

  if( *filename ) {
    if(( file = fopen( filename, "r" )) == NULL ) {
      amp_show_error( "Unable open skin %s, %s", filename, strerror( errno ));
    }
  }

  // Free loaded visual plugins.
  pg_remove_all_visuals( TRUE );

  bmp_clean_skin();
  bmp_init_skin_positions();

  if( file && *filename ) {
    sfname( skin.name, filename, sizeof( skin.name ));
  } else {
    strcpy( skin.name, "default" );
  }

  bmp_ulong[ UL_SHADE_BRIGHT   ] = 0x00FFFFFFUL;
  bmp_ulong[ UL_SHADE_DARK     ] = 0x00404040UL;
  bmp_ulong[ UL_SHADE_BRIGHT2  ] = 0xFFFFFFFFUL;
  bmp_ulong[ UL_SHADE_DARK2    ] = 0xFFFFFFFFUL;
  bmp_ulong[ UL_SLIDER_COLOR   ] = 0x00007F00UL;
  bmp_ulong[ UL_SHADE_STAT     ] = TRUE;
  bmp_ulong[ UL_SHADE_SLIDER   ] = TRUE;
  bmp_ulong[ UL_SHADE_PLAYER   ] = TRUE;
  bmp_ulong[ UL_SHADE_VOLUME   ] = TRUE;
  bmp_ulong[ UL_SLIDER_WIDTH   ] = 242;
  bmp_ulong[ UL_TIMER_SPACE    ] = 1;
  bmp_ulong[ UL_TIMER_SEPARATE ] = TRUE;
  bmp_ulong[ UL_VOLUME_SLIDER  ] = FALSE;
  bmp_ulong[ UL_VOLUME_HRZ     ] = FALSE;
  bmp_ulong[ UL_BPS_DIGITS     ] = FALSE;
  bmp_ulong[ UL_PL_COLOR       ] = 0x0000FF00UL;
  bmp_ulong[ UL_R_MSG_LEN      ] = 25;
  bmp_ulong[ UL_R_MSG_HEIGHT   ] = 0;
  bmp_ulong[ UL_S_MSG_LEN      ] = 25;
  bmp_ulong[ UL_S_MSG_HEIGHT   ] = 0;
  bmp_ulong[ UL_FG_MSG_COLOR   ] = 0x00FFFFFFUL;
  bmp_ulong[ UL_FG_COLOR       ] = 0xFFFFFFFFUL;
  bmp_ulong[ UL_BG_COLOR       ] = 0xFFFFFFFFUL;
  bmp_ulong[ UL_HI_FG_COLOR    ] = 0xFFFFFFFFUL;
  bmp_ulong[ UL_HI_BG_COLOR    ] = 0xFFFFFFFFUL;
  bmp_ulong[ UL_PL_INDEX       ] = FALSE;
  bmp_ulong[ UL_FONT           ] = 0;
  bmp_ulong[ UL_FONTS          ] = 2;
  bmp_ulong[ UL_IN_PIXELS      ] = FALSE;
  bmp_ulong[ UL_VOLUME_BGS     ] = 0;
  bmp_ulong[ UL_NO_SEEK_SLIDER ] = FALSE;
  bmp_ulong[ UL_TITLE_FG       ] = 0x00FFFFFFUL;
  bmp_ulong[ UL_ACT_TITLE_FG   ] = 0x00FFFFFFUL;
  bmp_ulong[ UL_TITLE_HEIGHT   ] = 16;
  bmp_ulong[ UL_SB_FG_COLOR    ] = 0xFFFFFFFFUL;
  bmp_ulong[ UL_SB_BG_COLOR    ] = 0xFFFFFFFFUL;

  strcpy( skin.fontname, "2.System VIO" );

  if( !file )
  {
    bmp_init_skins_bitmaps( hps );
    bmp_init_default_skin( hps );
    bmp_init_colors();
    load_skin_data( &skin );
    bmp_reflow_and_resize( WinQueryWindow( hplayer, QW_PARENT ));

    return FALSE;
  }

  while( !feof( file ) && fgets( line, 256, file ))
  {
    blank_strip( line );

    if( *line == '#' || *line == ';' ) {
      continue;
    }

    for( i = 0; line[i]; i++ ) {
      if( strchr( ",:=", line[i] )) {
        break;
      }
    }

    switch( line[i] ) {
      case '=': // plug-in
      {
        VISUAL visual;
        char*  p = strtok( line + i + 1, "," );
        char   param[_MAX_PATH];
        struct stat fi;

        if( p == NULL ) {
          break;
        }

        rel2abs( startpath, p, visual.pc.file, sizeof( visual.pc.file ));

        if(( p = strtok( NULL, "," )) != NULL ) {
          visual.x  = atoi(p);
        }
        if(( p = strtok( NULL, "," )) != NULL ) {
          visual.y  = atoi(p);
        }
        if(( p = strtok( NULL, "," )) != NULL ) {
          visual.cx = atoi(p);
        }
        if(( p = strtok( NULL, "," )) != NULL ) {
          visual.cy = atoi(p);
        }
        if(( p = strtok( NULL, "," )) != NULL ) {
          rel2abs( path, p, param, sizeof( param ));
          if( stat( param, &fi ) == 0 ) {
            strcpy( visual.param, param );
          } else {
            strcpy( visual.param, p );
          }
        }

        visual.skin = TRUE;
        pg_load_plugin( visual.pc.file, &visual );
        break;
      }

      case ':': // position
      {
        int x = POS_UNDEF;
        int y = POS_UNDEF;
        int i =  0;

        sscanf( line, "%d:%d,%d", &i, &x, &y );

        bmp_pos[ i ].x = x;
        bmp_pos[ i ].y = y;
        break;
      }

      case ',': // bitmap
      {
        char* p = line + i + 1;
        int   i = atoi( line );
        int   r, g, b;

        if( i == UL_SHADE_BRIGHT  ||
            i == UL_SHADE_DARK    ||
            i == UL_SHADE_BRIGHT2 ||
            i == UL_SHADE_DARK2   ||
            i == UL_SLIDER_BRIGHT ||
            i == UL_SLIDER_COLOR  ||
            i == UL_PL_COLOR      ||
            i == UL_FG_COLOR      ||
            i == UL_BG_COLOR      ||
            i == UL_HI_FG_COLOR   ||
            i == UL_HI_BG_COLOR   ||
            i == UL_FG_MSG_COLOR  ||
            i == UL_TITLE_FG      ||
            i == UL_ACT_TITLE_FG  ||
            i == UL_SB_FG_COLOR   ||
            i == UL_SB_BG_COLOR   )
        {
            sscanf( p, "%d/%d/%d", &r, &g, &b );
            bmp_ulong[ i ] = r << 16 | g << 8 | b;
            break;
        }
        switch( i ) {
          case UL_SHADE_STAT:   bmp_ulong[ UL_SHADE_STAT    ] = FALSE;   break;
          case UL_SHADE_VOLUME: bmp_ulong[ UL_SHADE_VOLUME  ] = FALSE;   break;

          case UL_DISPLAY_MSG:
            // Not supported since 1.32
            // if( amp_playmode == AMP_NOFILE ) {
            //  bmp_set_text( p );
            // }
            break;

          case UL_TIMER_SEPSPACE:
            // Not supported since 1.32
            // bmp_ulong[ UL_TIMER_SEPSPACE ] = atoi(p);
            break;

          case UL_SHADE_PLAYER:   bmp_ulong[ UL_SHADE_PLAYER   ] = FALSE;   break;
          case UL_SHADE_SLIDER:   bmp_ulong[ UL_SHADE_SLIDER   ] = FALSE;   break;
          case UL_R_MSG_LEN:      bmp_ulong[ UL_R_MSG_LEN      ] = atoi(p); break;
          case UL_SLIDER_WIDTH:   bmp_ulong[ UL_SLIDER_WIDTH   ] = atoi(p); break;
          case UL_S_MSG_LEN:      bmp_ulong[ UL_S_MSG_LEN      ] = atoi(p); break;
          case UL_FONT:           cfg.font = atoi(p); break;
          case UL_TIMER_SPACE:    bmp_ulong[ UL_TIMER_SPACE    ] = atoi(p); break;
          case UL_TIMER_SEPARATE: bmp_ulong[ UL_TIMER_SEPARATE ] = FALSE;   break;
          case UL_VOLUME_HRZ:     bmp_ulong[ UL_VOLUME_HRZ     ] = TRUE;    break;
          case UL_VOLUME_SLIDER:  bmp_ulong[ UL_VOLUME_SLIDER  ] = TRUE;    break;
          case UL_BPS_DIGITS:     bmp_ulong[ UL_BPS_DIGITS     ] = TRUE;    break;
          case UL_PL_INDEX:       bmp_ulong[ UL_PL_INDEX       ] = TRUE;    break;
          case UL_FONTS:          bmp_ulong[ UL_FONTS          ] = atoi(p); break;
          case UL_IN_PIXELS:      bmp_ulong[ UL_IN_PIXELS      ] = TRUE;    break;
          case UL_R_MSG_HEIGHT:   bmp_ulong[ UL_R_MSG_HEIGHT   ] = atoi(p); break;
          case UL_S_MSG_HEIGHT:   bmp_ulong[ UL_S_MSG_HEIGHT   ] = atoi(p); break;
          case UL_NO_SEEK_SLIDER: bmp_ulong[ UL_NO_SEEK_SLIDER ] = TRUE;    break;
          case UL_TITLE_HEIGHT:   bmp_ulong[ UL_TITLE_HEIGHT   ] = atoi(p); break;

          case UL_BUNDLE:
          {
            char bundle[_MAX_PATH];

            rel2abs( path, p, bundle, sizeof( bundle ));
            bmp_load_packfile( bundle );
            break;
          }

          case UL_FONTNAME:
            strlcpy( skin.fontname, p, sizeof( skin.fontname ));
            break;

          default:
          {
            char image[_MAX_PATH];

            rel2abs( path, p, image, sizeof( image ));
            bmp_cache[ i ] = bmp_load_bitmap( image );

            if( bmp_cache[ i ] == NULLHANDLE ) {
              ++errors;
            }
            break;
          }
        }
      }
    }
  }

  fclose( file );

  for( i = 0; i < sizeof( bmp_cache ) / sizeof( HBITMAP ); i++ ) {
    if( bmp_cache[ i ] ) {
      empty = FALSE;
      break;
    }
  }

  if( empty ) {
    bmp_init_default_skin( hps );
  }

  for( i = 0; i < 28 && bmp_cache[ BMP_VOLBG + i ]; i++ ) {
    ++bmp_ulong[ UL_VOLUME_BGS ];
  }
  if( bmp_ulong[ UL_VOLUME_BGS ] ) {
    GpiDeleteBitmap( bmp_cache[ BMP_VOLBAR ] );
    bmp_cache[ BMP_VOLBAR ] = bmp_cache[ BMP_VOLBG ];
  }

  if( cfg.mode != CFG_MODE_REGULAR && !bmp_is_mode_supported( cfg.mode )) {
    cfg.mode = CFG_MODE_REGULAR;
  }
  if( cfg.font > bmp_ulong[ UL_FONTS ] ) {
    cfg.font = 0;
  }

  bmp_init_skins_bitmaps( hps );
  bmp_init_colors();
  load_skin_data( &skin );
  bmp_reflow_and_resize( WinQueryWindow( hplayer, QW_PARENT ));

  if( errors > 0 ) {
    if( !amp_query( hplayer, "Some bitmaps of this skin was not found. "
                             "Would you like to continue the loading of this skin? "
                             "(if you select No, default skin will be used)" ))
    {
      strcpy( cfg.defskin, "" );
      return bmp_load_skin( "", hab, hplayer, hps );
    }
  }

  return TRUE;
}

/* Initializes specified skin button. */
static void
bmp_init_button( HWND hwnd, BMPBUTTON* button )
{
  DATA95 btn_data;
  int    x, y, cx, cy;
  int    pressed;
  int    release;

  switch( cfg.mode )
  {
    case CFG_MODE_REGULAR:

      x       = bmp_pos[ button->id_r_pos ].x;
      y       = bmp_pos[ button->id_r_pos ].y;
      pressed = button->id_r_pressed;
      release = button->id_r_release;
      cx      = bmp_cx( pressed );
      cy      = bmp_cy( pressed );
      break;

    case CFG_MODE_SMALL:

      x       = bmp_pos[ button->id_s_pos ].x;
      y       = bmp_pos[ button->id_s_pos ].y;
      pressed = bmp_cache[ button->id_s_pressed ] ? button->id_s_pressed : button->id_r_pressed;
      release = bmp_cache[ button->id_s_release ] ? button->id_s_release : button->id_r_release;
      cx      = bmp_cx( pressed );
      cy      = bmp_cy( pressed );
      break;

    default:

      x       = bmp_pos  [ button->id_t_pos ].x;
      y       = bmp_pos  [ button->id_t_pos ].y;
      pressed = bmp_cache[ button->id_s_pressed ] ? button->id_s_pressed : button->id_r_pressed;
      release = bmp_cache[ button->id_s_release ] ? button->id_s_release : button->id_r_release;
      cx      = bmp_cx( pressed );
      cy      = bmp_cy( pressed );
      break;
  }

  if( x != POS_UNDEF && y != POS_UNDEF ) {
    if( button->handle == NULLHANDLE )
    {
      btn_data.cb          =  sizeof( DATA95 );
      btn_data.pressed     =  0;
      btn_data.bmp_release = &bmp_cache[ release ];
      btn_data.bmp_pressed = &bmp_cache[ pressed ];
      btn_data.stick       =  button->sticky;
      btn_data.stickvar    = &button->state;
      btn_data.hwnd_owner  =  hwnd;

      strcpy( btn_data.help, button->help );
      button->handle = WinCreateWindow( hwnd, CLASSNAME, "", WS_VISIBLE, x, y, cx, cy,
                                        hwnd, HWND_TOP, button->id_r_pressed, &btn_data, NULL );
    }
    else
    {
      WinSetWindowPos( button->handle, HWND_TOP, x, y, cx, cy, SWP_MOVE | SWP_SIZE );
      WinSendMsg( button->handle, WM_CHANGEBMP, MPFROMLONG( &bmp_cache[ release ] ),
                                                MPFROMLONG( &bmp_cache[ pressed ] ));
    }
  }
  else
  {
    if( button->handle != NULLHANDLE ) {
      WinDestroyWindow( button->handle );
      button->handle = NULLHANDLE;
    }
  }
}

/* Adjusts current skin to the selected size of the player window. */
void
bmp_reflow_and_resize( HWND hframe )
{
  HWND hplayer = WinWindowFromID( hframe, FID_CLIENT );

  switch( cfg.mode )
  {
    case CFG_MODE_SMALL:
    {
      vis_terminate_all( TRUE );
      WinSetWindowPos( hframe, HWND_TOP, 0, 0,
                       bmp_pos[POS_S_SIZE].x, bmp_pos[POS_S_SIZE].y, SWP_SIZE );
      break;
    }

    case CFG_MODE_TINY:
    {
      vis_terminate_all( TRUE );
      WinSetWindowPos( hframe, HWND_TOP, 0, 0,
                       bmp_pos[POS_T_SIZE].x, bmp_pos[POS_T_SIZE].y, SWP_SIZE );
      break;
    }

    default:
    {
      WinSetWindowPos( hframe, HWND_TOP, 0, 0,
                       bmp_pos[POS_R_SIZE].x, bmp_pos[POS_R_SIZE].y, SWP_SIZE );

      vis_initialize_all( hplayer, TRUE );
      break;
    }
  }

  bmp_init_button( hplayer, &btn_play    );
  bmp_init_button( hplayer, &btn_pause   );
  bmp_init_button( hplayer, &btn_rew     );
  bmp_init_button( hplayer, &btn_fwd     );
  bmp_init_button( hplayer, &btn_pl      );
  bmp_init_button( hplayer, &btn_repeat  );
  bmp_init_button( hplayer, &btn_shuffle );
  bmp_init_button( hplayer, &btn_prev    );
  bmp_init_button( hplayer, &btn_next    );
  bmp_init_button( hplayer, &btn_power   );
  bmp_init_button( hplayer, &btn_stop    );
  bmp_init_button( hplayer, &btn_fload   );
  bmp_init_button( hplayer, &btn_hide    );
  bmp_init_button( hplayer, &btn_eq      );

  bmp_delete_text_buffer();
  bmp_set_text( NULL );
  amp_invalidate( UPD_WINDOW );
}

