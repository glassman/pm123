/*
 * Copyright 2018-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#define  INCL_LONGLONG
#include <os2.h>
#include <math.h>
#include <string.h>
#include <process.h>
#include <debuglog.h>

#undef   BYTE_ORDER
#include "pm123.h"
#include "playlist.h"
#include "upnp.h"
#include "messages.h"
#include "tags.h"

#define MAX_INITTIME 30
static  TID    init_tid     = 0;
static  time_t init_started = 0;
static  BOOL   init_break   = FALSE;

#define MAX_METADATA 8192
static  HMTX mutex = NULLHANDLE;

/* Device handle supplied by UPnP SDK. */
static UpnpDevice_Handle hdevice = -1;

const char* udn
 = "uuid:ab781c0a-bad8-492c-b7f7-8a62ef7822e4";

const char* service_type_RenderingControl
 = "urn:schemas-upnp-org:service:RenderingControl:1";
const char* service_id_RenderingControl
 = "urn:upnp-org:serviceId:RenderingControl";

UPNP_STATE_VARIABLE state_RenderingControl[]
 = {{ "LastChange", "" },
    { "A_ARG_TYPE_InstanceID", "0" },
    { "A_ARG_TYPE_Channel", "Master" },
    { "A_ARG_TYPE_PresetName", "FactoryDefaults" },
    { "PresetNameList", "FactoryDefaults" },
    { "Volume", "0" },
    { "VolumeDB", "0" },
    { "Mute", "0" }};

const char* service_type_AVTransport
 = "urn:schemas-upnp-org:service:AVTransport:1";
const char* service_id_AVTransport
 = "urn:upnp-org:serviceId:AVTransport";

UPNP_STATE_VARIABLE state_AVTransport[]
 = {{ "LastChange", "" },
    { "A_ARG_TYPE_InstanceID", "0" },
    { "A_ARG_TYPE_SeekMode", "TRACK_NR" },
    { "A_ARG_TYPE_SeekTarget", "0" },
    { "AbsoluteCounterPosition", "2147483647" },
    { "AbsoluteTimePosition", "0:00:00.000" },
    { "AVTransportURI", "" },
    { "AVTransportURIMetaData", "" },
    { "CurrentMediaCategory", "NO_MEDIA" }, // v3.0 only
    { "CurrentMediaDuration", "0:00:00.000" },
    { "CurrentPlayMode", "NORMAL" },
    { "CurrentRecordQualityMode", "NOT_IMPLEMENTED" },
    { "CurrentTrack", "0" },
    { "CurrentTrackDuration", "0:00:00.000" },
    { "CurrentTrackMetaData", "" },
    { "CurrentTrackURI", "" },
    { "CurrentTransportActions", "Play,Stop,Pause,Seek,Next,Previous" },
    { "NextAVTransportURI", "NOT_IMPLEMENTED" },
    { "NextAVTransportURIMetaData", "NOT_IMPLEMENTED" },
    { "NumberOfTracks", "0" },
    { "PlaybackStorageMedium", "NONE" },
    { "PossiblePlaybackStorageMedia", "NONE,UNKNOWN,CD-DA,HDD,NETWORK" },
    { "PossibleRecordQualityModes", "NOT_IMPLEMENTED" },
    { "PossibleRecordStorageMedia", "NOT_IMPLEMENTED" },
    { "RecordMediumWriteStatus", "NOT_IMPLEMENTED" },
    { "RecordStorageMedium", "NOT_IMPLEMENTED" },
    { "RelativeCounterPosition", "2147483647" },
    { "RelativeTimePosition", "0:00:00.000" },
    { "TransportPlaySpeed", "1" },
    { "TransportState", "NO_MEDIA_PRESENT" },
    { "TransportStatus", "OK" }};

const char* service_type_ConnectionManager
 = "urn:schemas-upnp-org:service:ConnectionManager:1";
const char* service_id_ConnectionManager
 = "urn:upnp-org:serviceId:ConnectionManager";

UPNP_STATE_VARIABLE state_ConnectionManager[]
 = {{ "A_ARG_TYPE_AVTransportID", "0" },
    { "A_ARG_TYPE_ConnectionID", "0" },
    { "A_ARG_TYPE_ConnectionManager", "" },
    { "A_ARG_TYPE_ConnectionStatus", "OK" },
    { "A_ARG_TYPE_Direction", "Input" },
    { "A_ARG_TYPE_ProtocolInfo", "" },
    { "A_ARG_TYPE_RcsID", "0" },
    { "CurrentConnectionIDs", "0" },
    { "FeatureList", "<?xml version=\"1.0\"?>\n"
                     "<Features\n"
                     "xmlns=\"urn:schemas-upnp-org:av:cm-featureList\"\n"
                     "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                     "xsi:schemaLocation=\"urn:schemas-upnp-org:av:cm-featureList http://www.upnp.org/schemas/av/cm-featureList.xsd\">\n"
                     "</Features>" }, // v3.0 only
    { "SinkProtocolInfo", "" },
    { "SourceProtocolInfo", "" }};

#define SET_UPNP_ERROR( x, s )  \
    strcpy( event->ErrStr, s ); \
    event->ErrCode = x

#ifdef UPNP_PRECISSION_TIME
  #define SPRINTF_TIME( x, ms )             \
    sprintf( Position, "%u:%02u:%02u.%03u", \
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60, ms % 1000 );
#else
  #define SPRINTF_TIME( x, ms )             \
    sprintf( Position, "%u:%02u:%02u",      \
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60 );
#endif

static char notifyRCS[] =
  "<Event\n"
  "xmlns=\"urn:schemas-upnp-org:metadata-1-0/RCS/\"\n"
  "xmlns:xsi=\"http://www.w3.org/2002/XMLSchema-instance\"\n"
  "xsi:schemaLocation=\"urn:schemas-upnp-org:metadata-1-0/RCS/ "
  "http://www.upnp.org/schemas/av/rcs-event-v3-20101231.xsd\">\n"
  "</Event>";
static char notifyAVT[] =
  "<Event\n"
  "xmlns=\"urn:schemas-upnp-org:metadata-1-0/AVT/\"\n"
  "xmlns:xsi=\"http://www.w3.org/2002/XMLSchema-instance\"\n"
  "xsi:schemaLocation=\"urn:schemas-upnp-org:metadata-1-0/AVT/ "
  "http://www.upnp.org/schemas/av/avt-event-v2-20080930.xsd\">\n"
  "</Event>";

/* Requests ownership of the state variables. */
static BOOL
upnp_state_request( void )
{
  APIRET rc = DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );

  if( rc != NO_ERROR )
  {
    char error[1024];
    amp_show_error( "Unable request the mutex semaphore.\n%s\n",
                    os2_strerror( rc, error, sizeof( error )));
    return FALSE;
  } else {
    return TRUE;
  }
}

/* Relinquishes ownership of the state variables was
   requested by upnp_state_request(). */
static BOOL
upnp_state_release( void )
{
  APIRET rc = DosReleaseMutexSem( mutex );

  if( rc != NO_ERROR )
  {
    char error[1024];
    amp_show_error( "Unable release the mutex semaphore.\n%s\n",
                    os2_strerror( rc, error, sizeof( error )));
    return FALSE;
  } else {
    return TRUE;
  }
}

/* Sets a single service state variable. */
int
upnp_set_service_var( unsigned int service,
                      unsigned int i, const char* value )
{
  if( hdevice != -1 ) {
    switch( service ) {
      case UPNP_RenderingControl:
        if( i < ARRAY_SIZE( state_RenderingControl ))
        {
          UPNP_STATE_VARIABLE* var = &state_RenderingControl[i];

          upnp_state_request();
          if( strcmp( var->strval, value ) != 0 ) {
            if( var->flags & UPNP_VAR_ALLOCATED ) {
              free( var->strval );
            }
            var->strval = strdup( value );
            var->flags  = UPNP_VAR_ALLOCATED;

            if( i != UPNP_LastChange ) {
              var->flags |= UPNP_VAR_CHANGED;
            }

            DEBUGLOG(( "upnp: set state of %s to %s\n", var->name, value ));

            if( i == UPNP_LastChange ) {
              UpnpNotify( hdevice, udn, service_id_RenderingControl,
                                  (const char**)&var->name, (const char**)&var->strval, 1 );
            }
          }
          upnp_state_release();
          return UPNP_E_SUCCESS;
        }
        break;

      case UPNP_AVTransport:
        if( i < ARRAY_SIZE( state_AVTransport ))
        {
          UPNP_STATE_VARIABLE* var = &state_AVTransport[i];

          upnp_state_request();
          if( strcmp( var->strval, value ) != 0 ) {
            if( var->flags & UPNP_VAR_ALLOCATED ) {
              free( var->strval );
            }
            var->strval = strdup( value );
            var->flags  = UPNP_VAR_ALLOCATED;

            if( i != UPNP_LastChange &&
                i != UPNP_RelativeTimePosition &&
                i != UPNP_AbsoluteTimePosition )
            {
              var->flags |= UPNP_VAR_CHANGED;
            }

            #if DEBUG && DEBUG < 2
            if( i != UPNP_RelativeTimePosition &&
                i != UPNP_AbsoluteTimePosition )
            {
            #endif
              DEBUGLOG(( "upnp: set state of %s to %s\n", var->name, value ));
            #if DEBUG && DEBUG < 2
            }
            #endif

            if( i == UPNP_LastChange ) {
              UpnpNotify( hdevice, udn, service_id_AVTransport,
                                  (const char**)&var->name, (const char**)&var->strval, 1 );
            }
          }
          upnp_state_release();
          return UPNP_E_SUCCESS;
        }
        break;

      case UPNP_ConnectionManager:
        if( i < ARRAY_SIZE( state_ConnectionManager ))
        {
          UPNP_STATE_VARIABLE* var = &state_ConnectionManager[i];

          upnp_state_request();
          if( strcmp( var->strval, value ) != 0 ) {
            if( var->flags & UPNP_VAR_ALLOCATED ) {
              free( var->strval );
            }
            var->strval = strdup( value );
            var->flags  = UPNP_VAR_ALLOCATED | UPNP_VAR_CHANGED;

            DEBUGLOG(( "upnp: set state of %s to %s\n", var->name, value ));

            if( i == UPNP_CurrentConnectionIDs ||
                i == UPNP_SinkProtocolInfo     ||
                i == UPNP_SourceProtocolInfo   )
            {
              UpnpNotify( hdevice, udn, service_id_ConnectionManager,
                                  (const char**)&var->name, (const char**)&var->strval, 1 );
            }
          }
          upnp_state_release();
          return UPNP_E_SUCCESS;
        }
        break;

    }
  }

  return UPNP_E_INVALID_PARAM;
}

/* Returns the value of the first document node with specified tag name.
   Must be freed after usage. */
static char*
upnp_get_first_document_item( IXML_Document* doc, const char* item )
{
  IXML_NodeList* nodeList = NULL;
  IXML_Node*     textNode = NULL;
  IXML_Node*     tempNode = NULL;
  char* result = NULL;

  nodeList = ixmlDocument_getElementsByTagName( doc, item );
  if( nodeList ) {
    tempNode = ixmlNodeList_item( nodeList, 0 );
    if( tempNode ) {
      textNode = ixmlNode_getFirstChild( tempNode );
      if( textNode ) {
        result = strdup( ixmlNode_getNodeValue( textNode ));
      }
    }
    ixmlNodeList_free( nodeList );
  }

  return result;
}

/* Converts a volume level to decibels. */
static short int
upnp_VolumeToDB( int volume )
{
  // The integer value shall be interpreted as having the decimal
  // point between the Most Significant Byte (MSB) and the Least
  // Significant Byte (LSB).
  double db = volume ? 20 * log10( volume / 100.0 ) : -72.0;
  return db * 256;
}

/* Converts decibels to a volume level. */
static double
upnp_DBToVolume( short int i )
{
  // The integer value shall be interpreted as having the decimal
  // point between the Most Significant Byte (MSB) and the Least
  // Significant Byte (LSB).
  double db = (double)i / 256;
  return db <= -72.0 ? 0 : pow( 10, db / 20 ) * 100;
}

/* Returns current player transport state. */
static const char*
upnp_TransportState( void )
{
  if( amp_playmode == AMP_NOFILE ) {
    return "NO_MEDIA_PRESENT";
  } else if( is_busy()) {
    return "TRANSITIONING";
  } else if( is_paused()) {
    return "PAUSED_PLAYBACK";
  } else if( decoder_playing()) {
    return "PLAYING";
  } else {
    return "STOPPED";
  }
}

/* Returns value indicates whether the current media is track-aware. */
static const char*
upnp_MediaCategory( void )
{
  if( amp_playmode == AMP_NOFILE ) {
    return "NO_MEDIA";
  } else if( amp_playmode == AMP_PLAYLIST ) {
    return "TRACK_AWARE";
  } else {
    return "TRACK_UNAWARE";
  }
}

/* Retursn a comma-separated list of transport-controlling actions
   that can be successfully invoked for the current resource at
   this specific point in time. */
static const char*
upnp_TransportActions( void )
{
  if( amp_playmode == AMP_NOFILE ) {
    return "Stop";
  } else if( amp_playmode == AMP_PLAYLIST ) {
    return "Play,Stop,Pause,Seek,Next,Previous";
  } else {
    return "Play,Stop,Pause,Seek";
  }
}

/* Returns value indicates the storage medium of the currently played file */
static const char*
upnp_PlaybackStorageMedium( void )
{
  if( amp_playmode == AMP_NOFILE ) {
    return "NONE";
  } else if( is_track( current_filename )) {
    return "CD_DA";
  } else if( is_url( current_filename )) {
    return "NETWORK";
  } else if( is_regular_file( current_filename )) {
    return "HDD";
  } else {
    return "UNKNOWN";
  }
}

/* Returns the current play mode. */
static const char*
upnp_PlayMode( void )
{
  if( amp_playmode == AMP_SINGLE ) {
    if( cfg.rpt ) {
      return "REPEAT_ONE";
    } else {
      return "DIRECT_1";
    }
  } else if( amp_playmode == AMP_PLAYLIST ) {
    // Do not know how to specify simultaneous repeate and shuffling.
    if( cfg.rpt ) {
      return "REPEAT_ALL";
    } else if( cfg.shf ) {
      return "SHUFFLE";
    }
  }

  return "NORMAL";
}

/* Returns the current play speed. */
static const char*
upnp_PlaySpeed( void )
{
  if( is_forward()) {
    return "10";
  } else if( is_rewind()) {
    return "-10";
  }

  return "1";
}

/* Returns the number of tracks. */
static const char*
upnp_Tracks( char* Tracks )
{
  ULONG i;

  if( amp_playmode == AMP_NOFILE ) {
    i = 0;
  } else if( amp_playmode == AMP_PLAYLIST ) {
    i = pl_size();
  } else {
    i = 1;
  }

  return ltoa( i, Tracks, 10 );
}

/* Returns the number of the currently selected track. */
static const char*
upnp_CurrentTrack( char* Track )
{
  ULONG i;

  if( amp_playmode == AMP_NOFILE ) {
    i = 0;
  } else if( amp_playmode == AMP_PLAYLIST ) {
    i = pl_loaded_index();
  } else {
    i = 1;
  }

  return ltoa( i, Track, 10 );
}

/* Returns the duration of the current track. */
static const char*
upnp_TrackDuration( char* Duration )
{
  int ms = current_info.songlength;

  #ifdef UPNP_EXACT_TIME
    sprintf( Duration, "%u:%02u:%02u.%03u",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60, ms % 1000 );
  #else
    sprintf( Duration, "%u:%02u:%02u",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60 );
  #endif

  return Duration;
}

/* Returns the duration of the media. */
static const char*
upnp_MediaDuration( char* Duration )
{
  LONGLONG ms;

  if( amp_playmode == AMP_PLAYLIST ) {
    ms = pl_playtime();
  } else {
    ms = current_info.songlength;
  }

  #ifdef UPNP_EXACT_TIME
    sprintf( Duration, "%llu:%02llu:%02llu.%03llu",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60, ms % 1000 );
  #else
    sprintf( Duration, "%llu:%02llu:%02llu",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60 );
  #endif

  return Duration;
}

/* Return a reference, in the form of a URI, to the resource
   controlled by the AVTransport instance. */
static const char*
upnp_TransportURI( void )
{
  if( amp_playmode == AMP_PLAYLIST ) {
    return current_playlist;
  } else if( amp_playmode == AMP_SINGLE ) {
    return current_filename;
  } else {
    return "";
  }
}

/* Returns the current track metadata, in the form of
   a DIDL-Lite XML Fragment. */
static const char*
upnp_TrackMetaData( char* MetaData, int Size )
{
  AMP_FILE file = { "", "", sizeof( DECODER_INFO )};
  char strval[32];

  if( amp_playmode == AMP_NOFILE ) {
    strlcpy( MetaData, "", Size );
  } else {
    strlcpy( MetaData, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                       "<DIDL-Lite\n"
                       "xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
                       "xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\"\n"
                       "xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\"\n"
                       "xmlns:dlna=\"urn:schemas-dlna-org:metadata-1-0/\">\n", Size );
    strlcat( MetaData, "<item id=\"\" parentID=\"\" restricted=\"1\">\n", Size );
    strlcat( MetaData, "<upnp:class>object.item.audioItem.musicTrack</upnp:class>\n", Size );

    WinSendMsg( amp_player_window(), WM_123FILE_CURRENT, MPFROMP( &file ), 0 );

    if( *file.info.title ) {
      strlcat( MetaData, "<dc:title>", Size );
      ch_cat( CH_DEFAULT, file.info.title, CH_UTF_8, MetaData, Size );
      strlcat( MetaData, "</dc:title>\n", Size );
    }
    if( *file.info.artist ) {
      strlcat( MetaData, "<upnp:artist>", Size );
      ch_cat( CH_DEFAULT, file.info.artist, CH_UTF_8, MetaData, Size );
      strlcat( MetaData, "</upnp:artist>\n", Size );
    }
    if( *file.info.album ) {
      strlcat( MetaData, "<upnp:album>", Size );
      ch_cat( CH_DEFAULT, file.info.album, CH_UTF_8, MetaData, Size );
      strlcat( MetaData, "</upnp:album>\n", Size );
    }
    if( *file.info.genre ) {
      strlcat( MetaData, "<upnp:genre>", Size );
      ch_cat( CH_DEFAULT, file.info.genre, CH_UTF_8, MetaData, Size );
      strlcat( MetaData, "</upnp:genre>\n", Size );
    }
    if( *file.info.year ) {
      strlcat( MetaData, "<dc:date>", Size );
      snprintf( strval, sizeof( strval ), "%04lu-01-01", atol( file.info.year ));
      strlcat( MetaData, strval, Size );
      strlcat( MetaData, "</dc:date>\n", Size );
    }
    if( *file.info.track ) {
      strlcat( MetaData, "<upnp:originalTrackNumber>", Size );
      snprintf( strval, sizeof( strval ), "%lu", atol( file.info.track ));
      strlcat( MetaData, strval, Size );
      strlcat( MetaData, "</upnp:originalTrackNumber>\n", Size );
    }

    strlcat( MetaData, "<res", Size );
    strlcat( MetaData, " protocolInfo=\"", Size );
    strlcat( MetaData, is_url( file.filename ) ? "http-get:*:" : "internal:*:", Size );
    strlcat( MetaData, dec_mimetype( file.decoder ), Size );
    strlcat( MetaData, ":*\"", Size );
    strlcat( MetaData, " size=\"", Size );
    strlcat( MetaData, lltoa( file.info.filesize, strval, 10 ), Size );
    strlcat( MetaData, "\"", Size );
    strlcat( MetaData, " duration=\"", Size );
    strlcat( MetaData, upnp_TrackDuration( strval ), Size );
    strlcat( MetaData, "\"", Size );
    strlcat( MetaData, ">", Size );
    strlcat( MetaData, file.filename, Size );
    strlcat( MetaData, "</res>\n", Size );
    strlcat( MetaData, "</item>\n", Size );
    strlcat( MetaData, "</DIDL-Lite>", Size );

    amp_cleaninfo( &file );
  }

  return MetaData;
}

/* Returns the current position in the track. */
static const char*
upnp_RelativeTime( char* Position )
{
  int ms = decoder_playing() ? out_playing_pos() : 0;

  #ifdef UPNP_EXACT_TIME
    sprintf( Position, "%u:%02u:%02u.%03u",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60, ms % 1000 );
  #else
    sprintf( Position, "%u:%02u:%02u",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60 );
  #endif

  return Position;
}

/* Returns the current position in the playlist. */
static const char*
upnp_AbsoluteTime( char* Position )
{
  LONGLONG ms = decoder_playing() ? out_playing_pos() : 0;

  if( amp_playmode == AMP_PLAYLIST ) {
    ms += pl_played();
  }

  #ifdef UPNP_EXACT_TIME
    sprintf( Position, "%llu:%02llu:%02llu.%03llu",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60, ms % 1000 );
  #else
    sprintf( Position, "%llu:%02llu:%02llu",
      ms / 3600000, ms % 3600000 / 60000, ms / 1000 % 60 );
  #endif

  return Position;
}

/* Returns a Comma-Separated Value (CSV) list of information on
   protocols this ConnectionManager supports for 'sinking'
   (receiving) data. */
static const char*
upnp_SinkProtocolInfo( char* Info, int Size )
{
  dec_protocols( Info, Size );
  DEBUGLOG(( "upnp: SinkProtocolInfo=%s\n", Info ));
  return Info;
}

/* Notifies UPNP subsystem about player state change. */
void upnp_notify_state_change( int event )
{
  char strval[32];

  IXML_Document* notify;
  IXML_Element*  instance;
  IXML_Node*     body;
  IXML_Element*  var;
  DOMString      str;
  unsigned int   i;

  if( hdevice == -1 ) {
    return;
  }

  #if DEBUG
  if( event == UPNP_N_ALL ) {
    DEBUGLOG(( "upnp: receive notify - UPNP_N_ALL\n" ));
  } else if( event == UPNP_N_TIMERS ) {
    DEBUGLOG2(( "upnp: receive notify - UPNP_N_TIMERS\n" ));
  } else if( event == UPNP_N_NOTIFY ) {
    DEBUGLOG2(( "upnp: receive notify - UPNP_N_NOTIFY\n" ));
  } else {
    DEBUGLOG(( "upnp: receive notify -%s%s%s%s%s%s%s%s\n",
                event & UPNP_N_VOLUME ? " UPNP_N_VOLUME" : "",
                event & UPNP_N_PLAYERSTATE ? " UPNP_N_PLAYERSTATE" : "",
                event & UPNP_N_ERROR ? " UPNP_N_ERROR" : "",
                event & UPNP_N_PLAYERMODE ? " UPNP_N_PLAYERMODE" : "",
                event & UPNP_N_FILELOADED ? " UPNP_N_FILELOADED" : "",
                event & UPNP_N_TIMERS ? " UPNP_N_TIMERS" : "",
                event & UPNP_N_TRACKS ? " UPNP_N_TRACKS" : "",
                event & UPNP_N_PLUGIN ? " UPNP_N_PLUGIN" : "" ));
  }
  #endif

  if( event & UPNP_N_VOLUME ) {
    upnp_set_service_var( UPNP_RenderingControl, UPNP_VolumeDB,
                          itoa( upnp_VolumeToDB( cfg.defaultvol ), strval, 10 ));
    upnp_set_service_var( UPNP_RenderingControl, UPNP_Volume,
                          itoa( cfg.defaultvol, strval, 10 ));
    upnp_set_service_var( UPNP_RenderingControl, UPNP_Mute, cfg.mute ? "1" : "0" );
  }
  if( event & UPNP_N_PLAYERSTATE ) {
    upnp_set_service_var( UPNP_AVTransport, UPNP_TransportState, upnp_TransportState());
    upnp_set_service_var( UPNP_AVTransport, UPNP_TransportPlaySpeed, upnp_PlaySpeed());
  }
  if( event & UPNP_N_ERROR ) {
    upnp_set_service_var( UPNP_AVTransport, UPNP_TransportStatus, "ERROR_OCCURRED" );
  }
  if( event & UPNP_N_PLAYERMODE ) {
    upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentMediaCategory, upnp_MediaCategory());
    upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentPlayMode, upnp_PlayMode());
    upnp_set_service_var( UPNP_AVTransport, UPNP_AVTransportURI, upnp_TransportURI());
    upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentTransportActions, upnp_TransportActions());
    upnp_set_service_var( UPNP_AVTransport, UPNP_AVTransportURIMetaData,
                          amp_playmode == AMP_SINGLE ? state_AVTransport[UPNP_CurrentTrackMetaData].strval : "" );
  }
  if( event & UPNP_N_FILELOADED )
  {
    char* MetaData = malloc( MAX_METADATA );

    upnp_set_service_var( UPNP_AVTransport, UPNP_AVTransportURI, upnp_TransportURI());
    upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentTrackURI, current_filename );
    upnp_set_service_var( UPNP_AVTransport, UPNP_PlaybackStorageMedium, upnp_PlaybackStorageMedium());
    upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentTrackDuration, upnp_TrackDuration( strval ));

    if( MetaData ) {
      upnp_TrackMetaData( MetaData, MAX_METADATA );
      upnp_set_service_var( UPNP_AVTransport, UPNP_AVTransportURIMetaData, amp_playmode == AMP_SINGLE ? MetaData : "" );
      upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentTrackMetaData, MetaData );
      free( MetaData );
    }
  }
  if( event & UPNP_N_TRACKS ) {
    upnp_set_service_var( UPNP_AVTransport, UPNP_NumberOfTracks, upnp_Tracks( strval ));
    upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentTrack, upnp_CurrentTrack( strval ));
    upnp_set_service_var( UPNP_AVTransport, UPNP_CurrentMediaDuration, upnp_MediaDuration( strval ));
  }
  if( event & UPNP_N_TIMERS ) {
    upnp_set_service_var( UPNP_AVTransport, UPNP_RelativeTimePosition, upnp_RelativeTime( strval ));
    upnp_set_service_var( UPNP_AVTransport, UPNP_AbsoluteTimePosition, upnp_AbsoluteTime( strval ));
  }
  if( event & UPNP_N_PLUGIN ) {
    char* Info = malloc( MAX_METADATA );

    if( Info ) {
      upnp_set_service_var( UPNP_ConnectionManager, UPNP_SinkProtocolInfo, upnp_SinkProtocolInfo( Info, MAX_METADATA ));
      free( Info );
    }
  }

  if( event & UPNP_N_NOTIFY )
  {
    for( i = 0; i < ARRAY_SIZE( state_RenderingControl ); i++ ) {
      if( state_RenderingControl[i].flags & UPNP_VAR_CHANGED ) {
        break;
      }
    }

    if( i < ARRAY_SIZE( state_RenderingControl ))
    {
      upnp_state_request();

      if( ixmlParseBufferEx( notifyRCS, &notify ) == IXML_SUCCESS ) {
        if(( body = ixmlNode_getFirstChild((IXML_Node*)notify )) != NULL ) {
          if(( instance = ixmlDocument_createElement( notify, "InstanceID" )) != NULL ) {
            if( ixmlElement_setAttribute( instance, "val", "0" ) == IXML_SUCCESS ) {
              for( i = 0; i < ARRAY_SIZE( state_RenderingControl ); i++ ) {
                if( state_RenderingControl[i].flags & UPNP_VAR_CHANGED ) {
                  if(( var = ixmlDocument_createElement( notify, state_RenderingControl[i].name )) != NULL ) {
                    if( i == UPNP_Volume || i == UPNP_VolumeDB || i == UPNP_Mute ) {
                      ixmlElement_setAttribute( var, "Channel", "Master" );
                    }
                    if( ixmlElement_setAttribute( var, "val", state_RenderingControl[i].strval ) == IXML_SUCCESS ) {
                      ixmlNode_appendChild((IXML_Node*)instance, (IXML_Node*)var );
                      state_RenderingControl[i].flags &= ~UPNP_VAR_CHANGED;
                    }
                  }
                }
              }
              ixmlNode_appendChild( body, (IXML_Node*)instance );
            }
          }
        }

        str = ixmlNodetoString((IXML_Node*)notify );
        upnp_set_service_var( UPNP_RenderingControl, UPNP_LastChange, str );
        ixmlFreeDOMString( str );
        ixmlDocument_free( notify );
      }

      upnp_state_release();
    }

    for( i = 0; i < ARRAY_SIZE( state_AVTransport ); i++ ) {
      if( state_AVTransport[i].flags & UPNP_VAR_CHANGED ) {
        break;
      }
    }

    if( i < ARRAY_SIZE( state_AVTransport ))
    {
      upnp_state_request();

      if( ixmlParseBufferEx( notifyAVT, &notify ) == IXML_SUCCESS ) {
        if(( body = ixmlNode_getFirstChild((IXML_Node*)notify )) != NULL ) {
          if(( instance = ixmlDocument_createElement( notify, "InstanceID" )) != NULL ) {
            if( ixmlElement_setAttribute( instance, "val", "0" ) == IXML_SUCCESS ) {
              for( i = 0; i < ARRAY_SIZE( state_AVTransport ); i++ ) {
                if( state_AVTransport[i].flags & UPNP_VAR_CHANGED ) {
                  if(( var = ixmlDocument_createElement( notify, state_AVTransport[i].name )) != NULL ) {
                    if( ixmlElement_setAttribute( var, "val", state_AVTransport[i].strval ) == IXML_SUCCESS ) {
                      ixmlNode_appendChild((IXML_Node*)instance, (IXML_Node*)var );
                      state_AVTransport[i].flags &= ~UPNP_VAR_CHANGED;
                    }
                  }
                }
              }
              ixmlNode_appendChild( body, (IXML_Node*)instance );
            }
          }
        }

        str = ixmlNodetoString((IXML_Node*)notify );
        upnp_set_service_var( UPNP_AVTransport, UPNP_LastChange, str );
        ixmlFreeDOMString( str );
        ixmlDocument_free( notify );
      }

      upnp_state_release();
    }
  }
}

/* Retrieves the current value of the Volume state variable of the
   specified channel for the specified instance. */
static void
upnp_GetVolume( struct Upnp_Action_Request* event )
{
  char  CurrentVolume[16];
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* channel  = upnp_get_first_document_item( event->ActionRequest, "Channel" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !channel || stricmp( channel, "Master" ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Channel" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "GetVolume", service_type_RenderingControl, 1,
                              "CurrentVolume", itoa( cfg.defaultvol, CurrentVolume, 10 ));
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( channel  );
  free( instance );
}

/* Retrieves the current value of the VolumeDB state variable of the
   specified channel for the specified instance. */
static void
upnp_GetVolumeDB( struct Upnp_Action_Request* event )
{
  char  CurrentVolume[16];
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* channel  = upnp_get_first_document_item( event->ActionRequest, "Channel" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !channel || stricmp( channel, "Master" ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Channel" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "GetVolumeDB", service_type_RenderingControl, 1,
                              "CurrentVolume", itoa( upnp_VolumeToDB( cfg.defaultvol ), CurrentVolume, 10 ));
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( channel );
  free( instance );
}

/* Retrieves the current value of the VolumeDB state variable of the
   specified channel for the specified instance. */
static void
upnp_GetVolumeDBRange( struct Upnp_Action_Request* event )
{
  char  MinValue[16];
  char  MaxValue[16];
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* channel  = upnp_get_first_document_item( event->ActionRequest, "Channel" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !channel || stricmp( channel, "Master" ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Channel" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "GetVolumeDB", service_type_RenderingControl, 2,
                              "MinValue", itoa( upnp_VolumeToDB(   0 ), MinValue, 10 ),
                              "MaxValue", itoa( upnp_VolumeToDB( 100 ), MaxValue, 10 ));
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( channel );
  free( instance );
}

/* Retrieves the current value of the Mute state variable of the
   specified channel for the specified instance. */
static void
upnp_GetMute( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* channel  = upnp_get_first_document_item( event->ActionRequest, "Channel" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !channel || stricmp( channel, "Master" ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Channel" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "GetMute", service_type_RenderingControl, 1,
                              "CurrentMute", cfg.mute ? "1" : "0" );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( channel  );
  free( instance );
}

/* Sets the Volume state variable of the specified instance and
   channel to the specified value. */
static void
upnp_SetVolume( struct Upnp_Action_Request* event )
{
  unsigned int DesiredVolume;
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* channel  = upnp_get_first_document_item( event->ActionRequest, "Channel" );
  char* volume   = upnp_get_first_document_item( event->ActionRequest, "DesiredVolume" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !channel || stricmp( channel, "Master" ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Channel" );
  } else if( !volume || ( DesiredVolume = atoi( volume )) > 100 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "DesiredVolume" );
  } else if( amp_set_volume( DesiredVolume )) {
    event->ActionResult =
      UpnpMakeActionResponse( "SetVolume", service_type_RenderingControl, 0, NULL );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( volume );
  free( channel );
  free( instance );
}

/* Sets the VolumeDB state variable of the specified instance and
   channel to the specified value. */
static void
upnp_SetVolumeDB( struct Upnp_Action_Request* event )
{
  unsigned int DesiredVolume;
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* channel  = upnp_get_first_document_item( event->ActionRequest, "Channel" );
  char* volume   = upnp_get_first_document_item( event->ActionRequest, "DesiredVolume" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !channel || stricmp( channel, "Master" ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Channel" );
  } else if( !volume || ( DesiredVolume = upnp_DBToVolume( atoi( volume ))) > 100 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "DesiredVolume" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "SetVolumeDB", service_type_RenderingControl, 0, NULL );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( volume );
  free( channel );
  free( instance );
}

/* Sets the Mute state variable of the specified instance and
   channel to the specified value. */
static void
upnp_SetMute( struct Upnp_Action_Request* event )
{
  unsigned int DesiredMute;
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* channel  = upnp_get_first_document_item( event->ActionRequest, "Channel" );
  char* mute     = upnp_get_first_document_item( event->ActionRequest, "DesiredMute" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !channel || stricmp( channel, "Master" ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Channel" );
  } else if( !mute || ( DesiredMute = atoi( mute )) > 1 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "DesiredMute" );
  } else if( amp_mute( DesiredMute )) {
    event->ActionResult =
      UpnpMakeActionResponse( "SetMute", service_type_RenderingControl, 0, NULL );
  } else {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "DesiredMute" );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( mute );
  free( channel );
  free( instance );
}

/* Specifies the URI of the resource to be controlled by the
   specified AVTransport instance. */
static void
upnp_SetAVTransportURI( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* uri      = upnp_get_first_document_item( event->ActionRequest, "CurrentURI" );
  char* metadata = upnp_get_first_document_item( event->ActionRequest, "CurrentURIMetaData" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !uri ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid CurrentURI" );
  }
  else
  {
    BOOL decoder_was_playing = decoder_playing();
    DEBUGLOG(( "upnp: CurrentURI=%s\n", uri ));

    if( amp_load_singlefile( uri, AMP_LOAD_NOT_RECALL | AMP_LOAD_NOT_PLAY ) &&
        upnp_set_service_var( UPNP_AVTransport, UPNP_TransportStatus, "OK" ) == UPNP_E_SUCCESS )
    {
      event->ActionResult =
        UpnpMakeActionResponse( "SetAVTransportURI", service_type_AVTransport, 0, NULL );

      if( metadata )
      {
        IXML_Document* meta;
        AMP_FILE file = { "", "", sizeof( DECODER_INFO )};

        DEBUGLOG(( "upnp: CurrentURIMetaData=%s\n", metadata ));
        if( ixmlParseBufferEx( metadata, &meta ) == IXML_SUCCESS )
        {
          char* title  = upnp_get_first_document_item( meta, "dc:title"  );
          char* artist = upnp_get_first_document_item( meta, "upnp:artist" );
          char* album  = upnp_get_first_document_item( meta, "upnp:album" );
          char* genre  = upnp_get_first_document_item( meta, "upnp:genre" );
          char* date   = upnp_get_first_document_item( meta, "dc:date" );
          char* track  = upnp_get_first_document_item( meta, "upnp:originalTrackNumber" );
          char* art    = upnp_get_first_document_item( meta, "upnp:albumArtURI" );

          WinSendMsg( amp_player_window(), WM_123FILE_CURRENT, MPFROMP( &file ), 0 );

          if( title  ) { ch_convert( CH_UTF_8, title,  CH_DEFAULT, file.info.title,  sizeof( file.info.title  )); }
          if( artist ) { ch_convert( CH_UTF_8, artist, CH_DEFAULT, file.info.artist, sizeof( file.info.artist )); }
          if( album  ) { ch_convert( CH_UTF_8, album,  CH_DEFAULT, file.info.album,  sizeof( file.info.album  )); }
          if( genre  ) { ch_convert( CH_UTF_8, genre,  CH_DEFAULT, file.info.genre,  sizeof( file.info.genre  )); }
          if( date   ) { ch_convert( CH_UTF_8, date,   CH_DEFAULT, file.info.year,   sizeof( file.info.year   )); }
          if( track  ) { ch_convert( CH_UTF_8, track,  CH_DEFAULT, file.info.track,  sizeof( file.info.track  )); }

          if( art ) {
            APIC* pic = tag_read_picture( art );

            if( pic ) {
              DEBUGLOG(( "upnp: done loading '%s' picture: %s\n", pic->mimetype, art ));
              dec_freepics( file.info.pics, file.info.pics_count );
              if(( file.info.pics = malloc( sizeof( APIC* ))) != NULL ) {
                file.info.pics[0] = pic;
                file.info.pics_count = 1;
              } else {
                file.info.pics_count = 0;
              }
            } else {
              DEBUGLOG(( "upnp: unable load picture: %s\n", art ));
            }
          }

          WinSendMsg( amp_player_window(), WM_123FILE_REFRESH, MPFROMP( &file ), 0 );
          amp_cleaninfo( &file );

          free( track  );
          free( date   );
          free( genre  );
          free( album  );
          free( artist );
          free( title  );
        }
      }

      if( decoder_was_playing ) {
        amp_play( 0 );
      }
    }
  }

  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( metadata );
  free( uri );
  free( instance );
}

/* This required action returns information associated with the current media of the specified
   instance; it has no effect on state. */
static void
upnp_GetMediaInfo( struct Upnp_Action_Request* event )
{
  char  NrTrack[32];
  char  MediaDuration[32];
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* MetaData = malloc( MAX_METADATA );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( MetaData ) {
    event->ActionResult =
      UpnpMakeActionResponse( "GetMediaInfo", service_type_AVTransport, 9,
                              "NrTracks", upnp_Tracks( NrTrack ),
                              "MediaDuration", upnp_MediaDuration( MediaDuration ),
                              "CurrentURI", upnp_TransportURI(),
                              "CurrentURIMetaData", amp_playmode == AMP_SINGLE ? upnp_TrackMetaData( MetaData, MAX_METADATA ) : "",
                              "NextURI", "NOT_IMPLEMENTED",
                              "NextURIMetaData", "NOT_IMPLEMENTED",
                              "PlayMedium", upnp_PlaybackStorageMedium(),
                              "RecordMedium", "NOT_IMPLEMENTED",
                              "WriteStatus", "NOT_IMPLEMENTED" );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( MetaData );
  free( instance );
}

/* This required action returns information associated with the current media of the specified
   instance; it has no effect on state.The information returned is identical to the information
   returned by the GetMediaInfo() action, except for the additionally returned CurrentType
   argument. */
static void
upnp_GetMediaInfo_Ext( struct Upnp_Action_Request* event )
{
  char  NrTrack[32];
  char  MediaDuration[32];
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* MetaData = malloc( MAX_METADATA );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( MetaData ) {
    event->ActionResult =
      UpnpMakeActionResponse( "GetMediaInfo_Ext", service_type_AVTransport, 10,
                              "CurrentType", upnp_MediaCategory(),
                              "NrTracks", upnp_Tracks( NrTrack ),
                              "MediaDuration", upnp_MediaDuration( MediaDuration ),
                              "CurrentURI", upnp_TransportURI(),
                              "CurrentURIMetaData", amp_playmode == AMP_SINGLE ? upnp_TrackMetaData( MetaData, MAX_METADATA ) : "",
                              "NextURI", "NOT_IMPLEMENTED",
                              "NextURIMetaData", "NOT_IMPLEMENTED",
                              "PlayMedium", upnp_PlaybackStorageMedium(),
                              "RecordMedium", "NOT_IMPLEMENTED",
                              "WriteStatus", "NOT_IMPLEMENTED" );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( MetaData );
  free( instance );
}

/* This required action returns information associated with the current
   transport state of the specified instance. */
static void
upnp_GetTransportInfo( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else {
    upnp_state_request();
    event->ActionResult =
      UpnpMakeActionResponse( "GetTransportInfo", service_type_AVTransport, 3,
                              "CurrentTransportState", upnp_TransportState(),
                              "CurrentTransportStatus", state_AVTransport[UPNP_TransportStatus].strval,
                              "CurrentSpeed", upnp_PlaySpeed());
    upnp_state_release();
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* This required action returns information associated with the current
   position of the transport of the specified instance; it has no effect
   on state. */
static void
upnp_GetPositionInfo( struct Upnp_Action_Request* event )
{
  char  Track[32];
  char  TrackDuration[32];
  char  RelTime[32];
  char  AbsTime[32];
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* MetaData = malloc( MAX_METADATA );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( MetaData ) {
    event->ActionResult =
      UpnpMakeActionResponse( "GetPositionInfo", service_type_AVTransport, 8,
                              "Track", upnp_CurrentTrack( Track ),
                              "TrackDuration", upnp_TrackDuration( TrackDuration ),
                              "TrackMetaData", upnp_TrackMetaData( MetaData, MAX_METADATA ),
                              "TrackURI", current_filename,
                              "RelTime", upnp_RelativeTime( RelTime ),
                              "AbsTime", upnp_AbsoluteTime( AbsTime ),
                              "RelCount", "2147483647",
                              "AbsCount", "2147483647" );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( MetaData );
  free( instance );
}

/* This required action returns information on device capabilities of the
   specified instance, such as the supported playback and recording
   formats, and the supported quality levels for recording. */
static void
upnp_GetDeviceCapabilities( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else {
    upnp_state_request();
    event->ActionResult =
      UpnpMakeActionResponse( "GetDeviceCapabilities", service_type_AVTransport, 3,
                              "PlayMedia", state_AVTransport[UPNP_TransportStatus].strval,
                              "RecMedia", "NOT_IMPLEMENTED",
                              "RecQualityModes", "NOT_IMPLEMENTED" );
    upnp_state_release();
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* This required action returns information on various settings of the
   specified instance, such as the current play mode and the current
   recording quality mode. */
static void
upnp_GetTransportSettings( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "GetTransportSettings", service_type_AVTransport, 2,
                              "PlayMode", upnp_PlayMode(),
                              "RecQualityMode", "NOT_IMPLEMENTED" );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* Stops the progression of the current resource. */
static void
upnp_Stop( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !is_busy() && amp_stop()) {
    pl_clean_shuffle();
    event->ActionResult =
      UpnpMakeActionResponse( "Stop", service_type_AVTransport, 0, NULL );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* Starts playing the resource of the specified instance, at the specified
   speed, starting at the current position, according to the current
   play mode. */
static void
upnp_Play( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* speed    = upnp_get_first_document_item( event->ActionRequest, "Speed" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !speed || ( strcmp( speed, "1" ) != 0 && strcmp( speed, "10" ) != 0 && strcmp( speed, "-10" ) != 0 )) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Speed" );
  } else if( decoder_playing() || amp_play( 0 )) {
    if( strcmp( speed, "10" ) == 0 ) {
      if( !is_forward()) {
         amp_forward();
      }
    } else if( strcmp( speed, "-10" ) == 0 ) {
      if( !is_rewind()) {
        amp_rewind();
      }
    } else {
      if( is_forward()) {
        amp_forward();
      }
      if( is_rewind()) {
        amp_rewind();
      }
    }
    if( is_paused()) {
      amp_pause();
    }
    event->ActionResult =
      UpnpMakeActionResponse( "Play", service_type_AVTransport, 0, NULL );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( speed );
  free( instance );
}
/* Halts the progression of the current resource. */
static void
upnp_Pause( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !is_busy()) {
    if( is_paused() || amp_pause()) {
      event->ActionResult =
        UpnpMakeActionResponse( "Pause", service_type_AVTransport, 0, NULL );
    }
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* Starts seeking through the resource controlled by the specified instance */
static void
upnp_Seek( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* unit     = upnp_get_first_document_item( event->ActionRequest, "Unit" );
  char* target   = upnp_get_first_document_item( event->ActionRequest, "Target" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !unit ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Unit" );
  } else if( !target ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Target" );
  } else if( stricmp( unit, "TRACK_NR" ) == 0 ) {
    ULONG index = atol( target );
    if( amp_playmode == AMP_PLAYLIST && index >= 1 && index <= pl_size()) {
      if( amp_load_record( index, 0 )) {
        event->ActionResult =
          UpnpMakeActionResponse( "Seek", service_type_AVTransport, 0, NULL );
      }
    } else {
      SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Target" );
    }
  } else if( stricmp( unit, "REL_TIME" ) == 0 ) {
    ULONG hours = 0;
    ULONG mins  = 0;
    float secs  = 0;
    ULONG ms;

    sscanf( target, "%lu:%lu:%f", &hours, &mins, &secs );
    ms = hours * 3600000 + mins * 60000 + secs * 1000;
    DEBUGLOG(( "upnp: relative pos is %lu ms\n", ms ));
    if( amp_seek( ms )) {
      event->ActionResult =
        UpnpMakeActionResponse( "Seek", service_type_AVTransport, 0, NULL );
    }
  } else {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid Unit" );
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( target );
  free( unit );
  free( instance );
}

/* This action is used to advance to the next track. */
static void
upnp_Next( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !is_busy()) {
    if( amp_next()) {
      event->ActionResult =
        UpnpMakeActionResponse( "Seek", service_type_AVTransport, 0, NULL );
    }
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* This action is used to advance to the previous track. */
static void
upnp_Previous( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !is_busy()) {
    if( amp_previous()) {
      event->ActionResult =
        UpnpMakeActionResponse( "Seek", service_type_AVTransport, 0, NULL );
    }
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* This allowed action sets the play mode of the specified
   AVTransport instance. */
static void
upnp_SetPlayMode( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  char* playmode = upnp_get_first_document_item( event->ActionRequest, "NewPlayMode" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else if( !playmode ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "NewPlayMode" );
  } else if( stricmp( playmode, "NORMAL" ) == 0 ) {
    amp_repeat ( FALSE );
    amp_shuffle( FALSE );
    event->ActionResult =
      UpnpMakeActionResponse( "SetPlayMode", service_type_AVTransport, 0, NULL );
  } else if( amp_playmode == AMP_SINGLE &&
             stricmp( playmode, "REPEAT_ONE" ) == 0 )
  {
    amp_repeat ( TRUE  );
    amp_shuffle( FALSE );
    event->ActionResult =
      UpnpMakeActionResponse( "SetPlayMode", service_type_AVTransport, 0, NULL );
  } else if( amp_playmode == AMP_SINGLE &&
             stricmp( playmode, "DIRECT_1" ) == 0 )
  {
    amp_repeat ( FALSE );
    amp_shuffle( FALSE );
    event->ActionResult =
      UpnpMakeActionResponse( "SetPlayMode", service_type_AVTransport, 0, NULL );
  } else if( amp_playmode == AMP_PLAYLIST &&
              stricmp( playmode, "REPEAT_ALL" ) == 0 )
  {
    amp_repeat ( TRUE  );
    amp_shuffle( FALSE );
    event->ActionResult =
      UpnpMakeActionResponse( "SetPlayMode", service_type_AVTransport, 0, NULL );
  } else if( amp_playmode == AMP_PLAYLIST &&
             stricmp( playmode, "SHUFFLE" ) == 0 )
  {
    amp_repeat ( FALSE  );
    amp_shuffle( TRUE );
    event->ActionResult =
      UpnpMakeActionResponse( "SetPlayMode", service_type_AVTransport, 0, NULL );
  } else {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "NewPlayMode" );
  }

  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( playmode );
  free( instance );
}

/* This allowed action returns the CurrentTransportActions state variable
   for the specified instance. */
static void
upnp_GetCurrentTransportActions( struct Upnp_Action_Request* event )
{
  char* instance = upnp_get_first_document_item( event->ActionRequest, "InstanceID" );
  event->ErrCode = UPNP_E_SUCCESS;

  if( !instance || atoi( instance ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid InstanceID" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "GetCurrentTransportActions", service_type_AVTransport, 1,
                              "Actions", upnp_TransportActions());
  }
  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( instance );
}

/* Returns the protocol-related info that this ConnectionManager supports
   in its current state. */
static void
upnp_GetProtocolInfo( struct Upnp_Action_Request* event )
{
  char* Sink = malloc( MAX_METADATA );

  event->ErrCode = UPNP_E_SUCCESS;
  event->ActionResult =
    UpnpMakeActionResponse( "GetProtocolInfo", service_type_ConnectionManager, 2,
                            "Source", "",
                            "Sink", upnp_SinkProtocolInfo( Sink, MAX_METADATA ));

  if( !event->ActionResult ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( Sink );
}

/* Returns a Comma-Separated Value list of ConnectionIDs of currently
   ongoing Connections. */
static void
upnp_GetCurrentConnectionIDs( struct Upnp_Action_Request* event )
{
  event->ErrCode = UPNP_E_SUCCESS;
  event->ActionResult =
    UpnpMakeActionResponse( "GetCurrentConnectionIDs", service_type_ConnectionManager, 1,
                            "ConnectionIDs", "0" );

  if( !event->ActionResult ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }
}

/* Returns associated information of the connection referred to
   by the ConnectionID input argument. */
static void
upnp_GetCurrentConnectionInfo( struct Upnp_Action_Request* event )
{
  char* connection = upnp_get_first_document_item( event->ActionRequest, "ConnectionID" );

  event->ErrCode = UPNP_E_SUCCESS;

  if( !connection || atoi( connection ) != 0 ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_INVALID_ARGS, "Invalid ConnectionID" );
  } else {
    event->ActionResult =
      UpnpMakeActionResponse( "GetCurrentConnectionInfo", service_type_ConnectionManager, 7,
                              "RcsID", "0",
                              "AVTransportID", "0",
                              "ProtocolInfo", "",
                              "PeerConnectionManager", "",
                              "PeerConnectionID", "-1",
                              "Direction", "Input",
                              "Status", "OK" );
  }

  if( !event->ActionResult && event->ErrCode != UPNP_SOAP_E_INVALID_ARGS ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }

  free( connection );
}

/* Returns a Features XML Document ConnectionManager Features this
   device supports, if any. */
static void
upnp_GetFeatureList( struct Upnp_Action_Request* event )
{
  event->ErrCode = UPNP_E_SUCCESS;

  upnp_state_request();
  event->ActionResult =
    UpnpMakeActionResponse( "GetCurrentConnectionIDs", service_type_ConnectionManager, 1,
                            "FeatureList", state_ConnectionManager[UPNP_FeatureList].strval );
  upnp_state_release();

  if( !event->ActionResult ) {
    SET_UPNP_ERROR( UPNP_SOAP_E_ACTION_FAILED, "Internal error" );
  }
}

/* Handle an action request. */
static int
upnp_handle_action_request( struct Upnp_Action_Request* event )
{
  DEBUGLOG(( "upnp: action %s request DevUDN=%s, ServiceID=%s\n",
              event->ActionName, event->DevUDN, event->ServiceID ));

  event->ErrCode = UPNP_SOAP_E_INVALID_ACTION;
  strcpy( event->ErrStr, "Invalid Action" );
  event->ActionResult = NULL;

  if( strcmp( event->DevUDN, udn ) == 0 ) {
    if( strcmp( event->ServiceID, service_id_RenderingControl ) == 0 ) {
      if( strcmp( event->ActionName, "GetVolume" ) == 0 ) {
        upnp_GetVolume( event );
      } else if( strcmp( event->ActionName, "SetVolume" ) == 0 ) {
        upnp_SetVolume( event );
      } else if( strcmp( event->ActionName, "GetVolumeDB" ) == 0 ) {
        upnp_GetVolumeDB( event );
      } else if( strcmp( event->ActionName, "GetVolumeDBRange" ) == 0 ) {
        upnp_GetVolumeDBRange( event );
      } else if( strcmp( event->ActionName, "SetVolumeDB" ) == 0 ) {
        upnp_SetVolumeDB( event );
      } else if( strcmp( event->ActionName, "GetMute" ) == 0 ) {
        upnp_GetMute( event );
      } else if( strcmp( event->ActionName, "SetMute" ) == 0 ) {
        upnp_SetMute( event );
      }
    } else if( strcmp( event->ServiceID, service_id_AVTransport ) == 0 ) {
      if( strcmp( event->ActionName, "SetAVTransportURI" ) == 0 ) {
        upnp_SetAVTransportURI( event );
      } else if( strcmp( event->ActionName, "GetMediaInfo" ) == 0 ) {
        upnp_GetMediaInfo( event );
      } else if( strcmp( event->ActionName, "GetMediaInfo_Ext" ) == 0 ) { // v3.0 only
        upnp_GetMediaInfo_Ext( event );
      } else if( strcmp( event->ActionName, "GetTransportInfo" ) == 0 ) {
        upnp_GetTransportInfo( event );
      } else if( strcmp( event->ActionName, "GetPositionInfo" ) == 0 ) {
        upnp_GetPositionInfo( event );
      } else if( strcmp( event->ActionName, "GetDeviceCapabilities" ) == 0 ) {
        upnp_GetDeviceCapabilities( event );
      } else if( strcmp( event->ActionName, "GetTransportSettings" ) == 0 ) {
        upnp_GetTransportSettings( event );
      } else if( strcmp( event->ActionName, "Stop" ) == 0 ) {
        upnp_Stop( event );
      } else if( strcmp( event->ActionName, "Play" ) == 0 ) {
        upnp_Play( event );
      } else if( strcmp( event->ActionName, "Pause" ) == 0 ) {
        upnp_Pause( event );
      } else if( strcmp( event->ActionName, "Seek" ) == 0 ) {
        upnp_Seek( event );
      } else if( strcmp( event->ActionName, "Next" ) == 0 ) {
        upnp_Next( event );
      } else if( strcmp( event->ActionName, "Previous" ) == 0 ) {
        upnp_Previous( event );
      } else if( strcmp( event->ActionName, "SetPlayMode" ) == 0 ) {
        upnp_SetPlayMode( event );
      } else if( strcmp( event->ActionName, "GetCurrentTransportActions" ) == 0 ) {
        upnp_GetCurrentTransportActions( event );
      }
    } else if( strcmp( event->ServiceID, service_id_ConnectionManager ) == 0 ) {
      if( strcmp( event->ActionName, "GetProtocolInfo" ) == 0 ) {
        upnp_GetProtocolInfo( event );
      } else if( strcmp( event->ActionName, "GetCurrentConnectionIDs" ) == 0 ) {
        upnp_GetCurrentConnectionIDs( event );
      } else if( strcmp( event->ActionName, "GetCurrentConnectionInfo" ) == 0 ) {
        upnp_GetCurrentConnectionInfo( event );
      } else if( strcmp( event->ActionName, "GetFeatureList" ) == 0 ) { // v3.0 only
        upnp_GetFeatureList( event );
      }
    }
  }

  return event->ErrCode;
}

/* Returns a single service state variable. */
static int
upnp_handle_get_var_request( struct Upnp_State_Var_Request* event )
{
  unsigned int i;

  DEBUGLOG(( "upnp: get var %s request DevUDN=%s, ServiceID=%s\n",
              event->StateVarName, event->DevUDN, event->ServiceID ));

  event->ErrCode = UPNP_SOAP_E_INVALID_VAR;

  if( strcmp( event->DevUDN, udn ) == 0 ) {
    if( strcmp( event->ServiceID, service_id_RenderingControl ) == 0 ) {
      for( i = 0; i < ARRAY_SIZE( state_RenderingControl ); i++ ) {
        if( strcmp( event->StateVarName,  state_RenderingControl[i].name ) == 0 )
        {
          upnp_state_request();
          event->CurrentVal = ixmlCloneDOMString( state_RenderingControl[i].strval );
          event->ErrCode = UPNP_E_SUCCESS;
          upnp_state_release();
          break;
        }
      }
    } else if( strcmp( event->ServiceID, service_id_AVTransport ) == 0 ) {
      for( i = 0; i < ARRAY_SIZE( state_AVTransport ); i++ ) {
        if( strcmp( event->StateVarName,  state_AVTransport[i].name ) == 0 )
        {
          upnp_state_request();
          event->CurrentVal = ixmlCloneDOMString( state_AVTransport[i].strval );
          event->ErrCode = UPNP_E_SUCCESS;
          upnp_state_release();
          break;
        }
      }
    } else if( strcmp( event->ServiceID, service_id_ConnectionManager ) == 0 ) {
      for( i = 0; i < ARRAY_SIZE( state_ConnectionManager ); i++ ) {
        if( strcmp( event->StateVarName,  state_ConnectionManager[i].name ) == 0 )
        {
          upnp_state_request();
          event->CurrentVal = ixmlCloneDOMString( state_ConnectionManager[i].strval );
          event->ErrCode = UPNP_E_SUCCESS;
          upnp_state_release();
          break;
        }
      }
    }
  }

  if( event->ErrCode != UPNP_E_SUCCESS ) {
    DEBUGLOG(( "upnp: unknown variable %s\n", event->StateVarName ));
    strcpy( event->ErrStr, "Invalid Variable" );
  }

  return ( event->ErrCode == UPNP_E_SUCCESS );
}

/* Accepts a subscription request and sends out the current state of the
   eventable variables for a service. */
static int
upnp_handle_subscription_request( struct Upnp_Subscription_Request* event )
{
  IXML_Document* propset = NULL;
  IXML_Document* notify;
  IXML_Element*  instance;
  IXML_Node*     body;
  IXML_Element*  var;
  DOMString      changes;
  unsigned int   i;

  DEBUGLOG(( "upnp: subscription request DevUDN=%s, ServiceID=%s\n",
              event->UDN, event->ServiceId ));

  if( strcmp( event->UDN, udn ) == 0 )
  {
    upnp_state_request();

    if( strcmp( event->ServiceId, service_id_RenderingControl ) == 0 )
    {
      if( ixmlParseBufferEx( notifyRCS, &notify ) == IXML_SUCCESS ) {
        if(( body = ixmlNode_getFirstChild((IXML_Node*)notify )) != NULL ) {
          if(( instance = ixmlDocument_createElement( notify, "InstanceID" )) != NULL ) {
            if( ixmlElement_setAttribute( instance, "val", "0" ) == IXML_SUCCESS ) {
              for( i = 0; i < ARRAY_SIZE( state_RenderingControl ); i++ ) {
                if(( var = ixmlDocument_createElement( notify, state_RenderingControl[i].name )) != NULL ) {
                  if( i == UPNP_Volume || i == UPNP_VolumeDB ) {
                    ixmlElement_setAttribute( var, "Channel", "Master" );
                  }
                  if( ixmlElement_setAttribute( var, "val", state_RenderingControl[i].strval ) == IXML_SUCCESS ) {
                    ixmlNode_appendChild((IXML_Node*)instance, (IXML_Node*)var );
                    state_RenderingControl[i].flags &= ~UPNP_VAR_CHANGED;
                  }
                }
              }
              ixmlNode_appendChild( body, (IXML_Node*)instance );
            }
          }
        }
      }
      changes = ixmlNodetoString((IXML_Node*)notify );
      propset = UpnpCreatePropertySet( 1, "LastChange", changes );
      ixmlFreeDOMString( changes );
      ixmlDocument_free( notify );
    }
    else if( strcmp( event->ServiceId, service_id_AVTransport ) == 0 )
    {
      if( ixmlParseBufferEx( notifyAVT, &notify ) == IXML_SUCCESS ) {
        if(( body = ixmlNode_getFirstChild((IXML_Node*)notify )) != NULL ) {
          if(( instance = ixmlDocument_createElement( notify, "InstanceID" )) != NULL ) {
            if( ixmlElement_setAttribute( instance, "val", "0" ) == IXML_SUCCESS ) {
              for( i = 0; i < ARRAY_SIZE( state_AVTransport ); i++ ) {
                if(( var = ixmlDocument_createElement( notify, state_AVTransport[i].name )) != NULL ) {
                  if( ixmlElement_setAttribute( var, "val", state_AVTransport[i].strval ) == IXML_SUCCESS ) {
                    ixmlNode_appendChild((IXML_Node*)instance, (IXML_Node*)var );
                    state_AVTransport[i].flags &= ~UPNP_VAR_CHANGED;
                  }
                }
              }
              ixmlNode_appendChild( body, (IXML_Node*)instance );
            }
          }
        }
      }

      changes = ixmlNodetoString((IXML_Node*)notify );
      propset = UpnpCreatePropertySet( 1, "LastChange", changes );
      ixmlFreeDOMString( changes );
      ixmlDocument_free( notify );
    }
    else if( strcmp( event->ServiceId, service_id_ConnectionManager ) == 0 )
    {
      propset = UpnpCreatePropertySet( 3, "CurrentConnectionIDs", state_ConnectionManager[UPNP_CurrentConnectionIDs].strval,
                                          "SinkProtocolInfo", state_ConnectionManager[UPNP_SinkProtocolInfo].strval,
                                          "SourceProtocolInfo", state_ConnectionManager[UPNP_SourceProtocolInfo].strval );
    }
    upnp_state_release();
  }

  if( propset ) {
    UpnpAcceptSubscriptionExt( hdevice, event->UDN, event->ServiceId, propset, event->Sid );
    ixmlDocument_free( propset );
  }

  return 1;
}

/* Handles UPNP events. */
static int
upnp_callback_event_handler( Upnp_EventType type, void* event, void* cookie )
{
  switch( type ) {
    case UPNP_CONTROL_GET_VAR_REQUEST:
      upnp_handle_get_var_request((struct Upnp_State_Var_Request*)event );
      break;

    case UPNP_CONTROL_ACTION_REQUEST:
      upnp_handle_action_request((struct Upnp_Action_Request*)event );
      break;

    case UPNP_EVENT_SUBSCRIPTION_REQUEST:
      upnp_handle_subscription_request((struct Upnp_Subscription_Request*)event );
      break;

    case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
    case UPNP_DISCOVERY_SEARCH_RESULT:
    case UPNP_DISCOVERY_SEARCH_TIMEOUT:
    case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
    case UPNP_CONTROL_ACTION_COMPLETE:
    case UPNP_CONTROL_GET_VAR_COMPLETE:
    case UPNP_EVENT_RECEIVED:
    case UPNP_EVENT_RENEWAL_COMPLETE:
    case UPNP_EVENT_SUBSCRIBE_COMPLETE:
    case UPNP_EVENT_UNSUBSCRIBE_COMPLETE:
      break;

    default:
      DEBUGLOG(( "upnp: unknown event type %d\n", type ));
      break;
  }
  return 0;
}

/* Initializes of the UPNP subsystem. */
static void TFNENTRY
upnp_do_init( void* dummy )
{
  HAB   hab = WinInitialize( 0 );
  HMQ   hmq = WinCreateMsgQueue( hab, 0 );
  int   rc  = UPNP_E_SUCCESS;

  char* ip_address;
  unsigned short port;
  char desc_doc_url[200];
  char rootdir[_MAX_PATH];

  if( hdevice != -1 ) {
    goto done;
  }

  init_started = time( NULL );
  init_break = FALSE;

  while(( rc = UpnpInit( NULL, 0 )) != UPNP_E_SUCCESS ) {
    if( difftime( time( NULL ), init_started ) > MAX_INITTIME ) {
      amp_show_error( "Unable initialize UPNP\n%s", UpnpGetErrorMessage(rc));
      UpnpFinish();
      goto done;
    }
    if( init_break ) {
      goto done;
    }
    UpnpFinish();
    DEBUGLOG(( "upnp: unable initialize UPNP (%s), try again\n", UpnpGetErrorMessage(rc)));
    DosSleep( 1000 );
  }

  ip_address = UpnpGetServerIpAddress();
  port = UpnpGetServerPort();

  DEBUGLOG(( "upnp: initialization completed, ip_address=%s, port=%u\n",
              ip_address ? ip_address : "NULL", port ));

  DosCreateMutexSem( NULL, &mutex, 0, FALSE );

  strlcpy( rootdir, startpath , sizeof( rootdir ));
  strlcat( rootdir, "upnp", sizeof( rootdir ));

  if(( rc = UpnpSetWebServerRootDir( rootdir )) != UPNP_E_SUCCESS ) {
    amp_show_error( "Unable initialize UPNP internal server\n%s", UpnpGetErrorMessage(rc));
    upnp_term();
    goto done;
  }

  snprintf( desc_doc_url, sizeof( desc_doc_url ), "http://%s:%d/%s", ip_address, port, "pm123.xml" );
  DEBUGLOG(( "upnp: registering the root device: %s\n" , desc_doc_url ));

  rc = UpnpRegisterRootDevice( desc_doc_url, upnp_callback_event_handler, &hdevice, &hdevice );
  if( rc != UPNP_E_SUCCESS ) {
    amp_show_error( "Unable initialize UPNP root device\n%s", UpnpGetErrorMessage(rc));
    upnp_term();
    goto done;
  }

  upnp_notify_state_change( UPNP_N_ALL );

  if(( rc = UpnpSendAdvertisement( hdevice, 100 )) != UPNP_E_SUCCESS ) {
    amp_show_error( "Error sending UPNP advertisements\n%s", UpnpGetErrorMessage(rc));
    upnp_term();
    goto done;
  }

done:

  init_tid = 0;
  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
  _endthread();
}

/* Starts the UPNP subsystem initialization thread. */
void upnp_init( void )
{
  if( hdevice == -1 && !init_tid ) {
    DEBUGLOG(( "upnp: initialize\n" ));
    if(( init_tid = _beginthread( upnp_do_init, NULL, 204800, NULL )) == -1 ) {
      amp_show_error( "Unable create the UPNP initialization thread." );
    }
  }
}

/* Terminates of the UPNP subsystem. */
void upnp_term( void )
{
  while( init_tid ) {
    init_break = TRUE;
    DosSleep(1);
  }

  if( hdevice != -1 ) {
    DEBUGLOG(( "upnp: unregistering the root device\n" ));
    UpnpUnRegisterRootDevice( hdevice );
    hdevice = -1;
  }

  DEBUGLOG(( "upnp: finish\n" ));
  UpnpFinish();

  if( mutex ) {
    DosCloseMutexSem( mutex );
    mutex = NULLHANDLE;
  }
}

