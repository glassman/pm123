/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef  PM123_H
#define  PM123_H

#include <config.h>

#ifndef  RC_INVOKED
#include <decoder_plug.h>
#include "properties.h"
#include "copyright.h"
#include "plugman.h"
#endif

#define RT_PNG              400

#define ICO_MAIN              1
#define ICO_MP3            1700
#define ICO_MP3PLAY        1701
#define ICO_MP3GRAY        1702

#define WIN_MAIN              1
#define HLP_MAIN              1

#define MNU_MAIN            500
#define IDM_M_LOAD_MENU     501
#define IDM_M_LOAD_FILE     502
#define IDM_M_LOAD_URL      503
#define IDM_M_LOAD_TRACK    504
#define IDM_M_EDIT          505
#define IDM_M_BM_ADD        506
#define IDM_M_BM_EDIT       507
#define IDM_M_PLAYLIST      508
#define IDM_M_EQUALIZE      509
#define IDM_M_MANAGER       510
#define IDM_M_PROPERTIES    511
#define IDM_M_PLAYBACK      512
#define IDM_M_PLAY          513
#define IDM_M_PAUSE         514
#define IDM_M_FWD           515
#define IDM_M_REW           516
#define IDM_M_NEXT          517
#define IDM_M_PREV          518
#define IDM_M_VOL_RAISE     519
#define IDM_M_VOL_LOWER     520
#define IDM_M_SKINS         521
#define IDM_M_LOAD_SKIN     522
#define IDM_M_FONT_MENU     523
#define IDM_M_FONT_SET1     524
#define IDM_M_FONT_SET2     525
#define IDM_M_SIZE_MENU     526
#define IDM_M_SIZE_SMALL    527
#define IDM_M_SIZE_TINY     528
#define IDM_M_SIZE_NORMAL   529
#define IDM_M_SAVE_STREAM   530
#define IDM_M_FLOAT         531
#define IDM_M_QUIT          532
#define IDM_M_MINIMIZE      533
#define IDM_M_LOAD_DISC     535
#define IDM_M_LOAD_CLEAR    536
#define IDM_M_REW_5SEC      537
#define IDM_M_FWD_5SEC      538
#define IDM_M_OPEN_FOLDER   539
#define IDM_M_MUTE          540

#define IDM_M_LOAD_LAST   10000 /* A lot of IDs after this need to be free. */
#define IDM_M_BOOKMARKS   11000 /* A lot of IDs after this need to be free. */
#define IDM_M_PLUGINS     15000 /* A lot of IDs after this need to be free. */
#define IDM_M_DISCS       16000 /* A lot of IDs after this need to be free. */

#define DLG_URL            2014
#define ENT_URL             101

#define DLG_TRACK          2021
#define LB_TRACKS          2022
#define ST_DRIVE           2023
#define CB_DRIVE           2024
#define PB_REFRESH         2025

#define HLP_MAIN_TABLE      100
#define HLP_NULL_TABLE      101

#define IDH_MAIN           1000
#define IDH_ADVANTAGES     1001
#define IDH_ANALYZER       1002
#define IDH_SUPPORT        1003
#define IDH_COPYRIGHT      1004
#define IDH_DRAG_AND_DROP  1005
#define IDH_EQUALIZER      1006
#define IDH_ID3_EDITOR     1007
#define IDH_INTERFACE      1008
#define IDH_MAIN_MENU      1009
#define IDH_MAIN_WINDOW    1010
#define IDH_PM             1011
#define IDH_NETSCAPE       1012
#define IDH_COMMANDLINE    1013
#define IDH_PL             1014
#define IDH_PROPERTIES     1015
#define IDH_REMOTE         1016
#define IDH_SKIN_GUIDE     1017
#define IDH_SKIN_UTILITY   1018
#define IDH_TROUBLES       1019
#define IDH_FIREFOX        1020

#define AMP_NOFILE            0
#define AMP_SINGLE            1
#define AMP_PLAYLIST          2

/* amp_load_singlefile options */
#define AMP_LOAD_NOT_PLAY     0x0001
#define AMP_LOAD_NOT_RECALL   0x0002
#define AMP_LOAD_PLAY         0x0004

/* amp_load_url options */
#define URL_ADD_TO_PLAYER     0x0000
#define URL_ADD_TO_LIST       0x0001

/* amp_load_track options */
#define TRK_ADD_TO_PLAYER     0x0000
#define TRK_ADD_TO_LIST       0x0001

/* amp_invalidate options */
#define UPD_TIMERS            0x0001
#define UPD_FILEINFO          0x0002
#define UPD_VOLUME            0x0004
#define UPD_WINDOW            0x0008
#define UPD_FILENAME          0x0010
#define UPD_SCROLLSPEED       0x0020
#define UPD_SLIDER            0x0040
#define UPD_DELAYED           0x8000

/* amp_parse_filename options */
#define PFN_NAME              0x0001
#define PFN_NAMEEXT           0x0002
#define PFN_MASK              0x0003
#define PFN_PRESCHEME         0x8000

/* dll_main options */
#define ARG_SHUFFLE           0x0001
#define ARG_ASSO_ALL          0x0002
#define ARG_ASSO_CLEAR        0x0004
#define ARG_ASSO_ON           0x0008
#define ARG_ASSO_OFF          0x0010

#ifndef DC_PREPAREITEM
#define DC_PREPAREITEM        0x0040
#endif

#define TID_UPDATE_TIMERS   ( TID_USERMAX - 1 )
#define TID_UPDATE_PLAYER   ( TID_USERMAX - 2 )
#define TID_ONTOP           ( TID_USERMAX - 3 )
#define TID_LONGWAIT        ( TID_USERMAX - 4 )
#define TID_UPDATE_BITRATE  ( TID_USERMAX - 5 )
#define TID_NOTIFY_UPNP     ( TID_USERMAX - 6 )
#define TID_POPUP           ( TID_USERMAX - 7 )

#define _MAX_DRAG_IMAGES     5

/* The target window send this message to the a source windows
   after drag'n'drop operation if it is necessary to delete
   the moved records. */

#define WM_123FILE_REMOVE   ( WM_USER + 500 )

/* The target window send this message to the a source windows
   after drag'n'drop operation if it is necessary to load of
   the record. */

#define WM_123FILE_LOAD     ( WM_USER + 501 )

/* It is posted after successful saving of the meta information
   of the specified file. */

#define WM_123FILE_REFRESH  ( WM_USER + 503 )

/* The target window send this message to the a source windows
   to query full path and file name of the dropped record. */

#define WM_123FILE_PATHNAME ( WM_USER + 504 )

/* Returns the copy of the meta information of the currently
   loaded file. */

#define WM_123FILE_CURRENT  ( WM_USER + 505 )

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _AMP_DROPINFO {

  USHORT cditem;    /* Count of dragged objects. */
  HWND   hwndItem;  /* Window handle of the source of the drag operation. */
  ULONG  ulItemID;  /* Information used by the source to identify the
                       object being dragged. */
} AMP_DROPINFO;

typedef struct _AMP_FILE {

  char filename[_MAX_URL];          /* Name of the file. */
  char decoder [_MAX_MODULE_NAME];  /* Decoder used for playing this file. */
  DECODER_INFO info;                /* Information about of the file. */

} AMP_FILE, *PAMP_FILE;

/* Returns the handle of the player window. */
HWND  amp_player_window( void );
/* Returns the anchor-block handle. */
HAB   amp_player_hab( void );
/* Marks the player window as needed of redrawing. */
void  amp_invalidate( int options );
/* Posts a command to the message queue associated with the player window. */
BOOL  amp_post_command( USHORT id );

/* Activates or deactivates the current playlist. */
BOOL  amp_pl_use( BOOL use );
/* Loads a standalone file or CD track to the player and
   plays it if this is specified in the player properties. */
BOOL  amp_load_singlefile( const char* filename, int options );
/* Loads the specified playlist record to the player and
   plays it if this is specified in the player properties. */
BOOL  amp_load_record( ULONG index, int options );
/* Opens folder containing specified file. */
BOOL  amp_open_containing_folder( const char* filename );
/* Begins playback of the currently loaded file from the specified position. */
BOOL  amp_play( int pos );
/* Stops playback of the currently played file. */
BOOL  amp_stop( void );
/* Stops playing and resets the player to its default state. */
BOOL  amp_reset( void );
/* Suspends or resumes playback of the currently played file. */
BOOL  amp_pause( void );
/* Sets the audio volume to the specified level. */
BOOL  amp_set_volume( int volume );
/* Mute audio. */
BOOL  amp_mute( BOOL mute );
/* Toggles fast forwarding of the currently played file. */
BOOL  amp_forward( void );
/* Toggles rewinding of the currently played file. */
BOOL  amp_rewind( void );
/* Changes the current playing position of the currently played file. */
BOOL  amp_seek( int pos );
/* Advances to the next playlist record. */
BOOL  amp_next( void );
/* Advances to the previous playlist record. */
BOOL  amp_previous( void );
/* Enables the shuffle mode. */
BOOL  amp_shuffle( BOOL enable );
/* Enables the repeat mode. */
BOOL  amp_repeat( BOOL enable );

/* Adds URL to the playlist or load it to the player. */
BOOL  amp_load_url( HWND owner, int options );
/* Adds CD tracks to the playlist or load one to the player. */
BOOL  amp_load_track( HWND owner, int options );
/* Reads url from the specified file. */
char* amp_url_from_file( char* result, const char* filename, int size );
/* Extracts song title from the specified file name. */
char* amp_title_from_filename( char* result, const char* filename, int size );

/* Copies file information from one data structure to another. Also duplicates
   of the array of loaded attached pictures. */
void amp_copyinfo( AMP_FILE* dst, const AMP_FILE* src );
/* Resets the file information data structure and frees memory allocated
   by previous function. */
void amp_cleaninfo( AMP_FILE* file );

/* Creates and displays a error message window. */
void  amp_show_error( const char* format, ... );
/* Creates and displays a message window. */
void  amp_show_info ( const char* format, ... );
/* Requests the user about specified action. */
BOOL  amp_query( HWND owner, const char* format, ... );
/* Requests the user about overwriting a file. */
BOOL  amp_warn_if_overwrite( HWND owner, const char* filename );
/* Tells the help manager to display a specific help window. */
void  amp_show_help( SHORT resid );

/* Extracts specified part of the path and file name. */
char* amp_parse_filename( char* result, const char* filename, int options, int size );

int  DLLENTRY pm123_getstring    ( int type, int subtype, int size, char* buffer );
void DLLENTRY pm123_control      ( int type, void* param );
void DLLENTRY pm123_display_info ( char* );
void DLLENTRY pm123_display_error( char* );

/* Global variables */

extern int      amp_playmode; /* Play mode        */
extern HPOINTER mp3;          /* Song file icon   */
extern HPOINTER mp3play;      /* Played file icon */
extern HPOINTER mp3gray;      /* Broken file icon */
extern HMODULE  hmodule;

/* Contains startup path of the program without its name. */
extern char startpath[_MAX_PATH];
/* Contains a name of the currently loaded file. */
extern char current_filename[_MAX_URL];
/* Contains a information about of the currently loaded file. */
extern DECODER_INFO current_info;
/* Other parameters of the currently loaded file. */
extern char current_decoder[_MAX_MODULE_NAME];

#ifdef __cplusplus
}
#endif
#endif /* PM123_H */
