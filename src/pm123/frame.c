/*
 * Copyright 2012-2018 Dmitry A.Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_PM
#define  INCL_BASE
#include <os2.h>
#include <stdlib.h>
#include <string.h>
#include <debuglog.h>
#include <utilfct.h>

#include "skin.h"
#include "button95.h"

extern HBITMAP bmp_cache[];
extern ULONG   bmp_ulong[];
extern POINTL  bmp_pos  [];

typedef struct _FRAMEDATA {
  char* pszWindowText;
  HWND  hMinimizeBtn;
  HWND  hCloseBtn;
} FRAMEDATA;

#define LCID_FONT 1L

/* Adjusts current skin to the selected size of the player window. */
static void
bmp_frame_reflow( HWND hwnd, FRAMEDATA* data )
{
  DATA95 btn_data = { sizeof(DATA95)};
  HWND   hclient  = WinWindowFromID( hwnd, FID_CLIENT );
  RECTL  rect;

  if( hclient ) {
    if( WinQueryWindowRect( hwnd, &rect ))
    {
      WinSendMsg( hwnd, WM_CALCFRAMERECT, MPFROMP(&rect), MPFROMSHORT(TRUE));
      WinSetWindowPos( hclient, NULLHANDLE,
                       rect.xLeft, rect.yBottom,
                       rect.xRight - rect.xLeft,
                       rect.yTop - rect.yBottom,
                       SWP_SIZE | SWP_MOVE );
    }
  }

  if( bmp_pos[ POS_MINIMIZE ].x == POS_UNDEF || bmp_pos[ POS_MINIMIZE ].y == POS_UNDEF ) {
    if( data->hMinimizeBtn ) {
      WinDestroyWindow( data->hMinimizeBtn );
      data->hMinimizeBtn = NULLHANDLE;
    }
  } else {
    if( data->hMinimizeBtn ) {
      WinSendMsg( data->hMinimizeBtn, WM_CHANGEBMP, MPFROMLONG( &bmp_cache[ BMP_N_MINIMIZE ] ),
                                                    MPFROMLONG( &bmp_cache[ BMP_MINIMIZE   ] ));
    } else {
      btn_data.bmp_release = &bmp_cache[ BMP_N_MINIMIZE ];
      btn_data.bmp_pressed = &bmp_cache[ BMP_MINIMIZE   ];
      btn_data.hwnd_owner  =  hwnd;
      strcpy( btn_data.help, "Hide" );

      data->hMinimizeBtn = WinCreateWindow( hwnd, CLASSNAME, "", WS_VISIBLE, -100, -100,
                                            bmp_cx( BMP_MINIMIZE ), bmp_cy( BMP_MINIMIZE ),
                                            hwnd, HWND_TOP, BTN_SC_HIDE, &btn_data, NULL );
    }
    if( WinQueryWindowRect( hwnd, &rect )) {
      WinSetWindowPos( data->hMinimizeBtn, HWND_TOP,
                       rect.xRight - bmp_pos[ POS_MINIMIZE ].x,
                       rect.yTop - bmp_pos[ POS_MINIMIZE ].y,
                       bmp_cx( BMP_MINIMIZE ),
                       bmp_cy( BMP_MINIMIZE ),
                       SWP_MOVE | SWP_SIZE );
    }
  }

  if( bmp_pos[ POS_CLOSE ].x == POS_UNDEF || bmp_pos[ POS_CLOSE ].y == POS_UNDEF ) {
    if( data->hCloseBtn ) {
      WinDestroyWindow( data->hCloseBtn );
      data->hCloseBtn = NULLHANDLE;
    }
  } else {
    if( data->hCloseBtn ) {
      WinSendMsg( data->hCloseBtn, WM_CHANGEBMP, MPFROMLONG( &bmp_cache[ BMP_N_CLOSE ] ),
                                                 MPFROMLONG( &bmp_cache[ BMP_CLOSE   ] ));
    } else {
      btn_data.bmp_release = &bmp_cache[ BMP_N_CLOSE ];
      btn_data.bmp_pressed = &bmp_cache[ BMP_CLOSE   ];
      btn_data.hwnd_owner  =  hwnd;
      strcpy( btn_data.help, "Close" );

      data->hCloseBtn = WinCreateWindow( hwnd, CLASSNAME, "", WS_VISIBLE, -100, -100,
                                         bmp_cx( BMP_CLOSE ), bmp_cy( BMP_CLOSE ),
                                         hwnd, HWND_TOP, BTN_SC_CLOSE, &btn_data, NULL );
    }
    if( WinQueryWindowRect( hwnd, &rect )) {
      WinSetWindowPos( data->hCloseBtn, HWND_TOP,
                       rect.xRight - bmp_pos[ POS_CLOSE ].x,
                       rect.yTop - bmp_pos[ POS_CLOSE ].y,
                       bmp_cx( BMP_CLOSE ),
                       bmp_cy( BMP_CLOSE ),
                       SWP_MOVE | SWP_SIZE );
    }
  }
}

/* This function calculates a client rectangle from a frame rectangle,
   or a frame rectangle from a client rectangle. */
static void
bmp_frame_calc_rect( HWND hwnd, PRECTL pRect, BOOL isFrame )
{
  if( isFrame ) {
    pRect->yTop    -= bmp_cy( BMP_TTL_INA );
    pRect->xRight  -= bmp_cx( BMP_RIGHT   );
    pRect->yBottom += bmp_cy( BMP_BOTTOM  );
    pRect->xLeft   += bmp_cx( BMP_LEFT    );
  } else {
    pRect->yTop    += bmp_cy( BMP_TTL_INA );
    pRect->xRight  += bmp_cx( BMP_RIGHT   );
    pRect->yBottom -= bmp_cy( BMP_BOTTOM  );
    pRect->xLeft   -= bmp_cx( BMP_LEFT    );
  }
}

/* This message is called for a frame window whose position
   or size is to be adjusted. */
static void
bmp_frame_adjust( HWND hwnd, PSWP pswp )
{
  LONG size;
  SWP  oldpos;

  if( !WinQueryWindowPos( hwnd, &oldpos )) {
    return;
  }

  size = bmp_cx( BMP_TLFT_CRN_ACT ) +
         bmp_cx( BMP_TLFT_ACT ) +
         bmp_cx( BMP_LFT_TTL_ACT ) +
         bmp_cx( BMP_TTL_ACT ) +
         bmp_cx( BMP_RGT_TTL_ACT ) +
         bmp_cx( BMP_TRGT_ACT ) +
         bmp_cx( BMP_TRGT_CRN_ACT );

  if( pswp->cx < size ) {
    pswp->x  = oldpos.x;
    pswp->cx = size;
  }

  size = bmp_cx( BMP_BLFT_CRN ) +
         bmp_cx( BMP_BOTTOM ) +
         bmp_cx( BMP_BRGT_CRN );

  if( pswp->cx < size ) {
    pswp->x  = oldpos.x;
    pswp->cx = size;
  }

  size = bmp_cy( BMP_TLFT_CRN_ACT ) +
         bmp_cy( BMP_BLFT ) +
         bmp_cy( BMP_LEFT ) +
         bmp_cy( BMP_BLFT_CRN );

  if( pswp->cy < size ) {
    pswp->y  = oldpos.y;
    pswp->cy = size;
  }
}

static USHORT
bmp_frame_format( HWND hwnd, FRAMEDATA* data, PSWP pswp, USHORT count )
{
  USHORT i;

  if( pswp ) {
    for( i = 0; i < count; i++ ) {
      if( WinQueryWindowUShort( pswp[i].hwnd, QWS_ID ) == FID_CLIENT )
      {
         RECTL rect;
         WinQueryWindowRect( hwnd, &rect );
         WinSendMsg( hwnd, WM_CALCFRAMERECT, MPFROMP(&rect), MPFROMSHORT(TRUE));
         pswp[i].fl = SWP_MOVE | SWP_SIZE;
         pswp[i].x  = rect.xLeft;
         pswp[i].y  = rect.yBottom;
         pswp[i].cx = rect.xRight - rect.xLeft;
         pswp[i].cy = rect.yTop - rect.yBottom;
      }
    }
  }
  if( data->hMinimizeBtn ) {
    if( pswp )
    {
      RECTL rect;
      WinQueryWindowRect( hwnd, &rect );
      pswp[count].hwnd = data->hMinimizeBtn;
      pswp[count].fl   = SWP_MOVE | SWP_SIZE;
      pswp[count].x    = rect.xRight - bmp_pos[ POS_MINIMIZE ].x;
      pswp[count].y    = rect.yTop - bmp_pos[ POS_MINIMIZE ].y;
      pswp[count].cx   = bmp_cx( BMP_MINIMIZE );
      pswp[count].cy   = bmp_cy( BMP_MINIMIZE );
    }
    ++count;
  }
  if( data->hCloseBtn ) {
    if( pswp )
    {
      RECTL rect;
      WinQueryWindowRect( hwnd, &rect );
      pswp[count].hwnd = data->hCloseBtn;
      pswp[count].fl   = SWP_MOVE | SWP_SIZE;
      pswp[count].x    = rect.xRight - bmp_pos[ POS_CLOSE ].x;
      pswp[count].y    = rect.yTop - bmp_pos[ POS_CLOSE ].y;
      pswp[count].cx   = bmp_cx( BMP_CLOSE );
      pswp[count].cy   = bmp_cy( BMP_CLOSE );
    }
    ++count;
  }

  return count;
}

/* Paints frame title and borders. */
static void
bmp_frame_paint( HWND hwnd, HPS hps, BOOL erasebg )
{
  BOOL   is_active = WinIsChild( WinQueryActiveWindow( HWND_DESKTOP ), hwnd );
  BOOL   center = DT_CENTER;
  RECTL  rect;
  int    wnd_cx;
  int    wnd_cy;
  int    x;
  int    y;

  char   title[256] = "";
  int    title_len;
  int    title_max;
  int    title_y;
  int    title_x;

  int    id_topleft_corner;
  int    id_topleft;
  int    id_left_title;
  int    id_title;
  int    id_right_title;
  int    id_topright;
  int    id_topright_corner;

  char   fontname[256];
  FATTRS attrs;
  SIZEF  charbox;
  LONG   size;

  if( !WinQueryWindowRect( hwnd, &rect )) {
    return;
  }

  wnd_cx = rect.xRight - rect.xLeft + 1;
  wnd_cy = rect.yTop - rect.yBottom + 1;

  GpiCreateLogColorTable( hps, 0, LCOLF_RGB, 0, 0, 0 );

  if( WinWindowFromID( hwnd, FID_CLIENT ) == NULLHANDLE && erasebg )
  {
    ULONG bg = 0;

    if( !WinQueryPresParam( hwnd, PP_BACKGROUNDCOLOR, 0, NULL, sizeof(bg), &bg, QPF_NOINHERIT )) {
      bg = SYSCLR_DIALOGBACKGROUND;
    }
    DEBUGLOG(( "color: %08X\n", bg ));

    WinFillRect( hps, &rect, bg );
  }

  y = 0;
  x = 0;

  bmp_draw_bitmap( hps, x, y, BMP_BLFT_CRN );
  y += bmp_cy( BMP_BLFT_CRN );
  bmp_draw_bitmap( hps, x, y, BMP_BLFT );
  y += bmp_cy( BMP_BLFT );

  for( ; y < wnd_cy - bmp_cy( BMP_TLFT_CRN_ACT ); y += bmp_cy( BMP_LEFT )) {
    bmp_draw_bitmap( hps, x, y, BMP_LEFT );
  }

  y = 0;
  x = bmp_cx( BMP_BLFT_CRN );

  for( ; x < wnd_cx; x += bmp_cx( BMP_BOTTOM )) {
    bmp_draw_bitmap( hps, x, y, BMP_BOTTOM );
  }

  x = wnd_cx - bmp_cx( BMP_BRGT_CRN ) - 1;
  bmp_draw_bitmap( hps, x, y, BMP_BRGT_CRN );
  y += bmp_cy( BMP_BRGT_CRN );
  x = wnd_cx - bmp_cx( BMP_BRGT ) - 1;
  bmp_draw_bitmap( hps, x, y, BMP_BRGT );
  y += bmp_cy( BMP_BRGT );

  for( ; y < wnd_cy; y += bmp_cy( BMP_RIGHT )) {
    bmp_draw_bitmap( hps, x, y, BMP_RIGHT );
  }

  if( is_active ) {
    id_topleft_corner  = BMP_TLFT_CRN_ACT;
    id_topleft         = BMP_TLFT_ACT;
    id_left_title      = BMP_LFT_TTL_ACT;
    id_title           = BMP_TTL_ACT;
    id_right_title     = BMP_RGT_TTL_ACT;
    id_topright        = BMP_TRGT_ACT;
    id_topright_corner = BMP_TRGT_CRN_ACT;
  } else {
    id_topleft_corner  = BMP_TLFT_CRN_INA;
    id_topleft         = BMP_TLFT_INA;
    id_left_title      = BMP_LFT_TTL_INA;
    id_title           = BMP_TTL_INA;
    id_right_title     = BMP_RGT_TTL_INA;
    id_topright        = BMP_TRGT_INA;
    id_topright_corner = BMP_TRGT_CRN_INA;
  }

  // Calculate length of text of the window title.

  // Temporary ignore presentation parameters of the frame window during
  // painting of the window title because this produced some
  // problems with the owned windows.
  // if( !WinQueryPresParam( hwnd, PP_FONTNAMESIZE, 0, NULL, sizeof(fontname), fontname, QPF_NOINHERIT )) {
  strcpy( fontname, "2.System VIO" );
  // }

  fontname_to_attrs( fontname, &attrs, &size, &charbox );

  GpiSetColor( hps, bmp_ulong[ is_active ? UL_ACT_TITLE_FG : UL_TITLE_FG ]);
  GpiSetBackMix( hps, BM_LEAVEALONE );
  GpiCreateLogFont( hps, NULL, LCID_FONT, &attrs );
  GpiSetCharSet( hps, LCID_FONT );

  if( attrs.fsFontUse & FATTR_FONTUSE_OUTLINE ) {
    GpiSetCharBox( hps, &charbox );
  }

  rect.xLeft   = 0;
  rect.yBottom = 0;
  rect.xRight  = 32000;
  rect.yTop    = 32000;

  WinQueryWindowText( hwnd, sizeof( title ), title );

  WinDrawText( hps, -1, title, &rect, 0, 0, DT_LEFT | DT_VCENTER | DT_TEXTATTRS | DT_QUERYEXTENT );
  title_len = rect.xRight - rect.xLeft + 1;

  // Calculate maximal possible length of the window title text area.
  title_max = wnd_cx - bmp_cx( id_topleft_corner )
                     - bmp_cx( id_topleft )
                     - bmp_cx( id_left_title )
                     - bmp_cx( id_right_title )
                     - bmp_cx( id_topright )
                     - bmp_cx( id_topright_corner );

  // Length of the window title text area must be an integer product of
  // the width of the central bitmap of the title.
  title_max =  title_max / bmp_cx( id_title ) * bmp_cx( id_title );

  if( title_len > title_max ) {
    title_len = title_max;
    center = DT_LEFT;
  } else {
    title_len = ((( title_len - 1 ) / bmp_cx( id_title )) + 1 ) * bmp_cx( id_title );
  }

  title_y = wnd_cy - bmp_cy( id_topleft_corner ) - 1;
  title_x = ( wnd_cx - title_len ) / 2 - bmp_cx( id_left_title );

  x = 0;

  // Draw top left corner
  bmp_draw_bitmap( hps, x, title_y, id_topleft_corner );
  x += bmp_cx( id_topleft_corner );
  // and top left part of the window border.
  for( ; x < title_x; x += bmp_cx( id_topleft )) {
    bmp_draw_bitmap( hps, x, title_y, id_topleft );
  }

  // Draw top right corner
  x = wnd_cx - bmp_cx( id_topright_corner ) - 1;
  bmp_draw_bitmap( hps, x, title_y, id_topright_corner );
  // and top right part of the window border.
  title_max = title_x + title_len +
              bmp_cx( id_left_title ) + bmp_cx( id_right_title );

  while( x > title_max ) {
    x -= bmp_cx( id_topright );
    bmp_draw_bitmap( hps, x, title_y, id_topright );
  }

  // Draw left part of windows title,
  x = title_x;
  bmp_draw_bitmap( hps, x, title_y, id_left_title );
  x += bmp_cx( id_left_title );

  title_max -= bmp_cx( id_right_title );

  // draw central part of title,
  for( ; x < title_max; x += bmp_cx( id_title )) {
    bmp_draw_bitmap( hps, x, title_y, id_title );
  }

  // and, finally, draw right part of the windows title.
  bmp_draw_bitmap( hps, x, title_y, id_right_title );

  // Draw window title
  rect.xLeft   = title_x + bmp_cx( id_left_title );
  rect.yBottom = title_y + bmp_cy( id_left_title ) - bmp_ulong[ UL_TITLE_HEIGHT ] - 3;
  rect.xRight  = rect.xLeft + title_len;
  rect.yTop    = rect.yBottom + bmp_ulong[ UL_TITLE_HEIGHT ];

  WinDrawText( hps, -1, title, &rect,
               bmp_ulong[ is_active ? UL_ACT_TITLE_FG : UL_TITLE_FG ],
               0, center | DT_VCENTER | DT_TEXTATTRS );

  GpiSetCharSet ( hps, LCID_DEFAULT );
  GpiDeleteSetId( hps, LCID_FONT );
}

/* Processes messages of the frame window. */
MRESULT EXPENTRY
bmp_frame_wnd_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  FRAMEDATA* data = (FRAMEDATA*)WinQueryWindowPtr( hwnd, QWL_USER );

  if( !data ) {
    data = (FRAMEDATA*)calloc( sizeof( FRAMEDATA ), 1 );
    WinSetWindowPtr( hwnd, QWL_USER, (PVOID)data );
  }

  switch( msg ) {

    case WM_INITDLG:
      bmp_frame_reflow( hwnd, data );
      return 0;

    case WM_SETWINDOWPARAMS:
    {
      PWNDPARAMS params = (PWNDPARAMS)mp1;

      if( params->fsStatus == WPM_TEXT )
      {
        free( data->pszWindowText );
        data->pszWindowText = strdup( params->pszText );

        WinInvalidateRect( hwnd, NULL, FALSE );
        return MRFROMLONG( TRUE );
      }
      break;
    }

    case WM_QUERYWINDOWPARAMS:
    {
      PWNDPARAMS params = (PWNDPARAMS)mp1;

      if( params->fsStatus & ( WPM_TEXT | WPM_CCHTEXT )) {
        if( params->fsStatus & WPM_CCHTEXT ) {
          params->cchText = ( data->pszWindowText ? strlen( data->pszWindowText ) : 0 ) + 1;
        }
        if( params->fsStatus & WPM_TEXT ) {
          if( data->pszWindowText ) {
            strlcpy( params->pszText, data->pszWindowText, params->cchText );
          } else {
            params->pszText[0] = 0;
          }
        }
        return MRFROMLONG(TRUE);
      }
      break;
    }

    case WM_CALCFRAMERECT:
      bmp_frame_calc_rect( hwnd, (PRECTL)mp1, SHORT1FROMMP(mp2));
      return MRFROMLONG(TRUE);

    case WM_CALCVALIDRECTS:
      return MRFROMLONG( CVR_ALIGNLEFT | CVR_ALIGNTOP );

    case WM_ADJUSTFRAMEPOS:
    {
      MRESULT rc = WinDefDlgProc( hwnd, msg, mp1, mp2 );
      bmp_frame_adjust( hwnd, (PSWP)mp1 );
      return rc;
    }

    case WM_QUERYFRAMECTLCOUNT:
    {
      USHORT count = (USHORT)(ULONG)WinDefDlgProc( hwnd, msg, mp1, mp2 );
      count = bmp_frame_format( hwnd, data, NULL, count );
      DEBUGLOG2(( "pm123: WM_QUERYFRAMECTLCOUNT for window %08X, rc=%d\n", hwnd, count ));
      return MRFROMSHORT(count);
    }

    case WM_FORMATFRAME:
    {
      USHORT count = (USHORT)(ULONG)WinDefDlgProc( hwnd, msg, mp1, mp2 );
      count = bmp_frame_format( hwnd, data, (PSWP)mp1, count );
      DEBUGLOG2(( "pm123: WM_FORMATFRAME for window %08X, mp1=%08X, mp2=%08X, rc=%d\n", hwnd, mp1, mp2, count ));
      WinInvalidateRect( hwnd, NULL, FALSE );
      return MRFROMSHORT(count);
    }

    case WM_ACTIVATE:
    {
      // The default dialog procedure draws dialog borders during
      // processing of the WM_ACTIVATE message. I try to disable
      // updating of the frame at this time to avoid flickering of
      // the borders.

      if( WinIsWindowVisible( hwnd ))
      {
        MRESULT rc;
        HPS hps;

        WinSetWindowBits( hwnd, QWL_STYLE, 0, WS_VISIBLE );
        rc = WinDefDlgProc( hwnd, msg, mp1, mp2 );
        WinSetWindowBits( hwnd, QWL_STYLE, WS_VISIBLE, WS_VISIBLE );

        hps = WinGetPS( hwnd );
        bmp_frame_paint( hwnd, hps, FALSE );
        WinReleasePS( hwnd );

        if( data->hMinimizeBtn ) {
          WinInvalidateRect( data->hMinimizeBtn, NULL, FALSE );
        }
        if( data->hCloseBtn ) {
          WinInvalidateRect( data->hCloseBtn, NULL, FALSE );
        }

        return rc;
      }
      break;
    }

   case WM_BUTTON1MOTIONSTART:
      WinSendMsg( hwnd, WM_TRACKFRAME, MPFROMSHORT( TF_MOVE | TF_STANDARD ), 0 );
      return 0;

    case WM_SKIN_CHANGED:
    {
      HWND hclient = WinWindowFromID( hwnd, FID_CLIENT );
      char fontname[256];

      bmp_frame_reflow( hwnd, data );

      if( hclient ) {
        if( WinQueryPresParam( hclient, PP_FONTNAMESIZE, 0, NULL, sizeof(fontname), fontname, 0 )) {
          WinSetPresParam( hclient, PP_FONTNAMESIZE, 13, fontname );
        }
      }

      WinInvalidateRect( hwnd, NULL, TRUE );
      return 0;
    }

    case WM_PAINT:
    {
      HPS hps = WinBeginPaint( hwnd, NULLHANDLE, NULL );
      bmp_frame_paint( hwnd, hps, TRUE );
      WinEndPaint( hps );
      return 0;
    }

    case WM_COMMAND:
      switch( SHORT1FROMMP(mp1)) {
        case BTN_SC_HIDE:
          WinSendMsg( hwnd, WM_SYSCOMMAND, MPFROMSHORT( SC_HIDE  ), mp2 );
          return 0;
        case BTN_SC_CLOSE:
          WinSendMsg( hwnd, WM_SYSCOMMAND, MPFROMSHORT( SC_CLOSE ), mp2 );
          return 0;
      }
      break;

    case WM_SYSCOMMAND:
      switch( SHORT1FROMMP(mp1)) {
        case SC_CLOSE:
        case SC_HIDE:
        case SC_MINIMIZE:
          DEBUGLOG(( "received WM_SYSCOMMAND %s for window %08X\n",
                      SHORT1FROMMP(mp1) == SC_CLOSE ? "SC_CLOSE" : "SC_HIDE", hwnd ));

          if( WinQueryActiveWindow( HWND_DESKTOP ) == hwnd &&
              WinIsWindowVisible( hwnd ))
          {
            HWND hnext = WinQueryWindow( hwnd, QW_NEXTTOP );

            if( hnext && WinIsWindowVisible( hnext )) {
              WinSetWindowPos( hnext, 0, 0, 0, 0, 0, SWP_ACTIVATE );
            }
          }

          if( SHORT1FROMMP(mp1) == SC_CLOSE ) {
            WinSendMsg( hwnd, WM_CLOSE, 0, 0 );
          } else {
            WinSetWindowPos( hwnd, HWND_TOP, 0, 0, 0, 0, SWP_HIDE );
          }
          return 0;

        case SC_MOVE:
          WinSendMsg( hwnd, WM_TRACKFRAME,
                      MPFROMSHORT( TF_MOVE | TF_SETPOINTERPOS | TF_STANDARD ), 0 );
          return 0;

        case SC_SIZE:
          if( WinQueryWindowULong( hwnd, QWL_STYLE ) & FS_SIZEBORDER ) {
            WinSendMsg( hwnd, WM_TRACKFRAME,
                        MPFROMSHORT( TF_RIGHT | TF_BOTTOM | TF_SETPOINTERPOS | TF_STANDARD ), 0 );
          }
          return 0;
      }
      break;

    case WM_DESTROY:
      WinSetWindowPtr( hwnd, QWL_USER, 0 );
      free( data->pszWindowText );
      free( data );
      return 0;
  }

  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

