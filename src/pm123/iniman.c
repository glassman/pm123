/*
 * Copyright 1997-2003 Samuel Audet  <guardia@step.polymtl.ca>
 *                     Taneli Lepp�  <rosmo@sektori.com>
 *
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <utilfct.h>
#include <debuglog.h>

#include "pm123.h"
#include "skin.h"
#include "iniman.h"
#include "equalizer.h"
#include "plugman.h"
#include "banner.h"
#include "playlist.h"

void
factory_settings( void )
{
  int i;

  ULONG cp[4], cp_size;
  memset( &cfg, 0, sizeof( cfg ));

  cfg.main.x = 1;
  cfg.main.y = 1;

  cfg.defaultvol     = 38;      // Default maximum volume, of course.
  cfg.mute           = FALSE;   // No audio mute.
  cfg.playonload     = TRUE;    // Play file automatically on load.
  cfg.autouse        = TRUE;    // Auto use playlist on load.
  cfg.font           = 1;       // Make the bolder font a default.
  cfg.buff_fill      = 0;       // Obsolete.
  cfg.buff_preload   = 32;
  cfg.buff_wait      = TRUE;
  cfg.buff_size      = cfg_suggested_buff_size();
  cfg.playonuse      = TRUE;
  cfg.scroll         = CFG_SCROLL_INFINITE;
  cfg.viewmode       = CFG_DISP_ID3TAG;
  cfg.hither_thither = TRUE;
  cfg.double_speed   = FALSE;
  cfg.mode           = CFG_MODE_REGULAR;
  cfg.dock_windows   = TRUE;
  cfg.dock_margin    = 10;
  cfg.add_recursive  = TRUE;
  cfg.save_relative  = TRUE;
  cfg.tags_charset   = CH_DEFAULT;
  cfg.conn_timeout   = 15;
  cfg.sbar_playlist  = 180;
  cfg.sbar_bmarks    = 180;
  cfg.skip_badfiles  = TRUE;
  cfg.eq_preamp      = 1.0;
  cfg.continuous     = TRUE;
  cfg.known_only     = TRUE;
  cfg.bn_pos         = BPOS_UPPER_RIGHT;
  cfg.bn_slide       = BSLIDE_HORIZONTAL;
  cfg.bn_delay       = 10;
  cfg.sort           = PL_SORT_TRCK;
  cfg.upnp           = FALSE;
  cfg.proxy_no_local = TRUE;
  cfg.freshness      = PLUGMAN_AGE;

  snprintf( cfg.lasteq, sizeof( cfg.lasteq  ), "%spresets\\user.eq", startpath );

  for( i = 0; i < EQ_BANDS; i++ ) {
    cfg.eq_gains[i] = 1.0;
  }

  // Selects russian auto-detect as default characters encoding
  // for russian peoples.
  if( DosQueryCp( sizeof( cp ), cp, &cp_size ) == NO_ERROR ) {
    if( cp[0] == 866 ) {
      cfg.tags_charset = CH_CYR_AUTO;
    }
  }

  strcpy( cfg.fontname, "2.System VIO" );
  strcpy( cfg.no_proxy, "localhost,127.0.0.1" );
}

void
load_ini( void )
{
  HINI INIhandle;
  BUFSTREAM *b;

  void* outputs_list;
  void* decoders_list;
  void* filters_list;
  void* visuals_list;
  ULONG size;

  factory_settings();

  if(( INIhandle = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( INIhandle, cfg.defaultvol );
    load_ini_value( INIhandle, cfg.playonload );
    load_ini_value( INIhandle, cfg.selectplayed );
    load_ini_value( INIhandle, cfg.autouse );
    load_ini_value( INIhandle, cfg.continuous );
    load_ini_value( INIhandle, cfg.known_only );
    load_ini_value( INIhandle, cfg.mode );
    load_ini_value( INIhandle, cfg.font );
    load_ini_value( INIhandle, cfg.shf );
    load_ini_value( INIhandle, cfg.rpt );
    load_ini_value( INIhandle, cfg.floatontop );
    load_ini_value( INIhandle, cfg.playonuse );
    load_ini_value( INIhandle, cfg.scroll );
    load_ini_value( INIhandle, cfg.hither_thither );
    load_ini_value( INIhandle, cfg.double_speed );
    load_ini_value( INIhandle, cfg.viewmode );
    load_ini_value( INIhandle, cfg.buff_wait );
    load_ini_value( INIhandle, cfg.buff_size );
    load_ini_value( INIhandle, cfg.buff_fill );
    load_ini_value( INIhandle, cfg.buff_preload );
    load_ini_value( INIhandle, cfg.conn_timeout );
    load_ini_value( INIhandle, cfg.add_recursive );
    load_ini_value( INIhandle, cfg.save_relative );
    load_ini_value( INIhandle, cfg.save_type );
    load_ini_value( INIhandle, cfg.eq_enabled );
    load_ini_value( INIhandle, cfg.eq_gains );
    load_ini_value( INIhandle, cfg.eq_preamp );
    load_ini_value( INIhandle, cfg.show_playlist );
    load_ini_value( INIhandle, cfg.show_bmarks );
    load_ini_value( INIhandle, cfg.show_plman );
    load_ini_value( INIhandle, cfg.dock_margin );
    load_ini_value( INIhandle, cfg.dock_windows );
    load_ini_value( INIhandle, cfg.tags_charset );
    load_ini_value( INIhandle, cfg.font_skinned );
    load_ini_value( INIhandle, cfg.main );
    load_ini_value( INIhandle, cfg.sbar_playlist );
    load_ini_value( INIhandle, cfg.sbar_bmarks );
    load_ini_value( INIhandle, cfg.tags_choice );
    load_ini_value( INIhandle, cfg.skip_badfiles );
    load_ini_value( INIhandle, cfg.freshness );
    load_ini_value( INIhandle, cfg.bn_pos );
    load_ini_value( INIhandle, cfg.bn_slide );
    load_ini_value( INIhandle, cfg.bn_delay );
    load_ini_value( INIhandle, cfg.sort_keep );
    load_ini_value( INIhandle, cfg.sort );
    load_ini_value( INIhandle, cfg.upnp );
    load_ini_value( INIhandle, cfg.proxy_no_local );

    load_ini_string( INIhandle, cfg.filedir );
    load_ini_string( INIhandle, cfg.listdir );
    load_ini_string( INIhandle, cfg.plugdir );
    load_ini_string( INIhandle, cfg.savedir );
    load_ini_string( INIhandle, cfg.cddrive );
    load_ini_string( INIhandle, cfg.proxy );
    load_ini_string( INIhandle, cfg.auth );
    load_ini_string( INIhandle, cfg.no_proxy );
    load_ini_string( INIhandle, cfg.defskin );
    load_ini_string( INIhandle, cfg.lasteq );
    load_ini_string( INIhandle, cfg.fontname );
    load_ini_string( INIhandle, cfg.apicdir );

    load_ini_array_of_strings( INIhandle, cfg.last_file );
    load_ini_array_of_strings( INIhandle, cfg.last_list );
    load_ini_array_of_strings( INIhandle, cfg.last_urls );

    // Used for compatibility.
    sscanf( cfg.proxy, "%511[^:]:%d",    cfg.proxy_host, &cfg.proxy_port );
    sscanf( cfg.auth,  "%127[^:]:%127s", cfg.proxy_user,  cfg.proxy_pass );

    // Also used for compatibility.
    if( cfg.buff_fill ) {
      cfg.buff_preload = cfg.buff_size * cfg.buff_fill / 100.0;
      cfg.buff_fill = 0;
    }

    load_ini_data_size( INIhandle, decoders_list, size );
    if( size > 0 && ( decoders_list = malloc( size )) != NULL )
    {
      load_ini_data( INIhandle, decoders_list, size );
      b = open_bufstream( decoders_list, size );
      if( pg_load_decoders( b )) {
        if( cfg.freshness < 20110513 ) {
          DEBUGLOG(( "pm123: [%d -> %d] add new decoders: oggplay, flacplay, macplay\n", cfg.freshness, PLUGMAN_AGE ));
          pg_load_fresh_decoder( "oggplay.dll"  );
          pg_load_fresh_decoder( "flacplay.dll" );
          pg_load_fresh_decoder( "macplay.dll"  );
        }
        if( cfg.freshness < 20140128 ) {
          DEBUGLOG(( "pm123: [%d -> %d] add new decoders: wvplay\n",  cfg.freshness, PLUGMAN_AGE ));
          pg_load_fresh_decoder( "wvplay.dll"   );
        }
        if( cfg.freshness < 20191002 ) {
          DEBUGLOG(( "pm123: [%d -> %d] add new decoders: dsfplay\n", cfg.freshness, PLUGMAN_AGE ));
          pg_load_fresh_decoder( "dsfplay.dll"  );
        }
        if( cfg.freshness < 20200710 ) {
          DEBUGLOG(( "pm123: [%d -> %d] add new decoders: dffplay\n", cfg.freshness, PLUGMAN_AGE ));
          pg_load_fresh_decoder( "dffplay.dll"  );
        }
        if( cfg.freshness < 20210915 ) {
          DEBUGLOG(( "pm123: [%d -> %d] add new decoders: aacplay\n", cfg.freshness, PLUGMAN_AGE ));
          pg_load_fresh_decoder( "aacplay.dll"  );
        }
      } else {
        pg_load_default_decoders();
      }
      close_bufstream( b );
      free( decoders_list );
    } else {
      pg_load_default_decoders();
    }

    load_ini_data_size( INIhandle, outputs_list, size );
    if( size > 0 && ( outputs_list = malloc( size )) != NULL )
    {
      load_ini_data( INIhandle, outputs_list, size );
      b = open_bufstream( outputs_list, size );
      if( !pg_load_outputs( b )) {
        pg_load_default_outputs();
      }
      close_bufstream( b );
      free( outputs_list );
    } else {
      pg_load_default_outputs();
    }

    load_ini_data_size( INIhandle, filters_list, size );
    if( size > 0 && ( filters_list = malloc( size )) != NULL )
    {
      load_ini_data( INIhandle, filters_list, size );
      b = open_bufstream( filters_list, size );
      if( !pg_load_filters( b )) {
        pg_load_default_filters();
      }
      close_bufstream( b );
      free( filters_list );
    } else {
      pg_load_default_filters();
    }

    load_ini_data_size( INIhandle, visuals_list, size );
    if( size > 0 && ( visuals_list = malloc( size )) != NULL )
    {
      load_ini_data( INIhandle, visuals_list, size );
      b = open_bufstream( visuals_list, size );
      if( !pg_load_visuals( b )) {
        pg_load_default_visuals();
      }
      close_bufstream( b );
      free( visuals_list );
    } else {
      pg_load_default_visuals();
    }

    if( cfg.freshness < 20220726 )
    {
      char  file[_MAX_PATH];
      char* longnames[] = { "pm123_128px.png", "avtransport.xml", "connection.xml", "rendering.xml" };
      int   i;

      DEBUGLOG(( "pm123: [%d -> %d] delete long-name UPnP definitions.\n", cfg.freshness, PLUGMAN_AGE ));

      for( i = 0; i < ARRAY_SIZE( longnames ); i++ ) {
        snprintf( file, sizeof( file ), "%supnp\\%s", startpath, longnames[i] );
        remove( file );
      }
    }

    close_ini( INIhandle );
  }
}

void
save_ini( void )
{
  HINI INIhandle;
  BUFSTREAM *b;

  void* outputs_list;
  void* decoders_list;
  void* filters_list;
  void* visuals_list;
  ULONG size;

  if(( INIhandle = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( INIhandle, cfg.defaultvol );
    save_ini_value( INIhandle, cfg.playonload );
    save_ini_value( INIhandle, cfg.selectplayed );
    save_ini_value( INIhandle, cfg.autouse );
    save_ini_value( INIhandle, cfg.continuous );
    save_ini_value( INIhandle, cfg.known_only );
    save_ini_value( INIhandle, cfg.mode );
    save_ini_value( INIhandle, cfg.font );
    save_ini_value( INIhandle, cfg.shf );
    save_ini_value( INIhandle, cfg.rpt );
    save_ini_value( INIhandle, cfg.floatontop );
    save_ini_value( INIhandle, cfg.playonuse );
    save_ini_value( INIhandle, cfg.scroll );
    save_ini_value( INIhandle, cfg.viewmode );
    save_ini_value( INIhandle, cfg.hither_thither );
    save_ini_value( INIhandle, cfg.double_speed );
    save_ini_value( INIhandle, cfg.buff_wait );
    save_ini_value( INIhandle, cfg.buff_size );
    save_ini_value( INIhandle, cfg.buff_fill );
    save_ini_value( INIhandle, cfg.buff_preload );
    save_ini_value( INIhandle, cfg.conn_timeout );
    save_ini_value( INIhandle, cfg.add_recursive );
    save_ini_value( INIhandle, cfg.save_relative );
    save_ini_value( INIhandle, cfg.save_type );
    save_ini_value( INIhandle, cfg.eq_enabled );
    save_ini_value( INIhandle, cfg.eq_gains );
    save_ini_value( INIhandle, cfg.eq_preamp );
    save_ini_value( INIhandle, cfg.show_playlist );
    save_ini_value( INIhandle, cfg.show_bmarks );
    save_ini_value( INIhandle, cfg.show_plman );
    save_ini_value( INIhandle, cfg.dock_windows );
    save_ini_value( INIhandle, cfg.dock_margin );
    save_ini_value( INIhandle, cfg.tags_charset );
    save_ini_value( INIhandle, cfg.font_skinned );
    save_ini_value( INIhandle, cfg.main );
    save_ini_value( INIhandle, cfg.sbar_playlist );
    save_ini_value( INIhandle, cfg.sbar_bmarks );
    save_ini_value( INIhandle, cfg.tags_choice );
    save_ini_value( INIhandle, cfg.skip_badfiles );
    save_ini_value( INIhandle, cfg.bn_pos );
    save_ini_value( INIhandle, cfg.bn_slide );
    save_ini_value( INIhandle, cfg.bn_delay );
    save_ini_value( INIhandle, cfg.sort_keep );
    save_ini_value( INIhandle, cfg.sort );
    save_ini_value( INIhandle, cfg.upnp );
    save_ini_value( INIhandle, cfg.proxy_no_local );

    // Used for compatibility.
    snprintf( cfg.proxy, sizeof( cfg.proxy ), "%s:%d", cfg.proxy_host, cfg.proxy_port );
    snprintf( cfg.auth,  sizeof( cfg.auth  ), "%s:%s", cfg.proxy_user, cfg.proxy_pass );

    save_ini_string( INIhandle, cfg.filedir );
    save_ini_string( INIhandle, cfg.listdir );
    save_ini_string( INIhandle, cfg.savedir );
    save_ini_string( INIhandle, cfg.plugdir );
    save_ini_string( INIhandle, cfg.cddrive );
    save_ini_string( INIhandle, cfg.proxy );
    save_ini_string( INIhandle, cfg.auth );
    save_ini_string( INIhandle, cfg.no_proxy );
    save_ini_string( INIhandle, cfg.defskin );
    save_ini_string( INIhandle, cfg.lasteq );
    save_ini_string( INIhandle, cfg.fontname );
    save_ini_string( INIhandle, cfg.apicdir );

    save_ini_array_of_strings( INIhandle, cfg.last_file );
    save_ini_array_of_strings( INIhandle, cfg.last_list );
    save_ini_array_of_strings( INIhandle, cfg.last_urls );

    b = create_bufstream( 1024 );
    pg_save_decoders( b );
    size = get_buffer_bufstream( b, &decoders_list );
    save_ini_data( INIhandle, decoders_list, size );
    close_bufstream( b );

    b = create_bufstream( 1024 );
    pg_save_outputs( b );
    size = get_buffer_bufstream( b, &outputs_list );
    save_ini_data( INIhandle, outputs_list, size );
    close_bufstream( b );

    b = create_bufstream( 1024 );
    pg_save_filters( b );
    size = get_buffer_bufstream( b, &filters_list );
    save_ini_data( INIhandle, filters_list, size );
    close_bufstream( b );

    b = create_bufstream( 1024 );
    pg_save_visuals( b );
    size = get_buffer_bufstream( b, &visuals_list );
    save_ini_data( INIhandle, visuals_list, size );
    close_bufstream( b );

    cfg.freshness = PLUGMAN_AGE;
    save_ini_value( INIhandle, cfg.freshness );

    close_ini( INIhandle );
  }
}

/* Copies the specified data from one profile to another. */
static BOOL
copy_ini_data( HINI ini_from, char* app_from, char* key_from,
               HINI ini_to,   char* app_to,   char* key_to )
{
  ULONG size;
  PVOID data;
  BOOL  rc = FALSE;

  if( PrfQueryProfileSize( ini_from, app_from, key_from, &size )) {
    data = malloc( size );
    if( data ) {
      if( PrfQueryProfileData( ini_from, app_from, key_from, data, &size )) {
        if( PrfWriteProfileData( ini_to, app_to, key_to, data, size )) {
          rc = TRUE;
        }
      }
      free( data );
    }
  }

  return rc;
}

/* Moves the specified data from one profile to another. */
static BOOL
move_ini_data( HINI ini_from, char* app_from, char* key_from,
               HINI ini_to,   char* app_to,   char* key_to )
{
  if( copy_ini_data( ini_from, app_from, key_from, ini_to, app_to, key_to )) {
    return PrfWriteProfileData( ini_from, app_from, key_from, NULL, 0 );
  } else {
    return FALSE;
  }
}

/* Saves the current size and position of the window specified by hwnd.
   This function will also save the presentation parameters. */
BOOL
save_window_pos( HWND hwnd, int options )
{
  char   key1st[32];
  char   key2st[32];
  char   key3st[32];
  PPIB   ppib;
  SHORT  id   = WinQueryWindowUShort( hwnd, QWS_ID );
  HINI   hini = open_module_ini();
  BOOL   rc   = FALSE;
  SWP    swp;
  POINTL pos[2];

  DosGetInfoBlocks( NULL, &ppib );

  if( hini != NULLHANDLE ) {
    sprintf( key1st, "WIN_%08X_%08lX", id, ppib->pib_ulpid );
    sprintf( key2st, "WIN_%08X", id );
    sprintf( key3st, "POS_%08X", id );

    if( WinStoreWindowPos( "PM123", key1st, hwnd )) {
      if( move_ini_data( HINI_PROFILE, "PM123", key1st, hini, "Positions", key2st )) {
        rc = TRUE;
      }
    }
    if( rc && options & WIN_MAP_POINTS ) {
      if( WinQueryWindowPos( hwnd, &swp )) {
        pos[0].x = swp.x;
        pos[0].y = swp.y;
        pos[1].x = swp.x + swp.cx;
        pos[1].y = swp.y + swp.cy;

        WinMapDlgPoints( hwnd, pos, 2, FALSE );
        rc = PrfWriteProfileData( hini, "Positions", key3st, &pos, sizeof( pos ));
      }
    }
    close_ini( hini );
  }
  return rc;
}

/* Restores the size and position of the window specified by hwnd to
   the state it was in when save_window_pos was last called.
   This function will also restore presentation parameters. */
BOOL
rest_window_pos( HWND hwnd, int options )
{
  char   key1st[32];
  char   key2st[32];
  char   key3st[32];
  PPIB   ppib;
  SHORT  id    = WinQueryWindowUShort( hwnd, QWS_ID );
  HINI   hini  = open_module_ini();
  BOOL   rc    = FALSE;
  BOOL   fixed = FALSE;
  POINTL pos[2];
  SWP    swp;
  SWP    desktop;
  ULONG  len  = sizeof(pos);

  DosGetInfoBlocks( NULL, &ppib );

  if( hini != NULLHANDLE ) {
    sprintf( key1st, "WIN_%08X_%08lX", id, ppib->pib_ulpid );
    sprintf( key2st, "WIN_%08X", id );
    sprintf( key3st, "POS_%08X", id );

    if(!((ULONG)WinSendMsg( hwnd, WM_QUERYFRAMEINFO, 0, 0 ) & FI_FRAME ) ||
       !( WinQueryWindowULong( hwnd, QWL_STYLE ) & FS_SIZEBORDER ))
    {
      fixed = TRUE;
      WinQueryWindowPos( hwnd, &swp );
    }

    if( copy_ini_data( hini, "Positions", key2st, HINI_PROFILE, "PM123", key1st )) {
      rc = WinRestoreWindowPos( "PM123", key1st, hwnd );
      PrfWriteProfileData( HINI_PROFILE, "PM123", key1st, NULL, 0 );
    }

    if( rc && options & WIN_MAP_POINTS ) {
      if( PrfQueryProfileData( hini, "Positions", key3st, &pos, &len ))
      {
        WinMapDlgPoints( hwnd, pos, 2, TRUE );
        WinSetWindowPos( hwnd, 0, pos[0].x, pos[0].y,
                         pos[1].x-pos[0].x, pos[1].y-pos[0].y, SWP_MOVE | SWP_SIZE );
      } else {
        rc = FALSE;
      }
    }

    if( fixed ) {
      WinSetWindowPos( hwnd, 0, 0, 0, swp.cx, swp.cy, SWP_SIZE );
    }

    if( rc && WinQueryWindowPos( hwnd, &swp )
           && WinQueryWindowPos( HWND_DESKTOP, &desktop ))
    {
      if( swp.y + swp.cy > desktop.cy )
      {
        swp.y = desktop.cy - swp.cy;
        WinSetWindowPos( hwnd, 0, swp.x, swp.y, 0, 0, SWP_MOVE );
      }
    }

    close_ini( hini );
  }
  return rc;
}

/* Saves skin presentation parameters. */
void
save_skin_data( SKIN* skin )
{
  HINI hini = open_module_ini();
  char key[_MAX_PATH] = "Skin.";

  strlcat( key, skin->name, sizeof( key ));
  DEBUGLOG(( "pm123: recommended skin `%s' font is `%s'\n", skin->name, skin->fontname ));

  if( nlstricmp( cfg.fontname, skin->fontname ) == 0 ) {
    PrfWriteProfileString( hini, key, "skin.fontname", NULL );
    DEBUGLOG(( "pm123: cleanup skin `%s' font\n", skin->name ));
  } else {
    PrfWriteProfileString( hini, key, "skin.fontname", cfg.fontname );
    DEBUGLOG(( "pm123: save skin `%s' font as `%s'\n", skin->name, cfg.fontname ));
  }

  close_ini( hini );
}

/* Loads skin presentation parameters. */
void
load_skin_data( SKIN* skin )
{
  HINI hini = open_module_ini();
  char key[_MAX_PATH] = "Skin.";

  strlcat( key, skin->name, sizeof( key ));
  strlcpy( cfg.fontname, skin->fontname, sizeof( cfg.fontname ));

  PrfQueryProfileString( hini, key, "skin.fontname", NULL,
                         cfg.fontname, sizeof( cfg.fontname ));

  DEBUGLOG(( "pm123: recommended skin `%s' font is `%s'\n", skin->name, skin->fontname ));
  DEBUGLOG(( "pm123: used skin `%s' font is `%s'\n", skin->name, cfg.fontname ));

  close_ini( hini );
}
