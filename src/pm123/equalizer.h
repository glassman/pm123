/*
 * Copyright 2011-2019 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Based on "Simple implementation of Biquad filters" of Tom St Denis and
 * on "Cookbook formulae for audio EQ biquad filter coefficients" of
 * Robert Bristow-Johnson
 *
 * http://www.musicdsp.org/files/Audio-EQ-Cookbook.txt
 */

#ifndef PM123_EQUALIZER_H
#define PM123_EQUALIZER_H

#ifndef  RC_INVOKED
#include <format.h>
#endif

#define DLG_EQUALIZER 2015
#define SL_EQ00        200
#define SL_EQ01        201
#define SL_EQ02        202
#define SL_EQ03        203
#define SL_EQ04        204
#define SL_EQ05        205
#define SL_EQ06        206
#define SL_EQ07        207
#define SL_EQ08        208
#define SL_EQ09        209
#define SL_EQ10        210
#define ST_EQ01        211
#define ST_EQ02        212
#define ST_EQ03        213
#define ST_EQ04        214
#define ST_EQ05        215
#define ST_EQ06        216
#define ST_EQ07        217
#define ST_EQ08        218
#define ST_EQ09        219
#define ST_EQ10        220
#define SL_PREAMP      SL_EQ00
#define CB_EQ_ENABLED  221
#define PB_EQ_DEFAULT  222
#define PB_EQ_LOAD     223
#define PB_EQ_SAVE     224
#define ST_BOTTOM      236
#define ST_TOP         237
#define ST_MIDDLE      238
#define ST_PREAMP      239
#define ST_DB01        240
#define ST_DB02        241
#define ST_DB03        242
#define ST_DB04        243
#define ST_DB05        244
#define ST_DB06        245
#define ST_DB07        246
#define ST_DB08        247
#define ST_DB09        248
#define ST_DB10        249
#define ST_DBPREAMP    250
#define ST_EQ_WARNING  251

#ifndef M_LN2
#define M_LN2 0.69314718055994530942
#endif
#ifndef M_PI
#define M_PI  3.14159265358979323846
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define EQ_BANDS 10

/* Whatever sample type you want. */
typedef double smp_type;
/* This holds the data required to update samples thru a filter. */
typedef struct {
  smp_type a0, a1, a2, a3, a4;
  smp_type x1, x2, y1, y2;
  float    gain;
  int      skip;
} biquad;

int DLLENTRY equalize_samples( FORMAT_INFO* format, char* buf, int len );

/* Sets the visibility state of the equalizer presentation window. */
void eq_show( BOOL show );
/* Destroys the equalizer presentation window. */
void eq_destroy( void );
/* Returns the visibility state of the equalizer presentation window. */
BOOL eq_is_visible( void );
/* Changes the equalizer colors. */
BOOL eq_set_colors( ULONG, ULONG, ULONG, ULONG, ULONG, ULONG );

#ifdef __cplusplus
}
#endif
#endif /* PM123_EQUALIZER_H */
