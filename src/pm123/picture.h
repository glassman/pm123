/*
 * Copyright 2014-2017 Dmitry A.Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_PICTURE_H
#define PM123_PICTURE_H

#ifdef __cplusplus
extern "C" {
#endif

#define WC_APIC         "PM123APic"
#define IM_DEFAULT_ART  777

#define WM_PIC_SETPICS        (WM_USER+1)
#define WM_PIC_GETPICS        (WM_USER+2)
#define WM_PIC_GETPIC         (WM_USER+3)
#define WM_PIC_COUNT          (WM_USER+4)
#define WM_PIC_SELECT         (WM_USER+5)
#define WM_PIC_SELECTNEXT     (WM_USER+6)
#define WM_PIC_SELECTPREV     (WM_USER+7)
#define WM_PIC_SELECTED       (WM_USER+8)

void InitPicture( HAB hab );

#ifdef __cplusplus
}
#endif
#endif
