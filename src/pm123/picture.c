/*
 * Copyright 2014-2017 Dmitry A.Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_PM
#define  INCL_BASE
#include <os2.h>
#include <debuglog.h>
#include <decoder_plug.h>

#include "pm123.h"
#include "picture.h"
#include "bitmap.h"

#define  QWP_APICS      0
#define  QWL_COUNT      4
#define  QWL_SELECTED   8
#define  QWL_HBITMAP   12

/* Prepares a current selected attached picture to painting. */
static void
pic_prepare( HWND hwnd )
{
  APIC**  pics  = WinQueryWindowPtr  ( hwnd, QWP_APICS    );
  ULONG   count = WinQueryWindowULong( hwnd, QWL_COUNT    );
  HBITMAP hbmp  = WinQueryWindowULong( hwnd, QWL_HBITMAP  );
  ULONG   i     = WinQueryWindowULong( hwnd, QWL_SELECTED );
  RECTL   rect;

  if( hbmp ) {
    GpiDeleteBitmap( hbmp );
    hbmp = NULLHANDLE;
  }

  WinQueryWindowRect( hwnd, &rect );

  if( pics && i < count )
  {
    APIC* p = pics[i];
      hbmp = bmp_make_scaled_bitmap( p->mimetype, (char*)p + sizeof(APIC),
                                     p->size - sizeof(APIC), rect.xRight, rect.yTop );
  }
  else
  {
    PVOID picdata;
    ULONG picsize;

    if( DosQueryResourceSize( hmodule, RT_PNG, IM_DEFAULT_ART, &picsize  ) == NO_ERROR ) {
      if( DosGetResource( hmodule, RT_PNG, IM_DEFAULT_ART, &picdata ) == NO_ERROR ) {
        hbmp = bmp_make_scaled_bitmap( "image/png", picdata, picsize, rect.xRight, rect.yTop );
      }
      DosFreeResource( picdata );
    }
  }

  WinSetWindowULong( hwnd, QWL_HBITMAP, hbmp );
  WinInvalidateRect( hwnd, NULL, TRUE );
}

/* Repaints window. */
void
pic_paint( HWND hwnd )
{
  HPS     hps  = WinBeginPaint( hwnd, NULLHANDLE, NULL );
  HBITMAP hbmp = WinQueryWindowULong( hwnd, QWL_HBITMAP );
  RECTL   rect;
  ULONG   bg;
  POINTL  pos[3];

  BITMAPINFOHEADER2 bmpinfo = { sizeof( bmpinfo )};

  GpiCreateLogColorTable( hps, 0, LCOLF_RGB, 0, 0, 0 );
  WinQueryWindowRect( hwnd, &rect );

  if( !WinQueryPresParam( hwnd, PP_BACKGROUNDCOLOR, PP_BACKGROUNDCOLOR, NULL,
                          sizeof( ULONG ), &bg, QPF_PURERGBCOLOR ))
  {
    bg = SYSCLR_DIALOGBACKGROUND;
  }

  WinFillRect( hps, &rect, bg );
  GpiQueryBitmapInfoHeader( hbmp, &bmpinfo );

  pos[0].x = ( rect.xRight - bmpinfo.cx ) / 2;
  pos[0].y = ( rect.yTop   - bmpinfo.cy ) / 2;
  pos[1].x = pos[0].x + bmpinfo.cx - 1;
  pos[1].y = pos[0].y + bmpinfo.cy - 1;
  pos[2].x = 0;
  pos[2].y = 0;

  GpiWCBitBlt( hps, hbmp, 3, pos, ROP_SRCCOPY, BBO_OR );
  WinEndPaint( hps );
}

/* Processes messages of the WC_PM123_PICTURE class window. */
static MRESULT EXPENTRY
pic_window_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_CREATE:
      pic_prepare( hwnd );
      break;

    case WM_PIC_SETPICS:
      WinSetWindowPtr( hwnd, QWP_APICS, mp1 );
      WinSetWindowULong( hwnd, QWL_COUNT, LONGFROMMP( mp2 ));
      WinSetWindowULong( hwnd, QWL_SELECTED, 0 );
      pic_prepare( hwnd );
      return 0;

    case WM_PIC_GETPICS:
      return MRFROMLONG( WinQueryWindowPtr( hwnd, QWP_APICS ));

    case WM_PIC_COUNT:
      return MRFROMLONG( WinQueryWindowULong( hwnd, QWL_COUNT ));

    case WM_PIC_SELECT:
      if( LONGFROMMP( mp1 ) < WinQueryWindowULong( hwnd, QWL_COUNT )) {
        WinSetWindowULong( hwnd, QWL_SELECTED, LONGFROMMP( mp1 ));
        pic_prepare( hwnd );
      }
      return 0;

    case WM_PIC_SELECTNEXT:
    {
      ULONG count = WinQueryWindowULong( hwnd, QWL_COUNT );
      ULONG i = WinQueryWindowULong( hwnd, QWL_SELECTED ) + 1;

      if( i < count ) {
        WinSetWindowULong( hwnd, QWL_SELECTED, i );
        pic_prepare( hwnd );
      }
      return 0;
    }

    case WM_PIC_SELECTPREV:
    {
      ULONG i = WinQueryWindowULong( hwnd, QWL_SELECTED );

      if( i > 0 ) {
        WinSetWindowULong( hwnd, QWL_SELECTED, i - 1 );
        pic_prepare( hwnd );
      }
      return 0;
    }

    case WM_PIC_SELECTED:
      return MRFROMLONG( WinQueryWindowULong( hwnd, QWL_SELECTED ));

    case WM_PIC_GETPIC:
    {
      APIC** pics  = WinQueryWindowPtr( hwnd, QWP_APICS );
      ULONG  count = WinQueryWindowULong( hwnd, QWL_COUNT );
      ULONG  i     = LONGFROMMP(mp1);

      if( pics && i < count ) {
        return pics[i];
      }

      return NULL;
    }

    case WM_CTLCOLORCHANGE:
    case WM_PRESPARAMCHANGED:
      WinInvalidateRect( hwnd, NULL, TRUE );
      break;

    case WM_PAINT:
      pic_paint( hwnd );
      return 0;

    case WM_SIZE:
      pic_prepare( hwnd );
      return 0;
  }
  return WinDefWindowProc( hwnd, msg, mp1, mp2 );
}

/* Initializes the attached picture window class. Must be called
   from the main thread. */
void
InitPicture( HAB hab ) {
  WinRegisterClass( 0, WC_APIC, pic_window_proc, CS_CLIPCHILDREN, 16 );
}



