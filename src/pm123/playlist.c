/*
 * Copyright 1997-2003 Samuel Audet  <guardia@step.polymtl.ca>
 *                     Taneli Lepp�  <rosmo@sektori.com>
 *
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_WIN
#define  INCL_GPI
#define  INCL_ERRORS
#define  INCL_LONGLONG
#include <os2.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <io.h>
#include <process.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <utilfct.h>
#include <hashtable.h>
#include <debuglog.h>

#include "playlist.h"
#include "pm123.h"
#include "docking.h"
#include "pfreq.h"
#include "assertions.h"
#include "tags.h"
#include "filedlg.h"
#include "hotkeys.h"
#include "messages.h"
#include "skin.h"
#include "iniman.h"
#include "upnp.h"

#define PL_ADD_FILE         0
#define PL_ADD_DIRECTORY    1
#define PL_COMPLETED        2
#define PL_SELECT_BY_INDEX  3
#define PL_RELOAD_BY_INDEX  4
#define PL_LOAD_PLAYLIST    5
#define PL_CLEAR            6
#define PL_TERMINATE        7

/* Structure that contains information for records in
   the playlist container control. */

typedef struct _PLRECORD {

  RECORDCORE   rc;
  char*        songname;    /* Name of the song.              */
  char*        full;        /* Full path and file name.       */
  char*        size;        /* Size of the file.              */
  char*        time;        /* Play time of the file.         */
  char*        moreinfo;    /* Information about the song.    */
  int          played;      /* Is it already played file.     */
  BOOL         playable;    /* Is this file playable.         */
  BOOL         playing;     /* Is it currently playing file.  */
  DECODER_INFO info;        /* File info returned by decoder. */
  char         decoder[_MAX_MODULE_NAME];

} PLRECORD, *PPLRECORD;

typedef struct _PLDATA {

  char* filename;
  char* songname;
  int   index;
  int   options;

} PLDATA;

typedef struct _PLDROPINFO {

  int    index;     /* Index of inserted record. */
  HWND   hwndItem;  /* Window handle of the source of the drag operation. */
  ULONG  ulItemID;  /* Information used by the source to identify the
                       object being dragged. */
} PLDROPINFO;

#define INDEX_FIRST  0
#define INDEX_END   -1
#define INDEX_NONE  -2

static HWND   menu_playlist  = NULLHANDLE;
static HWND   menu_record    = NULLHANDLE;
static HWND   playlist       = NULLHANDLE;
static HWND   container      = NULLHANDLE;
static BOOL   pl_busy        = FALSE;
static int    played         = 0;
static TID    broker_tid     = 0;
static PQUEUE broker_queue   = NULL;

/* Contains a name of the currently loaded playlist. */
char current_playlist[_MAX_URL];

/* To keep a random sort order, it is necessary that the result of
   comparing the two records be random and repeatable. I use
   randbase for this. */

static int randbase = 0;

#define PL_MSG_MARK_AS          ( WM_USER + 1000 )
#define PL_MSG_PLAY_LEFT        ( WM_USER + 1001 )
#define PL_MSG_LOAD_FRST_RECORD ( WM_USER + 1002 )
#define PL_MSG_LOAD_NEXT_RECORD ( WM_USER + 1003 )
#define PL_MSG_LOAD_PREV_RECORD ( WM_USER + 1004 )
#define PL_MSG_LOAD_FILE_RECORD ( WM_USER + 1005 )
#define PL_MSG_REMOVE_RECORD    ( WM_USER + 1006 )
#define PL_MSG_REMOVE_ALL       ( WM_USER + 1007 )
#define PL_MSG_INSERT_RECORD    ( WM_USER + 1008 )
#define PL_MSG_LOADED_INDEX     ( WM_USER + 1009 )
#define PL_MSG_SELECT_BY_INDEX  ( WM_USER + 1010 )
#define PL_MSG_LOAD_BY_INDEX    ( WM_USER + 1011 )
#define PL_MSG_INSERT_RECALL    ( WM_USER + 1012 )
#define PL_MSG_REFRESH_FILE     ( WM_USER + 1013 )
#define PL_MSG_REFRESH_STATUS   ( WM_USER + 1014 )
#define PL_MSG_CLEAN_SHUFFLE    ( WM_USER + 1015 )
#define PL_MSG_SAVE_PLAYLIST    ( WM_USER + 1016 )
#define PL_MSG_SAVE_BUNDLE      ( WM_USER + 1017 )
#define PL_MSG_REFRESH_SONGNAME ( WM_USER + 1018 )
#define PL_MSG_PLAY_TIME        ( WM_USER + 1019 )
#define PL_MSG_PLAYED           ( WM_USER + 1020 )

#define MARK_AS_STOP  0
#define MARK_AS_PLAY  1
#define MARK_AS_GREY  2

static BOOL pl_load_any_list( const char* filename, int options, HPOPULATE hp );
static BOOL pl_m_save_list  ( const char* filename, int options );
static BOOL pl_m_save_bundle( const char* filename, int options );
static void pl_m_insert_to_recall_list( const char* filename );

/* The pointer to playlist record of the currently loaded file,
   the pointer is NULL if such record is not present. */
static PLRECORD* loaded_record;
/* The pointer to playlist record of the currently played file,
   the pointer is NULL if such record is not present. */
static PLRECORD* played_record;

/* WARNING!!! All functions returning a pointer to the
   playlist record, return a NULL if suitable record is not found. */

static INLINE PLRECORD*
PL_RC( MRESULT rc )
{
  if((PLRECORD*)rc != (PLRECORD*)-1 ) {
    return (PLRECORD*)rc;
  } else {
    return NULL;
  }
}

/* Returns the pointer to the first playlist record. */
static PLRECORD*
pl_m_first_record( void )
{
  ASSERT_IS_MAIN_THREAD;
  return PL_RC( WinSendMsg( container, CM_QUERYRECORD, NULL,
                            MPFROM2SHORT( CMA_FIRST, CMA_ITEMORDER )));
}

/* Returns the pointer to the next playlist record of specified. */
static PLRECORD*
pl_m_next_record( PLRECORD* rec )
{
  ASSERT_IS_MAIN_THREAD;
  return PL_RC( WinSendMsg( container, CM_QUERYRECORD, MPFROMP(rec),
                            MPFROM2SHORT( CMA_NEXT, CMA_ITEMORDER )));
}

/* Returns the pointer to the previous playlist record of specified. */
static PLRECORD*
pl_m_prev_record( PLRECORD* rec )
{
  ASSERT_IS_MAIN_THREAD;
  return PL_RC( WinSendMsg( container, CM_QUERYRECORD, MPFROMP(rec),
                            MPFROM2SHORT( CMA_PREV, CMA_ITEMORDER )));
}

/* Returns the pointer to the first selected playlist record. */
static PLRECORD*
pl_m_first_selected( void )
{
  ASSERT_IS_MAIN_THREAD;
  return PL_RC( WinSendMsg( container, CM_QUERYRECORDEMPHASIS,
                            MPFROMP( CMA_FIRST ), MPFROMSHORT( CRA_SELECTED )));
}

/* Returns the pointer to the next selected playlist record of specified. */
static PLRECORD*
pl_m_next_selected( PLRECORD* rec )
{
  ASSERT_IS_MAIN_THREAD;
  return PL_RC( WinSendMsg( container, CM_QUERYRECORDEMPHASIS,
                            MPFROMP( rec ), MPFROMSHORT( CRA_SELECTED )));
}

/* Returns the pointer to the cursored playlist record. */
static PLRECORD*
pl_m_cursored( void )
{
  ASSERT_IS_MAIN_THREAD;
  return PL_RC( WinSendMsg( container, CM_QUERYRECORDEMPHASIS,
                            MPFROMP( CMA_FIRST ), MPFROMSHORT( CRA_CURSORED )));
}

/* Selects the specified record and deselects all others. */
static void
pl_m_select( PLRECORD* select )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  if( select ) {
    for( rec = pl_m_first_selected(); rec; rec = pl_m_next_selected( rec )) {
      WinSendMsg( container, CM_SETRECORDEMPHASIS,
                  MPFROMP( rec ), MPFROM2SHORT( FALSE, CRA_SELECTED ));
    }
    WinSendMsg( container, CM_SETRECORDEMPHASIS,
                MPFROMP( select ), MPFROM2SHORT( TRUE , CRA_SELECTED | CRA_CURSORED ));
  }
}

/* Queries a random playlist record for playing. */
static PLRECORD*
pl_m_query_random_record( void )
{
  PLRECORD* rec;
  int pos = 0;

  ASSERT_IS_MAIN_THREAD;

  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
    if( !rec->played && rec->playable ) {
      ++pos;
    }
  }

  if( pos )
  {
    pos = rand() % pos;
    for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
      if( !rec->played && rec->playable && !pos-- ) {
        break;
      }
    }
  }
  return rec;
}

/* Queries a first playlist record for playing. */
static PLRECORD*
pl_m_query_first_record( void )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  if( cfg.shf ) {
    return pl_m_query_random_record();
  } else {
    for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
      if( rec->playable ) {
        break;
      }
    }
    return rec;
  }
}

/* Queries a next playlist record for playing. */
static PLRECORD*
pl_m_query_next_record( void )
{
  PLRECORD* found = NULL;
  ASSERT_IS_MAIN_THREAD;

  if( loaded_record ) {
    if( cfg.shf ) {
      if( loaded_record->played == played ) {
        found =  pl_m_query_random_record();
      } else {
        PLRECORD* rec = pl_m_first_record();

        while( rec ) {
          // Search for record which it was played before current record.
          if( rec->played > loaded_record->played ) {
            if( !found || rec->played < found->played ) {
              found = rec;
              // Make search little bit faster.
              if( found->played - loaded_record->played == 1 ) {
                break;
              }
            }
          }
          rec = pl_m_next_record( rec );
        }
        if( !found ) {
          found = pl_m_query_random_record();
        }
      }
    } else {
      found = pl_m_next_record( loaded_record );
      while( found && !found->playable ) {
        found = pl_m_next_record( found );
      }
    }
  }

  return found;
}

/* Queries a previous playlist record for playing. */
static PLRECORD*
pl_m_query_prev_record( void )
{
  PLRECORD* found = NULL;
  ASSERT_IS_MAIN_THREAD;

  if( loaded_record ) {
    if( cfg.shf ) {
      PLRECORD* rec = pl_m_first_record();
      while( rec ) {
        // Search for record which it was played before current record.
        if( rec->played && rec->played < loaded_record->played ) {
          if( !found || rec->played > found->played ) {
            found = rec;
            // Make search little bit faster.
            if( loaded_record->played - found->played == 1 ) {
              break;
            }
          }
        }
        rec = pl_m_next_record( rec );
      }
    } else {
      found = pl_m_prev_record( loaded_record );
      while( found && !found->playable ) {
        found = pl_m_prev_record( found );
      }
    }
  }

  return found;
}

/* Queries a playlist record of the specified file for playing. */
static PLRECORD*
pl_m_query_file_record( const char* filename )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  if( loaded_record ) {
    if( strcmp( loaded_record->full, filename ) == 0 ) {
      return loaded_record;
    }
  }

  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
    if( strcmp( rec->full, filename ) == 0 ) {
      break;
    }
  }

  return rec;
}

/* Invalidates the specified playlist record. */
static void
pl_m_invalidate_record( PLRECORD* rec, USHORT flags )
{
  WinSendMsg( container, CM_INVALIDATERECORD,
              MPFROMP( &rec ), MPFROM2SHORT( 1, flags ));
}

/* Fills record by the information provided by the decoder. */
static void
pl_m_fill_record( PLRECORD* rec, const DECODER_INFO* info )
{
  char buffer[64];

  free( rec->songname );
  free( rec->moreinfo );
  free( rec->size );
  free( rec->time );

  if( info ) {
    rec->info = *info;
    rec->info.pics = NULL;
  } else {
    memset( &rec->info, 0, sizeof( rec->info ));
    rec->info.size = sizeof( rec->info );
  }

  rec->size = strdup( readable_file_size( rec->info.filesize, buffer, sizeof( buffer )));
  sprintf( buffer, "%02u:%02u", rec->info.songlength / 60000, rec->info.songlength / 1000 % 60 );
  rec->time = strdup( buffer );

  // Songname
  if( *rec->info.title )
  {
    rec->songname = malloc( strlen( rec->info.artist ) + 3 +
                            strlen( rec->info.title  ) + 1 );
    if( rec->songname ) {
      strcpy( rec->songname, rec->info.artist );
      if( *rec->info.title && *rec->info.artist ) {
        strcat( rec->songname, " - " );
      }
      strcat( rec->songname, rec->info.title );
    }
  // If the song name is undefined then use the file name instead.
  } else {
    char filename[_MAX_URL];
    amp_title_from_filename( filename, rec->rc.pszIcon, sizeof( filename ));

    rec->songname = malloc( strlen( rec->info.artist ) + 3 +
                            strlen( filename         ) + 1 );
    if( rec->songname ) {
      strcpy( rec->songname, rec->info.artist );
      if( *rec->info.artist && *filename ) {
        strcat( rec->songname, " - " );
      }
      strcat( rec->songname, filename );
    }
  }

  // Information
  rec->moreinfo = malloc( strlen( rec->info.album       ) + 1 +
                          strlen( rec->info.year        ) + 2 +
                          strlen( rec->info.genre       ) + 2 +
                          strlen( rec->info.track       ) + 4 +
                          strlen( rec->info.tech_info   ) +
                          strlen( rec->info.comment     ) + 12 );
  if( rec->moreinfo ) {
    strcpy( rec->moreinfo, rec->info.album );

    if( *rec->info.album ) {
      if( *rec->info.year  ) {
        strcat( rec->moreinfo, " "   );
      } else {
        strcat( rec->moreinfo, ", "  );
      }
    }
    if( *rec->info.year  ) {
      strcat( rec->moreinfo, rec->info.year );
      strcat( rec->moreinfo, ", " );
    }
    if( *rec->info.genre ) {
      strcat( rec->moreinfo, rec->info.genre );
      strcat( rec->moreinfo, ", " );
    }
    if( *rec->info.track ) {
      sprintf( rec->moreinfo + strlen( rec->moreinfo ), "#%02ld, ", atol( rec->info.track ));
    }

    strcat( rec->moreinfo, rec->info.tech_info );

    if( *rec->info.comment ) {
      strcat( rec->moreinfo, ", comment: " );
      strcat( rec->moreinfo, rec->info.comment );
    }
  }

  control_strip( rec->songname );
  control_strip( rec->moreinfo );
}

/* Returns an icon suitable to the specified record. */
static HPOINTER
pl_m_proper_icon( PLRECORD* rec )
{
  if( rec->playing ) {
    return mp3play;
  } else if( !rec->playable ) {
    return mp3gray;
  } else {
    return mp3;
  }
}

/* Prepares the specified playlist record to play. */
static BOOL
pl_m_load_record( PLRECORD* rec )
{
  ASSERT_IS_MAIN_THREAD;

  if( rec ) {
    strlcpy( current_filename, rec->full,    sizeof( current_filename ));
    strlcpy( current_decoder,  rec->decoder, sizeof( current_decoder  ));

    dec_copyinfo( &current_info, &rec->info );
    loaded_record = rec;

    amp_invalidate( UPD_FILENAME | UPD_FILEINFO | UPD_TIMERS );
    upnp_notify_state_change( UPNP_N_FILELOADED | UPNP_N_TRACKS | UPNP_N_TIMERS );
    return TRUE;
  }

  return FALSE;
}

static SHORT EXPENTRY
pl_m_compare_rand( PLRECORD* p1, PLRECORD* p2, PVOID pStorage )
{
  // To keep a random sort order, it is necessary that the result of
  // comparing the two records be random and repeatable. I use
  // randbase for this.
  return (int)p1 % randbase - (int)p2 % randbase;
}

static SHORT EXPENTRY
pl_m_compare_size( PLRECORD* p1, PLRECORD* p2, PVOID pStorage )
{
  if( p1->info.filesize < p2->info.filesize ) return -1;
  if( p1->info.filesize > p2->info.filesize ) return  1;
  return 0;
}

static SHORT EXPENTRY
pl_m_compare_time( PLRECORD* p1, PLRECORD* p2, PVOID pStorage )
{
  if( p1->info.songlength < p2->info.songlength ) return -1;
  if( p1->info.songlength > p2->info.songlength ) return  1;
  return 0;
}

static SHORT EXPENTRY
pl_m_compare_name( PLRECORD* p1, PLRECORD* p2, PVOID pStorage )
{
  char s1[_MAX_URL], s2[_MAX_URL];

  sfname( s1, p1->full, sizeof( s1 ));
  sfname( s2, p2->full, sizeof( s2 ));

  return nlstricmp( s1, s2 );
}

static SHORT EXPENTRY
pl_m_compare_song( PLRECORD* p1, PLRECORD* p2, PVOID pStorage ) {
  return nlstricmp( p1->songname, p2->songname );
}

static SHORT EXPENTRY
pl_m_compare_trck( PLRECORD* p1, PLRECORD* p2, PVOID pStorage )
{
  SHORT rc;

  if(( rc = nlstricmp( p1->info.artist, p2->info.artist )) == 0 ) {
    if(( rc = nlstricmp( p1->info.album, p2->info.album )) == 0 ) {
      return atol( p1->info.track ) - atol( p2->info.track );
    }
  }
  return rc;
}

/* Sorts the playlist records. */
static void
pl_m_sort( int sort )
{
  PVOID   pSortRecord;
  CNRINFO info = { sizeof( info )};

  ASSERT_IS_MAIN_THREAD;

  switch( sort ) {
    case PL_SORT_RAND:
      pSortRecord = (PVOID)pl_m_compare_rand;
      randbase = rand() % 32767;
      break;
    case PL_SORT_SIZE:
      pSortRecord = (PVOID)pl_m_compare_size;
      break;
    case PL_SORT_TIME:
      pSortRecord = (PVOID)pl_m_compare_time;
      break;
    case PL_SORT_FILE:
      pSortRecord = (PVOID)pl_m_compare_name;
      break;
    case PL_SORT_TRCK:
      pSortRecord = (PVOID)pl_m_compare_trck;
      break;
    case PL_SORT_SONG:
      pSortRecord = (PVOID)pl_m_compare_song;
      break;
    default:
      pSortRecord = NULL;
      break;
  }

  if( cfg.sort_keep ) {
    info.pSortRecord = pSortRecord;
    WinSendMsg( container, CM_SETCNRINFO, MPFROMP( &info ), MPFROMLONG( CMA_PSORTRECORD ));
  } else {
    info.pSortRecord = NULL;
    WinSendMsg( container, CM_SETCNRINFO, MPFROMP( &info ), MPFROMLONG( CMA_PSORTRECORD ));

    if( pSortRecord ) {
      WinSendMsg( container, CM_SORTRECORD, MPFROMP( pSortRecord ), 0 );
    }
  }

  amp_invalidate( UPD_WINDOW );
}

/* Marks the currently loaded playlist record as currently played,
   currently stopped or as non playable. */
static void
pl_m_mark_as( ULONG as )
{
  ASSERT_IS_MAIN_THREAD;

  if( as == MARK_AS_PLAY ) {
    if( played_record ) {
      if( played_record != loaded_record ) {
        pl_m_mark_as( MARK_AS_STOP );
      }
    }
    if( loaded_record ) {
      played_record = loaded_record;
      loaded_record->playing  = TRUE;
      loaded_record->playable = TRUE;
      loaded_record->rc.hptrIcon = pl_m_proper_icon( loaded_record );

      if( cfg.selectplayed ) {
        pl_m_select( loaded_record );
      }
      if( cfg.shf && !loaded_record->played ) {
        loaded_record->played = ++played;
      }

      cn_scroll_to( container, (PRECORDCORE)loaded_record );
      pl_m_invalidate_record( loaded_record, CMA_NOREPOSITION );
    }
  } else if( as == MARK_AS_STOP ) {
    if( played_record ) {
      played_record->playing = FALSE;
      played_record->rc.hptrIcon = pl_m_proper_icon( played_record );
      pl_m_invalidate_record( played_record, CMA_NOREPOSITION );
      played_record = NULL;
    }
  } else if( as == MARK_AS_GREY ) {
    if( loaded_record ) {
      loaded_record->playing = FALSE;
      loaded_record->playable = FALSE;
      loaded_record->rc.hptrIcon = pl_m_proper_icon( loaded_record );
      pl_m_invalidate_record( loaded_record, CMA_NOREPOSITION );
    }
  }
}

/* Removes "already played" marks from all playlist records. */
static void
pl_m_clean_shuffle( void )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
    rec->played = 0;
  }
  played = 0;
}

/* Prepares the first playlist record to playing. */
BOOL
pl_load_first_record( void ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_LOAD_FRST_RECORD, 0, 0 ));
}

/* Prepares the next playlist record to playing. */
BOOL
pl_load_next_record( void ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_LOAD_NEXT_RECORD, 0, 0 ));
}

/* Prepares the previous playlist record to playing. */
BOOL
pl_load_prev_record( void ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_LOAD_PREV_RECORD, 0, 0 ));
}

/* Prepares the first playlist record to playing. */
BOOL
pl_load_file_record( const char* filename ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_LOAD_FILE_RECORD, MPFROMP( filename ), 0 ));
}

/* Prepares the specified playlist record to playing. */
BOOL
pl_load_record( ULONG index ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_LOAD_BY_INDEX, MPFROMLONG( index ), 0 ));
}

/* Marks the currently loaded playlist record as currently played. */
void
pl_mark_as_play( void ) {
  WinSendMsg( playlist, PL_MSG_MARK_AS, MPFROMLONG( MARK_AS_PLAY ), 0 );
}

/* Marks the currently loaded playlist record as currently stopped. */
void
pl_mark_as_stop( void ) {
  WinSendMsg( playlist, PL_MSG_MARK_AS, MPFROMLONG( MARK_AS_STOP ), 0 );
}

/* Marks the currently loaded playlist record as non playable. */
void
pl_mark_as_grey( void ) {
  WinSendMsg( playlist, PL_MSG_MARK_AS, MPFROMLONG( MARK_AS_GREY ), 0 );
}

/* Removes "already played" marks from all playlist records. */
void
pl_clean_shuffle( void ) {
  WinSendMsg( playlist, PL_MSG_CLEAN_SHUFFLE, 0, 0 );
}

/* Returns a ordinal number of the specified record. */
static ULONG
pl_m_index_of_record( PLRECORD* find )
{
  PLRECORD* rec;
  ULONG     pos = 1;

  ASSERT_IS_MAIN_THREAD;

  if( find == (PLRECORD*)CMA_END ) {
    return INDEX_END;
  } else if( find == (PLRECORD*)CMA_FIRST ) {
    return INDEX_FIRST;
  } else {
    for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec ), ++pos ) {
      if( rec == find ) {
        return pos;
      }
    }
  }
  return INDEX_NONE;
}

/* Returns a record specified via ordinal number. */
static PLRECORD*
pl_m_record_by_index( ULONG pos )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  if( pos > 0 ) {
    for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
      if( !--pos ) {
        break;
      }
    }
    return rec;
  } else {
    return NULL;
  }
}

/* Inserts one record into a container control. */
static BOOL
pl_m_insert_record( int index, PLRECORD* rec )
{
  RECORDINSERT ins;
  PLRECORD*    pos;
  POINTL       ptl = {0};

  ASSERT_IS_MAIN_THREAD;

  if( index > 0 ) {
    if(!( pos = pl_m_record_by_index( index ))) {
      pos = (PLRECORD*)CMA_END;
    }
  } else if( index == INDEX_FIRST ) {
    pos = (PLRECORD*)CMA_FIRST;
  } else {
    pos = (PLRECORD*)CMA_END;
  }

  ins.cb                = sizeof(RECORDINSERT);
  ins.pRecordOrder      = (PRECORDCORE)pos;
  ins.pRecordParent     = NULL;
  ins.fInvalidateRecord = FALSE;
  ins.zOrder            = CMA_TOP;
  ins.cRecordsInsert    = 1;

  // Using pl_m_invalidate_record instead of setting of fInvalidateRecord
  // to TRUE is more gently and pleasing but this is not working in
  // case bottom part of container is not visible.
  if( WinMapWindowPoints( container, HWND_DESKTOP, &ptl, 1 ) && ptl.y < 0 ) {
    ins.fInvalidateRecord = TRUE;
  }
  if( WinSendMsg( container, CM_INSERTRECORD, MPFROMP( rec ), MPFROMP( &ins )) != 0 ) {
    if( !ins.fInvalidateRecord ) {
      pl_m_invalidate_record( rec, CMA_TEXTCHANGED );
    }
    return TRUE;
  }

  return FALSE;
}

/* Frees the data contained in playlist record. */
static void
pl_m_free_record( PLRECORD* rec )
{
  free( rec->rc.pszIcon );
  free( rec->songname );
  free( rec->size );
  free( rec->time );
  free( rec->moreinfo );
  free( rec->full );
}

/* Creates the playlist record for specified file. */
static PLRECORD*
pl_m_create_record( const char* filename, int index,
                    const DECODER_INFO* info, const char* decoder )
{
  PLRECORD* rec;
  char name[_MAX_URL];

  if( !filename || !*filename ) {
    return NULL;
  }

  // Allocate a new record.
  rec = (PLRECORD*)WinSendMsg( container, CM_ALLOCRECORD,
                               MPFROMLONG( sizeof( PLRECORD ) - sizeof( RECORDCORE )),
                               MPFROMLONG( 1 ));
  if( rec ) {
    rec->rc.cb           = sizeof( RECORDCORE );
    rec->rc.flRecordAttr = CRA_DROPONABLE;
    rec->played          = 0;
    rec->playable        = info != NULL;
    rec->rc.hptrIcon     = pl_m_proper_icon( rec );
    rec->full            = strdup( filename );
    rec->size            = NULL;
    rec->time            = NULL;
    rec->songname        = NULL;
    rec->moreinfo        = NULL;

    amp_parse_filename( name, filename, PFN_NAMEEXT, sizeof( name ));
    rec->rc.pszIcon = strdup( name );
    pl_m_fill_record( rec, info );

    if( decoder ) {
      strlcpy( rec->decoder, decoder, sizeof( rec->decoder ));
    }

    if( WinSendMsg( playlist, PL_MSG_INSERT_RECORD, MPFROMLONG( index ), rec )) {
      amp_invalidate( UPD_FILEINFO | UPD_TIMERS | UPD_DELAYED );
      upnp_notify_state_change( UPNP_N_TRACKS | UPNP_N_TIMERS );
      return rec;
    } else {
      pl_m_free_record( rec );
      WinSendMsg( container, CM_FREERECORD, MPFROMP( &rec ), MPFROMSHORT( 1 ));
    }
  }
  return NULL;
}

/* Copies the playlist record to specified position. */
static PLRECORD*
pl_m_copy_record( PLRECORD* rec, PLRECORD* pos )
{
  RECORDINSERT insert;
  PLRECORD*    copy;

  ASSERT_IS_MAIN_THREAD;
  copy = (PLRECORD*)WinSendMsg( container, CM_ALLOCRECORD,
                                MPFROMLONG( sizeof( PLRECORD ) - sizeof( RECORDCORE )),
                                MPFROMLONG( 1 ));
  if( !copy ) {
    return NULL;
  }

  copy->rc.cb           = sizeof(RECORDCORE);
  copy->rc.flRecordAttr = CRA_DROPONABLE;
  copy->full            = strdup( rec->full );
  copy->size            = strdup( rec->size );
  copy->songname        = strdup( rec->songname );
  copy->moreinfo        = strdup( rec->moreinfo );
  copy->time            = strdup( rec->time );
  copy->rc.pszIcon      = strdup( rec->rc.pszIcon );
  copy->info            = rec->info;
  copy->played          = 0;
  copy->playing         = FALSE;
  copy->playable        = rec->playable;
  copy->rc.hptrIcon     = pl_m_proper_icon( copy );

  strcpy( copy->decoder, rec->decoder );

  insert.cb                = sizeof(RECORDINSERT);
  insert.pRecordOrder      = (PRECORDCORE)pos;
  insert.pRecordParent     = NULL;
  insert.fInvalidateRecord = TRUE;
  insert.zOrder            = CMA_TOP;
  insert.cRecordsInsert    = 1;

  if( WinSendMsg( container, CM_INSERTRECORD,
                  MPFROMP( copy ), MPFROMP( &insert )) == 0 )
  {
    return NULL;
  } else {
    upnp_notify_state_change( UPNP_N_TRACKS | UPNP_N_TIMERS );
    return copy;
  }
}

/* Moves the playlist record to specified position. */
static PLRECORD*
pl_m_move_record( PLRECORD* rec, PLRECORD* pos )
{
  RECORDINSERT insert;
  ASSERT_IS_MAIN_THREAD;

  if( WinSendMsg( container, CM_REMOVERECORD, MPFROMP( &rec ),
                  MPFROM2SHORT( 1, CMA_INVALIDATE )) != MRFROMLONG( -1 ))
  {
    insert.cb                = sizeof(RECORDINSERT);
    insert.pRecordOrder      = (PRECORDCORE)pos;
    insert.pRecordParent     = NULL;
    insert.fInvalidateRecord = TRUE;
    insert.zOrder            = CMA_TOP;
    insert.cRecordsInsert    = 1;

    if( WinSendMsg( container, CM_INSERTRECORD,
                    MPFROMP( rec ), MPFROMP( &insert )) == 0 )
    {
      return NULL;
    }
  }
  return rec;
}

/* Removes the specified playlist records. */
static BOOL
pl_m_remove_records( PLRECORD** array, USHORT count, int options )
{
  PLRECORD* load = loaded_record;
  PLRECORD* rec;

  int array_size = 0;
  int i;

  ASSERT_IS_MAIN_THREAD;

  if( options & PL_REMOVE_SELECTED )
  {
    array = NULL;
    count = 0;

    for( rec = pl_m_first_selected(); rec; rec = pl_m_next_selected( rec )) {
      if( count == array_size ) {
        array_size += 20;
        array = realloc( array, array_size * sizeof( PLRECORD* ));
      }
      if( !array ) {
        break;
      }
      array[count++] = rec;
    }
  }
  else if( options & PL_REMOVE_LOADED )
  {
    if( loaded_record ) {
      if(( array = malloc( sizeof( PLRECORD* ))) != NULL ) {
        array[0] = loaded_record;
        count    = 1;
      }
    }
  }
  else if( options & PL_REMOVE_DEAD )
  {
    array = NULL;
    count = 0;

    for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
      if( !rec->playable ) {
        if( count == array_size ) {
          array_size += 20;
          array = realloc( array, array_size * sizeof( PLRECORD* ));
        }
        if( !array ) {
          break;
        }
        array[count++] = rec;
      }
    }
  }
  else if( options & PL_REMOVE_DUPLICATE )
  {
    ght_hash_table_t* ht = ght_create( 0 );

    if( loaded_record ) {
      ght_insert( ht, loaded_record, strlen( loaded_record->full ) + 1, loaded_record->full );
    }

    array = NULL;
    count = 0;

    for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
      if( ght_get( ht, strlen( rec->full ) + 1, rec->full )) {
        if( rec != loaded_record ) {
          if( count == array_size ) {
            array_size += 20;
            array = realloc( array, array_size * sizeof( PLRECORD* ));
          }
          if( !array ) {
            break;
          }
          array[count++] = rec;
        }
      } else {
        ght_insert( ht, rec, strlen( rec->full ) + 1, rec->full );
      }
    }

    ght_finalize( ht );
  }

  if( !count || !array ) {
    return FALSE;
  }

  for( i = count - 1; i >= 0; i-- ) {
    if( array[i] == played_record ) {
      played_record = NULL;
    }
    if( array[i] == load && amp_playmode == AMP_PLAYLIST ) {
      while(( load = pl_m_prev_record( load )) != NULL ) {
        if( load->playable ) {
          break;
        }
      }
    }
  }

  if( WinSendMsg( container, CM_REMOVERECORD, MPFROMP( array ),
                  MPFROM2SHORT( count, CMA_INVALIDATE )) == MRFROMLONG( -1 ))
  {
    return FALSE;
  }

  if( !load ) {
    for( load = pl_m_first_record(); load; load = pl_m_next_record( load )) {
      if( load->playable ) {
        break;
      }
    }
  }

  if( amp_playmode == AMP_PLAYLIST ) {
    if( loaded_record != load && decoder_playing()) {
      amp_stop();
    }
    if( load ) {
      pl_m_load_record( load );
    } else {
      loaded_record = NULL;
      amp_reset();
    }
  }

  for( i = 0; i < count; i++ ) {
    if( options & PL_DELETE_FILES ) {
      if( is_regular_file( array[i]->full ) && unlink( array[i]->full ) != 0 ) {
        amp_show_error( "Unable delete file:\n%s\n%s",
                         array[i]->full, strerror( errno ));
      }
    }
    pl_m_free_record( array[i] );
  }

  WinSendMsg( container, CM_FREERECORD,
              MPFROMP( array ), MPFROMSHORT( count ));

  pl_m_select( pl_m_cursored());

  if( options & PL_REMOVE_SELECTED ||
      options & PL_REMOVE_LOADED    )
  {
    free( array );
  }
  if( amp_playmode == AMP_PLAYLIST ) {
    amp_invalidate( UPD_FILEINFO | UPD_TIMERS | UPD_DELAYED );
  }

  upnp_notify_state_change( UPNP_N_TRACKS | UPNP_N_TIMERS );
  return TRUE;
}

/* Removes all playlist records. */
static void
pl_m_remove_all( void )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  if( amp_playmode == AMP_PLAYLIST ) {
    if( !amp_reset()) {
      return;
    }
  }

  loaded_record = NULL;
  played_record = NULL;

  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
    pl_m_free_record( rec );
  }

  WinSendMsg( container, CM_REMOVERECORD,
              MPFROMP( NULL ), MPFROM2SHORT( 0, CMA_FREE | CMA_INVALIDATE ));

  upnp_notify_state_change( UPNP_N_TRACKS | UPNP_N_TIMERS );
}

/* Removes the specified playlist records. */
void
pl_remove_records( int options ) {
  WinSendMsg( playlist, PL_MSG_REMOVE_RECORD, MPFROMLONG( options ), 0 );
}

/* Refreshes the playlist records of the specified file. */
static void
pl_m_refresh_file( const char* filename, const DECODER_INFO* info )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
    if( nlstricmp( rec->full, filename ) == 0 ) {
      rec->playable = TRUE;
      rec->rc.hptrIcon = pl_m_proper_icon( rec );
      pl_m_fill_record( rec, info );
      pl_m_invalidate_record( rec, CMA_TEXTCHANGED );
    }
  }
}

/* Sets the songname for the specified file. */
static void
pl_m_refresh_songname( const char* filename, const char* songname )
{
  PLRECORD* rec;
  ASSERT_IS_MAIN_THREAD;

  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec )) {
    if( nlstricmp( rec->full, filename ) == 0 ) {
      free( rec->songname );
      rec->songname = strdup( songname );
      pl_m_invalidate_record( rec, CMA_TEXTCHANGED );
    }
  }
}

/* Refreshes the playlist records of the specified file. */
void
pl_refresh_file( const char* filename, const DECODER_INFO* info ) {
  WinSendMsg( playlist, PL_MSG_REFRESH_FILE,
                        MPFROMP( filename ), MPFROMP( info ));
}

/* Sets the songname for the specified file. */
void
pl_refresh_songname( const char* filename, const char* songname ) {
  WinSendMsg( playlist, PL_MSG_REFRESH_SONGNAME,
                        MPFROMP( filename ), MPFROMP( songname ));
}

/* Returns a ordinal number of the currently loaded record. */
ULONG
pl_loaded_index( void ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_LOADED_INDEX, 0, 0 ));
}

/* Sets the title of the playlist window according to current
   playlist state. */
static void
pl_m_refresh_status( const char* playlist_name )
{
  char title[ _MAX_FNAME + 128 ];
  char file [ _MAX_FNAME ];

  ASSERT_IS_MAIN_THREAD;
  strcpy( title, "PM123 Playlist"  );

  if( playlist_name ) {
    strlcpy( current_playlist, playlist_name, sizeof( current_playlist ));
  }
  if( *current_playlist ) {
    amp_parse_filename( file, current_playlist, PFN_NAMEEXT, sizeof( file ));
    strlcat( title, " - ", sizeof( title ));
    strlcat( title, file,  sizeof( title ));
  }

  if( amp_playmode == AMP_PLAYLIST ) {
    strlcat( title, " - USED", sizeof( title ));
  }
  if( pl_busy ) {
    strlcat( title, " - loading...", sizeof( title ));
  }

  WinSetWindowText( playlist, (PSZ)title );
}

/* Sets the title of the playlist window according to current
   playlist state. */
void
pl_refresh_status( void ) {
  WinSendMsg( playlist, PL_MSG_REFRESH_STATUS, NULL, 0 );
}

static PLDATA*
pl_m_create_request_data( const char* filename,
                          const char* songname, int index, int options )
{
  PLDATA* data = (PLDATA*)malloc( sizeof( PLDATA ));

  if( data ) {
    data->filename = filename ? strdup( filename ) : NULL;
    data->songname = songname ? strdup( songname ) : NULL;
    data->index    = index;
    data->options  = options;
  }

  return data;
}

static void
pl_m_delete_request_data( PLDATA* data )
{
  if( data ) {
    free( data->filename );
    free( data->songname );
    free( data );
  }
}

/* Purges a specified queue of all its requests. */
static void
pl_m_purge_queue( PQUEUE queue )
{
  ULONG  request;
  PVOID  data;

  while( !qu_empty( queue )) {
    qu_read( queue, &request, &data );
    pl_m_delete_request_data( data );
  }
}

/* Examines a queue request without removing it from the queue. */
static BOOL
pl_m_peek_queue( PQUEUE queue, ULONG first, ULONG last )
{
  ULONG request;
  PVOID data;

  if( qu_peek( queue, &request, &data )) {
    return request >= first && request <= last;
  } else {
    return FALSE;
  }
}

/* The playlist broker helper function. */
static void
pl_broker_add_file( const char* filename,
                    const char* title, int index, int options )
{
  DECODER_INFO info = { sizeof( DECODER_INFO )};
  char         decoder[_MAX_MODULE_NAME] = "";

  if( !pl_busy ) {
    pl_busy = TRUE;
    pl_refresh_status();
  }

  if( dec_fileinfo( filename, &info, decoder, 0 ) == PLUGIN_OK ) {
    if( !*info.title && title ) {
      strlcpy( info.title, title, sizeof( info.title ));
    }
    pl_m_create_record( filename, index, &info, decoder );
  } else {
    pl_m_create_record( filename, index, NULL, NULL );
  }
}

/* The playlist broker helper function. */
static void
pl_broker_add_directory( const char* path, int options )
{
  char  findpath[_MAX_PATH];
  char  findspec[_MAX_PATH];
  char  fullname[_MAX_PATH];
  ULONG findrc;
  HDIR  hdir;

  FILEFINDBUF3 findbuff;

  if( !pl_busy ) {
    pl_busy = TRUE;
    pl_refresh_status();
  }

  strcpy( findpath, path );
  if( *findpath && findpath[strlen(findpath)-1] != '\\' ) {
    strcat( findpath, "\\" );
  }

  strcpy( findspec, findpath );
  strcat( findspec, "*" );

  findrc = findfirst( &hdir, findspec, FIND_ALL, &findbuff );

  while( findrc == 0 && !pl_m_peek_queue( broker_queue, PL_CLEAR, PL_TERMINATE ))
  {
    strcpy( fullname, findpath );
    strcat( fullname, findbuff.achName );

    if( findbuff.attrFile & FILE_DIRECTORY ) {
      if( options & PL_DIR_RECURSIVE
          && strcmp( findbuff.achName, "."  ) != 0
          && strcmp( findbuff.achName, ".." ) != 0 )
      {
        pl_broker_add_directory( fullname, options );
      }
    } else {
      char decoder[_MAX_MODULE_NAME] = "";
      DECODER_INFO info = { sizeof( DECODER_INFO )};

      if( dec_fileinfo( fullname, &info, decoder, 0 ) == PLUGIN_OK ) {
        pl_m_create_record( fullname, INDEX_END, &info, decoder );
      }
    }

    findrc = findnext( hdir, &findbuff );
  }

  if( findrc != ERROR_NO_MORE_FILES && findrc != NO_ERROR ) {
    char msg[1024];
    amp_show_error( "Unable scan the directory:\n%s\n%s", path,
                    os2_strerror( findrc, msg, sizeof( msg )));
  }

  findclose( hdir );
}

/* Dispatches the playlist management requests. */
static void TFNENTRY
pl_broker( void* dummy )
{
  ULONG   request;
  PVOID   reqdata;
  HAB     hab = WinInitialize( 0 );
  HMQ     hmq = WinCreateMsgQueue( hab, 0 );

  while( qu_read( broker_queue, &request, &reqdata ))
  {
    PLDATA* data = (PLDATA*)reqdata;

    switch( request ) {
      case PL_CLEAR:
        WinSendMsg( playlist, PL_MSG_REMOVE_ALL, 0, 0 );
        WinSendMsg( playlist, PL_MSG_REFRESH_STATUS, NULL, 0 );
        break;

      case PL_ADD_FILE:
        pl_broker_add_file( data->filename, data->songname,
                            data->index, data->options );
        break;

      case PL_TERMINATE:
        break;

      case PL_ADD_DIRECTORY:
        pl_broker_add_directory( data->filename, data->options );
        pl_completed();
        break;

      case PL_LOAD_PLAYLIST:
        pl_load_any_list( data->filename, data->options, NULL );
        break;

      case PL_SELECT_BY_INDEX:
        if( !decoder_playing()) {
          WinSendMsg( playlist, PL_MSG_SELECT_BY_INDEX, MPFROMLONG( data->index ), 0 );
        }
        break;

      case PL_RELOAD_BY_INDEX:
        amp_playmode = AMP_PLAYLIST;
        upnp_notify_state_change( UPNP_N_PLAYERMODE );
        if( !decoder_playing()) {
          WinSendMsg( playlist, PL_MSG_LOAD_BY_INDEX, MPFROMLONG( data->index ), 0 );
        } else {
          pl_mark_as_play();
        }
        break;

      case PL_COMPLETED:
        if( cfg.autouse || amp_playmode == AMP_PLAYLIST ) {
          amp_pl_use( TRUE );
        }
        break;
    }

    pl_m_delete_request_data( data );

    if( qu_empty( broker_queue ) && pl_busy ) {
      pl_busy = FALSE;
      pl_refresh_status();
    }

    if( request == PL_TERMINATE ) {
      break;
    }
  }

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
  _endthread();
}

/* Returns the number of records in the playlist. */
ULONG
pl_size( void )
{
  CNRINFO info;

  if( WinSendMsg( container, CM_QUERYCNRINFO,
                  MPFROMP(&info), MPFROMLONG(sizeof(info))) != 0 )
  {
    return info.cRecords;
  } else {
    return 0;
  }
}

/* Returns a summary play time in seconds of the remained part of the playlist. */
static ULONG
pl_m_playleft( void )
{
  ULONG     time = 0;
  PLRECORD* rec;

  ASSERT_IS_MAIN_THREAD;

  if( cfg.shf || !loaded_record ) {
    rec = pl_m_first_record();
  } else {
    rec = loaded_record;
  }

  while( rec ) {
    if( !rec->played || rec == loaded_record || !cfg.shf ) {
      time += rec->info.songlength / 1000;
    }
    rec = pl_m_next_record( rec );
  }
  return time;
}

/* Returns a summary play time in seconds of the remained part of the playlist. */
ULONG
pl_playleft( void ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_PLAY_LEFT, 0, 0 ));
}

/* Returns a summary play time in ms of the playlist. */
static LONGLONG
pl_m_playtime( void )
{
  LONGLONG  ms  = 0;
  PLRECORD* rec = pl_m_first_record();

  ASSERT_IS_MAIN_THREAD;

  while( rec ) {
    ms += rec->info.songlength;
    rec = pl_m_next_record( rec );
  }

  return ms;
}

/* Returns a summary play time in ms of the playlist. */
LONGLONG
pl_playtime( void )
{
  LONGLONG ms;

  WinSendMsg( playlist, PL_MSG_PLAY_TIME, MPFROMP( &ms ), 0 );
  return ms;
}

/* Returns a length in ms of the played part of the playlist. */
static LONGLONG
pl_m_played( void )
{
  LONGLONG  ms  = 0;
  PLRECORD* rec = pl_m_first_record();

  ASSERT_IS_MAIN_THREAD;

  while( rec && ( rec != loaded_record || cfg.shf )) {
    if( rec != loaded_record && ( rec->played || !cfg.shf )) {
      ms += rec->info.songlength;
    }

    rec = pl_m_next_record( rec );
  }

  return ms;
}

/* Returns a length in ms of the played part of the playlist. */
LONGLONG
pl_played( void )
{
  LONGLONG ms;

  WinSendMsg( playlist, PL_MSG_PLAYED, MPFROMP( &ms ), 0 );
  return ms;
}

/* Deletes all selected files. */
static void
pl_m_delete_selected( void )
{
  ASSERT_IS_MAIN_THREAD;

  if( amp_query( playlist, "Do you want remove all selected files from the playlist "
                           "and delete this files from your disk?" ))
  {
    pl_m_remove_records( NULL, 0, PL_REMOVE_SELECTED | PL_DELETE_FILES );
  }
}

/* Shows the context menu of the playlist. */
static void
pl_m_show_context_menu( HWND parent, PLRECORD* rec )
{
  POINTL pos;
  SWP    swp;
  char   file[_MAX_URL];
  HWND   hmenu = rec ? menu_record : menu_playlist;
  HWND   submenu;
  int    i;
  int    count;

  ASSERT_IS_MAIN_THREAD;

  WinQueryPointerPos( HWND_DESKTOP, &pos );
  WinMapWindowPoints( HWND_DESKTOP, parent, &pos, 1 );

  if( WinWindowFromPoint( parent, &pos, TRUE ) == NULLHANDLE )
  {
    // The context menu is probably activated from the keyboard.
    WinQueryWindowPos( parent, &swp );
    pos.x = swp.cx / 2;
    pos.y = swp.cy / 2;
  }

  submenu = mn_get_submenu( hmenu, IDM_PL_OPEN_MENU );
  count = mn_count( submenu );

  // Remove all items from the open menu except of first
  // intended for a choice of object of loading.
  for( i = 1; i < count; i++ ) {
    mn_remove_item( submenu, mn_item_id( submenu, 1 ));
  }

  // Fill the recall list.
  if( *cfg.last_list[0] ) {
    mn_add_separator( submenu, 0 );

    for( i = 0; i < MAX_RECALL; i++ ) {
      if( *cfg.last_list[i] ) {
        sprintf( file, "~%u ", i + 1 );
        amp_parse_filename( file + strlen( file ), cfg.last_list[i],
                            PFN_NAMEEXT | PFN_PRESCHEME, sizeof( file ) - strlen( file ) );
        mn_add_item( submenu, IDM_PL_LAST + i + 1, file, TRUE, FALSE, NULL );
      }
    }

    mn_add_separator( submenu, 0 );
    mn_add_item( submenu, IDM_PL_LAST_CLEAR, "~Clear history", TRUE, FALSE, NULL );
  }

  if( amp_playmode == AMP_PLAYLIST ) {
    WinSendMsg( hmenu, MM_SETITEMTEXT,
                MPFROMSHORT( IDM_PL_USE ), MPFROMP( "Don't ~use this Playlist\tCtrl+U" ));
  } else {
    WinSendMsg( hmenu, MM_SETITEMTEXT,
                MPFROMSHORT( IDM_PL_USE ), MPFROMP( "~Use this Playlist\tCtrl+U" ));
  }

  // Update status
  if( rec ) {
    mn_enable_item( menu_record,   IDM_PL_USE,              !is_busy());
    mn_enable_item( menu_record,   IDM_PL_DEAD_REMOVE,      !is_busy());
    mn_enable_item( menu_record,   IDM_PL_DUPLICATE_REMOVE, !is_busy());
    mn_enable_item( menu_record,   IDM_PL_CLEAR,            !is_busy());
    mn_enable_item( menu_record,   IDM_PL_OPEN_MENU,        !is_busy());
    mn_enable_item( menu_record,   IDM_RC_PLAY,             !is_busy());
    mn_enable_item( menu_record,   IDM_RC_REMOVE,           !is_busy());
    mn_enable_item( menu_record,   IDM_RC_DELETE,           !is_busy());
    mn_enable_item( menu_record,   IDM_RC_EDIT,             !is_busy());
    mn_enable_item( menu_record,   IDM_RC_OPEN_FOLDER,       is_file( rec->full ));

    mn_check_item ( menu_record,   IDM_PL_SORT_KEEP, cfg.sort_keep );
    mn_check_item ( menu_record,   IDM_PL_SORT_SIZE, cfg.sort_keep && cfg.sort == PL_SORT_SIZE );
    mn_check_item ( menu_record,   IDM_PL_SORT_TIME, cfg.sort_keep && cfg.sort == PL_SORT_TIME );
    mn_check_item ( menu_record,   IDM_PL_SORT_FILE, cfg.sort_keep && cfg.sort == PL_SORT_FILE );
    mn_check_item ( menu_record,   IDM_PL_SORT_TRCK, cfg.sort_keep && cfg.sort == PL_SORT_TRCK );
    mn_check_item ( menu_record,   IDM_PL_SORT_SONG, cfg.sort_keep && cfg.sort == PL_SORT_SONG );
    mn_check_item ( menu_record,   IDM_PL_SORT_RAND, cfg.sort_keep && cfg.sort == PL_SORT_RAND );

  } else {
    mn_enable_item( menu_playlist, IDM_PL_USE,              !is_busy());
    mn_enable_item( menu_playlist, IDM_PL_DEAD_REMOVE,      !is_busy());
    mn_enable_item( menu_playlist, IDM_PL_DUPLICATE_REMOVE, !is_busy());
    mn_enable_item( menu_playlist, IDM_PL_CLEAR,            !is_busy());
    mn_enable_item( menu_playlist, IDM_PL_OPEN_MENU,        !is_busy());

    mn_check_item ( menu_playlist, IDM_PL_SORT_KEEP, cfg.sort_keep );
    mn_check_item ( menu_playlist, IDM_PL_SORT_SIZE, cfg.sort_keep && cfg.sort == PL_SORT_SIZE );
    mn_check_item ( menu_playlist, IDM_PL_SORT_TIME, cfg.sort_keep && cfg.sort == PL_SORT_TIME );
    mn_check_item ( menu_playlist, IDM_PL_SORT_FILE, cfg.sort_keep && cfg.sort == PL_SORT_FILE );
    mn_check_item ( menu_playlist, IDM_PL_SORT_TRCK, cfg.sort_keep && cfg.sort == PL_SORT_TRCK );
    mn_check_item ( menu_playlist, IDM_PL_SORT_SONG, cfg.sort_keep && cfg.sort == PL_SORT_SONG );
    mn_check_item ( menu_playlist, IDM_PL_SORT_RAND, cfg.sort_keep && cfg.sort == PL_SORT_RAND );
  }

  WinPopupMenu( parent, parent, hmenu, pos.x, pos.y, rec ? IDM_RC_PLAY : IDM_PL_USE,
                PU_POSITIONONITEM | PU_HCONSTRAIN   | PU_VCONSTRAIN |
                PU_MOUSEBUTTON1   | PU_MOUSEBUTTON2 | PU_KEYBOARD   );
}

/* Prepares the playlist container item to the drag operation. */
static void
pl_m_drag_init_item( HWND hwnd, PLRECORD* rec,
                     PDRAGINFO drag_infos, PDRAGIMAGE drag_image, int i )
{
  DRAGITEM ditem = { 0 };
  char pathname[_MAX_URL];
  char filename[_MAX_URL];

  ASSERT_IS_MAIN_THREAD;

  sdrivedir( pathname, rec->full, sizeof( pathname ));
  sfnameext( filename, rec->full, sizeof( filename ));

  ditem.hwndItem          = hwnd;
  ditem.ulItemID          = (ULONG)rec;
  ditem.hstrType          = DrgAddStrHandle( DRT_BINDATA );
  ditem.hstrContainerName = DrgAddStrHandle( pathname );
  ditem.hstrSourceName    = DrgAddStrHandle( filename );
  ditem.hstrTargetName    = DrgAddStrHandle( filename );
  ditem.fsSupportedOps    = DO_COPYABLE | DO_LINKABLE;

  ditem.hstrRMF = DrgAddStrHandle( is_url(rec->full) ?
                      "(DRM_123FILE,DRM_DISCARD)x(DRF_UNKNOWN)" :
                      "(DRM_123FILE,DRM_OS2FILE,DRM_DISCARD)x(DRF_UNKNOWN)" );

  DrgSetDragitem( drag_infos, &ditem, sizeof( DRAGITEM ), i );

  if( i < _MAX_DRAG_IMAGES )
  {
    drag_image[i].cb       = sizeof( DRAGIMAGE );
    drag_image[i].hImage   = rec->rc.hptrIcon;
    drag_image[i].fl       = DRG_ICON | DRG_MINIBITMAP;
    drag_image[i].cxOffset = 5 * i;
    drag_image[i].cyOffset = 5 * i;
  }

  WinSendDlgItemMsg( hwnd, CNR_PLAYLIST, CM_SETRECORDEMPHASIS,
                     MPFROMP( rec ), MPFROM2SHORT( TRUE, CRA_SOURCE ));
}

/* Prepares the playlist container to the drag operation. */
static MRESULT
pl_m_drag_init( HWND hwnd, PCNRDRAGINIT pcdi )
{
  PLRECORD*  rec;
  BOOL       drag_selected     = FALSE;
  int        drag_count        = 0;
  int        drag_images_count = _MAX_DRAG_IMAGES;
  PDRAGIMAGE drag_images       = NULL;
  PDRAGINFO  drag_infos        = NULL;

  ASSERT_IS_MAIN_THREAD;

  // If the record under the mouse is NULL, we must be over whitespace,
  // in which case we don't want to drag any records.

  if( !(PLRECORD*)pcdi->pRecord ) {
    return 0;
  }

  // Count the selected records. Also determine whether or not we should
  // process the selected records. If the container record under the
  // mouse does not have this emphasis, we shouldn't.

  for( rec = pl_m_first_selected(); rec; rec = pl_m_next_selected( rec )) {
    if( rec == (PLRECORD*)pcdi->pRecord ) {
      drag_selected = TRUE;
    }
    ++drag_count;
  }

  if( !drag_selected ) {
    drag_count = 1;
  }

  // Allocate an array of DRAGIMAGE structures. Each structure contains
  // info about an image that will be under the mouse pointer during the
  // drag. This image will represent a container record being dragged.

  if( drag_count < drag_images_count ) {
    drag_images_count = drag_count;
  }

  drag_images = (PDRAGIMAGE)malloc( sizeof( DRAGIMAGE ) * drag_images_count );

  if( !drag_images ) {
    return 0;
  }

  // Let PM allocate enough memory for a DRAGINFO structure as well as
  // a DRAGITEM structure for each record being dragged. It will allocate
  // shared memory so other processes can participate in the drag/drop.

  drag_infos = DrgAllocDraginfo( drag_count );

  if( !drag_infos ) {
    return 0;
  }

  if( drag_selected ) {
    int i = 0;
    for( rec = pl_m_first_selected(); rec; rec = pl_m_next_selected( rec ), i++ ) {
      pl_m_drag_init_item( hwnd, rec, drag_infos, drag_images, i );
    }
  } else {
    pl_m_drag_init_item( hwnd, (PLRECORD*)pcdi->pRecord,
                         drag_infos, drag_images, 0 );
  }

  // If DrgDrag returns NULLHANDLE, that means the user hit Esc or F1
  // while the drag was going on so the target didn't have a chance to
  // delete the string handles. So it is up to the source window to do
  // it. Unfortunately there doesn't seem to be a way to determine
  // whether the NULLHANDLE means Esc was pressed as opposed to there
  // being an error in the drag operation. So we don't attempt to figure
  // that out. To us, a NULLHANDLE means Esc was pressed...

  if( !DrgDrag( hwnd, drag_infos, drag_images, drag_images_count, VK_ENDDRAG, NULL )) {
    DrgDeleteDraginfoStrHandles( drag_infos );
  }

  rec = (PLRECORD*)CMA_FIRST;
  while( rec ) {
    rec = (PLRECORD*)WinSendDlgItemMsg( hwnd, CNR_PLAYLIST, CM_QUERYRECORDEMPHASIS,
                                        MPFROMP( rec ), MPFROMSHORT( CRA_SOURCE ));

    if( rec == (PLRECORD*)(-1)) {
      break;
    } else if( rec ) {
      WinSendDlgItemMsg( hwnd, CNR_PLAYLIST, CM_SETRECORDEMPHASIS,
                         MPFROMP( rec ), MPFROM2SHORT( FALSE, CRA_SOURCE ));
    }
  }

  WinSendDlgItemMsg( hwnd, CNR_PLAYLIST, CM_INVALIDATERECORD, NULL,
                                         MPFROM2SHORT( 0, CMA_NOREPOSITION ));
  free( drag_images );
  DrgFreeDraginfo( drag_infos );
  return 0;
}

/* Prepares the playlist container to the drop operation. */
static MRESULT
pl_m_drag_over( HWND hwnd, PCNRDRAGINFO pcdi )
{
  PDRAGINFO pdinfo = pcdi->pDragInfo;
  PDRAGITEM pditem;
  int       i;
  USHORT    drag_op = DO_UNKNOWN;
  USHORT    drag    = DOR_NEVERDROP;

  ASSERT_IS_MAIN_THREAD;

  if( !DrgAccessDraginfo( pdinfo )) {
    return MRFROM2SHORT( DOR_NEVERDROP, 0 );
  }

  for( i = 0; i < pdinfo->cditem; i++ )
  {
    pditem = DrgQueryDragitemPtr( pdinfo, i );

    if( DrgVerifyRMF( pditem, "DRM_123FILE", NULL )) {
      if( pdinfo->usOperation == DO_DEFAULT )
      {
        drag    = DOR_DROP;
        drag_op = pdinfo->hwndSource == hwnd ? DO_MOVE : DO_COPY;
      }
      else if( pdinfo->usOperation == DO_COPY ||
               pdinfo->usOperation == DO_MOVE ||
               pdinfo->usOperation == DO_LINK )
      {
        drag    = DOR_DROP;
        drag_op = pdinfo->usOperation;
      } else {
        drag    = DOR_NODROPOP;
        drag_op = DO_UNKNOWN;
        break;
      }
    } else if( DrgVerifyRMF( pditem, "DRM_OS2FILE", NULL )) {
      if( pdinfo->usOperation == DO_DEFAULT &&
          pditem->fsSupportedOps & DO_COPYABLE )
      {
        drag    = DOR_DROP;
        drag_op = DO_COPY;
      }
      else if( pdinfo->usOperation == DO_LINK &&
               pditem->fsSupportedOps & DO_LINKABLE )
      {
        drag    = DOR_DROP;
        drag_op = pdinfo->usOperation;
      } else {
        drag    = DOR_NODROPOP;
        drag_op = DO_UNKNOWN;
        break;
      }
    } else {
      drag    = DOR_NEVERDROP;
      drag_op = DO_UNKNOWN;
      break;
    }
  }

  DrgFreeDraginfo( pdinfo );
  return MPFROM2SHORT( drag, drag_op );
}

/* Discards playlist records dropped into shredder.
   Must be called from the main thread. */
static MRESULT
pl_m_drag_discard( HWND hwnd, PDRAGINFO pdinfo )
{
  PDRAGITEM  pditem;
  PLRECORD** array = NULL;
  int        i;

  ASSERT_IS_MAIN_THREAD;

  if( !DrgAccessDraginfo( pdinfo )) {
    return MRFROMLONG( DRR_ABORT );
  }

  // We get as many DM_DISCARDOBJECT messages as there are
  // records dragged but the first one has enough info to
  // process all of them.

  array = malloc( pdinfo->cditem * sizeof( PLRECORD* ));

  if( array ) {
    for( i = 0; i < pdinfo->cditem; i++ ) {
      pditem = DrgQueryDragitemPtr( pdinfo, i );
      array[i] = (PLRECORD*)pditem->ulItemID;
    }

    pl_m_remove_records( array, pdinfo->cditem, 0 );
    free( array );
  }

  DrgFreeDraginfo( pdinfo );
  return MRFROMLONG( DRR_SOURCE );
}

/* Receives the dropped playlist records. */
static MRESULT
pl_m_drag_drop( HWND hwnd, PCNRDRAGINFO pcdi )
{
  PDRAGINFO pdinfo = pcdi->pDragInfo;
  PDRAGITEM pditem;

  char pathname[_MAX_URL];
  char filename[_MAX_URL];
  char fullname[_MAX_URL];
  int  i;

  PLRECORD* pos = pcdi->pRecord ? (PLRECORD*)pcdi->pRecord : (PLRECORD*)CMA_END;
  int index = pl_m_index_of_record( pos );

  ASSERT_IS_MAIN_THREAD;

  if( !DrgAccessDraginfo( pdinfo )) {
    return 0;
  }

  for( i = 0; i < pdinfo->cditem; i++ )
  {
    pditem = DrgQueryDragitemPtr( pdinfo, i );

    DrgQueryStrName( pditem->hstrSourceName, sizeof( filename ), filename );
    DrgQueryStrName( pditem->hstrContainerName, sizeof( pathname ), pathname );
    strlcpy( fullname, pathname, sizeof( fullname ));
    strlcat( fullname, filename, sizeof( fullname ));

    if( DrgVerifyRMF( pditem, "DRM_123FILE", NULL ))
    {
      PLRECORD* rec = (PLRECORD*)pditem->ulItemID;
      PLRECORD* ins;

      if( pdinfo->hwndSource == hwnd ) {
        if( pdinfo->usOperation == DO_MOVE ) {
          ins = pl_m_move_record( rec, pos );
        } else {
          ins = pl_m_copy_record( rec, pos );
        }
        if( ins ) {
          pos = ins;
        }
      }
      else
      {
        PID   pid;
        TID   tid;
        PCHAR sharedstr;

        // Because DrgAddStrHandle limits the length of the string to 255
        // characters, here used shared memory and a special message to get
        // an URL the length of which can exceed these limits.
        if( WinQueryWindowProcess( pdinfo->hwndSource, &pid, &tid )) {
          if( DosAllocSharedMem((PVOID*)&sharedstr, NULL, _MAX_URL,
                                 PAG_READ | OBJ_GIVEABLE | PAG_COMMIT ) == NO_ERROR )
          {
            if( DosGiveSharedMem( sharedstr, pid, PAG_WRITE ) == NO_ERROR ) {
              if( WinSendMsg( pdinfo->hwndSource, WM_123FILE_PATHNAME, MPFROMP( rec ), MPFROMP( sharedstr ))) {
                strlcpy( fullname, sharedstr, sizeof( fullname ));
              }
            }
            DosFreeMem( sharedstr );
          }
        }

        if( is_playlist( fullname )) {
          pl_load( fullname, 0 );
        } else {
          qu_write( broker_queue, PL_ADD_FILE,
                    pl_m_create_request_data( fullname, NULL, index, 0 ));

          if( index >= 0 ) {
            ++index;
          }

          if( pdinfo->usOperation == DO_MOVE ) {
            WinSendMsg( pdinfo->hwndSource, WM_123FILE_REMOVE, MPFROMP( rec ), 0 );
          }
        }
      }
    }
    else if( DrgVerifyRMF( pditem, "DRM_OS2FILE", NULL ))
    {
      if( pditem->hstrContainerName && pditem->hstrSourceName ) {
        // Have full qualified file name.
        if( DrgVerifyType( pditem, "UniformResourceLocator" )) {
          amp_url_from_file( fullname, fullname, sizeof( fullname ));
        }
        if( is_dir( fullname )) {
          pl_add_directory( fullname, PL_DIR_RECURSIVE );
        } else if( is_playlist( fullname )) {
          pl_load( fullname, 0 );
        } else {
          qu_write( broker_queue, PL_ADD_FILE,
                    pl_m_create_request_data( fullname, NULL, index, 0 ));

          if( index >= 0 ) {
            ++index;
          }
        }
        if( pditem->hwndItem ) {
          // Tell the source you're done.
          DrgSendTransferMsg( pditem->hwndItem, DM_ENDCONVERSATION, (MPARAM)pditem->ulItemID,
                                                                    (MPARAM)DMFL_TARGETSUCCESSFUL );
        }
      }
      else if( pditem->hwndItem &&
               DrgVerifyType( pditem, "UniformResourceLocator" ))
      {
        // The droped item must be rendered.
        PDRAGTRANSFER pdtrans  = DrgAllocDragtransfer(1);
        PLDROPINFO* pdsource = (PLDROPINFO*)malloc( sizeof( PLDROPINFO ));
        char renderto[_MAX_PATH];

        if( !pdtrans || !pdsource ) {
          return 0;
        }

        pdsource->index    = index;
        pdsource->hwndItem = pditem->hwndItem;
        pdsource->ulItemID = pditem->ulItemID;

        if( index >= 0 ) {
          ++index;
        }

        pdtrans->cb               = sizeof( DRAGTRANSFER );
        pdtrans->hwndClient       = hwnd;
        pdtrans->pditem           = pditem;
        pdtrans->hstrSelectedRMF  = DrgAddStrHandle( "<DRM_OS2FILE,DRF_TEXT>" );
        pdtrans->hstrRenderToName = 0;
        pdtrans->ulTargetInfo     = (ULONG)pdsource;
        pdtrans->fsReply          = 0;
        pdtrans->usOperation      = pdinfo->usOperation;

        // Send the message before setting a render-to name.
        if( pditem->fsControl & DC_PREPAREITEM ) {
          DrgSendTransferMsg( pditem->hwndItem, DM_RENDERPREPARE, (MPARAM)pdtrans, 0 );
        }

        strlcpy( renderto, startpath , sizeof( renderto ));
        strlcat( renderto, "pm123.dd", sizeof( renderto ));

        pdtrans->hstrRenderToName = DrgAddStrHandle( renderto );

        // Send the message after setting a render-to name.
        if(( pditem->fsControl & ( DC_PREPARE | DC_PREPAREITEM )) == DC_PREPARE ) {
          DrgSendTransferMsg( pditem->hwndItem, DM_RENDERPREPARE, (MPARAM)pdtrans, 0 );
        }

        // Ask the source to render the selected item.
        DrgSendTransferMsg( pditem->hwndItem, DM_RENDER, (MPARAM)pdtrans, 0 );
      }
    }
  }

  DrgDeleteDraginfoStrHandles( pdinfo );
  DrgFreeDraginfo( pdinfo );
  return 0;
}

/* Receives dropped and rendered files and urls. */
static MRESULT
pl_m_drag_render_done( HWND hwnd, PDRAGTRANSFER pdtrans, USHORT rc )
{
  char rendered[_MAX_PATH];
  char fullname[_MAX_URL];

  PLDROPINFO* pdsource = (PLDROPINFO*)pdtrans->ulTargetInfo;
  ASSERT_IS_MAIN_THREAD;

  // If the rendering was successful, use the file, then delete it.
  if(( rc & DMFL_RENDEROK ) && pdsource &&
       DrgQueryStrName( pdtrans->hstrRenderToName, sizeof( rendered ), rendered ))
  {
    amp_url_from_file( fullname, rendered, sizeof( fullname ));
    DosDelete( rendered );

    if( is_playlist( fullname )) {
      pl_load( fullname, 0 );
    } else {
      qu_write( broker_queue, PL_ADD_FILE,
                pl_m_create_request_data( fullname, NULL, pdsource->index, 0 ));
    }

    // Tell the source you're done.
    DrgSendTransferMsg( pdsource->hwndItem, DM_ENDCONVERSATION,
                       (MPARAM)pdsource->ulItemID, (MPARAM)DMFL_TARGETSUCCESSFUL );
    free( pdsource );
  }

  DrgDeleteStrHandle ( pdtrans->hstrSelectedRMF );
  DrgDeleteStrHandle ( pdtrans->hstrRenderToName );
  DrgFreeDragtransfer( pdtrans );
  return 0;
}

/* Loads the specified playlist record into the player and
   plays it if this is specified in the player properties or
   the player is already playing. Must be called from the
   main thread. */
static BOOL
pl_m_play_record( PLRECORD* rec )
{
  BOOL decoder_was_playing = decoder_playing();
  ASSERT_IS_MAIN_THREAD;

  if( decoder_was_playing ) {
    if( !cfg.continuous || is_always_hungry()) {
      amp_stop();
    }
  }

  if( amp_playmode == AMP_NOFILE ) {
    amp_playmode = AMP_SINGLE;
  }

  if( pl_m_load_record( rec )) {
    if( cfg.playonload || decoder_was_playing ) {
      return amp_play( 0 );
    }
  }

  return TRUE;
}

/* Adds user selected files or directory to the playlist. */
static void
pl_dlg_add_files( HWND owner )
{
  FILEDLG filedialog;

  int   i = 0;
  char* file;
  char  type_audio[2048];
  char  type_all  [2048];
  APSZ  types[4] = {{ 0 }};

  ASSERT_IS_MAIN_THREAD;

  types[0][0] = type_audio;
  types[1][0] = FDT_PLAYLIST;
  types[2][0] = type_all;

  memset( &filedialog, 0, sizeof( FILEDLG ));
  filedialog.cbSize     = sizeof( FILEDLG );
  filedialog.fl         = FDS_CENTER | FDS_OPEN_DIALOG | FDS_MULTIPLESEL;
  filedialog.ulUser     = FDU_DIR_ENABLE | FDU_RECURSEBTN;
  filedialog.pszTitle   = "Add file(s) to playlist";

  // WinFileDlg returns error if a length of the pszIType string is above
  // 255 characters. Therefore the small part from the full filter is used
  // as initial extended-attribute type filter. This part has enough to
  // find the full filter in the papszITypeList.
  filedialog.papszITypeList = types;
  filedialog.pszIType       = FDT_AUDIO_ALL;

  strcpy( type_audio, FDT_AUDIO );
  dec_fill_types( type_audio + strlen( type_audio ),
                  sizeof( type_audio ) - strlen( type_audio ) - 1 );
  strcat( type_audio, ")" );

  strcpy( type_all, FDT_AUDIO_ALL );
  dec_fill_types( type_all + strlen( type_all ),
                  sizeof( type_all ) - strlen( type_all ) - 1 );
  strcat( type_all, ")" );

  strcpy( filedialog.szFullFile, cfg.filedir );
  amp_file_dlg( HWND_DESKTOP, owner, &filedialog );

  if( filedialog.lReturn == DID_OK ) {
    if( filedialog.ulFQFCount > 1 ) {
      file = (*filedialog.papszFQFilename)[i];
    } else {
      file = filedialog.szFullFile;
    }

    while( *file ) {
      if( is_dir( file )) {
        pl_add_directory( file, filedialog.ulUser & FDU_RECURSE_ON ? PL_DIR_RECURSIVE : 0 );
        strcpy( cfg.filedir, file );
        if( !is_root( file )) {
          strcat( cfg.filedir, "\\" );
        }
      } else if( is_playlist( file )) {
        pl_load( file, PL_LOAD_NOT_RECALL );
        sdrivedir( cfg.filedir, file, sizeof( cfg.filedir ));
      } else {
        pl_add_file( file, NULL, 0 );
        sdrivedir( cfg.filedir, file, sizeof( cfg.filedir ));
      }

      if( ++i >= filedialog.ulFQFCount ) {
        break;
      } else {
        file = (*filedialog.papszFQFilename)[i];
      }
    }
    pl_completed();
  }

  WinFreeFileDlgList( filedialog.papszFQFilename );
}

/* Loads a playlist selected by the user to the player. Must be
   called from the main thread. */
static void
pl_dlg_load_list( HWND owner )
{
  FILEDLG filedialog;
  APSZ types[] = {{ FDT_PLAYLIST }, { 0 }};

  ASSERT_IS_MAIN_THREAD;
  memset( &filedialog, 0, sizeof( FILEDLG ));

  filedialog.cbSize         = sizeof( FILEDLG );
  filedialog.fl             = FDS_CENTER | FDS_OPEN_DIALOG;
  filedialog.pszTitle       = "Open playlist";
  filedialog.papszITypeList = types;
  filedialog.pszIType       = FDT_PLAYLIST;

  strcpy( filedialog.szFullFile, cfg.listdir );
  amp_file_dlg( HWND_DESKTOP, owner, &filedialog );

  if( filedialog.lReturn == DID_OK )
  {
    sdrivedir( cfg.listdir, filedialog.szFullFile, sizeof( cfg.listdir ));
    if( is_playlist( filedialog.szFullFile )) {
      pl_load( filedialog.szFullFile, PL_LOAD_CLEAR );
    }
  }
}

/* Saves current playlist to the file specified by user.
   Must be called from the main thread. */
static void
pl_dlg_save_list( HWND owner )
{
  FILEDLG filedialog;

  APSZ  types[] = {{ FDT_PLAYLIST_LST }, { FDT_PLAYLIST_M3U }, { FDT_PLAYLIST_M3U8 }, { 0 }};
  char  filez[_MAX_PATH];
  char  ext  [_MAX_EXT ];
  int   options = 0;

  ASSERT_IS_MAIN_THREAD;
  memset( &filedialog, 0, sizeof( FILEDLG ));

  filedialog.cbSize         = sizeof( FILEDLG );
  filedialog.fl             = FDS_CENTER | FDS_SAVEAS_DIALOG | FDS_ENABLEFILELB;
  filedialog.pszTitle       = "Save playlist";
  filedialog.ulUser         = FDU_RELATIVBTN;
  filedialog.papszITypeList = types;
  filedialog.pszIType       = *types[ limit2( cfg.save_type, 0, 2 )];

  strcpy( filedialog.szFullFile, cfg.listdir );
  amp_file_dlg( HWND_DESKTOP, owner, &filedialog );

  if( filedialog.lReturn == DID_OK )
  {
    sdrivedir( cfg.listdir, filedialog.szFullFile, sizeof( cfg.listdir ));
    cfg.save_type = limit2( filedialog.sEAType, 0, 2 );

    if( stricmp( *types[ cfg.save_type ], FDT_PLAYLIST_M3U ) == 0 ) {
      options |= PL_SAVE_M3U;
    } else if( stricmp( *types[ cfg.save_type ], FDT_PLAYLIST_M3U8 ) == 0 ) {
      options |= PL_SAVE_M3U | PL_SAVE_UTF8;
    } else {
      options |= PL_SAVE_LST;
    }
    if( filedialog.ulUser & FDU_RELATIV_ON ) {
      options |= PL_SAVE_RELATIVE;
    }

    strcpy( filez, filedialog.szFullFile );
    if( strcmp( sfext( ext, filez, sizeof( ext )), "" ) == 0 ) {
      if( options & PL_SAVE_M3U ) {
        if( options & PL_SAVE_UTF8 ) {
          strcat( filez, ".m3u8" );
        } else {
          strcat( filez, ".m3u"  );
        }
      } else {
        strcat( filez, ".lst" );
      }
    }
    if( amp_warn_if_overwrite( owner, filez )) {
      pl_save( filez, options );
    }
  }
}

/* Initializes the playlist presentation window. */
static void
pl_m_init_window( HWND hwnd )
{
  FIELDINFO* first;
  FIELDINFO* field;
  HPOINTER   hicon;

  FIELDINFOINSERT insert;
  CNRINFO cnrinfo;

  ASSERT_IS_MAIN_THREAD;
  container = WinWindowFromID( hwnd, CNR_PLAYLIST );

  /* Initializes the container of the playlist. */
  first = (FIELDINFO*)WinSendMsg( container, CM_ALLOCDETAILFIELDINFO, MPFROMSHORT(6), 0 );
  field = first;

  field->flData     = CFA_SEPARATOR | CFA_HORZSEPARATOR | CFA_BITMAPORICON;
  field->pTitleData = "";
  field->offStruct  = FIELDOFFSET( PLRECORD, rc.hptrIcon);

  field = field->pNextFieldInfo;

  field->flData     = CFA_STRING | CFA_HORZSEPARATOR;
  field->pTitleData = "Filename";
  field->offStruct  = FIELDOFFSET( PLRECORD, rc.pszIcon );

  field = field->pNextFieldInfo;

  field->flData     = CFA_SEPARATOR | CFA_HORZSEPARATOR | CFA_STRING;
  field->pTitleData = "Song name";
  field->offStruct  = FIELDOFFSET( PLRECORD, songname );

  field = field->pNextFieldInfo;

  field->flData     = CFA_SEPARATOR | CFA_HORZSEPARATOR | CFA_STRING | CFA_RIGHT;
  field->pTitleData = "Size";
  field->offStruct  = FIELDOFFSET( PLRECORD, size );

  field = field->pNextFieldInfo;

  field->flData     = CFA_SEPARATOR | CFA_HORZSEPARATOR | CFA_STRING;
  field->pTitleData = "Time";
  field->offStruct  = FIELDOFFSET( PLRECORD, time );

  field = field->pNextFieldInfo;

  field->flData     = CFA_SEPARATOR | CFA_HORZSEPARATOR | CFA_STRING;
  field->pTitleData = "Information";
  field->offStruct  = FIELDOFFSET( PLRECORD, moreinfo );

  insert.cb = sizeof(FIELDINFOINSERT);
  insert.pFieldInfoOrder = (PFIELDINFO)CMA_FIRST;
  insert.fInvalidateFieldInfo = TRUE;
  insert.cFieldInfoInsert = 6;

  WinSendMsg( container, CM_INSERTDETAILFIELDINFO,
              MPFROMP( first ), MPFROMP( &insert ));

  cnrinfo.cb             = sizeof(cnrinfo);
  cnrinfo.pFieldInfoLast = first->pNextFieldInfo;
  cnrinfo.flWindowAttr   = CV_DETAIL | CV_MINI  | CA_DRAWICON |
                           CA_DETAILSVIEWTITLES | CA_ORDEREDTARGETEMPH;
  cnrinfo.xVertSplitbar  = cfg.sbar_playlist;

  WinSendMsg( container, CM_SETCNRINFO, MPFROMP(&cnrinfo),
              MPFROMLONG( CMA_PFIELDINFOLAST | CMA_XVERTSPLITBAR | CMA_FLWINDOWATTR ));

  hicon = WinLoadPointer( HWND_DESKTOP, hmodule, ICO_MAIN );
  WinSendMsg( hwnd, WM_SETICON, (MPARAM)hicon, 0 );
  do_warpsans( hwnd );

  if( !rest_window_pos( hwnd, 0 )) {
    pl_set_colors( DEF_FG_COLOR, DEF_BG_COLOR,
                   DEF_HI_FG_COLOR, DEF_HI_BG_COLOR,
                   DEF_SB_FG_COLOR, DEF_SB_BG_COLOR );
  }

  menu_playlist = WinLoadMenu( HWND_OBJECT, hmodule, MNU_PLAYLIST );
  menu_record   = WinLoadMenu( HWND_OBJECT, hmodule, MNU_RECORD   );

  mn_set_default( mn_get_submenu( menu_playlist, IDM_PL_OPEN_MENU ), IDM_PL_OPEN_LIST );
  mn_set_default( mn_get_submenu( menu_playlist, IDM_PL_ADD_MENU  ), IDM_PL_ADD_FILES );
  mn_set_default( mn_get_submenu( menu_record,   IDM_PL_OPEN_MENU ), IDM_PL_OPEN_LIST );
  mn_set_default( mn_get_submenu( menu_record,   IDM_PL_ADD_MENU  ), IDM_PL_ADD_FILES );

  broker_queue = qu_create();

  if( !broker_queue ) {
    amp_show_error( "Unable create playlist service queue." );
  } else {
    if(( broker_tid = _beginthread( pl_broker, NULL, 2048000, NULL )) == -1 ) {
      amp_show_error( "Unable create the playlist service thread." );
    }
  }

  if( cfg.sort_keep && cfg.sort ) {
    pl_m_sort( cfg.sort );
  }
}

/* Processes messages of the playlist presentation window. */
static MRESULT EXPENTRY
pl_m_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
      pl_m_init_window( hwnd );
      dk_add_window( hwnd, 0 );
      break;

    case WM_HELP:
      amp_show_help( IDH_PL );
      return 0;

    case WM_SYSCOMMAND:
      if( SHORT1FROMMP( mp1 ) == SC_CLOSE ) {
        pl_show( FALSE );
        return 0;
      }
      break;

    case WM_WINDOWPOSCHANGED:
    {
      SWP* pswp = PVOIDFROMMP(mp1);

      if( pswp[0].fl & SWP_SHOW ) {
        cfg.show_playlist = TRUE;
      }
      if( pswp[0].fl & SWP_HIDE ) {
        cfg.show_playlist = FALSE;
      }
      break;
    }

    case DM_DISCARDOBJECT:
      return pl_m_drag_discard( hwnd, (PDRAGINFO)mp1 );
    case DM_RENDERCOMPLETE:
      return pl_m_drag_render_done( hwnd, (PDRAGTRANSFER)mp1, SHORT1FROMMP( mp2 ));

    case WM_123FILE_REMOVE:
    {
      PLRECORD* rec = (PLRECORD*)mp1;
      pl_m_remove_records( &rec, 1, 0 );
      return 0;
    }

    case WM_123FILE_LOAD:
      // This message sends as result of operation DM_DROP, and never sends
      // in case the player is busy.
      pl_m_play_record((PLRECORD*)mp1 );
      return 0;

    case WM_123FILE_PATHNAME:
      strlcpy( mp2, ((PLRECORD*)mp1)->full, _MAX_URL );
      return MRFROMLONG( TRUE );

    case WM_COMMAND:
      if( !is_busy() && COMMANDMSG(&msg)->cmd >  IDM_PL_LAST &&
                        COMMANDMSG(&msg)->cmd <= IDM_PL_LAST + MAX_RECALL )
      {
        char filename[_MAX_URL];
        strcpy( filename, cfg.last_list[ COMMANDMSG(&msg)->cmd - IDM_PL_LAST - 1 ]);

        if( is_playlist( filename )) {
          pl_load( filename, PL_LOAD_CLEAR );
        }
        return 0;
      }

      switch( COMMANDMSG(&msg)->cmd ) {
        case IDM_PL_SORT_RAND:
          pl_m_sort( cfg.sort = PL_SORT_RAND );
          return 0;
        case IDM_PL_SORT_SIZE:
          pl_m_sort( cfg.sort = PL_SORT_SIZE );
          return 0;
        case IDM_PL_SORT_TIME:
          pl_m_sort( cfg.sort = PL_SORT_TIME );
          return 0;
        case IDM_PL_SORT_FILE:
          pl_m_sort( cfg.sort = PL_SORT_FILE );
          return 0;
        case IDM_PL_SORT_SONG:
          pl_m_sort( cfg.sort = PL_SORT_SONG );
          return 0;
        case IDM_PL_SORT_TRCK:
          pl_m_sort( cfg.sort = PL_SORT_TRCK );
          return 0;
        case IDM_PL_SORT_KEEP:
          cfg.sort_keep = !cfg.sort_keep;
          pl_m_sort( cfg.sort );
          return 0;

        case IDM_PL_DEAD_REMOVE:
          if( !is_busy()) {
            pl_m_remove_records( NULL, 0, PL_REMOVE_DEAD );
          }
          return 0;

        case IDM_PL_DUPLICATE_REMOVE:
          if( !is_busy()) {
            pl_m_remove_records( NULL, 0, PL_REMOVE_DUPLICATE );
          }
          return 0;

        case IDM_PL_CLEAR:
          if( !is_busy()) {
            pl_clear();
          }
          return 0;

        case IDM_PL_USE:
          if( !is_busy()) {
            amp_pl_use(!( amp_playmode == AMP_PLAYLIST ));
          }
          return 0;

        case IDM_PL_ADD_URL:
          amp_load_url( hwnd, URL_ADD_TO_LIST );
          return 0;
        case IDM_PL_ADD_TRACKS:
          amp_load_track( hwnd, TRK_ADD_TO_LIST );
          return 0;
        case IDM_PL_ADD_FILES:
          pl_dlg_add_files( hwnd );
          return 0;
        case IDM_PL_SAVE_LIST:
          pl_dlg_save_list( hwnd );
          return 0;
        case IDM_PL_OPEN_LIST:
          if( !is_busy()) {
            pl_dlg_load_list( hwnd );
          }
          return 0;

        case IDM_RC_PLAY:
          if( !is_busy()) {
            pl_m_play_record( pl_m_cursored());
          }
          return 0;

        case IDM_RC_OPEN_FOLDER:
        {
          PLRECORD* rec = pl_m_cursored();
          if( rec ) {
            amp_open_containing_folder( rec->full );
          }
          return 0;
        }

        case IDM_RC_REMOVE:
          if( !is_busy()) {
            pl_m_remove_records( NULL, 0, PL_REMOVE_SELECTED );
          }
          return 0;

        case IDM_RC_DELETE:
          if( !is_busy()) {
            pl_m_delete_selected();
          }
          return 0;

        case IDM_PL_CLOSE:
          pl_show( FALSE );
          return 0;

        case IDM_PL_LAST_CLEAR:
        {
          int i;
          for( i = 0; i < MAX_RECALL; i++ ) {
            cfg.last_list[i][0] = 0;
          }
          return 0;
        }

        case IDM_RC_EDIT:
          if( !is_busy())
          {
            PLRECORD* rec;
            int count = 0;

            for( rec = pl_m_first_selected(); rec; rec = pl_m_next_selected( rec )) {
              ++count;
            }

            if( count )
            {
              AMP_FILE* tags = malloc( sizeof( AMP_FILE ) * count );
              int i;

              if( !tags ) {
                amp_show_error( "Not enough memory." );
              } else {
                for( rec = pl_m_first_selected(), i = 0; rec; rec = pl_m_next_selected( rec ), i++ ) {
                  strlcpy( tags[i].filename, rec->full,    sizeof( tags[i].filename ));
                  strlcpy( tags[i].decoder,  rec->decoder, sizeof( tags[i].decoder  ));
                  tags[i].info = rec->info;
                }

                tag_edit( hwnd, tags, count );
                free( tags );
              }
            }
          }
          return 0;
      }
      break;

    case WM_CONTROL:
      switch( SHORT2FROMMP( mp1 )) {
        case CN_CONTEXTMENU:
          pl_m_show_context_menu( hwnd, (PLRECORD*)mp2 );
          return 0;

        case CN_HELP:
          amp_show_help( IDH_PL );
          return 0;

        case CN_ENTER:
        {
          NOTIFYRECORDENTER* notify = (NOTIFYRECORDENTER*)mp2;
          if( !is_busy() && notify->pRecord ) {
            pl_m_play_record((PLRECORD*)notify->pRecord );
          }
          return 0;
        }

        case CN_INITDRAG:
          return pl_m_drag_init( hwnd, (PCNRDRAGINIT)mp2 );
        case CN_DRAGAFTER:
          return pl_m_drag_over( hwnd, (PCNRDRAGINFO)mp2 );
        case CN_DROP:
          return pl_m_drag_drop( hwnd, (PCNRDRAGINFO)mp2 );
      }
      break;

    case WM_TRANSLATEACCEL:
      if( hk_translate( HKW_PLAYLIST, mp1, mp2 )) {
        return MRFROMLONG( TRUE );
      }
      break;

    case PL_MSG_MARK_AS:
      pl_m_mark_as( LONGFROMMP( mp1 ));
      return 0;
    case PL_MSG_REMOVE_ALL:
      pl_m_remove_all();
      return 0;
    case PL_MSG_REMOVE_RECORD:
      pl_m_remove_records( NULL, 0, LONGFROMMP( mp1 ));
      return 0;
    case PL_MSG_REFRESH_FILE:
      pl_m_refresh_file( mp1, mp2 );
      return 0;
    case PL_MSG_REFRESH_SONGNAME:
      pl_m_refresh_songname( mp1, mp2 );
      return 0;
    case PL_MSG_REFRESH_STATUS:
      pl_m_refresh_status( mp1 );
      return 0;
    case PL_MSG_SELECT_BY_INDEX:
      pl_m_select( pl_m_record_by_index( LONGFROMMP( mp1 )));
      return 0;
    case PL_MSG_CLEAN_SHUFFLE:
      pl_m_clean_shuffle();
      return 0;
    case PL_MSG_INSERT_RECALL:
      pl_m_insert_to_recall_list( mp1 );
      return 0;
    case PL_MSG_PLAY_TIME:
      *((LONGLONG*)mp1) = pl_m_playtime();
      return 0;
    case PL_MSG_PLAYED:
      *((LONGLONG*)mp1) = pl_m_played();
      return 0;

    case PL_MSG_LOAD_FRST_RECORD:
      return MRFROMLONG( pl_m_load_record( pl_m_query_first_record()));
    case PL_MSG_LOAD_NEXT_RECORD:
      return MRFROMLONG( pl_m_load_record( pl_m_query_next_record()));
    case PL_MSG_LOAD_PREV_RECORD:
      return MRFROMLONG( pl_m_load_record( pl_m_query_prev_record()));
    case PL_MSG_LOAD_FILE_RECORD:
      return MRFROMLONG( pl_m_load_record( pl_m_query_file_record( mp1 )));
    case PL_MSG_LOAD_BY_INDEX:
      return MRFROMLONG( pl_m_load_record( pl_m_record_by_index( LONGFROMMP( mp1 ))));
    case PL_MSG_PLAY_LEFT:
      return MRFROMLONG( pl_m_playleft());
    case PL_MSG_INSERT_RECORD:
      return MRFROMLONG( pl_m_insert_record( LONGFROMMP( mp1 ), mp2 ));
    case PL_MSG_LOADED_INDEX:
      return MRFROMLONG( pl_m_index_of_record( loaded_record ));
    case PL_MSG_SAVE_BUNDLE:
      return MRFROMLONG( pl_m_save_bundle( mp1, LONGFROMMP( mp2 )));
    case PL_MSG_SAVE_PLAYLIST:
      return MRFROMLONG( pl_m_save_list( mp1, LONGFROMMP( mp2 )));
  }

  return bmp_frame_wnd_proc( hwnd, msg, mp1, mp2 );
}

/* Sets the visibility state of the playlist presentation window. */
void
pl_show( BOOL show )
{
  if(!( dk_get_state( playlist ) & DK_IS_DOCKED ))
  {
    HSWITCH hswitch = WinQuerySwitchHandle( playlist, 0 );
    SWCNTRL swcntrl;

    if( WinQuerySwitchEntry( hswitch, &swcntrl ) == 0 ) {
      if( show && !swcntrl.hwndIcon ) {
        swcntrl.hwndIcon = (HPOINTER)WinSendMsg( playlist, WM_QUERYICON, 0, 0 );
      }
      swcntrl.uchVisibility = show ? SWL_VISIBLE : SWL_INVISIBLE;
      WinChangeSwitchEntry( hswitch, &swcntrl );
    }
  }

  dk_set_state( playlist, show ? 0 : DK_IS_GHOST );

  if( show ) {
    WinSetWindowPos( playlist, HWND_TOP, 0, 0, 0, 0,
                     SWP_SHOW | SWP_ZORDER | SWP_ACTIVATE );
  } else {
    WinSendMsg( playlist, WM_SYSCOMMAND,
                MPFROMSHORT( SC_HIDE ), MPFROM2SHORT( CMDSRC_OTHER, FALSE ));
  }
}

/* Returns the visibility state of the playlist presentation window. */
BOOL
pl_is_visible( void ) {
  return WinIsWindowVisible( playlist );
}

/* Changes the playlist colors. */
BOOL
pl_set_colors( ULONG fgcolor, ULONG bgcolor,
               ULONG hi_fgcolor, ULONG hi_bgcolor,
               ULONG sb_fgcolor, ULONG sb_bgcolor )
{
  RGB  rgb;
  HWND vscroll = WinWindowFromID( container, CID_VSCROLL );
  HWND righths = WinWindowFromID( container, CID_RIGHTHSCROLL );
  HWND hscroll = WinWindowFromID( container, CID_HSCROLL );

  if( sb_bgcolor == 0xFFFFFFFFUL ) {
    sb_bgcolor = bgcolor;
  }
  if( sb_fgcolor == 0xFFFFFFFFUL ) {
    sb_fgcolor = fgcolor;
  }

  if( fgcolor != 0xFFFFFFFFUL ) {
    rgb = ultorgb( fgcolor );
    WinSetPresParam( container, PP_FOREGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( container, PP_BORDERCOLOR, sizeof(rgb), &rgb );
  }
  if( bgcolor != 0xFFFFFFFFUL ) {
    rgb = ultorgb( bgcolor );
    WinSetPresParam( container, PP_BACKGROUNDCOLOR, sizeof(rgb), &rgb );
  }
  if( hi_fgcolor != 0xFFFFFFFFUL ) {
    rgb = ultorgb( hi_fgcolor );
    WinSetPresParam( container, PP_HILITEFOREGROUNDCOLOR, sizeof(rgb), &rgb );
  }
  if( hi_bgcolor != 0xFFFFFFFFUL ) {
    rgb = ultorgb( hi_bgcolor );
    WinSetPresParam( container, PP_HILITEBACKGROUNDCOLOR, sizeof(rgb), &rgb );
  }
  if( sb_bgcolor != 0xFFFFFFFFUL ) {
    rgb = ultorgb( sb_bgcolor );

    WinSetPresParam( container, PP_FIELDBACKGROUNDCOLOR, sizeof(rgb), &rgb );

    // Sets colors of the scroll bar backgrounds and buttons.
    WinSetPresParam( hscroll, PP_BACKGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( hscroll, PP_FOREGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( hscroll, PP_HILITEFOREGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_BACKGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_FOREGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_HILITEFOREGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_BACKGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_FOREGROUNDCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_HILITEFOREGROUNDCOLOR, sizeof(rgb), &rgb );

    rgb = rgb_lighten( ultorgb( sb_bgcolor ), 60 );
    WinSetPresParam( container, PP_BORDERCOLOR, sizeof(rgb), &rgb );

    // Sets colors of the scroll bar 3d light colors.
    WinSetPresParam( hscroll, PP_BORDERLIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( hscroll, PP_BUTTONBORDERLIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( hscroll, PP_BORDER2LIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_BORDER2LIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_BORDERLIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_BUTTONBORDERLIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_BORDER2LIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_BORDERLIGHTCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_BUTTONBORDERLIGHTCOLOR, sizeof(rgb), &rgb );

    rgb = rgb_lighten( ultorgb( sb_bgcolor ), -60 );

    // Sets colors of the scroll bar 3d dark colors.
    WinSetPresParam( hscroll, PP_BORDERDARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( hscroll, PP_BUTTONBORDERDARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( hscroll, PP_BORDER2DARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_BORDERDARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_BUTTONBORDERDARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_BORDER2DARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_BORDERDARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_BUTTONBORDERDARKCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_BORDER2DARKCOLOR, sizeof(rgb), &rgb );
  }
  if( sb_fgcolor != 0xFFFFFFFFUL ) {
    rgb = ultorgb( sb_fgcolor );

    // Sets colors of the scroll bar handle and edge arrows.
    WinSetPresParam( hscroll, PP_ARROWCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( vscroll, PP_ARROWCOLOR, sizeof(rgb), &rgb );
    WinSetPresParam( righths, PP_ARROWCOLOR, sizeof(rgb), &rgb );
  }

  WinSendMsg( playlist, WM_SKIN_CHANGED, 0, 0 );
  return TRUE;
}

/* Creates the playlist presentation window. Must be called
   from the main thread. */
HWND
pl_create( void )
{
  ASSERT_IS_MAIN_THREAD;
  playlist = WinLoadDlg( HWND_DESKTOP, HWND_DESKTOP,
                         pl_m_dlg_proc, hmodule, DLG_PLAYLIST, NULL );
  // Because the title text is not set during creating of the skinned
  // window this must be set manually here.
  WinSetWindowText( playlist, "PM123 Playlist" );

  pl_show( cfg.show_playlist );
  hk_register( HKW_PLAYLIST, playlist );
  return playlist;
}

/* Destroys the playlist presentation window. Must be called
   from the main thread. */
void
pl_destroy( void )
{
  CNRINFO info;
  ASSERT_IS_MAIN_THREAD;

  pl_m_purge_queue( broker_queue );
  qu_push( broker_queue, PL_TERMINATE,
           pl_m_create_request_data( NULL, NULL, INDEX_NONE, 0 ));

  wait_thread( broker_tid, 2000 );
  qu_close( broker_queue );
  save_window_pos( playlist, 0 );

  if( WinSendMsg( container, CM_QUERYCNRINFO,
                  MPFROMP(&info), MPFROMLONG(sizeof(info))) != 0 )
  {
    cfg.sbar_playlist = info.xVertSplitbar;
  }

  pl_m_remove_all();
  WinDestroyWindow( menu_record   );
  WinDestroyWindow( menu_playlist );
  WinDestroyWindow( playlist      );
}

/* Sends request about clearing of the playlist. */
BOOL
pl_clear()
{
  pl_m_purge_queue( broker_queue );
  WinSendMsg( playlist, PL_MSG_REFRESH_STATUS, "", 0 );
  return qu_push( broker_queue, PL_CLEAR,
                  pl_m_create_request_data( NULL, NULL, INDEX_NONE, 0 ));
}

/* Sends request about addition of the whole directory to the playlist. */
BOOL
pl_add_directory( const char* path, int options )
{
  return qu_write( broker_queue, PL_ADD_DIRECTORY,
                   pl_m_create_request_data( path, NULL, INDEX_END, options ));
}

/* Sends request about addition of the file to the playlist. */
BOOL
pl_add_file( const char* filename, const char* songname, int options )
{
  return qu_write( broker_queue, PL_ADD_FILE,
                   pl_m_create_request_data( filename, songname, INDEX_END, options ));
}

/* Notifies on completion of the playlist */
BOOL
pl_completed( void )
{
  return qu_write( broker_queue, PL_COMPLETED,
                   pl_m_create_request_data( NULL, NULL, INDEX_NONE, 0 ));
}

/* Returns true if the specified file is a playlist file. */
BOOL
is_playlist( const char *filename )
{
 char ext[_MAX_EXT];
 sfext( ext, filename, sizeof( ext ));
 return ( !is_cuesheet( filename ) && (
          stricmp( ext, ".lst"  ) == 0 ||
          stricmp( ext, ".mpl"  ) == 0 ||
          stricmp( ext, ".pls"  ) == 0 ||
          stricmp( ext, ".m3u"  ) == 0 ||
          stricmp( ext, ".m3u8" ) == 0 ||
          stricmp( ext, ".cue"  ) == 0 ));
}

/* Loads the PM123 native playlist file. */
static BOOL
pl_load_lst_list( const char* filename, XFILE* playlist, int options, HPOPULATE hp )
{
  char basepath[_MAX_URL];
  char fullname[_MAX_URL] = "";
  char file    [_MAX_URL];
  BOOL end_pm = FALSE;

  int     bitrate    = -1;
  int     samplerate = -1;
  int     mode       = -1;
  int64_t filesize   = -1;
  int     secs       = -1;

  sdrivedir( basepath, filename, sizeof( basepath ));

  if( options & PL_LOAD_TO_PM && !hp ) {
    if(!( hp = pm_begin_populate( filename ))) {
      return FALSE;
    }
    end_pm = TRUE;
  }

  while( xio_fgets( file, sizeof(file), playlist ))
  {
    blank_strip( file );

    if( *file == '>' ) {
      sscanf( file, ">%d,%d,%d,%lld,%d\n", &bitrate, &samplerate, &mode, &filesize, &secs );
    } else if( *file != 0 && *file != '#' && *file != '<' ) {
      if( *fullname ) {
        if( is_playlist( fullname )) {
          pl_load_any_list( fullname, options | PL_LOAD_NOT_COMPLETE | PL_LOAD_NOT_RECALL, hp );
        } else {
          if( options & PL_LOAD_TO_PM ) {
            if( !pm_add_file( hp, fullname, bitrate, samplerate, mode, filesize, secs )) {
              break;
            } else {
              bitrate    = -1;
              samplerate = -1;
              mode       = -1;
              filesize   = -1;
              secs       = -1;
            }
          } else {
            pl_add_file( fullname, NULL, 0 );
          }
        }
      }
      rel2abs( basepath, file, fullname, sizeof( fullname ));
    }
  }

  if( xio_ferror( playlist )) {
    amp_show_error( "%s", xio_strerror( xio_errno()));
  }

  if( *fullname ) {
    if( is_playlist( fullname )) {
      pl_load_any_list( fullname, options | PL_LOAD_NOT_COMPLETE | PL_LOAD_NOT_RECALL, hp );
    } else {
      if( options & PL_LOAD_TO_PM ) {
        pm_add_file( hp, fullname, bitrate, samplerate, mode, filesize, secs );
      } else {
        pl_add_file( fullname, NULL, 0 );
      }
    }
  }

  if( end_pm ) {
    pm_end_populate( hp );
  }
  return TRUE;
}

/* Loads the M3U playlist file. */
static BOOL
pl_load_m3u_list( const char* filename, XFILE* playlist, int options, HPOPULATE hp )
{
  char basepath[_MAX_URL];
  char fullname[_MAX_URL];
  char file    [_MAX_URL];
  int  secs;
  BOOL end_pm = FALSE;

  sdrivedir( basepath, filename, sizeof( basepath ));

  if( options & PL_LOAD_TO_PM && !hp ) {
    if(!( hp = pm_begin_populate( filename ))) {
      return FALSE;
    }
    end_pm = TRUE;
  }

  if( xio_fgets( file, sizeof(file), playlist )) {
    if( strncmp( file, "\xEF\xBB\xBF", 3 ) == 0 ) {
      options |= PL_LOAD_UTF8;
      memmove( file, file + 3, strlen( file ) + 1 - 3 );
    }
  }

  do
  {
    blank_strip( file );

    if( *file == '#' ) {
      if( strnicmp( file, "#EXTINF:", 8 ) == 0 ) {
        sscanf( file, "#EXTINF:%d,%*s", &secs );
      }
    } else if( *file != 0 ) {
      if( options & PL_LOAD_UTF8 ) {
        ch_convert( CH_UTF_8, file, CH_DEFAULT, file, sizeof( file ));
      }
      if( rel2abs( basepath, file, fullname, sizeof(fullname))) {
        if( is_playlist( fullname )) {
          pl_load_any_list( fullname, options | PL_LOAD_NOT_COMPLETE | PL_LOAD_NOT_RECALL, hp );
        } else {
          if( options & PL_LOAD_TO_PM ) {
            if( !pm_add_file( hp, fullname, -1, -1, -1, -1, secs )) {
              break;
            }
            secs = -1;
          } else {
            pl_add_file( fullname, NULL, 0 );
          }
        }
      }
    }
  } while( xio_fgets( file, sizeof(file), playlist ));

  if( xio_ferror( playlist )) {
    amp_show_error( "%s", xio_strerror( xio_errno()));
  }

  if( end_pm ) {
    pm_end_populate( hp );
  }
  return TRUE;
}

/* Loads the WinAMP playlist file. */
static BOOL
pl_load_pls_list( const char* filename, XFILE* playlist, int options, HPOPULATE hp )
{
  char  basepath[_MAX_URL];
  char  fullname[_MAX_URL] = "";
  char  file    [_MAX_URL] = "";
  char  title   [_MAX_URL] = "";
  char  line    [_MAX_URL];
  char* eq_pos;
  int   secs   = -1;
  int   id     = -1;
  BOOL  end_pm = FALSE;

  sdrivedir( basepath, filename, sizeof( basepath ));

  if( options & PL_LOAD_TO_PM && !hp ) {
    if(!( hp = pm_begin_populate( filename ))) {
      return FALSE;
    }
    end_pm = TRUE;
  }

  while( xio_fgets( line, sizeof(line), playlist ))
  {
    blank_strip( line );

    if( *line != 0 && *line != '#' && *line != '[' && *line != '>' && *line != '<' )
    {
      eq_pos = strchr( line, '=' );

      if( eq_pos ) {
        if( strnicmp( line, "File", 4 ) == 0 )
        {
          if( *fullname ) {
            if( is_playlist( fullname )) {
              pl_load_any_list( fullname, options | PL_LOAD_NOT_COMPLETE | PL_LOAD_NOT_RECALL, hp );
            } else {
              if( options & PL_LOAD_TO_PM ) {
                if( !pm_add_file( hp, fullname, -1, -1, -1, -1, secs )) {
                  return FALSE;
                }
              } else {
                pl_add_file( fullname, *title ? title : NULL, 0 );
              }
            }
           *title =  0;
            secs  = -1;
          }

          strlcpy( file, eq_pos + 1, sizeof(file));
          rel2abs( basepath, file, fullname, sizeof(fullname));
          id = atoi( &line[4] );
        }
        else if( strnicmp( line, "Title", 5 ) == 0 )
        {
          // We hope the title field always follows the file field.
          if( id == atoi( &line[5] )) {
            strlcpy( title, eq_pos + 1, sizeof(title));
          }
        }
        else if( strnicmp( line, "Length", 6 ) == 0 )
        {
          // We hope the length field always follows the file field.
          if( id == atoi( &line[6] )) {
            secs = atoi( eq_pos + 1 );
          }
        }
      }
    }
  }

  if( *fullname ) {
    if( is_playlist( fullname )) {
      pl_load_any_list( fullname, options | PL_LOAD_NOT_COMPLETE | PL_LOAD_NOT_RECALL, hp );
    } else {
      if( options & PL_LOAD_TO_PM ) {
        pm_add_file( hp, fullname, -1, -1, -1, -1, secs );
      } else {
        pl_add_file( fullname, *title ? title : NULL, 0 );
      }
    }
  }

  if( end_pm ) {
    pm_end_populate( hp );
  }
  return TRUE;
}

/* Loads the cue sheet file. */
static BOOL
pl_load_cue_list( const char* filename, XFILE* playlist, int options, HPOPULATE hp )
{
  char line[_MAX_URL+50];
  BOOL end_pm = FALSE;

  if( options & PL_LOAD_TO_PM && !hp ) {
    if(!( hp = pm_begin_populate( filename ))) {
      return FALSE;
    }
    end_pm = TRUE;
  }

  while( xio_fgets( line, sizeof(line), playlist )) {
    if( strnicmp( blank_strip( line ), "TRACK ", 6 ) == 0 )
    {
      char* type;
      int   track = strtol( blank_strip( line + 6 ), &type, 10 );
      char  cuename[_MAX_URL+10];

      if( !*type || stricmp( blank_strip( type ), "AUDIO" ) == 0 ) {
        snprintf( cuename, sizeof(cuename), "cue:///%s,%02u", filename, track );

        if( options & PL_LOAD_TO_PM ) {
          pm_add_file( hp, cuename, -1, -1, -1, -1, -1 );
        } else {
          pl_add_file( cuename, NULL, 0 );
        }
      }
    }
  }

  if( end_pm ) {
    pm_end_populate( hp );
  }

  return TRUE;
}

/* Loads the WarpAMP playlist file. */
static BOOL
pl_load_mpl_list( const char* filename, XFILE* playlist, int options, HPOPULATE hp ) {
  return pl_load_pls_list( filename, playlist, options, hp );
}

/* Appends the specified playlist to recall list. */
static void
pl_m_insert_to_recall_list( const char* filename )
{
  int i;
  ASSERT_IS_MAIN_THREAD;

  for( i = 0; i < MAX_RECALL; i++ ) {
    if( nlstricmp( cfg.last_list[i], filename ) == 0 ) {
      while( ++i < MAX_RECALL ) {
        strcpy( cfg.last_list[i-1], cfg.last_list[i] );
      }
      break;
    }
  }

  for( i = MAX_RECALL - 2; i >= 0; i-- ) {
    strcpy( cfg.last_list[i + 1], cfg.last_list[i] );
  }

  strcpy( cfg.last_list[0], filename );
}

/* Loads the specified playlist file. */
static BOOL
pl_load_any_list( const char* filename, int options, HPOPULATE hp )
{
  XFILE* file;
  BOOL   rc = FALSE;
  char   ext[_MAX_EXT];

  if( !( options & PL_LOAD_TO_PM )) {
    if( !pl_busy ) {
      pl_busy = TRUE;
      pl_refresh_status();
    }
  }

  if(!( file = xio_fopen( filename, "r" ))) {
    if(!( options & PL_LOAD_TO_PM )) {
      amp_show_error( "Unable open playlist file:\n%s\n%s",
                      filename, xio_strerror( xio_errno()));
    }
    return FALSE;
  }

  sfext( ext, filename, sizeof( ext ));

  if( stricmp( ext, ".lst" ) == 0 ) {
    rc = pl_load_lst_list( filename, file, options, hp );
  } else if( stricmp( ext, ".m3u"  ) == 0 ) {
    rc = pl_load_m3u_list( filename, file, options, hp );
  } else if( stricmp( ext, ".m3u8" ) == 0 ) {
    rc = pl_load_m3u_list( filename, file, options | PL_LOAD_UTF8, hp );
  } else if( stricmp( ext, ".mpl"  ) == 0 ) {
    rc = pl_load_mpl_list( filename, file, options, hp );
  } else if( stricmp( ext, ".pls"  ) == 0 ) {
    rc = pl_load_pls_list( filename, file, options, hp );
  } else if( stricmp( ext, ".cue"  ) == 0 ) {
    rc = pl_load_cue_list( filename, file, options, hp );
  } else {
    rc = pl_load_m3u_list( filename, file, options, hp );
  }

  if( rc && !( options & PL_LOAD_TO_PM )) {
    if(!( options & PL_LOAD_NOT_COMPLETE )) {
      pl_completed();
      WinSendMsg( playlist, PL_MSG_REFRESH_STATUS, MPFROMP( filename ), 0 );
    }
    if(!( options & PL_LOAD_NOT_RECALL )) {
      WinSendMsg( playlist, PL_MSG_INSERT_RECALL,  MPFROMP( filename ), 0 );
    }
  }

  xio_fclose( file );
  return rc;
}

/* Sends request about loading the specified playlist file. */
BOOL
pl_load( const char* filename, int options )
{
  if( options & PL_LOAD_CLEAR ) {
    pl_clear();
  }

  if( options & PL_LOAD_TO_PM ) {
    return pl_load_any_list( filename, options, NULL );
  } else {
    return qu_write( broker_queue, PL_LOAD_PLAYLIST,
                     pl_m_create_request_data( filename, NULL, INDEX_NONE, options ));
  }
}

/* Saves playlist to the specified file. */
static BOOL
pl_m_save_list( const char* filename, int options )
{
  PLRECORD* rec;
  FILE*     file;
  char      base[_MAX_URL];
  char      path[_MAX_URL];
  char      line[_MAX_URL];

  ASSERT_IS_MAIN_THREAD;

  if(!( file = fopen( filename, "w" ))) {
    amp_show_error( "Unable open playlist file:\n%s\n%s", filename, strerror( errno ));
    return FALSE;
  }

  if( options & PL_SAVE_M3U ) {
    if( options & PL_SAVE_UTF8 ) {
      fprintf( file, "\xEF\xBB\xBF#EXTM3U\n" );
    } else {
      fprintf( file, "#EXTM3U\n" );
    }
  } else {
    fprintf( file, "#\n"
                   "# Playlist created with %s\n"
                   "# Do not modify!\n"
                   "# Lines starting with '>' are used by Playlist Manager.\n"
                   "#\n", AMP_FULLNAME );
  }

  sdrivedir( base, filename, sizeof( base ));
  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec ))
  {
    if( options & PL_SAVE_M3U ) {
      if( options & PL_SAVE_UTF8 ) {
        ch_convert( CH_DEFAULT, rec->songname, CH_UTF_8, line, sizeof( line ));
      } else {
        strlcpy( line, rec->songname, sizeof( line ));
      }
      fprintf( file, "#EXTINF:%d,%s\n", rec->info.songlength / 1000, line  );
    } else {
      fprintf( file, "# %s, %s, %s\n", rec->size, rec->time, rec->moreinfo );
    }

    if( options & PL_SAVE_RELATIVE
        && is_file( rec->full )
        && abs2rel( base, rec->full, path, sizeof(path)))
    {
      strlcpy( line, path, sizeof( line ));
    } else {
      strlcpy( line, rec->full, sizeof( line ));
    }

    if( options & PL_SAVE_UTF8 ) {
      ch_convert( CH_DEFAULT, line, CH_UTF_8, line, sizeof( line ));
    }

    fprintf( file, "%s\n", line );

    if( !(options & PL_SAVE_M3U )) {
      fprintf( file, ">%d,%ld,%d,%lld,%d\n",
               rec->info.bitrate, rec->info.format.samplerate,
               rec->info.mode, rec->info.filesize, rec->info.songlength / 1000 );
    }
  }

  if( !(options & PL_SAVE_M3U )) {
    fprintf( file, "# End of playlist\n" );
  }

  fclose( file );
  WinSendMsg( playlist, PL_MSG_REFRESH_STATUS, MPFROMP( filename ), 0 );
  return TRUE;
}

/* Saves playlist to the specified file. */
BOOL
pl_save( const char* filename, int options ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_SAVE_PLAYLIST,
                                 MPFROMP( filename ), MPFROMLONG( options )));
}

/* Saves the playlist and player status to the specified file. */
static BOOL
pl_m_save_bundle( const char* filename, int options )
{
  FILE*     file;
  PLRECORD* rec;

  ASSERT_IS_MAIN_THREAD;

  if(!( file = fopen( filename, "w" ))) {
    amp_show_error( "Unable create status file:\n%s\n%s", filename, strerror(errno));
    return FALSE;
  }

  fprintf( file, "#\n"
                 "# Player state file created with %s\n"
                 "# Do not modify! This file is compatible with the playlist format,\n"
                 "# but information written in this file is different.\n"
                 "#\n", AMP_FULLNAME );

  for( rec = pl_m_first_record(); rec; rec = pl_m_next_record( rec ))
  {
    fprintf( file, ">%u,%u\n",
             rec->rc.flRecordAttr & CRA_CURSORED ? 1 : 0,
             rec == loaded_record && amp_playmode == AMP_PLAYLIST );
    fprintf( file, "%s\n" , rec->full );
  }

  if(( amp_playmode == AMP_SINGLE && *current_filename ) &&
     ( is_file( current_filename )))
  {
    fprintf( file, "<%s\n", current_filename );
  }

  fprintf( file, "# End of playlist\n" );
  fclose ( file );
  return TRUE;
}

/* Saves the playlist and player status to the specified file. */
BOOL
pl_save_bundle( const char* filename, int options ) {
  return LONGFROMMR( WinSendMsg( playlist, PL_MSG_SAVE_BUNDLE,
                                 MPFROMP( filename ), MPFROMLONG( options )));
}

/* Loads the playlist and player status from specified file. */
BOOL
pl_load_bundle( const char *filename, int options )
{
  char  file[_MAX_URL];
  BOOL  select   = FALSE;
  BOOL  loaded   = FALSE;
  FILE* playlist = fopen( filename, "r" );
  ULONG ordinal  = 0;

  if( !playlist ) {
    amp_show_error( "Unable open status file:\n%s\n%s", filename, strerror( errno ));
    return FALSE;
  }

  pl_clear();

  while( fgets( file, sizeof(file), playlist ))
  {
    blank_strip( file );

    if( *file == '<' )
    {
      char   name[_MAX_URL];
      char*  p;
      struct stat fi;

      if( is_cuesheet( file + 1 )) {
        strlcpy( name, file + 8, sizeof( name ));
        if(( p = strrchr( name, ',' )) != NULL ) {
          *p = 0;
        }
      } else {
        strlcpy( name, file + 1, sizeof( name ));
      }

      if( stat( name, &fi ) == 0 && !decoder_playing()) {
        amp_load_singlefile( file + 1, AMP_LOAD_NOT_PLAY | AMP_LOAD_NOT_RECALL );
      }
    } else if( *file == '>' ) {
      sscanf( file, ">%lu,%lu\n", &select, &loaded );
    } else if( *file != 0 && *file != '#' ) {
      ordinal++;
      pl_add_file( file, NULL, 0 );

      if( select ) {
        select = FALSE;
        qu_write( broker_queue, PL_SELECT_BY_INDEX,
                  pl_m_create_request_data( NULL, NULL, ordinal, 0 ));
      }
      if( loaded ) {
        loaded = FALSE;
        qu_write( broker_queue, PL_RELOAD_BY_INDEX,
                  pl_m_create_request_data( NULL, NULL, ordinal, 0 ));
      }
    }
  }
  fclose( playlist );
  return TRUE;
}

