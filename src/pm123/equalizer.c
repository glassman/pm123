/*
 * Copyright 2011-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Based on "Simple implementation of Biquad filters" of Tom St Denis and
 * on "Cookbook formulae for audio EQ biquad filter coefficients" of
 * Robert Bristow-Johnson
 *
 * http://www.musicdsp.org/files/Audio-EQ-Cookbook.txt
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>

#include <filter_plug.h>
#include <debuglog.h>

#include "equalizer.h"
#include "filedlg.h"
#include "pm123.h"
#include "assertions.h"
#include "skin.h"
#include "iniman.h"
#include "hotkeys.h"

static HWND heq = NULLHANDLE;
static FORMAT_INFO original_format;
static biquad* filters;
static BOOL illegal_format = FALSE;

static const float freqs[EQ_BANDS] = {
  31.25, 62.5, 125, 250, 500, 1000, 2000, 4000, 8000, 16000
};

static  ULONG fg = DEF_FG_COLOR, bg = DEF_BG_COLOR;

static  BOOL gains_changed = TRUE;
#define EQ_REFRESH_CONTROLS  ( WM_USER + 1000 )
#define EQ_REFRESH_COLORS    ( WM_USER + 1001 )
#define EQ_ILLEGAL_FORMAT    ( WM_USER + 1002 )

#define EQ_8U_MAX 127.0
#define EQ_16_MAX 32767.0
#define EQ_24_MAX 8388607.0
#define EQ_32_MAX 2147483647.0

/* Computes a BiQuad filter on a sample */
static INLINE smp_type
eq_biquad( smp_type sample, biquad* b )
{
  if( !b->skip )
  {
    smp_type result;

    // Compute result.
    result = b->a0 * sample + b->a1 * b->x1 + b->a2 * b->x2 -
             b->a3 * b->y1 - b->a4 * b->y2;

    // Shift x1 to x2, sample to x1.
    b->x2 = b->x1;
    b->x1 = sample;

    // Shift y1 to y2, result to y1.
    b->y2 = b->y1;
    b->y1 = result;

    return result;
  } else {
    return sample;
  }
}

/* Sets up a BiQuad filter. */
static void
eq_biquad_init( biquad* b, smp_type gain,     /* Gain of filter       */
                           smp_type freq,     /* Center frequency     */
                           smp_type srate,    /* Sampling rate        */
                           smp_type bandwidth /* Bandwidth in octaves */ )
{
  smp_type A, omega, sn, cs, alpha;
  smp_type a0, a1, a2, b0, b1, b2;
  smp_type alpha_m_A, alpha_d_A;

  if( freq < srate / 2 )
  {
    // Setup variables.
    A = sqrt( gain );
    b->gain = gain;
    omega = 2 * M_PI * freq / srate;
    sn = sin( omega );
    cs = cos( omega );
    alpha = sn * sinh( M_LN2 / 2 * bandwidth * omega / sn );

    alpha_m_A = alpha * A;
    alpha_d_A = alpha / A;

    b0 =  1 + alpha_m_A;
    b1 = -2 * cs;
    b2 =  1 - alpha_m_A;
    a0 =  1 + alpha_d_A;
    a1 =  b1;
    a2 =  1 - alpha_d_A;

    // Precompute the coefficients.
    b->a0 = b0 / a0;
    b->a1 = b1 / a0;
    b->a2 = b2 / a0;
    b->a3 = a1 / a0;
    b->a4 = a2 / a0;

    // Zero initial samples.
    // b->x1 = b->x2 = 0;
    // b->y1 = b->y2 = 0;

    b->skip = 0;
  } else {
    b->skip = 1;
  }
}

/* Reallocates biquad filters data according to
   new count of audio channels. */
static BOOL
eq_realloc_filters( FORMAT_INFO* format )
{
  int ch, i;

  DEBUGLOG(( "eq: old format: size %d, sample: %d, channels: %d, bits: %d, id: %d\n",
    original_format.size, original_format.samplerate, original_format.channels,
    original_format.bits, original_format.format ));
  DEBUGLOG(( "eq: new format: size %d, sample: %d, channels: %d, bits: %d, id: %d\n",
    format->size, format->samplerate, format->channels,
    format->bits, format->format ));

  original_format = *format;
  filters = realloc( filters, sizeof( biquad ) * original_format.channels * EQ_BANDS  );

  if( filters ) {
    for( ch = 0; ch < original_format.channels; ch++ ) {
      for( i = 0; i < EQ_BANDS; i++ ) {
        memset( filters+ch*EQ_BANDS+i, 0, sizeof( *filters ));
        // Negative gain value cause filter reinitialization.
        filters[ch*EQ_BANDS+i].gain = -1.0;
      }
    }
    gains_changed = TRUE;
    return TRUE;
  } else {
    return FALSE;
  }
}

/* Reinits biquad filters. */
static void
eq_reinit_filters( void )
{
  int ch, i;
  gains_changed = FALSE;

  for( i = 0; i < EQ_BANDS; i++ ) {
    if( filters[i].gain != cfg.eq_gains[i] ) {
      for( ch = 0; ch < original_format.channels; ch++ ) {
        eq_biquad_init( &filters[ch*EQ_BANDS+i], cfg.eq_gains[i], freqs[i], original_format.samplerate, 1 );
      }
      DEBUGLOG(( "eq: set biquad filter %08.2f Hz to %05.3f%s\n",
                      freqs[i], cfg.eq_gains[i], filters[i].skip ? " [skipped]" : "" ));
    }
  }
}

/* Equalizes block of samples. */
int DLLENTRY
equalize_samples( FORMAT_INFO* format, BYTE* buf, int len )
{
  int   ch_len, ch, i, bytespersample;
  BYTE* p;

  if( format->bits > 32 || format->bits % 8 != 0 || format->format != WAVE_FORMAT_PCM ) {
    if( !illegal_format ) {
      WinPostMsg( heq, EQ_ILLEGAL_FORMAT, MPFROMLONG( TRUE  ), 0 );
      illegal_format = TRUE;
    }
    return len;
  } else {
    if( illegal_format ) {
      WinPostMsg( heq, EQ_ILLEGAL_FORMAT, MPFROMLONG( FALSE ), 0 );
      illegal_format = FALSE;
    }
  }

  if( !cfg.eq_enabled ) {
    return len;
  }

  DEBUGLOG2(( "eq: equalize %d bytes of %d-bit samples\n", len, format->bits ));

  if( memcmp( format, &original_format, sizeof( *format )) != 0 ) {
    eq_realloc_filters( format );
  }
  if( gains_changed ) {
    eq_reinit_filters();
  }

  bytespersample = original_format.bits / 8;
  ch_len = len / bytespersample;

  for( i = 0, p = buf; i < ch_len; i += original_format.channels ) {
    for( ch = 0; ch < original_format.channels; ch++ )
    {
      int fi = ch*EQ_BANDS;
      smp_type b = 0;

      switch( original_format.bits ) {
        case  8: b = GETSMPL8U(p) / EQ_8U_MAX; break;
        case 16: b = GETSMPL16(p) / EQ_16_MAX; break;
        case 24: b = GETSMPL24(p) / EQ_24_MAX; break;
        case 32: b = GETSMPL32(p) / EQ_32_MAX; break;
      }

      b *= cfg.eq_preamp;
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = eq_biquad( b, &filters[fi++] );
      b = limit2( b, -1.0, 1.0 );

      switch( original_format.bits ) {
        case  8: b *= EQ_8U_MAX; PUTSMPL8U(p,b); break;
        case 16: b *= EQ_16_MAX; PUTSMPL16(p,b); break;
        case 24: b *= EQ_24_MAX; PUTSMPL24(p,b); break;
        case 32: b *= EQ_32_MAX; PUTSMPL32(p,b); break;
      }

      p += bytespersample;
    }
  }

  return len;
}

/* Saves the equalizer preset to user selected file.
 * Must be called from the main thread. */
static BOOL
eq_m_save( HWND owner )
{
  FILEDLG filedialog;
  FILE*   file;
  int     i;
  char    ext[_MAX_EXT];
  APSZ    types[] = {{ FDT_EQUALIZER_EQ }, { 0 }};

  ASSERT_IS_MAIN_THREAD;
  memset( &filedialog, 0, sizeof( FILEDLG ));

  filedialog.cbSize         = sizeof( FILEDLG );
  filedialog.fl             = FDS_CENTER | FDS_SAVEAS_DIALOG | FDS_ENABLEFILELB;
  filedialog.pszTitle       = "Save equalizer";
  filedialog.papszITypeList = types;
  filedialog.pszIType       = FDT_EQUALIZER_EQ;

  strcpy( filedialog.szFullFile, cfg.lasteq );
  amp_file_dlg( HWND_DESKTOP, owner, &filedialog );

  if( filedialog.lReturn == DID_OK )
  {
    if( strcmp( sfext( ext, filedialog.szFullFile, sizeof( ext )), "" ) == 0 ) {
      strcat( filedialog.szFullFile, ".eq" );
    }

    if( amp_warn_if_overwrite( owner, filedialog.szFullFile ))
    {
      strcpy( cfg.lasteq, filedialog.szFullFile );
      file = fopen( filedialog.szFullFile, "w" );

      if( file == NULL ) {
        return FALSE;
      }

      fprintf( file, "#\n# Equalizer created with %s\n# Do not modify!\n#\n", AMP_FULLNAME );

      // Duplicate gains twice because the previous versions of the
      // equalizer saved left and right channel separately.
      fprintf( file, "# Band gains\n" );
      for( i = 0; i < EQ_BANDS; i++ ) {
        fprintf( file, "%g\n", cfg.eq_gains[i] );
      }
      for( i = 0; i < EQ_BANDS; i++ ) {
        fprintf( file, "%g\n", cfg.eq_gains[i] );
      }

      fprintf( file, "# Mutes\n" );
      for( i = 0; i < EQ_BANDS * 2; i++ ) {
        fprintf( file, "%u\n", 0 );
      }

      fprintf( file, "# Preamplifier\n" );
      fprintf( file, "%g\n", cfg.eq_preamp );

      fprintf( file, "# End of equalizer\n" );
      fclose( file );
      return TRUE;
    }
  }
  return FALSE;
}

/* Loads the equalizer preset from specified file.
 * Must be called from the main thread. */
static BOOL
eq_m_load_eq_file( char* filename )
{
  FILE* file;
  char  vz[CCHMAXPATH];
  int   i;

  ASSERT_IS_MAIN_THREAD;

  file = fopen( filename, "r" );
  if( !file ) {
    return FALSE;
  }

  i = 0;
  while( !feof( file ))
  {
    fgets( vz, sizeof( vz ), file );
    blank_strip( vz );
    if( *vz && vz[0] != '#' && vz[0] != ';' && i < 41 )
    {
      if( i < EQ_BANDS ) {
        cfg.eq_gains[i] = atof(vz);
      }
      // Skip second channel of gains and two
      // channels of mutes.
      if( i == EQ_BANDS * 4 ) {
        cfg.eq_preamp = atof(vz);
      }
      i++;
    }
  }
  fclose( file );
  gains_changed = TRUE;
  return TRUE;
}

/* Loads the WinAmp equalizer preset from specified file.
 * Must be called from the main thread. */
static BOOL
eq_m_load_eqf_file( char* filename )
{
  FILE* file;
  int   i, o;
  float p1;
  float p2;
  float eqf_gains[11];

  static const float eqf_freqs[10] =
    { 70, 180, 320, 600, 1000, 3000, 6000, 12000, 14000, 16000 };

  // Every .eqf-file has a size of exactly 299 bytes.
  char  buff[299];

  #define EQF_BANDS        10
  #define EQF_SLIDER_RANGE 64
  #define EQF_SLIDER_MAXDB 12

  ASSERT_IS_MAIN_THREAD;

  file = fopen( filename, "rb" );
  if( !file ) {
    return FALSE;
  }

  if( fread( buff, 1, sizeof( buff ), file ) != sizeof( buff )) {
    fclose( file );
    return FALSE;
  }

  fclose( file );

  if( strnicmp( buff, "Winamp EQ library file v1.1\x1A!--Entry1", 38 ) != 0 ) {
    return FALSE;
  }

  for( i = 0; i < EQF_BANDS + 1; i++ ) {
     // Transforming -12/12 dB into voltage gain.
     eqf_gains[i] = (( -1.0 - buff[0x120+i] ) / EQF_SLIDER_RANGE * 2.0 + 1 ) * EQF_SLIDER_MAXDB;
     eqf_gains[i] = pow( 10, eqf_gains[i] / 20.0 );
  }

  // Tries to approximate the PM123 equalizer frequencies with
  // WinAmp equalizer frequencies.

  for( i = 0; i < EQ_BANDS && freqs[i] < eqf_freqs[0]; i++ ) {
    cfg.eq_gains[i] = eqf_gains[0];
  }

  p1 = eqf_gains[0];
  p2 = eqf_gains[1];
  o  = 1;

  for( ; i < EQ_BANDS && o < 10; i++ ) {
    while( freqs[i] >= eqf_freqs[o] ) {
      p1 = p2;
      p2 = eqf_gains[++o];
    }
    cfg.eq_gains[i] = p1 + ( freqs[i] - eqf_freqs[o-1] ) * ( p2 - p1 ) / ( eqf_freqs[o] - eqf_freqs[o-1] );
  }

  for( ; i < EQ_BANDS; i++ ) {
    cfg.eq_gains[i] = eqf_gains[9];
  }

  cfg.eq_preamp = eqf_gains[EQF_BANDS];
  gains_changed = TRUE;
  return TRUE;
}

/* Loads the equalizer preset from the user selected file.
 * Must be called from the main thread. */
static BOOL
eq_m_load( HWND hwnd )
{
  FILEDLG filedialog;
  APSZ    types[] = {{ FDT_EQUALIZER }, { 0 }};

  ASSERT_IS_MAIN_THREAD;
  memset( &filedialog, 0, sizeof( FILEDLG ));

  filedialog.cbSize         = sizeof( FILEDLG );
  filedialog.fl             = FDS_CENTER | FDS_OPEN_DIALOG;
  filedialog.pszTitle       = "Load equalizer";
  filedialog.papszITypeList = types;
  filedialog.pszIType       = FDT_EQUALIZER;

  sdrivedir( filedialog.szFullFile, cfg.lasteq, sizeof( filedialog.szFullFile ));
  amp_file_dlg( HWND_DESKTOP, HWND_DESKTOP, &filedialog );

  if( filedialog.lReturn == DID_OK )
  {
    char ext[_MAX_EXT];

    strcpy( cfg.lasteq, filedialog.szFullFile );
    sfext( ext, filedialog.szFullFile, sizeof( ext ));

    if( stricmp( ext, ".eqf" ) == 0 ) {
      // Converts .eqf to .eq
      cfg.lasteq[ strlen( cfg.lasteq ) - 1 ] = 0;
      return eq_m_load_eqf_file( filedialog.szFullFile );
    } else {
      return eq_m_load_eq_file( filedialog.szFullFile );
    }
  }

  return FALSE;
}

static void
eq_refresh_colors( void )
{
  HWND hwnd;
  int  i;

  if( bg != 0xFFFFFFFFUL )
  {
    RGB rgb = ultorgb(bg);
    RGB rgb_light;
    RGB rgb_dark;

    if((UCHAR)rgb.bRed + (UCHAR)rgb.bGreen + (UCHAR)rgb.bBlue > 750 ) {
      rgb_light = rgb_lighten(rgb, -30 );
      rgb_dark  = rgb_lighten(rgb, -90 );
    } else if((UCHAR)rgb.bRed + (UCHAR)rgb.bGreen + (UCHAR)rgb.bBlue < 45 ) {
      rgb_light = rgb_lighten(rgb,  90 );
      rgb_dark  = rgb_lighten(rgb,  60 );
    } else {
      rgb_light = rgb_lighten(rgb,  60 );
      rgb_dark  = rgb_lighten(rgb, -60 );
    }

    WinSetPresParam( heq, PP_BACKGROUNDCOLOR, sizeof(rgb), &rgb );

    for( i = SL_EQ00; i <= SL_EQ10; i++ ) {
      if(( hwnd = WinWindowFromID( heq, i )) != NULLHANDLE )
      {
        SWP swp;

        WinSetPresParam( hwnd, PP_BACKGROUNDCOLOR, sizeof(RGB), &rgb );
        WinSetPresParam( hwnd, PP_BUTTONBACKGROUNDCOLOR, sizeof(RGB), &rgb );
        WinSetPresParam( hwnd, PP_BORDERLIGHTCOLOR, sizeof(RGB), &rgb_light );
        WinSetPresParam( hwnd, PP_BUTTONBORDERLIGHTCOLOR, sizeof(RGB), &rgb_light );
        WinSetPresParam( hwnd, PP_BORDER2LIGHTCOLOR, sizeof(RGB), &rgb_light );
        WinSetPresParam( hwnd, PP_BORDERDARKCOLOR, sizeof(RGB), &rgb_dark );
        WinSetPresParam( hwnd, PP_BUTTONBORDERDARKCOLOR, sizeof(RGB), &rgb_dark );
        WinSetPresParam( hwnd, PP_BORDER2DARKCOLOR, sizeof(RGB), &rgb_dark );
        // The color of the slider button changes only after receiving a WM_SIZE
        // message. Possible this is a some OS/2 bug.
        if( WinQueryWindowPos( hwnd, &swp )) {
          WinSendMsg( hwnd, WM_SIZE, 0, MPFROM2SHORT( swp.cx, swp.cy ));
        }
      }
    }
    for( i = PB_EQ_DEFAULT; i <= PB_EQ_SAVE; i++ ) {
      if(( hwnd = WinWindowFromID( heq, i )) != NULLHANDLE ) {
        WinSetPresParam( hwnd, PP_BACKGROUNDCOLOR, sizeof(RGB), &rgb );
        WinSetPresParam( hwnd, PP_BORDERLIGHTCOLOR, sizeof(RGB), &rgb_light );
        WinSetPresParam( hwnd, PP_BORDERDARKCOLOR, sizeof(RGB), &rgb_dark );
      }
    }
  }
  if( fg != 0xFFFFFFFFUL ) {
    RGB rgb = ultorgb(fg);
    WinSetPresParam( heq, PP_FOREGROUNDCOLOR, sizeof(rgb), &rgb );

    for( i = SL_EQ00; i <= SL_EQ10; i++ ) {
      if(( hwnd = WinWindowFromID( heq, i )) != NULLHANDLE ) {
        WinSetPresParam( hwnd, PP_BORDERCOLOR, sizeof(RGB), &rgb );
      }
    }
    for( i = ST_EQ01; i <= ST_EQ10; i++ ) {
      if(( hwnd = WinWindowFromID( heq, i )) != NULLHANDLE ) {
        WinSetPresParam( hwnd, PP_FOREGROUNDCOLOR, sizeof(RGB), &rgb );
      }
    }
    for( i = ST_BOTTOM; i <= ST_DBPREAMP; i++ ) {
      if(( hwnd = WinWindowFromID( heq, i )) != NULLHANDLE ) {
        WinSetPresParam( hwnd, PP_FOREGROUNDCOLOR, sizeof(RGB), &rgb );
      }
    }
    for( i = PB_EQ_DEFAULT; i <= PB_EQ_SAVE; i++ ) {
      if(( hwnd = WinWindowFromID( heq, i )) != NULLHANDLE ) {
        WinSetPresParam( hwnd, PP_FOREGROUNDCOLOR, sizeof(RGB), &rgb );
      }
    }
  }

  WinSendMsg( heq, WM_SKIN_CHANGED, 0, 0 );
}

/* Changes the equalizer colors. */
BOOL eq_set_colors( ULONG fgcolor, ULONG bgcolor,
                    ULONG hi_fgcolor, ULONG hi_bgcolor,
                    ULONG sb_fgcolor, ULONG sb_bgcolor )
{
  bg = sb_bgcolor == 0xFFFFFFFFUL ? bgcolor : sb_bgcolor;
  fg = sb_fgcolor == 0xFFFFFFFFUL ? fgcolor : sb_fgcolor;

  eq_refresh_colors();
  return TRUE;
}

/* Processes messages of the equalizer dialog.
   Must be called from the main thread. */
static MRESULT EXPENTRY
eq_m_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static  BOOL nottheuser = FALSE;
  #define EQ_SLIDER_RANGE 24
  #define EQ_SLIDER_MAXDB 12

  switch( msg )
  {
    case WM_INITDLG:
    {
      int  i;

      nottheuser = TRUE;

      for( i = SL_EQ00; i <= SL_EQ10; i++ )
      {
        WinSendDlgItemMsg( hwnd, i, SLM_SETTICKSIZE,
                           MPFROM2SHORT( SMA_SETALLTICKS, 2 ), 0 );
        WinSendDlgItemMsg( hwnd, i, SLM_SETSLIDERINFO,
                           MPFROM2SHORT( SMA_SLIDERARMPOSITION, SMA_INCREMENTVALUE ),
                           MPFROMLONG( EQ_SLIDER_RANGE / 2 ));
      }

      nottheuser = FALSE;

      WinCheckButton( hwnd, CB_EQ_ENABLED, cfg.eq_enabled );
      WinSendMsg( hwnd, EQ_REFRESH_CONTROLS, 0, 0 );
      WinSendMsg( hwnd, EQ_ILLEGAL_FORMAT, MPFROMLONG( illegal_format ), 0 );
      break;
    }

    case EQ_REFRESH_COLORS:
      eq_refresh_colors();
      return 0;

    case EQ_ILLEGAL_FORMAT:
      WinSetDlgItemText( hwnd, ST_EQ_WARNING, mp1 ? "Currently played audio can't be equalized" : "" );
      return 0;

    case WM_CLOSE:
      heq = NULLHANDLE;
      WinDestroyWindow( hwnd );
      return 0;

    case WM_DESTROY:
      save_window_pos( hwnd, WIN_MAP_POINTS );
      break;

    case WM_HELP:
      amp_show_help( IDH_EQUALIZER );
      return 0;

    case EQ_REFRESH_CONTROLS:
    {
      ULONG pos;
      int   i;
      char  buff[8];
      float gain;

      nottheuser = TRUE;

      for( i = 0; i < EQ_BANDS; i++ ) {
        gain = 20.0 * log10( cfg.eq_gains[i] );
        WinSetDlgItemText( hwnd, ST_DB01 + i, ltoa( gain, buff, 10 ));
        pos  = ( gain / EQ_SLIDER_MAXDB + 1 ) * EQ_SLIDER_RANGE / 2;
        WinSendDlgItemMsg( hwnd, SL_EQ01 + i, SLM_SETSLIDERINFO,
                           MPFROM2SHORT( SMA_SLIDERARMPOSITION, SMA_INCREMENTVALUE ),
                           MPFROMSHORT( pos ));
      }

      gain = 20.0 * log10( cfg.eq_preamp );
      WinSetDlgItemText( hwnd, ST_DBPREAMP, ltoa( gain, buff, 10 ));
      pos  = ( gain / EQ_SLIDER_MAXDB + 1 ) * EQ_SLIDER_RANGE / 2;
      WinSendDlgItemMsg( hwnd, SL_PREAMP, SLM_SETSLIDERINFO,
                         MPFROM2SHORT( SMA_SLIDERARMPOSITION, SMA_INCREMENTVALUE ),
                         MPFROMSHORT( pos ));

      nottheuser = FALSE;
      return 0;
    }

    case WM_TRANSLATEACCEL:
      if( hk_translate( HKW_GLOBAL, mp1, mp2 )) {
        return MRFROMLONG( TRUE );
      }
      break;

    case WM_COMMAND:
      switch( COMMANDMSG(&msg)->cmd )
      {
        case PB_EQ_LOAD:
          if( eq_m_load( hwnd )) {
            WinSendMsg( hwnd, EQ_REFRESH_CONTROLS, 0, 0 );
          }
          return 0;

        case PB_EQ_SAVE:
          eq_m_save( hwnd );
          return 0;

        case PB_EQ_DEFAULT:
        {
          int  i;
          for( i = 0; i < EQ_BANDS; i++ ) {
            cfg.eq_gains[i] = 1.0;
          }

          cfg.eq_preamp = 1.0;
          gains_changed = TRUE;
          WinSendMsg( hwnd, EQ_REFRESH_CONTROLS, 0, 0 );
          return 0;
        }

        case DID_CANCEL:
          eq_show( FALSE );
          return 0;
      }
      break;

    case WM_CONTROL:
    {
      int  id     = SHORT1FROMMP(mp1);
      int  notify = SHORT2FROMMP(mp1);
      char buff[8];

      if( nottheuser ) {
        break;
      }

      if( id == CB_EQ_ENABLED ) {
        if( notify == BN_CLICKED ) {
          cfg.eq_enabled = WinQueryButtonCheckstate( hwnd, CB_EQ_ENABLED );
          gains_changed = TRUE;
        }
      } else if( id == SL_PREAMP ) {
        if( notify == SLN_SLIDERTRACK || notify == SLN_CHANGE )
        {
          USHORT pos  = SHORT1FROMMR( WinSendDlgItemMsg( hwnd, id, SLM_QUERYSLIDERINFO,
                                      MPFROM2SHORT( SMA_SLIDERARMPOSITION, SMA_INCREMENTVALUE ), 0 ));
          // -12 to 12 dB.
          float  gain = ((float)pos / EQ_SLIDER_RANGE * 2.0 - 1 ) * EQ_SLIDER_MAXDB;
          WinSetDlgItemText( hwnd, ST_DBPREAMP, ltoa( gain, buff, 10 ));
          // Transforming dB into voltage gain.
          cfg.eq_preamp = pow( 10, gain / 20.0 );
          gains_changed = TRUE;
        }
      } else if( id >= SL_EQ01 && id <= SL_EQ10 ) {
        if( notify == SLN_SLIDERTRACK || notify == SLN_CHANGE )
        {
          USHORT pos  = SHORT1FROMMR( WinSendDlgItemMsg( hwnd, id, SLM_QUERYSLIDERINFO,
                                      MPFROM2SHORT( SMA_SLIDERARMPOSITION, SMA_INCREMENTVALUE ), 0 ));
          // -12 to 12 dB.
          float  gain = ((float)pos / EQ_SLIDER_RANGE * 2.0 - 1 ) * EQ_SLIDER_MAXDB;
          WinSetDlgItemText( hwnd, id-SL_EQ01+ST_DB01, ltoa( gain, buff, 10 ));
          // Transforming dB into voltage gain.
          cfg.eq_gains[id-SL_EQ01] = pow( 10, gain / 20.0 );
          gains_changed = TRUE;
        }
      }
      break;
    }
  }

  return bmp_frame_wnd_proc( hwnd, msg, mp1, mp2 );
}

/* Shows the equalizer presentation window.
   Must be called from the main thread. */
void
eq_show( BOOL show )
{
  ASSERT_IS_MAIN_THREAD;

  if( show ) {
     if( !heq ) {
       heq = WinLoadDlg( HWND_DESKTOP, HWND_DESKTOP,
                         eq_m_dlg_proc, hmodule, DLG_EQUALIZER, NULL );
       do_warpsans( heq );
       do_warpsans( WinWindowFromID( heq, CB_EQ_ENABLED ));
       do_warpsans( WinWindowFromID( heq, PB_EQ_DEFAULT ));
       do_warpsans( WinWindowFromID( heq, PB_EQ_LOAD ));
       do_warpsans( WinWindowFromID( heq, PB_EQ_SAVE ));

       WinSetWindowText( heq, "Graphical Sound Equalizer" );
     }

     rest_window_pos( heq, WIN_MAP_POINTS );
     eq_refresh_colors();

     WinSetWindowPos( heq, HWND_TOP, 0, 0, 0, 0,
                           SWP_ZORDER | SWP_SHOW | SWP_ACTIVATE );
  } else {
    if( heq ) {
      WinDestroyWindow( heq );
      heq = NULLHANDLE;
    }
  }
}

/* Destroys the equalizer presentation window.
   Must be called from the main thread. */
void
eq_destroy( void ) {
  eq_show( FALSE );
}

/* Returns the visibility state of the equalizer presentation window.
   Must be called from the main thread. */
BOOL
eq_is_visible( void )
{
  ASSERT_IS_MAIN_THREAD;
  return heq && WinIsWindowShowing( heq );
}

