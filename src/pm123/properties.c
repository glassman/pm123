/*
 * Copyright 1997-2003 Samuel Audet  <guardia@step.polymtl.ca>
 *                     Taneli Lepp�  <rosmo@sektori.com>
 *
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_WIN
#define  INCL_GPI
#define  INCL_ERRORS
#include <os2.h>
#include <stdio.h>
#include <memory.h>
#include <os2fonts.h>
#include <debuglog.h>

#include "properties.h"
#include "pm123.h"
#include "plugman.h"
#include "filedlg.h"
#include "asso.h"
#include "skin.h"
#include "iniman.h"
#include "messages.h"
#include "banner.h"
#include "upnp.h"

#define  CFG_REFRESH_LIST ( WM_USER + 1000 )
#define  CFG_REFRESH_INFO ( WM_USER + 1001 )
#define  CFG_DEFAULT      ( WM_USER + 1002 )
#define  CFG_UNDO         ( WM_USER + 1003 )
#define  CFG_REFRESH_PAGE ( WM_USER + 1004 )
#define  CFG_REFRESH_FONT ( WM_USER + 1005 )

amp_cfg cfg;

static HWND page01;
static HWND page02;
static HWND page03;
static HWND page04;
static HWND page05;
static HWND page06;
static HWND page07;

static BOOL restart_wps = FALSE;
BOOL32 APIENTRY WinRestartWorkplace( VOID );

/* Returns suggested read ahead buffer size. */
size_t
cfg_suggested_buff_size( void )
{
  ULONG  mem_size = 0;
  size_t buf_size = 128;

  if( DosQuerySysInfo( QSV_TOTPHYSMEM, QSV_TOTPHYSMEM, &mem_size, sizeof( mem_size )) == NO_ERROR )
  {
    mem_size = mem_size / 1048576;

    if( mem_size >= 256 ) {
      buf_size = 16384;
    } else if( mem_size >= 192 ) {
      buf_size = 8192;
    } else if( mem_size >= 128 ) {
      buf_size = 4096;
    } else if( mem_size >= 96 ) {
      buf_size = 2048;
    } else if( mem_size >= 64 ) {
      buf_size = 1024;
    } else if( mem_size >= 56 ) {
      buf_size = 512;
    } else if( mem_size >= 32 ) {
      buf_size = 256;
    } else {
      buf_size = 128;
    }

    DEBUGLOG(( "pm123: total physical memory: %lu mb, suggested buffer size: %u\n", mem_size, buf_size ));
  }

  return buf_size;
}

static ULONG
cfg_add_plugin( HWND hwnd, ULONG types )
{
  FILEDLG filedialog;
  ULONG   rc = 0;
  APSZ    ftypes[] = {{ FDT_PLUGIN }, { 0 }};
  char    name[64];

  memset( &filedialog, 0, sizeof( FILEDLG ));

  filedialog.cbSize         = sizeof( FILEDLG );
  filedialog.fl             = FDS_CENTER | FDS_OPEN_DIALOG | FDS_CUSTOM;
  filedialog.pszTitle       = "Load a plug-in";
  filedialog.hMod           = hmodule;
  filedialog.usDlgId        = DLG_FILE;
  filedialog.papszITypeList = ftypes;
  filedialog.pszIType       = FDT_PLUGIN;

  strcpy( filedialog.szFullFile, cfg.plugdir );
  amp_file_dlg( HWND_DESKTOP, hwnd, &filedialog );

  if( filedialog.lReturn == DID_OK ) {
    sdrivedir( cfg.plugdir, filedialog.szFullFile, sizeof( cfg.plugdir ));
    rc = pg_load_plugin( filedialog.szFullFile, NULL );
    if( rc & PLUGIN_VISUAL ) {
      vis_initialize( sfname( name, filedialog.szFullFile, sizeof( name )),
                      amp_player_window());
    }
    if(( rc & PLUGIN_FILTER ) && !is_stopped()) {
      amp_show_info( "This filter will be enabled only after cycle of a stop/start of the player." );
    }
    WinSendMsg( page04, CFG_REFRESH_LIST, MPFROMLONG( rc ), 0 );
    WinSendMsg( page05, CFG_REFRESH_LIST, MPFROMLONG( rc ), 0 );
  }
  return rc;
}

/* Processes messages of the setings page of the setup notebook.
 */

static MRESULT EXPENTRY
cfg_page1_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static char* buff_sizes[] = {      "0",     "32",    "64",    "96",   "128",   "256",    "384",    "512",    "768",
                                  "1024",   "1536",  "2048",  "3072",  "4096",  "5120",   "6144",   "7168",   "8192",
                                 "10240",  "12288", "14336", "16384", "19456", "22528",  "25600",  "28672",  "32768",
                                 "36864",  "40960", "45056", "51200", "65536", "81920",  "98304", "131072", "163840",
                                "196608", "260096"  };
  switch( msg ) {
    case WM_INITDLG:
    {
      char buffer[128];
      LONG i;

      WinCheckButton( hwnd, CB_PLAYONLOAD,   cfg.playonload     );
      WinCheckButton( hwnd, CB_AUTOUSEPL,    cfg.autouse        );
      WinCheckButton( hwnd, CB_AUTOPLAYPL,   cfg.playonuse      );
      WinCheckButton( hwnd, CB_SELECTPLAYED, cfg.selectplayed   );
      WinCheckButton( hwnd, CB_BADFILES,     cfg.skip_badfiles  );
      WinCheckButton( hwnd, CB_DOCK,         cfg.dock_windows   );
      WinCheckButton( hwnd, CB_CONTINUOUS,   cfg.continuous     );
      WinCheckButton( hwnd, CB_KNOWNONLY,    cfg.known_only     );
      WinCheckButton( hwnd, CB_ACTASUPNP,    cfg.upnp           );
      WinCheckButton( hwnd, CB_NO_LOCAL,     cfg.proxy_no_local );

      WinSetDlgItemText( hwnd, EF_DOCK, itoa( cfg.dock_margin, buffer, 10 ));
      WinSetDlgItemText( hwnd, EF_PROXY_HOST, cfg.proxy_host );
      WinSetDlgItemText( hwnd, EF_PROXY_PORT, itoa( cfg.proxy_port, buffer, 10 ));
      WinSetDlgItemText( hwnd, EF_PROXY_USER, cfg.proxy_user );
      WinSetDlgItemText( hwnd, EF_PROXY_PASS, cfg.proxy_pass );
      WinSetDlgItemText( hwnd, EF_NO_PROXY,   cfg.no_proxy   );
      WinCheckButton   ( hwnd, CB_FILLBUFFER, cfg.buff_wait  );
      WinEnableControl ( hwnd, SB_FILLBUFFER, cfg.buff_wait  );

      WinSendDlgItemMsg( hwnd, SB_BUFFERSIZE, SPBM_SETARRAY,
                               MPFROMP( buff_sizes ), MPFROMLONG( ARRAY_SIZE( buff_sizes )));

      for( i = 0; i < ARRAY_SIZE( buff_sizes ) - 1; i++ ) {
        if( cfg.buff_size <= atol( buff_sizes[i])) {
          break;
        }
      }

      WinSendDlgItemMsg( hwnd, SB_BUFFERSIZE, SPBM_SETCURRENTVALUE,
                               MPFROMLONG( i ), 0 );
      WinSendDlgItemMsg( hwnd, SB_FILLBUFFER, SPBM_SETLIMITS,
                               MPFROMLONG( cfg.buff_size  ), MPFROMLONG( 1 ));
      WinSendDlgItemMsg( hwnd, SB_FILLBUFFER, SPBM_SETCURRENTVALUE,
                               MPFROMLONG( cfg.buff_preload ), 0 );
      WinSendDlgItemMsg( hwnd, SB_TIMEOUT, SPBM_SETLIMITS,
                               MPFROMLONG( 300  ), MPFROMLONG( 1 ));
      WinSendDlgItemMsg( hwnd, SB_TIMEOUT, SPBM_SETCURRENTVALUE,
                               MPFROMLONG( cfg.conn_timeout ), 0 );
      return 0;
    }

    case WM_COMMAND:
      return 0;

    case WM_CONTROL:
      if( SHORT1FROMMP(mp1) == CB_FILLBUFFER &&
        ( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ))
      {
        BOOL fill = WinQueryButtonCheckstate( hwnd, CB_FILLBUFFER );
        WinEnableControl( hwnd, SB_FILLBUFFER, fill );
      }
      else if( SHORT1FROMMP(mp1) == SB_BUFFERSIZE && SHORT2FROMMP(mp1) == SPBN_CHANGE )
      {
        size_t buff_size;
        int    i;

        WinSendDlgItemMsg( hwnd, SB_BUFFERSIZE, SPBM_QUERYVALUE,
                           MPFROMP( &i ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
        buff_size = atol( buff_sizes[i] );
        WinSendDlgItemMsg( hwnd, SB_FILLBUFFER, SPBM_SETLIMITS,
                           MPFROMLONG( buff_size ), MPFROMLONG( 1 ));
      }
      return 0;

    case CFG_UNDO:
      WinSendMsg( hwnd, WM_INITDLG, 0, 0 );
      return 0;

    case CFG_DEFAULT:
    {
      size_t buff_size = cfg_suggested_buff_size();
      LONG i;

      WinCheckButton( hwnd, CB_PLAYONLOAD,   TRUE  );
      WinCheckButton( hwnd, CB_AUTOUSEPL,    TRUE  );
      WinCheckButton( hwnd, CB_AUTOPLAYPL,   TRUE  );
      WinCheckButton( hwnd, CB_SELECTPLAYED, FALSE );
      WinCheckButton( hwnd, CB_BADFILES,     TRUE  );
      WinCheckButton( hwnd, CB_DOCK,         TRUE  );
      WinCheckButton( hwnd, CB_FILLBUFFER,   TRUE  );
      WinCheckButton( hwnd, CB_CONTINUOUS,   TRUE  );
      WinCheckButton( hwnd, CB_KNOWNONLY,    TRUE  );
      WinCheckButton( hwnd, CB_ACTASUPNP,    FALSE );
      WinCheckButton( hwnd, CB_NO_LOCAL,     TRUE  );

      for( i = 0; i < ARRAY_SIZE( buff_sizes ) - 1; i++ ) {
        if( buff_size <= atol( buff_sizes[i])) {
          break;
        }
      }

      // The buffer size must be set before buffer prefill value.
      WinSendDlgItemMsg( hwnd, SB_BUFFERSIZE, SPBM_SETCURRENTVALUE,
                               MPFROMLONG( i ), 0 );

      WinSetDlgItemText( hwnd, EF_DOCK,       "10" );
      WinSetDlgItemText( hwnd, EF_PROXY_HOST,  ""  );
      WinSetDlgItemText( hwnd, EF_PROXY_PORT, "0"  );
      WinSetDlgItemText( hwnd, EF_PROXY_USER,  ""  );
      WinSetDlgItemText( hwnd, EF_PROXY_PASS,  ""  );
      WinSetDlgItemText( hwnd, EF_NO_PROXY,   "localhost,127.0.0.1" );
      WinSendDlgItemMsg( hwnd, SB_FILLBUFFER, SPBM_SETCURRENTVALUE, MPFROMLONG(  32 ), 0 );
      WinEnableControl ( hwnd, SB_FILLBUFFER, TRUE );
      WinSendDlgItemMsg( hwnd, SB_TIMEOUT,    SPBM_SETCURRENTVALUE, MPFROMLONG(  15 ), 0 );

      return 0;
    }

    case WM_DESTROY:
    {
      char buffer[8];
      int  i;

      cfg.playonload     = WinQueryButtonCheckstate( hwnd, CB_PLAYONLOAD   );
      cfg.autouse        = WinQueryButtonCheckstate( hwnd, CB_AUTOUSEPL    );
      cfg.playonuse      = WinQueryButtonCheckstate( hwnd, CB_AUTOPLAYPL   );
      cfg.selectplayed   = WinQueryButtonCheckstate( hwnd, CB_SELECTPLAYED );
      cfg.skip_badfiles  = WinQueryButtonCheckstate( hwnd, CB_BADFILES     );
      cfg.dock_windows   = WinQueryButtonCheckstate( hwnd, CB_DOCK         );
      cfg.continuous     = WinQueryButtonCheckstate( hwnd, CB_CONTINUOUS   );
      cfg.known_only     = WinQueryButtonCheckstate( hwnd, CB_KNOWNONLY    );
      cfg.upnp           = WinQueryButtonCheckstate( hwnd, CB_ACTASUPNP    );
      cfg.proxy_no_local = WinQueryButtonCheckstate( hwnd, CB_NO_LOCAL     );

      WinQueryDlgItemText( hwnd, EF_DOCK, 8, buffer );
      cfg.dock_margin = atoi( buffer );

      WinSendDlgItemMsg( hwnd, SB_BUFFERSIZE, SPBM_QUERYVALUE,
                         MPFROMP( &i ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
      cfg.buff_size = atol( buff_sizes[i] );

      WinSendDlgItemMsg( hwnd, SB_FILLBUFFER, SPBM_QUERYVALUE,
                         MPFROMP( &i ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
      cfg.buff_preload = i;

      WinSendDlgItemMsg( hwnd, SB_TIMEOUT, SPBM_QUERYVALUE,
                         MPFROMP( &i ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
      cfg.conn_timeout = i;

      WinQueryDlgItemText( hwnd, EF_PROXY_HOST, sizeof( cfg.proxy_host ), cfg.proxy_host );
      WinQueryDlgItemText( hwnd, EF_PROXY_USER, sizeof( cfg.proxy_user ), cfg.proxy_user );
      WinQueryDlgItemText( hwnd, EF_PROXY_PASS, sizeof( cfg.proxy_pass ), cfg.proxy_pass );
      WinQueryDlgItemText( hwnd, EF_PROXY_PORT, 8, buffer );

      cfg.proxy_port = atoi( buffer );
      cfg.buff_wait = WinQueryButtonCheckstate( hwnd, CB_FILLBUFFER );

      xio_set_http_proxy_host( cfg.proxy_host );
      xio_http_proxy_host( cfg.proxy_host, sizeof( cfg.proxy_host ));

      xio_set_http_proxy_port( cfg.proxy_port );
      xio_set_http_proxy_user( cfg.proxy_user );
      xio_set_http_proxy_pass( cfg.proxy_pass );
      xio_set_bypass_local( cfg.proxy_no_local );
      xio_set_buffer_size( cfg.buff_size * 1024 );
      xio_set_buffer_wait( cfg.buff_wait );
      xio_set_buffer_preload( cfg.buff_preload * 1024 );
      xio_set_connect_timeout( cfg.conn_timeout );

      WinQueryDlgItemText( hwnd, EF_NO_PROXY, sizeof( cfg.no_proxy ), cfg.no_proxy );
      xio_set_no_proxy( cfg.no_proxy );

      return 0;
    }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the display page of the setup notebook.
 */

static MRESULT EXPENTRY
cfg_page2_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static char old_fontname[MAX_FONTNAME];
  static BOOL old_font_skinned;
  static int  old_scroll;
  static int  old_viewmode;
  static int  old_hither_thither;
  static int  old_double_speed;
  static int  old_tags_charset;

  switch( msg ) {
    case WM_INITDLG:
    {
      int  i;
      for( i = 0; i < ch_list_size; i++ ) {
        lb_add_with_handle( hwnd, CB_CHARSET, ch_list[i].name, ch_list[i].id );
      }

      strlcpy( old_fontname, cfg.fontname, sizeof( old_fontname ));

      old_font_skinned   = cfg.font_skinned;
      old_scroll         = cfg.scroll;
      old_viewmode       = cfg.viewmode;
      old_hither_thither = cfg.hither_thither;
      old_double_speed   = cfg.double_speed;
      old_tags_charset   = cfg.tags_charset;

      // continute to CFG_REFRESH_PAGE...
    }

    case CFG_REFRESH_PAGE:
    {
      WinCheckButton( hwnd, RB_DISP_FILENAME + cfg.viewmode, TRUE );
      WinCheckButton( hwnd, RB_SCROLL_INFINITE + cfg.scroll, TRUE );
      WinCheckButton( hwnd, CB_HITHER_THITHER, cfg.hither_thither );
      WinCheckButton( hwnd, CB_DOUBLE_SPEED, cfg.double_speed );
      WinCheckButton( hwnd, CB_USE_SKIN_FONT, cfg.font_skinned );

      lb_select_by_handle( hwnd, CB_CHARSET, cfg.tags_charset );

      WinEnableControl( hwnd, PB_FONT_SELECT, !cfg.font_skinned );
      WinEnableControl( hwnd, ST_FONT_SAMPLE, !cfg.font_skinned );

      WinSetDlgItemText( hwnd, ST_FONT_SAMPLE, cfg.fontname );
      WinSetPresParam( WinWindowFromID( hwnd, ST_FONT_SAMPLE ), PP_FONTNAMESIZE,
                       strlen( cfg.fontname ) + 1, cfg.fontname );
      return 0;
    }

    case CFG_REFRESH_FONT:
      WinSetDlgItemText( hwnd, ST_FONT_SAMPLE, cfg.fontname );
      WinSetPresParam( WinWindowFromID( hwnd, ST_FONT_SAMPLE ), PP_FONTNAMESIZE,
                       strlen( cfg.fontname ) + 1, cfg.fontname );

      bmp_refresh_font();
      amp_invalidate( UPD_FILEINFO );
      return 0;

    case WM_COMMAND:
      if( COMMANDMSG( &msg )->cmd == PB_FONT_SELECT )
      {
        FONTDLG fontdialog = { 0 };
        LONG    fontsize   = 0;

        fontname_to_attrs( cfg.fontname, &fontdialog.fAttrs, &fontsize, NULL );

        fontdialog.cbSize         = sizeof( fontdialog );
        fontdialog.hpsScreen      = WinGetScreenPS( HWND_DESKTOP );
        fontdialog.pszFamilyname  = malloc( FACESIZE );
        fontdialog.usFamilyBufLen = FACESIZE;
        fontdialog.pszTitle       = "PM123 scroller font";
        fontdialog.pszPreview     = "128 kb/s, 44.1 kHz, Joint-Stereo";
        fontdialog.fl             = FNTS_CENTER | FNTS_RESETBUTTON | FNTS_INITFROMFATTRS;
        fontdialog.clrFore        = CLR_BLACK;
        fontdialog.clrBack        = CLR_WHITE;
        fontdialog.fxPointSize    = MAKEFIXED( fontsize, 0 );

        WinFontDlg( HWND_DESKTOP, hwnd, &fontdialog );

        if( fontdialog.lReturn == DID_OK ) {
          attrs_to_fontname( &fontdialog.fAttrs, fontdialog.fxPointSize >> 16,
                               cfg.fontname, sizeof( cfg.fontname ));

          WinSendMsg( hwnd, CFG_REFRESH_FONT, 0, 0 );
        }

        free( fontdialog.pszFamilyname );
      }
      return 0;

    case WM_CONTROL:
      switch( SHORT1FROMMP(mp1)) {
        case CB_USE_SKIN_FONT:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED )
          {
            cfg.font_skinned = WinQueryButtonCheckstate( hwnd, CB_USE_SKIN_FONT );
            WinEnableControl( hwnd, PB_FONT_SELECT, !cfg.font_skinned );
            WinEnableControl( hwnd, ST_FONT_SAMPLE, !cfg.font_skinned );

            WinSendMsg( hwnd, CFG_REFRESH_FONT, 0, 0 );
          }
          return 0;

        case CB_HITHER_THITHER:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ) {
            cfg.hither_thither = WinQueryButtonCheckstate( hwnd, CB_HITHER_THITHER );
            amp_invalidate( UPD_FILEINFO );
          }
          return 0;

        case CB_DOUBLE_SPEED:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ) {
            cfg.double_speed = WinQueryButtonCheckstate( hwnd, CB_DOUBLE_SPEED );
            amp_invalidate( UPD_SCROLLSPEED );
          }
          return 0;

        case RB_SCROLL_INFINITE:
        case RB_SCROLL_ONCE:
        case RB_SCROLL_DISABLE:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ) {
            cfg.scroll = LONGFROMMR( WinSendDlgItemMsg( hwnd, RB_SCROLL_INFINITE, BM_QUERYCHECKINDEX, 0, 0 ));
            amp_invalidate( UPD_FILEINFO );
          }
          return 0;

        case RB_DISP_FILENAME:
        case RB_DISP_ID3TAG:
        case RB_DISP_FILEINFO:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ) {
            cfg.viewmode = LONGFROMMR( WinSendDlgItemMsg( hwnd, RB_DISP_FILENAME, BM_QUERYCHECKINDEX, 0, 0 ));
            amp_invalidate( UPD_FILENAME | UPD_FILEINFO );
          }
          return 0;

        case CB_CHARSET:
          if( SHORT2FROMMP(mp1) == CBN_EFCHANGE )
          {
            int i;
            i = lb_cursored( hwnd, CB_CHARSET );
            if( i != LIT_NONE ) {
              cfg.tags_charset = (int)lb_get_handle( hwnd, CB_CHARSET, i );
              amp_invalidate( UPD_FILENAME | UPD_FILEINFO );
            }
          }
          return 0;
      }
      break;

    case CFG_UNDO:
      strlcpy( cfg.fontname, old_fontname, sizeof( cfg.fontname ));

      cfg.font_skinned   = old_font_skinned;
      cfg.scroll         = old_scroll;
      cfg.viewmode       = old_viewmode;
      cfg.hither_thither = old_hither_thither;
      cfg.double_speed   = old_double_speed;
      cfg.tags_charset   = old_tags_charset;

      WinSendMsg( hwnd, CFG_REFRESH_PAGE, 0, 0 );
      WinSendMsg( hwnd, CFG_REFRESH_FONT, 0, 0 );
      amp_invalidate( UPD_SCROLLSPEED );
      return 0;

    case CFG_DEFAULT:
    {
      ULONG cp[4], cp_size;

      strcpy( cfg.fontname, bmp_default_fontname());

      cfg.font_skinned   = FALSE;
      cfg.scroll         = CFG_SCROLL_INFINITE;
      cfg.viewmode       = CFG_DISP_ID3TAG;
      cfg.hither_thither = TRUE;
      cfg.double_speed   = FALSE;
      cfg.tags_charset   = CH_DEFAULT;

      // Selects russian auto-detect as default characters encoding
      // for russian peoples.
      if( DosQueryCp( sizeof( cp ), cp, &cp_size ) == NO_ERROR ) {
        if( cp[0] == 866 ) {
          cfg.tags_charset = CH_CYR_AUTO;
        }
      }

      WinSendMsg( hwnd, CFG_REFRESH_PAGE, 0, 0 );
      WinSendMsg( hwnd, CFG_REFRESH_FONT, 0, 0 );
      amp_invalidate( UPD_SCROLLSPEED );
      return 0;
    }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the song banner page of the setup notebook.
 */

static MRESULT EXPENTRY
cfg_page3_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static ULONG old_pos;
  static ULONG old_slide;
  static ULONG old_delay;

  switch( msg ) {
    case WM_INITDLG:
    {
      old_pos   = cfg.bn_pos;
      old_slide = cfg.bn_slide;
      old_delay = cfg.bn_delay;

      WinSendMsg( hwnd, CFG_REFRESH_PAGE, 0, 0 );
      return MRFROMLONG( TRUE );
    }

    case CFG_REFRESH_PAGE:
      WinCheckButton( hwnd, RB_TOP_RIGHT + cfg.bn_pos, TRUE );
      WinCheckButton( hwnd, RB_SLIDE_NONE + cfg.bn_slide, TRUE );
      WinSendDlgItemMsg( hwnd, SB_SHOW_DELAY, SPBM_SETCURRENTVALUE, MPFROMLONG( cfg.bn_delay ), 0 );
      return 0;

    case WM_CONTROL:
      switch( SHORT1FROMMP(mp1)) {
        case RB_TOP_RIGHT:
        case RB_TOP_LEFT:
        case RB_BOTTOM_RIGHT:
        case RB_BOTTOM_LEFT:
        case RB_NO_BANNER:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ) {
            cfg.bn_pos = LONGFROMMR( WinSendDlgItemMsg( hwnd, RB_TOP_RIGHT, BM_QUERYCHECKINDEX, 0, 0 ));
            bn_set_parameters( cfg.bn_pos, cfg.bn_slide, cfg.bn_delay );
          }
          return 0;

        case RB_SLIDE_VERT:
        case RB_SLIDE_HORZ:
        case RB_SLIDE_NONE:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ) {
            cfg.bn_slide = LONGFROMMR( WinSendDlgItemMsg( hwnd, RB_SLIDE_VERT, BM_QUERYCHECKINDEX, 0, 0 ));
            bn_set_parameters( cfg.bn_pos, cfg.bn_slide, cfg.bn_delay );
          }
          return 0;

        case SB_SHOW_DELAY:
          if( SHORT2FROMMP(mp1) == SPBN_CHANGE ) {
            WinSendDlgItemMsg( hwnd, SB_SHOW_DELAY, SPBM_QUERYVALUE,
                               MPFROMP( &cfg.bn_delay ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
            bn_set_parameters( cfg.bn_pos, cfg.bn_slide, cfg.bn_delay );
          }
          return 0;
      }
      break;

    case WM_COMMAND:
      return 0;

    case CFG_UNDO:
      cfg.bn_pos   = old_pos;
      cfg.bn_slide = old_slide;
      cfg.bn_delay = old_delay;

      WinSendMsg( hwnd, CFG_REFRESH_PAGE, 0, 0 );
      return 0;

    case CFG_DEFAULT:
    {
      cfg.bn_pos   = BPOS_UPPER_RIGHT;
      cfg.bn_slide = BSLIDE_HORIZONTAL;
      cfg.bn_delay = 10;

      WinSendMsg( hwnd, CFG_REFRESH_PAGE, 0, 0 );
      return 0;
    }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the plug-ins page 1 of the setup notebook.
 */

static MRESULT EXPENTRY
cfg_page4_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case CFG_REFRESH_LIST:
    {
      if( LONGFROMMP(mp1) & PLUGIN_VISUAL  )
      {
        lb_remove_all( hwnd, LB_VISPLUG );
        pg_expand_to ( hwnd, LB_VISPLUG, PLUGIN_VISUAL );

        if( lb_count( hwnd, LB_VISPLUG )) {
          lb_select( hwnd, LB_VISPLUG, 0 );
        } else {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_VISUAL ), MPFROMSHORT( LIT_NONE ));
        }
      }

      if( LONGFROMMP(mp1) & PLUGIN_DECODER )
      {
        lb_remove_all( hwnd, LB_DECPLUG );
        pg_expand_to ( hwnd, LB_DECPLUG, PLUGIN_DECODER );

        if( lb_count( hwnd, LB_DECPLUG )) {
          lb_select( hwnd, LB_DECPLUG, 0 );
        } else {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_DECODER ), MPFROMSHORT( LIT_NONE ));
        }
      }
      return 0;
    }

    case CFG_REFRESH_INFO:
    {
      SHORT i = SHORT1FROMMP(mp2);

      char buffer[256];
      char name[64];
      BOOL enabled;

      PLUGIN_QUERYPARAM info;

      if( LONGFROMMP(mp1) & PLUGIN_VISUAL  )
      {
        if( i != LIT_NONE ) {
          lb_get_item( hwnd, LB_VISPLUG, i, name, sizeof( name ));
          pg_get_info( name, PLUGIN_VISUAL, &info );
          enabled = pg_is_enabled( name, PLUGIN_VISUAL );
          sprintf( buffer, "Author: %s", info.author );
          WinSetDlgItemText( hwnd, ST_VIS_AUTHOR, buffer );
          sprintf( buffer, "Desc: %s", info.desc );
          WinSetDlgItemText( hwnd, ST_VIS_DESC, buffer );
          WinSetDlgItemText( hwnd, PB_VIS_ENABLE, enabled ? "Disabl~e" : "~Enable" );
          WinEnableControl ( hwnd, PB_VIS_ENABLE, TRUE );
          WinEnableControl ( hwnd, PB_VIS_UNLOAD, TRUE );
          WinEnableControl ( hwnd, PB_VIS_CONFIG, info.configurable && enabled );
        } else {
          WinSetDlgItemText( hwnd, ST_VIS_AUTHOR, "Author: <none>" );
          WinSetDlgItemText( hwnd, ST_VIS_DESC, "Desc: <none>" );
          WinSetDlgItemText( hwnd, PB_VIS_ENABLE, "~Enable" );
          WinEnableControl ( hwnd, PB_VIS_CONFIG, FALSE );
          WinEnableControl ( hwnd, PB_VIS_ENABLE, FALSE );
          WinEnableControl ( hwnd, PB_VIS_UNLOAD, FALSE );
        }
      }
      if( LONGFROMMP(mp1) & PLUGIN_DECODER ) {
        if( i != LIT_NONE ) {
          lb_get_item( hwnd, LB_DECPLUG, i, name, sizeof( name ));
          pg_get_info( name, PLUGIN_DECODER, &info );
          enabled = pg_is_enabled( name, PLUGIN_DECODER );
          snprintf( buffer, sizeof( buffer ), "Author: %s", info.author );
          WinSetDlgItemText( hwnd, ST_DEC_AUTHOR, buffer );
          snprintf( buffer, sizeof( buffer ), "Desc: %s", info.desc );
          WinSetDlgItemText( hwnd, ST_DEC_DESC, buffer );
          WinSetDlgItemText( hwnd, PB_DEC_ENABLE, enabled ? "Disabl~e" : "~Enable" );
          WinEnableControl ( hwnd, PB_DEC_ENABLE, TRUE );
          WinEnableControl ( hwnd, PB_DEC_UNLOAD, TRUE );
          WinEnableControl ( hwnd, PB_DEC_CONFIG, info.configurable && enabled );
        } else {
          WinSetDlgItemText( hwnd, ST_DEC_AUTHOR, "Author: <none>" );
          WinSetDlgItemText( hwnd, ST_DEC_DESC, "Desc: <none>" );
          WinSetDlgItemText( hwnd, PB_DEC_ENABLE, "~Enable" );
          WinEnableControl ( hwnd, PB_DEC_CONFIG, FALSE );
          WinEnableControl ( hwnd, PB_DEC_ENABLE, FALSE );
          WinEnableControl ( hwnd, PB_DEC_UNLOAD, FALSE );
        }
      }
      return 0;
    }

    case WM_INITDLG:
      WinSendMsg( hwnd, CFG_REFRESH_LIST,
                  MPFROMLONG( PLUGIN_VISUAL | PLUGIN_DECODER ), 0 );
      return 0;

    case WM_CONTROL:
      if( SHORT2FROMMP( mp1 ) == LN_SELECT )
      {
        int i = WinQueryLboxSelectedItem((HWND)mp2);

        if( SHORT1FROMMP( mp1 ) == LB_VISPLUG ) {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_VISUAL  ), MPFROMSHORT( i ));
        } else if( SHORT1FROMMP( mp1 ) == LB_DECPLUG ) {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_DECODER ), MPFROMSHORT( i ));
        }
      }
      break;

    case WM_COMMAND:
      switch( COMMANDMSG( &msg )->cmd )
      {
        case PB_VIS_ADD:
          cfg_add_plugin( hwnd, PLUGIN_VISUAL );
          return 0;

        case PB_VIS_CONFIG:
        {
          SHORT i = lb_cursored( hwnd, LB_VISPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item ( hwnd, LB_VISPLUG, i, name, sizeof( name ));
            pg_configure( name, PLUGIN_VISUAL, hwnd );
          }
          return 0;
        }

        case PB_VIS_ENABLE:
        {
          SHORT i = lb_cursored( hwnd, LB_VISPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item( hwnd, LB_VISPLUG, i, name, sizeof( name ));
            if( pg_is_enabled( name, PLUGIN_VISUAL )) {
              pg_enable( name, PLUGIN_VISUAL, FALSE );
              vis_terminate( name );
            } else {
              pg_enable( name, PLUGIN_VISUAL, TRUE  );
              vis_initialize( name, amp_player_window());
            }
            WinSendMsg( hwnd, CFG_REFRESH_INFO,
                        MPFROMLONG( PLUGIN_VISUAL ), MPFROMSHORT( i ));
          }
          return 0;
        }

        case PB_VIS_UNLOAD:
        {
          SHORT i = lb_cursored( hwnd, LB_VISPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item( hwnd, LB_VISPLUG, i, name, sizeof( name ));
            pg_remove_visual( name );
            if( lb_remove( hwnd, LB_VISPLUG, i ) == 0 ) {
              WinSendMsg( hwnd, CFG_REFRESH_INFO,
                          MPFROMLONG( PLUGIN_VISUAL ), MPFROMSHORT( LIT_NONE ));
            } else {
              lb_select( hwnd, LB_VISPLUG, 0 );
            }
          }
          return 0;
        }

        case PB_DEC_ADD:
          cfg_add_plugin( hwnd, PLUGIN_DECODER );
          return 0;

        case PB_DEC_CONFIG:
        {
          SHORT i = lb_cursored( hwnd, LB_DECPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item ( hwnd, LB_DECPLUG, i, name, sizeof( name ));
            pg_configure( name, PLUGIN_DECODER, hwnd );
          }
          return 0;
        }

        case PB_DEC_ENABLE:
        {
          SHORT i = lb_cursored( hwnd, LB_DECPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item( hwnd, LB_DECPLUG, i, name, sizeof( name ));
            if( !pg_is_enabled( name, PLUGIN_DECODER )) {
              pg_enable( name, PLUGIN_DECODER, TRUE );
            } else {
              if( dec_is_active( name )) {
                if( decoder_playing()) {
                  amp_show_error( "Cannot disable currently in use decoder." );
                } else {
                  pg_enable( name, PLUGIN_DECODER, FALSE );
                  dec_set_active( NULL );
                }
              } else {
                pg_enable( name, PLUGIN_DECODER, FALSE );
              }
            }
            WinSendMsg( hwnd, CFG_REFRESH_INFO,
                        MPFROMLONG( PLUGIN_DECODER ), MPFROMSHORT( i ));
          }
          return 0;
        }

        case PB_DEC_UNLOAD:
        {
          SHORT i = lb_cursored( hwnd, LB_DECPLUG );
          char  name[64];

          if( i != LIT_NONE )
          {
            lb_get_item( hwnd, LB_DECPLUG, i, name, sizeof( name ));

            if( !is_stopped() && dec_is_active( name )) {
              amp_show_error( "Cannot unload currently used decoder." );
            } else {
              pg_remove_decoder( name );
              if( lb_remove( hwnd, LB_DECPLUG, i ) == 0 ) {
                WinSendMsg( hwnd, CFG_REFRESH_INFO,
                            MPFROMLONG( PLUGIN_DECODER ), MPFROMSHORT( LIT_NONE ));
              } else {
                lb_select( hwnd, LB_DECPLUG, 0 );
              }
            }
          }
          return 0;
        }

        case PB_DEC_UP:
        {
          SHORT i = lb_cursored( hwnd, LB_DECPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item( hwnd, LB_DECPLUG, i, name, sizeof( name ));
            if( pg_move_decoder( name, MOVE_UP )) {
              lb_move_item( hwnd, LB_DECPLUG, i, MOVE_UP );
            }
          }
          return 0;
        }

        case PB_DEC_DOWN:
        {
          SHORT i = lb_cursored( hwnd, LB_DECPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item( hwnd, LB_DECPLUG, i, name, sizeof( name ));
            if( pg_move_decoder( name, MOVE_DOWN )) {
              lb_move_item( hwnd, LB_DECPLUG, i, MOVE_DOWN );
            }
          }
          return 0;
        }

        default:
          return 0;
      }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the plug-ins page 2 of the setup notebook.
 */

static MRESULT EXPENTRY
cfg_page5_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case CFG_REFRESH_LIST:
    {
      if( LONGFROMMP(mp1) & PLUGIN_OUTPUT  )
      {
        lb_remove_all( hwnd, LB_OUTPLUG );
        pg_expand_to ( hwnd, LB_OUTPLUG, PLUGIN_OUTPUT );

        if( lb_count( hwnd, LB_OUTPLUG )) {
          lb_select( hwnd, LB_OUTPLUG, 0 );
        } else {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_OUTPUT ), MPFROMSHORT( LIT_NONE ));
        }
      }

      if( LONGFROMMP(mp1) & PLUGIN_FILTER )
      {
        lb_remove_all( hwnd, LB_FILPLUG );
        pg_expand_to ( hwnd, LB_FILPLUG, PLUGIN_FILTER );

        if( lb_count( hwnd, LB_FILPLUG )) {
          lb_select( hwnd, LB_FILPLUG, 0 );
        } else {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_FILTER ), MPFROMSHORT( LIT_NONE ));
        }
      }
      return 0;
    }

    case CFG_REFRESH_INFO:
    {
      SHORT i = SHORT1FROMMP(mp2);

      char buffer[256];
      char name[64];
      BOOL enabled;

      PLUGIN_QUERYPARAM info;

      if( LONGFROMMP(mp1) & PLUGIN_OUTPUT )
      {
        if( i != LIT_NONE ) {
          lb_get_item( hwnd, LB_OUTPLUG, i, name, sizeof( name ));
          pg_get_info( name, PLUGIN_OUTPUT, &info );
          snprintf( buffer, sizeof( buffer ), "Author: %s", info.author );
          WinSetDlgItemText( hwnd, ST_OUT_AUTHOR, buffer );
          snprintf( buffer, sizeof( buffer ), "Desc: %s", info.desc );
          WinSetDlgItemText( hwnd, ST_OUT_DESC, buffer );
          WinEnableControl ( hwnd, PB_OUT_UNLOAD, TRUE );
          WinEnableControl ( hwnd, PB_OUT_ACTIVATE, !out_is_active( name ));
          WinEnableControl ( hwnd, PB_OUT_CONFIG, info.configurable );
        } else {
          WinSetDlgItemText( hwnd, ST_OUT_AUTHOR, "Author: <none>" );
          WinSetDlgItemText( hwnd, ST_OUT_DESC, "Desc: <none>" );
          WinEnableControl ( hwnd, PB_OUT_CONFIG, FALSE );
          WinEnableControl ( hwnd, PB_OUT_ACTIVATE, FALSE );
          WinEnableControl ( hwnd, PB_OUT_UNLOAD, FALSE );
        }
      }
      if( LONGFROMMP(mp1) & PLUGIN_FILTER ) {
        if( i != LIT_NONE ) {
          lb_get_item( hwnd, LB_FILPLUG, i, name, sizeof( name ));
          pg_get_info( name, PLUGIN_FILTER, &info );
          enabled = pg_is_enabled( name, PLUGIN_FILTER );
          snprintf( buffer, sizeof( buffer ), "Author: %s", info.author );
          WinSetDlgItemText( hwnd, ST_FIL_AUTHOR, buffer );
          snprintf( buffer, sizeof( buffer ), "Desc: %s", info.desc );
          WinSetDlgItemText( hwnd, ST_FIL_DESC, buffer );
          WinSetDlgItemText( hwnd, PB_FIL_ENABLE, enabled ? "Disabl~e" : "~Enable" );
          WinEnableControl ( hwnd, PB_FIL_ENABLE, TRUE );
          WinEnableControl ( hwnd, PB_FIL_UNLOAD, TRUE );
          WinEnableControl ( hwnd, PB_FIL_CONFIG, info.configurable && enabled );
        } else {
          WinSetDlgItemText( hwnd, ST_FIL_AUTHOR, "Author: <none>" );
          WinSetDlgItemText( hwnd, ST_FIL_DESC, "Desc: <none>" );
          WinSetDlgItemText( hwnd, PB_FIL_ENABLE, "~Enable" );
          WinEnableControl ( hwnd, PB_FIL_CONFIG, FALSE );
          WinEnableControl ( hwnd, PB_FIL_ENABLE, FALSE );
          WinEnableControl ( hwnd, PB_FIL_UNLOAD, FALSE );
        }
      }
      return 0;
    }

    case WM_INITDLG:
      WinSendMsg( hwnd, CFG_REFRESH_LIST,
                  MPFROMLONG( PLUGIN_OUTPUT | PLUGIN_FILTER ), 0 );
      return 0;

    case WM_CONTROL:
      if( SHORT2FROMMP( mp1 ) == LN_SELECT )
      {
        int i = WinQueryLboxSelectedItem((HWND)mp2);

        if( SHORT1FROMMP( mp1 ) == LB_OUTPLUG ) {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_OUTPUT ), MPFROMSHORT( i ));
        } else if( SHORT1FROMMP( mp1 ) == LB_FILPLUG ) {
          WinSendMsg( hwnd, CFG_REFRESH_INFO,
                      MPFROMLONG( PLUGIN_FILTER ), MPFROMSHORT( i ));
        }
      }
      break;

    case WM_COMMAND:
      switch( COMMANDMSG( &msg )->cmd )
      {
        case PB_OUT_ADD:
          cfg_add_plugin( hwnd, PLUGIN_OUTPUT );
          return 0;

        case PB_OUT_CONFIG:
        {
          SHORT i = lb_cursored( hwnd, LB_OUTPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item ( hwnd, LB_OUTPLUG, i, name, sizeof( name ));
            pg_configure( name, PLUGIN_OUTPUT, hwnd );
          }
          return 0;
        }

        case PB_OUT_ACTIVATE:
        {
          SHORT i = lb_cursored( hwnd, LB_OUTPLUG );
          char  name[64];

          if( i != LIT_NONE )
          {
            lb_get_item( hwnd, LB_OUTPLUG, i, name, sizeof( name ));

            if( !is_stopped()) {
              amp_show_error( "Cannot change active output while playing." );
            } else {
              out_set_active( name );
              WinSendMsg( hwnd, CFG_REFRESH_INFO,
                          MPFROMLONG( PLUGIN_OUTPUT ), MPFROMSHORT( i ));
            }
          }
          return 0;
        }

        case PB_OUT_UNLOAD:
        {
          SHORT i = lb_cursored( hwnd, LB_OUTPLUG );
          char  name[64];

          if( i != LIT_NONE )
          {
            lb_get_item( hwnd, LB_OUTPLUG, i, name, sizeof( name ));

            if( !is_stopped() && out_is_active( name )) {
              amp_show_error( "Cannot unload currently used output." );
            } else {
              pg_remove_output( name );
              if( lb_remove( hwnd, LB_OUTPLUG, i ) == 0 ) {
                WinSendMsg( hwnd, CFG_REFRESH_INFO,
                            MPFROMLONG( PLUGIN_OUTPUT ), MPFROMSHORT( LIT_NONE ));
              } else {
                lb_select( hwnd, LB_OUTPLUG, 0 );
              }
            }
          }
          return 0;
        }

        case PB_FIL_ADD:
          cfg_add_plugin( hwnd, PLUGIN_FILTER );
          return 0;

        case PB_FIL_CONFIG:
        {
          SHORT i = lb_cursored( hwnd, LB_FILPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item ( hwnd, LB_FILPLUG, i, name, sizeof( name ));
            pg_configure( name, PLUGIN_FILTER, hwnd );
          }
          return 0;
        }

        case PB_FIL_ENABLE:
        {
          SHORT i = lb_cursored( hwnd, LB_FILPLUG );
          char  name[64];

          if( i != LIT_NONE )
          {
            lb_get_item( hwnd, LB_FILPLUG, i, name, sizeof( name ));

            if( !pg_is_enabled( name, PLUGIN_FILTER )) {
              pg_enable( name, PLUGIN_FILTER, TRUE  );
              if( decoder_playing()) {
                amp_show_info( "This filter will be enabled only after cycle of a stop/start of the player." );
              }
            } else {
              pg_enable( name, PLUGIN_FILTER, FALSE );
              if( decoder_playing()) {
                amp_show_info( "This filter will be disabled only after cycle of a stop/start of the player." );
              }
            }
            WinSendMsg( hwnd, CFG_REFRESH_INFO,
                        MPFROMLONG( PLUGIN_FILTER ), MPFROMSHORT( i ));
          }
          return 0;
        }

        case PB_FIL_UNLOAD:
        {
          SHORT i = lb_cursored( hwnd, LB_FILPLUG );
          char  name[64];

          if( i != LIT_NONE )
          {
            lb_get_item( hwnd, LB_FILPLUG, i, name, sizeof( name ));

            if( !is_stopped()) {
              amp_show_error( "Cannot unload currently used filter." );
            } else {
              pg_remove_filter( name );
              if( lb_remove( hwnd, LB_FILPLUG, i ) == 0 ) {
                WinSendMsg( hwnd, CFG_REFRESH_INFO,
                            MPFROMLONG( PLUGIN_FILTER ), MPFROMSHORT( LIT_NONE ));
              } else {
                lb_select( hwnd, LB_FILPLUG, 0 );
              }
            }
          }
          return 0;
        }

        case PB_FIL_UP:
        {
          SHORT i = lb_cursored( hwnd, LB_FILPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item( hwnd, LB_FILPLUG, i, name, sizeof( name ));
            if( pg_move_filter( name, MOVE_UP )) {
              lb_move_item( hwnd, LB_FILPLUG, i, MOVE_UP );
            }
          }
          return 0;
        }

        case PB_FIL_DOWN:
        {
          SHORT i = lb_cursored( hwnd, LB_FILPLUG );
          char  name[64];

          if( i != LIT_NONE ) {
            lb_get_item( hwnd, LB_FILPLUG, i, name, sizeof( name ));
            if( pg_move_filter( name, MOVE_DOWN )) {
              lb_move_item( hwnd, LB_FILPLUG, i, MOVE_DOWN );
            }
          }
          return 0;
        }

        default:
          return 0;
      }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the association page of the setup notebook.
 */

static MRESULT EXPENTRY
cfg_page6_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static HWND hmenu;
  static ASSORECORD* rec = NULL;

  switch( msg ) {
    case WM_COMMAND:
      switch( SHORT1FROMMP(mp1)) {
        case IDM_ASSO_TOGGLE:
          if( rec->rc.flRecordAttr & CRA_INUSE ) {
            asso_remove( rec->file );
          } else {
            asso_create( rec->file );
          }

          WinSendDlgItemMsg( hwnd, CNR_ASSO, CM_SETRECORDEMPHASIS, MPFROMP( rec ),
                             MPFROM2SHORT( asso_is_registered( rec->rc.pszIcon ), CRA_INUSE ));

          WinSetFocus( HWND_DESKTOP, WinWindowFromID( hwnd, CNR_ASSO ));
          restart_wps = TRUE;
          break;

        case IDM_ASSO_ALL:
        case IDM_ASSO_CLEAR:
        {
          ASSORECORD* rec = (ASSORECORD*)WinSendDlgItemMsg( hwnd, CNR_ASSO, CM_QUERYRECORD, MPFROMP( NULL ),
                                                            MPFROM2SHORT( CMA_FIRST, CMA_ITEMORDER ));
          if( SHORT1FROMMP(mp1) == IDM_ASSO_ALL ) {
            asso_create_all();
          } else {
            asso_remove_all();
          }

          while( rec ) {
            WinSendDlgItemMsg( hwnd, CNR_ASSO, CM_SETRECORDEMPHASIS, MPFROMP( rec ),
                               MPFROM2SHORT( asso_is_registered( rec->rc.pszIcon ), CRA_INUSE ));

            rec = (ASSORECORD*)WinSendDlgItemMsg( hwnd, CNR_ASSO, CM_QUERYRECORD, MPFROMP( rec ),
                                                  MPFROM2SHORT( CMA_NEXT, CMA_ITEMORDER ));
          }

          WinSetFocus( HWND_DESKTOP, WinWindowFromID( hwnd, CNR_ASSO ));
          restart_wps = TRUE;
          break;
        }
      }
      return 0;

    case WM_MENUEND:
      if( rec ) {
        WinSendDlgItemMsg( hwnd, CNR_ASSO, CM_SETRECORDEMPHASIS, MPFROMP( rec ),
                           MPFROM2SHORT( FALSE, CRA_SOURCE ));
      }
      WinDestroyWindow( hmenu );
      return 0;

    case WM_CONTROL:

      if( SHORT1FROMMP( mp1 ) == CNR_ASSO ) {
        switch( SHORT2FROMMP( mp1 )) {
          case CN_CONTEXTMENU:
          {
            POINTL pos;
            SWP    swp;

            rec   = (ASSORECORD*)mp2;
            hmenu = WinLoadMenu( HWND_OBJECT, hmodule, rec ? MNU_ASSO_FILE : MNU_ASSO );

            WinQueryPointerPos( HWND_DESKTOP, &pos );
            WinMapWindowPoints( HWND_DESKTOP, hwnd, &pos, 1 );

            if( WinWindowFromPoint( hwnd, &pos, TRUE ) == NULLHANDLE )
            {
              // The context menu is probably activated from the keyboard.
              WinQueryWindowPos( hwnd, &swp );
              pos.x = swp.cx / 2;
              pos.y = swp.cy / 2;
            }

            if( rec ) {
              WinSendDlgItemMsg( hwnd, CNR_ASSO, CM_SETRECORDEMPHASIS, MPFROMP( rec ),
                                 MPFROM2SHORT( TRUE, CRA_SOURCE ));
              if( asso_is_registered( rec->rc.pszIcon )) {
                WinSendMsg( hmenu, MM_SETITEMTEXT, MPFROMSHORT( IDM_ASSO_TOGGLE ), "~Clear" );
              }
            }

            WinPopupMenu( hwnd, hwnd, hmenu, pos.x, pos.y, rec ? IDM_ASSO_TOGGLE : IDM_ASSO_ALL,
                          PU_POSITIONONITEM | PU_HCONSTRAIN   | PU_VCONSTRAIN |
                          PU_MOUSEBUTTON1   | PU_MOUSEBUTTON2 | PU_KEYBOARD   );
            break;
          }

          case CN_ENTER:
          {
            PNOTIFYRECORDENTER pnotify = (PNOTIFYRECORDENTER)mp2;
            rec = (ASSORECORD*)pnotify->pRecord;

            if( rec ) {
              WinSendMsg( hwnd, WM_COMMAND, MPFROMSHORT( IDM_ASSO_TOGGLE ),
                                            MPFROM2SHORT( CMDSRC_OTHER, FALSE ));
            }
            break;
          }
        }
      } else if( SHORT1FROMMP( mp1 ) == CB_ASSO_INTEGRATE ) {
        if( SHORT2FROMMP( mp1 ) == BN_CLICKED ) {
          asso_integrate( WinQueryButtonCheckstate( hwnd, CB_ASSO_INTEGRATE ));
        }
      }
      return 0;

    case WM_DESTROY:
      asso_term( WinWindowFromID( hwnd, CNR_ASSO ));
      return 0;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the about page of the setup notebook.
 */

static MRESULT EXPENTRY
cfg_page7_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case WM_COMMAND:
    case WM_CONTROL:
      return 0;

    case WM_INITDLG:
      WinSetDlgItemText( hwnd, ST_AUTHORS, SDG_AUT );
      WinSetDlgItemText( hwnd, ST_CREDITS, SDG_MSG );
      WinSetDlgItemText( hwnd, ST_TITLE1,  AMP_FULLNAME );
      return 0;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the setup dialog.
 */

static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case WM_COMMAND:
      switch( COMMANDMSG( &msg )->cmd )
      {
        case PB_UNDO:
        case PB_DEFAULT:
        {
          LONG id;
          HWND page = NULLHANDLE;

          id = (LONG)WinSendDlgItemMsg( hwnd, NB_CONFIG, BKM_QUERYPAGEID, 0,
                                              MPFROM2SHORT(BKA_TOP,BKA_MAJOR));

          if( id && id != BOOKERR_INVALID_PARAMETERS ) {
              page = (HWND)WinSendDlgItemMsg( hwnd, NB_CONFIG,
                                              BKM_QUERYPAGEWINDOWHWND, MPFROMLONG(id), 0 );
          }

          if( page && page != BOOKERR_INVALID_PARAMETERS ) {
            WinSendMsg( page,
              COMMANDMSG( &msg )->cmd == PB_UNDO ? CFG_UNDO : CFG_DEFAULT, 0, 0 );
          }
          return MRFROMLONG(1L);
        }

        case PB_HELP:
          amp_show_help( IDH_PROPERTIES );
          return 0;
      }
      return 0;

    case WM_DESTROY:
      save_window_pos( hwnd, WIN_MAP_POINTS );
      return 0;

    case WM_WINDOWPOSCHANGED:
      if(((PSWP)mp1)[0].fl & SWP_SIZE ) {
        nb_adjust( hwnd );
      }
      break;

    case WM_CONTROL:
      if( SHORT1FROMMP(mp1) == NB_CONFIG && SHORT2FROMMP(mp1) == BKN_PAGESELECTED )
      {
        PAGESELECTNOTIFY* pn = (PAGESELECTNOTIFY*)mp2;
        HWND hwndPage = (HWND)WinSendMsg( pn->hwndBook, BKM_QUERYPAGEWINDOWHWND,
                                                        MPFROMLONG( pn->ulPageIdNew ), 0 );

        // Transfers focus to a first tabbed dialog control.
        if( hwndPage != NULLHANDLE )
        {
          HWND hwndFocus = WinEnumDlgItem( hwndPage, NULLHANDLE, EDI_FIRSTTABITEM );
          WinSetFocus( HWND_DESKTOP, hwndFocus );
        }
        if( hwndPage == page06 ) {
          asso_init( WinWindowFromID( hwndPage, CNR_ASSO ));
          WinEnableControl( hwndPage, CB_ASSO_INTEGRATE, asso_can_integrated());
          WinCheckButton( hwndPage, CB_ASSO_INTEGRATE, asso_is_integrated());
        }
      }
      break;

    case WM_TRANSLATEACCEL:
    {
      // Check for SV_CONTEXTMENUKB translation
      // Low word is the virtual key code (VK_*)
      // High word is the keyboard control code (KC_*)

      PQMSG pqmsg = (PQMSG)mp1;
      ULONG svKey = WinQuerySysValue( HWND_DESKTOP, SV_CONTEXTMENUKB );

      // Because the keyboard does not really send the exact same
      // keyboard control codes, we need to do the checking in
      // several stages:
      //
      //  - bits 0 thru 2 must have the desired bits turned on, but
      //    may also have others (KC_CHAR, KC_VIRTUALKEY, KC_SCANCODE)
      //  - bits 3 thru 7 must match (KC_SHIFT, KC_CTRL, KC_ALT,
      //    KC_KEYUP, KC_KEYDOWN)
      //  - bits 8 thru 15 can be ignored
      //  - the virtual key (VK_*) values must match

      if(( SHORT1FROMMP( pqmsg->mp1 ) & SHORT2FROMMP( svKey ) & 0x07 ) &&
        (( SHORT1FROMMP( pqmsg->mp1 ) & 0xF8 ) == ( SHORT2FROMMP( svKey ) & 0xF8 )) &&
         ( SHORT2FROMMP( pqmsg->mp2 ) == SHORT1FROMMP( svKey )))
      {
        // This is a popup menu request from the keyboard
        pqmsg->msg  = WM_CONTEXTMENU;
        pqmsg->mp1  = 0;
        pqmsg->mp2  = MPFROMLONG( 0x10000 );
        return MRFROMLONG( TRUE );
      }
      break;
    }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Creates the properties dialog.
 */

void
cfg_properties( HWND howner )
{
  HWND hwnd;
  HWND book;

  MRESULT id;
  char built[512];

  restart_wps = FALSE;

  hwnd = WinLoadDlg( HWND_DESKTOP, howner, cfg_dlg_proc, hmodule, DLG_CONFIG, 0 );
  do_warpsans( hwnd );
  book = WinWindowFromID( hwnd, NB_CONFIG );
  do_warpsans( book );

  WinSendMsg( book, BKM_SETDIMENSIONS, MPFROM2SHORT( 100,25 ), MPFROMSHORT( BKA_MAJORTAB ));
  WinSendMsg( book, BKM_SETDIMENSIONS, MPFROMLONG( 0 ), MPFROMSHORT( BKA_MINORTAB ));
  WinSendMsg( book, BKM_SETNOTEBOOKCOLORS, MPFROMLONG ( SYSCLR_FIELDBACKGROUND ),
                                           MPFROMSHORT( BKA_BACKGROUNDPAGECOLORINDEX ));

  page01 = WinLoadDlg( book, book, cfg_page1_dlg_proc, hmodule, CFG_PAGE1, 0 );

  do_warpsans( page01 );
  do_warpsans( WinWindowFromID( page01, ST_PROXY_HOST ));
  do_warpsans( WinWindowFromID( page01, ST_PROXY_PORT ));
  do_warpsans( WinWindowFromID( page01, ST_PROXY_USER ));
  do_warpsans( WinWindowFromID( page01, ST_PROXY_PASS ));
  do_warpsans( WinWindowFromID( page01, ST_NO_PROXY   ));
  do_warpsans( WinWindowFromID( page01, ST_PIXELS     ));
  do_warpsans( WinWindowFromID( page01, ST_BUFFERSIZE ));
  do_warpsans( WinWindowFromID( page01, ST_KB         ));
  do_warpsans( WinWindowFromID( page01, ST_FILLBUFFER ));
  do_warpsans( WinWindowFromID( page01, ST_TIMEOUT    ));

  id = WinSendMsg( book, BKM_INSERTPAGE, 0,
                   MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MAJOR | BKA_STATUSTEXTON, BKA_FIRST ));

  WinSendMsg( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG( id ), MPFROMLONG( page01 ));
  WinSendMsg( book, BKM_SETTABTEXT, MPFROMLONG( id ), MPFROMP( "~Settings" ));

  page02 = WinLoadDlg( book, book, cfg_page2_dlg_proc, hmodule, CFG_PAGE2, 0 );

  do_warpsans( page02 );
  do_warpsans( WinWindowFromID( page02, ST_CHARSET    ));

  id = WinSendMsg( book, BKM_INSERTPAGE, 0,
                   MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MAJOR | BKA_MINOR | BKA_STATUSTEXTON, BKA_LAST ));

  WinSendMsg( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG( id ), MPFROMLONG( page02 ));
  WinSendMsg( book, BKM_SETTABTEXT, MPFROMLONG( id ), MPFROMP( "~Display" ));
  WinSendMsg( book, BKM_SETSTATUSLINETEXT, MPFROMLONG( id ), MPFROMP( "Page 1 of 2" ));

  page03 = WinLoadDlg( book, book, cfg_page3_dlg_proc, hmodule, CFG_PAGE3, 0 );

  do_warpsans( page03 );
  do_warpsans( WinWindowFromID( page03, ST_SHOW_DELAY ));

  id = WinSendMsg( book, BKM_INSERTPAGE, MPFROMLONG( id ),
                   MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MINOR | BKA_STATUSTEXTON, BKA_NEXT ));

  WinSendMsg( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG( id ), MPFROMLONG( page03 ));
  WinSendMsg( book, BKM_SETSTATUSLINETEXT, MPFROMLONG( id ), MPFROMP( "Page 2 of 2" ));

  page04 = WinLoadDlg( book, book, cfg_page4_dlg_proc, hmodule, CFG_PAGE4, 0 );

  do_warpsans( page04 );
  do_warpsans( WinWindowFromID( page04, ST_VIS_AUTHOR ));
  do_warpsans( WinWindowFromID( page04, ST_VIS_DESC   ));
  do_warpsans( WinWindowFromID( page04, ST_DEC_AUTHOR ));
  do_warpsans( WinWindowFromID( page04, ST_DEC_DESC   ));

  id = WinSendMsg( book, BKM_INSERTPAGE, 0,
                   MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MAJOR | BKA_MINOR | BKA_STATUSTEXTON, BKA_LAST ));

  WinSendMsg( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG( id ), MPFROMLONG( page04 ));
  WinSendMsg( book, BKM_SETTABTEXT, MPFROMLONG( id ), MPFROMP( "~Plug-Ins" ));
  WinSendMsg( book, BKM_SETSTATUSLINETEXT, MPFROMLONG( id ), MPFROMP( "Page 1 of 2" ));

  page05 = WinLoadDlg( book, book, cfg_page5_dlg_proc, hmodule, CFG_PAGE5, 0 );

  do_warpsans( page05 );
  do_warpsans( WinWindowFromID( page05, ST_FIL_AUTHOR ));
  do_warpsans( WinWindowFromID( page05, ST_FIL_DESC   ));
  do_warpsans( WinWindowFromID( page05, ST_OUT_AUTHOR ));
  do_warpsans( WinWindowFromID( page05, ST_OUT_DESC   ));

  id = WinSendMsg( book, BKM_INSERTPAGE, MPFROMLONG( id ),
                   MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MINOR | BKA_STATUSTEXTON, BKA_NEXT ));

  WinSendMsg( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG( id ), MPFROMLONG( page05 ));
  WinSendMsg( book, BKM_SETSTATUSLINETEXT, MPFROMLONG( id ), MPFROMP( "Page 2 of 2" ));

  page06 = WinLoadDlg( book, book, cfg_page6_dlg_proc, hmodule, CFG_PAGE6, 0 );

  do_warpsans( page06 );

  id = WinSendMsg( book, BKM_INSERTPAGE, 0,
                   MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MAJOR | BKA_STATUSTEXTON, BKA_LAST ));

  WinSendMsg( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG( id ), MPFROMLONG( page06 ));
  WinSendMsg( book, BKM_SETTABTEXT, MPFROMLONG( id ), MPFROMP( "~Associations" ));

  page07 = WinLoadDlg( book, book, cfg_page7_dlg_proc, hmodule, CFG_ABOUT, 0 );

  do_warpsans( page07 );
  do_warpsans( WinWindowFromID( page07, ST_TITLE2 ));
  do_warpsans( WinWindowFromID( page07, ST_BUILT  ));

  #if defined(__IBMC__)
    sprintf( built, "(built %s", __DATE__ );
    #if __IBMC__ <= 300
      strcat( built, " using IBM VisualAge C++ 3.08)" );
    #else
      strcat( built, " using IBM VisualAge C++ 3.6)"  );
    #endif
  #elif defined(__WATCOMC__)
    #if __WATCOMC__ < 1200
      sprintf( built, "(built %s using Watcom C++ %d.%d)",
               __DATE__, __WATCOMC__ / 100, __WATCOMC__ % 100 );
    #else
      sprintf( built, "(built %s using Open Watcom C++ %d.%d)",
               __DATE__, __WATCOMC__ / 100 - 11, __WATCOMC__ % 100 );
    #endif
  #elif defined(__GNUC__)
    #if __GNUC__ < 3
      sprintf( built, "(built " __DATE__ " using GNU C++ %d.%d)",
               __GNUC__, __GNUC_MINOR__ );
    #else
      sprintf( built, "(built " __DATE__ " using GNU C++ %d.%d.%d)",
               __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__ );
    #endif
  #else
    *built = 0;
  #endif

  WinSetDlgItemText( page07, ST_BUILT, built );

  id = WinSendMsg( book, BKM_INSERTPAGE, 0,
                   MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MAJOR, BKA_LAST ));

  WinSendMsg( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG( id ), MPFROMLONG( page07 ));
  WinSendMsg( book, BKM_SETTABTEXT, MPFROMLONG( id ), MPFROMP( "~About" ));

  // Prevents closing of a owner window if this window is closed via Alt+F4 keys.
  WinSetOwner( page01, book );
  WinSetOwner( page02, book );
  WinSetOwner( page03, book );
  WinSetOwner( page04, book );
  WinSetOwner( page05, book );
  WinSetOwner( page06, book );
  WinSetOwner( page07, book );

  rest_window_pos( hwnd, WIN_MAP_POINTS );
  nb_adjust( hwnd );
  WinSetFocus( HWND_DESKTOP, WinWindowFromID( page01, CB_PLAYONLOAD ));

  WinProcessDlg( hwnd );
  WinDestroyWindow( hwnd );

  save_ini();

  if( cfg.upnp ) {
    upnp_init();
  } else {
    upnp_term();
  }

  if( restart_wps ) {
    if( amp_query( howner, "The made changes can require restart of the WPS. To restart the WPS?" )) {
      WinRestartWorkplace();
    }
  }
}

