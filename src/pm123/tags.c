/*
 * Copyright 2007-2021 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#define  INCL_PM
#include <os2.h>
#include <process.h>
#include <debuglog.h>
#include <gbm.h>

#include <decoder_plug.h>
#include <queue.h>
#include <xio.h>

#include "pm123.h"
#include "plugman.h"
#include "assertions.h"
#include "genre.h"
#include "playlist.h"
#include "picture.h"
#include "bitmap.h"
#include "tags.h"
#include "filedlg.h"

#define  TAG_UPDATE    0
#define  TAG_TERMINATE 1

static   TID    broker_tid   = 0;
static   PQUEUE broker_queue = NULL;

#define  KEEP "< keep >"
#define  DIFFERENT "< Different images >"

static char *picture_types[] =
{
  /* 0x00 */ "Other",
  /* 0x01 */ "File icon (PNG only)",
  /* 0x02 */ "Other file icon",
  /* 0x03 */ "Cover (front)",
  /* 0x04 */ "Cover (back)",
  /* 0x05 */ "Leaflet page",
  /* 0x06 */ "Media (e.g. label side of CD)",
  /* 0x07 */ "Lead artist/lead performer/soloist",
  /* 0x08 */ "Artist/performer",
  /* 0x09 */ "Conductor",
  /* 0x0A */ "Band/Orchestra",
  /* 0x0B */ "Composer",
  /* 0x0C */ "Lyricist/text writer",
  /* 0x0D */ "Recording Location",
  /* 0x0E */ "During recording",
  /* 0x0F */ "During performance",
  /* 0x10 */ "Movie/video screen capture",
  /* 0x11 */ "A bright coloured fish",
  /* 0x12 */ "Illustration",
  /* 0x13 */ "Band/artist logotype",
  /* 0x14 */ "Publisher/Studio logotype"
};

#define DECODER_PICS_TYPES 0xC0000000UL

/* Dispatches the tags management requests. */
static void TFNENTRY
tag_broker( void* dummy )
{
  ULONG request;
  PVOID reqdata;
  HAB   hab = WinInitialize( 0 );
  HMQ   hmq = WinCreateMsgQueue( hab, 0 );
  ULONG rc;

  while( qu_read( broker_queue, &request, &reqdata ))
  {
    AMP_FILE* file = (AMP_FILE*)reqdata;

    switch( request ) {
      case TAG_UPDATE:
      {
        int opts = 0;

        if( file->info.pics_count == -1 ) {
          DEBUGLOG(( "pm123: keep pics of %s\n", file->filename ));
          file->info.pics_count = 0;
        } else {
          DEBUGLOG(( "pm123: request save %d pics to %s\n", file->info.pics_count, file->filename ));
          opts = DECODER_SAVEPICS;
        }

        pl_refresh_songname( file->filename, "Updating..." );
        rc = dec_saveinfo( file->filename, &file->info, file->decoder, opts );
        if( rc == PLUGIN_OK ) {
          dec_fileinfo( file->filename, &file->info, file->decoder, 0 );
          WinSendMsg( amp_player_window(), WM_123FILE_REFRESH, file, 0 );
        } else {
          pl_refresh_songname( file->filename, "Can't update file info" );
        }
        amp_cleaninfo( file );
        break;
      }
    }

    if( file ) {
      free( file );
    }

    if( request == TAG_TERMINATE ) {
      break;
    }
  }

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
  _endthread();
}

/* Processes messages of the tech information dialog.
   Must be called from the main thread. */
static MRESULT EXPENTRY
tag_tech_info_page_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_COMMAND:
      WinDismissDlg((HWND)WinSendMsg( hwnd, WM_QUERYFOCUSCHAIN, MPFROMSHORT( QFC_FRAME ), 0 ),
                    COMMANDMSG( &msg )->cmd );
      return 0;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the song information dialog.
   Must be called from the main thread. */
static MRESULT EXPENTRY
tag_edit_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case WM_SYSCOMMAND:
      if( SHORT1FROMMP( mp1 ) == SC_CLOSE ) {
        WinDismissDlg( hwnd, DID_CANCEL );
        return 0;
      }
      break;

    case WM_CONTROL:
      if( SHORT1FROMMP(mp1) == NB_FILEINFO && SHORT2FROMMP(mp1) == BKN_PAGESELECTED )
      {
        PAGESELECTNOTIFY* pn = (PAGESELECTNOTIFY*)mp2;
        HWND hwndPage = (HWND)WinSendMsg( pn->hwndBook, BKM_QUERYPAGEWINDOWHWND,
                                                        MPFROMLONG( pn->ulPageIdNew ), 0 );

        // Transfers focus to a first tabbed dialog control.
        if( hwndPage != NULLHANDLE )
        {
          HWND hwndFocus = WinEnumDlgItem( hwndPage, NULLHANDLE, EDI_FIRSTTABITEM );
          WinSetFocus( HWND_DESKTOP, hwndFocus );
        }
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Inserts the tag value in the combination-box,
   if it is not already in it. */
static void
tag_add_value( HWND hwnd, SHORT id, const char* ptag )
{
  if((SHORT)(LONG)WinSendDlgItemMsg( hwnd, id, LM_SEARCHSTRING,
                                     MPFROM2SHORT( LSS_CASESENSITIVE, LIT_FIRST ),
                                     MPFROMP( ptag )) == LIT_NONE )
  {
    WinSendDlgItemMsg( hwnd, id, LM_INSERTITEM,
                       MPFROMSHORT( LIT_SORTASCENDING ), MPFROMP( ptag ));
  }
}

/* Selects appropriate defaul tag value. */
static void
tag_select_default_value( HWND hwnd, SHORT id )
{
  switch( lb_count( hwnd, id )) {
    case 1:
      // All tags are identical. Select this tag if it is not empty.
      if( lb_get_item_size( hwnd, id, 0 ) == 0 ) {
        lb_remove( hwnd, id, 0 );
      } else {
        lb_select( hwnd, id, 0 );
      }

      lb_ins_item( hwnd, id, 0, KEEP );
      break;

    default:
      // Tags are not identical. Select "keep" item.
      if( lb_get_item_size( hwnd, id, 0 ) == 0 ) {
        lb_remove( hwnd, id, 0 );
      }

      lb_ins_item( hwnd, id, 0, KEEP );
      lb_select( hwnd, id, 0 );
      break;
  }
}

/* Sets the enable state of the all related to picture controls
   to the enable flag. */
static void
tag_enable_picture_controls( HWND hwnd, BOOL enable )
{
  en_readonly( hwnd, EN_PIC_INFO, TRUE );
  cb_readonly( hwnd, CB_PIC_TYPE, !enable );
  en_readonly( hwnd, EN_PIC_DESC, !enable );

  WinEnableControl( hwnd, PB_PIC_ADD,     enable );
  WinEnableControl( hwnd, PB_PIC_CLEAR,   enable );
  WinEnableControl( hwnd, PB_PIC_EXTRACT, enable );
  WinEnableControl( hwnd, PB_PIC_PREV,    enable );
  WinEnableControl( hwnd, PB_PIC_NEXT,    enable );
}

/* Refreshes displayed information about selected picture. */
static void
tag_refresh_picture_controls( HWND hwnd )
{
  HWND  hpics  = WinWindowFromID( hwnd, PC_COVER );
  ULONG count  = (ULONG)WinSendMsg( hpics, WM_PIC_COUNT, 0, 0 );
  ULONG select = (ULONG)WinSendMsg( hpics, WM_PIC_SELECTED, 0, 0 );
  APIC* pic    = WinSendMsg( hpics, WM_PIC_GETPIC, MPFROMLONG( select ), 0 );
  char  info[128];

  snprintf( info, sizeof( info ), "%ld of %ld", count ? select + 1 : 0, count );
  WinSetDlgItemText( hwnd, ST_PIC_SELECTED, info );

  if( pic )
  {
    int  w = 0;
    int  h = 0;
    char size[128];

    bmp_query_bitmap_size( pic->mimetype, (char*)pic + sizeof(APIC),
                           pic->size - sizeof(APIC), &w, &h );

    snprintf( info, sizeof( info ), "%s, %dx%d, %s", pic->mimetype, w, h,
              readable_file_size( pic->size - sizeof( APIC ), size, sizeof( size )));

    WinSetDlgItemText( hwnd, EN_PIC_INFO, info );
    WinSetDlgItemText( hwnd, EN_PIC_DESC, pic->desc );
    lb_select_by_handle( hwnd, CB_PIC_TYPE, pic->type );
  } else {
    WinSetDlgItemText( hwnd, EN_PIC_INFO, "" );
    WinSetDlgItemText( hwnd, EN_PIC_DESC, "" );
    WinSetDlgItemText( hwnd, CB_PIC_TYPE, "" );
  }

  WinEnableControl( hwnd, PB_PIC_NEXT, count - select > 1 );
  WinEnableControl( hwnd, PB_PIC_PREV, select > 0 );
}

/* Updates edited information about selected picture before
   switch to another picture. */
static void
tag_update_picture( HWND hwnd )
{
  HWND  hpics  = WinWindowFromID( hwnd, PC_COVER );
  ULONG select = (ULONG)WinSendMsg( hpics, WM_PIC_SELECTED, 0, 0 );
  APIC* pic    = WinSendMsg( hpics, WM_PIC_GETPIC, MPFROMLONG( select ), 0 );

  if( pic ) {
    if( !en_is_readonly( hwnd, EN_PIC_DESC )) {
      WinQueryDlgItemText( hwnd, EN_PIC_DESC, sizeof( pic->desc ), pic->desc );
    }
    if( !cb_is_readonly( hwnd, CB_PIC_TYPE )) {
      pic->type = lb_get_handle( hwnd, CB_PIC_TYPE, lb_cursored( hwnd, CB_PIC_TYPE ));
    }
  }
}

/* Clears the list of attached pictures. */
static void
tag_clear_pictures( HWND hwnd, BOOL invalidate )
{
  HWND   hpics = WinWindowFromID( hwnd, PC_COVER );
  ULONG  count = (ULONG)WinSendMsg( hpics, WM_PIC_COUNT, 0, 0 );
  APIC** pics  = WinSendMsg( hpics, WM_PIC_GETPICS, 0, 0 );

  WinSendMsg( hpics, WM_PIC_SETPICS, NULL, 0 );

  if( pics ) {
    dec_freepics( pics, count );
  }

  if( invalidate ) {
    tag_enable_picture_controls( hwnd, FALSE );
    // If pictures can be cleaned, then this means that its can be edited.
    WinEnableControl( hwnd, PB_PIC_ADD, TRUE );
    tag_refresh_picture_controls( hwnd );
  }
}

/* Use small chunks of saved data because too big chunk size produces ERROR_PMM_LOCK_FAILED
   at DosWrite on FAT32 IFS. */
static BOOL
tag_write_picture( FILE* file, APIC* pic )
{
  int   chunk = 0;
  int   size  = pic->size  - sizeof(APIC);
  char* p     = (char*)pic + sizeof(APIC);

  while( size ) {
    chunk = size < 65535 ? size : 65535;

    if( fwrite( p, 1, chunk, file ) != chunk ) {
      return FALSE;
    }

    p    += chunk;
    size -= chunk;
  }

  return TRUE;
}

/* Extracts the current selected picture. */
static void
tag_extract_picture( HWND hwnd )
{
  HWND      hpics  = WinWindowFromID( hwnd, PC_COVER );
  ULONG     select = (ULONG)WinSendMsg( hpics, WM_PIC_SELECTED, 0, 0 );
  APIC*     pic    = WinSendMsg( hpics, WM_PIC_GETPIC, MPFROMLONG( select ), 0 );
  AMP_FILE* tags   = (AMP_FILE*)WinQueryWindowPtr( hwnd, 0 );

  if( pic )
  {
    FILEDLG filedialog;
    char    type[64];
    char*   p;

    memset( &filedialog, 0, sizeof( FILEDLG ));

    filedialog.cbSize   = sizeof( FILEDLG );
    filedialog.fl       = FDS_CENTER | FDS_SAVEAS_DIALOG | FDS_ENABLEFILELB;
    filedialog.pszTitle = "Save image";

    if( is_file( tags[0].filename )) {
      sdrivedir( filedialog.szFullFile, tags[0].filename, sizeof( filedialog.szFullFile ));
    } else {
      strcpy( filedialog.szFullFile, cfg.apicdir );
    }

    strlcat( filedialog.szFullFile, "cover.", sizeof( filedialog.szFullFile ));

    strlcpy( type, pic->mimetype, sizeof( type ));
    if(( p = strpbrk( type, "/")) != NULL ) {
      if( stricmp( ++p, "jpeg" ) == 0 ) {
        strlcat( filedialog.szFullFile, "jpg", sizeof( filedialog.szFullFile ));
      } else {
        strlcat( filedialog.szFullFile, strlwr( p ), sizeof( filedialog.szFullFile ));
      }
    }

    amp_file_dlg( HWND_DESKTOP, hwnd, &filedialog );

    if( filedialog.lReturn == DID_OK ) {
      if( amp_warn_if_overwrite( hwnd, filedialog.szFullFile ))
      {
        FILE* file = fopen( filedialog.szFullFile, "wb" );

        if( file ) {
          if( !tag_write_picture( file, pic )) {
            amp_show_error( "Unable write to file:\n%s\n", filedialog.szFullFile );
          } else {
            if( !is_file( tags[0].filename )) {
              sdrivedir( cfg.apicdir, filedialog.szFullFile, sizeof( cfg.apicdir ));
            }
          }
          fclose( file );
        } else {
          amp_show_error( "Unable create file:\n%s\n", filedialog.szFullFile );
        }
      }
    }
  }
}

/* Returns the MIME type of a bitmap file. */
BOOL
tag_query_bitmap_mimetype( const char* filename, APIC* pic )
{
  unsigned char sigs[6][2][16] = {{{ 3, 0x49, 0x49, 0x2a }, "image/tiff" },
                                  {{ 2, 0x42, 0x4D       }, "image/bmp"  },
                                  {{ 2, 0x42, 0x41       }, "image/bmp"  },
                                  {{ 3, 0x47, 0x49, 0x46 }, "image/gif"  },
                                  {{ 3, 0xFF, 0xD8, 0xFF }, "image/jpeg" },
                                  {{ 3, 0x89, 0x50, 0x4E }, "image/png"  }};
  int   filetype;
  GBMFT ft;
  int   i, j;
  BYTE* data = (BYTE*)pic + sizeof(APIC);
  int   size = pic->size - sizeof(APIC);

  for( i = 0; i < 5; i++ ) {
    if( sigs[i][0][0] <= size )
    {
      BOOL found = TRUE;

      for( j = 1; j <= sigs[i][0][0]; j++ ) {
        if( data[j-1] != sigs[i][0][j] ) {
          found = FALSE;
          break;
        }
      }

      if( found ) {
        strcpy( pic->mimetype, (char*)sigs[i][1] );
        return TRUE;
      }
    }
  }

  if( gbm_guess_filetype( filename, &filetype ) != GBM_ERR_OK ||
      gbm_query_filetype( filetype, &ft ) != GBM_ERR_OK )
  {
    amp_show_error( "Unable deduce bitmap format from file extension:\n%s\n", filename );
    return FALSE;
  }

  strcpy( pic->mimetype, "image/" );
  strcat( pic->mimetype, ft.short_name );
  strlwr( pic->mimetype );

  return TRUE;
}

/* Reads the new picture. */
APIC*
tag_read_picture( const char* filename )
{
  XFILE* file = xio_fopen( filename, "rb" );
  APIC*  pic;

  if( file ) {
    int size = xio_fsize( file );

    if(( pic  = malloc( sizeof( APIC ) + size )) != NULL ) {
      memset( pic, 0, sizeof( APIC ));
      if( xio_fread((BYTE*)pic + sizeof(APIC), 1, size, file ) == size )
      {
        pic->size = size + sizeof( APIC );
        pic->type = PIC_COVERFRONT;

        if( tag_query_bitmap_mimetype( filename, pic )) {
          xio_fclose( file );
          return pic;
        }
      }
      free( pic );
    }
    xio_fclose( file );
  }

  return NULL;
}

/* Add the new picture. */
static void
tag_add_picture( HWND hwnd )
{
  FILEDLG filedialog;
  APSZ    types[] = {{ FDT_IMAGE }, { 0 }};

  memset( &filedialog, 0, sizeof( FILEDLG ));

  filedialog.cbSize         = sizeof( FILEDLG );
  filedialog.fl             = FDS_CENTER | FDS_OPEN_DIALOG;
  filedialog.pszTitle       = "Add image";
  filedialog.papszITypeList = types;
  filedialog.pszIType       = FDT_IMAGE;

  strcpy( filedialog.szFullFile, cfg.apicdir );
  amp_file_dlg( HWND_DESKTOP, hwnd, &filedialog );

  if( filedialog.lReturn == DID_OK )
  {
    APIC* pic = tag_read_picture( filedialog.szFullFile );
    sdrivedir( cfg.apicdir, filedialog.szFullFile, sizeof( cfg.apicdir ));

    if( pic )
    {
      HWND   hpics = WinWindowFromID( hwnd, PC_COVER );
      ULONG  count = (ULONG)WinSendMsg( hpics, WM_PIC_COUNT, 0, 0 );
      APIC** pics  = WinSendMsg( hpics, WM_PIC_GETPICS, 0, 0 );

      tag_update_picture( hwnd );
      if(( pics = realloc( pics, sizeof(APIC*) * ( count + 1 ))) != NULL )
      {
        pics[count] = pic;
        WinSendMsg( hpics, WM_PIC_SETPICS, MPFROMP( pics ), MPFROMLONG( count + 1 ));
        WinSendMsg( hpics, WM_PIC_SELECT, MPFROMLONG( count ), 0 );
        tag_enable_picture_controls( hwnd, TRUE );
        tag_refresh_picture_controls( hwnd );
      } else {
        free( pic );
      }
    } else {
      amp_show_error( "Unable to read file:\n%s\n", filedialog.szFullFile );
    }
  }
}

/* Initializes the song information dialog. */
static void
tag_song_info_init( HWND hwnd, AMP_FILE* tags, int count )
{
  int  i;
  BOOL keep_genre = FALSE;
  BOOL keep_apics = FALSE;

  APIC** pics = NULL;
  int    pics_count = 0;
  BOOL   pics_editable = FALSE;
  int    pics_types = 0;

  WinSetWindowPtr( hwnd, 0, tags );

  cb_readonly( hwnd, CB_TAG_TITLE,     TRUE );
  cb_readonly( hwnd, CB_TAG_ARTIST,    TRUE );
  cb_readonly( hwnd, CB_TAG_ALBUM,     TRUE );
  cb_readonly( hwnd, CB_TAG_COMMENT,   TRUE );
  cb_readonly( hwnd, CB_TAG_COPYRIGHT, TRUE );
  cb_readonly( hwnd, CB_TAG_YEAR,      TRUE );
  cb_readonly( hwnd, CB_TAG_TRACK,     TRUE );
  cb_readonly( hwnd, CB_TAG_GENRE,     TRUE );
  cb_readonly( hwnd, CB_TAG_DISC,      TRUE );

  tag_enable_picture_controls( hwnd, FALSE );

  for( i = 0; i <= GENRE_LARGEST; i++ ) {
    lb_add_item( hwnd, CB_TAG_GENRE, genres[i] );
  }

  for( i = 0; i < count; i++ ) {
    if( tags[i].info.saveinfo )
    {
      int haveinfo = tags[i].info.haveinfo;

      if( haveinfo == 0 ) {
        haveinfo = DECODER_HAVE_TITLE   | DECODER_HAVE_ARTIST    | DECODER_HAVE_ALBUM |
                   DECODER_HAVE_COMMENT | DECODER_HAVE_COPYRIGHT | DECODER_HAVE_YEAR  |
                   DECODER_HAVE_TRACK   | DECODER_HAVE_GENRE;
      }

      if( haveinfo & DECODER_HAVE_TITLE     ) { cb_readonly( hwnd, CB_TAG_TITLE,     FALSE ); }
      if( haveinfo & DECODER_HAVE_ARTIST    ) { cb_readonly( hwnd, CB_TAG_ARTIST,    FALSE ); }
      if( haveinfo & DECODER_HAVE_ALBUM     ) { cb_readonly( hwnd, CB_TAG_ALBUM,     FALSE ); }
      if( haveinfo & DECODER_HAVE_COMMENT   ) { cb_readonly( hwnd, CB_TAG_COMMENT,   FALSE ); }
      if( haveinfo & DECODER_HAVE_COPYRIGHT ) { cb_readonly( hwnd, CB_TAG_COPYRIGHT, FALSE ); }
      if( haveinfo & DECODER_HAVE_YEAR      ) { cb_readonly( hwnd, CB_TAG_YEAR,      FALSE ); }
      if( haveinfo & DECODER_HAVE_TRACK     ) { cb_readonly( hwnd, CB_TAG_TRACK,     FALSE ); }
      if( haveinfo & DECODER_HAVE_GENRE     ) { cb_readonly( hwnd, CB_TAG_GENRE,     FALSE ); }
      if( haveinfo & DECODER_HAVE_DISC      ) { cb_readonly( hwnd, CB_TAG_DISC,      FALSE ); }
    }

    tag_add_value( hwnd, CB_TAG_TITLE,     tags[i].info.title     );
    tag_add_value( hwnd, CB_TAG_ARTIST,    tags[i].info.artist    );
    tag_add_value( hwnd, CB_TAG_ALBUM,     tags[i].info.album     );
    tag_add_value( hwnd, CB_TAG_COMMENT,   tags[i].info.comment   );
    tag_add_value( hwnd, CB_TAG_COPYRIGHT, tags[i].info.copyright );
    tag_add_value( hwnd, CB_TAG_YEAR,      tags[i].info.year      );
    tag_add_value( hwnd, CB_TAG_TRACK,     tags[i].info.track     );
    tag_add_value( hwnd, CB_TAG_DISC,      tags[i].info.disc      );

    if( lb_search( hwnd, CB_TAG_GENRE, LIT_FIRST, tags[i].info.genre ) == LIT_NONE ) {
      tag_add_value( hwnd, CB_TAG_GENRE, tags[i].info.genre );
    }
    if( i > 0 && stricmp( tags[i-1].info.genre, tags[i].info.genre ) != 0 ) {
      keep_genre = TRUE;
    }

    if( *tags[i].decoder && !keep_apics )
    {
      DECODER_INFO info = { sizeof( DECODER_INFO )};

      if( tags[i].info.pics_count ) {
        if( tags[i].info.pics ) {
          dec_copyinfo( &info, &tags[i].info );
          DEBUGLOG(( "pm123: tag_song_info_init used cached file info\n" ));
        } else {
          #if DEBUG
          int rc =
          #endif
              dec_fileinfo( tags[i].filename, &info, tags[i].decoder, DECODER_LOADPICS );

          DEBUGLOG(( "pm123: dec_fileinfo returns %d, pictures found=%d\n", rc, info.pics_count ));
        }
      }

      if( tags[i].info.saveinfo && ( tags[i].info.haveinfo & DECODER_HAVE_PICS )) {
        pics_editable = TRUE;
      }

      if( i == 0 ) {
        pics_count = info.pics_count;
        pics = info.pics;
      } else {
        if( pics_count != info.pics_count ) {
          DEBUGLOG(( "pm123: different images, %d != %d\n", pics_count, info.pics_count ));
          keep_apics = TRUE;
        } else {
          int j;
          for( j = 0; j < pics_count; j++ ) {
            if( pics[j]->size != info.pics[j]->size ) {
              DEBUGLOG(( "pm123: different images, different size of the %d picture\n", j ));
              keep_apics = TRUE;
              break;
            } else if( memcmp( pics[j], info.pics[j], pics[j]->size ) != 0 ) {
              DEBUGLOG(( "pm123: different images, different contents of the %d pictures\n", j ));
              keep_apics = TRUE;
              break;
            }
          }
        }
        dec_cleaninfo( &info );
      }
    }
    pics_types |= ( tags[i].info.haveinfo & DECODER_PICS_TYPES );
  }

  tag_select_default_value( hwnd, CB_TAG_TITLE     );
  tag_select_default_value( hwnd, CB_TAG_ARTIST    );
  tag_select_default_value( hwnd, CB_TAG_ALBUM     );
  tag_select_default_value( hwnd, CB_TAG_COMMENT   );
  tag_select_default_value( hwnd, CB_TAG_COPYRIGHT );
  tag_select_default_value( hwnd, CB_TAG_YEAR      );
  tag_select_default_value( hwnd, CB_TAG_TRACK     );
  tag_select_default_value( hwnd, CB_TAG_DISC      );

  if( keep_genre || !*tags[0].info.genre ) {
    tag_select_default_value( hwnd, CB_TAG_GENRE );
  } else {
    SHORT gen_id = lb_search( hwnd, CB_TAG_GENRE, LIT_FIRST, tags[0].info.genre );

    if( gen_id != LIT_ERROR && gen_id != LIT_NONE ) {
      lb_select( hwnd, CB_TAG_GENRE, gen_id );
    }

    lb_ins_item( hwnd, CB_TAG_GENRE, 0, KEEP );
  }

  if( pics_types == DECODER_PICS_ANYTYPE ) {
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_OTHER], PIC_OTHER );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_OTHERICON], PIC_OTHERICON );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_ICON], PIC_ICON );
  }
  if( pics_types == DECODER_PICS_ANYTYPE || pics_types & DECODER_PICS_COVERFRONT ) {
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_COVERFRONT], PIC_COVERFRONT );
  }
  if( pics_types == DECODER_PICS_ANYTYPE || pics_types & DECODER_PICS_COVERBACK ) {
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_COVERBACK], PIC_COVERBACK );
  }
  if( pics_types == DECODER_PICS_ANYTYPE ) {
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_LEAFLET], PIC_LEAFLET );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_MEDIA], PIC_MEDIA );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_LEADARTIST], PIC_LEADARTIST );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_ARTIST], PIC_ARTIST );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_CONDUCTOR], PIC_CONDUCTOR );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_BAND], PIC_BAND );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_COMPOSER], PIC_COMPOSER );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_LYRICIST], PIC_LYRICIST );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_LOCATION], PIC_LOCATION );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_RECORDING], PIC_RECORDING );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_PERFORMANCE], PIC_PERFORMANCE );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_CAPTURE], PIC_CAPTURE );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_FISH], PIC_FISH );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_ILLUSTRATION], PIC_ILLUSTRATION );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_BANDLOGO], PIC_BANDLOGO );
    lb_add_with_handle( hwnd, CB_PIC_TYPE, picture_types[PIC_PUBLISHERLOGO], PIC_PUBLISHERLOGO );
  }

  if( keep_apics ) {
    WinEnableControl( hwnd, PB_PIC_CLEAR, pics_editable );
    WinSetDlgItemText( hwnd, EN_PIC_INFO, DIFFERENT );
    dec_freepics( pics, pics_count );
  } else if( !pics_count ) {
    WinEnableControl( hwnd, PB_PIC_ADD, pics_editable );
  } else {
    WinSendDlgItemMsg( hwnd, PC_COVER, WM_PIC_SETPICS, MPFROMP( pics ), MPFROMLONG( pics_count ));
    tag_enable_picture_controls( hwnd, pics_editable );
    WinEnableControl( hwnd, PB_PIC_EXTRACT, TRUE );
    tag_refresh_picture_controls( hwnd );
  }
}

/* Initializes the tech information dialog. */
static void
tag_tech_info_init( HWND hwnd, const char* filename, char* decoder, DECODER_INFO* info )
{
  char buffer[512];

  en_readonly( hwnd, EN_FILENAME,   TRUE );
  en_readonly( hwnd, EN_DECODER,    TRUE );
  en_readonly( hwnd, EN_INFO,       TRUE );
  en_readonly( hwnd, EN_MPEGINFO,   TRUE );
  en_readonly( hwnd, EN_SONGLENGTH, TRUE );
  en_readonly( hwnd, EN_FILESIZE,   TRUE );
  en_readonly( hwnd, EN_STARTED,    TRUE );
  en_readonly( hwnd, EN_ENDED,      TRUE );
  en_readonly( hwnd, EN_TRACK_GAIN, TRUE );
  en_readonly( hwnd, EN_TRACK_PEAK, TRUE );
  en_readonly( hwnd, EN_ALBUM_GAIN, TRUE );
  en_readonly( hwnd, EN_ALBUM_PEAK, TRUE );

  WinSetDlgItemText( hwnd, EN_FILENAME, (PSZ)filename );
  WinSetDlgItemText( hwnd, EN_DECODER,  dec_get_description( decoder, buffer, sizeof( buffer )));
  WinSetDlgItemText( hwnd, EN_INFO,     info->tech_info );

  if( info->mpeg && info->layer ) {
    snprintf( buffer, sizeof( buffer ), "mpeg %.1f layer %d", (float)info->mpeg/10, info->layer );
    WinSetDlgItemText( hwnd, EN_MPEGINFO, buffer );
  }
  if( info->songlength > 0 ) {
    snprintf( buffer, sizeof( buffer ), "%02d:%02d", info->songlength / 60000, info->songlength % 60000 / 1000 );
    WinSetDlgItemText( hwnd, EN_SONGLENGTH, buffer );
  }
  if( info->filesize > 0 ) {
    snprintf( buffer, sizeof( buffer ), "%lld bytes", info->filesize );
    WinSetDlgItemText( hwnd, EN_FILESIZE, buffer );
  }
  if( info->start ) {
    snprintf( buffer, sizeof( buffer ), "%d ms", info->start );
    WinSetDlgItemText( hwnd, EN_STARTED, buffer );
  } else if( info->junklength ) {
    snprintf( buffer, sizeof( buffer ), "%d bytes", info->junklength );
    WinSetDlgItemText( hwnd, EN_STARTED, buffer );
  }
  if( info->end ) {
    snprintf( buffer, sizeof( buffer ), "%d ms", info->end );
    WinSetDlgItemText( hwnd, EN_ENDED, buffer );
  }
  if( info->track_gain ) {
    snprintf( buffer, sizeof( buffer ), "%+.2f dB", info->track_gain );
    WinSetDlgItemText( hwnd, EN_TRACK_GAIN, buffer );
  }
  if( info->track_peak ) {
    snprintf( buffer, sizeof( buffer ), "%.6f", info->track_peak );
    WinSetDlgItemText( hwnd, EN_TRACK_PEAK, buffer );
  }
  if( info->album_gain ) {
    snprintf( buffer, sizeof( buffer ), "%+.2f dB", info->album_gain );
    WinSetDlgItemText( hwnd, EN_ALBUM_GAIN, buffer );
  }
  if( info->album_peak ) {
    snprintf( buffer, sizeof( buffer ), "%.6f", info->album_peak );
    WinSetDlgItemText( hwnd, EN_ALBUM_PEAK, buffer );
  }
}

/* Processes messages of the song information dialog.
   Must be called from the main thread. */
static MRESULT EXPENTRY
tag_song_info_page_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case WM_INITDLG:
    {
      RECTL rect;
      HWND  hpics = WinWindowFromID( hwnd, PC_COVER );

      // Correct picture shape to a square.
      WinQueryWindowRect( hpics, &rect );
      WinSetWindowPos( hpics, NULLHANDLE, 0, 0, rect.yTop, rect.yTop, SWP_SIZE );
      break;
    }

    case WM_DESTROY:
      tag_clear_pictures( hwnd, FALSE );
      break;

    case WM_COMMAND:
      switch( COMMANDMSG( &msg )->cmd ) {
        case PB_PIC_NEXT:
          tag_update_picture( hwnd );
          WinSendDlgItemMsg( hwnd, PC_COVER, WM_PIC_SELECTNEXT, 0, 0 );
          tag_refresh_picture_controls( hwnd );
          break;

        case PB_PIC_PREV:
          tag_update_picture( hwnd );
          WinSendDlgItemMsg( hwnd, PC_COVER, WM_PIC_SELECTPREV, 0, 0 );
          tag_refresh_picture_controls( hwnd );
          break;

        case PB_PIC_CLEAR:
          tag_clear_pictures( hwnd, TRUE );
          break;

        case PB_PIC_EXTRACT:
          tag_extract_picture( hwnd );
          break;

        case PB_PIC_ADD:
          tag_add_picture( hwnd );
          break;

        case DID_OK:
        case DID_CANCEL:
          WinDismissDlg((HWND)WinSendMsg( hwnd, WM_QUERYFOCUSCHAIN, MPFROMSHORT( QFC_FRAME ), 0 ),
                        COMMANDMSG( &msg )->cmd );
          break;
      }
      return 0;

    case WM_CHAR:
      if(!( SHORT1FROMMP(mp1) & KC_KEYUP ) &&
          ( SHORT1FROMMP(mp1) & KC_VIRTUALKEY ))
      {
        if( SHORT2FROMMP(mp2) == VK_ENTER || SHORT2FROMMP(mp2) == VK_NEWLINE ) {
          WinSendMsg( hwnd, WM_COMMAND, MPFROMSHORT( DID_OK ),
                            MPFROM2SHORT( CMDSRC_ACCELERATOR, FALSE ));
          return 0;
        }
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Edits a meta information of the specified file.
   Must be called from the main thread. */
void
tag_edit( HWND owner, AMP_FILE* tags, int count )
{
  char    caption[_MAX_FNAME] = "File Info - ";
  HWND    hwnd;
  HWND    book;
  HWND    page01;
  HWND    page02;
  MRESULT id;
  ULONG   rc;

  ASSERT_IS_MAIN_THREAD;

  hwnd = WinLoadDlg( HWND_DESKTOP, owner,
                     tag_edit_dlg_proc, hmodule, DLG_FILEINFO, 0 );

  amp_parse_filename( caption + strlen( caption ), tags[0].filename,
                      PFN_NAMEEXT, sizeof( caption ) - strlen( caption ));

  WinSetWindowText( hwnd, caption );

  book = WinWindowFromID( hwnd, NB_FILEINFO );
  do_warpsans( hwnd );
  do_warpsans( book );

  WinSendMsg( book, BKM_SETDIMENSIONS, MPFROM2SHORT(100,25), MPFROMSHORT(BKA_MAJORTAB));
  WinSendMsg( book, BKM_SETDIMENSIONS, MPFROMLONG(0), MPFROMSHORT(BKA_MINORTAB));
  WinSendMsg( book, BKM_SETNOTEBOOKCOLORS, MPFROMLONG(SYSCLR_FIELDBACKGROUND),
              MPFROMSHORT(BKA_BACKGROUNDPAGECOLORINDEX));

  page01 = WinLoadDlg( book, book, tag_song_info_page_dlg_proc, hmodule, DLG_SONGINFO, 0 );
  id     = WinSendMsg( book, BKM_INSERTPAGE, MPFROMLONG(0),
                       MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MAJOR, BKA_FIRST ));
  do_warpsans( page01 );
  WinSendMsg ( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG(id), MPFROMLONG( page01 ));
  WinSendMsg ( book, BKM_SETTABTEXT, MPFROMLONG( id ), MPFROMP( "~Song Info" ));

  page02 = WinLoadDlg( book, book, tag_tech_info_page_dlg_proc, hmodule, DLG_TECHINFO, 0 );
  id     = WinSendMsg( book, BKM_INSERTPAGE, MPFROMLONG(id),
                       MPFROM2SHORT( BKA_AUTOPAGESIZE | BKA_MAJOR, BKA_NEXT ));
  do_warpsans( page02 );
  WinSendMsg ( book, BKM_SETPAGEWINDOWHWND, MPFROMLONG(id), MPFROMLONG( page02 ));
  WinSendMsg ( book, BKM_SETTABTEXT, MPFROMLONG( id ), MPFROMP( "~Tech Info" ));

  tag_tech_info_init( page02, tags[0].filename, tags[0].decoder, &tags[0].info );
  tag_song_info_init( page01, tags, count );

  // Prevents closing of a owner window if this window is closed via Alt+F4 keys.
  WinSetOwner( page01, book );
  WinSetOwner( page02, book );

  WinSetFocus( HWND_DESKTOP, WinWindowFromID( page01, CB_TAG_TITLE ));
  rc = WinProcessDlg( hwnd );

  if( rc == DID_OK )
  {
    DECODER_INFO info;

    char pic_info[128];
    int  keep = 0;
    int  i;

    HWND   hpics = WinWindowFromID( page01, PC_COVER );
    ULONG  pics_count = (ULONG)WinSendMsg( hpics, WM_PIC_COUNT, 0, 0 );
    APIC** pics = WinSendMsg( hpics, WM_PIC_GETPICS, 0, 0 );

    WinQueryDlgItemText( page01, CB_TAG_TITLE,     sizeof( info.title     ), info.title     );
    WinQueryDlgItemText( page01, CB_TAG_ARTIST,    sizeof( info.artist    ), info.artist    );
    WinQueryDlgItemText( page01, CB_TAG_ALBUM,     sizeof( info.album     ), info.album     );
    WinQueryDlgItemText( page01, CB_TAG_COMMENT,   sizeof( info.comment   ), info.comment   );
    WinQueryDlgItemText( page01, CB_TAG_COPYRIGHT, sizeof( info.copyright ), info.copyright );
    WinQueryDlgItemText( page01, CB_TAG_YEAR,      sizeof( info.year      ), info.year      );
    WinQueryDlgItemText( page01, CB_TAG_TRACK,     sizeof( info.track     ), info.track     );
    WinQueryDlgItemText( page01, CB_TAG_DISC,      sizeof( info.disc      ), info.disc      );
    WinQueryDlgItemText( page01, CB_TAG_GENRE,     sizeof( info.genre     ), info.genre     );

    if( stricmp( info.title,     KEEP ) == 0 ) { keep |= DECODER_HAVE_TITLE;     }
    if( stricmp( info.artist,    KEEP ) == 0 ) { keep |= DECODER_HAVE_ARTIST;    }
    if( stricmp( info.album,     KEEP ) == 0 ) { keep |= DECODER_HAVE_ALBUM;     }
    if( stricmp( info.comment,   KEEP ) == 0 ) { keep |= DECODER_HAVE_COMMENT;   }
    if( stricmp( info.copyright, KEEP ) == 0 ) { keep |= DECODER_HAVE_COPYRIGHT; }
    if( stricmp( info.year,      KEEP ) == 0 ) { keep |= DECODER_HAVE_YEAR;      }
    if( stricmp( info.track,     KEEP ) == 0 ) { keep |= DECODER_HAVE_TRACK;     }
    if( stricmp( info.disc,      KEEP ) == 0 ) { keep |= DECODER_HAVE_DISC;      }
    if( stricmp( info.genre,     KEEP ) == 0 ) { keep |= DECODER_HAVE_GENRE;     }

    WinQueryDlgItemText( page01, EN_PIC_INFO, sizeof( pic_info ), pic_info );
    if( stricmp( pic_info, DIFFERENT ) == 0 ) {
      DEBUGLOG(( "pm123: keep attached pictures\n" ));
      keep |= DECODER_HAVE_PICS;
    }

    #define TAG_COPY_FIELD( type, name )                                   \
      if(!( keep & type )) {                                               \
        strlcpy( pdata->info.name, info.name, sizeof( pdata->info.name )); \
      }

    for( i = 0; i < count; i++ ) {
      if( tags[i].info.saveinfo )
      {
        PAMP_FILE pdata = malloc( sizeof( AMP_FILE ));

        if( pdata ) {
          *pdata = tags[i];
          TAG_COPY_FIELD( DECODER_HAVE_TITLE,     title     );
          TAG_COPY_FIELD( DECODER_HAVE_ARTIST,    artist    );
          TAG_COPY_FIELD( DECODER_HAVE_ALBUM,     album     );
          TAG_COPY_FIELD( DECODER_HAVE_YEAR,      year      );
          TAG_COPY_FIELD( DECODER_HAVE_COMMENT,   comment   );
          TAG_COPY_FIELD( DECODER_HAVE_GENRE,     genre     );
          TAG_COPY_FIELD( DECODER_HAVE_COPYRIGHT, copyright );
          TAG_COPY_FIELD( DECODER_HAVE_TRACK,     track     );
          TAG_COPY_FIELD( DECODER_HAVE_DISC,      disc      );

          if( keep & DECODER_HAVE_PICS ) {
            pdata->info.pics_count = -1;
            pdata->info.pics = NULL;
          } else {
            tag_update_picture( page01 );
            pdata->info.pics_count = pics_count;
            pdata->info.pics = dec_copypics( pics, pics_count );
          }

          qu_write( broker_queue, TAG_UPDATE, pdata );
        }
      }
    }
  }

  WinDestroyWindow( hwnd );
}

/* Initializes of the tagging module. */
void
tag_init( void )
{
  ASSERT_IS_MAIN_THREAD;
  broker_queue = qu_create();

  if( !broker_queue ) {
    amp_show_error( "Unable create tags service queue." );
  } else {
    if(( broker_tid = _beginthread( tag_broker, NULL, 204800, NULL )) == -1 ) {
      amp_show_error( "Unable create the tags service thread." );
    }
  }
}

/* Terminates  of the tagging module. */
void
tag_term( void )
{
  ULONG request;
  PVOID data;

  ASSERT_IS_MAIN_THREAD;

  while( !qu_empty( broker_queue )) {
    qu_read( broker_queue, &request, &data );
    free( data );
  }

  qu_push( broker_queue, TAG_TERMINATE, NULL );
  wait_thread( broker_tid, 2000 );
  qu_close( broker_queue );
}

