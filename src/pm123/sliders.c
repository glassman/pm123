/*
 * Copyright 2014-2015 Dmitry A.Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_WIN
#include <os2.h>
#include <debuglog.h>

#include "sliders.h"

PFNWP old_slider_wnd_proc = NULL;

// IBM developers calculated size of the slider shaft and handle based on
// the screen size. This is produces too enormous sliders on modern
// high-resolution displays. Here I try recalculate slider size based on
// current DPI value.

static MRESULT EXPENTRY
slider_wnd_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_CREATE:
    {
      MRESULT rc  = old_slider_wnd_proc( hwnd, msg, mp1, mp2 );
      HPS     hps = WinGetPS( hwnd );
      HDC     hdc = GpiQueryDevice( hps );
      LONG    dpi = 96;

      DevQueryCaps( hdc, CAPS_HORIZONTAL_FONT_RES, 1L, &dpi );
      WinReleasePS( hps );

      WinSendMsg( hwnd, SLM_SETSLIDERINFO,
                  MPFROM2SHORT( SMA_SHAFTDIMENSIONS, SMA_RANGEVALUE ),
                  MPFROM2SHORT(( 12 * dpi / 96 + 1 ) & 0xFE, 0 ));
      WinSendMsg( hwnd, SLM_SETSLIDERINFO,
                  MPFROM2SHORT( SMA_SLIDERARMDIMENSIONS, SMA_RANGEVALUE ),
                  MPFROM2SHORT(( 12 * dpi / 96 + 1 ) & 0xFE, ( 24 * dpi / 96 + 1 ) & 0xFE ));
      return rc;
    }
  }

  return old_slider_wnd_proc( hwnd, msg, mp1, mp2 );
}

void
InitSlider( HAB hab )
{
  CLASSINFO cinfo;

  if( WinQueryClassInfo( hab, WC_SLIDER, &cinfo )) {
    old_slider_wnd_proc = cinfo.pfnWindowProc;
    WinRegisterClass( hab, WC_PM123_SLIDER, slider_wnd_proc, cinfo.flClassStyle & ~CS_PUBLIC, cinfo.cbWindowData );
  }
}



