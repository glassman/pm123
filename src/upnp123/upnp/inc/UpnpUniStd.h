#ifndef UPNPUNISTD_H
#define UPNPUNISTD_H

#if defined(WIN32) || defined(__OS2__)
	/* Do not #include <unistd.h> */
#else /* WIN32 */
	#include <unistd.h> /* for close() */
#endif /* WIN32 */

#endif /* UPNPUNISTD_H */
