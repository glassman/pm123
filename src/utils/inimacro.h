/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2006 Dmitry A.Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INIMACRO_H
#define INIMACRO_H

#ifndef INI_SECTION
#define INI_SECTION  "Settings"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Opens the specified profile file. */
HINI open_ini( const char* filename );
/* Opens a profile file by the name of the module in the program directory. */
HINI open_module_ini( void );
/* Closes a opened profile file. */
BOOL close_ini( HINI hini );

#ifdef __cplusplus
}
#endif

/* Saves a numeric or boolean value to the specified profile file. */
#define save_ini_value( hini, var ) \
  PrfWriteProfileData ( hini, INI_SECTION, #var, &var, sizeof( var ));

/* Saves a characters string to the specified profile file. */
#define save_ini_string( hini, var ) \
  PrfWriteProfileString( hini, INI_SECTION, #var, var );

/* Saves a binary data to the specified profile file. */
#define save_ini_data( hini, var, size ) \
  PrfWriteProfileData( hini, INI_SECTION, #var, var, size );

/* Saves an array of strings to the specified profile file. */
#define save_ini_array_of_strings( hini, var )                       \
{                                                                    \
  ULONG datasize = 1, i;                                             \
  char *data, *p;                                                    \
  for( i = 0; i < ARRAY_SIZE(var); i++ ) {                           \
    datasize += strlen( var[i] ) + 1;                                \
  }                                                                  \
  if(( data = malloc( datasize )) != NULL ) {                        \
    for( i = 0, p = data; i < ARRAY_SIZE(var); i++ ) {               \
      size_t size = strlen( var[i] );                                \
      memcpy( p, var[i], size + 1 );                                 \
      p += size + 1;                                                 \
    }                                                                \
    *p = 0;                                                          \
    PrfWriteProfileData( hini, INI_SECTION, #var, data, datasize );  \
    free( data );                                                    \
  }                                                                  \
}

/* Loads a numeric or boolean value from the specified profile file. */
#define load_ini_value( hini, var )                                  \
{                                                                    \
  ULONG datasize;                                                    \
  PrfQueryProfileSize( hini, INI_SECTION, #var, &datasize );         \
  if( datasize == sizeof( var )) {                                   \
    PrfQueryProfileData( hini, INI_SECTION, #var, &var, &datasize ); \
  }                                                                  \
}

/* Loads a characters string from the specified profile file. */
#define load_ini_string( hini, var ) \
  PrfQueryProfileString( hini, INI_SECTION, #var, NULL, var, sizeof( var ));

/* Querys a size of the binary data saved to the specified profile file. */
#define load_ini_data_size( hini, var, size )                        \
{                                                                    \
  size = 0;                                                          \
  PrfQueryProfileSize( hini, INI_SECTION, #var, &size );             \
}

/* Loads a binary data from the specified profile file. */
#define load_ini_data( hini, var, size )                             \
{                                                                    \
  ULONG datasize;                                                    \
  PrfQueryProfileSize( hini, INI_SECTION, #var, &datasize );         \
  if( datasize == size ) {                                           \
    PrfQueryProfileData( hini, INI_SECTION, #var, var, &datasize );  \
  }                                                                  \
}

/* Loads an array of strings from the specified profile file. */
#define load_ini_array_of_strings( hini, var )                       \
{                                                                    \
  ULONG datasize = 0, i;                                             \
  char *data, *p;                                                    \
  memset( var, sizeof(var), 0 );                                     \
  PrfQueryProfileSize( hini, INI_SECTION, #var, &datasize );         \
  if( datasize ) {                                                   \
    if(( data = malloc( datasize )) != NULL ) {                      \
      if( PrfQueryProfileData( hini, INI_SECTION, #var, data, &datasize )) { \
        for( i = 0, p = data; *p && i < ARRAY_SIZE(var); i++ ) {     \
          strlcpy( var[i], p, sizeof(var[i]));                       \
          p += strlen(p) + 1;                                        \
        }                                                            \
      }                                                              \
      free( data );                                                  \
    }                                                                \
  }                                                                  \
}

#endif
