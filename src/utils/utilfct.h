/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2004-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_UTILS_H
#define PM123_UTILS_H

#include <config.h>
#include "rel2abs.h"
#include "abs2rel.h"
#include "bufstream.h"
#include "charset.h"
#include "wildcards.h"
#include "filefind.h"
#include "fileutil.h"
#include "errorstr.h"
#include "inimacro.h"
#include "queue.h"
#include "minmax.h"
#include "eautils.h"
#include "nls.h"

#ifndef BKS_TABBEDDIALOG
#define BKS_TABBEDDIALOG 0x00000800UL /* Tabbed dialog. */
#endif
#ifndef BKS_BUTTONAREA
#define BKS_BUTTONAREA   0x00000200UL /* Reserve space for buttons. */
#endif

#define MOVE_UP   0
#define MOVE_DOWN 1

#ifdef __cplusplus
extern "C" {
#endif

/* Returns TRUE if the WarpSans is supported by operating system. */
BOOL check_warpsans( void );
/* Assigns the 9.WarpSans as default font for a specified window if it is supported by
   operating system. Otherwise assigns the 8.Helv as default font. */
void do_warpsans( HWND hwnd );
/* Assigns the 9.WarpSans Bold as default font for a specified window if it is supported by
   operating system. Otherwise assigns the 8.Helv Bold as default font. */
void do_warpsans_bold( HWND hwnd );

/* Queries a module handle and name. */
void getModule ( HMODULE* hmodule, char* name, int name_size );
/* Queries a program name. */
void getExeName( char* name, int name_size );

/* Removes leading and trailing spaces. */
char* blank_strip( char* string );
/* Replaces series of control characters by one space. */
char* control_strip( char* string );
/* Removes leading and trailing spaces and quotes. */
char* quote_strip( char* string );
/* Removes comments starting with "#". */
char* uncomment( char* string );
/* Returns a human-readable file size. */
char* readable_file_size( long long filesize, char* result, size_t size );

/* Places the current thread into a wait state until another thread
   in the current process has ended. Kills another thread if the
   time expires and return FALSE. */
BOOL  wait_thread( TID tid, ULONG msec );

/* Adds an item into a menu control. */
SHORT mn_add_item( HWND hmenu, SHORT item_id, const char* item_text, BOOL enable, BOOL check, PVOID handle );
/* Adds a separator into a menu control. */
SHORT mn_add_separator( HWND hmenu, SHORT item_id );
/* Deletes an item from the menu control. */
SHORT mn_remove_item( HWND hmenu, SHORT item_id );

/* Returns the identity of a menu item of a specified index. */
SHORT mn_item_id( HWND hmenu, SHORT i );

/* Makes a menu item selectable. */
BOOL  mn_enable_item( HWND hmenu, SHORT item_id, BOOL enable );
/* Places a a check mark to the left of the menu item. */
BOOL  mn_check_item( HWND hmenu, SHORT item_id, BOOL check  );
/* Sets the default item in a conditional cascade menu. */
BOOL  mn_set_default( HWND hmenu, SHORT item_id );

/* Returns the handle of the specified menu item. */
PVOID mn_get_handle( HWND hmenu, SHORT item_id );
/* Returns the handle of the specified submenu. */
HWND  mn_get_submenu( HWND hmenu, SHORT item_id );
/* Returns a count of the number of items in the menu control. */
SHORT mn_count( HWND hmenu );

/* Adds an item into a list box control. */
SHORT lb_add_item( HWND hwnd, SHORT lb_id, const char* item_text );
/* Adds an item with handle into a list box control. */
SHORT lb_add_with_handle( HWND hwnd, SHORT lb_id, const char* item_text, ULONG handle );
/* Inserts an item into a list box control. */
SHORT lb_ins_item( HWND hwnd, SHORT lb_id, SHORT index, const char* item_text );
/* Deletes all items from the list box control. */
BOOL  lb_remove_all( HWND hwnd, SHORT lb_id );
/* Deletes an item from the list box control. Returns the number of
   items in the list after the item is deleted. */
SHORT lb_remove( HWND hwnd, SHORT lb_id, SHORT i );
/* Moves an item up in a list box control. */
BOOL  lb_move_item( HWND hwnd, SHORT lb_id, SHORT i, int dir );

/* Queries the indexed item of the list box control. */
SHORT lb_get_item( HWND hwnd, SHORT lb_id, SHORT i, char* item_text, SHORT size );
/* Queries a size of the indexed item of the list box control.
   Returns the length of the text string, excluding the null
   termination character. */
SHORT lb_get_item_size( HWND hwnd, SHORT lb_id, SHORT i );

/* Sets the handle of the specified list box item. */
BOOL  lb_set_handle( HWND hwnd, SHORT lb_id, SHORT i, ULONG handle );
/* Returns the handle of the indexed item of the list box control. */
ULONG lb_get_handle( HWND hwnd, SHORT lb_id, SHORT i );
/* Sets the selection state of an item in a list box. */
BOOL  lb_select( HWND hwnd, SHORT lb_id, SHORT i );
/* Sets the selection state of an item in a list box with specified handle. */
BOOL  lb_select_by_handle( HWND hwnd, SHORT lb_id, ULONG handle );

/* Returns the current cursored item. */
SHORT lb_cursored( HWND hwnd, SHORT lb_id );
/* Returns the current selected item. */
SHORT lb_selected( HWND hwnd, SHORT lb_id, SHORT starti );

/* Returns a count of the number of items in the list box control. */
SHORT lb_count( HWND hwnd, SHORT lb_id );
/* Searches an item in a list box control. */
SHORT lb_search( HWND hwnd, SHORT lb_id, SHORT starti, char* item_text );

/* Sets the read-only state of the combination-box in the dialog template. */
void  cb_readonly( HWND hwnd, SHORT id, BOOL enable );
/* Returns TRUE if the specified combination-box has the read-only state. */
BOOL  cb_is_readonly( HWND hwnd, SHORT id );

/* Sets the read-only state of the entryfield in the dialog template. */
void  en_readonly( HWND hwnd, SHORT id, BOOL enable );
/* Returns TRUE if the specified entryfield has the read-only state. */
BOOL  en_is_readonly( HWND hwnd, SHORT id );

/* Adjusting the position and size of a notebook window. */
void  nb_adjust( HWND hwnd );

/* Scrolls the container so that the specified record became visible. */
BOOL  cn_scroll_to( HWND hwnd, PRECORDCORE rec );

/* This function sets the visibility state of a dialog item. */
#define WinShowDlgItem( hwndDlg, idItem, fNewVisibility ) \
        WinShowWindow( WinWindowFromID( hwndDlg, idItem ), fNewVisibility )

/* Start/Stop timer. */
#define start_timer( hwnd, tid, timeout ) \
        WinStartTimer( WinQueryAnchorBlock( hwnd ), hwnd, tid, timeout );
#define stop_timer( hwnd, tid ) \
        WinStopTimer( WinQueryAnchorBlock( hwnd ), hwnd, tid );

/* Static array size. */
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

#ifdef __cplusplus
}
#endif
#endif /* PM123_UTILS_H */
