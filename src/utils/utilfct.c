/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2004-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_PM
#define  INCL_ERRORS
#include <os2.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include "utilfct.h"

APIRET APIENTRY DosQueryModFromEIP( HMODULE *phMod, ULONG *pObjNum, ULONG BuffLen,
                                    PCHAR pBuff, ULONG *pOffset, ULONG Address );
static BOOL have_warpsans = -1;

/* Returns TRUE if the WarpSans is supported by operating system. */
BOOL
check_warpsans( void )
{
  if( have_warpsans == -1 )
  {
    LONG fontcounter = 0;
    HPS  hps;
    BOOL rc;

    hps = WinGetPS( HWND_DESKTOP );
    rc  = GpiQueryFonts( hps, QF_PUBLIC, "WarpSans", &fontcounter, 0, NULL );
    WinReleasePS( hps );

    have_warpsans = ( rc != 0 && rc != GPI_ALTERROR );
  }

  return have_warpsans;
}

/* Assigns the 9.WarpSans as default font for a specified window if it is supported by
   operating system. Otherwise assigns the 8.Helv as default font. */
void
do_warpsans( HWND hwnd )
{
  char *font = check_warpsans() ? "9.WarpSans" : "8.Helv";
  WinSetPresParam( hwnd, PP_FONTNAMESIZE, strlen( font ) + 1, font );
}

/* Assigns the 9.WarpSans Bold as default font for a specified window if it is supported by
   operating system. Otherwise assigns the 8.Helv Bold as default font. */
void
do_warpsans_bold( HWND hwnd )
{
  char *font = check_warpsans() ? "9.WarpSans Bold" : "8.Helv Bold";
  WinSetPresParam( hwnd, PP_FONTNAMESIZE, strlen( font ) + 1, font );
}

/* Queries a module handle and name. */
void
getModule( HMODULE* hmodule, char* name, int name_size )
{
  if( name && name_size > 0 )
  {
    ULONG ObjNum = 0, Offset = 0;

    DosQueryModFromEIP( hmodule, &ObjNum, name_size, name, &Offset, (ULONG)(&getModule));
    DosQueryModuleName( *hmodule, name_size, name );
  }
}

/* Queries a program name. */
void
getExeName( char* name, int name_size )
{
  if( name && name_size > 0 )
  {
    PPIB ppib;
    PTIB ptib;

    DosGetInfoBlocks( &ptib, &ppib );
    DosQueryModuleName( ppib->pib_hmte, name_size, name );
  }
}

/* Removes leading and trailing spaces. */
char*
blank_strip( char* string )
{
  int   i;
  char* pos = string;

  while( *pos == ' ' || *pos == '\t' || *pos == '\n' || *pos == '\r' ) {
    pos++;
  }

  i = strlen(pos)+1;

  if( pos != string ) {
    memmove( string, pos, i );
  }

  i -= 2;

  while( i >= 0 && ( string[i] == ' ' || string[i] == '\t' || string[i] == '\n' || string[i] == '\r' )) {
    string[i] = 0;
    i--;
  }

  return string;
}

/* Replaces series of control characters by one space. */
char*
control_strip( char* string )
{
  char* s = string;
  char* t = string;

  while( *s ) {
    if( iscntrl( *s )) {
      while( *s && iscntrl( *s )) {
        ++s;
      }
      if( *s ) {
        *t++ = ' ';
      }
    } else {
      *t++ = *s++;
    }
  }
  *t = 0;
  return string;
}

/* Removes leading and trailing spaces and quotes. */
char*
quote_strip( char* string )
{
  int   i, e;
  char* pos = string;

  while( *pos == ' ' || *pos == '\t' || *pos == '\n' ) {
    pos++;
  }

  i = strlen( pos ) - 1;

  for( i = strlen( pos ) - 1; i > 0; i-- ) {
    if( pos[i] != ' ' && pos[i] != '\t' && pos[i] != '\n' ) {
      break;
    }
  }

  if( *pos == '\"' && pos[i] == '\"' )
  {
    pos++;
    i -= 2;
  }

  for( e = 0; e < i + 1; e++ ) {
    string[e] = pos[e];
  }

  string[e] = 0;
  return string;
}

/* Removes comments starting with "#". */
char*
uncomment( char *string )
{
  int  source   = 0;
  BOOL inquotes = FALSE;

  while( string[source] ) {
     if( string[source] == '\"' ) {
       inquotes = !inquotes;
     } else if( string[source] == '#' && !inquotes ) {
       string[source] = 0;
       break;
     }
     source++;
  }
  return string;
}

/* Returns a human-readable file size. */
char*
readable_file_size( long long filesize, char* result, size_t size )
{
    int   i       = -1;
    char* units[] = { "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
    float bytes   = filesize;

    do {
      bytes = bytes / 1024;
      i++;
    } while( bytes > 1024 );

    snprintf( result, size, "%0.1f %s", bytes, units[i] );
    return result;
};

/* Places the current thread into a wait state until another thread
   in the current process has ended. Kills another thread if the
   time expires and return FALSE. */
BOOL
wait_thread( TID tid, ULONG msec )
{
  clock_t waits  = msec * CLOCKS_PER_SEC / 1000;
  clock_t begin  = clock();
  APIRET  rc;

  while(( rc = DosWaitThread( &tid, DCWW_NOWAIT )) == ERROR_THREAD_NOT_TERMINATED &&
          clock() - begin < waits )
  {
    DosSleep(1);
  }

  if( rc == ERROR_THREAD_NOT_TERMINATED ) {
    DosKillThread( tid );
    return FALSE;
  } else {
    return TRUE;
  }
}

/* Adds an item into a menu control. */
SHORT
mn_add_item( HWND hmenu, SHORT item_id, const char* item_text, BOOL enable, BOOL check, PVOID handle )
{
  MENUITEM mi;

  mi.iPosition   = MIT_END;
  mi.afStyle     = MIS_TEXT;
  mi.hwndSubMenu = NULLHANDLE;
  mi.hItem       = (ULONG)handle;
  mi.id          = item_id;
  mi.afAttribute = 0;

  if( !enable ) {
    mi.afAttribute |= MIA_DISABLED;
  }
  if( check ) {
    mi.afAttribute |= MIA_CHECKED;
  }

  return SHORT1FROMMR( WinSendMsg( hmenu, MM_INSERTITEM, MPFROMP( &mi ), MPFROMP( item_text )));
}

/* Adds a separator into a menu control. */
SHORT
mn_add_separator( HWND hmenu, SHORT item_id )
{
  MENUITEM mi;

  mi.iPosition   = MIT_END;
  mi.afStyle     = MIS_SEPARATOR;
  mi.hwndSubMenu = NULLHANDLE;
  mi.hItem       = 0;
  mi.id          = item_id;
  mi.afAttribute = 0;

  return SHORT1FROMMR( WinSendMsg( hmenu, MM_INSERTITEM, MPFROMP( &mi ), 0 ));
}

/* Returns the identity of a menu item of a specified index. */
SHORT
mn_item_id( HWND hmenu, SHORT i )
{
  return SHORT1FROMMR( WinSendMsg( hmenu, MM_ITEMIDFROMPOSITION,
                                   MPFROMSHORT( i ), 0 ));
}

/* Deletes an item from the menu control. */
SHORT
mn_remove_item( HWND hmenu, SHORT item_id )
{
  return SHORT1FROMMR( WinSendMsg( hmenu, MM_REMOVEITEM,
                                   MPFROM2SHORT( item_id, TRUE ), 0 ));
}

/* Makes a menu item selectable. */
BOOL
mn_enable_item( HWND hmenu, SHORT item_id, BOOL enable )
{
  return LONGFROMMR( WinSendMsg( hmenu, MM_SETITEMATTR,
                                 MPFROM2SHORT( item_id,  TRUE ),
                                 MPFROM2SHORT( MIA_DISABLED, enable ? 0 : MIA_DISABLED )));
}

/* Places a a check mark to the left of the menu item. */
BOOL
mn_check_item( HWND hmenu, SHORT item_id, BOOL check )
{
  return LONGFROMMR( WinSendMsg( hmenu, MM_SETITEMATTR,
                                 MPFROM2SHORT( item_id,  TRUE ),
                                 MPFROM2SHORT( MIA_CHECKED, check ? MIA_CHECKED : 0 )));
}

/* Sets the default item in a conditional cascade menu. */
BOOL
mn_set_default( HWND hmenu, SHORT item_id )
{
  WinSetWindowULong( hmenu, QWL_STYLE,
    WinQueryWindowULong( hmenu, QWL_STYLE ) | MS_CONDITIONALCASCADE );

  return LONGFROMMR( WinSendMsg( hmenu, MM_SETDEFAULTITEMID, MPFROMSHORT( item_id ), 0 ));
}

/* Returns the handle of the specified menu item. */
PVOID
mn_get_handle( HWND hmenu, SHORT item_id )
{
  MENUITEM mi;

  if( LONGFROMMR( WinSendMsg( hmenu, MM_QUERYITEM,
                              MPFROM2SHORT( item_id, TRUE ), MPFROMP( &mi )))) {
    return (PVOID)mi.hItem;
  } else {
    return NULL;
  }
}

/* Returns the handle of the specified submenu. */
HWND
mn_get_submenu( HWND hmenu, SHORT item_id )
{
  MENUITEM mi;

  if( LONGFROMMR( WinSendMsg( hmenu, MM_QUERYITEM,
                              MPFROM2SHORT( item_id, TRUE ), MPFROMP( &mi )))) {
    return mi.hwndSubMenu;
  } else {
    return NULLHANDLE;
  }
}

/* Returns a count of the number of items in the menu control. */
SHORT
mn_count( HWND hmenu ) {
  return SHORT1FROMMR( WinSendMsg( hmenu, MM_QUERYITEMCOUNT, 0, 0 ));
}

/* Deletes all the items from the list box control. */
BOOL
lb_remove_all( HWND hwnd, SHORT lb_id ) {
  return LONGFROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_DELETEALL, 0, 0 ));
}

/* Deletes an item from the list box control. Returns the number of
   items in the list after the item is deleted. */
SHORT
lb_remove( HWND hwnd, SHORT lb_id, SHORT i )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id,  LM_DELETEITEM,
                                          MPFROMSHORT( i ), 0 ));
}

/* Moves an item up in a list box control. */
BOOL lb_move_item( HWND hwnd, SHORT lb_id, SHORT i, int dir )
{
  SHORT size[2];
  char* text[2] = { 0 };
  ULONG hndl[2];
  BOOL  rc      = FALSE;
  SHORT count   = lb_count( hwnd, lb_id );

  SHORT i0 = dir == MOVE_UP ? i - 1 : i + 1;
  SHORT i1 = i;

  if( count >= 2 && (( dir == MOVE_UP && i > 0 ) || ( dir == MOVE_DOWN && i < count - 1 )))
  {
    hndl[0] = lb_get_handle( hwnd, lb_id, i0 );
    hndl[1] = lb_get_handle( hwnd, lb_id, i1 );
    size[0] = lb_get_item_size( hwnd, lb_id, i0 );
    size[1] = lb_get_item_size( hwnd, lb_id, i1 );
    text[0] = malloc( size[0] + 1 );
    text[1] = malloc( size[1] + 1 );

    if( text[0] && text[1] )
    {
      lb_get_item( hwnd, lb_id, i0, text[0], size[0] + 1 );
      lb_get_item( hwnd, lb_id, i1, text[1], size[1] + 1 );
      WinSendDlgItemMsg( hwnd, lb_id, LM_SETITEMTEXT, MPFROMSHORT( i0 ), MPFROMP( text[1] ));
      WinSendDlgItemMsg( hwnd, lb_id, LM_SETITEMTEXT, MPFROMSHORT( i1 ), MPFROMP( text[0] ));
      lb_set_handle( hwnd, lb_id, i0, hndl[1] );
      lb_set_handle( hwnd, lb_id, i1, hndl[0] );

      if( lb_selected( hwnd, lb_id, i0 ) == i1 ) {
        lb_select( hwnd, lb_id, i0 );
      }
    }

    free( text[0] );
    free( text[1] );
  }

  return rc;
}

/* Adds an item into a list box control. */
SHORT
lb_add_item( HWND hwnd, SHORT lb_id, const char* item_text )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_INSERTITEM,
                       MPFROMSHORT( LIT_END ), MPFROMP( item_text )));
}

/* Adds an item with handle into a list box control. */
SHORT
lb_add_with_handle( HWND hwnd, SHORT lb_id, const char* item_text, ULONG handle )
{
  SHORT i = SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_INSERTITEM,
                          MPFROMSHORT( LIT_END ), MPFROMP( item_text )));
  if( i >= 0 ) {
    WinSendDlgItemMsg( hwnd, lb_id, LM_SETITEMHANDLE,
                       MPFROMSHORT( i ), MPFROMLONG( handle ));
  }

  return i;
}

/* Inserts an item into a list box control. */
SHORT
lb_ins_item( HWND hwnd, SHORT lb_id, SHORT index, const char* item_text )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_INSERTITEM,
                       MPFROMSHORT( index ), MPFROMP( item_text )));
}

/* Queries the indexed item of the list box control. */
SHORT
lb_get_item( HWND hwnd, SHORT lb_id, SHORT i, char* item_text, SHORT size )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_QUERYITEMTEXT,
                                          MPFROM2SHORT( i, size ), MPFROMP( item_text )));
}

/* Queries a size the indexed item of the list box control. */
SHORT
lb_get_item_size( HWND hwnd, SHORT lb_id, SHORT i )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_QUERYITEMTEXTLENGTH,
                                          MPFROMSHORT( i ), 0 ));
}

/* Sets the handle of the specified list box item. */
BOOL
lb_set_handle( HWND hwnd, SHORT lb_id, SHORT i, ULONG handle )
{
  return LONGFROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_SETITEMHANDLE,
                     MPFROMSHORT( i ), MPFROMLONG( handle )));
}

/* Returns the handle of the indexed item of the list box control. */
ULONG
lb_get_handle( HWND hwnd, SHORT lb_id, SHORT i )
{
  return (ULONG)( WinSendDlgItemMsg( hwnd, lb_id, LM_QUERYITEMHANDLE,
                  MPFROMSHORT( i ), 0 ));
}

/* Sets the selection state of an item in a list box. */
BOOL
lb_select( HWND hwnd, SHORT lb_id, SHORT i )
{
  return LONGFROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_SELECTITEM,
                     MPFROMSHORT( i ), MPFROMSHORT( TRUE )));
}

/* Sets the selection state of an item in a list box with specified handle. */
BOOL
lb_select_by_handle( HWND hwnd, SHORT lb_id, ULONG handle )
{
  SHORT count = lb_count( hwnd, lb_id );
  SHORT i;

  for( i = 0; i < count; i++ ) {
    if( lb_get_handle( hwnd, lb_id, i ) == handle ) {
      return lb_select( hwnd, lb_id, i );
    }
  }

  return FALSE;
}

/* Returns the current cursored item. */
SHORT
lb_cursored( HWND hwnd, SHORT lb_id )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_QUERYSELECTION,
                       MPFROMSHORT( LIT_CURSOR ), 0 ));
}

SHORT
lb_selected( HWND hwnd, SHORT lb_id, SHORT starti )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_QUERYSELECTION,
                       MPFROMSHORT( starti ), 0 ));
}

/* Returns a count of the number of items in the list box control. */
SHORT
lb_count( HWND hwnd, SHORT lb_id ) {
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_QUERYITEMCOUNT, 0, 0 ));
}

/* Searches an item in a list box control. */
SHORT
lb_search( HWND hwnd, SHORT lb_id, SHORT starti, char* item_text )
{
  return SHORT1FROMMR( WinSendDlgItemMsg( hwnd, lb_id, LM_SEARCHSTRING,
                       MPFROM2SHORT( 0, starti ), MPFROMP( item_text )));
}

/* Sets the read-only state of the combination-box in the dialog template. */
void
cb_readonly( HWND hwnd, SHORT id, BOOL state )
{
  ULONG bg_color = state ? SYSCLR_DIALOGBACKGROUND : SYSCLR_ENTRYFIELD;
  HWND  hcombo   = WinWindowFromID( hwnd, id );
  HWND  hedit    = WinWindowFromID( hcombo, CBID_EDIT );

  if( hcombo ) {
    // WinEnableWindow( hcombo, !state );
    // WinEnableWindow( hedit, TRUE );
    WinSetPresParam( hedit, PP_BACKGROUNDCOLORINDEX, sizeof( bg_color ), &bg_color );
    WinSendMsg( hedit, EM_SETREADONLY, MPFROMSHORT( state ), 0 );
  }
}

/* Returns TRUE if the specified combination-box has the read-only state. */
BOOL
cb_is_readonly( HWND hwnd, SHORT id ) {
  return (BOOL)WinSendDlgItemMsg( WinWindowFromID( hwnd, id ), CBID_EDIT, EM_QUERYREADONLY, 0, 0 );
}

/* Sets the read-only state of the entryfield in the dialog template. */
void
en_readonly( HWND hwnd, SHORT id, BOOL state )
{
  ULONG bg_color = state ? SYSCLR_DIALOGBACKGROUND : SYSCLR_ENTRYFIELD;
  HWND  hcontrol = WinWindowFromID( hwnd, id );

  if( hcontrol ) {
    WinSendMsg( hcontrol, EM_SETREADONLY, MPFROMSHORT( state ), 0 );
    WinSetPresParam( hcontrol, PP_BACKGROUNDCOLORINDEX, sizeof( bg_color ), &bg_color );
  }
}

/* Returns TRUE if the specified entryfield has the read-only state. */
BOOL
en_is_readonly( HWND hwnd, SHORT id ) {
  return (BOOL)WinSendDlgItemMsg( hwnd, id, EM_QUERYREADONLY, 0, 0 );
}

/* Adjusting the position and size of a notebook window. */
void
nb_adjust( HWND hwnd )
{
  int    buttons_count = 0;
  HWND   notebook = NULLHANDLE;
  HENUM  henum;
  HWND   hnext;
  char   classname[128];
  RECTL  rect;
  POINTL pos[2];

  struct BUTTON {
    HWND hwnd;
    SWP  swp;

  } buttons[32];

  henum = WinBeginEnumWindows( hwnd );

  while(( hnext = WinGetNextWindow( henum )) != NULLHANDLE ) {
    if( WinQueryClassName( hnext, sizeof( classname ), classname ) > 0 ) {
      if( strcmp( classname, "#40" ) == 0 ) {
        notebook = hnext;
      } else if( strcmp( classname, "#3" ) == 0 ) {
        if( buttons_count < sizeof( buttons ) / sizeof( struct BUTTON )) {
          if( WinQueryWindowPos( hnext, &buttons[buttons_count].swp )) {
            if(!( buttons[buttons_count].swp.fl & SWP_HIDE )) {
              buttons[buttons_count].hwnd = hnext;
              buttons_count++;
            }
          }
        }
      }
    }
  }
  WinEndEnumWindows( henum );

  if( !WinQueryWindowRect( hwnd, &rect ) ||
      !WinCalcFrameRect  ( hwnd, &rect, TRUE ))
  {
    return;
  }

  // Resizes notebook window.
  if( notebook != NULLHANDLE )
  {
    pos[0].x = rect.xLeft;
    pos[0].y = rect.yBottom;
    pos[1].x = rect.xRight;
    pos[1].y = rect.yTop;

    if( buttons_count ) {
      WinMapDlgPoints( hwnd, pos, 2, FALSE );
      pos[0].y += 14;
      WinMapDlgPoints( hwnd, pos, 2, TRUE  );
    }

    WinSetWindowPos( notebook, 0, pos[0].x,  pos[0].y,
                                  pos[1].x - pos[0].x, pos[1].y - pos[0].y, SWP_MOVE | SWP_SIZE );
  }

  // Adjust buttons.
  if( buttons_count )
  {
    int total_width = buttons_count * 2;
    int start;
    int i;

    for( i = 0; i < buttons_count; i++ ) {
      total_width += buttons[i].swp.cx;
    }

    start = ( rect.xRight - rect.xLeft + 1 - total_width ) / 2;

    for( i = 0; i < buttons_count; i++ ) {
      WinSetWindowPos( buttons[i].hwnd, 0, start, buttons[i].swp.y, 0, 0, SWP_MOVE );
      WinInvalidateRect( buttons[i].hwnd, NULL, FALSE );
      start += buttons[i].swp.cx + 2;
    }
  }
}

/* Scrolls the container so that the specified record became visible. */
BOOL
cn_scroll_to( HWND hwnd, PRECORDCORE rec )
{
  QUERYRECORDRECT prcItem;
  RECTL rclRecord;
  RECTL rclContainer;

  prcItem.cb       = sizeof(prcItem);
  prcItem.pRecord  = rec;
  prcItem.fsExtent = CMA_ICON | CMA_TEXT;

  if( !WinSendMsg( hwnd, CM_QUERYRECORDRECT, &rclRecord, &prcItem )) {
    return FALSE;
  }

  if( !WinSendMsg( hwnd, CM_QUERYVIEWPORTRECT, &rclContainer,
                         MPFROM2SHORT( CMA_WINDOW, FALSE )))
  {
    return FALSE;
  }

  if( rclRecord.yBottom < rclContainer.yBottom )  {
    WinPostMsg( hwnd, CM_SCROLLWINDOW, (MPARAM)CMA_VERTICAL,
                (MPARAM)(rclContainer.yBottom - rclRecord.yBottom ));
  } else if( rclRecord.yTop > rclContainer.yTop ) {
    WinPostMsg( hwnd, CM_SCROLLWINDOW, (MPARAM)CMA_VERTICAL,
                (MPARAM)(rclContainer.yTop - rclRecord.yTop ));
  }
  return TRUE;
}

