/*
 * Copyright 2011 Dmitry A.Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_GPI
#include <os2.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "os2fonts.h"
#include "debuglog.h"

char*
attrs_to_fontname( const FATTRS* attrs, LONG fontsize, char* fontname, int size )
{
  snprintf( fontname, size, "%ld.%s", fontsize, attrs->szFacename );

  if( attrs->fsSelection & FATTR_SEL_ITALIC  ) {
    strlcat( fontname, ".Italic", size );
  }
  if( attrs->fsSelection & FATTR_SEL_OUTLINE ) {
    strlcat( fontname, ".Outline", size );
  }
  if( attrs->fsSelection & FATTR_SEL_STRIKEOUT ) {
    strlcat( fontname, ".Strikeout", size );
  }
  if( attrs->fsSelection & FATTR_SEL_UNDERSCORE ) {
    strlcat( fontname, ".Underscore", size );
  }
  if( attrs->fsSelection & FATTR_SEL_BOLD ) {
    strlcat( fontname, ".Bold", size );
  }

  return fontname;
}

BOOL
fontname_to_attrs( const char* fontname, FATTRS* attrs, LONG* pfontsize, SIZEF* psizef )
{
  const char* pname = fontname;
  const char* pos;
  int         size;

  HPS  ps = WinGetPS( HWND_DESKTOP );
  HDC  dc = GpiQueryDevice( ps );

  LONG rem_fonts = 0;
  LONG req_fonts = 0;
  LONG how_close = 0x7FFFFFFF;
  LONG res_horz  = 0;
  LONG res_vert  = 0;
  LONG vec_match = 0;
  LONG i;
  BOOL rc = FALSE;

  DevQueryCaps( dc, CAPS_HORIZONTAL_FONT_RES, 1L, &res_horz );
  DevQueryCaps( dc, CAPS_VERTICAL_FONT_RES  , 1L, &res_vert );

  memset( attrs, 0, sizeof(FATTRS));
  attrs->usRecordLength = sizeof(FATTRS);

  DEBUGLOG2(( "fontname_to_attrs: begin parse '%s'\n", fontname ));

  if(( pos = strchr( pname, '.' )) != NULL ) {
   *pfontsize = atol( pname );
    DEBUGLOG2(( "fontname_to_attrs: size is %d\n", *pfontsize ));
    pname = pos + 1;
  }
  if(( pos = strchr( pname, '.' )) != NULL ) {
    if(( size = pos - pname + 1 ) > FACESIZE ) {
      size = FACESIZE;
    }
    strlcpy( attrs->szFacename, pname, size );
    DEBUGLOG2(( "fontname_to_attrs: face name is %s\n", attrs->szFacename ));
    pname = pos;
  } else {
    strlcpy( attrs->szFacename, pname, FACESIZE );
  }

  while( *pname == '.' ) {
    if( strnicmp( pname, ".Italic", 7 ) == 0 ) {
      attrs->fsSelection |= FATTR_SEL_ITALIC;
      DEBUGLOG2(( "fontname_to_attrs: FATTR_SEL_ITALIC\n" ));
      pname += 7;
      continue;
    }
    if( strnicmp( pname, ".Outline", 8 ) == 0 ) {
      attrs->fsSelection |= FATTR_SEL_OUTLINE;
      DEBUGLOG2(( "fontname_to_attrs: FATTR_SEL_OUTLINE\n" ));
      pname += 8;
      continue;
    }
    if( strnicmp( pname, ".Strikeout", 10 ) == 0 ) {
      attrs->fsSelection |= FATTR_SEL_STRIKEOUT;
      DEBUGLOG2(( "fontname_to_attrs: FATTR_SEL_STRIKEOUT\n" ));
      pname += 10;
      continue;
    }
    if( strnicmp( pname, ".Underscore", 11 ) == 0 ) {
      attrs->fsSelection |= FATTR_SEL_UNDERSCORE;
      DEBUGLOG2(( "fontname_to_attrs: FATTR_SEL_UNDERSCORE\n" ));
      pname += 11;
      continue;
    }
    if( strnicmp( pname, ".Bold", 5 ) == 0 ) {
      attrs->fsSelection |= FATTR_SEL_BOLD;
      DEBUGLOG2(( "fontname_to_attrs: FATTR_SEL_BOLD\n" ));
      pname += 5;
      continue;
    }
    if(( pos = strchr( pname + 1, '.' )) != NULL ) {
      pname = pos;
    }
  }

  rem_fonts = GpiQueryFonts( ps, QF_PUBLIC | QF_PRIVATE,
                             attrs->szFacename, &req_fonts, sizeof(FONTMETRICS), 0 );
  if( rem_fonts != GPI_ALTERROR )
  {
    FONTMETRICS* pfm = calloc( sizeof( FONTMETRICS ), rem_fonts );

    if( pfm ) {
      req_fonts = rem_fonts;
      rem_fonts = GpiQueryFonts( ps, QF_PUBLIC | QF_PRIVATE,
                                 attrs->szFacename, &req_fonts, sizeof(FONTMETRICS), pfm );
      if( rem_fonts != GPI_ALTERROR ) {
        for( i = 0; i < req_fonts; i++ ) {
          // Vector font doesn't need to check device resolution and point size.
          if( pfm[i].fsDefn & FM_DEFN_OUTLINE )
          {
            vec_match = pfm[i].lMatch;
            continue;
          }
          // Check bitmap font for device resolution and point size.
          if(( pfm[i].sXDeviceRes == res_horz ) &&
             ( pfm[i].sYDeviceRes == res_vert ))
          {
            LONG current_how_close =
                 abs((long)(*pfontsize*10 - pfm[i].sNominalPointSize ));

            if( current_how_close < how_close )
            {
              attrs->lMatch = pfm[i].lMatch;
              how_close  = current_how_close;

              if( psizef ) {
                psizef->cx = MAKEFIXED( pfm[i].lEmInc, 0 );
                psizef->cy = MAKEFIXED( pfm[i].lEmHeight, 0 );
              }
            }
          }
        }
        if( vec_match && ( !attrs->lMatch || how_close != 0 ))
        {
          attrs->fsFontUse |= FATTR_FONTUSE_OUTLINE;

          // Calculate the size of the character box, based on the
          // point size selected and the resolution of the device.
          // The size parameters are of type FIXED, NOT int.
          // NOTE: 1 point == 1/72 of an inch.

          if( psizef ) {
            psizef->cx = ( MAKEFIXED( *pfontsize, 0 ) / 72 ) * res_horz;
            psizef->cy = ( MAKEFIXED( *pfontsize, 0 ) / 72 ) * res_vert;
          }
        }

        rc = TRUE;
      }
      free( pfm );
    }
  }

  WinReleasePS( ps );
  return rc;
}

