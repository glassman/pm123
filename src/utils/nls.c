/*
 * Copyright 2009 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#include <os2.h>

#include <string.h>
#include "nls.h"

static char mapcase[256];
static BOOL init = FALSE;

static void
init_mapcase( void )
{
  COUNTRYCODE countrycode = { 0 };
  int  i;

  for( i = 0; i < 256; i++ ) {
    mapcase[i] = i;
    DosMapCase( 1, &countrycode, &mapcase[i] );
  }

  init = TRUE;
}

/* Compare strings without case sensitivity.
 */

int
nlstricmp( const char* string1, const char* string2 )
{
  int relationship = 0;

  if( !init ) {
    init_mapcase();
  }

  do {
    relationship = mapcase[(unsigned char)*string1] -
                   mapcase[(unsigned char)*string2];

  } while( *string1++ && *string2++ && relationship == 0 );

  return relationship;
}

/* Compare strings without case sensitivity.
 */

int
nlstrnicmp( const char* string1, const char* string2, int n )
{
  int relationship = 0;

  if( !init ) {
    init_mapcase();
  }

  if( n > 0 ) {
    do {
      relationship = mapcase[(unsigned char)*string1] -
                     mapcase[(unsigned char)*string2];

    } while( *string1++ && *string2++ && --n && relationship == 0 );
  }

  return relationship;
}

/* Converts any lowercase letters in string to uppercase.
 * Other characters are not affected.
 */

char*
nlstrupr( char* string )
{
  char* p = string;

  if( !init ) {
    init_mapcase();
  }

  while( *p ) {
    *p = mapcase[(unsigned char)*p];
     p++;
  }

  return string;
}

/* Converts the lowercase letter to the corresponding
 * uppercase letter.
 */

int
nltoupper( int c )
{
  if( !init ) {
    init_mapcase();
  }

  return mapcase[(unsigned char)c];
}

