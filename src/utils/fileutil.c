/*
 * Copyright 2004-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#include <os2.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <direct.h>
#include <sys/stat.h>

#include "fileutil.h"
#include "debuglog.h"
#include "minmax.h"

#define  isslash( c ) ( c == '/' || c == '\\' )

/* Returns TRUE if the specified location is a CD track. */
BOOL
is_track( const char* location )
{
  // Format of the CD track URL is: "cd:///X:\\Track XX"
  return strnicmp( location, "cd:///", 6 ) == 0 &&
         isalpha ( location[ 6] ) &&
         strnicmp( location + 7, ":\\Track ", 8 ) == 0 &&
         isdigit ( location[15] );
}

/* Returns TRUE if the specified location is a cue sheet track. */
BOOL is_cuesheet( const char* location )
{
  // Format of the Cue sheet track URL is: "cue:///pathname,XX"
  return strnicmp( location, "cue:///", 7 ) == 0;
}

/* Returns TRUE if the specified location is a URL. This also includes tracks of
   a cue sheet that is addressed by the URL.*/
BOOL
is_url( const char* location )
{
  if( is_cuesheet( location )) {
    return is_url( location + 7 );
  }
  if( !is_track( location ) && !( isalpha( location[0] ) && location[1] == ':' ))
  {
    const char* pc;

    // If the parse string contain a colon after the 1st character
    // and before any characters not allowed as part of a scheme
    // name (i.e. any not alphanumeric, '+', '.' or '-'),
    // the scheme of the url is the substring of chars up to
    // but not including the first colon. These chars and the
    // colon are then removed from the parse string before
    // continuing.

    for( pc = location; isalnum( *pc ) || ( *pc == '+' ) || ( *pc == '-' ) || ( *pc == '.' ); ++pc )
    {}

    if( *pc == ':' ) {
      return TRUE;
    }
  }
  return FALSE;
}

/* Returns TRUE if the specified location is a file or directory.
   This also includes tracks of a cue sheet if they are regular files. */
BOOL
is_file( const char* location )
{
  if( is_cuesheet( location )) {
    return is_file( location + 7 );
  } else {
    return *location &&
           !is_track( location ) &&
           !is_url( location );
  }
}

/* Returns TRUE if the specified location is a regular file or directory. */
BOOL
is_regular_file( const char* location )
{
  return !is_track( location ) &&
         !is_cuesheet( location ) &&
         !is_url( location );
}

/* Returns the track number if it is specified in location,
   otherwise returns 0. */
int
strack( const char* location )
{
  if( is_track( location )) {
    return atol( location + 15 );
  }

  if( is_cuesheet( location ))
  {
    char* p = strrchr( location, ',' );

    if( p ) {
      return atol( p + 1 );
    }
  }

  return 0;
}

/* Returns the drive letter followed by a colon (:)
   if a drive is specified in the location. */
char*
sdrive( char* result, const char* location, size_t size )
{
  if( is_track( location )) {
    strlcpy( result, location + 6, min( 3, size ));
  } else if( is_cuesheet( location ) && isalpha( location[7] ) && location[8] == ':' ) {
    strlcpy( result, location + 7, min( 3, size ));
  } else if( isalpha( location[0] ) && location[1] == ':' ) {
    strlcpy( result, location, min( 3, size ));
  } else {
    *result = 0;
  }
  return result;
}

/* Returns the scheme followed by a colon (:)
   of the specified location. */
char*
scheme( char* result, const char* location, size_t size )
{
  if( is_cuesheet( location )) {
    return scheme( result, location + 7, size );
  }
  if( !is_track( location ) && !( isalpha( location[0] ) && location[1] == ':' ))
  {
    const char* pc;

    // If the parse string contain a colon after the 1st character
    // and before any characters not allowed as part of a scheme
    // name (i.e. any not alphanumeric, '+', '.' or '-'),
    // the scheme of the url is the substring of chars up to
    // but not including the first colon.

    for( pc = location; isalnum( *pc ) || ( *pc == '+' ) || ( *pc == '-' ) || ( *pc == '.' ); pc++ )
    {}

    if( *pc == ':' ) {
      strlcpy( result, location, min( pc - location + 2, size ));
    }
  } else if( size ) {
    *result = 0;
  }
  return result;
}

/* Returns the base file name with file extension. */
char*
sfnameext( char *result, const char* location, size_t size )
{
  const char* phead = location;
  const char* ptail = location + strlen( location );
  const char* pc;

  BOOL  cuesheet = FALSE;
  BOOL  url = FALSE;

  if( is_cuesheet( location )) {
    phead = location = location + 7;
    cuesheet = TRUE;
  }

  // If the parse string contain a colon after the 1st character
  // and before any characters not allowed as part of a scheme
  // name (i.e. any not alphanumeric, '+', '.' or '-'),
  // the scheme of the url is the substring of chars up to
  // but not including the first colon. These chars and the
  // colon are then removed from the parse string before
  // continuing.
  while( isalnum( *phead ) || ( *phead == '+' ) || ( *phead == '-' ) || ( *phead == '.' )) {
    ++phead;
  }
  if( *phead != ':' ) {
    phead = location;
  } else {
    phead++;
    if( phead - location > 2 || !isalpha( *location )) {
      // If the scheme consists more than of one symbol or its first
      // symbol not the alphabetic character that it is exact not a
      // name of the drive.
      url = TRUE;
    }
  }

  // Skip location (user:password@host:port part of the url
  // or \\server part of the regular pathname) on the front.
  if(( url || location == phead ) &&
     ( isslash( phead[0] ) && isslash( phead[1] )))
  {
    phead += 2;
    while( *phead && !isslash( *phead )) {
      ++phead;
    }
  }

  // Remove fragment identifier, parameters or query information,
  // if any, from the back of the url.
  if( url ) {
    if(( pc = strpbrk( phead, "#?;" )) != NULL ) {
      ptail = pc;
    }
  }
  // Remove track identifier if any, from the back of the cue sheet url.
  if( cuesheet ) {
    if(( pc = strrchr( phead, ',' )) != NULL ) {
      ptail = pc;
    }
  }

  // Skip path name.
  for( pc = phead; *pc && pc < ptail; pc++ ) {
    if( isslash( *pc )) {
      phead = pc + 1;
    }
  }

  // Remainder is a file name.
  strlcpy( result, phead, min( ptail - phead + 1, size ));
  return result;
}

/* Returns the file name extension, if any,
   including the leading period (.). */
char*
sfext( char* result, const char* location, size_t size )
{
  const char* phead  = location;
  const char* ptail  = location + strlen( location );
  const char* pc;

  BOOL  cuesheet = FALSE;
  BOOL  url = FALSE;

  if( is_cuesheet( location )) {
    phead = location = location + 7;
    cuesheet = TRUE;
  }

  // If the parse string contain a colon after the 1st character
  // and before any characters not allowed as part of a scheme
  // name (i.e. any not alphanumeric, '+', '.' or '-'),
  // the scheme of the url is the substring of chars up to
  // but not including the first colon. These chars and the
  // colon are then removed from the parse string before
  // continuing.
  while( isalnum( *phead ) || ( *phead == '+' ) || ( *phead == '-' ) || ( *phead == '.' )) {
    ++phead;
  }
  if( *phead != ':' ) {
    phead = location;
  } else {
    phead++;
    if( phead - location > 2 || !isalpha( *location )) {
      // If the scheme consists more than of one symbol or its first
      // symbol not the alphabetic character that it is exact not a
      // name of the drive.
      url = TRUE;
    }
  }

  // Skip location (user:password@host:port part of the url
  // or \\server part of the regular pathname) on the front.
  if(( url || location == phead ) &&
     ( isslash( phead[0] ) && isslash( phead[1] )))
  {
    phead += 2;
    while( *phead && !isslash( *phead )) {
      ++phead;
    }
  }

  // Remove fragment identifier, parameters or query information,
  // if any, from the back of the url.
  if( url ) {
    if(( pc = strpbrk( phead, "#?;" )) != NULL ) {
      ptail = pc;
    }
  }
  // Remove track identifier if any, from the back of the cue sheet url.
  if( cuesheet ) {
    if(( pc = strrchr( phead, ',' )) != NULL ) {
      ptail = pc;
    }
  }

  // Skip path name.
  for( pc = phead; *pc && pc < ptail; pc++ ) {
    if( isslash( *pc )) {
      phead = pc + 1;
    }
  }

  // Remainder is a file name, search file extension.
  for( pc = ptail - 1; pc > phead && *pc != '.'; pc-- )
  {}

  if( *pc == '.' && pc != phead ) {
    strlcpy( result, pc, min( ptail - pc + 1, size ));
  } else if( size ) {
    *result = 0;
  }

  return result;
}

/* Returns the base file name without any extensions. */
char*
sfname( char* result, const char* location, size_t size )
{
  const char* phead  = location;
  const char* ptail  = location + strlen( location );
  const char* pc;

  BOOL  cuesheet = FALSE;
  BOOL  url = FALSE;

  if( is_cuesheet( location )) {
    phead = location = location + 7;
    cuesheet = TRUE;
  }

  // If the parse string contain a colon after the 1st character
  // and before any characters not allowed as part of a scheme
  // name (i.e. any not alphanumeric, '+', '.' or '-'),
  // the scheme of the url is the substring of chars up to
  // but not including the first colon. These chars and the
  // colon are then removed from the parse string before
  // continuing.
  while( isalnum( *phead ) || ( *phead == '+' ) || ( *phead == '-' ) || ( *phead == '.' )) {
    ++phead;
  }
  if( *phead != ':' ) {
    phead = location;
  } else {
    phead++;
    if( phead - location > 2 || !isalpha( *location )) {
      // If the scheme consists more than of one symbol or its first
      // symbol not the alphabetic character that it is exact not a
      // name of the drive.
      url = TRUE;
    }
  }

  // Skip location (user:password@host:port part of the url
  // or \\server part of the regular pathname) on the front.
  if(( url || cuesheet || location == phead ) &&
     ( isslash( phead[0] ) && isslash( phead[1] )))
  {
    phead += 2;
    while( *phead && !isslash( *phead )) {
      ++phead;
    }
  }

  // Remove fragment identifier, parameters or query information,
  // if any, from the back of the url.
  if( url ) {
    if(( pc = strpbrk( phead, "#?;" )) != NULL ) {
      ptail = pc;
    }
  }
  // Remove track identifier if any, from the back of the cue sheet url.
  if( cuesheet ) {
    if(( pc = strrchr( phead, ',' )) != NULL ) {
      ptail = pc;
    }
  }

  // Skip path name.
  for( pc = phead; *pc && pc < ptail; pc++ ) {
    if( isslash( *pc )) {
      phead = pc + 1;
    }
  }

  // Remainder is a file name, skip file extension.
  for( pc = ptail - 1; pc > phead && *pc != '.'; pc-- )
  {}

  if( *pc == '.' && pc != phead ) {
    ptail = pc;
  }

  strlcpy( result, phead, min( ptail - phead + 1, size ));
  return result;
}

/* Returns the drive letter or scheme and the path of
   subdirectories, if any, including the trailing slash.
   Slashes (/), backslashes (\), or both may be present
   in location. */
char*
sdrivedir( char *result, const char* location, size_t size )
{
  const char* phead  = location;
  const char* ptail  = location + strlen( location );
  const char* pc;

  BOOL  cuesheet = FALSE;
  BOOL  url = FALSE;

  if( is_cuesheet( location )) {
    phead = location = location + 7;
    cuesheet = TRUE;
  }

  // If the parse string contain a colon after the 1st character
  // and before any characters not allowed as part of a scheme
  // name (i.e. any not alphanumeric, '+', '.' or '-'),
  // the scheme of the url is the substring of chars up to
  // but not including the first colon. These chars and the
  // colon are then removed from the parse string before
  // continuing.
  while( isalnum( *phead ) || ( *phead == '+' ) || ( *phead == '-' ) || ( *phead == '.' )) {
    ++phead;
  }
  if( *phead != ':' ) {
    phead = location;
  } else {
    phead++;
    if( phead - location > 2 || !isalpha( *location )) {
      // If the scheme consists more than of one symbol or its first
      // symbol not the alphabetic character that it is exact not a
      // name of the drive.
      url = TRUE;
    }
  }

  // Remove track identifier if any, from the back of the cue sheet url.
  if( cuesheet ) {
    if(( pc = strrchr( phead, ',' )) != NULL ) {
      ptail = pc;
    }
  }

  // Skip location (user:password@host:port part of the url
  // or \\server part of the regular pathname) on the front.
  if(( url || location == phead ) &&
     ( isslash( phead[0] ) && isslash( phead[1] )))
  {
    phead += 2;
    while( *phead && !isslash( *phead )) {
      ++phead;
    }
  }

  // Remove fragment identifier, parameters or query information,
  // if any, from the back of the url.
  if( url ) {
    if(( pc = strpbrk( phead, "#?;" )) != NULL ) {
      ptail = pc;
    }
  }

  // Search end of the path name.
  for( pc = ptail - 1; pc > phead && !isslash( *pc ); pc-- )
  {}

  if( isslash( *pc )) {
    ptail = pc + 1;
  }

  phead = location;

  strlcpy( result, phead, min( ptail - phead + 1, size ));
  return result;
}

/* Decodes any string value from URL transmission. */
char*
sdecode( char* result, const char* location, size_t size )
{
  const char* digits = "0123456789ABCDEF";
  const char* p;
  const char* ps = location;
  char* pr = result;
  int   i;

  if( size-- ) {
    while( *ps && size )
    {
      if( *ps == '%' ) {
        if( *ps && ( p = strchr( digits, toupper( *++ps ))) != NULL ) {
          i = p - digits;
          if( *ps && ( p = strchr( digits, toupper( *++ps ))) != NULL ) {
            i = i * 16 + p - digits;
            if( size ) {
              *pr = (char)i;
            }
          }
        }
      } else {
        if( size ) {
          *pr = *ps;
        }
      }
      ++ps;
      ++pr;
      --size;
    }
    *pr = 0;
  }

  return result;
}

/* Returns TRUE if the specified location is a root directory. */
BOOL
is_root( const char* location )
{
  return strlen( location ) == 3
         && location[1] == ':'
         && isslash( location[2] );
}

/* Returns TRUE if the specified location is a directory. */
BOOL
is_dir( const char* location )
{
  struct stat fi;
  return ( stat( location, &fi ) == 0 ) && ( fi.st_mode & S_IFDIR );
}

/* Creates a single path name, composed of a base path name and file
 * or directory name. */
char*
smakepath( char* result, const char* pathname, const char* name, int size )
{
  int path_size;

  strlcpy( result, pathname, size );
  path_size = strlen( result );

  if( path_size && !isslash( result[ path_size - 1 ] )) {
    strlcat( result, "\\", size );
  }

  strlcat( result, name, size );
  return result;
}

/* Creates full path and specified directory. */
int
mkcomplexdir( const char* location )
{
  char*  pathname = strdup( location );
  char*  p;
  size_t len;
  int    rc;

  struct stat fs;

  DEBUGLOG(( "mkcomplexdir: %s\n", location ));

  if( !pathname ) {
    return -1;
  }

  len = strlen ( pathname );
  p   = strrchr( pathname, '\\' );

  if( p && p == ( pathname + len - 1 )) {
    if( len == 3 && pathname[1] == ':' ) {
      return 0; // Disk folder;
    }
    *p = 0;
    --len;
  }

  p = pathname + len;

  for(;;) {

    #if defined(__GNUC__)
    rc = mkdir( pathname, 0755 );
    #else
    rc = mkdir( pathname );
    #endif

    DEBUGLOG(( "mkcomplexdir [1]: try to create %s, rc=%d\n", pathname, rc == 0 ? 0 : errno ));

    if( rc == 0 ) {
      break;
    }

    if( errno == EACCES ) {
      if( stat( pathname, &fs ) != 0 ) {
        return -1;
      }
      if(!( fs.st_mode & S_IFDIR )) {
        errno = EACCES;
        return -1;
      }
      break;
    }
    p = strrchr( pathname, '\\' );
    if( !p || p == pathname || p[-1] == ':' ) {
      return -1;
    }
    *p = 0;
  }

  strcpy( pathname, location );

  while( p < pathname + len ) {
    p = strchr( p + 1, '\\' );
    if( !p ) {
      p = pathname + len;
    }
    *p = 0;

    #if defined(__GNUC__)
    rc = mkdir( pathname, 0755 );
    #else
    rc = mkdir( pathname );
    #endif

    DEBUGLOG(( "mkcomplexdir [2]: try to create %s, rc=%d\n", pathname, rc == 0 ? 0 : errno ));
    if( rc != 0 ) {
      return -1;
    }
    *p = '\\';
  }

  return 0;
}

#if 0
int
main( int argc, char* argv[] )
{
  char result[4096] = "";

  if( argc < 3 ) {
    fprintf( stderr, "usage: fileutil <utility> <location>\n" );
    return 1;
  }

  if( stricmp( argv[1], "strack" ) == 0 ) {
    ltoa( strack( argv[2] ), result, 10 );
  } else if( stricmp( argv[1], "sdrive" ) == 0 ) {
    sdrive( result, argv[2], sizeof( result ));
  } else if( stricmp( argv[1], "scheme" ) == 0 ) {
    scheme( result, argv[2], sizeof( result ));
  } else if( stricmp( argv[1], "sfnameext" ) == 0 ) {
    sfnameext( result, argv[2], sizeof( result ));
  } else if( stricmp( argv[1], "sfext" ) == 0 ) {
    sfext( result, argv[2], sizeof( result ));
  } else if( stricmp( argv[1], "sfname" ) == 0 ) {
    sfname( result, argv[2], sizeof( result ));
  } else if( stricmp( argv[1], "sdrivedir" ) == 0 ) {
    sdrivedir( result, argv[2], sizeof( result ));
  } else {
    fprintf( stderr, "unknown utility: %s\n", argv[1] );
  }

  printf( "%s\n", result );
  return 0;
}
#endif
