/*
 * Copyright 2004-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FILEUTIL_H
#define FILEUTIL_H

#ifdef __cplusplus
extern "C" {
#endif

// Because the result string always less or is equal to a location
// string all functions can safely use the same storage area for a
// location and result.

/* Returns the drive letter followed by a colon if a drive is specified in the location. */
char* sdrive( char* result, const char* location, size_t size );
/* Returns the track number if it is specified in location, otherwise returns 0. */
int   strack( const char* location );
/* Returns the scheme followed by a colon if a scheme is specified in the location. */
char* scheme( char* result, const char* location, size_t size );
/* Returns the base file name without any extensions. */
char* sfname( char* result, const char* location, size_t size );
/* Returns the file name extension, if any, including the leading period. */
char* sfext( char* result, const char* location, size_t size );
/* Returns the base file name with file extension. */
char* sfnameext( char* result, const char* location, size_t size );
/* Returns the drive letter or scheme and the path of subdirectories, if any,
 * including the trailing slash. */
char* sdrivedir( char* result, const char* location, size_t size );
/* Creates a single path name, composed of a base path name and file
 * or directory name. */
char* smakepath( char* result, const char* pathname, const char* name, int size );

/* Returns TRUE if the specified location is a CD track. */
BOOL is_track( const char* location );
/* Returns TRUE if the specified location is a cue sheet track. */
BOOL is_cuesheet( const char* location );
/* Returns TRUE if the specified location is a regular file or directory.
   This also includes tracks of a cue sheet if they are regular files. */
BOOL is_file( const char* location );
/* Returns TRUE if the specified location is a regular file or directory. */
BOOL is_regular_file( const char* location );
/* Returns TRUE if the specified location is a URL. This also includes tracks of
   a cue sheet that is addressed by the URL.*/
BOOL is_url( const char* location );
/* Returns TRUE if the specified location is a root directory. */
BOOL is_root( const char* location );
/* Returns TRUE if the specified location is a directory. */
BOOL is_dir( const char* location );

/* Decodes any string value from URL transmission. */
char* sdecode( char* result, const char* location, size_t size );

/* Creates full path and specified directory. */
int mkcomplexdir( const char* pathname );

#ifdef __cplusplus
}
#endif

#endif /* FILEUTIL_H */
