/*
 * Copyright 2011 Dmitry A.Steklenev
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_BASE
#include <os2.h>
#include <string.h>
#include "pipe.h"

/* Opens the specified pipe. */
HPIPE
pipe_open( const char* pipename )
{
  HPIPE  hpipe;
  ULONG  action;
  APIRET rc;
  ULONG  attempts = 0;

  while(( rc = DosOpen( (PSZ)pipename, &hpipe, &action, 0, FILE_NORMAL,
                        OPEN_ACTION_FAIL_IF_NEW  | OPEN_ACTION_OPEN_IF_EXISTS,
                        OPEN_SHARE_DENYREADWRITE | OPEN_ACCESS_READWRITE | OPEN_FLAGS_FAIL_ON_ERROR,
                        NULL )) == ERROR_PIPE_BUSY && attempts++ < 100 )
  {
    DosSleep(1);
  }

  if( rc == NO_ERROR ) {
    return hpipe;
  } else {
    return NULLHANDLE;
  }
}

/* Closes the specified pipe. */
BOOL
pipe_close( HPIPE hpipe ) {
  return ( DosClose( hpipe ) == NO_ERROR );
}

/* Writes string to the specified pipe. */
BOOL
pipe_write( HPIPE hpipe, const char* string )
{
  ULONG done;
  ULONG size = strlen( string ) + 1;

  DosWrite( hpipe, (PVOID)string, size, &done );
  DosResetBuffer( hpipe );

  return size == done;
}

/* Opens the specified pipe and writes data to it. */
BOOL
pipe_open_and_write( const char* pipename, const char* string )
{
  HPIPE hpipe = pipe_open( pipename );

  if( hpipe != NULLHANDLE )
  {
    pipe_write( hpipe, string );
    pipe_close( hpipe );
    return TRUE;
  } else {
    return FALSE;
  }
}

/* Reads string from the specified pipe. */
char*
pipe_read( HPIPE hpipe, char* buffer, int size )
{
  ULONG read = 0;
  int   done = 0;
  char* p    = buffer;

  while( done < size - 1 ) {
    if( DosRead( hpipe, p, 1, &read ) == NO_ERROR && read == 1 ) {
      if( *p == '\r' ) {
        continue;
      } else if( *p == '\n' || !*p ) {
        break;
      } else {
        ++p;
        ++done;
      }
    } else {
      if( !done ) {
        return NULL;
      } else {
        break;
      }
    }
  }

  *p = 0;
  return buffer;
}

