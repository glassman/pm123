/*
 * Copyright 2011-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_PM
#define  INCL_ERRORS
#include <os2.h>
#include <string.h>
#include <stdio.h>
#include <process.h>
#include <errno.h>

#include <decoder_plug.h>
#include <plugin.h>
#include <debuglog.h>
#include <charset.h>
#include <utilfct.h>
#include <FLAC\callback.h>
#include <FLAC\stream_decoder.h>
#include <FLAC\metadata.h>

#include "flacplay.h"

#ifndef  EIO
#define  EIO EBADF
#endif

static DECODER_STRUCT** instances = NULL;
static int  instances_count = 0;
static HMTX mutex;

/* Read callback. The signature and semantics match POSIX fread()
   implementations and can generally be used interchangeably. */
static size_t io_read_cb( void* ptr, size_t size, size_t nmemb, FLAC__IOHandle handle ) {
  return xio_fread( ptr, size, nmemb, (XFILE*)handle );
}

/* Write callback. The signature and semantics match POSIX fwrite()
   implementations and can generally be used interchangeably. */
static size_t io_write_cb( const void* ptr, size_t size, size_t nmemb, FLAC__IOHandle handle ) {
  return xio_fwrite((void*)ptr, size, nmemb, (XFILE*)handle );
}

/* Seek callback. The signature and semantics mostly match POSIX fseek()
   WITH ONE IMPORTANT EXCEPTION: the offset is a 64-bit type whereas fseek() is
   generally 'long' and 32-bits wide. */
static int io_seek_cb( FLAC__IOHandle handle, FLAC__int64 offset, int whence ) {
  return xio_fseek((XFILE*)handle, offset, whence );
}

/* Tell callback. The signature and semantics mostly match POSIX ftell()
   WITH ONE IMPORTANT EXCEPTION: the offset is a 64-bit type whereas ftell()
   is generally 'long' and 32-bits wide. */
static FLAC__int64 io_tell_cb( FLAC__IOHandle handle ) {
  return xio_ftell((XFILE*)handle );
}

/* EOF callback. The signature and semantics mostly match POSIX feof() */
static int io_eof_cb( FLAC__IOHandle handle ) {
  return xio_feof((XFILE*)handle );
}

/* Close callback. The signature and semantics match POSIX fclose() implementations
   and can generally be used interchangeably. */
static int io_close_cb( FLAC__IOHandle handle ) {
  return xio_fclose((XFILE*)handle );
}

/* Called when the decoder needs to seek the input stream. The decoder will
   pass the absolute byte offset to seek to, 0 meaning the beginning of
   the stream. */
static FLAC__StreamDecoderSeekStatus
flac_seek_cb( const FLAC__StreamDecoder* decoder, FLAC__uint64 absolute_byte_offset, void* w )
{
  XFILE* x = ((DECODER_STRUCT*)w)->file;

  if( !xio_can_seek(x) && absolute_byte_offset > 0 ) {
    return FLAC__STREAM_DECODER_SEEK_STATUS_UNSUPPORTED;
  }
  if( xio_fseek( x, absolute_byte_offset, XIO_SEEK_SET ) < 0 ) {
    return FLAC__STREAM_DECODER_SEEK_STATUS_ERROR;
  }

  return FLAC__STREAM_DECODER_SEEK_STATUS_OK;
}

/* Called when the decoder wants to know the current position of the
   stream. The callback should return the byte offset from the
   beginning of the stream. */
static FLAC__StreamDecoderTellStatus
flac_tell_cb( const FLAC__StreamDecoder* decoder, FLAC__uint64* absolute_byte_offset, void* w )
{
  XFILE* x = ((DECODER_STRUCT*)w)->file;
  long pos = xio_ftell( x );

  if( pos < 0 ) {
    return FLAC__STREAM_DECODER_TELL_STATUS_ERROR;
  } else {
   *absolute_byte_offset = pos;
    return FLAC__STREAM_DECODER_TELL_STATUS_OK;
  }
}

/* Called when the decoder needs to know if the end of the
   stream has been reached. */
static FLAC__bool
flac_eof_cb( const FLAC__StreamDecoder* decoder, void* data )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)data;
  return ( w->flac_error != -1 || xio_feof( w->file )) ? true : false;
}

/* Called when the decoder needs more input data. The address of the buffer
   to be filled is supplied, along with the number of bytes the buffer can
   hold. The callback may choose to supply less data and modify the byte
   count but must be careful not to overflow the buffer. The callback then
   returns a status code chosen from FLAC__StreamDecoderReadStatus. */
static FLAC__StreamDecoderReadStatus
flac_read_cb( const FLAC__StreamDecoder* decoder, FLAC__byte buffer[], size_t* bytes, void* w )
{
  XFILE* x = ((DECODER_STRUCT*)w)->file;
  size_t read = *bytes;

  if( read > 0 ) {
    *bytes = xio_fread( buffer, sizeof( FLAC__byte ), read, x );

    if( *bytes != read ) {
      if( xio_ferror( x )) {
        return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
      } else {
        return FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM;
      }
    } else {
      return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
    }
  }

  return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
}

/* Called when the decoder wants to know the total length of the
   stream in bytes. */
static FLAC__StreamDecoderLengthStatus
flac_length_cb( const FLAC__StreamDecoder* decoder, FLAC__uint64* stream_length, void* w )
{
  XFILE*  x = ((DECODER_STRUCT*)w)->file;
  long size = xio_fsize(x);

  if( size == -1 ) {
    return FLAC__STREAM_DECODER_LENGTH_STATUS_UNSUPPORTED;
  }

  *stream_length = (FLAC__uint64)size;
  return FLAC__STREAM_DECODER_LENGTH_STATUS_OK;
}

static BOOL
flac_flush_output( DECODER_STRUCT* w )
{
  if( w->output_play_samples( w->a, &w->output_format, w->buffer, w->bufpos, w->posmarker ) != w->bufpos ) {
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    return FALSE;
  }

  w->posmarker = -1;
  w->bufpos    =  0;

  return TRUE;
}

/* Called when the decoder has decoded a single audio frame. The
   decoder will pass the frame metadata as well as an array of
   pointers (one for each channel) pointing to the decoded audio. */
static FLAC__StreamDecoderWriteStatus
flac_play_cb( const FLAC__StreamDecoder* decoder,
              const FLAC__Frame* frame, const FLAC__int32* const buffer[],
              void* data )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)data;

  unsigned int samples  = frame->header.blocksize;
  unsigned int channels = frame->header.channels;
  unsigned int bytespersample;
  unsigned int ch, i;
  FLAC__int32  scale;

  DEBUGLOG2(( "flacplay: play %d length block, ch = %d [%d], bps = %d\n",
               samples, channels, frame->header.channel_assignment, frame->header.bits_per_sample ));

  if( !w->output_play_samples || !w->buffer || w->noplay ) {
    return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
  }

  if( w->startpos && !w->start_pcms ) {
    w->start_pcms = w->startpos / 1000.0 * frame->header.sample_rate;
  }
  if( w->endpos && !w->end_pcms ) {
    w->end_pcms = w->endpos / 1000.0 * frame->header.sample_rate - 1;
  }
  if( frame->header.number_type == FLAC__FRAME_NUMBER_TYPE_FRAME_NUMBER ) {
    w->played_pcms = frame->header.number.frame_number * samples;
  } else {
    w->played_pcms = frame->header.number.sample_number;
  }

  scale = 1 << ( w->output_format.bits - frame->header.bits_per_sample );
  bytespersample = w->output_format.bits / 8;

  for( i = 0; i < samples; i++ )
  {
    if( w->posmarker == -1 ) {
      w->posmarker = 1000.0 * w->played_pcms / frame->header.sample_rate - w->startpos;
    }

    if( w->end_pcms && w->played_pcms >= w->end_pcms ) {
      DEBUGLOG(( "flacplay: break at %d because end of fragment is reached\n", w->played_pcms ));
      break;
    }

    for( ch = 0; ch < channels; ch++ )
    {
      FLAC__int32 sample = buffer[ch][i] * scale;

      switch( w->output_format.bits ) {
        case  8: PUTSMPL8U( w->buffer + w->bufpos, sample ); break;
        case 16: PUTSMPL16( w->buffer + w->bufpos, sample ); break;
        case 24: PUTSMPL24( w->buffer + w->bufpos, sample ); break;
        case 32: PUTSMPL32( w->buffer + w->bufpos, sample ); break;
      }

      w->bufpos += bytespersample;

      if( w->bufpos == w->audio_buffersize ) {
        flac_flush_output( w );
      }
    }

    w->played_pcms++;
  }

  w->resync = 0;
  return ( i < samples ) ? FLAC__STREAM_DECODER_WRITE_STATUS_ABORT
                         : FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

/* Called when the decoder has decoded a metadata block. */
static void
flac_metadata_cb( const FLAC__StreamDecoder*  decoder,
                  const FLAC__StreamMetadata* metadata, void* data )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)data;
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  switch( metadata->type ) {
    case FLAC__METADATA_TYPE_STREAMINFO:
      w->output_format.size       = sizeof( w->output_format );
      w->output_format.format     = WAVE_FORMAT_PCM;
      w->output_format.channels   = metadata->data.stream_info.channels;
      w->output_format.samplerate = metadata->data.stream_info.sample_rate;
      w->bps                      = metadata->data.stream_info.bits_per_sample;

      if( w->bps <= 8 ) {
        w->output_format.bits = 8;
      } else if( w->bps <= 16 ) {
        w->output_format.bits = 16;
      } else if( w->bps <= 24 ) {
        w->output_format.bits = 24;
      } else {
        w->output_format.bits = 32;
      }

      w->songlength = ( metadata->data.stream_info.total_samples * 1000.0 ) /
                        metadata->data.stream_info.sample_rate;
      w->bitrate    = ( metadata->data.stream_info.sample_rate * metadata->data.stream_info.bits_per_sample *
                        metadata->data.stream_info.channels ) / 1000;
      w->blocksize  = ( metadata->data.stream_info.min_blocksize +
                        metadata->data.stream_info.max_blocksize ) / 2;
      break;

    default:
      break;
  }

  DosReleaseMutexSem( w->mutex );
}

/* Called whenever an error occurs during decoding. */
static void
flac_error_cb( const FLAC__StreamDecoder* decoder,
               FLAC__StreamDecoderErrorStatus status, void* data )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)data;

  if( status == FLAC__STREAM_DECODER_ERROR_STATUS_LOST_SYNC ) {
    if( ++w->resync < MAXRESYNC ) {
      return;
    }
  }

  w->flac_error = status;
  DEBUGLOG(( "flacplay: activated error callback, status = %d\n", status ));

  if( w->hwnd ) {
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  }
}

/* Seek to an absolute sample. */
static BOOL
flac_seek( DECODER_STRUCT* w, FLAC__uint64 sample )
{
  BOOL rc;

  // Because the FLAC decoder flushes the input before a start of seeking
  // it is necessary to skip this data to avoid some side effects.
  w->noplay = TRUE;
  if(( rc = FLAC__stream_decoder_seek_absolute( w->flac_decoder, sample )) != TRUE ) {
    FLAC__stream_decoder_flush( w->flac_decoder );
  }
  w->noplay = FALSE;
  return rc;
}

/* Closes FLAC file. */
static void
flac_close( DECODER_STRUCT* w )
{
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->flac_chain ) {
    FLAC__metadata_chain_delete( w->flac_chain );
    w->flac_chain = NULL;
  }
  if( w->flac_decoder ) {
    FLAC__stream_decoder_delete( w->flac_decoder );
    w->flac_decoder = NULL;
  }
  if( w->file ) {
    xio_fclose( w->file );
    w->file = NULL;
  }

  DosReleaseMutexSem( w->mutex );
}

/* Returns TRUE if the specified stream is a OGG stream. */
static BOOL
flac_is_ogg( XFILE* file )
{
  FLAC__byte id[4] = { 0 };
  long pos = xio_ftell( file );

  xio_rewind( file );
  xio_fread( id, 1, 4, file );
  xio_fseek( file, pos, XIO_SEEK_SET );

  return id[0] == 0x4F &&
         id[1] == 0x67 &&
         id[2] == 0x67 &&
         id[3] == 0x53;
}

/* Opens FLAC file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static ULONG
flac_open( DECODER_STRUCT* w )
{
  FLAC__StreamDecoderInitStatus rc;
  FLAC__IOCallbacks io_cb;

  io_cb.read  = io_read_cb;
  io_cb.write = io_write_cb;
  io_cb.seek  = io_seek_cb;
  io_cb.tell  = io_tell_cb;
  io_cb.eof   = io_eof_cb;
  io_cb.close = io_close_cb;

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  w->flac_error = -1;
  w->resync = 0;

  DEBUGLOG(( "flacplay: try to open file %s\n", w->filename ));

  if(( w->file = xio_fopen( w->filename, "rb" )) == NULL ) {
    DosReleaseMutexSem( w->mutex );
    DEBUGLOG(( "flacplay: xio_fopen returns error: %s\n", xio_strerror( xio_errno())));
    return xio_errno();
  }

  w->seekable = xio_can_seek( w->file );

  if(( w->flac_decoder = FLAC__stream_decoder_new()) == NULL ) {
    DosReleaseMutexSem( w->mutex );
    return errno;
  }

  if(( w->ogg = flac_is_ogg( w->file )) == TRUE )
  {
    rc = FLAC__stream_decoder_init_ogg_stream( w->flac_decoder,
                                               flac_read_cb,
                                               flac_seek_cb,
                                               flac_tell_cb,
                                               flac_length_cb,
                                               flac_eof_cb,
                                               flac_play_cb,
                                               flac_metadata_cb,
                                               flac_error_cb, w );
  } else {
    if( w->seekable ) {
      if(( w->flac_chain = FLAC__metadata_chain_new()) == NULL ) {
        DosReleaseMutexSem( w->mutex );
        return ENOMEM;
      }

      if( !FLAC__metadata_chain_read_with_callbacks( w->flac_chain, w->file, io_cb )) {
        if( FLAC__metadata_chain_status( w->flac_chain ) == FLAC__METADATA_CHAIN_STATUS_READ_ERROR ) {
          return xio_errno();
        } else {
          return -1;
        }
      }

      FLAC__metadata_chain_merge_padding( w->flac_chain );
      xio_rewind( w->file );
    }

    rc = FLAC__stream_decoder_init_stream( w->flac_decoder,
                                           flac_read_cb,
                                           flac_seek_cb,
                                           flac_tell_cb,
                                           flac_length_cb,
                                           flac_eof_cb,
                                           flac_play_cb,
                                           flac_metadata_cb,
                                           flac_error_cb, w );
  }

  DEBUGLOG(( "flacplay: decoder stream initialization completed, rc = %d, decoder state = %d\n",
             rc, FLAC__stream_decoder_get_state( w->flac_decoder )));

  if( rc != FLAC__STREAM_DECODER_INIT_STATUS_OK ) {
    DosReleaseMutexSem( w->mutex );
    flac_close( w );
    return -1;
  }
  if( !FLAC__stream_decoder_process_until_end_of_metadata( w->flac_decoder )) {
    DosReleaseMutexSem( w->mutex );
    flac_close( w );
    return -1;
  }

  DEBUGLOG(( "flacplay: metadata initialization completed, decoder state = %d\n",
             FLAC__stream_decoder_get_state( w->flac_decoder )));

  DosReleaseMutexSem( w->mutex );
  return 0;
}

/* Returns a specified field of the given Ogg Vorbis comment. */
static char*
flac_get_string( FLAC__StreamMetadata* meta, char* target, char* type, int size )
{
  FLAC__uint32 i;
  int type_len = strlen( type );

 *target = 0;

  for( i = 0; i < meta->data.vorbis_comment.num_comments; i++ )
  {
    FLAC__StreamMetadata_VorbisComment_Entry entry =
          meta->data.vorbis_comment.comments[i];

    if( strnicmp((char*)entry.entry, type, type_len ) == 0 &&
        entry.entry[type_len] == '=' )
    {
      ch_convert( CH_UTF_8, (char*)entry.entry + type_len + 1, CH_DEFAULT, target, size );
      break;
    }
  }

  return target;
}

/* Sets a specified field of the given Ogg Vorbis comment. */
static BOOL
flac_set_string( FLAC__StreamMetadata* meta, char* source, char* type )
{
  FLAC__StreamMetadata_VorbisComment_Entry entry;
  FLAC__uint32 typelen = strlen( type );
  BOOL rc;

  entry.length = strlen( source ) * 4 + typelen + 2;

  if(( entry.entry = (FLAC__byte*)malloc( entry.length )) == NULL ) {
    return FALSE;
  }

  strlcpy((char*)entry.entry, type, entry.length );
  strlcat((char*)entry.entry, "=",  entry.length );

  ch_convert( CH_DEFAULT, source, CH_UTF_8, (char*)entry.entry + typelen + 1, entry.length - typelen );
  entry.length = strlen((char*)entry.entry );

  rc = FLAC__metadata_object_vorbiscomment_replace_comment( meta, entry, false, true );
  free( entry.entry );

  return rc;
}

/* Removes a specified field of the given Ogg Vorbis comment. */
static BOOL
flac_remove_string( FLAC__StreamMetadata* meta, char* type ) {
  return FLAC__metadata_object_vorbiscomment_remove_entry_matching( meta, type ) != -1;
}

/* Notify an error. */
static void
flac_show_error( DECODER_STRUCT* w, int style, const char* msg, int rc )
{
  char buff[1024];

  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );

  if( rc == -1 ) {
    strlcat( buff, "Unsupported format of the file.", sizeof( buff ));
  } else if( xio_errno()) {
    strlcat( buff, xio_strerror( xio_errno()), sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  ULONG resetcount;
  ULONG rc;

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  w->frew = FALSE;
  w->ffwd = FALSE;

  if(( rc = flac_open( w )) != 0 ) {
    flac_show_error( w, MB_ERROR, "Unable open file", rc );
    goto end;
  }

  // After opening a new file we so are in its beginning.
  if( w->jumptopos == 0 && w->startpos == 0 ) {
      w->jumptopos = -1;
  }

  if(!( w->buffer = (char*)malloc( w->audio_buffersize ))) {
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    w->status = DECODER_ERROR;
    goto end;
  }

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    w->played_pcms =  0;
    w->bufpos      =  0;
    w->posmarker   = -1;
    w->start_pcms  =  0;
    w->end_pcms    =  0;

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop ) {
      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

      if( w->jumptopos >= 0 )
      {
        FLAC__uint64 pcms = ( w->jumptopos + w->startpos ) / 1000.0 * w->output_format.samplerate;

        w->posmarker = -1;
        w->bufpos    =  0;

        DEBUGLOG(( "flacplay: jumps to %ld ms (%lld samples)\n", w->jumptopos + w->startpos, pcms ));
        rc = flac_seek( w, pcms );
        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        if( !rc ) {
          flac_show_error( w, MB_WARNING, "Unable seek in file", xio_errno());
        }

        w->jumptopos = -1;
      } else if( w->ffwd ) {
        // The skip of this interval will force the decoder to be
        // accelerated approximately in 10 times.
        if( !flac_seek( w, w->played_pcms + w->blocksize * 9  )) {
          DosReleaseMutexSem( w->mutex );
          break;
        }
      } else if( w->frew ) {
        // The skip of this interval will force the decoder to be
        // accelerated approximately in 10 times.
        FLAC__uint64 skip = w->blocksize * 11;

        if( w->played_pcms < skip ||
            w->played_pcms - skip < w->start_pcms ||
            !flac_seek( w, w->played_pcms - skip ))
        {
          DosReleaseMutexSem( w->mutex );
          break;
        }
      }

      if( !FLAC__stream_decoder_process_single( w->flac_decoder ) ||
           FLAC__stream_decoder_get_state( w->flac_decoder ) >= FLAC__STREAM_DECODER_END_OF_STREAM )
      {
        DosReleaseMutexSem( w->mutex );
        if( xio_ferror( w->file )) {
          flac_show_error( w, MB_ERROR, "Unable read file", xio_errno());
          DosReleaseMutexSem( w->mutex );
          goto end;
        } else {
          break;
        }
      }

      DosReleaseMutexSem( w->mutex );
    }

    flac_flush_output( w );
    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

  w->status = DECODER_STOPPED;

end:

  flac_close( w );
  free( w->buffer );
  w->buffer = NULL;

  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );
  DosCreateMutexSem( NULL, &w->mutex, 0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  instances = realloc( instances, ++instances_count * sizeof( DECODER_STRUCT* ));
  instances[ instances_count - 1 ] = w;
  DosReleaseMutexSem( mutex );

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than this is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  int i;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  decoder_command( w, DECODER_STOP, NULL );

  for( i = 0; i < instances_count; i++ ) {
    if( instances[i] == w ) {
      if( i < instances_count - 1 ) {
        memmove( instances + i, instances + i + 1,
               ( instances_count - i - 1 ) * sizeof( DECODER_STRUCT* ));
      }
      instances = realloc( instances, --instances_count * sizeof( DECODER_STRUCT* ));
    }
  }

  DosReleaseMutexSem( mutex  );
  DosCloseEventSem( w->play  );
  DosCloseMutexSem( w->mutex );

  free( w );
  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = arg;

  switch( msg )
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      w->metadata_buff       = info->metadata_buffer;
      w->metadata_buff[0]    = 0;
      w->metadata_size       = info->metadata_size;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
      strlcpy( w->filename, info->filename, sizeof( w->filename ));
      DosReleaseMutexSem( w->mutex );

      w->jumptopos  = info->jumpto;
      w->startpos   = info->start;
      w->endpos     = info->end;
      w->status     = DECODER_STARTING;
      w->stop       = FALSE;
      w->songlength = 0;
      w->seekable   = TRUE;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;

      if( w->file ) {
        xio_fabort( w->file );
      }

      DosPostEventSem( w->play  );
      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_FFWD:
      if( info->ffwd ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->ffwd = info->ffwd;
      break;

    case DECODER_REW:
      if( info->rew ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->frew = info->rew;
      break;

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      if( w->status == DECODER_STOPPED ) {
        DosPostEventSem( w->play );
      }
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }

  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = arg;

  if( w->seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else {
      return w->songlength;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  FLAC__Metadata_Iterator* i = NULL;
  DECODER_STRUCT* w;
  int rc;
  int count = 0;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  strlcpy( w->filename, filename, sizeof( w->filename ));
  DosReleaseMutexSem( w->mutex );

  if(( rc = flac_open( w )) != 0 ) {
    decoder_uninit( w );
    return rc == -1 ? PLUGIN_NO_PLAY : PLUGIN_NO_READ;
  }

  // Because the decoder hopes for the best up to EOF it is necessary to
  // check up errors which could be found during decoder initialization.
  // It is FLAC__STREAM_DECODER_ERROR_STATUS_LOST_SYNC usually for
  // non-FLAC streams.
  if( w->flac_error != -1 ) {
    flac_close( w );
    decoder_uninit( w );
    return PLUGIN_NO_PLAY;
  }

  if(( i = FLAC__metadata_iterator_new()) == NULL ) {
    return ENOMEM;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  info->format.size       = sizeof( w->output_format );
  info->format.format     = WAVE_FORMAT_PCM;
  info->format.bits       = w->output_format.bits;
  info->format.channels   = w->output_format.channels;
  info->format.samplerate = w->output_format.samplerate;
  info->mode              = w->output_format.channels == 1 ? 3 : 0;
  info->songlength        = w->songlength;
  info->bitrate           = w->bitrate;

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = xio_fsize64( w->file );
  }

  snprintf( info->tech_info, sizeof( info->tech_info ), "%u kbs, %.1f kHz, %d bits, %s",
            info->bitrate, ( info->format.samplerate / 1000.0 ), w->bps,
            info->format.channels == 1 ? "Mono" : "Stereo" );

  xio_get_metainfo( w->file, XIO_META_GENRE, info->genre,   sizeof( info->genre   ));
  xio_get_metainfo( w->file, XIO_META_NAME,  info->comment, sizeof( info->comment ));
  xio_get_metainfo( w->file, XIO_META_TITLE, info->title,   sizeof( info->title   ));

  if( w->flac_chain )
  {
    FLAC__metadata_iterator_init( i, w->flac_chain );

    do {
      switch( FLAC__metadata_iterator_get_block_type(i)) {
        case FLAC__METADATA_TYPE_VORBIS_COMMENT:
        {
          FLAC__StreamMetadata* meta = FLAC__metadata_iterator_get_block(i);

          flac_get_string( meta, info->artist,  "ARTIST",  sizeof( info->artist  ));
          flac_get_string( meta, info->album,   "ALBUM",   sizeof( info->album   ));
          flac_get_string( meta, info->title,   "TITLE",   sizeof( info->title   ));
          flac_get_string( meta, info->genre,   "GENRE",   sizeof( info->genre   ));
          flac_get_string( meta, info->year,    "YEAR" ,   sizeof( info->year    ));
          flac_get_string( meta, info->comment, "COMMENT", sizeof( info->comment ));

          if( !*info->year ) {
            flac_get_string( meta, info->year, "DATE", sizeof( info->year ));
          }

          if( HAVE_FIELD( info, track ))
          {
            char buffer[128];

            flac_get_string( meta, info->copyright, "COPYRIGHT",   sizeof( info->copyright ));
            flac_get_string( meta, info->disc,      "DISCNUMBER",  sizeof( info->disc      ));
            flac_get_string( meta, info->track,     "TRACKNUMBER", sizeof( info->track     ));

            flac_get_string( meta, buffer, "replaygain_album_gain", sizeof( buffer ));
            info->album_gain = atof( buffer );
            flac_get_string( meta, buffer, "replaygain_album_peak", sizeof( buffer ));
            info->album_peak = atof( buffer );
            flac_get_string( meta, buffer, "replaygain_track_gain", sizeof( buffer ));
            info->track_gain = atof( buffer );
            flac_get_string( meta, buffer, "replaygain_track_peak", sizeof( buffer ));
            info->track_peak = atof( buffer );
          }

          break;
        }

        case FLAC__METADATA_TYPE_PICTURE:
          if( HAVE_FIELD( info, pics_count )) {
            if( options & DECODER_LOADPICS )
            {
              FLAC__StreamMetadata* meta = FLAC__metadata_iterator_get_block(i);

              APIC** pics;
              APIC*  pic;

              if(( pics = info->memalloc(( count + 1 ) * sizeof( APIC* ))) == NULL ) {
                break;
              }

              memcpy( pics, info->pics, count * sizeof( APIC* ));
              info->memfree( info->pics );
              info->pics = pics;

              if(( info->pics[count] = info->memalloc( meta->data.picture.data_length + sizeof( APIC ))) == NULL ) {
                break;
              }

              pic = info->pics[count];
              pic->size = meta->data.picture.data_length + sizeof( APIC );
              pic->type = meta->data.picture.type;

              strlcpy( pic->mimetype, meta->data.picture.mime_type, sizeof( pic->mimetype ));
              ch_convert( CH_UTF_8, (char*)meta->data.picture.description, CH_DEFAULT, pic->desc, sizeof( pic->desc ));
              memcpy((char*)pic + sizeof( APIC ), meta->data.picture.data, meta->data.picture.data_length );
            }
            info->pics_count = ++count;
          }
          break;

        default:
          break;
      }

    } while( FLAC__metadata_iterator_next(i));
  }

  DosReleaseMutexSem( w->mutex );
  DEBUGLOG(( "flacplay: found %d attached pictures\n", info->pics_count ));

  if( HAVE_FIELD( info, haveinfo ))
  {
    info->saveinfo = is_file( filename ) && !w->ogg;
    info->haveinfo = DECODER_HAVE_TITLE      |
                     DECODER_HAVE_ARTIST     |
                     DECODER_HAVE_ALBUM      |
                     DECODER_HAVE_YEAR       |
                     DECODER_HAVE_GENRE      |
                     DECODER_HAVE_TRACK      |
                     DECODER_HAVE_DISC       |
                     DECODER_HAVE_COMMENT    |
                     DECODER_HAVE_COPYRIGHT  |
                     DECODER_HAVE_TRACK_GAIN |
                     DECODER_HAVE_TRACK_PEAK |
                     DECODER_HAVE_ALBUM_GAIN |
                     DECODER_HAVE_ALBUM_PEAK |
                     DECODER_HAVE_PICS       |
                     DECODER_PICS_ANYTYPE;

    info->codepage = ch_default();
  }

  FLAC__metadata_iterator_delete( i );
  flac_close( w );
  decoder_uninit( w );
  return PLUGIN_OK;
}

/* It is called if it is necessary to change the meta information
   for the specified file. */
ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  int    rc = PLUGIN_OK;
  char   savename[_MAX_URL] = "";
  char   buffer[64];
  XFILE* file = NULL;
  XFILE* save = NULL;
  int    j;

  FLAC__Metadata_Chain*    chain = NULL;
  FLAC__Metadata_Iterator* i     = NULL;
  FLAC__StreamMetadata*    meta;
  FLAC__IOCallbacks        io_cb;
  DECODER_STRUCT*          w;

  if( !*filename ) {
    return EINVAL;
  }

  io_cb.read  = io_read_cb;
  io_cb.write = io_write_cb;
  io_cb.seek  = io_seek_cb;
  io_cb.tell  = io_tell_cb;
  io_cb.eof   = io_eof_cb;
  io_cb.close = io_close_cb;

  if(( file = xio_fopen( filename, "r+b" )) == NULL ) {
    rc = xio_errno();
    DEBUGLOG(( "flacplay: unable to open file for saving %s, rc = %d\n", filename, rc ));
    return rc;
  }

  if( flac_is_ogg( file )) {
    DEBUGLOG(( "flacplay: detect a ogg stream file: %s\n", filename ));
    return EINVAL;
  }

  for(;;) {
    if(( chain = FLAC__metadata_chain_new()) == NULL ) {
      DEBUGLOG(( "flacplay: unable to create new metadata object\n" ));
      rc = ENOMEM;
      break;
    }
    if( !FLAC__metadata_chain_read_with_callbacks( chain, file, io_cb )) {
      DEBUGLOG(( "flacplay: unable to read metadata from %s\n", filename ));
      rc = EIO;
      break;
    }

    FLAC__metadata_chain_merge_padding( chain );

    if(( i = FLAC__metadata_iterator_new()) == NULL ) {
      DEBUGLOG(( "flacplay: unable to create a new metadata iteartor\n" ));
      rc = ENOMEM;
      break;
    }

    FLAC__metadata_iterator_init( i, chain );

    while( FLAC__metadata_iterator_get_block_type(i) != FLAC__METADATA_TYPE_VORBIS_COMMENT ) {
      if( !FLAC__metadata_iterator_next(i)) {
        DEBUGLOG(( "flacplay: unable to find vorbis comment block in %s\n", filename ));
        rc = EINVAL;
        break;
      }
    }
    if( FLAC__metadata_iterator_get_block_type(i) != FLAC__METADATA_TYPE_VORBIS_COMMENT ) {
      if(( meta = FLAC__metadata_object_new( FLAC__METADATA_TYPE_VORBIS_COMMENT )) == NULL ) {
        rc = ENOMEM;
        break;
      }
      if( !FLAC__metadata_iterator_insert_block_after( i, meta )) {
        DEBUGLOG(( "flacplay: unable to insert new vorbis comment block\n" ));
        rc = EINVAL;
        break;
      }
    } else {
      DEBUGLOG(( "flacplay: found a vorbis comment block\n" ));
      meta = FLAC__metadata_iterator_get_block(i);
    }

    if( !flac_set_string( meta, info->artist,    "ARTIST"      ) ||
        !flac_set_string( meta, info->album,     "ALBUM"       ) ||
        !flac_set_string( meta, info->title,     "TITLE"       ) ||
        !flac_set_string( meta, info->genre,     "GENRE"       ) ||
        !flac_set_string( meta, info->year,      "YEAR"        ) ||
        !flac_set_string( meta, info->year,      "DATE"        ) ||
        !flac_set_string( meta, info->track,     "TRACKNUMBER" ) ||
        !flac_set_string( meta, info->disc,      "DISCNUMBER"  ) ||
        !flac_set_string( meta, info->copyright, "COPYRIGHT"   ) ||
        !flac_set_string( meta, info->comment,   "COMMENT"     ))
    {
      DEBUGLOG(( "flacplay: unable to set a vorbis comment\n" ));
      rc = ENOMEM;
      break;
    }

    if( info->album_gain ) {
      sprintf( buffer, "%.2f dB", info->album_gain );
      if( !flac_set_string( meta, buffer, "replaygain_album_gain" )) {
        DEBUGLOG(( "flacplay: unable to set a vorbis comment\n" ));
        rc = ENOMEM;
        break;
      }
    } else {
      flac_remove_string( meta, "replaygain_album_gain" );
    }

    if( info->album_peak ) {
      sprintf( buffer, "%.6f", info->album_peak );
      if( !flac_set_string( meta, buffer, "replaygain_album_peak" )) {
        DEBUGLOG(( "flacplay: unable to set a vorbis comment\n" ));
        rc = ENOMEM;
        break;
      }
    } else {
      flac_remove_string( meta, "replaygain_album_peak" );
    }

    if( info->track_gain ) {
      sprintf( buffer, "%.2f dB", info->track_gain );
      if( !flac_set_string( meta, buffer, "replaygain_track_gain" )) {
        DEBUGLOG(( "flacplay: unable to set a vorbis comment\n" ));
        rc = ENOMEM;
        break;
      }
    } else {
      flac_remove_string( meta, "replaygain_track_gain" );
    }

    if( info->track_peak ) {
      sprintf( buffer, "%.6f", info->track_peak );
      if( !flac_set_string( meta, buffer, "replaygain_track_peak" )) {
        DEBUGLOG(( "flacplay: unable to set a vorbis comment\n" ));
        rc = ENOMEM;
        break;
      }
    } else {
      flac_remove_string( meta, "replaygain_track_peak" );
    }

    if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS ))
    {
      FLAC__metadata_iterator_init( i, chain );

      do {
        if( FLAC__metadata_iterator_get_block_type(i) == FLAC__METADATA_TYPE_PICTURE ) {
          FLAC__metadata_iterator_delete_block( i, false );
          DEBUGLOG(( "flacplay: delete picture matadata block %d\n", i ));
        }
      } while( FLAC__metadata_iterator_next(i));

      FLAC__metadata_iterator_init( i, chain );

      while( FLAC__metadata_iterator_get_block_type(i) != FLAC__METADATA_TYPE_VORBIS_COMMENT ) {
        if( !FLAC__metadata_iterator_next(i)) {
          DEBUGLOG(( "flacplay: unable to find vorbis comment block in %s\n", filename ));
          rc = EINVAL;
          break;
        }
      }
      for( j = 0; j < info->pics_count; j++ )
      {
        APIC* pic = info->pics[j];
        FLAC__StreamMetadata* meta = FLAC__metadata_object_new( FLAC__METADATA_TYPE_PICTURE );
        FLAC__byte desc[sizeof(pic->desc)*4];

        if( !meta ) {
          rc = ENOMEM;
          break;
        }

        meta->data.picture.type = pic->type;

        ch_convert( CH_DEFAULT, pic->desc, CH_UTF_8, (char*)desc, sizeof( desc ));
        if( !FLAC__metadata_object_picture_set_mime_type( meta, pic->mimetype, true ) ||
            !FLAC__metadata_object_picture_set_description( meta, desc, true ) ||
            !FLAC__metadata_object_picture_set_data( meta, (FLAC__byte*)pic + sizeof( APIC ), pic->size - sizeof( APIC ), true ))
        {
          rc = ENOMEM;
          break;
        }
        if( !FLAC__metadata_iterator_insert_block_after( i, meta )) {
          DEBUGLOG(( "flacplay: unable to insert new vorbis comment block\n" ));
          rc = EINVAL;
          break;
        }
      }
    }

    if( FLAC__metadata_chain_check_if_tempfile_needed( chain, true ))
    {
      strlcpy( savename, filename, sizeof( savename ));
      savename[ strlen( savename ) - 1 ] = '~';

      DEBUGLOG(( "flacplay: needed a temporary file: %s\n", savename ));

      if(( save = xio_fopen( savename, "wb" )) == NULL ) {
        rc = xio_errno();
        DEBUGLOG(( "flacplay: unable to create the temporary file, rc = %d\n", rc ));
        break;
      }

      if( !FLAC__metadata_chain_write_with_callbacks_and_tempfile( chain, true, file, io_cb, save, io_cb )) {
        DEBUGLOG(( "flacplay: unable to write metadata to the temporary file\n" ));
        rc = EIO;
        break;
      }
    }

    // Suspend all active instances of the updated file.
    DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );

    for( j = 0; j < instances_count; j++ )
    {
      w = instances[j];
      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

      if( nlstricmp( w->filename, filename ) == 0 && w->file ) {
        w->resume_pcms = w->played_pcms;
        DEBUGLOG(( "flacplay: suspend currently used file: %s, pos = %d\n", w->filename, w->resume_pcms ));
        flac_close( w );
      } else {
        w->resume_pcms = -1;
      }
    }

    if( *savename )
    {
      xio_fclose( save );
      save = NULL;
      xio_fclose( file );
      file = NULL;

      // Preserve EAs.
      eacopy( filename, savename );

      // Replace file.
      if( remove( filename ) == 0 ) {
        if( rename( savename, filename ) != 0 ) {
          DEBUGLOG(( "flacplay: unable to rename file: %s, rc = %d\n", savename, rc ));
          rc = errno;
        }
      } else {
        rc = errno;
        DEBUGLOG(( "flacplay: unable to remove file: %s, rc = %d\n", filename, rc ));
        remove( savename );
      }
    } else {
      DEBUGLOG(( "flacplay: write metadata to file: %s\n", filename ));
      if( !FLAC__metadata_chain_write_with_callbacks( chain, true, file, io_cb )) {
        DEBUGLOG(( "flacplay: unable to write metadata to the file\n" ));
        rc = EIO;
      }
    }

    // Resume all suspended instances of the updated file.
    for( j = 0; j < instances_count; j++ )
    {
      w = instances[j];
      if( w->resume_pcms != -1 ) {
        DEBUGLOG(( "flacplay: resumes currently used file: %s, pos = %d\n", w->filename, w->resume_pcms ));
        if( flac_open( w ) == PLUGIN_OK ) {
          FLAC__stream_decoder_seek_absolute( w->flac_decoder, w->resume_pcms );
        }
      }
      DosReleaseMutexSem( w->mutex );
    }

    DosReleaseMutexSem( mutex );
    break;
  }

  if( file  ) {
    xio_fclose( file );
  }
  if( save  ) {
    xio_fclose( save );
  }
  if( chain ) {
    FLAC__metadata_chain_delete( chain );
  }
  if( i ) {
    FLAC__metadata_iterator_delete( i );
  }

  return rc;
}

/* Returns information about specified track. */
ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

/* Returns information about a disc inserted to the specified drive. */
ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

/* What can be played via the decoder. */
ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 7 ) {
      strcpy( ext[0], "*.FLAC" );
      strcpy( ext[1], "*.OGG" );
      strcpy( ext[2], "*.OGA" );
      strcpy( ext[3], "audio/flac" );
      strcpy( ext[4], "audio/x-flac" );
      strcpy( ext[5], "audio/ogg" );
      strcpy( ext[6], "application/ogg" );
    }
    *size = 7;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_STREAM | DECODER_SAVEINFO;
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "FLAC Play " VER_STRING;
  param->configurable = FALSE;
}

int INIT_ATTRIBUTE
__dll_initialize( void )
{
  if( DosCreateMutexSem( NULL, &mutex, 0, FALSE ) != NO_ERROR ) {
    return 0;
  }

  return 1;
}

int TERM_ATTRIBUTE
__dll_terminate( void )
{
  free( instances );
  DosCloseMutexSem( mutex );
  return 1;
}

#if defined(__IBMC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag       )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    return __dll_initialize();
  } else if( flag == DLL_PROCESS_DETACH ) {
    __dll_terminate();
    #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
    #endif
    _CRT_term();
  }
  return 1UL;
}
#endif

