/*
 * Copyright 2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef PM123_DFF_H
#define PM123_DFF_H

#pragma pack(1)

typedef struct _DFFChunk
{
  uint32_t id;    /* Chunk id.            */
  uint64_t size;  /* Size of this chunk.  */

} DFFChunk;

typedef struct _DSTFrameIndex
{
  uint64_t offset;
  uint32_t length;

} DSTFrameIndex;

#pragma pack()

/* DSD chunk id numbers. */
#define DSD_CHUNK_ID( a,b,c,d ) ( a | ( b << 8 ) | ( c << 16 ) | ( d << 24 ))

#define ID_FRM8 DSD_CHUNK_ID( 'F','R','M','8' )
#define ID_DSD  DSD_CHUNK_ID( 'D','S','D',' ' )
#define ID_PROP DSD_CHUNK_ID( 'P','R','O','P' )
#define ID_SND  DSD_CHUNK_ID( 'S','N','D',' ' )
#define ID_FS   DSD_CHUNK_ID( 'F','S',' ',' ' )
#define ID_CHNL DSD_CHUNK_ID( 'C','H','N','L' )
#define ID_CMPR DSD_CHUNK_ID( 'C','M','P','R' )
#define ID_DST  DSD_CHUNK_ID( 'D','S','T',' ' )
#define ID_FRTE DSD_CHUNK_ID( 'F','R','T','E' )
#define ID_ID3  DSD_CHUNK_ID( 'I','D','3',' ' )
#define ID_DSTI DSD_CHUNK_ID( 'D','S','T','I' )
#define ID_DSTF DSD_CHUNK_ID( 'D','S','T','F' )
#define ID_DSTC DSD_CHUNK_ID( 'D','S','T','C' )

#define hton16(x)            \
    ((((x) & 0xff00) >> 8) | \
     (((x) & 0x00ff) << 8))
#define hton32(x)                 \
    ((((x) & 0xff000000) >> 24) | \
     (((x) & 0x00ff0000) >>  8) | \
     (((x) & 0x0000ff00) <<  8) | \
     (((x) & 0x000000ff) << 24))
#define hton64(x)                            \
    ((((x) & 0xff00000000000000ULL) >> 56) | \
     (((x) & 0x00ff000000000000ULL) >> 40) | \
     (((x) & 0x0000ff0000000000ULL) >> 24) | \
     (((x) & 0x000000ff00000000ULL) >>  8) | \
     (((x) & 0x00000000ff000000ULL) <<  8) | \
     (((x) & 0x0000000000ff0000ULL) << 24) | \
     (((x) & 0x000000000000ff00ULL) << 40) | \
     (((x) & 0x00000000000000ffULL) << 56))

#define padded(x) ((x)+(x)%2)

#define DSD_ZERO 0x69

#endif /* PM123_DFF_H */

