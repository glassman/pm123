/*
 * Copyright 2021 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_ERRORS
#define  INCL_PM
#include <os2.h>
#include <process.h>
#include <errno.h>

#include "dffplay.h"
#include "dff.h"
#include "libdstdec/decoder.h"

#include <utilfct.h>
#include <output_plug.h>
#include <decoder_plug.h>
#include <debuglog.h>

static  DECODER_SETTINGS cfg;

#define WM_UPDATE_CONTROLS ( WM_USER + 1 )

// A DSD stream contain of 1-bit samples, but the stream that we have
// to output must contain a grouped 32-bit samples. Here we declare
// the types and constants needed for this conversion.
#define out_t     int32_t
#define out_size  sizeof(out_t)
#define out_bits  (out_size*8)

/* Closes the DSDIFF file. */
static int
dsd_close( DECODER_STRUCT* w )
{
  int rc = 0;

  if( w->file ) {
    rc = xio_fclose( w->file );
    w->file = NULL;
  }
  if( w->tagv2 ) {
    id3v2_free_tag( w->tagv2 );
    w->tagv2 = NULL;
  }

  return rc;
}

/* Read DSDDIFF file chunk header. */
static BOOL
dsd_read_chunk_header( DECODER_STRUCT* w, DFFChunk* chunk )
{
  if( xio_fread( chunk, 1, sizeof( *chunk ), w->file ) != sizeof( *chunk )) {
    return FALSE;
  }

  chunk->size = hton64( chunk->size );
  DEBUGLOG(( "dffplay: read chunk id=%c%c%c%c, size=%llu, end=%llu\n",
              chunk->id, chunk->id >> 8, chunk->id >> 16, chunk->id >> 24,
              chunk->size, xio_ftell64( w->file ) + chunk->size ));

  return TRUE;
}

/* Read DSDIFF file chunk identifier. */
static BOOL
dsd_read_chunk_id( DECODER_STRUCT* w, uint32_t* id )
{
  if( xio_fread( id, 1, 4, w->file ) != 4 ) {
    return FALSE;
  }

  DEBUGLOG(( "dffplay: read chunk id=%c%c%c%c\n",
              *id, *id >> 8, *id >> 16, *id >> 24 ));
  return TRUE;
}

/* Read 32-bit unsigned integer from DSDIFF file. */
static BOOL
dsd_read_uint32( DECODER_STRUCT* w, uint32_t* x ) {
  if( xio_fread( x, 1, 4, w->file ) == 4 ) {
    *x = hton32(*x);
    return TRUE;
  }
  return FALSE;
}

/* Read 16-bit unsigned integer from DSDIFF file. */
static BOOL
dsd_read_uint16( DECODER_STRUCT* w, uint16_t* x ) {
  if( xio_fread( x, 1, 2, w->file ) == 2 ) {
    *x = hton16(*x);
    return TRUE;
  }
  return FALSE;
}

/* Read frame from DSDIFF file. */
static int
dsd_read_frame( DECODER_STRUCT* w, uint8_t* data, int size )
{
  int done   = -1;
  int resync =  0;

  if( !w->dst_encoded ) {
    xio_fpos64_t pos = xio_ftell64( w->file );
    if( pos + size > (xio_fpos64_t)( w->dsd_start + w->dsd_size )) {
      memset( data, DSD_ZERO, size );
      size = w->dsd_start + w->dsd_size - pos;
      DEBUGLOG(( "dffplay: truncate frame size to %d\n", size ));
    }
    done = xio_fread( data, 1, size, w->file );
  } else {
    DFFChunk chunk;
    if( xio_fread( &chunk, 1, sizeof( chunk ), w->file ) == sizeof( chunk )) {
      for(;;) {
        if( chunk.id == ID_DSTF ) {
          uint64_t read = hton64( chunk.size );
          if( read <= (uint64_t)size ) {
            if(( done = xio_fread( data, 1, read, w->file )) == (int)read ) {
              if( read & 1 ) {
                xio_fseek64( w->file, 1, XIO_SEEK_CUR );
              }
              #if DEBUG
              if( resync ) {
                DEBUGLOG(( "dffplay: found DSTF chunk after %d attempts.\n", resync ));
              }
              #endif
              return done;
            } else {
              DEBUGLOG(( "dffplay: unable to read DSTF chunk data.\n" ));
              break;
            }
          }
        } else if( chunk.id == ID_DSTC && hton64( chunk.size ) == 4 ) {
          xio_fseek64( w->file, 4, XIO_SEEK_CUR );
        } else {
          if( ++resync > MAXRESYNC ) {
            DEBUGLOG(( "dffplay: giving up searching valid DSTF chunk...\n" ));
            break;
          } else {
            memmove( &chunk, ((char*)&chunk) + 1, sizeof( chunk ) - 1 );
            if( xio_fread(((char*)&chunk) + sizeof( chunk ) - 1, 1, 1, w->file ) != 1 ) {
              break;
            }
          }
        }
      }
    }
  }

  return done;
}

/* Open a DSDIFF file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static int
dsd_open_file( DECODER_STRUCT* w, const char* filename, const char* mode )
{
  int          rc = 0;
  DFFChunk     chunk;
  uint32_t     id;
  xio_fpos64_t frm8_end;
  xio_fpos64_t prop_end;

  if( w->filename != filename ) {
    strlcpy( w->filename, filename, sizeof( w->filename ));
  }

  DEBUGLOG(( "dffplay: %s\n", filename ));

  w->samplerate     = 0;
  w->channels       = 0;
  w->framerate      = 0;
  w->dsd_size       = 0;
  w->dst_encoded    = 0;
  w->dst_index      = 0;
  w->dst_index_size = 0;
  w->metadata       = 0;

  if(( w->file = xio_fopen( filename, mode )) == NULL ) {
    return xio_errno();
  }
  if( !xio_can_seek( w->file )) {
    DEBUGLOG(( "dffplay: Unable to open file that doesn't support movement of the read pointer\n" ));
    goto failed;
  }

  if( !dsd_read_chunk_header( w, &chunk ) || chunk.id != ID_FRM8 ) {
    goto failed;
  }
  if( !dsd_read_chunk_id( w, &id ) || id != ID_DSD ) {
    goto failed;
  }

  frm8_end = sizeof( chunk ) + chunk.size;
  w->dst_encoded = 0;

  while( xio_ftell64( w->file ) < frm8_end ) {
    if( !dsd_read_chunk_header( w, &chunk )) {
      goto failed;
    }

    if( chunk.id == ID_PROP ) {
      if( !dsd_read_chunk_id( w, &id ) || id != ID_SND ) {
        goto failed;
      }

      prop_end = xio_ftell64( w->file ) - sizeof( id ) + chunk.size;
      while( xio_ftell64( w->file ) < prop_end ) {
        if( !dsd_read_chunk_header( w, &chunk )) {
          goto failed;
        }
        if( chunk.id == ID_FS ) {
          if( !dsd_read_uint32( w, &w->samplerate )) {
            goto failed;
          }
        } else if( chunk.id == ID_CHNL ) {
          if( !dsd_read_uint16( w, &w->channels )) {
            goto failed;
          }
          xio_fseek64( w->file, padded( chunk.size ) -
                                sizeof( w->channels ), XIO_SEEK_CUR );
        } else if( chunk.id == ID_CMPR ) {
          if( !dsd_read_chunk_id( w, &id )) {
            goto failed;
          }
          if( id == ID_DST ) {
            w->dst_encoded = TRUE;
          }
          xio_fseek64( w->file, padded( chunk.size ) -
                                sizeof( id ), XIO_SEEK_CUR );
        } else {
          if( xio_fseek64( w->file, padded( chunk.size ), XIO_SEEK_CUR ) != 0 ) {
            goto failed;
          }
        }
      }
    } else if( chunk.id == ID_DSD ) {
      if( !w->samplerate || !w->channels ) {
        // ID_PROP chunk must be preceded ID_DSD.
        goto failed;
      }
      w->dsd_start  = xio_ftell64( w->file );
      w->dsd_size   = chunk.size;
      w->songlength = 1000.0 * w->dsd_size / w->channels * 8 / w->samplerate;
      w->framerate  = 75;
      xio_fseek64( w->file, padded( chunk.size ), XIO_SEEK_CUR );
    } else if( chunk.id == ID_DST ) {
      if( !w->samplerate || !w->channels ) {
        // ID_PROP chunk must be preceded ID_DST.
        goto failed;
      }
      w->dsd_start  = xio_ftell64( w->file );
      w->dsd_size   = chunk.size;
      if( !dsd_read_chunk_header( w, &chunk ) ||
           chunk.id != ID_FRTE || chunk.size != 6 )
      {
        goto failed;
      }
      if( !dsd_read_uint32( w, &w->frames    ) ||
          !dsd_read_uint16( w, &w->framerate ))
      {
        goto failed;
      }
      w->dsd_start += 18;
      w->dsd_size  -= 18;
      w->songlength = 1000.0 * w->frames / w->framerate;
      xio_fseek64( w->file, w->dsd_start + padded( w->dsd_size ), XIO_SEEK_SET );
    } else if( chunk.id == ID_DSTI ) {
      w->dst_index = xio_ftell64( w->file );
      w->dst_index_size = chunk.size / sizeof( DSTFrameIndex );
      xio_fseek64( w->file, padded( chunk.size ), XIO_SEEK_CUR );
    } else if( chunk.id == ID_ID3 ) {
      w->metadata = xio_ftell64( w->file ) - sizeof( chunk );
      w->tagv2 = id3v2_get_tag( w->file, 0 );
      xio_fseek64( w->file, w->metadata +
                   sizeof( chunk ) + padded( chunk.size ), XIO_SEEK_SET );
    } else {
      xio_fpos64_t pos = xio_ftell64( w->file ) + padded( chunk.size );
      if( xio_fseek64( w->file, pos, XIO_SEEK_SET ) != 0 ||
          pos != xio_ftell64( w->file ))
      {
        goto failed;
      }
    }
  }

  if( w->dsd_size )
  {
    w->bitrate = w->samplerate * w->channels / 1000;
    DEBUGLOG(( "dffplay: started=%lld, size=%llu, channels=%d, samplerate=%ld, DST=%d, frames=%ld, framerate=%d\n",
                w->dsd_start, w->dsd_size, w->channels, w->samplerate, w->dst_encoded, w->frames, w->framerate ));
    return rc;
  }

failed:

  if( xio_ferror( w->file )) {
    rc = xio_errno();
  } else {
    rc = -1;
  }

  dsd_close( w );
  return rc;
}

/* Return offset of the specified DST-encoded frame. */
static xio_fpos64_t
dsd_frame_offset( DECODER_STRUCT* w, uint32_t i )
{
  DSTFrameIndex frame_index;

  if( xio_fseek64( w->file, w->dst_index + i * sizeof( DSTFrameIndex ), XIO_SEEK_SET ) == 0 &&
      xio_fread( &frame_index, 1, sizeof( frame_index ), w->file ) == sizeof( frame_index ))
  {
    DEBUGLOG(( "dffplay: frame %u offset=%lld, length=%d\n",
                i, hton64( frame_index.offset ), hton32( frame_index.length )));
    return hton64( frame_index.offset ) - sizeof( DFFChunk );
  } else {
    DEBUGLOG(( "dffplay: unable find index record for frame %u\n", i ));
    return -1;
  }
}

/* Returns the offset in the file for the specified time. */
static xio_fpos64_t
dsd_offset_for_time( DECODER_STRUCT* w, uint32_t ms )
{
  xio_fpos64_t offset;
  float seconds = (float)ms / 1000;

  if( w->dst_encoded ) {
    if( w->dst_index_size )
    {
      uint32_t i = seconds * w->framerate;

      if( i < w->frames && i < w->dst_index_size ) {
        offset = dsd_frame_offset( w, i ) - w->dsd_start;
      } else {
        offset = w->dsd_size;
      }
    } else {
      offset = seconds * w->framerate / w->frames * w->dsd_size;
    }
  } else {
    offset = seconds * w->samplerate / 8 * w->channels;
  }

  return offset;
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  int   rc;
  ULONG resetcount;
  char  errorbuf[1024];

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  FORMAT_INFO output_format;
  decoder_t* decoder = NULL;

  out_t*   buffer    = NULL;
  int      bufsize   = w->audio_buffersize / out_size;
  int      bufpos    = 0;
  int      markerpos = 0;
  float    playedpos = 0;
  uint8_t* frame     = NULL;
  uint8_t* decoded   = NULL;
  int      framesize;
  float    framelen;

  w->frew = FALSE;
  w->ffwd = FALSE;

  if(( rc = dsd_open_file( w, w->filename, "rb" )) != 0 )
  {
    strlcpy( errorbuf, "Unable open file:\n", sizeof( errorbuf ));
    strlcat( errorbuf, w->filename, sizeof( errorbuf ));
    strlcat( errorbuf, "\n", sizeof( errorbuf ));

    if( rc != -1 ) {
      strlcat( errorbuf, xio_strerror( xio_errno()), sizeof( errorbuf ));
    } else {
      strlcat( errorbuf, "Unsupported format of the file.", sizeof( errorbuf ));
    }

    w->error_display( errorbuf );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  framesize = w->samplerate / 8 / w->framerate * w->channels;
  framelen  = 1000.0 / w->framerate;

  if(( buffer = (out_t*)malloc( w->audio_buffersize )) == NULL ||
     ( frame = (uint8_t*)malloc( framesize )) == NULL )
  {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  if( w->dst_encoded ) {
    if(( decoded = (uint8_t*)malloc( framesize )) == NULL ) {
      w->status = DECODER_ERROR;
      WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
      goto end;
    }
    decoder = new decoder_t();
    decoder->init( w->channels, w->samplerate / 8 / w->framerate );
  }

  output_format.size       = sizeof( output_format );
  output_format.format     = WAVE_FORMAT_DSD;
  output_format.channels   = w->channels;
  output_format.samplerate = w->samplerate / out_bits;
  output_format.bits       = out_bits;

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop )
    {
      int done;

      if( w->jumptopos >= 0 )
      {
        xio_fpos64_t pos = w->dsd_start + dsd_offset_for_time( w, w->startpos + w->jumptopos );
        DEBUGLOG(( "dffplay: jump to %d ms, %lld bytes\n", w->startpos + w->jumptopos, pos ));
        if( xio_fseek64( w->file, pos, XIO_SEEK_SET ) != 0 ||
            xio_ftell64( w->file ) != pos )
        {
          break;
        }

        markerpos = playedpos = w->jumptopos;
        w->jumptopos = -1;
        bufpos = 0;

        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );
      }

      if(( w->endpos && w->startpos + playedpos >= w->endpos ) ||
         ( xio_ftell64( w->file ) >= (xio_fpos64_t)( w->dsd_start + w->dsd_size )))
      {
        break;
      }

      if(( done = dsd_read_frame( w, frame, framesize )) > 0 )
      {
        unsigned int i, ch;
        int bitrate;
        uint8_t* p;
        uint8_t* pend;

        if( w->dst_encoded ) {
          decoder->decode( frame, done * 8, decoded );
          p = decoded;
          pend = decoded + framesize;
        } else {
          p = frame;
          pend = frame + done;
        }

        if(( bitrate = done * 8 / framelen ) != w->bitrate ) {
          w->bitrate = bitrate;
          WinPostMsg( w->hwnd, WM_CHANGEBR, MPFROMLONG( w->bitrate ), 0 );
        }

        playedpos += framelen;

        for( ; p < pend; p += out_size * w->channels ) {
          for( ch = 0; ch < w->channels; ch++ )
          {
            out_t z = 0;

            for( i = 0; i < out_size ; i++ ) {
              z |= ( p[i*w->channels+ch] << ( i * 8 ));
            }
            buffer[bufpos++] = z;

            if( bufpos == bufsize ) {
              w->output_play_samples( w->a, &output_format, (char*)buffer, bufpos * out_size, markerpos );
              markerpos = playedpos;
              bufpos = 0;
            }
          }
        }
      } else {
        if( xio_ferror( w->file )) {
          strlcpy( errorbuf, "Unable read file:\n", sizeof( errorbuf ));
          strlcat( errorbuf, w->filename, sizeof( errorbuf ));

          if( xio_errno()) {
            strlcat( errorbuf, "\n", sizeof( errorbuf ));
            strlcat( errorbuf, xio_strerror( xio_errno()), sizeof( errorbuf ));
          }

          w->error_display( errorbuf );
          w->status = DECODER_ERROR;
          WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
          goto end;
        } else {
          break;
        }
      }
    }

    if( bufpos ) {
      w->output_play_samples( w->a, &output_format, (char*)buffer, bufpos * out_size, markerpos );
    }

    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

  w->status = DECODER_STOPPED;

end:

  free( buffer );
  free( frame );
  free( decoded );
  delete decoder;
  dsd_close( w );

  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than yours is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  decoder_command( w, DECODER_STOP, NULL );
  DosCloseEventSem( w->play );
  free( w );

  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  switch( msg )
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      strlcpy( w->filename, info->filename, sizeof( w->filename ));

      w->jumptopos  = info->jumpto;
      w->startpos   = info->start;
      w->endpos     = info->end;
      w->status     = DECODER_STARTING;
      w->stop       = FALSE;
      w->songlength = 0;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;
      xio_fabort( w->file );
      DosPostEventSem( w->play );

      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      DosPostEventSem( w->play );
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }

  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  if( w->endpos ) {
    return w->endpos - w->startpos;
  } else {
    return w->songlength;
  }
}

void static
copy_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* result, int size )
{
  ID3V2_FRAME* frame = NULL;

  if( !*result ) {
    if(( frame = id3v2_get_frame( tag, id, 1 )) != NULL ) {
      id3v2_get_string( frame, result, size );
    }
  }
}

void static
copy_id3v2_tag( DECODER_INFO* info, ID3V2_TAG* tag )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( tag ) {
    copy_id3v2_string( tag, ID3V2_TIT2, info->title,  sizeof( info->title  ));
    copy_id3v2_string( tag, ID3V2_TPE1, info->artist, sizeof( info->artist ));
    copy_id3v2_string( tag, ID3V2_TALB, info->album,  sizeof( info->album  ));
    copy_id3v2_string( tag, ID3V2_TCON, info->genre,  sizeof( info->genre  ));
    copy_id3v2_string( tag, ID3V2_TDRC, info->year,   sizeof( info->year   ));

    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        id3v2_get_string( frame, info->comment, sizeof( info->comment ) );
        break;
      }
    }

    // Disable Open Watcom C++ "&array may not produce intended result"
    #ifdef __WATCOMC__
    #pragma disable_message(7)
    #endif

    if( HAVE_FIELD( info, track ))
    {
      copy_id3v2_string( tag, ID3V2_TCOP, info->copyright, sizeof( info->copyright ));
      copy_id3v2_string( tag, ID3V2_TRCK, info->track, sizeof( info->track ));
      copy_id3v2_string( tag, ID3V2_TPOS, info->disc, sizeof( info->disc ));

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_RVA2, i )) != NULL ; i++ )
      {
        float gain = 0;
        unsigned char* data = (unsigned char*)frame->fr_data;

        // Format of RVA2 frame:
        //
        // Identification          <text string> $00
        // Type of channel         $xx
        // Volume adjustment       $xx xx
        // Bits representing peak  $xx
        // Peak volume             $xx (xx ...)

        id3v2_get_description( frame, buffer, sizeof( buffer ));

        // Skip identification string.
        data += strlen((char*)data ) + 1;
        // Search the master volume.
        while( data - (unsigned char*)frame->fr_data < frame->fr_size ) {
          if( *data == 0x01 ) {
            gain = (float)((signed char)data[1] << 8 | data[2] ) / 512;
            break;
          } else {
            data += 3 + (( data[3] + 7 ) / 8 );
          }
        }
        if( gain != 0 ) {
          if( stricmp( buffer, "album" ) == 0 ) {
            info->album_gain = gain;
          } else {
            info->track_gain = gain;
          }
        }
      }

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_TXXX, i )) != NULL ; i++ )
      {
        id3v2_get_description( frame, buffer, sizeof( buffer ));

        if( stricmp( buffer, "replaygain_album_gain" ) == 0 ) {
          info->album_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_album_peak" ) == 0 ) {
          info->album_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_gain" ) == 0 ) {
          info->track_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_peak" ) == 0 ) {
          info->track_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        }
      }
    }
  }
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  LONG rc = PLUGIN_OK;
  DECODER_STRUCT* w;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  if(( rc = dsd_open_file( w, filename, "rb" )) != 0 ) {
    decoder_uninit( w );
    return rc == -1 ? PLUGIN_NO_PLAY : PLUGIN_NO_READ;
  }

  info->format.size       = sizeof( info->format );
  info->format.format     = WAVE_FORMAT_DSD;
  info->format.bits       = 32;
  info->format.channels   = w->channels;
  info->format.samplerate = w->samplerate / 32;
  info->mode              = w->channels == 1 ? 3 : 0;
  info->bitrate           = w->samplerate * w->channels / 1000;
  info->songlength        = w->songlength;

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = xio_fsize64( w->file );
  }

  snprintf( info->tech_info, sizeof( info->tech_info ), "%u kbs, %.1f kHz, DSD%d, %s",
            info->bitrate, ( w->samplerate / 1000.0 ), w->samplerate / 44100,
            w->channels == 1 ? "Mono" : "Stereo" );

  if( w->tagv2 ) {
    copy_id3v2_tag( info, w->tagv2 );
  }

  if( HAVE_FIELD( info, pics_count ))
  {
    ID3V2_FRAME* frame;
    int i, j, count = 0;

    if( w->tagv2 )
    {
      for( i = 1; ( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, i )) != NULL ; i++ ) {
        ++count;
      }

      if( count  && ( options & DECODER_LOADPICS )) {
        if(( info->pics = (APIC**)info->memalloc( sizeof( APIC* ) * count )) != NULL ) {
          for( i = 1, j = 0; ( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, i )) != NULL ; i++ )
          {
            int   size = sizeof( APIC ) + id3v2_picture_size( frame );
            APIC* pic;

            if(( id3v2_picture_size( frame ) > frame->fr_size  ) ||
               ( info->pics[j] = (APIC*)info->memalloc( size )) == NULL )
            {
              count = j;
              break;
            }

            pic = info->pics[j];

            pic->size = size;
            pic->type = id3v2_picture_type( frame );

            id3v2_get_mime_type( frame, pic->mimetype, sizeof( pic->mimetype ));
            id3v2_get_description( frame, pic->desc, sizeof( pic->desc ));

            DEBUGLOG(( "dffplay: load APIC type %d, %s, '%s', size %d\n",
                        pic->type, pic->mimetype, pic->desc, id3v2_picture_size( frame )));

            memcpy((char*)pic + sizeof( APIC ),
                   id3v2_picture_data_ptr( frame ), id3v2_picture_size( frame ));
            ++j;
          }
        }
      }
    }

    info->pics_count = count;
    DEBUGLOG(( "dffplay: found %d attached pictures\n", info->pics_count ));
  }

  if( HAVE_FIELD( info, saveinfo ))
  {
    info->saveinfo = is_file( filename );
    info->codepage = ch_default();
    info->haveinfo = DECODER_HAVE_TITLE      |
                     DECODER_HAVE_ARTIST     |
                     DECODER_HAVE_ALBUM      |
                     DECODER_HAVE_YEAR       |
                     DECODER_HAVE_GENRE      |
                     DECODER_HAVE_TRACK      |
                     DECODER_HAVE_DISC       |
                     DECODER_HAVE_COMMENT    |
                     DECODER_HAVE_COPYRIGHT  |
                     DECODER_HAVE_TRACK_GAIN |
                     DECODER_HAVE_TRACK_PEAK |
                     DECODER_HAVE_ALBUM_GAIN |
                     DECODER_HAVE_ALBUM_PEAK |
                     DECODER_HAVE_PICS       |
                     DECODER_PICS_ANYTYPE;
  }

  dsd_close( w );
  decoder_uninit( w );
  return rc;
}

static void
replace_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* string )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( id == ID3V2_COMM ) {
    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        break;
      }
    }
  } else {
    frame = id3v2_get_frame( tag, id, 1 );
  }

  if( frame == NULL ) {
    frame = id3v2_add_frame( tag, id );
  }
  if( frame != NULL ) {
    id3v2_set_string( frame, string );
  }
}

/* It is called if it is necessary to change the meta information
   for the specified file. */
ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  ULONG    rc = PLUGIN_OK;
  int      i;
  DFFChunk chunk = { ID_ID3 };

  DECODER_STRUCT* w;

  if( !*filename ) {
    return EINVAL;
  }

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return errno;
  }

  if(( rc = dsd_open_file( w, filename, "r+b" )) != 0 ) {
    DEBUGLOG(( "dsfplay: unable open file for saving %s, rc=%d\n", filename, rc ));
    decoder_uninit( w );
    return rc;
  }

  if( !w->tagv2 ) {
    w->tagv2 = id3v2_new_tag();
  }

  // FIX ME: Replay gain info also must be saved.

  replace_id3v2_string( w->tagv2, ID3V2_TIT2, info->title     );
  replace_id3v2_string( w->tagv2, ID3V2_TPE1, info->artist    );
  replace_id3v2_string( w->tagv2, ID3V2_TALB, info->album     );
  replace_id3v2_string( w->tagv2, ID3V2_TDRC, info->year      );
  replace_id3v2_string( w->tagv2, ID3V2_COMM, info->comment   );
  replace_id3v2_string( w->tagv2, ID3V2_TCON, info->genre     );
  replace_id3v2_string( w->tagv2, ID3V2_TCOP, info->copyright );
  replace_id3v2_string( w->tagv2, ID3V2_TRCK, info->track     );
  replace_id3v2_string( w->tagv2, ID3V2_TPOS, info->disc      );

  if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS ))
  {
    ID3V2_FRAME* frame;

    while(( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, 1 )) != NULL ) {
      id3v2_delete_frame( frame );
    }
    for( i = 0; i < info->pics_count; i++ ) {
      if(( frame = id3v2_add_frame( w->tagv2, ID3V2_APIC )) != NULL )
      {
        APIC* pic = info->pics[i];

        id3v2_set_picture( frame, pic->type, pic->mimetype, pic->desc,
                           (char*)pic + sizeof(APIC), pic->size - sizeof(APIC));
      }
    }
  }

  if( w->metadata ) {
    xio_fseek64( w->file, w->metadata, XIO_SEEK_SET );
  } else {
    xio_fseek64( w->file, 0, XIO_SEEK_END );
    w->metadata = xio_ftell64( w->file );
  }

  if( xio_fwrite( &chunk, 1, sizeof( chunk ), w->file ) != sizeof( chunk )) {
    rc = xio_errno();
  } else {
    switch( id3v2_set_tag( w->file, w->tagv2, NULL )) {
      case ID3V2_ERROR:
        rc = xio_errno();
        break;
      case ID3V2_BADPOS:
        rc = EINVAL;
        break;
      case ID3V2_OK:
        xio_fseek64( w->file, w->metadata, XIO_SEEK_SET );
        chunk.size = hton64( w->tagv2->id3_totalsize );
        xio_fwrite( &chunk, 1, sizeof( chunk ), w->file );
        chunk.id = ID_FRM8;
        chunk.size = hton64( xio_fsize64( w->file ) - sizeof( chunk ));
        xio_rewind( w->file );
        xio_fwrite( &chunk, 1, sizeof( chunk ), w->file );
        break;
    }
  }

  dsd_close( w );
  decoder_uninit( w );
  return rc;
}

/* Unsupported. */
ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

/* Unsupported. */
ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

/* What can be played via the decoder. */
ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 2 ) {
      strcpy( ext[0], "*.DFF" );
      strcpy( ext[1], "audio/x-dff" );
    }
    *size = 2;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_SAVEINFO;
}

static void
init_tag( void )
{
  id3v2_set_read_charset( cfg.tag_read_id3v2_charset );
  id3v2_set_save_charset( cfg.tag_save_id3v2_charset );
}

/* Saves plug-in's settings. */
static void
save_ini( void )
{
  HINI hini;
  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, cfg.tag_read_id3v2_charset );
    save_ini_value( hini, cfg.tag_save_id3v2_charset );
    close_ini( hini );
  }
}

/* Loads plug-in's settings. */
static void
load_ini( void )
{
  HINI hini;

  if( ch_default() == CH_CYR_OS2 ) {
    cfg.tag_read_id3v2_charset = CH_CYR_WIN;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
  } else {
    cfg.tag_read_id3v2_charset = CH_DEFAULT;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
  }

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, cfg.tag_read_id3v2_charset );
    load_ini_value( hini, cfg.tag_save_id3v2_charset );
    close_ini( hini );
  }

  init_tag();
}

/* Processes messages of the configuration dialog. */
static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
    {
      int i;

      for( i = 0; i < ch_list_size; i++ ) {
        lb_add_with_handle( hwnd, CB_ID3V2_RDCH, ch_list[i].name, ch_list[i].id );
      }

      for( i = 0; i < ch_list_size; i++ ) {
        // All except for auto detection and UCS-2 LE.
        if( ch_list[i].id >= 0 && ch_list[i].id != CH_UCS_2LE ) {
          lb_add_with_handle( hwnd, CB_ID3V2_WRCH, ch_list[i].name, ch_list[i].id );
        }
      }

      do_warpsans( hwnd );
      // continue to WM_UPDATE_CONTROLS...
    }

    case WM_UPDATE_CONTROLS:
    {
      lb_select_by_handle( hwnd, CB_ID3V2_RDCH, cfg.tag_read_id3v2_charset );
      lb_select_by_handle( hwnd, CB_ID3V2_WRCH, cfg.tag_save_id3v2_charset );
      return 0;
    }

    case WM_DESTROY:
    {
      int  i;

      if(( i = lb_cursored( hwnd, CB_ID3V2_RDCH )) != LIT_NONE ) {
        cfg.tag_read_id3v2_charset = (int)lb_get_handle( hwnd, CB_ID3V2_RDCH, i );
      }
      if(( i = lb_cursored( hwnd, CB_ID3V2_WRCH )) != LIT_NONE ) {
        cfg.tag_save_id3v2_charset = (int)lb_get_handle( hwnd, CB_ID3V2_WRCH, i );
      }

      save_ini();
      init_tag();
      break;
    }

    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case PB_UNDO:
          WinSendMsg( hwnd, WM_UPDATE_CONTROLS, 0, 0 );
          return 0;

        case PB_DEFAULT:
        {
          if( ch_default() == CH_CYR_OS2 ) {
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_CYR_WIN  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          } else {
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_DEFAULT  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          }

          return 0;
        }
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in. */
void DLLENTRY
plugin_configure( HWND hwnd, HMODULE module ) {
  WinDlgBox( HWND_DESKTOP, hwnd, cfg_dlg_proc, module, DLG_CONFIGURE, NULL );
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "DSDIFF Decoder " VER_STRING;
  param->configurable = TRUE;

  load_ini();
}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    __ctordtorInit();
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    __ctordtorTerm();
    _CRT_term();
  }
  return 1UL;
}
#endif
