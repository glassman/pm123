/*
 * Copyright 2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_DFFPLAY_H
#define PM123_DFFPLAY_H

#ifndef  RC_INVOKED
#include <plugin.h>
#include <xio.h>
#include <id3v2.h>
#endif

#define DLG_CONFIGURE     1
#define GB_TAGS         900
#define ST_COPYRIGHT    901
#define ST_ID3V2_RDCH   902
#define CB_ID3V2_RDCH   903
#define ST_ID3V2_WRCH   904
#define CB_ID3V2_WRCH   905
#define PB_DEFAULT      906
#define PB_UNDO         907

#define MAXRESYNC     98303

typedef struct _DECODER_STRUCT
{
  char         filename[_MAX_URL];
  XFILE*       file;
  ID3V2_TAG*   tagv2;

  uint32_t     samplerate;
  uint16_t     channels;
  uint16_t     framerate;
  uint32_t     frames;          // Number of DST frames (the number of chunks) in the file.
  BOOL         dst_encoded;
  xio_fpos64_t dst_index;
  uint64_t     dst_index_size;
  xio_fpos64_t dsd_start;
  uint64_t     dsd_size;
  xio_fpos64_t metadata;        // Pointer to ID3 metadata chunk.
  int          bitrate;

  HEV   play;                   // For internal use to sync the decoder thread.
  int   decodertid;             // Decoder thread indentifier.
  ULONG songlength;
  int   jumptopos;
  int   startpos;
  int   endpos;
  BOOL  stop;
  BOOL  frew;
  BOOL  ffwd;
  ULONG status;

  void (DLLENTRYP error_display)( char* );
  void (DLLENTRYP info_display )( char* );
  int  (DLLENTRYP output_play_samples)( void* a, FORMAT_INFO* format, char* buf, int len, int posmarker );

  HWND  hwnd;                   // PM interface main frame window handle.
  void* a;                      // Only to be used with the precedent function.
  int   audio_buffersize;

} DECODER_STRUCT;

// used internally to manage DFFPLAY and saved in dffplay.ini file.
typedef struct _DECODER_SETTINGS
{
  int tag_read_id3v2_charset;
  int tag_save_id3v2_charset;

} DECODER_SETTINGS;

#endif /* PM123_DFFPLAY_H */

