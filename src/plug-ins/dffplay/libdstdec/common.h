/*
* Direct Stream Transfer (DST) codec
* ISO/IEC 14496-3 Part 3 Subpart 10: Technical description of lossless coding of oversampled audio
*/

#ifndef COMMON_H
#define COMMON_H

#include <inttypes.h>
#include <debuglog.h>

#define GET_BIT(base, index) ((((unsigned char*)base)[index >> 3] >> (7 - (index & 7))) & 1)
#define GET_NIBBLE(base, index) ((((unsigned char*)base)[index >> 1] >> ((index & 1) << 2)) & 0x0f)

#endif
