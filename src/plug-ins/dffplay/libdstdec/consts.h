/*
* Direct Stream Transfer (DST) codec
* ISO/IEC 14496-3 Part 3 Subpart 10: Technical description of lossless coding of oversampled audio
*/

#ifndef CONSTS_H
#define CONSTS_H

#include <config.h>

#define MAXDSTCHANNELS 6

// Prediction
// Number of bits in the stream for representing the CodedPredOrder in each frame
#define SIZE_CODEDPREDORDER 7
// Maximum prediction filter order
#define MAXPREDORDER (1 << SIZE_CODEDPREDORDER)

// Probability tables
// Number bits for p-table length
#define SIZE_CODEDPTABLELEN 6
// Maximum length of p-tables
#define MAXPTABLELEN (1 << SIZE_CODEDPTABLELEN)
// Number of bits in the stream for representing each filter coefficient in each frame
#define SIZE_PREDCOEF 9

// Arithmetic coding
// Number of bits and maximum level for coding the probability
#define AC_BITS    8
#define AC_PROBS  (1 << AC_BITS)
// Number of entries in the histogram
#define AC_HISBITS 6
#define AC_HISMAX (1 << AC_HISBITS)
// Quantization step for histogram
#define AC_QSTEP  (SIZE_PREDCOEF - AC_HISBITS)

// Rice coding of filter coefficients and probability tables
// Number of different Pred. Methods for filters  used in combination with Rice coding
#define NROFFRICEMETHODS 3
// Number of different Pred. Methods for Ptables  used in combination with Rice coding
#define NROFPRICEMETHODS 3
// max pred.order for prediction of filter coefs / Ptables entries
#define MAXCPREDORDER    3
// nr of bits in stream for indicating method
#define SIZE_RICEMETHOD  2
// nr of bits in stream for indicating m
#define SIZE_RICEM       3
// Max. value of m for filters
#define MAX_RICE_M_F     6
// Max. value of m for Ptables
#define MAX_RICE_M_P     4

// Segmentation
// max nr of segments per channel for filters
#define MAXNROF_FSEGS 4
// max nr of segments per channel for Ptables
#define MAXNROF_PSEGS 8
// min segment length in bits of filters
#define MIN_FSEG_LEN  1024
// min segment length in bits of Ptables
#define MIN_PSEG_LEN  32
// max nr of segments per channel for filters or Ptables
#define MAXNROF_SEGS  8

#endif
