/*
* Direct Stream Transfer (DST) codec
* ISO/IEC 14496-3 Part 3 Subpart 10: Technical description of lossless coding of oversampled audio
*/

#ifndef FH_H
#define FH_H

#include <inttypes.h>
#include "consts.h"
#include "segment.h"

#define MAXFIR (MAXDSTCHANNELS * 2)
#define MAXTAB (MAXDSTCHANNELS * 2)

class fh_t {
public:
  int       NrOfChannels;                                       // Number of channels in the recording
  int       NrOfFilters;                                        // Number of filters used for this frame
  int       NrOfPtables;                                        // Number of Ptables used for this frame
  int       PredOrder[MAXFIR];                                  // Prediction order used for this frame
  int       PtableLen[MAXTAB];                                  // Nr of Ptable entries used for this frame
  int16_t   ICoefA[MAXFIR][MAXPREDORDER];                       // Integer coefs for actual coding
  bool      DSTCoded;                                           // true if DST coded is put in DST stream, false if DSD is put in DST stream
  int       CalcNrOfBytes;                                      // Contains number of bytes of the complete channel stream after arithmetic encoding (also containing bytestuff-, ICoefA-bits, etc.)
  int       CalcNrOfBits;                                       // Contains number of bits of the complete channel stream after arithmetic encoding (also containing bytestuff-, ICoefA-bits, etc.)
  int       HalfProb[MAXDSTCHANNELS];                           // Defines per channel which probability is applied for the first PredOrder[] bits of a frame (0 = use Ptable entry, 1 = 128)
  int       NrOfHalfBits[MAXDSTCHANNELS];                       // Defines per channel how many bits at the start of each frame are optionally coded with p=0.5
  segment_t FSegment;                                           // Contains segmentation data for filters
  uint8_t*  Filter4Bit[MAXDSTCHANNELS];                         // Filter4Bit[ChNr][BitNr]
  segment_t PSegment;                                           // Contains segmentation data for Ptables
  uint8_t*  Ptable4Bit[MAXDSTCHANNELS];                         // Ptable4Bit[ChNr][BitNr]
  bool      PSameSegAsF;                                        // true if segmentation is equal for F and P
  bool      PSameMapAsF;                                        // true if mapping is equal for F and P
  bool      FSameSegAllCh;                                      // true if all channels have same Filtersegm
  bool      FSameMapAllCh;                                      // true if all channels have same Filtermap
  bool      PSameSegAllCh;                                      // true if all channels have same Ptablesegm
  bool      PSameMapAllCh;                                      // true if all channels have same Ptablemap
  int       SegAndMapBits;                                      // Number of bits in the stream for Seg&Map
  int       MaxNrOfFilters;                                     // Max. nr. of filters allowed per frame
  int       MaxNrOfPtables;                                     // Max. nr. of Ptables allowed per frame
  int       MaxFrameLen;                                        // Max frame length of stream
  int       ByteStreamLen;                                      // MaxFrameLen * NrOfChannels
  int       BitStreamLen;                                       // ByteStreamLen * RESOL
  int       NrOfBitsPerCh;                                      // MaxFrameLen * RESOL

  fh_t() {
    memset(Filter4Bit, 0, sizeof(Filter4Bit));
    memset(Ptable4Bit, 0, sizeof(Ptable4Bit));
  }
 ~fh_t() {
    for (int i = 0; i < MAXDSTCHANNELS; i++) {
      free(Filter4Bit[i]);
      free(Ptable4Bit[i]);
    }
  }

public:
  void init(int channels, int channel_frame_size) {
    int i;
    NrOfChannels = channels;
    MaxFrameLen = channel_frame_size;
    ByteStreamLen = MaxFrameLen * NrOfChannels;
    BitStreamLen = ByteStreamLen * 8;
    NrOfBitsPerCh = MaxFrameLen * 8;
    MaxNrOfFilters = 2 * NrOfChannels;
    MaxNrOfPtables = 2 * NrOfChannels;
    memset(PredOrder, 0, sizeof(PredOrder));
    for (i = 0; i < channels; i++) { Filter4Bit[i] = (uint8_t*)realloc(Filter4Bit[i],4 * channel_frame_size); }
    for (i = 0; i < channels; i++) { Ptable4Bit[i] = (uint8_t*)realloc(Ptable4Bit[i],4 * channel_frame_size); }
  }
};

#endif

