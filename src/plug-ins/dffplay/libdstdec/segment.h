/*
* Direct Stream Transfer (DST) codec
* ISO/IEC 14496-3 Part 3 Subpart 10: Technical description of lossless coding of oversampled audio
*/

#ifndef SEGMENT_H
#define SEGMENT_H

#include <inttypes.h>
#include "consts.h"

class segment_t {
public:
  int Resolution;                                   // Resolution for segments
  int SegmentLength[MAXDSTCHANNELS][MAXNROF_SEGS];  // SegmentLength[ChNr][SegmentNr]
  int NrOfSegments[MAXDSTCHANNELS];                 // NrOfSegments[ChNr]
  int Table4Segment[MAXDSTCHANNELS][MAXNROF_SEGS];  // Table4Segment[ChNr][SegmentNr]
};

#endif
