/*
* Direct Stream Transfer (DST) codec
* ISO/IEC 14496-3 Part 3 Subpart 10: Technical description of lossless coding of oversampled audio
*/

#ifndef CT_H
#define CT_H

#include <inttypes.h>
#include "consts.h"

#define MAXFIR  (MAXDSTCHANNELS * 2)

class ct_t {
public:
  int StreamBits;                                 // Number of bits all filters use in the stream
  int CPredOrder[NROFFRICEMETHODS];               // Code_PredOrder[Method]
  int CPredCoef[NROFPRICEMETHODS][MAXCPREDORDER]; // Code_PredCoef[Method][CoefNr]
  bool Coded[MAXFIR];                             // DST encode coefs/entries of Fir/PtabNr
  int BestMethod[MAXFIR];                         // BestMethod[Fir/PtabNr]
  int m[MAXFIR][NROFFRICEMETHODS];                // m[Fir/PtabNr][Method]
};

class ft_t : public ct_t  {
public:
  ft_t() {
      CPredOrder[0] = 1;
      CPredCoef[0][0] = -8;
      CPredOrder[1] = 2;
      CPredCoef[1][0] = -16;
      CPredCoef[1][1] = 8;
      CPredOrder[2] = 3;
      CPredCoef[2][0] = -9;
      CPredCoef[2][1] = -5;
      CPredCoef[2][2] = 6;
#if NROFFRICEMETHODS == 4
      CPredOrder[3] = 1;
      CPredCoef[3][0] = 8;
#endif
  }
};

class pt_t : public ct_t {
public:
  pt_t() {
      CPredOrder[0] = 1;
      CPredCoef[0][0] = -8;
      CPredOrder[1] = 2;
      CPredCoef[1][0] = -16;
      CPredCoef[1][1] = 8;
      CPredOrder[2] = 3;
      CPredCoef[2][0] = -24;
      CPredCoef[2][1] = 24;
      CPredCoef[2][2] = -8;
  }
};

#endif
