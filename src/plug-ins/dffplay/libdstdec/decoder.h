/*
* Direct Stream Transfer (DST) codec
* ISO/IEC 14496-3 Part 3 Subpart 10: Technical description of lossless coding of oversampled audio
*/

#ifndef DECODER_H
#define DECODER_H

#include <inttypes.h>
#include "segment.h"
#include "ct.h"
#include "fh.h"
#include "ac.h"
#include "fr.h"
#include "stream.h"

class decoder_t {
  static int  GC_ICoefSign[256];
  static int  GC_ICoefIndex[256];
  static bool GC_ICoefInit;
public:
  fr_t m_fr;                           // Contains frame based header information
  ft_t m_ft;                           // Contains FIR-coef. compression data
  pt_t m_pt;                           // Contains Ptable-entry compression data
  int  P_one[MAXTAB][AC_HISMAX];       // Probability table for arithmetic coder
  uint8_t* AData;                      // Contains the arithmetic coded bit stream of a complete frame
  int  ADataLen;                       // Number of code bits contained in AData[]
  int16_t LT_ICoefI[MAXFIR][16][256];
  uint8_t LT_Status[MAXDSTCHANNELS][16];
public:
  decoder_t();
  ~decoder_t();
  int init(int channels, int channel_frame_size);
  int close();
  int decode(const uint8_t* dst_data, int dst_bits, uint8_t* dsd_data);
private:
  int unpack(const uint8_t* dst_data, uint8_t* dsd_data);
  int16_t reverse7LSBs(int16_t c);
  void fillTable4Bit(segment_t& S, uint8_t** Table4Bit);
  void GC_InitCoefTables( int16_t ICoefI[][16][256]);
  void LT_InitStatus(uint8_t Status[][16]);
  int16_t LT_RunFilter( int16_t FilterTable[16][256], uint8_t ChannelStatus[16]);
};

#endif
