/*
 * Copyright 2019 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef PM123_DSF_H
#define PM123_DSF_H

#pragma pack(1)

typedef struct _DSD_chunk
{
  char    fourcc[4];    /* 'D','S','D',' ' (includes 1 DSD chunk space).  */
  int64_t size;         /* Size of this chunk (28 bytes).                 */
  int64_t filesize;     /* Total file size.                               */
  int64_t metadata;     /* Pointer to Metadata chunk.                     */

} DSD_chunk;

#define DSD_CHT_MONO    1 /* mono         */
#define DSD_CHT_STEREO  2 /* stereo       */
#define DSD_CHT_3       3 /* 3 channels   */
#define DSD_CHT_QUAD    4 /* quad         */
#define DSD_CHT_4       5 /* 4 channels   */
#define DSD_CHT_5       6 /* 5 channels   */
#define DSD_CHT_51      7 /* 5.1 channels */

typedef struct _fmt_chunk
{
  char    fourcc[4];    /* 'f','m','t',' ' (includes 1 fmt chunk space).  */
  int64_t size;         /* Size of this chunk (usually 52 bytes).         */
  int32_t fmtversion;   /* Version of this file format.                   */
  int32_t fmtid;        /* Format ID (0 - DSD raw).                       */
  int32_t chtype;       /* Channel Type (see DSD_CHT_*).                  */
  int32_t channels;     /* Channel numbers.                               */
  int32_t freq;         /* Sampling frequency, Hz (2822400, 5644800).     */
  int32_t bps;          /* Bits per sample (1, 8).                        */
  int64_t samples;      /* Sample count.                                  */
  int32_t blocksize;    /* Block size per channel.                        */
  int32_t reserved;     /* Fill zero.                                     */

} fmt_chunk;

typedef struct _dat_chunk
{
  char    fourcc[4];    /* 'd','a','t','a'.                               */
  int64_t size;         /* Size of this chunk.                            */

} dat_chunk;

#pragma pack()

#define DSD_ZERO 0x69

#endif /* PM123_DSF_H */

