/*
 * Copyright 2019-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_PM
#define  INCL_DOS
#define  INCL_ERRORS
#include <os2.h>
#include <process.h>
#include <errno.h>

#include "dsfplay.h"

#include <utilfct.h>
#include <output_plug.h>
#include <decoder_plug.h>
#include <debuglog.h>

static  DECODER_SETTINGS cfg;

#define WM_UPDATE_CONTROLS ( WM_USER + 1 )

// A DSD stream contain of 1-bit samples, but the stream that we have
// to output must contain a grouped 32-bit samples. Here we declare
// the types and constants needed for this conversion.
#define out_t     int32_t
#define out_size  sizeof(out_t)
#define out_bits  (out_size*8)

/* Closes the DSD file. */
static int
dsd_close( DECODER_STRUCT* w )
{
  int rc = 0;

  if( w->file ) {
    rc = xio_fclose( w->file );
    w->file = NULL;
  }

  if( w->tagv2 ) {
    id3v2_free_tag( w->tagv2 );
    w->tagv2 = NULL;
  }

  return rc;
}

/* Open DSD file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static int
dsd_open_file( DECODER_STRUCT* w, const char* filename, const char* mode )
{
  int rc = 0;

  if( w->filename != filename ) {
    strlcpy( w->filename, filename, sizeof( w->filename ));
  }

  DEBUGLOG(( "dsfplay: %s\n", filename ));

  if(( w->file = xio_fopen( filename, mode )) == NULL ) {
    return xio_errno();
  }

  if( xio_fread( &w->dsd_header, 1, sizeof( w->dsd_header ), w->file ) != sizeof( w->dsd_header )) {
    return xio_errno();
  }

  DEBUGLOG(( "dsfplay: DSD chunk '%c%c%c%c', size=%lld, filesize=%lld, metadata=%lld\n",
              w->dsd_header.fourcc[0], w->dsd_header.fourcc[1], w->dsd_header.fourcc[2], w->dsd_header.fourcc[3],
              w->dsd_header.size, w->dsd_header.filesize, w->dsd_header.metadata ));

  if( strncmp( w->dsd_header.fourcc, "DSD ", 4 ) != 0 || w->dsd_header.size != 28 ) {
    dsd_close( w );
    return -1;
  }

  if( xio_fread( &w->dsd_format, 1, sizeof( w->dsd_format ), w->file ) != sizeof( w->dsd_format )) {
    return xio_errno();
  }

  if( strncmp( w->dsd_format.fourcc, "fmt ", 4 ) != 0 || w->dsd_format.size != 52 ) {
    dsd_close( w );
    return -1;
  }

  DEBUGLOG(( "dsfplay: channels=%ld, bps=%ld, samplerate=%ld, blocksize=%ld\n",
              w->dsd_format.channels, w->dsd_format.bps, w->dsd_format.freq, w->dsd_format.blocksize ));

  if( w->dsd_format.blocksize == 0 || w->dsd_format.blocksize % out_size ) {
    dsd_close( w );
    return -1;
  }

  if( xio_fread( &w->dsd_data, 1, sizeof( w->dsd_data ), w->file ) != sizeof( w->dsd_data )) {
    return xio_errno();
  }

  if( strncmp( w->dsd_data.fourcc, "data", 4 ) != 0 ) {
    dsd_close( w );
    return -1;
  }

  w->dsd_start  = xio_ftell64( w->file );
  w->dsd_size   = w->dsd_data.size - sizeof( w->dsd_data );
  w->songlength = w->dsd_format.samples * 1000.0 / w->dsd_format.freq;
  w->seekable   = xio_can_seek( w->file );

  if( w->dsd_header.metadata && w->seekable ) {
    xio_fseek64( w->file, w->dsd_header.metadata, XIO_SEEK_SET );
    w->tagv2 = id3v2_get_tag( w->file, 0 );
    xio_fseek64( w->file, w->dsd_start, XIO_SEEK_SET );
  }

  return rc;
}

/* Notify an error. */
static void
dsd_show_error( DECODER_STRUCT* w, int style, const char* msg, int rc )
{
  char buff[1024];

  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );

  if( rc == -1 ) {
    strlcat( buff, "Unsupported format of the file.", sizeof( buff ));
  } else if( xio_errno()) {
    strlcat( buff, xio_strerror( xio_errno()), sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  int   rc;
  ULONG resetcount;
  int   i, j;

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  FORMAT_INFO output_format;

  out_t*  buffer    = NULL;
  int     bufsize   = w->audio_buffersize / out_size;
  int     bufpos    = 0;
  int     markerpos = -1;

  int64_t samplesize; // Size of the one sample in bytes.
  int64_t bps;        // Bytes per second.
  int64_t startpos;
  int64_t endpos;

  unsigned char* blocks = NULL;
  unsigned char  swap_bits[256];

  w->frew = FALSE;
  w->ffwd = FALSE;

  for( i = 0; i < 256; i++ ) {
    swap_bits[i] = 0;
    for( j = 0; j < 8; j++ ) {
      swap_bits[i] |= ((i >> j) & 1) << (7 - j);
    }
  }

  if(( rc = dsd_open_file( w, w->filename, "rb" )) != 0 ) {
    dsd_show_error( w, MB_ERROR, "Unable open file", rc );
    goto end;
  }

  samplesize = w->dsd_format.blocksize * w->dsd_format.channels;
  bps = w->dsd_format.freq / 8 * w->dsd_format.channels;

  DEBUGLOG(( "dsfplay: sample size is %d bytes, %d bytes per seconds\n", samplesize, bps ));
  #define MS2BYTES( ms ) ( ms * bps / 1000 / samplesize * samplesize )

  if(!( buffer = (out_t*)malloc( w->audio_buffersize ))) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  if(!( blocks = (unsigned char*)malloc( samplesize ))) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  output_format.size       = sizeof( output_format );
  output_format.format     = WAVE_FORMAT_DSD;
  output_format.channels   = w->dsd_format.channels;
  output_format.samplerate = w->dsd_format.freq / out_bits;
  output_format.bits       = out_bits;

  startpos = w->dsd_start + MS2BYTES( w->startpos );
  endpos   = w->dsd_start + ( w->endpos ? MS2BYTES( w->endpos ) : w->dsd_size );

  DEBUGLOG(( "dsfplay: startpos=%lld, endpos=%lld\n", startpos, endpos ));

  // After opening a new file we so are in its beginning.
  if( w->jumptopos == 0 && !w->startpos ) {
    w->jumptopos = -1;
  }

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop )
    {
      int done;

      if( w->jumptopos >= 0 )
      {
        int64_t pos = startpos + MS2BYTES( w->jumptopos );
        DEBUGLOG(( "dsfplay: jump to %d ms, %lld bytes\n", w->startpos + w->jumptopos, pos ));
        rc = xio_fseek64( w->file, pos, XIO_SEEK_SET );
        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        if( rc != 0 ) {
          dsd_show_error( w, MB_WARNING, "Unable seek in file", xio_errno());
        }

        w->jumptopos = -1;
        markerpos = -1;
        bufpos = 0;
      }

      if( markerpos < 0 ) {
        markerpos = ( xio_ftell64( w->file ) - startpos ) * 1000 / bps;
      }

      if( xio_ftell64( w->file ) >= endpos ) {
        if( bufpos ) {
          w->output_play_samples( w->a, &output_format, (char*)buffer, bufpos * out_size, markerpos );
        }
        break;
      }

      if(( done = xio_fread( blocks, 1, samplesize, w->file )) > 0 )
      {
        if( w->dsd_format.bps == 1 ) {
          for( i = 0; i < done; i++ ) {
            blocks[i] = swap_bits[blocks[i]];
          }
        }
        if( xio_ftell64( w->file ) >= endpos && !w->endpos ) {
          // The last blocks in the file can be padded with ZERO(0x00).
          // Needed to replace these zeros with silence.
          for( j = 0; j < w->dsd_format.channels; j++ ) {
            for( i = w->dsd_format.blocksize - 1; i >= 0 && blocks[w->dsd_format.blocksize * j + i] == 0; i-- ) {
              blocks[w->dsd_format.blocksize * j + i] = DSD_ZERO;
            }
            DEBUGLOG(( "dsfplay: replaced %d zero bytes with silence on %d channel.\n",
                                 w->dsd_format.blocksize - i - 1, j + 1 ));
          }
        }
        for( i = 0; i < w->dsd_format.blocksize; i += out_size ) {
          for( j = 0; j < w->dsd_format.channels; j++ )
          {
            buffer[bufpos++] = *(out_t*)( blocks + i + ( j * w->dsd_format.blocksize ));

            if( bufpos == bufsize ) {
              w->output_play_samples( w->a, &output_format, (char*)buffer, bufpos * out_size, markerpos );
              markerpos = -1;
              bufpos = 0;
            }
          }
        }
      } else {
        if( xio_ferror( w->file )) {
          dsd_show_error( w, MB_ERROR, "Unable read file", xio_errno());
          goto end;
        } else {
          break;
        }
      }
    }

    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

  w->status = DECODER_STOPPED;

end:

  dsd_close( w );
  free( buffer );
  free( blocks );

  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than yours is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = arg;

  decoder_command( w, DECODER_STOP, NULL );
  DosCloseEventSem( w->play );
  free( w );

  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = arg;

  switch( msg )
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      strlcpy( w->filename, info->filename, sizeof( w->filename ));

      w->jumptopos  = info->jumpto;
      w->startpos   = info->start;
      w->endpos     = info->end;
      w->status     = DECODER_STARTING;
      w->stop       = FALSE;
      w->songlength = 0;
      w->seekable   = TRUE;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;
      xio_fabort( w->file );
      DosPostEventSem( w->play );

      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      DosPostEventSem( w->play );
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }
  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = arg;

  if( w->seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else {
      return w->songlength;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

void static
copy_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* result, int size )
{
  ID3V2_FRAME* frame = NULL;

  if( !*result ) {
    if(( frame = id3v2_get_frame( tag, id, 1 )) != NULL ) {
      id3v2_get_string( frame, result, size );
    }
  }
}

void static
copy_id3v2_tag( DECODER_INFO* info, ID3V2_TAG* tag )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( tag ) {
    copy_id3v2_string( tag, ID3V2_TIT2, info->title,  sizeof( info->title  ));
    copy_id3v2_string( tag, ID3V2_TPE1, info->artist, sizeof( info->artist ));
    copy_id3v2_string( tag, ID3V2_TALB, info->album,  sizeof( info->album  ));
    copy_id3v2_string( tag, ID3V2_TCON, info->genre,  sizeof( info->genre  ));
    copy_id3v2_string( tag, ID3V2_TDRC, info->year,   sizeof( info->year   ));

    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        id3v2_get_string( frame, info->comment, sizeof( info->comment ) );
        break;
      }
    }

    if( HAVE_FIELD( info, track ))
    {
      copy_id3v2_string( tag, ID3V2_TCOP, info->copyright, sizeof( info->copyright ));
      copy_id3v2_string( tag, ID3V2_TRCK, info->track, sizeof( info->track ));
      copy_id3v2_string( tag, ID3V2_TPOS, info->disc, sizeof( info->disc ));

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_RVA2, i )) != NULL ; i++ )
      {
        float gain = 0;
        unsigned char* data = (unsigned char*)frame->fr_data;

        // Format of RVA2 frame:
        //
        // Identification          <text string> $00
        // Type of channel         $xx
        // Volume adjustment       $xx xx
        // Bits representing peak  $xx
        // Peak volume             $xx (xx ...)

        id3v2_get_description( frame, buffer, sizeof( buffer ));

        // Skip identification string.
        data += strlen((char*)data ) + 1;
        // Search the master volume.
        while( data - (unsigned char*)frame->fr_data < frame->fr_size ) {
          if( *data == 0x01 ) {
            gain = (float)((signed char)data[1] << 8 | data[2] ) / 512;
            break;
          } else {
            data += 3 + (( data[3] + 7 ) / 8 );
          }
        }
        if( gain != 0 ) {
          if( stricmp( buffer, "album" ) == 0 ) {
            info->album_gain = gain;
          } else {
            info->track_gain = gain;
          }
        }
      }

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_TXXX, i )) != NULL ; i++ )
      {
        id3v2_get_description( frame, buffer, sizeof( buffer ));

        if( stricmp( buffer, "replaygain_album_gain" ) == 0 ) {
          info->album_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_album_peak" ) == 0 ) {
          info->album_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_gain" ) == 0 ) {
          info->track_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_peak" ) == 0 ) {
          info->track_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        }
      }
    }
  }
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  ULONG  rc = PLUGIN_OK;
  DECODER_STRUCT* w;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  if(( rc = dsd_open_file( w, filename, "rb" )) != 0 ) {
    decoder_uninit( w );
    return rc == -1 ? PLUGIN_NO_PLAY : PLUGIN_NO_READ;
  }

  info->format.size       = sizeof( info->format );
  info->format.format     = WAVE_FORMAT_DSD;
  info->format.bits       = 32;
  info->format.channels   = w->dsd_format.channels;
  info->format.samplerate = w->dsd_format.freq / 32;
  info->mode              = w->dsd_format.channels == 1 ? 3 : 0;
  info->bitrate           = w->dsd_format.freq * w->dsd_format.channels / 1000;
  info->songlength        = w->songlength;

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = xio_fsize64( w->file );
  }

  snprintf( info->tech_info, sizeof( info->tech_info ), "%u kbs, %.1f kHz, DSD%d, %s",
            info->bitrate, ( w->dsd_format.freq / 1000.0 ), w->dsd_format.freq / 44100,
            info->format.channels == 1 ? "Mono" : "Stereo" );

  if( w->tagv2 ) {
    copy_id3v2_tag( info, w->tagv2 );
  }

  if( HAVE_FIELD( info, pics_count ))
  {
    ID3V2_FRAME* frame;
    int i, j, count = 0;

    if( w->tagv2 )
    {
      for( i = 1; ( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, i )) != NULL ; i++ ) {
        ++count;
      }

      if( count  && ( options & DECODER_LOADPICS )) {
        if(( info->pics = info->memalloc( sizeof( APIC* ) * count )) != NULL ) {
          for( i = 1, j = 0; ( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, i )) != NULL ; i++ )
          {
            int   size = sizeof( APIC ) + id3v2_picture_size( frame );
            APIC* pic;

            if(( id3v2_picture_size( frame ) > frame->fr_size  ) ||
               ( info->pics[j] = info->memalloc( size )) == NULL )
            {
              count = j;
              break;
            }

            pic = info->pics[j];

            pic->size = size;
            pic->type = id3v2_picture_type( frame );

            id3v2_get_mime_type( frame, pic->mimetype, sizeof( pic->mimetype ));
            id3v2_get_description( frame, pic->desc, sizeof( pic->desc ));

            DEBUGLOG(( "dsfplay: load APIC type %d, %s, '%s', size %d\n",
                        pic->type, pic->mimetype, pic->desc, id3v2_picture_size( frame )));

            memcpy((char*)pic + sizeof( APIC ),
                   id3v2_picture_data_ptr( frame ), id3v2_picture_size( frame ));
            ++j;
          }
        }
      }
    }

    info->pics_count = count;
    DEBUGLOG(( "dsfplay: found %d attached pictures\n", info->pics_count ));
  }

  if( HAVE_FIELD( info, saveinfo ))
  {
    info->saveinfo = is_file( filename );
    info->codepage = ch_default();
    info->haveinfo = DECODER_HAVE_TITLE      |
                     DECODER_HAVE_ARTIST     |
                     DECODER_HAVE_ALBUM      |
                     DECODER_HAVE_YEAR       |
                     DECODER_HAVE_GENRE      |
                     DECODER_HAVE_TRACK      |
                     DECODER_HAVE_DISC       |
                     DECODER_HAVE_COMMENT    |
                     DECODER_HAVE_COPYRIGHT  |
                     DECODER_HAVE_TRACK_GAIN |
                     DECODER_HAVE_TRACK_PEAK |
                     DECODER_HAVE_ALBUM_GAIN |
                     DECODER_HAVE_ALBUM_PEAK |
                     DECODER_HAVE_PICS       |
                     DECODER_PICS_ANYTYPE;
  }

  dsd_close( w );
  decoder_uninit( w );
  return rc;
}

static void
replace_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* string )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( id == ID3V2_COMM ) {
    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        break;
      }
    }
  } else {
    frame = id3v2_get_frame( tag, id, 1 );
  }

  if( frame == NULL ) {
    frame = id3v2_add_frame( tag, id );
  }
  if( frame != NULL ) {
    id3v2_set_string( frame, string );
  }
}

ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  ULONG  rc = PLUGIN_OK;
  int    i;

  DECODER_STRUCT* w;

  if( !*filename ) {
    return EINVAL;
  }

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return errno;
  }

  if(( rc = dsd_open_file( w, filename, "r+b" )) != 0 ) {
    DEBUGLOG(( "dsfplay: unable open file for saving %s, rc=%d\n", filename, rc ));
    decoder_uninit( w );
    return rc;
  }

  if( !w->tagv2 ) {
    w->tagv2 = id3v2_new_tag();
  }

  // FIX ME: Replay gain info also must be saved.

  replace_id3v2_string( w->tagv2, ID3V2_TIT2, info->title     );
  replace_id3v2_string( w->tagv2, ID3V2_TPE1, info->artist    );
  replace_id3v2_string( w->tagv2, ID3V2_TALB, info->album     );
  replace_id3v2_string( w->tagv2, ID3V2_TDRC, info->year      );
  replace_id3v2_string( w->tagv2, ID3V2_COMM, info->comment   );
  replace_id3v2_string( w->tagv2, ID3V2_TCON, info->genre     );
  replace_id3v2_string( w->tagv2, ID3V2_TCOP, info->copyright );
  replace_id3v2_string( w->tagv2, ID3V2_TRCK, info->track     );
  replace_id3v2_string( w->tagv2, ID3V2_TPOS, info->disc      );

  if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS ))
  {
    ID3V2_FRAME* frame;

    while(( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, 1 )) != NULL ) {
      id3v2_delete_frame( frame );
    }
    for( i = 0; i < info->pics_count; i++ ) {
      if(( frame = id3v2_add_frame( w->tagv2, ID3V2_APIC )) != NULL )
      {
        APIC* pic = info->pics[i];

        id3v2_set_picture( frame, pic->type, pic->mimetype, pic->desc,
                           (char*)pic + sizeof(APIC), pic->size - sizeof(APIC));
      }
    }
  }

  if( !w->dsd_header.metadata ) {
    w->dsd_header.metadata = w->dsd_start + w->dsd_size;
  }

  xio_fseek64( w->file, w->dsd_header.metadata, XIO_SEEK_SET );

  switch( id3v2_set_tag( w->file, w->tagv2, NULL )) {
    case ID3V2_ERROR:
      rc = xio_errno();
      break;
    case ID3V2_BADPOS:
      rc = EINVAL;
      break;
    case ID3V2_OK:
      w->dsd_header.filesize = xio_fsize64( w->file );
      xio_rewind( w->file );
      if( xio_fwrite( &w->dsd_header, 1, sizeof( w->dsd_header ), w->file ) != sizeof( w->dsd_header )) {
        rc = xio_errno();
      }
      break;
  }

  dsd_close( w );
  decoder_uninit( w );
  return rc;
}

ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 3 ) {
      strcpy( ext[0], "*.DSF" );
      strcpy( ext[1], "audio/x-dsf" );
      strcpy( ext[2], "audio/x-dsd" );
    }
    *size = 3;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_SAVEINFO;
}

static void
init_tag( void )
{
  id3v2_set_read_charset( cfg.tag_read_id3v2_charset );
  id3v2_set_save_charset( cfg.tag_save_id3v2_charset );
}

static void
save_ini( void )
{
  HINI hini;
  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, cfg.tag_read_id3v2_charset );
    save_ini_value( hini, cfg.tag_save_id3v2_charset );
    close_ini( hini );
  }
}

static void
load_ini( void )
{
  HINI hini;

  if( ch_default() == CH_CYR_OS2 ) {
    cfg.tag_read_id3v2_charset = CH_CYR_WIN;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
  } else {
    cfg.tag_read_id3v2_charset = CH_DEFAULT;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
  }

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, cfg.tag_read_id3v2_charset );
    load_ini_value( hini, cfg.tag_save_id3v2_charset );
    close_ini( hini );
  }

  init_tag();
}

/* Processes messages of the configuration dialog. */
static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
    {
      int i;

      for( i = 0; i < ch_list_size; i++ ) {
        lb_add_with_handle( hwnd, CB_ID3V2_RDCH, ch_list[i].name, ch_list[i].id );
      }

      for( i = 0; i < ch_list_size; i++ ) {
        // All except for auto detection and UCS-2 LE.
        if( ch_list[i].id >= 0 && ch_list[i].id != CH_UCS_2LE ) {
          lb_add_with_handle( hwnd, CB_ID3V2_WRCH, ch_list[i].name, ch_list[i].id );
        }
      }

      do_warpsans( hwnd );
      // continue to WM_UPDATE_CONTROLS...
    }

    case WM_UPDATE_CONTROLS:
    {
      lb_select_by_handle( hwnd, CB_ID3V2_RDCH, cfg.tag_read_id3v2_charset );
      lb_select_by_handle( hwnd, CB_ID3V2_WRCH, cfg.tag_save_id3v2_charset );
      return 0;
    }

    case WM_DESTROY:
    {
      int  i;

      if(( i = lb_cursored( hwnd, CB_ID3V2_RDCH )) != LIT_NONE ) {
        cfg.tag_read_id3v2_charset = (int)lb_get_handle( hwnd, CB_ID3V2_RDCH, i );
      }
      if(( i = lb_cursored( hwnd, CB_ID3V2_WRCH )) != LIT_NONE ) {
        cfg.tag_save_id3v2_charset = (int)lb_get_handle( hwnd, CB_ID3V2_WRCH, i );
      }

      save_ini();
      init_tag();
      break;
    }

    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case PB_UNDO:
          WinSendMsg( hwnd, WM_UPDATE_CONTROLS, 0, 0 );
          return 0;

        case PB_DEFAULT:
        {
          if( ch_default() == CH_CYR_OS2 ) {
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_CYR_WIN  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          } else {
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_DEFAULT  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          }

          return 0;
        }
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in. */
void DLLENTRY
plugin_configure( HWND hwnd, HMODULE module ) {
  WinDlgBox( HWND_DESKTOP, hwnd, cfg_dlg_proc, module, DLG_CONFIGURE, NULL );
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "DSD Storage Facility Decoder " VER_STRING;
  param->configurable = TRUE;

  load_ini();
}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    _CRT_term();
  }
  return 1UL;
}
#endif
