/*
 * Copyright 2020-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_ERRORS
#define  INCL_PM
#include <os2.h>
#include <process.h>
#include <errno.h>

#include "aacplay.h"

#include <utilfct.h>
#include <output_plug.h>
#include <decoder_plug.h>
#include <debuglog.h>

static  DECODER_STRUCT** instances = NULL;
static  int  instances_count = 0;
static  HMTX mutex;
static  DECODER_SETTINGS cfg;

#define WM_UPDATE_CONTROLS ( WM_USER + 1 )

#define BUFF_SIZE     ( FAAD_MIN_STREAMSIZE * 64 )
#define ADIF_MAX_SIZE ( 30 )
#define ADTS_MAX_SIZE ( 10 )

static int sample_rates[] = { 96000, 88200, 64000, 48000, 44100, 32000,
                              24000, 22050, 16000, 12000, 11025, 8000, 7350 };

// A fixed encoder delay of 2112 samples was chosen because at
// that time this was the common encoding delay used, for various
// reasons, by most of the shipping implementations of AAC
// encoders (commercial and otherwise).
// See: https://developer.apple.com/library/archive/documentation/QuickTime/QTFF/QTFFAppenG/QTFFAppenG.html

#define DECODER_DELAY 2112

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte) \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

static uint32_t
cb_read( void* udata, void* buffer, uint32_t length ) {
  return xio_fread( buffer, 1, length, ((DECODER_STRUCT*)udata)->file );
}

static uint32_t
cb_write( void* udata, void* buffer, uint32_t length ) {
  return xio_fwrite( buffer, 1, length, ((DECODER_STRUCT*)udata)->file );
}

static uint32_t
cb_seek( void* udata, uint64_t pos ) {
  return xio_fseek64(((DECODER_STRUCT*)udata)->file, pos, XIO_SEEK_SET );
}

static uint32_t
cb_truncate( void* udata ) {
  return xio_ftruncate64(((DECODER_STRUCT*)udata)->file,
                         xio_ftell64(((DECODER_STRUCT*)udata)->file ));
}

static uint32_t
cb_is_pipe( void* udata ) {
  return !xio_can_seek(((DECODER_STRUCT*)udata)->file );
}

static uint32_t
cb_skip( void* udata, uint32_t skip )
{
  char buffer[32];
  uint32_t reads = skip / sizeof(buffer);

  while( reads-- ) {
    if( xio_fread( buffer, 1, sizeof(buffer), ((DECODER_STRUCT*)udata)->file ) != sizeof(buffer)) {
      return -1;
    }
  }

  if(( reads = skip % sizeof(buffer)) != 0 ) {
    if( xio_fread( buffer, 1, reads, ((DECODER_STRUCT*)udata)->file ) != reads ) {
      return -1;
    }
  }

  return 0;
}

static mp4ff_callback_t*
cb_alloc( DECODER_STRUCT* w )
{
  mp4ff_callback_t* cb = malloc( sizeof( mp4ff_callback_t ));

  if( cb ) {
    cb->read      = cb_read;
    cb->write     = cb_write;
    cb->seek      = cb_seek;
    cb->truncate  = cb_truncate;
    cb->is_pipe   = cb_is_pipe;
    cb->user_data = w;
  }

  return cb;
}

/* Converts position in ADIF/ADTS stream to milliseconds. */
static int
aac_pos_to_ms( DECODER_STRUCT* w, uint64_t pos ) {
  return (float)( pos - w->tagv2_size ) / ( xio_fsize64( w->file ) - w->tagv2_size ) * w->songlength;
}

/* Converts miliiseconds to position in ADIF/ADTS stream. */
static uint64_t
aac_ms_to_pos( DECODER_STRUCT* w, int ms ) {
  return (float)ms / w->songlength * ( xio_fsize64( w->file ) - w->tagv2_size ) + w->tagv2_size;
}

/* Parses ADIF / Audio Data Interchange Format header. Returns 0 if it
   successfully parses the stream header. A -1 return value indicates an
   error or unsupported format of the file. */
static int
read_ADIF_header( DECODER_STRUCT* w )
{
  uint8_t buffer[ADIF_MAX_SIZE];
  int skip = 0, rc = -1;

  NeAACDecHandle decoder = 0;
  w->filetype = FILE_ADIF;
  w->seekable = 0;
  w->started  = xio_ftell64( w->file );

  // Get ADIF header data.
  if( xio_fread( buffer, 1, ADIF_MAX_SIZE, w->file ) == 0 ) {
    return -1;
  }

  // Copyright string, skip 9 bytes.
  if( buffer[4] & 0x80 ) {
    skip += 9;
  }

  w->bitrate = ((ULONG)( buffer[4 + skip] & 0x0F ) << 19 ) |
               ((ULONG)( buffer[5 + skip] ) << 11 ) |
               ((ULONG)( buffer[6 + skip] ) << 3  ) |
               ((ULONG)( buffer[7 + skip] & 0xE0 >> 5 ));

  DEBUGLOG(( "aacplay: bitrate bits: "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN" "
             BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN", cbr = %d, bitrate = %d\n",
             BYTE_TO_BINARY(( buffer[4 + skip] & 0x0F )), BYTE_TO_BINARY(( buffer[5 + skip] )),
             BYTE_TO_BINARY(( buffer[6 + skip] )), BYTE_TO_BINARY(( buffer[7 + skip] & 0xE0 )),
             (( buffer[4 + skip] & 0x10 ) >> 4 ), w->bitrate ));

  if( w->bitrate ) {
    w->songlength = (float)( xio_fsize64( w->file ) - w->tagv2_size ) / ( w->bitrate / 8 ) * 1000;
    w->bitrate /= 1000;
  } else {
    w->songlength = 0;
  }

  if(( decoder = NeAACDecOpen()) != NULL ) {
    if( NeAACDecInit( decoder, buffer, ADIF_MAX_SIZE, &w->samplerate, &w->channels ) != -1 ) {
      rc = 0;
    }
  }

  NeAACDecClose( decoder );
  xio_fseek64( w->file, -ADIF_MAX_SIZE, XIO_SEEK_CUR );
  return rc;
}

/* Parse ADTS / Audio Data Transport Stream header. Returns 0 if it
   successfully parses the stream header. A -1 return value indicates an
   error or unsupported format of the file. */
static int
read_ADTS_header( DECODER_STRUCT* w, uint8_t* readed_before )
{
  uint8_t  buffer[ADTS_MAX_SIZE];
  int      resync    = 0;
  int      frames    = 0;
  int      readahead;
  int      framesize;
  int      totalsize;

  // Fill the buffer with previously read and new data.
  memcpy( buffer, readed_before, 4 );
  if( xio_fread( buffer + 4, 1, ADTS_MAX_SIZE - 4, w->file ) != ADTS_MAX_SIZE - 4 ) {
    DEBUGLOG(( "aacplay: found a very short AAC file...\n" ));
    return -1;
  }

  w->filetype = FILE_ADTS;
  w->seekable = xio_can_seek( w->file );

  // Find syncword.
  while(( buffer[0] != 0xFF ) || (( buffer[1] & 0xF6 ) != 0xF0 ))
  {

read_again:

    if( ++resync > MAXRESYNC ) {
      DEBUGLOG(( "aacplay: giving up searching valid ADTS header...\n" ));
      return -1;
    }
    memmove( buffer, buffer + 1, ADTS_MAX_SIZE - 1 );
    if( xio_fread( buffer + ADTS_MAX_SIZE - 1, 1, 1, w->file ) != 1 ) {
      DEBUGLOG(( "aacplay: found a very short AAC file...\n" ));
      return -1;
    }
  }

  w->started = xio_ftell64( w->file ) - ADTS_MAX_SIZE;
  readahead  = 43 * ( xio_can_seek( w->file ) == XIO_CAN_SEEK_FAST ? 10 : 1 );
  totalsize  = 0;

  // Read 1 or 10 seconds worth to ensure correct time and bitrate.
  for( frames = 0; frames < readahead; frames++ )
  {
    if( frames != 0 ) {
      if( xio_fread( buffer, 1, ADTS_MAX_SIZE, w->file ) != ADTS_MAX_SIZE ) {
        if( frames < 10 ) {
          DEBUGLOG(( "aacplay: found a very short AAC file...\n" ));
          return -1;
        } else {
          break;
        }
      }
    }

    // Check syncword.
    if(( buffer[0] != 0xFF ) || (( buffer[1] & 0xF6 ) != 0xF0 )) {
      goto read_again;
    }

    // Fixed ADTS header is the same for every frame, so we read it only once.
    if( frames == 0 )
    {
      int sr_idx = ( buffer[2] & 0x3C ) >> 2;
      w->samplerate = sample_rates[sr_idx];
      w->channels = (( buffer[2] & 0x01 ) << 2 ) | (( buffer[3] & 0xC0 ) >> 6 );
      w->profile = ( buffer[2] >> 6 ) + 1;
    }

    // ...and the variable ADTS header.
    framesize = ((((uint32_t)buffer[3] & 0x3 )) << 11 ) | (((uint32_t)buffer[4] ) << 3 ) | ( buffer[5] >> 5 );
    totalsize += framesize;

    // ...and skip remain frame data.
    if( cb_skip( w, framesize - ADTS_MAX_SIZE ) == -1 ) {
      return -1;
    }
  }

  if( resync ) {
    DEBUGLOG(( "aacplay: found ADTS header after %d attempts...\n", resync ));
  } else {
    DEBUGLOG(( "aacplay: found ADTS header.\n" ));
  }

  w->bitrate = ( totalsize / frames ) * ( w->samplerate / 1024 );
  w->songlength = (float)( xio_fsize64( w->file ) - w->tagv2_size ) / w->bitrate * 1000;
  w->bitrate = w->bitrate * 8 / 1000;

  if( w->seekable ) {
    xio_fseek64( w->file, w->started, XIO_SEEK_SET );
  }

  return 0;
}

/* Get AAC format specifications. Returns 0 if it successfully
   detect and parses the stream header. A -1 return value indicates
   an error or unsupported format of the file. */
static int
aac_query_info( DECODER_STRUCT* w )
{
  uint8_t buffer[4];

  if( xio_fread( buffer, 1, 4, w->file ) != 4 ) {
    return xio_errno();
  }

  // Determine the header type, check the first two bytes.
  if( buffer[0] == 'A' &&
      buffer[1] == 'D' &&
      buffer[2] == 'I' &&
      buffer[3] == 'F' )
  {
    DEBUGLOG(( "aacplay: found ADIF header.\n" ));
    xio_fseek64( w->file, -4, XIO_SEEK_CUR );
    if( read_ADIF_header( w ) != 0 ) {
      return -1;
    }
  }
  else
  {
    DEBUGLOG(( "aacplay: try to find ADTS header.\n" ));
    if( read_ADTS_header( w, buffer ) != 0 ) {
      return -1;
    }
  }

  // MPEG specification states: assume SBR on files with samplerate <= 24000 Hz.
  if( w->samplerate <= 24000 ) {
    w->samplerate *= 2;
  }

  // upMatrix to 2 channels for implicit signalling of PS.
  if( w->channels == 1 ) {
    w->channels = 2;
  }

  return 0;
}

/* Get MPEG4 AAC format specifications. Returns 0 if it successfully
   detect and parses the stream header. A -1 return value indicates
   an error or unsupported format of the file. */
static int
aac_query_mp4_info( DECODER_STRUCT* w )
{
  uint8_t* buffer = NULL;
  uint32_t buffsize = 0;
  int i, tracks;

  if(( w->mp4cb = cb_alloc( w )) == NULL ) {
    return errno;
  }
  if(( w->mp4file = mp4ff_open_read( w->mp4cb )) == NULL ) {
    return -1;
  }

  w->filetype = FILE_MP4;
  w->seekable = xio_can_seek( w->file );

  tracks = mp4ff_total_tracks( w->mp4file );
  for( i = 0; i < tracks; i++ )
  {
    mp4AudioSpecificConfig mp4ASC;
    mp4ff_get_decoder_config( w->mp4file, i, &buffer, &buffsize );

    if( buffer ) {
      if( NeAACDecAudioSpecificConfig( buffer, buffsize, &mp4ASC ) == 0 )
      {
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.objectTypeIndex = %u\n", mp4ASC.objectTypeIndex ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.samplingFrequencyIndex = %u\n", mp4ASC.samplingFrequencyIndex ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.samplingFrequency = %lu\n", mp4ASC.samplingFrequency ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.channelsConfiguration = %u\n", mp4ASC.channelsConfiguration ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.frameLengthFlag = %u\n", mp4ASC.frameLengthFlag ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.dependsOnCoreCoder = %u\n", mp4ASC.dependsOnCoreCoder ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.coreCoderDelay = %u\n", mp4ASC.coreCoderDelay ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.extensionFlag = %u\n", mp4ASC.extensionFlag ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.aacSectionDataResilienceFlag = %u\n", mp4ASC.aacSectionDataResilienceFlag ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.aacScalefactorDataResilienceFlag = %u\n", mp4ASC.aacScalefactorDataResilienceFlag ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.aacSpectralDataResilienceFlag = %u\n", mp4ASC.aacSpectralDataResilienceFlag ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.epConfig = %u\n", mp4ASC.epConfig ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.sbr_present_flag = %d\n", mp4ASC.sbr_present_flag ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.forceUpSampling = %d\n", mp4ASC.forceUpSampling ));
        DEBUGLOG(( "aacplay: mp4AudioSpecificConfig.downSampledSBR = %d\n", mp4ASC.downSampledSBR ));

        w->profile    = mp4ASC.objectTypeIndex;
        w->samplerate = mp4ASC.samplingFrequency;
        w->channels   = mp4ASC.channelsConfiguration;
        w->songlength = mp4ff_get_track_duration( w->mp4file, i ) * 1000.0 / mp4ff_time_scale( w->mp4file, i );
        w->bitrate    = mp4ff_get_avg_bitrate( w->mp4file, i ) / 1000;

        if( w->bitrate == 0 && tracks == 1 ) {
          w->bitrate = xio_fsize64( w->file ) / w->songlength * 8;
        }

        free( buffer );
        w->mp4aac_track = i;
        return 0;
      } else {
        free( buffer );
      }
    }
  }

  return -1;
}

/* Closes the MPEG4 or AAC file. */
static int
aac_close( DECODER_STRUCT* w )
{
  int rc = 0;
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->mp4file ) {
    mp4ff_close( w->mp4file );
    w->mp4file = NULL;
  }
  if( w->mp4cb ) {
    free( w->mp4cb );
    w->mp4cb = NULL;
  }
  if( w->file ) {
    rc = xio_fclose( w->file );
    w->file = NULL;
  }
  if( w->tagv2 ) {
    id3v2_free_tag( w->tagv2 );
    w->tagv2 = NULL;
  }

  DosReleaseMutexSem( w->mutex );
  return rc;
}

/* Open MPEG4 or AAC file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static int
aac_open_file( DECODER_STRUCT* w, const char* filename, const char* mode )
{
  int  rc;
  char header[8] = { 0 };

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->filename != filename ) {
    strlcpy( w->filename, filename, sizeof( w->filename ));
  }

  DEBUGLOG(( "aacplay: %s\n", filename ));

  w->filetype     = FILE_UNKNOWN;
  w->mp4cb        = NULL;
  w->mp4file      = NULL;
  w->mp4aac_track = -1;
  w->tagv2        = NULL;
  w->tagv2_size   = 0;
  w->started      = 0;

  w->delay = DECODER_DELAY;
  w->trim  = 0;

  if(( w->file = xio_fopen( filename, mode )) == NULL ) {
    DosReleaseMutexSem( w->mutex );
    return xio_errno();
  }

  if( w->hwnd ) {
    xio_set_observer( w->file, w->hwnd, w->metadata_buff, w->metadata_size );
  }

  if( xio_fread( header, 1, 8, w->file ) != 8 ) {
    rc = xio_ferror( w->file ) ? xio_errno() : -1;
  } else {
    xio_rewind( w->file );

    if( header[4] == 'f' &&
        header[5] == 't' &&
        header[6] == 'y' &&
        header[7] == 'p' )
    {
      rc = aac_query_mp4_info( w );
    } else {
      if( header[0] == 'I' &&
          header[1] == 'D' &&
          header[2] == '3' )
      {
        if(( w->tagv2 = id3v2_get_tag( w->file, 0 )) != NULL ) {
          w->tagv2_size = w->tagv2->id3_totalsize;
        }
      }
      rc = aac_query_info( w );
    }
  }

  if( rc != 0 ) {
    aac_close( w );
  }

  DosReleaseMutexSem( w->mutex );
  return rc;
}

/* Flushes currently decoded samples. */
static int
aac_flush_output( DECODER_STRUCT* w )
{
  if( w->output_play_samples( w->a, &w->output_format, w->buffer, w->bufpos, w->posmarker ) != w->bufpos ) {
    return -1;
  }

  w->bufpos = 0;
  return 0;
}

/* Move a count of bytes of the decoded audio samples
   to the audio buffer. */
static int
aac_play_samples( DECODER_STRUCT* w, int16_t* samples, int samples_count, ULONG bps )
{
  int done, bytes_count;

  if( w->delay && samples_count > 0 )
  {
    // If this is a first frames, samples which are added at start
    // by encoder (encoder delay) must be skipped.

    UCHAR channel;
    BOOL  silence = TRUE;
    int   skip = 0;

    while( samples_count > skip && silence && w->delay )
    {
      // Find the first non-gap sample, and adjust delay accordingly.
      for( channel = 0; channel < w->channels; channel++ ) {
        if( abs((samples+skip)[channel]) > NOISE_GATE_LEVEL ) {
          silence  = FALSE;
          w->delay = 0;
        }
      }
      if( silence ) {
        skip += w->channels;
        --w->delay;
      }
    }

    DEBUGLOG(( "aacplay: skip a %d silence samples from the %d beginning.\n",
                skip / w->channels, samples_count / w->channels ));
    samples_count -= skip;
    samples += skip;
  }

  if( w->trim && samples_count )
  {
    // If this is a last frame, samples which are added at end
    // by encoder (encoder padding) must be skipped.

    UCHAR channel;
    BOOL  silence = TRUE;
    int   skip = 0;

    while( samples_count > skip && silence )
    {
      // Find the first non-gap sample, and adjust padding accordingly.
      for( channel = 0; channel < w->channels; channel++ ) {
        if( abs((samples+samples_count-skip-1)[-channel]) > NOISE_GATE_LEVEL ) {
          silence = FALSE;
          w->trim = 0;
        }
      }
      if( silence ) {
        skip += w->channels;
      }
    }

    DEBUGLOG(( "aacplay: skip a %d silence samples from last %d.\n",
                skip / w->channels, samples_count / w->channels ));
    samples_count -= skip;
  }

  for( done = 0, bytes_count = samples_count * sizeof(int16_t); done < bytes_count; )
  {
    int bytes  = bytes_count - done;
    int remain = w->buffersize - w->bufpos;

    if( bytes > remain ) {
      bytes = remain;
    }

    memcpy( w->buffer + w->bufpos, (BYTE*)samples + done, bytes );
    w->bufpos += bytes;
    done += bytes;

    if( w->bufpos == w->buffersize ) {
      if( aac_flush_output( w ) != 0 ) {
        return -1;
      }
      if( bps > 0 && bps != w->bitrate ) {
        WinPostMsg( w->hwnd, WM_CHANGEBR, MPFROMLONG( bps ), 0 );
        w->bitrate = bps;
      }
    }
  }

  return 0;
}

/* Notify an error. */
static void
aac_show_error( DECODER_STRUCT* w, int style, const char* msg, const char* what, int rc )
{
  char buff[1024];
  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );

  if( what && *what ) {
    strlcat( buff, what, sizeof( buff ));
  } else if( rc == -1 ) {
    strlcat( buff, "Unsupported format of the file.", sizeof( buff ));
  } else if( xio_errno()) {
    strlcat( buff, xio_strerror( xio_errno()), sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}

/* AAC Decoding. Returns 0 if it successfully decodes the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static int
aac_decode( DECODER_STRUCT* w )
{
  NeAACDecHandle decoder = NeAACDecOpen();

  ULONG    resetcount;
  uint8_t* buffer   = NULL;
  uint32_t buffsize = 0;
  uint32_t consumed = 0;
  int      rc = 0;

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( !decoder
      || ( buffer = malloc( BUFF_SIZE )) == NULL
      || ( buffsize = xio_fread( buffer, 1, BUFF_SIZE, w->file )) == 0 )
  {
    aac_show_error( w, MB_ERROR, "Unable init AAC decoder", NULL, 0 );
    DosReleaseMutexSem( w->mutex );
    rc = -1; goto end;
  }

  consumed = NeAACDecInit( decoder, buffer, buffsize, &w->samplerate, &w->channels );
  DEBUGLOG(( "aacplay: samplerate=%d, channels=%d\n", w->samplerate, w->channels ));
  DosReleaseMutexSem( w->mutex );
  w->posmarker = 0;

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop )
    {
      void* samplebuffer;
      NeAACDecFrameInfo frameinfo;
      int bps = w->bitrate;

      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

      if( w->jumptopos >= 0 )
      {
        w->jumptopos += w->startpos;
        DEBUGLOG(( "aacplay: jumps to %d ms (%lld)\n", w->jumptopos, aac_ms_to_pos( w, w->jumptopos )));

        if( w->filetype != FILE_ADIF ) {
          rc = xio_fseek64( w->file, aac_ms_to_pos( w, w->jumptopos ), XIO_SEEK_SET );
        } else {
          rc = -1;
        }

        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        w->delay = w->jumptopos ? 0 : DECODER_DELAY;
        w->jumptopos = -1;

        if( rc == -1 ) {
          aac_show_error( w, MB_WARNING, "Unable seek in file", NULL, w->filetype == FILE_ADIF ? -1 : 0 );
        } else {
          w->bufpos = 0;
          consumed = 0;
          buffsize = xio_fread( buffer, 1, BUFF_SIZE, w->file );
        }
      }

      if( consumed > 0 ) {
        memmove( buffer, buffer + consumed, buffsize - consumed );
        buffsize -= consumed;
        buffsize += xio_fread( buffer + buffsize, 1, BUFF_SIZE - buffsize, w->file );
        consumed = 0;
      }

      DosReleaseMutexSem( w->mutex );

      if( buffsize ) {
        samplebuffer = NeAACDecDecode( decoder, &frameinfo, buffer, buffsize );
        consumed = frameinfo.bytesconsumed;

        if( w->songlength ) {
          w->posmarker  = aac_pos_to_ms( w, xio_ftell64( w->file ) - buffsize ) - w->startpos;
        } else {
          w->posmarker += (float)frameinfo.samples * 1000 / frameinfo.samplerate / frameinfo.channels;
        }

        if( frameinfo.error > 0 ) {
          aac_show_error( w, MB_ERROR, "Error decoding file", NeAACDecGetErrorMessage( frameinfo.error ), 0 );
          DEBUGLOG(( "aacplay: %s (%d)\n", NeAACDecGetErrorMessage( frameinfo.error ), frameinfo.error ));
          w->bufpos = 0; rc = -1; goto end;
        }

        if( w->endpos && w->startpos + w->posmarker >= w->endpos  ) {
          DEBUGLOG(( "aacplay: break at %d ms (%lld) because %d ms end is reached\n", w->startpos + w->posmarker,
                      xio_ftell64( w->file ) - buffsize, w->endpos ));
          break;
        }

        if( buffsize == consumed ) {
          w->trim = 1;
        }

        if( frameinfo.samples ) {
          bps = frameinfo.bytesconsumed * 8 * frameinfo.samplerate * frameinfo.channels / frameinfo.samples / 1000;
        }
        if( aac_play_samples( w, samplebuffer, frameinfo.samples, bps ) != 0 ) {
          w->bufpos = 0; rc = -1; goto end;
        }
      } else {
        break;
      }
    }

    aac_flush_output( w );
    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

end:

  NeAACDecClose( decoder );
  free( buffer );
  return rc;
}

/* MPEG4 AAC Decoding. Returns 0 if it successfully decodes the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static int
aac_decode_mp4( DECODER_STRUCT* w )
{
  NeAACDecHandle decoder = NeAACDecOpen();

  ULONG    resetcount;
  uint8_t* buffer   = NULL;
  uint32_t buffsize = 0;
  float    samplelen;     // in milliseconds
  int32_t  samples;
  int32_t  played   = 0;  // played sample
  int32_t  start;         // first played sample
  int32_t  end;           // last sample to play
  int      rc = 0;

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( !decoder
      || mp4ff_get_decoder_config( w->mp4file, w->mp4aac_track, &buffer, &buffsize ) != 0
      || NeAACDecInit2( decoder, buffer, buffsize, &w->samplerate, &w->channels ) != 0 )
  {
    aac_show_error( w, MB_ERROR, "Unable init AAC decoder", NULL, 0 );
    DosReleaseMutexSem( w->mutex );
    rc = -1; goto end;
  }

  free( buffer );
  buffer = NULL;

  samples   = mp4ff_num_samples( w->mp4file, w->mp4aac_track );
  samplelen = mp4ff_get_sample_duration( w->mp4file, w->mp4aac_track, 0 ) * 1000.0 / mp4ff_time_scale( w->mp4file, w->mp4aac_track );
  start     = w->startpos / samplelen;
  end       = w->endpos ? w->endpos / samplelen : samples - 1;

  DosReleaseMutexSem( w->mutex );

  DEBUGLOG(( "aacplay: play %d samples of MPEG4 AAC, ch = %d, bps = %d, sample = %f ms\n",
              samples, w->channels, w->bitrate, samplelen ));
  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop )
    {
      void* samplebuffer;
      NeAACDecFrameInfo frameinfo;
      int32_t bytes;

      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

      if( w->jumptopos >= 0 )
      {
        played = ( w->startpos + w->jumptopos ) / samplelen;
        DEBUGLOG(( "aacplay: jumps to %d ms (%d sample)\n", w->startpos + w->jumptopos, played ));

        w->jumptopos = -1;
        w->bufpos = 0;
        w->delay = played ? 0 : DECODER_DELAY;

        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );
        DosResetEventSem( w->play, &resetcount );
      }

      bytes = mp4ff_read_sample( w->mp4file, w->mp4aac_track, played, &buffer, &buffsize );
      DosReleaseMutexSem( w->mutex );

      if( bytes == 0 ) {
        aac_show_error( w, MB_ERROR, "Unable read file", NULL, 0 );
        rc = -1; goto end;
      }

      w->posmarker = ( played - start ) * samplelen;
      samplebuffer = NeAACDecDecode( decoder, &frameinfo, buffer, buffsize );
      free( buffer );
      buffer = NULL;

      if( frameinfo.error > 0 ) {
        aac_show_error( w, MB_ERROR, "Error decoding file", NeAACDecGetErrorMessage( frameinfo.error ), 0 );
        rc = -1; goto end;
      }

      if( ++played >= (samples - 1)) {
        w->trim = ( played == samples - 1 );
      }

      if( aac_play_samples( w, samplebuffer, frameinfo.samples, bytes * 8 / samplelen ) != 0 ) {
        aac_show_error( w, MB_ERROR, "Error decoding file", NULL, 0 );
        rc = -1; goto end;
      }

      if( w->ffwd ) {
        // The skip of this intervals will force the decoder to be
        // accelerated approximately in 10 times.
        played += 9;
      } else if( w->frew ) {
        played -= 11;
      }

      if( played >= end ) {
        DEBUGLOG(( "aacplay: break at %d ms (%d sample) "
                   "because end is reached\n", w->posmarker, played ));
        break;
      }
      if( played < start ) {
        DEBUGLOG(( "aacplay: break at %d ms (%d sample) "
                   "because start is reached\n", w->posmarker, played ));
        break;
      }
    }

    aac_flush_output( w );
    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

end:

  NeAACDecClose( decoder );
  free( buffer );
  return rc;
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  int  rc;

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  w->frew = FALSE;
  w->ffwd = FALSE;

  if(( rc = aac_open_file( w, w->filename, "rb" )) != 0 ) {
    aac_show_error( w, MB_ERROR, "Unable open file", NULL, rc );
  } else {
    // After opening a new file we so are in its beginning.
    if( w->jumptopos == 0 && !w->startpos ) {
      w->jumptopos = -1;
    }

    w->output_format.size       = sizeof( w->output_format );
    w->output_format.format     = WAVE_FORMAT_PCM;
    w->output_format.bits       = 16;
    w->output_format.channels   = w->channels;
    w->output_format.samplerate = w->samplerate;

    if(( w->buffer = (char*)malloc( w->buffersize )) == NULL ) {
      aac_show_error( w, MB_ERROR, "Unable open file", NULL, rc );
    } else {
      if( w->filetype == FILE_MP4 ) {
        rc = aac_decode_mp4( w );
      } else {
        rc = aac_decode( w );
      }

      if( rc == 0 ) {
        w->status = DECODER_STOPPED;
      }
    }
  }

  free( w->buffer );
  aac_close( w );
  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );
  DosCreateMutexSem( NULL, &w->mutex, 0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  instances = realloc( instances, ++instances_count * sizeof( DECODER_STRUCT* ));
  instances[ instances_count - 1 ] = w;
  DosReleaseMutexSem( mutex );

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than yours is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = arg;
  int i;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  decoder_command( w, DECODER_STOP, NULL );

  for( i = 0; i < instances_count; i++ ) {
    if( instances[i] == w ) {
      if( i < instances_count - 1 ) {
        memmove( instances + i, instances + i + 1,
               ( instances_count - i - 1 ) * sizeof( DECODER_STRUCT* ));
      }
      instances = realloc( instances, --instances_count * sizeof( DECODER_STRUCT* ));
    }
  }

  DosReleaseMutexSem( mutex  );
  DosCloseEventSem( w->play  );
  DosCloseMutexSem( w->mutex );
  free( w );

  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  switch( msg )
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->buffersize          = info->audio_buffersize;
      w->metadata_buff       = info->metadata_buffer;
      w->metadata_buff[0]    = 0;
      w->metadata_size       = info->metadata_size;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
      strlcpy( w->filename, info->filename, sizeof( w->filename ));
      DosReleaseMutexSem( w->mutex );

      w->startpos   = info->start;
      w->endpos     = info->end;
      w->jumptopos  = info->jumpto;
      w->status     = DECODER_STARTING;
      w->stop       = FALSE;
      w->songlength = 0;
      w->seekable   = TRUE;
      // The libfaad2 library requires a big stack.
      w->decodertid = _beginthread( decoder_thread, 0, 262144, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;

      if( w->file ) {
        xio_fabort( w->file );
      }

      DosPostEventSem( w->play  );
      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_FFWD:
      DEBUGLOG(( "aacplay: DECODER_FFWD = %d\n", info->ffwd ));
      if( info->ffwd ) {
        if( w->decodertid == -1 ||
            w->filetype != FILE_MP4 ||
            xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST )
        {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->ffwd = info->ffwd;
      break;

    case DECODER_REW:
      DEBUGLOG(( "aacplay: DECODER_REW = %d\n", info->rew ));
      if( info->rew ) {
        if( w->decodertid == -1 ||
            w->filetype != FILE_MP4 ||
            xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST )
        {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->frew = info->rew;
      break;

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      DosPostEventSem( w->play );
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }

  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  if( w->seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else {
      return w->songlength;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

/* Returns a specified field of the given MPEG4 comment. */
static char*
mp4_get_string( DECODER_STRUCT* w, char* value, const char* type, int size )
{
  char* string;

  if( mp4ff_meta_find_by_name( w->mp4file, type, &string ) > 0 ) {
    ch_convert( CH_UTF_8, string, CH_DEFAULT, value, size );
    free( string );
  } else if( size ) {
    *value = 0;
  }

  return value;
}

/* Sets a specified field of the given MPEG4 comment. */
static int32_t
mp4_set_string( DECODER_STRUCT* w, const char* value, const char* type )
{
  char string[128*4];

  ch_convert( CH_DEFAULT, value, CH_UTF_8, string, sizeof( string ));
  return mp4ff_meta_set_by_name( w->mp4file, type, string, strlen( string ));
}

/* Removes a specified MPEG4 field. */
static int32_t
mp4_remove_string( DECODER_STRUCT* w, const char* type ) {
  return mp4ff_meta_delete( w->mp4file, type );
}

/* Guess image mimetype based on image header. */
static void
guess_mimetype( char* mimetype, char* data, int size )
{
  unsigned char sigs[5][2][16] = {{{ 3, 0x49, 0x49, 0x2a }, "image/tiff" },
                                  {{ 2, 0x42, 0x4D       }, "image/bmp"  },
                                  {{ 2, 0x42, 0x41       }, "image/bmp"  },
                                  {{ 3, 0x47, 0x49, 0x46 }, "image/gif"  },
                                  {{ 3, 0x89, 0x50, 0x4E }, "image/png"  }};
  int i, j;

  for( i = 0; i < 5; i++ ) {
    if( sigs[i][0][0] <= size )
    {
      BOOL found = TRUE;

      for( j = 1; j <= sigs[i][0][0]; j++ ) {
        if( data[j-1] != sigs[i][0][j] ) {
          found = FALSE;
          break;
        }
      }

      if( found ) {
        strcpy( mimetype, (char*)sigs[i][1] );
        return;
      }
    }
  }

  strcpy( mimetype, "image/jpeg" );
}

void static
copy_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* result, int size )
{
  ID3V2_FRAME* frame = NULL;

  if( !*result ) {
    if(( frame = id3v2_get_frame( tag, id, 1 )) != NULL ) {
      id3v2_get_string( frame, result, size );
    }
  }
}

void static
copy_id3v2_tag( DECODER_INFO* info, ID3V2_TAG* tag )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( tag ) {
    copy_id3v2_string( tag, ID3V2_TIT2, info->title,  sizeof( info->title  ));
    copy_id3v2_string( tag, ID3V2_TPE1, info->artist, sizeof( info->artist ));
    copy_id3v2_string( tag, ID3V2_TALB, info->album,  sizeof( info->album  ));
    copy_id3v2_string( tag, ID3V2_TCON, info->genre,  sizeof( info->genre  ));
    copy_id3v2_string( tag, ID3V2_TDRC, info->year,   sizeof( info->year   ));

    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        id3v2_get_string( frame, info->comment, sizeof( info->comment ) );
        break;
      }
    }

    // Disable Open Watcom C++ "&array may not produce intended result"
    #ifdef __WATCOMC__
    #pragma disable_message(7)
    #endif

    if( HAVE_FIELD( info, track ))
    {
      copy_id3v2_string( tag, ID3V2_TCOP, info->copyright, sizeof( info->copyright ));
      copy_id3v2_string( tag, ID3V2_TRCK, info->track, sizeof( info->track ));
      copy_id3v2_string( tag, ID3V2_TPOS, info->disc, sizeof( info->disc ));

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_RVA2, i )) != NULL ; i++ )
      {
        float gain = 0;
        unsigned char* data = (unsigned char*)frame->fr_data;

        // Format of RVA2 frame:
        //
        // Identification          <text string> $00
        // Type of channel         $xx
        // Volume adjustment       $xx xx
        // Bits representing peak  $xx
        // Peak volume             $xx (xx ...)

        id3v2_get_description( frame, buffer, sizeof( buffer ));

        // Skip identification string.
        data += strlen((char*)data ) + 1;
        // Search the master volume.
        while( data - (unsigned char*)frame->fr_data < frame->fr_size ) {
          if( *data == 0x01 ) {
            gain = (float)((signed char)data[1] << 8 | data[2] ) / 512;
            break;
          } else {
            data += 3 + (( data[3] + 7 ) / 8 );
          }
        }
        if( gain != 0 ) {
          if( stricmp( buffer, "album" ) == 0 ) {
            info->album_gain = gain;
          } else {
            info->track_gain = gain;
          }
        }
      }

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_TXXX, i )) != NULL ; i++ )
      {
        id3v2_get_description( frame, buffer, sizeof( buffer ));

        if( stricmp( buffer, "replaygain_album_gain" ) == 0 ) {
          info->album_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_album_peak" ) == 0 ) {
          info->album_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_gain" ) == 0 ) {
          info->track_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_peak" ) == 0 ) {
          info->track_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        }
      }
    }
  }
}

static void
replace_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* string )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( id == ID3V2_COMM ) {
    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        break;
      }
    }
  } else {
    frame = id3v2_get_frame( tag, id, 1 );
  }

  if( frame == NULL ) {
    frame = id3v2_add_frame( tag, id );
  }
  if( frame != NULL ) {
    id3v2_set_string( frame, string );
  }
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  ULONG rc = PLUGIN_OK;
  DECODER_STRUCT* w;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  if(( rc = aac_open_file( w, filename, "rb" )) != 0 ) {
    decoder_uninit( w );
    return rc == -1 ? PLUGIN_NO_PLAY : PLUGIN_NO_READ;
  }

  info->format.size       = sizeof( info->format );
  info->format.format     = WAVE_FORMAT_PCM;
  info->format.bits       = 16;
  info->format.channels   = w->channels;
  info->format.samplerate = w->samplerate;
  info->mode              = w->channels == 1 ? 3 : 0;
  info->songlength        = w->songlength;
  info->bitrate           = w->bitrate;
  info->junklength        = w->started;

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = xio_fsize64( w->file );
  }

  if( info->bitrate ) {
    snprintf( info->tech_info, sizeof( info->tech_info ), "AAC%s (%s) %u kbs, %.1f kHz, %s",
              PROFILE( w->profile ), FILE_TYPE( w->filetype ),
              info->bitrate, w->samplerate / 1000.0, w->channels == 1 ? "Mono" : "Stereo" );
  } else {
    snprintf( info->tech_info, sizeof( info->tech_info ), "AAC%s (%s) %.1f kHz, %s",
              PROFILE( w->profile ), FILE_TYPE( w->filetype ),
              w->samplerate / 1000.0, w->channels == 1 ? "Mono" : "Stereo" );
  }

  if( w->filetype == FILE_MP4 )
  {
    mp4_get_string( w, info->artist,  "artist",  sizeof( info->artist  ));
    mp4_get_string( w, info->album,   "album",   sizeof( info->album   ));
    mp4_get_string( w, info->title,   "title",   sizeof( info->title   ));
    mp4_get_string( w, info->genre,   "genre",   sizeof( info->genre   ));
    mp4_get_string( w, info->year,    "date" ,   sizeof( info->year    ));
    mp4_get_string( w, info->comment, "comment", sizeof( info->comment ));

    if( HAVE_FIELD( info, track ))
    {
      char buffer[128];

      mp4_get_string( w, info->copyright, "copyright", sizeof( info->copyright ));
      mp4_get_string( w, info->disc,  "disc",  sizeof( info->disc  ));
      mp4_get_string( w, info->track, "track", sizeof( info->track ));

      mp4_get_string( w, buffer, "replaygain_album_gain", sizeof( buffer ));
      info->album_gain = atof( buffer );
      mp4_get_string( w, buffer, "replaygain_album_peak", sizeof( buffer ));
      info->album_peak = atof( buffer );
      mp4_get_string( w, buffer, "replaygain_track_gain", sizeof( buffer ));
      info->track_gain = atof( buffer );
      mp4_get_string( w, buffer, "replaygain_track_peak", sizeof( buffer ));
      info->track_peak = atof( buffer );

      info->saveinfo = is_file( filename );
      info->haveinfo = DECODER_HAVE_TITLE      |
                       DECODER_HAVE_ARTIST     |
                       DECODER_HAVE_ALBUM      |
                       DECODER_HAVE_YEAR       |
                       DECODER_HAVE_GENRE      |
                       DECODER_HAVE_TRACK      |
                       DECODER_HAVE_DISC       |
                       DECODER_HAVE_COMMENT    |
                       DECODER_HAVE_COPYRIGHT  |
                       DECODER_HAVE_TRACK_GAIN |
                       DECODER_HAVE_TRACK_PEAK |
                       DECODER_HAVE_ALBUM_GAIN |
                       DECODER_HAVE_ALBUM_PEAK |
                       DECODER_HAVE_PICS       |
                       DECODER_PICS_COVERFRONT;

      info->codepage = ch_default();
    }
    if( HAVE_FIELD( info, pics_count ))
    {
      char* data;
      int   size = mp4ff_meta_find_by_name( w->mp4file, "cover", &data );

      if( size ) {
        if(( info->pics = (APIC**)info->memalloc( sizeof(APIC*))) != NULL ) {
          if(( info->pics[0] = (APIC*)info->memalloc( size + sizeof(APIC))) != NULL ) {
            APIC* pic = info->pics[0];

            pic->size = size + sizeof( APIC );
            pic->type = PIC_COVERFRONT;
            guess_mimetype( pic->mimetype, data, size );
            memcpy((char*)pic + sizeof( APIC ), data, size );

            DEBUGLOG(( "aacplay: load APIC type %d, %s, size %d\n",
                        pic->type, pic->mimetype, size ));

            info->pics_count = 1;
          }
        }
        free( data );
      }
      DEBUGLOG(( "aacplay: found %d attached pictures\n", info->pics_count ));
    }
  }
  else
  {
    xio_get_metainfo( w->file, XIO_META_GENRE, info->genre,   sizeof( info->genre   ));
    xio_get_metainfo( w->file, XIO_META_NAME,  info->comment, sizeof( info->comment ));
    xio_get_metainfo( w->file, XIO_META_TITLE, info->title,   sizeof( info->title   ));

    if( w->tagv2 ) {
      copy_id3v2_tag( info, w->tagv2 );
    }

    if( HAVE_FIELD( info, pics_count ))
    {
      ID3V2_FRAME* frame;
      int i, j, count = 0;

      if( w->tagv2 )
      {
        for( i = 1; ( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, i )) != NULL ; i++ ) {
          ++count;
        }

        if( count  && ( options & DECODER_LOADPICS )) {
          if(( info->pics = info->memalloc( sizeof( APIC* ) * count )) != NULL ) {
            for( i = 1, j = 0; ( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, i )) != NULL ; i++ )
            {
              int   size = sizeof( APIC ) + id3v2_picture_size( frame );
              APIC* pic;

              if(( id3v2_picture_size( frame ) > frame->fr_size  ) ||
                 ( info->pics[j] = info->memalloc( size )) == NULL )
              {
                count = j;
                break;
              }

              pic = info->pics[j];

              pic->size = size;
              pic->type = id3v2_picture_type( frame );

              id3v2_get_mime_type( frame, pic->mimetype, sizeof( pic->mimetype ));
              id3v2_get_description( frame, pic->desc, sizeof( pic->desc ));

              DEBUGLOG(( "aacplay: load APIC type %d, %s, '%s', size %d\n",
                          pic->type, pic->mimetype, pic->desc, id3v2_picture_size( frame )));

              memcpy((char*)pic + sizeof( APIC ),
                     id3v2_picture_data_ptr( frame ), id3v2_picture_size( frame ));
              ++j;
            }
          }
        }
      }

      info->pics_count = count;
      DEBUGLOG(( "aacplay: found %d attached pictures\n", info->pics_count ));
    }

    if( HAVE_FIELD( info, saveinfo ))
    {
      info->saveinfo = is_file( filename );
      info->codepage = ch_default();
      info->haveinfo = DECODER_HAVE_TITLE      |
                       DECODER_HAVE_ARTIST     |
                       DECODER_HAVE_ALBUM      |
                       DECODER_HAVE_YEAR       |
                       DECODER_HAVE_GENRE      |
                       DECODER_HAVE_TRACK      |
                       DECODER_HAVE_DISC       |
                       DECODER_HAVE_COMMENT    |
                       DECODER_HAVE_COPYRIGHT  |
                       DECODER_HAVE_TRACK_GAIN |
                       DECODER_HAVE_TRACK_PEAK |
                       DECODER_HAVE_ALBUM_GAIN |
                       DECODER_HAVE_ALBUM_PEAK |
                       DECODER_HAVE_PICS       |
                       DECODER_PICS_ANYTYPE;
    }
  }

  aac_close( w );
  decoder_uninit( w );
  return rc;
}

/* It is called if it is necessary to change the meta information
   for the specified file. */
ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  ULONG rc = PLUGIN_OK;
  DECODER_STRUCT* w;
  char buffer[64];
  int  i;

  if( !*filename ) {
    return EINVAL;
  }

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return errno;
  }

  if(( rc = aac_open_file( w, filename, "r+b" )) != 0 ) {
    DEBUGLOG(( "aacplay: unable open file for saving %s, rc=%d\n", filename, rc ));
    decoder_uninit( w );
    return rc;
  }

  if( w->filetype == FILE_MP4 )
  {
    mp4_set_string( w, info->artist,    "artist"    );
    mp4_set_string( w, info->album,     "album"     );
    mp4_set_string( w, info->title,     "title"     );
    mp4_set_string( w, info->genre,     "genre"     );
    mp4_set_string( w, info->year,      "date"      );
    mp4_set_string( w, info->comment,   "comment"   );
    mp4_set_string( w, info->track,     "track"     );
    mp4_set_string( w, info->disc,      "disc"      );
    mp4_set_string( w, info->copyright, "copyright" );

    if( HAVE_FIELD( info, album_gain ))
    {
      if( info->album_gain ) {
        sprintf( buffer, "%.2f dB", info->album_gain );
        mp4_set_string( w, buffer, "replaygain_album_gain" );
      } else {
        mp4_remove_string( w, "replaygain_album_gain" );
      }

      if( info->album_peak ) {
        sprintf( buffer, "%.6f", info->album_peak );
        mp4_set_string( w, buffer, "replaygain_album_peak" );
      } else {
        mp4_remove_string( w, "replaygain_album_peak" );
      }

      if( info->track_gain ) {
        sprintf( buffer, "%.2f dB", info->track_gain );
        mp4_set_string( w, buffer, "replaygain_track_gain" );
      } else {
        mp4_remove_string( w, "replaygain_track_gain" );
      }

      if( info->track_peak ) {
        sprintf( buffer, "%.6f", info->track_peak );
        mp4_set_string( w, buffer, "replaygain_track_peak" );
      } else {
        mp4_remove_string( w, "replaygain_track_peak" );
      }

      if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS )) {
        while( mp4_remove_string( w, "cover" )) {}
        for( i = 0; i < info->pics_count; i++ ) {
          APIC* pic = info->pics[i];
          mp4ff_meta_set_by_name( w->mp4file, "cover", (char*)pic + sizeof( APIC ), pic->size - sizeof( APIC ));
        }
      }
    }
    if( !mp4ff_meta_save( w->mp4file )) {
      DEBUGLOG(( "aacplay: unable save metadata %s\n", filename ));
      rc = EINVAL;
    }
  }
  else
  {
    BOOL copy = FALSE;
    char savename[_MAX_PATH];
    int64_t started;

    strlcpy( savename, filename, sizeof( savename ));
    savename[ strlen( savename ) - 1 ] = '~';

    if( !w->tagv2 ) {
      w->tagv2 = id3v2_new_tag();
    }

    if( w->tagv2->id3_started == 0 )
    {
      int set_rc;

      // FIX ME: Replay gain info also must be saved.

      replace_id3v2_string( w->tagv2, ID3V2_TIT2, info->title     );
      replace_id3v2_string( w->tagv2, ID3V2_TPE1, info->artist    );
      replace_id3v2_string( w->tagv2, ID3V2_TALB, info->album     );
      replace_id3v2_string( w->tagv2, ID3V2_TDRC, info->year      );
      replace_id3v2_string( w->tagv2, ID3V2_COMM, info->comment   );
      replace_id3v2_string( w->tagv2, ID3V2_TCON, info->genre     );
      replace_id3v2_string( w->tagv2, ID3V2_TCOP, info->copyright );
      replace_id3v2_string( w->tagv2, ID3V2_TRCK, info->track     );
      replace_id3v2_string( w->tagv2, ID3V2_TPOS, info->disc      );

      if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS ))
      {
        ID3V2_FRAME* frame;

        while(( frame = id3v2_get_frame( w->tagv2, ID3V2_APIC, 1 )) != NULL ) {
          id3v2_delete_frame( frame );
        }
        for( i = 0; i < info->pics_count; i++ ) {
          if(( frame = id3v2_add_frame( w->tagv2, ID3V2_APIC )) != NULL )
          {
            APIC* pic = info->pics[i];

            id3v2_set_picture( frame, pic->type, pic->mimetype, pic->desc,
                               (char*)pic + sizeof(APIC), pic->size - sizeof(APIC));
          }
        }
      }

      xio_rewind( w->file );
      set_rc = id3v2_set_tag( w->file, w->tagv2, savename );

      if( set_rc == ID3V2_COPIED ) {
        copy = TRUE;
      } else if( set_rc == ID3V2_ERROR ) {
        rc = xio_errno();
        DEBUGLOG(( "aacplay: unable save id3v2 tag, rc=%d\n", rc ));
      }

      aac_close( w );

      if( rc == PLUGIN_OK && copy )
      {
        if(( rc = aac_open_file( w, savename, "rb" )) != 0 ) {
          decoder_uninit( w );
          return rc;
        } else {
          // Remember a new position of the beginning of the data stream.
          started = w->started;
          aac_close( w );
          decoder_uninit( w );
        }

        // Suspend all active instances of the updated file.
        DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );

        for( i = 0; i < instances_count; i++ )
        {
          w = instances[i];
          DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

          if( nlstricmp( w->filename, filename ) == 0 && w->file ) {
            DEBUGLOG(( "aacplay: suspend currently used file: %s\n", w->filename ));
            w->resumepos = xio_ftell64( w->file ) - w->started;
            xio_fclose( w->file );
          } else {
            w->resumepos = -1;
          }
        }

        // Preserve EAs.
        eacopy( filename, savename );

        // Replace file.
        if( remove( filename ) == 0 ) {
          if( rename( savename, filename ) != 0 ) {
            rc = errno;
          }
        } else {
          rc = errno;
          remove( savename );
        }

        // Resume all suspended instances of the updated file.
        for( i = 0; i < instances_count; i++ )
        {
          w = instances[i];
          if( w->resumepos != -1 ) {
            DEBUGLOG(( "aacplay: resumes currently used file: %s\n", w->filename ));
            if(( w->file = xio_fopen( w->filename, "rb" )) != NULL ) {
              xio_fseek64( w->file, w->resumepos + started, XIO_SEEK_SET );
              w->started = started;
            }
          }
          DosReleaseMutexSem( w->mutex );
        }

        DosReleaseMutexSem( mutex );
        return rc;
      }
    }
  }

  aac_close( w );
  decoder_uninit( w );
  return rc;
}

/* Unsupported. */
ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

/* Unsupported. */
ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

/* What can be played via the decoder. */
ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 7 ) {
      strcpy( ext[0], "*.AAC" );
      strcpy( ext[1], "*.M4A" );
      strcpy( ext[2], "*.M4B" );
      strcpy( ext[3], "*.MP4" );
      strcpy( ext[4], "audio/x-aac" );
      strcpy( ext[5], "audio/aac" );
      strcpy( ext[6], "audio/aacp" );
    }
    *size = 7;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_SAVEINFO | DECODER_STREAM;
}

static void
init_tag( void )
{
  id3v2_set_read_charset( cfg.tag_read_id3v2_charset );
  id3v2_set_save_charset( cfg.tag_save_id3v2_charset );
}

/* Saves plug-in's settings. */
static void
save_ini( void )
{
  HINI hini;
  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, cfg.tag_read_id3v2_charset );
    save_ini_value( hini, cfg.tag_save_id3v2_charset );
    close_ini( hini );
  }
}

/* Loads plug-in's settings. */
static void
load_ini( void )
{
  HINI hini;

  if( ch_default() == CH_CYR_OS2 ) {
    cfg.tag_read_id3v2_charset = CH_CYR_WIN;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
  } else {
    cfg.tag_read_id3v2_charset = CH_DEFAULT;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
  }

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, cfg.tag_read_id3v2_charset );
    load_ini_value( hini, cfg.tag_save_id3v2_charset );
    close_ini( hini );
  }

  init_tag();
}

/* Processes messages of the configuration dialog. */
static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
    {
      int i;

      for( i = 0; i < ch_list_size; i++ ) {
        lb_add_with_handle( hwnd, CB_ID3V2_RDCH, ch_list[i].name, ch_list[i].id );
      }

      for( i = 0; i < ch_list_size; i++ ) {
        // All except for auto detection and UCS-2 LE.
        if( ch_list[i].id >= 0 && ch_list[i].id != CH_UCS_2LE ) {
          lb_add_with_handle( hwnd, CB_ID3V2_WRCH, ch_list[i].name, ch_list[i].id );
        }
      }

      do_warpsans( hwnd );
      // continue to WM_UPDATE_CONTROLS...
    }

    case WM_UPDATE_CONTROLS:
    {
      lb_select_by_handle( hwnd, CB_ID3V2_RDCH, cfg.tag_read_id3v2_charset );
      lb_select_by_handle( hwnd, CB_ID3V2_WRCH, cfg.tag_save_id3v2_charset );
      return 0;
    }

    case WM_DESTROY:
    {
      int  i;

      if(( i = lb_cursored( hwnd, CB_ID3V2_RDCH )) != LIT_NONE ) {
        cfg.tag_read_id3v2_charset = (int)lb_get_handle( hwnd, CB_ID3V2_RDCH, i );
      }
      if(( i = lb_cursored( hwnd, CB_ID3V2_WRCH )) != LIT_NONE ) {
        cfg.tag_save_id3v2_charset = (int)lb_get_handle( hwnd, CB_ID3V2_WRCH, i );
      }

      save_ini();
      init_tag();
      break;
    }

    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case PB_UNDO:
          WinSendMsg( hwnd, WM_UPDATE_CONTROLS, 0, 0 );
          return 0;

        case PB_DEFAULT:
        {
          if( ch_default() == CH_CYR_OS2 ) {
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_CYR_WIN  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          } else {
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_DEFAULT  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          }

          return 0;
        }
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in. */
void DLLENTRY
plugin_configure( HWND hwnd, HMODULE module ) {
  WinDlgBox( HWND_DESKTOP, hwnd, cfg_dlg_proc, module, DLG_CONFIGURE, NULL );
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "AAC Decoder " VER_STRING;
  param->configurable = TRUE;

  load_ini();
}

int INIT_ATTRIBUTE
__dll_initialize( void )
{
  if( DosCreateMutexSem( NULL, &mutex, 0, FALSE ) != NO_ERROR ) {
    return 0;
  }

  return 1;
}

int TERM_ATTRIBUTE
__dll_terminate( void )
{
  free( instances );
  DosCloseMutexSem( mutex );
  return 1;
}

#if defined(__IBMC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    return __dll_initialize();
  } else if( flag == DLL_PROCESS_DETACH ) {
    __dll_terminate();
    #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
    #endif
    _CRT_term();
  }
  return 1UL;
}
#endif

