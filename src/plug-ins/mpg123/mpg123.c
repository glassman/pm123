/*
 * Copyright 2007-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_PM
#define  INCL_DOS
#define  INCL_ERRORS
#include <os2.h>

#include <stdio.h>
#include <malloc.h>
#include <process.h>
#include <errno.h>

#include "mpg123.h"

#include <utilfct.h>
#include <output_plug.h>
#include <decoder_plug.h>
#include <debuglog.h>
#include <id3v1.h>
#include <id3v2.h>

static DECODER_STRUCT** instances = NULL;
static int  instances_count = 0;
static HMTX mutex;
static DECODER_SETTINGS cfg;

#define WM_UPDATE_CONTROLS ( WM_USER + 1 )

/* Open MPEG file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static int
plg_open_file( DECODER_STRUCT* w, const char* filename, const char* mode )
{
  int rc;
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->filename != filename ) {
    strlcpy( w->filename, filename, sizeof( w->filename ));
  }

  if(( rc = mpg_open_file( &w->mpeg, w->filename, mode )) == 0 ) {

    if( w->hwnd ) {
      xio_set_observer( w->mpeg.file, w->hwnd, w->metadata_buff, w->metadata_size );
    }

    w->output_format.samplerate = w->mpeg.samplerate;
    w->bitrate = w->mpeg.bitrate;
  }

  DosReleaseMutexSem( w->mutex );
  return rc;
}

/* Closes the MPEG file. */
static void
plg_close_file( DECODER_STRUCT* w )
{
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  mpg_close( &w->mpeg );
  DosReleaseMutexSem( w->mutex );
}

/* Changes the current file position to a new time within the file.
   Returns 0 if it successfully moves the pointer. A nonzero return
   value indicates an error. On devices that cannot seek the return
   value is nonzero. */
static int
seek_to( DECODER_STRUCT* w, int ms, int origin )
{
  int rc;

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  rc = mpg_seek_ms( &w->mpeg, ms, origin );
  DosReleaseMutexSem( w->mutex );
  return rc;
}

/* Reads a next valid frame and decodes it. Returns 0 if it
   successfully reads a frame, or -1 if any errors were detected. */
static int
read_and_save_frame( DECODER_STRUCT* w )
{
  char errorbuf[1024];
  int  pos;

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( mpg_read_frame( &w->mpeg ) != 0 ) {
    DosReleaseMutexSem( w->mutex );
    return -1;
  }

  pos = mpg_tell_ms( &w->mpeg );

  if(( w->startpos && pos <  w->startpos ) ||
     ( w->endpos   && pos >= w->endpos   ))
  {
    DosReleaseMutexSem( w->mutex );
    return -1;
  }

  if( w->posmarker < 0 ) {
    w->posmarker = pos - w->startpos;
  }

  if( !w->save && *w->savename ) {
    // Opening a save stream.
    if(!( w->save = xio_fopen( w->savename, "ab" )))
    {
      strlcpy( errorbuf, "Could not open file to save data:\n", sizeof( errorbuf ));
      strlcat( errorbuf, w->savename, sizeof( errorbuf ));
      strlcat( errorbuf, "\n", sizeof( errorbuf ));
      strlcat( errorbuf, xio_strerror( xio_errno()), sizeof( errorbuf ));
      w->info_display( errorbuf );
      w->savename[0] = 0;
    }
  } else if( w->save && !*w->savename  ) {
    // Closing a save stream.
    xio_fclose( w->save );
    w->save = NULL;
  }

  if( w->save ) {
    if( mpg_save_frame( &w->mpeg, w->save ) != 0 )
    {
      strlcpy( errorbuf, "Error writing to stream save file:\n", sizeof( errorbuf ));
      strlcat( errorbuf, w->savename, sizeof( errorbuf ));
      strlcat( errorbuf, "\n", sizeof( errorbuf ));
      strlcat( errorbuf, xio_strerror( xio_errno()), sizeof( errorbuf ));

      xio_fclose( w->save );
      w->save = NULL;
      w->savename[0] = 0;
      w->info_display( errorbuf );
    }
  }

  mpg_decode_frame( &w->mpeg );

  if( w->mpeg.fr.bitrate != w->bitrate )
  {
    w->bitrate = w->mpeg.fr.bitrate;
    WinPostMsg( w->hwnd, WM_CHANGEBR, MPFROMLONG( w->bitrate ), 0 );
  }

  DosReleaseMutexSem( w->mutex );
  return 0;
}

/* Flushes currently decoded samples. */
static BOOL
flush_output( DECODER_STRUCT* w, unsigned char* buffer, int* size, BOOL force )
{
  if( !*size ) {
    return TRUE;
  }

  if( w->mpeg.is_starts )
  {
    // If this is a first frame, samples which are added at start
    // by encoder (encoder delay) must be skipped. There must also be
    // skipped 528 samples added by the decoder (decoder delay).
    int skip = (w->mpeg.xing_header.delay + 528) * w->mpeg.fr.channels * 2;

    if( !skip ) {
      // If the delay is not specified in file header, find the first
      // non-gap sample, and adjust delay accordingly.
      if( w->mpeg.fr.channels == 1 ) {
        while( *size > skip && skip < MAXFRAMESIZE ) {
          if( abs(((short*)(buffer+skip))[0]) <= NOISE_GATE_LEVEL ) {
            skip += 2;
          } else {
            break;
          }
        }
      } else {
        while( *size > skip && skip < MAXFRAMESIZE ) {
          if( abs(((short*)(buffer+skip))[0]) <= NOISE_GATE_LEVEL &&
              abs(((short*)(buffer+skip))[1]) <= NOISE_GATE_LEVEL )
          {
            skip += 4;
          } else {
            break;
          }
        }
      }
    } else if( skip > *size ) {
      skip = *size;
    }

    if( skip ) {
      DEBUGLOG(( "mpg123: skip a %d silence bytes from the %d beginning.\n", skip, *size ));
      memmove( buffer, buffer + skip, *size - skip );
      *size -= skip;
    }
  }

  if( *size < w->audio_buffersize && !w->mpeg.is_ended && !force ) {
    return TRUE;
  }

  if( w->mpeg.is_ended )
  {
    // If this is a last frame, samples which are added at end
    // by encoder (encoder padding) must be skipped. We must also remember
    // that the decoder will eat the last 528 samples. The true way - fix that,
    // but for now just reduce the encoder padding.
    int skip = (w->mpeg.xing_header.padding-528) * w->mpeg.fr.channels * 2;

    if( !skip ) {
      // If the padding is not specified in file header, find the first
      // non-gap sample, and adjust padding accordingly.
      if( w->mpeg.fr.channels == 1 ) {
        while( *size > skip && skip < 2 * MAXFRAMESIZE ) {
          if( abs(((short*)(buffer+*size-skip-3))[0]) <= NOISE_GATE_LEVEL ) {
            skip += 2;
          } else {
            break;
          }
        }
      } else {
        while( *size > skip && skip < 2 * MAXFRAMESIZE ) {
          if( abs(((short*)(buffer+*size-skip-5))[ 0]) <= NOISE_GATE_LEVEL &&
              abs(((short*)(buffer+*size-skip-5))[-1]) <= NOISE_GATE_LEVEL )
          {
            skip += 4;
          } else {
            break;
          }
        }
      }
    } else if( skip > *size ) {
      skip = *size;
    }

    if( skip > 0 ) {
      DEBUGLOG(( "mpg123: skip a %d silence bytes from last %d.\n", skip, *size ));
      *size -= skip;
    }
  }

  while( *size )
  {
    int chunk = min( *size, w->audio_buffersize );

    DEBUGLOG2(( "mpg123: flush %d from %d bytes, bof = %d, eof = %d\n",
                chunk, *size, w->mpeg.is_starts, w->mpeg.is_ended ));

    if( w->output_play_samples( w->a, &w->output_format, (char*)buffer, chunk, w->posmarker ) != chunk ) {
      WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
      return FALSE;
    }

    *size -= chunk;
    memmove( buffer, buffer + chunk, *size );
    w->mpeg.is_starts = 0;

    if( !w->mpeg.is_ended && !force  ) {
      break;
    }
  }

  w->posmarker = -1;
  return TRUE;
}

/* Notify an error. */
static void
show_error( DECODER_STRUCT* w, int style, const char* msg, int rc )
{
  char buff[1024];

  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );

  if( rc == -1 ) {
    strlcat( buff, "Unsupported format of the file.", sizeof( buff ));
  } else if( rc ) {
    strlcat( buff, xio_strerror( rc ), sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  unsigned char*  buffer = NULL;

  int   bufsize = w->audio_buffersize + ( 3 * MAXFRAMESIZE / 4 * 4 );
  int   bufpos  = 0;
  int   done;
  ULONG resetcount;
  int   rc;

  if(( rc = plg_open_file( w, w->filename, "rb" )) != 0 ) {
    show_error( w, MB_ERROR, "Unable open file", rc );
    goto end;
  }

  if(!( buffer = malloc( bufsize ))) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  w->frew = FALSE;
  w->ffwd = FALSE;
  w->stop = FALSE;

  // After opening a new file we so are in its beginning.
  if( w->jumptopos == 0 && w->startpos == 0 ) {
      w->jumptopos = -1;
  }

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->mpeg.file );

    while( !w->stop )
    {
      if( w->jumptopos >= 0 ) {
        DEBUGLOG(( "mpg123: jump to %d ms\n", w->jumptopos + w->startpos ));
        rc = seek_to( w, w->jumptopos + w->startpos, MPG_SEEK_SET );
        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        if( rc == -1 ) {
          show_error( w, MB_WARNING, "Unable seek in file", xio_errno());
        }

        bufpos = 0;
        w->posmarker = -1;
        w->jumptopos = -1;
      }

      if( read_and_save_frame( w ) != 0 ) {
        if( xio_ferror( w->mpeg.file )) {
          show_error( w, MB_ERROR, "Unable read file", xio_errno());
          goto end;
        } else {
          break;
        }
      }

      if( w->frew || w->ffwd )
      {
        // The skip of this interval will force the decoder to be
        // accelerated approximately in 10 times.
        int skip = w->mpeg.pcm_count * 1000    /
                   w->output_format.samplerate /
                   w->output_format.channels   / ( w->output_format.bits / 8 );

        if( skip ) {
          if( w->frew ) {
            if( seek_to( w, -11 * skip, MPG_SEEK_CUR ) != 0 ) {
              break;
            }
          } else if( w->ffwd ) {
            seek_to( w, 9 * skip, MPG_SEEK_CUR );
          }
        }
      }

      while(( done = mpg_move_sound( &w->mpeg, buffer + bufpos, bufsize - bufpos )) > 0 )
      {
        bufpos += done;

        if( bufpos == bufsize ) {
          if( !flush_output( w, buffer, &bufpos, FALSE )) {
            w->stop = TRUE;
          }
        }
      }
    }

    if( !w->stop ) {
      flush_output( w, buffer, &bufpos, TRUE );
    }

    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

  w->status = DECODER_STOPPED;

end:

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  plg_close_file( w );

  if( w->save ) {
    xio_fclose( w->save );
    w->save = NULL;
  }
  if( buffer ) {
    free( buffer );
  }

  w->decodertid = -1;
  DosReleaseMutexSem( w->mutex );
  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );
  DosCreateMutexSem( NULL, &w->mutex, 0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  instances = realloc( instances, ++instances_count * sizeof( DECODER_STRUCT* ));
  instances[ instances_count - 1 ] = w;
  DosReleaseMutexSem( mutex );

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than yours is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = arg;
  int i;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  decoder_command( w, DECODER_STOP, NULL );

  for( i = 0; i < instances_count; i++ ) {
    if( instances[i] == w ) {
      if( i < instances_count - 1 ) {
        memmove( instances + i, instances + i + 1,
               ( instances_count - i - 1 ) * sizeof( DECODER_STRUCT* ));
      }
      instances = realloc( instances, --instances_count * sizeof( DECODER_STRUCT* ));
    }
  }

  DosReleaseMutexSem( mutex  );
  DosCloseEventSem( w->play  );
  DosCloseMutexSem( w->mutex );
  free( w );

  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = arg;

  switch( msg )
  {
    case DECODER_SETUP:

      w->output_format.size     = sizeof( w->output_format );
      w->output_format.format   = WAVE_FORMAT_PCM;
      w->output_format.bits     = 16;
      w->output_format.channels = 2;

      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->output_play_samples = info->output_play_samples;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      w->metadata_buff       = info->metadata_buffer;
      w->metadata_buff[0]    = 0;
      w->metadata_size       = info->metadata_size;
      w->savename[0]         = 0;

      mpg_init( &w->mpeg, cfg.mmx_enable );
      break;

    case DECODER_PLAY:
    {
      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

      if( w->decodertid != -1 ) {
        DosReleaseMutexSem( w->mutex );
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
          DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      strlcpy( w->filename, info->filename, sizeof( w->filename ));

      w->status     = DECODER_STARTING;
      w->startpos   = info->start;
      w->endpos     = info->end;
      w->jumptopos  = info->jumpto;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosReleaseMutexSem( w->mutex );
      DosPostEventSem( w->play );
      break;
    }

    case DECODER_STOP:
    {
      // Don't use w->mutex here because any operation can be interrupted.
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;
      mpg_abort( &w->mpeg );
      DosPostEventSem( w->play  );
      wait_thread( w->decodertid, 5000 );

      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_FFWD:
      if( info->ffwd ) {
        if( w->decodertid == -1 || w->mpeg.is_stream ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->ffwd = info->ffwd;
      break;

    case DECODER_REW:
      if( info->rew ) {
        if( w->decodertid == -1 || w->mpeg.is_stream ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->frew = info->rew;
      break;

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;

      if( w->status == DECODER_STOPPED ) {
        DosPostEventSem( w->play );
      }
      break;

    case DECODER_SAVEDATA:
      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
      if( info->save_filename == NULL ) {
        w->savename[0] = 0;
      } else {
        strlcpy( w->savename, info->save_filename, sizeof( w->savename ));
      }
      DosReleaseMutexSem( w->mutex );
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }
  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = arg;

  if( w->mpeg.seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else {
      return w->mpeg.songlength;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

void static
copy_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* result, int size )
{
  ID3V2_FRAME* frame = NULL;

  if( !*result ) {
    if(( frame = id3v2_get_frame( tag, id, 1 )) != NULL ) {
      id3v2_get_string( frame, result, size );
    }
  }
}

void static
copy_id3v2_tag( DECODER_INFO* info, ID3V2_TAG* tag )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( tag ) {
    copy_id3v2_string( tag, ID3V2_TIT2, info->title,   sizeof( info->title   ));
    copy_id3v2_string( tag, ID3V2_TPE1, info->artist,  sizeof( info->artist  ));
    copy_id3v2_string( tag, ID3V2_TALB, info->album,   sizeof( info->album   ));
    copy_id3v2_string( tag, ID3V2_TCON, info->genre,   sizeof( info->genre   ));
    copy_id3v2_string( tag, ID3V2_TDRC, info->year,    sizeof( info->year    ));

    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        id3v2_get_string( frame, info->comment, sizeof( info->comment ) );
        break;
      }
    }

    if( HAVE_FIELD( info, track ))
    {
      copy_id3v2_string( tag, ID3V2_TCOP, info->copyright, sizeof( info->copyright ));
      copy_id3v2_string( tag, ID3V2_TRCK, info->track, sizeof( info->track ));
      copy_id3v2_string( tag, ID3V2_TPOS, info->disc, sizeof( info->disc ));

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_RVA2, i )) != NULL ; i++ )
      {
        float gain = 0;
        unsigned char* data = (unsigned char*)frame->fr_data;

        // Format of RVA2 frame:
        //
        // Identification          <text string> $00
        // Type of channel         $xx
        // Volume adjustment       $xx xx
        // Bits representing peak  $xx
        // Peak volume             $xx (xx ...)

        id3v2_get_description( frame, buffer, sizeof( buffer ));

        // Skip identification string.
        data += strlen((char*)data ) + 1;
        // Search the master volume.
        while( data - (unsigned char*)frame->fr_data < frame->fr_size ) {
          if( *data == 0x01 ) {
            gain = (float)((signed char)data[1] << 8 | data[2] ) / 512;
            break;
          } else {
            data += 3 + (( data[3] + 7 ) / 8 );
          }
        }
        if( gain != 0 ) {
          if( stricmp( buffer, "album" ) == 0 ) {
            info->album_gain = gain;
          } else {
            info->track_gain = gain;
          }
        }
      }

      for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_TXXX, i )) != NULL ; i++ )
      {
        id3v2_get_description( frame, buffer, sizeof( buffer ));

        if( stricmp( buffer, "replaygain_album_gain" ) == 0 ) {
          info->album_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_album_peak" ) == 0 ) {
          info->album_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_gain" ) == 0 ) {
          info->track_gain = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        } else if( stricmp( buffer, "replaygain_track_peak" ) == 0 ) {
          info->track_peak = atof( id3v2_get_string( frame, buffer, sizeof( buffer )));
        }
      }
    }
  }
}

void static
copy_id3v1_string( ID3V1_TAG* tag, int id, char* result, int size )
{
  if( !*result ) {
    id3v1_get_string( tag, id, result, size );
  }
}

void static
copy_id3v1_tag( DECODER_INFO* info, ID3V1_TAG* tag )
{
  copy_id3v1_string( tag, ID3V1_TITLE,   info->title,   sizeof( info->title   ));
  copy_id3v1_string( tag, ID3V1_ARTIST,  info->artist,  sizeof( info->artist  ));
  copy_id3v1_string( tag, ID3V1_ALBUM,   info->album,   sizeof( info->album   ));
  copy_id3v1_string( tag, ID3V1_YEAR,    info->year,    sizeof( info->year    ));
  copy_id3v1_string( tag, ID3V1_COMMENT, info->comment, sizeof( info->comment ));
  copy_id3v1_string( tag, ID3V1_GENRE,   info->genre,   sizeof( info->genre   ));

  if( HAVE_FIELD( info, track )) {
    copy_id3v1_string( tag, ID3V1_TRACK, info->track,   sizeof( info->track   ));
  }
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  int rc = PLUGIN_OK;
  DECODER_STRUCT* w;
  ID3V2_FRAME* frame;
  int count = 0;
  int i, j;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  if(( rc = plg_open_file( w, filename, "rb" )) != 0 ) {
    decoder_uninit( w );
    return rc == -1 ? PLUGIN_NO_PLAY : PLUGIN_NO_READ;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  info->format.size       = sizeof( info->format );
  info->format.format     = WAVE_FORMAT_PCM;
  info->format.bits       = 16;
  info->format.channels   = 2;
  info->format.samplerate = w->mpeg.samplerate;
  info->mpeg              = w->mpeg.fr.mpeg25 ? 25 : ( w->mpeg.fr.lsf ? 20 : 10 );
  info->layer             = w->mpeg.fr.layer;
  info->mode              = w->mpeg.fr.mode;
  info->modext            = w->mpeg.fr.mode_ext;
  info->bpf               = w->mpeg.fr.framesize + 4;
  info->bitrate           = w->mpeg.bitrate;
  info->extention         = w->mpeg.fr.extension;
  info->junklength        = w->mpeg.started;
  info->songlength        = w->mpeg.songlength;

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = w->mpeg.filesize;
  }

  sprintf( info->tech_info, "%u kbs, %.1f kHz, %s",
           info->bitrate, ( info->format.samplerate / 1000.0 ), modes( info->mode ));

  if( w->mpeg.xing_header.flags & TOC_FLAG ) {
    strcat( info->tech_info, ", Xing" );
  }

  xio_get_metainfo( w->mpeg.file, XIO_META_GENRE, info->genre,   sizeof( info->genre   ));
  xio_get_metainfo( w->mpeg.file, XIO_META_NAME,  info->comment, sizeof( info->comment ));
  xio_get_metainfo( w->mpeg.file, XIO_META_TITLE, info->title,   sizeof( info->title   ));

  switch( cfg.tag_read_type ) {
    case TAG_READ_ID3V2_AND_ID3V1:
      copy_id3v2_tag( info,  w->mpeg.tagv2 );
      copy_id3v1_tag( info, &w->mpeg.tagv1 );
      break;
    case TAG_READ_ID3V1_AND_ID3V2:
      copy_id3v1_tag( info, &w->mpeg.tagv1 );
      copy_id3v2_tag( info,  w->mpeg.tagv2 );
      break;
    case TAG_READ_ID3V2_ONLY:
      copy_id3v2_tag( info,  w->mpeg.tagv2 );
      break;
    case TAG_READ_ID3V1_ONLY:
      copy_id3v1_tag( info, &w->mpeg.tagv1 );
      break;
  }

  if( HAVE_FIELD( info, pics_count )) {
    if(( cfg.tag_read_type != TAG_READ_ID3V1_ONLY ) && w->mpeg.tagv2 )
    {
      for( i = 1; ( frame = id3v2_get_frame( w->mpeg.tagv2, ID3V2_APIC, i )) != NULL ; i++ ) {
        ++count;
      }

      if( count && ( options & DECODER_LOADPICS )) {
        if(( info->pics = info->memalloc( sizeof( APIC* ) * count )) != NULL ) {
          for( i = 1, j = 0; ( frame = id3v2_get_frame( w->mpeg.tagv2, ID3V2_APIC, i )) != NULL ; i++ )
          {
            int   size = sizeof( APIC ) + id3v2_picture_size( frame );
            APIC* pic;

            if(( id3v2_picture_size( frame ) > frame->fr_size  ) ||
               ( info->pics[j] = info->memalloc( size )) == NULL )
            {
              count = j;
              break;
            }

            pic = info->pics[j];

            pic->size = size;
            pic->type = id3v2_picture_type( frame );

            id3v2_get_mime_type( frame, pic->mimetype, sizeof( pic->mimetype ));
            id3v2_get_description( frame, pic->desc, sizeof( pic->desc ));

            DEBUGLOG(( "mpg123: load APIC type %d, %s, '%s', size %d\n",
                        pic->type, pic->mimetype, pic->desc, id3v2_picture_size( frame )));

            memcpy((char*)pic + sizeof( APIC ),
                   id3v2_picture_data_ptr( frame ), id3v2_picture_size( frame ));
            ++j;
          }
        }
      }
    }

    info->pics_count = count;
    DEBUGLOG(( "mpg123: found %d attached pictures\n", info->pics_count ));
  }

  if( !w->mpeg.is_stream && HAVE_FIELD( info, saveinfo ))
  {
    info->saveinfo = 1;
    info->codepage = ch_default();
    info->haveinfo = DECODER_HAVE_TITLE   |
                     DECODER_HAVE_ARTIST  |
                     DECODER_HAVE_ALBUM   |
                     DECODER_HAVE_YEAR    |
                     DECODER_HAVE_GENRE   |
                     DECODER_HAVE_TRACK   |
                     DECODER_HAVE_DISC    |
                     DECODER_HAVE_COMMENT ;

    if( cfg.tag_read_type != TAG_READ_ID3V1_ONLY )
    {
      info->haveinfo |= DECODER_HAVE_COPYRIGHT  |
                        DECODER_HAVE_TRACK_GAIN |
                        DECODER_HAVE_TRACK_PEAK |
                        DECODER_HAVE_ALBUM_GAIN |
                        DECODER_HAVE_ALBUM_PEAK |
                        DECODER_HAVE_PICS       |
                        DECODER_PICS_ANYTYPE;
    }
  }

  DosReleaseMutexSem( w->mutex );

  plg_close_file( w );
  decoder_uninit( w );
  return rc;
}

static void
replace_id3v2_string( ID3V2_TAG* tag, unsigned int id, char* string )
{
  ID3V2_FRAME* frame;
  char buffer[128];
  int  i;

  if( id == ID3V2_COMM ) {
    for( i = 1; ( frame = id3v2_get_frame( tag, ID3V2_COMM, i )) != NULL ; i++ )
    {
      id3v2_get_description( frame, buffer, sizeof( buffer ));

      // Skip iTunes specific comment tags.
      if( strnicmp( buffer, "iTun", 4 ) != 0 ) {
        break;
      }
    }
  } else {
    frame = id3v2_get_frame( tag, id, 1 );
  }

  if( frame == NULL ) {
    frame = id3v2_add_frame( tag, id );
  }
  if( frame != NULL ) {
    id3v2_set_string( frame, string );
  }
}

ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  XFILE* save = NULL;
  BOOL   copy = FALSE;
  char   savename[_MAX_PATH];
  ULONG  rc = PLUGIN_OK;
  int    i;
  int    started;

  DECODER_STRUCT* w;

  if( !*filename ) {
    return EINVAL;
  }

  strlcpy( savename, filename, sizeof( savename ));
  savename[ strlen( savename ) - 1 ] = '~';

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return errno;
  }

  if(( rc = plg_open_file( w, filename, "r+b" )) != 0 ) {
    DEBUGLOG(( "mpg123: unable open file for saving %s, rc=%d\n", filename, rc ));
    decoder_uninit( w );
    return rc;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( !w->mpeg.tagv2 ) {
    w->mpeg.tagv2 = id3v2_new_tag();
  }

  if( w->mpeg.tagv2->id3_started == 0 ) {
    if( cfg.tag_save_type == TAG_SAVE_ID3V2_AND_ID3V1 ||
        cfg.tag_save_type == TAG_SAVE_ID3V2           ||
        cfg.tag_save_type == TAG_SAVE_ID3V2_ONLY       )
    {
      int set_rc;

      // FIX ME: Replay gain info also must be saved.

      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TIT2, info->title     );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TPE1, info->artist    );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TALB, info->album     );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TDRC, info->year      );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_COMM, info->comment   );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TCON, info->genre     );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TCOP, info->copyright );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TRCK, info->track     );
      replace_id3v2_string( w->mpeg.tagv2, ID3V2_TPOS, info->disc      );

      if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS ))
      {
        ID3V2_FRAME* frame;

        while(( frame = id3v2_get_frame( w->mpeg.tagv2, ID3V2_APIC, 1 )) != NULL ) {
          id3v2_delete_frame( frame );
        }
        for( i = 0; i < info->pics_count; i++ ) {
          if(( frame = id3v2_add_frame( w->mpeg.tagv2, ID3V2_APIC )) != NULL )
          {
            APIC* pic = info->pics[i];

            id3v2_set_picture( frame, pic->type, pic->mimetype, pic->desc,
                               (char*)pic + sizeof(APIC), pic->size - sizeof(APIC));
          }
        }
      }

      xio_rewind( w->mpeg.file );
      set_rc = id3v2_set_tag( w->mpeg.file, w->mpeg.tagv2, savename );

      if( set_rc == ID3V2_COPIED ) {
        copy = TRUE;
      } else if( set_rc == ID3V2_ERROR ) {
        rc = xio_errno();
      }
    } else if( cfg.tag_save_type == TAG_SAVE_ID3V1_ONLY && w->mpeg.tagv2->id3_newtag == 0 ) {
      xio_rewind( w->mpeg.file );
      if( id3v2_wipe_tag( w->mpeg.file, savename ) == ID3V2_COPIED ) {
        copy = TRUE;
      } else {
        rc = xio_errno();
      }
    }
  }

  if( rc == PLUGIN_OK ) {
    if( copy ) {
      if(( save = xio_fopen( savename, "r+b" )) == NULL ) {
        rc = xio_errno();
      }
    } else {
      save = w->mpeg.file;
    }
  } else {
    DEBUGLOG(( "mpg123: unable save id3v2 tag, rc=%d\n", rc ));
  }

  if( rc == PLUGIN_OK )
  {
    if( cfg.tag_save_type == TAG_SAVE_ID3V2_AND_ID3V1 ||
        cfg.tag_save_type == TAG_SAVE_ID3V1           ||
        cfg.tag_save_type == TAG_SAVE_ID3V1_ONLY       )
    {
      id3v1_clean_tag ( &w->mpeg.tagv1 );
      id3v1_set_string( &w->mpeg.tagv1, ID3V1_TITLE,   info->title   );
      id3v1_set_string( &w->mpeg.tagv1, ID3V1_ARTIST,  info->artist  );
      id3v1_set_string( &w->mpeg.tagv1, ID3V1_ALBUM,   info->album   );
      id3v1_set_string( &w->mpeg.tagv1, ID3V1_YEAR,    info->year    );
      id3v1_set_string( &w->mpeg.tagv1, ID3V1_COMMENT, info->comment );
      id3v1_set_string( &w->mpeg.tagv1, ID3V1_GENRE,   info->genre   );
      id3v1_set_string( &w->mpeg.tagv1, ID3V1_TRACK,   info->track   );

      if( id3v1_set_tag( save, &w->mpeg.tagv1 ) != 0 ) {
        rc = xio_errno();
      }
    } else if( cfg.tag_save_type == TAG_SAVE_ID3V2_ONLY ) {
      if( id3v1_wipe_tag( save ) != 0 ) {
        rc = xio_errno();
      }
    }
    if( rc != PLUGIN_OK ) {
      DEBUGLOG(( "mpg123: unable save id3v1 tag, rc=%d\n", rc ));
    }
  }

  if( save && save != w->mpeg.file ) {
    xio_fclose( save );
  }

  plg_close_file( w );
  DosReleaseMutexSem( w->mutex );

  if( rc != PLUGIN_OK || !copy ) {
    decoder_uninit( w );
    return rc;
  }

  if(( rc = plg_open_file( w, savename, "rb" )) != 0 ) {
    decoder_uninit( w );
    return rc;
  } else {
    // Remember a new position of the beginning of the data stream.
    started = w->mpeg.started;
    plg_close_file( w );
    decoder_uninit( w );
  }

  // Suspend all active instances of the updated file.
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );

  for( i = 0; i < instances_count; i++ )
  {
    w = instances[i];
    DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

    if( nlstricmp( w->filename, filename ) == 0 && w->mpeg.file ) {
      DEBUGLOG(( "mpg123: suspend currently used file: %s\n", w->filename ));
      w->resumepos = xio_ftell( w->mpeg.file ) - w->mpeg.started;
      xio_fclose( w->mpeg.file );
    } else {
      w->resumepos = -1;
    }
  }

  // Preserve EAs.
  eacopy( filename, savename );

  // Replace file.
  if( remove( filename ) == 0 ) {
    if( rename( savename, filename ) != 0 ) {
      rc = errno;
    }
  } else {
    rc = errno;
    remove( savename );
  }

  // Resume all suspended instances of the updated file.
  for( i = 0; i < instances_count; i++ )
  {
    w = instances[i];
    if( w->resumepos != -1 ) {
      DEBUGLOG(( "mpg123: resumes currently used file: %s\n", w->filename ));
      if(( w->mpeg.file = xio_fopen( w->mpeg.filename, "rb" )) != NULL ) {
        xio_fseek( w->mpeg.file, w->resumepos + started, XIO_SEEK_SET  );
        w->mpeg.started = started;
      }
    }
    DosReleaseMutexSem( w->mutex );
  }

  DosReleaseMutexSem( mutex );
  return rc;
}

ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 4 )
    {
      strcpy( ext[0], "*.MP3" );
      strcpy( ext[1], "*.MP2" );
      strcpy( ext[2], "*.MP1" );
      strcpy( ext[3], "audio/mpeg" );
    }
    *size = 4;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_SAVEINFO | DECODER_STREAM;
}

static void
init_tag( void )
{
  id3v2_set_read_charset( cfg.tag_read_id3v2_charset );
  id3v2_set_save_charset( cfg.tag_save_id3v2_charset );
  id3v1_set_read_charset( cfg.tag_read_id3v1_charset );
  id3v1_set_save_charset( cfg.tag_save_id3v1_charset );
}

static void
save_ini( void )
{
  HINI hini;
  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, cfg.tag_read_type          );
    save_ini_value( hini, cfg.tag_read_id3v1_charset );
    save_ini_value( hini, cfg.tag_read_id3v2_charset );
    save_ini_value( hini, cfg.tag_save_type          );
    save_ini_value( hini, cfg.tag_save_id3v1_charset );
    save_ini_value( hini, cfg.tag_save_id3v2_charset );
    save_ini_value( hini, cfg.mmx_enable );
    close_ini( hini );
  }
}

static void
load_ini( void )
{
  HINI hini;

  cfg.mmx_enable    = 0;
  cfg.tag_read_type = TAG_READ_ID3V2_AND_ID3V1;
  cfg.tag_save_type = TAG_SAVE_ID3V2_AND_ID3V1;

  if( ch_default() == CH_CYR_OS2 ) {
    cfg.tag_read_id3v2_charset = CH_CYR_WIN;
    cfg.tag_read_id3v1_charset = CH_CYR_WIN;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
    cfg.tag_save_id3v1_charset = CH_CYR_WIN;
  } else {
    cfg.tag_read_id3v2_charset = CH_DEFAULT;
    cfg.tag_read_id3v1_charset = CH_DEFAULT;
    cfg.tag_save_id3v2_charset = CH_UCS_2BOM;
    cfg.tag_save_id3v1_charset = CH_DEFAULT;
  }

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, cfg.tag_read_type          );
    load_ini_value( hini, cfg.tag_read_id3v1_charset );
    load_ini_value( hini, cfg.tag_read_id3v2_charset );
    load_ini_value( hini, cfg.tag_save_type          );
    load_ini_value( hini, cfg.tag_save_id3v1_charset );
    load_ini_value( hini, cfg.tag_save_id3v2_charset );
    load_ini_value( hini, cfg.mmx_enable );
    close_ini( hini );
  }

  init_tag();
}

/* Processes messages of the configuration dialog. */
static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
    {
      int i;

      lb_add_item( hwnd, CB_READ, "ID3v2 + ID3v1" );
      lb_add_item( hwnd, CB_READ, "ID3v1 + ID3v2" );
      lb_add_item( hwnd, CB_READ, "ID3v2"         );
      lb_add_item( hwnd, CB_READ, "ID3v1"         );

      for( i = 0; i < ch_list_size; i++ ) {
        lb_add_with_handle( hwnd, CB_ID3V1_RDCH, ch_list[i].name, ch_list[i].id );
      }
      for( i = 0; i < ch_list_size; i++ ) {
        lb_add_with_handle( hwnd, CB_ID3V2_RDCH, ch_list[i].name, ch_list[i].id );
      }

      lb_add_item( hwnd, CB_WRITE, "ID3v2 + ID3v1" );
      lb_add_item( hwnd, CB_WRITE, "ID3v2"         );
      lb_add_item( hwnd, CB_WRITE, "ID3v2 only"    );
      lb_add_item( hwnd, CB_WRITE, "ID3v1"         );
      lb_add_item( hwnd, CB_WRITE, "ID3v1 only"    );

      for( i = 0; i < ch_list_size; i++ ) {
        // All except for auto detection and UCS-2 LE.
        if( ch_list[i].id >= 0 && ch_list[i].id != CH_UCS_2LE ) {
          lb_add_with_handle( hwnd, CB_ID3V1_WRCH, ch_list[i].name, ch_list[i].id );
        }
      }
      for( i = 0; i < ch_list_size; i++ ) {
        // All except for auto detection and UCS-2 LE.
        if( ch_list[i].id >= 0 && ch_list[i].id != CH_UCS_2LE ) {
          lb_add_with_handle( hwnd, CB_ID3V2_WRCH, ch_list[i].name, ch_list[i].id );
        }
      }

      do_warpsans( hwnd );
      // continue to WM_UPDATE_CONTROLS...
    }

    case WM_UPDATE_CONTROLS:
    {
      lb_select( hwnd, CB_READ,  cfg.tag_read_type );
      lb_select( hwnd, CB_WRITE, cfg.tag_save_type );

      lb_select_by_handle( hwnd, CB_ID3V1_RDCH, cfg.tag_read_id3v1_charset );
      lb_select_by_handle( hwnd, CB_ID3V2_RDCH, cfg.tag_read_id3v2_charset );
      lb_select_by_handle( hwnd, CB_ID3V1_WRCH, cfg.tag_save_id3v1_charset );
      lb_select_by_handle( hwnd, CB_ID3V2_WRCH, cfg.tag_save_id3v2_charset );

      WinSendDlgItemMsg( hwnd, CB_USEMMX, BM_SETCHECK, MPFROMSHORT( cfg.mmx_enable ), 0 );

      if( !detect_MMX()) {
        WinEnableWindow( WinWindowFromID( hwnd, CB_USEMMX ), FALSE );
      }

      return 0;
    }

    case WM_DESTROY:
    {
      int  i;

      if(( i = lb_cursored( hwnd, CB_READ )) != LIT_NONE ) {
        cfg.tag_read_type = i;
      }
      if(( i = lb_cursored( hwnd, CB_ID3V1_RDCH )) != LIT_NONE ) {
        cfg.tag_read_id3v1_charset = lb_get_handle( hwnd, CB_ID3V1_RDCH, i );
      }
      if(( i = lb_cursored( hwnd, CB_ID3V2_RDCH )) != LIT_NONE ) {
        cfg.tag_read_id3v2_charset = lb_get_handle( hwnd, CB_ID3V2_RDCH, i );
      }

      if(( i = lb_cursored( hwnd, CB_WRITE )) != LIT_NONE ) {
        cfg.tag_save_type = i;
      }
      if(( i = lb_cursored( hwnd, CB_ID3V1_WRCH )) != LIT_NONE ) {
        cfg.tag_save_id3v1_charset = lb_get_handle( hwnd, CB_ID3V1_WRCH, i );
      }
      if(( i = lb_cursored( hwnd, CB_ID3V2_WRCH )) != LIT_NONE ) {
        cfg.tag_save_id3v2_charset = lb_get_handle( hwnd, CB_ID3V2_WRCH, i );
      }

      if(( i = WinQueryButtonCheckstate( hwnd, CB_USEMMX )) != cfg.mmx_enable ) {
        cfg.mmx_enable = i;
        if( instances_count ) {
          WinMessageBox( HWND_DESKTOP, hwnd,
              "Some settings will apply to the next song being played.",
              "MP3 Decoder", 0, MB_WARNING | MB_OK | MB_MOVEABLE );
        }
      }

      save_ini();
      init_tag();
      break;
    }

    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case PB_UNDO:
          WinSendMsg( hwnd, WM_UPDATE_CONTROLS, 0, 0 );
          return 0;

        case PB_DEFAULT:
        {
          lb_select( hwnd, CB_READ,  TAG_READ_ID3V2_AND_ID3V1 );
          lb_select( hwnd, CB_WRITE, TAG_SAVE_ID3V2_AND_ID3V1 );

          if( ch_default() == CH_CYR_OS2 ) {
            lb_select_by_handle( hwnd, CB_ID3V1_RDCH, CH_CYR_WIN  );
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_CYR_WIN  );
            lb_select_by_handle( hwnd, CB_ID3V1_WRCH, CH_CYR_WIN  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          } else {
            lb_select_by_handle( hwnd, CB_ID3V1_RDCH, CH_DEFAULT  );
            lb_select_by_handle( hwnd, CB_ID3V2_RDCH, CH_DEFAULT  );
            lb_select_by_handle( hwnd, CB_ID3V1_WRCH, CH_DEFAULT  );
            lb_select_by_handle( hwnd, CB_ID3V2_WRCH, CH_UCS_2BOM );
          }

          WinSendDlgItemMsg( hwnd, CB_USEMMX, BM_SETCHECK, 0, 0 );
          return 0;
        }
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in. */
void DLLENTRY
plugin_configure( HWND hwnd, HMODULE module ) {
  WinDlgBox( HWND_DESKTOP, hwnd, cfg_dlg_proc, module, DLG_CONFIGURE, NULL );
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Samuel Audet, Dmitry Steklenev";
  param->desc         = "MP3 Decoder " VER_STRING;
  param->configurable = TRUE;

  load_ini();
}

int INIT_ATTRIBUTE
__dll_initialize( void )
{
  if( DosCreateMutexSem( NULL, &mutex, 0, FALSE ) != NO_ERROR ) {
    return 0;
  }

  return 1;
}

int TERM_ATTRIBUTE
__dll_terminate( void )
{
  free( instances );
  DosCloseMutexSem( mutex );
  return 1;
}

#if defined(__IBMC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    return __dll_initialize();
  } else if( flag == DLL_PROCESS_DETACH ) {
    __dll_terminate();
    #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
    #endif
    _CRT_term();
  }
  return 1UL;
}
#endif
