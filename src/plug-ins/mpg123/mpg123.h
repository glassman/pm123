/*
 * Copyright 2007-2018 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef  RC_INVOKED
#include <plugin.h>
#include "common.h"
#endif

#define TAG_READ_ID3V2_AND_ID3V1  0
#define TAG_READ_ID3V1_AND_ID3V2  1
#define TAG_READ_ID3V2_ONLY       2
#define TAG_READ_ID3V1_ONLY       3

#define TAG_SAVE_ID3V2_AND_ID3V1  0
#define TAG_SAVE_ID3V2            1
#define TAG_SAVE_ID3V2_ONLY       2
#define TAG_SAVE_ID3V1            3
#define TAG_SAVE_ID3V1_ONLY       4

#define DLG_CONFIGURE     1
#define ID_NULL         900
#define CB_READ         910
#define CB_ID3V1_RDCH   920
#define CB_ID3V2_RDCH   930
#define CB_WRITE        940
#define CB_ID3V1_WRCH   950
#define CB_ID3V2_WRCH   960
#define CB_USEMMX       970
#define PB_DEFAULT      971
#define PB_UNDO         972

#define NOISE_GATE_LEVEL 80
#define LOOK_AHEAD        3

typedef struct _DECODER_STRUCT
{
   char     filename[_MAX_URL];
   MPG_FILE mpeg;
   // Used at saving stream from internet.
   char     savename[_MAX_PATH];
   XFILE*   save;

   FORMAT_INFO output_format;

   HEV   play;            // For internal use to sync the decoder thread.
   HMTX  mutex;           // For internal use to sync the decoder thread.
   int   decodertid;      // Decoder thread indentifier.
   BOOL  stop;
   BOOL  frew;
   BOOL  ffwd;
   int   startpos;
   int   endpos;
   int   jumptopos;
   ULONG status;
   HWND  hwnd;            // PM interface main frame window handle.
   int   bitrate;         // For VBR events.
   int   posmarker;
   long  resumepos;

   char* metadata_buff;
   int   metadata_size;

   void (DLLENTRYP error_display)( char* );
   void (DLLENTRYP info_display )( char* );
   int  (DLLENTRYP output_play_samples)( void* a, FORMAT_INFO* format, char* buf, int len, int posmarker );

   void* a;               // Only to be used with the precedent function.
   int   audio_buffersize;

} DECODER_STRUCT;

// used internally to manage MPG123 and saved in mpg123.ini file.
typedef struct _DECODER_SETTINGS
{
  int mmx_enable;
  int tag_read_type;
  int tag_read_id3v2_charset;
  int tag_read_id3v1_charset;
  int tag_save_type;
  int tag_save_id3v2_charset;
  int tag_save_id3v1_charset;

} DECODER_SETTINGS;

