/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2007-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define DLG_CONFIGURE 1
#define GB_DEVICE     1000
#define CB_DEVICE     1010
#define CB_SHARED     1020
#define CB_MASTERVOL  1021
#define PB_ADJUST     1022
#define GB_SETTINGS   1025
#define SB_BUFFERS    1030
#define ST_BUFFERS    1031
#define ST_FORMAT     1040
#define CB_FORMAT     1041
#define ST_DITHER     1042
#define CB_DITHER     1043
#define GB_RG         1050
#define CB_RG_ENABLE  1060
#define CB_RG_TYPE    1070
#define SB_RG_PREAMP  1080
#define ST_RG_PREAMP  1090
#define SB_PREAMP     1100
#define ST_PREAMP     1110
#define PB_UNDO       1111
#define PB_DEFAULT    1112
#define ST_AUTHOR1    1200
#define ST_AUTHOR2    1210

#define DLG_ADJUST    2
#define GB_SAMPLERATE 1000
#define CB_PCM_8000   1010
#define CB_PCM_11025  1011
#define CB_PCM_12000  1012
#define CB_PCM_16000  1013
#define CB_PCM_22050  1014
#define CB_PCM_24000  1015
#define CB_PCM_32000  1016
#define CB_PCM_44100  1017
#define CB_PCM_48000  1018
#define CB_PCM_88200  1019
#define CB_PCM_96000  1020
#define CB_PCM_176400 1021
#define CB_PCM_192000 1022
#define CB_PCM_352800 1023
#define CB_PCM_384000 1024
#define CB_PCM_705600 1025
#define CB_PCM_768000 1026
#define CB_HQ_CONVERT 1030
#define GB_DSD        1100
#define CB_DSD_32     1110
#define CB_DSD_64     1111
#define CB_DSD_128    1112
#define CB_DSD_256    1113
#define CB_DSD_512    1114
#define CB_DSD_1024   1115
#define ST_DSD_TYPE   1200
#define CB_DSD_TYPE   1210
#define CB_DSD_BITS   1215
#define ST_DSD_RATIO  1220
#define CB_DSD_RATIO  1230
#define ST_DSD_VOLUME 1240
#define CB_DSD_VOLUME 1250

#define OUTPUT_IS_HUNGRY  16  /* buffers */
#define OUTPUT_IS_SATED   32  /* buffers */
#define OUTPUT_OUTOFDATA  0.5 /* seconds */

#define DEVICE_OPENING    1
#define DEVICE_OPENED     2
#define DEVICE_CLOSING    3
#define DEVICE_CLOSED     4
#define DEVICE_FAILED     5

#define DEVCAP_OUT_AS_IS  0x00000000UL
#define DEVCAP_OUT_08BIT  0x00000001UL
#define DEVCAP_OUT_16BIT  0x00000002UL
#define DEVCAP_OUT_24BIT  0x00000003UL
#define DEVCAP_OUT_32BIT  0x00000004UL
#define DEVCAP_OUT_DEPTH  0x00000007UL
#define DEVCAP_PCM_8000   0x00000008UL
#define DEVCAP_PCM_11025  0x00000010UL
#define DEVCAP_PCM_12000  0x00000020UL
#define DEVCAP_PCM_16000  0x00000040UL
#define DEVCAP_PCM_22050  0x00000080UL
#define DEVCAP_PCM_24000  0x00000100UL
#define DEVCAP_PCM_32000  0x00000200UL
#define DEVCAP_PCM_44100  0x00000400UL
#define DEVCAP_PCM_48000  0x00000800UL
#define DEVCAP_PCM_88200  0x00001000UL
#define DEVCAP_PCM_96000  0x00002000UL
#define DEVCAP_PCM_176400 0x00004000UL
#define DEVCAP_PCM_192000 0x00008000UL
#define DEVCAP_PCM_352800 0x00010000UL
#define DEVCAP_PCM_384000 0x00020000UL
#define DEVCAP_PCM_705600 0x00040000UL
#define DEVCAP_PCM_768000 0x00080000UL

#define DEVCAP_DSD_64     0x80000000UL
#define DEVCAP_DSD_128    0x40000000UL
#define DEVCAP_DSD_256    0x20000000UL
#define DEVCAP_DSD_512    0x10000000UL
#define DEVCAP_DSD_1024   0x08000000UL

#define DEVCAP_DEFAULT  ( DEVCAP_OUT_AS_IS | \
                          DEVCAP_PCM_8000  | \
                          DEVCAP_PCM_11025 | \
                          DEVCAP_PCM_12000 | \
                          DEVCAP_PCM_16000 | \
                          DEVCAP_PCM_22050 | \
                          DEVCAP_PCM_24000 | \
                          DEVCAP_PCM_32000 | \
                          DEVCAP_PCM_44100 | \
                          DEVCAP_PCM_48000 )

// Additional information associated with audio buffer.
typedef struct BUFFERINFO
{
  MCI_MIX_BUFFER*  next;
  ULONG            playingpos;
  ULONG            offset;
  struct OS2AUDIO* a;
  BOOL             eq;

} BUFFERINFO;

#define INFO( pbuffer ) ((BUFFERINFO*)((pbuffer)->ulUserParm))

typedef struct OS2AUDIO
{
  ULONG playingpos;
  int   mastervol;    /* Control master volume instead of instance volume.    */
  int   numbuffers;   /* Suggested total audio buffers.                       */
  int   filled;       /* Number of buffers that are ready to play.            */
  int   buffersize;   /* Suggested size of the audio buffers.                 */
  int   volume;       /* Current audio device volume level.                   */
  int   master_vol;   /* System master volume level saved at device opening.  */
  float amplifier;
  int   zero;         /* This is 128 for 8 bit unsigned.                      */
  int   configured;

  int   rg_enable;    /* Replay gain related settings.                        */
  int   rg_type;
  float rg_preamp;
  float rg_preamp_other;

  float track_gain;   /* Defines Replay Gain values as specified at           */
  float track_peak;   /* http://www.replaygain.org                            */
  float album_gain;
  float album_peak;

  BOOL  trashed;      /* A next waiting portion of samples should be trashed. */
  BOOL  nomoredata;   /* The audio device don't have audio buffers for play.  */
  HEV   dataplayed;   /* Posted after playing of the next audio buffer.       */
  HMTX  mutex;        /* Serializes access to this structure.                 */
  int   status;       /* See DEVICE_* definitions.                            */

  OUTPUT_PARAMS params;
  DSP_CONV*     dsp;
  char*         dsp_buffer;

  TID   drivethread;  /* An identifier of the driver thread.                  */
  BOOL  boosted;      /* The priority of the driver thread is boosted.        */

  USHORT             mci_device_id; /* An identifier of the audio device.     */
  MCI_MIXSETUP_PARMS mci_mix;       /* Parameters of the audio mixer.         */
  MCI_BUFFER_PARMS   mci_buf_parms; /* Parameters of the audio buffers.       */
  MCI_MIX_BUFFER*    mci_buffers;   /* Audio buffers.                         */
  BUFFERINFO*        mci_buff_info; /* Audio buffers additional information.  */
  MCI_MIX_BUFFER*    mci_to_fill;   /* The buffer for a next portion of data. */
  MCI_MIX_BUFFER*    mci_is_play;   /* The buffer played now.                 */
  int                nearend;       /* The number of remaining buffers when   */
                                    /* it is time to send the                 */
                                    /* WM_OUTPUT_OUTOFDATA.                   */

  int (DLLENTRYP equalize_samples)( FORMAT_INFO*, char*, int );

} OS2AUDIO;

// used internally to manage OS2AUDIO and saved in os2audio.ini file.
typedef struct _OS2AUDIO_SETTINGS
{
  int   device;           /* Audio device ordinal.                              */
  int   lockdevice;       /* Exclusive use of the audio device.                 */
  int   mastervol;        /* Use the system master volume instead of the audio  */
                          /* device volume.                                     */
  ULONG devcaps;          /* Audio device capabilities.                         */
  int   numbuffers;       /* Number of buffers for use with the audio device.   */
  int   dither;           /* Type of the dither noise.                          */
  int   convert_hq;       /* Use high quality sampling rate conversion.         */
  int   dsd_type;         /* Type of the DSD to PCM conversion technique.       */
  int   dsd_bits;         /* Bits per sample of the PCM converted from DSD.    */
  int   dsd_ratio;        /* PCM/DSD sampling rate convertion ratio.           */
  int   dsd_volume;       /* Corrective volume added to PCM converted from DSD. */
  int   rg_enable;        /* Enable Replay Gain processing.                     */
  int   rg_type;          /* Preffered type of the Replay Gain.                 */
  float rg_preamp;        /* Pre amplifying for track with Replay Gain info.    */
  float rg_preamp_other;  /* Pre amplifying for other tracks.                   */

} OS2AUDIO_SETTINGS;

