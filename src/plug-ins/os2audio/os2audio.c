/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2007-2021 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_OS2MM
#define  INCL_PM
#define  INCL_DOS
#include <os2.h>
#include <os2me.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <utilfct.h>
#include <format.h>
#include <output_plug.h>
#include <decoder_plug.h>
#include <plugin.h>
#include <debuglog.h>
#include <dsp.h>

#include "os2audio.h"

#define  BUFFID( pbuffer ) ( INFO( pbuffer ) - INFO( pbuffer )->a->mci_buff_info )

#if 0
#define  DosRequestMutexSem( mutex, wait )                                                 \
         DEBUGLOG(( "os2audio: request mutex %08X at line %04d\n", mutex, __LINE__ ));     \
         DEBUGLOG(( "os2audio: request mutex %08X at line %04d is completed, rc = %08X\n", \
                               mutex, __LINE__, DosRequestMutexSem( mutex, wait )))

#define  DosReleaseMutexSem( mutex )                                                       \
         DEBUGLOG(( "os2audio: release mutex %08X at line %04d\n", mutex, __LINE__ ));     \
         DosReleaseMutexSem( mutex )
#endif

#define RG_PREFER_ALBUM                   0
#define RG_PREFER_ALBUM_AND_NOT_CLIPPING  1
#define RG_PREFER_TRACK                   2
#define RG_PREFER_TRACK_AND_NOT_CLIPPING  3

// Because an audio device always caches a currently played and
// following buffer, they must be skipped during equalization.
#define EQ_SKIP_BUFFERS 2

static OS2AUDIO_SETTINGS cfg;
static PSZ dBList[242];
static int opened = 0;

static ULONG pcm_rates[][2] = {{ DEVCAP_PCM_8000,     8000 },
                               { DEVCAP_PCM_11025,   11025 },
                               { DEVCAP_PCM_12000,   12000 },
                               { DEVCAP_PCM_16000,   16000 },
                               { DEVCAP_PCM_22050,   22050 },
                               { DEVCAP_PCM_24000,   24000 },
                               { DEVCAP_PCM_32000,   32000 },
                               { DEVCAP_PCM_44100,   44100 },
                               { DEVCAP_PCM_48000,   48000 },
                               { DEVCAP_PCM_88200,   88200 },
                               { DEVCAP_PCM_96000,   96000 },
                               { DEVCAP_PCM_176400, 176400 },
                               { DEVCAP_PCM_192000, 192000 },
                               { DEVCAP_PCM_352800, 352800 },
                               { DEVCAP_PCM_384000, 384000 },
                               { DEVCAP_PCM_705600, 705600 },
                               { DEVCAP_PCM_768000, 768000 }};

static ULONG dsd_rates[][2] = {{ DEVCAP_DSD_64,   44100 *   64 / 32 },
                               { DEVCAP_DSD_128,  44100 *  128 / 32 },
                               { DEVCAP_DSD_256,  44100 *  256 / 32 },
                               { DEVCAP_DSD_512,  44100 *  512 / 32 },
                               { DEVCAP_DSD_1024, 44100 * 1024 / 32 }};

#define WM_UPDATE_CONTROLS ( WM_USER + 1 )
#define WM_SET_DEVCAPS     ( WM_USER + 2 )
#define WM_GET_DEVCAPS     ( WM_USER + 3 )

static void
output_error( OS2AUDIO* a, ULONG ulError )
{
  char message[1536];
  char buffer [1024];

  if( mciGetErrorString( ulError, buffer, sizeof( buffer )) == MCIERR_SUCCESS ) {
    sprintf( message, "MCI Error %d: %s\n", LOUSHORT( ulError ), buffer );
  } else {
    sprintf( message, "MCI Error %d: Cannot query error message.\n", LOUSHORT( ulError ));
  }

  a->params.error_display( message );
  WinPostMsg( a->params.hwnd, WM_PLAYERROR, 0, 0 );
}

/* Returns the current position of the audio device in milliseconds. */
static APIRET
output_position( OS2AUDIO* a, ULONG* position )
{
  MCI_STATUS_PARMS mstatp = { 0 };
  ULONG rc;

  mstatp.ulItem = MCI_STATUS_POSITION;
  rc = mciSendCommand( a->mci_device_id, MCI_STATUS, MCI_STATUS_ITEM | MCI_WAIT, &mstatp, 0 );

  if( LOUSHORT(rc) == MCIERR_SUCCESS ) {
    *position = mstatp.ulReturn;
  }

  return rc;
}

/* Boosts a priority of the driver thread. */
static void
output_boost_priority( OS2AUDIO* a )
{
  if( a->drivethread && !a->boosted ) {
    DEBUGLOG(( "os2audio: boosts priority of the driver thread %d.\n", a->drivethread ));
    DosSetPriority( PRTYS_THREAD, PRTYC_TIMECRITICAL, PRTYD_MAXIMUM, a->drivethread );
    a->boosted = TRUE;
  }
}

/* Normalizes a priority of the driver thread. */
static void
output_normal_priority( OS2AUDIO* a )
{
  if( a->drivethread && a->boosted ) {
    DEBUGLOG(( "os2audio: normalizes priority of the driver thread %d.\n", a->drivethread ));
    DosSetPriority( PRTYS_THREAD, PRTYC_REGULAR, PRTYD_MAXIMUM, a->drivethread );
    a->boosted = FALSE;
  }
}

/* When the mixer has finished writing a buffer it will
   call this function. */
static LONG APIENTRY
dart_event( ULONG status, MCI_MIX_BUFFER* buffer, ULONG flags )
{
  OS2AUDIO* a = INFO(buffer)->a;
  DEBUGLOG2(( "os2audio: receive DART event for buffer %03d, status=%d, flags=%08X, next=%03d\n",
               BUFFID( buffer ), status, flags, BUFFID( INFO( buffer )->next )));

  if( flags & MIX_WRITE_COMPLETE )
  {
    DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );
    a->mci_is_play = INFO( buffer )->next;

    // Current time of the device is placed at end of
    // playing the buffer, but we require this already now.
    output_position( a, &a->mci_is_play->ulTime );

    if( a->filled ) {
      --a->filled;
    }

    // Because an audio device always caches a currently played and
    // following buffer, they must be skipped during equalization.
    if( a->equalize_samples && a->filled >= EQ_SKIP_BUFFERS )
    {
      MCI_MIX_BUFFER* buf = a->mci_is_play;
      int  i;

      for( i = 0; i < EQ_SKIP_BUFFERS; i++ ) {
        buf = INFO( buf )->next;
      }

      if( !INFO( buf )->eq )
      {
        FORMAT_INFO format = { sizeof( FORMAT_INFO )};

        format.samplerate = a->mci_mix.ulSamplesPerSec;
        format.channels   = a->mci_mix.ulChannels;
        format.bits       = a->mci_mix.ulBitsPerSample;
        format.format     = a->mci_mix.ulFormatTag;

        a->equalize_samples( &format, buf->pBuffer, a->mci_buf_parms.ulBufferSize );
        INFO( buf )->eq = TRUE;
      }
    }

    // If we're about to be short of decoder's data, let's boost its priority!
    if( a->filled <= OUTPUT_IS_HUNGRY ) {
      if( !a->nomoredata ) {
        output_boost_priority(a);
      }
    }

    if( a->filled < a->nearend && a->filled > 0 ) {
      DEBUGLOG(( "os2audio: output is near to finish (%d).\n", a->filled ));
      WinPostMsg( a->params.hwnd, WM_OUTPUT_OUTOFDATA, MPFROMLONG( a->filled ), 0 );
    }

    // Just too bad, the decoder fell behind...
    if( a->mci_is_play == a->mci_to_fill )
    {
      DEBUGLOG(( "os2audio: output is hungry.\n" ));
      a->mci_to_fill = INFO( a->mci_is_play )->next;
      a->nomoredata  = TRUE;
      WinPostMsg( a->params.hwnd, WM_OUTPUT_OUTOFDATA, 0, 0 );
    } else {
      a->playingpos = INFO( a->mci_is_play )->playingpos;
    }

    // Clear the played buffer and to place it to the end of the queue.
    // By the moment of playing of this buffer, it already must contain a new data.
    memset( buffer->pBuffer, a->zero, a->mci_buf_parms.ulBufferSize );
    INFO( buffer )->offset = 0;
    INFO( buffer )->eq = FALSE;
    a->mci_mix.pmixWrite( a->mci_mix.ulMixHandle, buffer, 1 );

    DosReleaseMutexSem( a->mutex );
    DosPostEventSem( a->dataplayed );
  }
  return TRUE;
}

/* Set audio device instance volume level. */
static ULONG
output_set_device_volume( void* A, unsigned char volume )
{
  OS2AUDIO* a = (OS2AUDIO*)A;
  ULONG rc = MCIERR_SUCCESS;

  MCI_SET_PARMS msp = { 0 };
  msp.ulAudio = MCI_SET_AUDIO_ALL;
  msp.ulLevel = volume;

  rc = mciSendCommand( a->mci_device_id, MCI_SET, MCI_WAIT | MCI_SET_AUDIO | MCI_SET_VOLUME, &msp, 0 );
  DEBUGLOG(( "os2audio: set device instance volume to %d, rc=%08X\n", volume, rc ));
  return rc;
}

/* Set system master volume level. */
static ULONG
output_set_master_volume( void* A, unsigned char volume )
{
  ULONG rc = MCIERR_SUCCESS;

  MCI_MASTERAUDIO_PARMS msp = { 0 };
  msp.ulMasterVolume = volume;

  rc = mciSendCommand( 0, MCI_MASTERAUDIO, MCI_WAIT | MCI_MASTERVOL, &msp, 0 );
  DEBUGLOG(( "os2audio: set master volume to %d, rc=%08X\n", volume, rc ));
  return rc;
}

/* Changes the volume of an audio device. */
static ULONG
output_set_volume( void* A, unsigned char volume, float amplifier )
{
  OS2AUDIO* a = (OS2AUDIO*)A;
  ULONG rc = MCIERR_SUCCESS;

  // Useful when device is closed and reopened.
  a->volume    = min( volume, 100 );
  a->amplifier = amplifier;

  if( a->status == DEVICE_OPENED || a->status == DEVICE_OPENING ) {
    if( a->mci_mix.ulFormatTag == WAVE_FORMAT_DSD ) {
      // If set any volume level other than the maximum
      // when playing the DSD, we will hear noise only.
      rc = output_set_device_volume( a, 100 );
      rc = output_set_master_volume( a, 100 );
    } else {
      if( !a->mastervol ) {
        rc = output_set_device_volume( a, a->volume );
      } else {
        rc = output_set_master_volume( a, a->volume );
      }
    }
  }

  return rc;
}

/* Pauses or resumes the playback. */
static ULONG DLLENTRY
output_pause( void* A, BOOL pause )
{
  OS2AUDIO* a = (OS2AUDIO*)A;
  MCI_GENERIC_PARMS mgp = { 0 };
  ULONG rc = 0;

  if( a->status == DEVICE_OPENED )
  {
    rc = mciSendCommand( a->mci_device_id, pause ? MCI_PAUSE : MCI_RESUME, MCI_WAIT, &mgp, 0 );

    if( LOUSHORT(rc) != MCIERR_SUCCESS ) {
      output_error( a, rc );
    }
  }
  return rc;
}

/* Closes the output audio device. */
ULONG DLLENTRY
output_close( void* A )
{
  OS2AUDIO* a = (OS2AUDIO*)A;
  ULONG rc = MCIERR_SUCCESS;
  BOOL  close_failed;

  MCI_GENERIC_PARMS mgp = { 0 };

  DEBUGLOG(( "os2audio: closing the output device.\n" ));
  DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );

  if( a->status != DEVICE_OPENED && a->status != DEVICE_FAILED ) {
    DEBUGLOG(( "os2audio: unable to close a not opened device.\n" ));
    DosReleaseMutexSem( a->mutex );
    return 0;
  }

  close_failed = ( a->status == DEVICE_FAILED );
  a->status = DEVICE_CLOSING;
  DosReleaseMutexSem( a->mutex );

  // Release a waiting decoder thread if any.
  DosPostEventSem( a->dataplayed );

  if( !close_failed ) {
    rc = mciSendCommand( a->mci_device_id, MCI_STOP,  MCI_WAIT, &mgp, 0 );
  }

  if( LOUSHORT(rc) != MCIERR_SUCCESS ) {
    DEBUGLOG(( "os2audio: unable to stop device before closing.\n" ));
  }

  rc = mciSendCommand( a->mci_device_id, MCI_CLOSE, MCI_WAIT, &mgp, 0 );

  if( opened ) {
    --opened;
  }

  if( LOUSHORT(rc) != MCIERR_SUCCESS ) {
    DEBUGLOG(( "os2audio: unable to close device.\n" ));
    output_error( a, rc );
  }

  if( a->mci_mix.ulFormatTag == WAVE_FORMAT_DSD && a->master_vol >= 0 ) {
    // Restore the master volume level if it has been saved.
    output_set_master_volume( a, a->master_vol );
  }

  DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );

  free( a->mci_buff_info );
  free( a->mci_buffers   );
  free( a->dsp_buffer    );

  if( a->dsp ) {
    dsp_conv_cleanup( a->dsp );
  }

  a->mci_buff_info = NULL;
  a->mci_buffers   = NULL;
  a->mci_is_play   = NULL;
  a->mci_to_fill   = NULL;
  a->mci_device_id = 0;
  a->dsp_buffer    = NULL;
  a->dsp           = NULL;
  a->status        = DEVICE_CLOSED;
  a->nomoredata    = TRUE;

  DEBUGLOG(( "os2audio: output device is successfully closed.\n" ));
  DosReleaseMutexSem( a->mutex );
  return rc;
}

/* Calculates scale factor according to current replay gain values. */
static void
recalc_replay_gain( OS2AUDIO* a )
{
  float gain      = 0.0;
  float peak      = 0.0;
  float max_scale = 0.0;
  float scale     = 1.0;

  if( a->rg_enable )
  {
    if(( a->rg_type == RG_PREFER_ALBUM ||
         a->rg_type == RG_PREFER_ALBUM_AND_NOT_CLIPPING ||
         a->track_gain == 0 ) && a->album_gain != 0 )
    {
      gain = a->album_gain;
    } else {
      gain = a->track_gain;
    }

    if(( a->rg_type == RG_PREFER_ALBUM ||
         a->rg_type == RG_PREFER_ALBUM_AND_NOT_CLIPPING ||
         a->track_peak == 0 ) && a->album_peak != 0 )
    {
      peak = a->album_peak;
    } else {
      peak = a->track_peak;
    }

    if( peak ) {
      // If a peak value is above 1.0, this is already
      // clipped by decoder and we must use 1.0 instead.
      max_scale = 1.0f / min( peak, 1.0f );
    }

    if( gain != 0 ) {
      gain += a->rg_preamp;
    } else {
      gain += a->rg_preamp_other;
    }

    scale = pow( 10, gain / 20 );

    if(( a->rg_type == RG_PREFER_ALBUM_AND_NOT_CLIPPING ||
         a->rg_type == RG_PREFER_TRACK_AND_NOT_CLIPPING ) && max_scale )
    {
      scale = min( scale, max_scale );
    }

    DEBUGLOG(( "os2audio: replay gain is %.2f dB, scale is %.8f (max is %.8f)\n",
                                                        gain, scale, max_scale ));
  }

  if( a->dsp ) {
    if( a->params.formatinfo.format == WAVE_FORMAT_DSD ) {
      if( cfg.dsd_volume ) {
        scale *= pow( 10, (float)cfg.dsd_volume / 20 );
        DEBUGLOG(( "os2audio: applying DSD to PCM volume +%d dB, "
                   "new scale is %.8f\n", cfg.dsd_volume, scale ));
      }
    }
    a->dsp->scale = scale;
  }
}

/* Initialize DSP library. */
BOOL
output_init_conversion( OS2AUDIO* a )
{
  if( a->mci_mix.ulFormatTag     != a->params.formatinfo.format     ||
      a->mci_mix.ulSamplesPerSec != a->params.formatinfo.samplerate ||
      a->mci_mix.ulBitsPerSample != a->params.formatinfo.bits       ||
    ( a->mci_mix.ulFormatTag     == WAVE_FORMAT_PCM && a->rg_enable ))
  {
    int samples = a->params.buffersize / a->params.formatinfo.channels / ( a->params.formatinfo.bits / 8 );
    int max_out = ceil((double)( samples + 1 ) * a->mci_mix.ulSamplesPerSec / a->params.formatinfo.samplerate );
    int options = cfg.dsd_type;
    int bufsize = max_out * ( a->mci_mix.ulBitsPerSample / 8 ) * a->mci_mix.ulChannels;
    FORMAT_INFO frm_out = { sizeof( FORMAT_INFO )};

    DEBUGLOG(( "oa2audio: maximal input/output samples according the buffer size is %d/%d\n", samples, max_out ));
    a->dsp_buffer = malloc( bufsize );

    if( !a->dsp_buffer ) {
      DEBUGLOG(( "os2audio: not enough memory.\n" ));
      return FALSE;
    }

    memset( a->dsp_buffer, a->zero, bufsize );

    frm_out.format     = a->mci_mix.ulFormatTag;
    frm_out.samplerate = a->mci_mix.ulSamplesPerSec;
    frm_out.channels   = a->mci_mix.ulChannels;
    frm_out.bits       = a->mci_mix.ulBitsPerSample;

    if(( cfg.devcaps & DEVCAP_OUT_DEPTH ) != DEVCAP_OUT_AS_IS ) {
      options |= cfg.dither;
    }

    a->dsp = dsp_conv_init( &a->params.formatinfo, &frm_out, samples, 1.0, options );

    if( !a->dsp ) {
      DEBUGLOG(( "os2audio: unable initialize the DSP library.\n" ));
      free( a->dsp_buffer );
      a->dsp_buffer = NULL;
      return FALSE;
    }
  }

  return TRUE;
}

/* Returns nearest supported sampling rate that is greater than or
   equal to the specified. */
static ULONG
output_near_samplerate( ULONG samplerate )
{
  ULONG nearrate = samplerate;
  int   i;

  DEBUGLOG(( "os2audio: begin search sampling rate nearest to %d\n", samplerate ));

  for( i = 0; i < ARRAY_SIZE( pcm_rates ); i++ ) {
    if( cfg.devcaps & pcm_rates[i][0] ) {
      nearrate = pcm_rates[i][1];
      if( nearrate >= samplerate ) {
        break;
      }
    }
  }

  DEBUGLOG(( "os2audio: nearest sampling rate is %d\n", nearrate ));
  return nearrate;
}

/* Returns TRUE if specified PCM sampling rate is supported by device. */
static ULONG
output_pcm_supported( ULONG samplerate )
{
  int  i;

  for( i = 0; i < ARRAY_SIZE( pcm_rates ); i++ ) {
    if( pcm_rates[i][1] == samplerate &&
        cfg.devcaps & pcm_rates[i][0] )
    {
      return TRUE;
    }
  }

  return FALSE;
}

/* Returns TRUE if specified DSD sampling rate is supported by device. */
static ULONG
output_dsd_supported( ULONG samplerate )
{
  int  i;

  for( i = 0; i < ARRAY_SIZE( dsd_rates ); i++ ) {
    if( dsd_rates[i][1] == samplerate &&
        cfg.devcaps & dsd_rates[i][0] )
    {
      return TRUE;
    }
  }

  DEBUGLOG(( "os2audio: DSD %d streams with sampling rate %d * 32 is not supported.\n",
              samplerate / 44100 * 32,  samplerate ));

  return FALSE;
}


/* Returns nearest supported sampling rate of the PCM converted
   from specified DSD. */
static ULONG
output_dsd_samplerate( ULONG samplerate )
{
  ULONG rate = samplerate / cfg.dsd_ratio;
  ULONG nearrate;

  // This is a maximal output sampling rate supported by the
  // DSD to PCM conversion engine.
  if( rate > 352800 ) {
    rate = 352800;
  }

  // Try to find the best multiple to 44100 sampling
  // rate for conversion.
  for( nearrate = rate; nearrate >= 44100; nearrate /= 2 ) {
    if( output_pcm_supported( nearrate )) {
      return nearrate;
    }
  }

  // Otherwise return nearest supported sampling rate.
  return output_near_samplerate( rate );
}

/* Opens the output audio device. */
static ULONG
output_open( OS2AUDIO* a, ULONG temp_playingpos )
{
  ULONG openflags = 0;
  ULONG i;
  ULONG rc;
  int   samplesize;

  MCI_AMP_OPEN_PARMS mci_aop = {0};

  DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );
  DEBUGLOG(( "os2audio: opening the output device.\n" ));

  if( a->status != DEVICE_CLOSED ) {
    DEBUGLOG(( "os2audio: reopen a not closed device.\n" ));

    // Output device can be reopening in continuous mode only before
    // starting of play of the next song. Because new song can have new length
    // we must zeroes all buffer positions which are left by previous song.
    for( i = 0; i < a->mci_buf_parms.ulNumBuffers; i++ ) {
      INFO( &a->mci_buffers[i] )->playingpos = temp_playingpos;
    }
    a->playingpos  = temp_playingpos;
    a->drivethread = 0;
    a->boosted     = FALSE;
    a->trashed     = FALSE;

    DosReleaseMutexSem( a->mutex );
    return 0;
  }

  a->status          = DEVICE_OPENING;
  a->playingpos      = temp_playingpos;
  a->numbuffers      = cfg.numbuffers;
  a->mci_device_id   = 0;
  a->drivethread     = 0;
  a->boosted         = FALSE;
  a->nomoredata      = TRUE;
  a->filled          = 0;
  a->trashed         = FALSE;
  a->mastervol       = cfg.mastervol;
  a->zero            = 0;
  a->rg_enable       = cfg.rg_enable;
  a->rg_type         = cfg.rg_type;
  a->rg_preamp_other = cfg.rg_preamp_other;
  a->rg_preamp       = cfg.rg_preamp;
  a->master_vol      = -1;

  // There can be the audio device supports other formats, but
  // whether can interpret other plug-ins them correctly?
  switch( a->params.formatinfo.format ) {
    case WAVE_FORMAT_PCM:
      if( a->params.formatinfo.channels > 2  ||
          a->params.formatinfo.bits % 8 != 0 ||
          a->params.formatinfo.bits > 32 )
      {
        rc = MCIERR_UNSUPP_FORMAT_MODE;
        goto end;
      }
      break;

    case WAVE_FORMAT_DSD:
      if( a->params.formatinfo.channels > 2  ||
          a->params.formatinfo.bits != 32 )
      {
        rc = MCIERR_UNSUPP_FORMAT_MODE;
        goto end;
      }
      break;

    default:
      rc = MCIERR_UNSUPP_FORMAT_MODE;
      goto end;
  }

  memset( &a->mci_mix, 0, sizeof( a->mci_mix ));
  memset( &a->mci_buf_parms, 0, sizeof( a->mci_buf_parms ));

  // Open the mixer device.
  mci_aop.pszDeviceType = (PSZ)MAKEULONG( MCI_DEVTYPE_AUDIO_AMPMIX, cfg.device );

  if( !cfg.lockdevice ) {
    openflags = MCI_OPEN_SHAREABLE;
  }

  rc = mciSendCommand( 0, MCI_OPEN, MCI_WAIT | MCI_OPEN_TYPE_ID | openflags, &mci_aop, 0 );
  if( LOUSHORT(rc) != MCIERR_SUCCESS ) {
    DEBUGLOG(( "os2audio: unable to open a mixer.\n" ));
    goto end;
  }

  a->mci_device_id = mci_aop.usDeviceID;
  ++opened;

  // Set the MCI_MIXSETUP_PARMS data structure.
  a->mci_mix.ulFormatTag     = a->params.formatinfo.format;
  a->mci_mix.ulChannels      = a->params.formatinfo.channels;
  a->mci_mix.ulBitsPerSample = a->params.formatinfo.bits;
  a->mci_mix.ulSamplesPerSec = a->params.formatinfo.samplerate;

  switch( a->params.formatinfo.format ) {
    case WAVE_FORMAT_DSD:
      if( output_dsd_supported( a->params.formatinfo.samplerate ))
      {
        // When playing a DSD stream the all volume levels must be set
        // to maximum, so we save the system master volume level here
        // to restore it when the device is closed.
        MCI_MASTERAUDIO_PARMS msp = { 0 };

        rc = mciSendCommand( 0, MCI_MASTERAUDIO,
                             MCI_WAIT | MCI_MASTERVOL | MCI_QUERYCURRENTSETTING, &msp, 0 );

        if( LOUSHORT(rc) == MCIERR_SUCCESS ) {
          a->master_vol = msp.ulReturn;
        }

        DEBUGLOG(( "os2audio: get master volume %d, rc=%08X\n", a->master_vol, rc ));
        break;
      }

      a->mci_mix.ulFormatTag = WAVE_FORMAT_PCM;
      a->mci_mix.ulSamplesPerSec = output_dsd_samplerate( a->params.formatinfo.samplerate * a->params.formatinfo.bits );

      if(( cfg.devcaps & DEVCAP_OUT_DEPTH ) == DEVCAP_OUT_AS_IS ) {
        a->mci_mix.ulBitsPerSample = cfg.dsd_bits;
        break;
      }

      // Continue to WAVE_FORMAT_PCM...

    case WAVE_FORMAT_PCM:
      a->mci_mix.ulSamplesPerSec = output_near_samplerate( a->mci_mix.ulSamplesPerSec );

      DEBUGLOG(( "os2audio: cfg.devcaps=%08X, DEVCAP_OUT_DEPTH=%d\n",
                  cfg.devcaps, cfg.devcaps & DEVCAP_OUT_DEPTH ));

      switch( cfg.devcaps & DEVCAP_OUT_DEPTH ) {
        case DEVCAP_OUT_08BIT: a->mci_mix.ulBitsPerSample =  8; break;
        case DEVCAP_OUT_16BIT: a->mci_mix.ulBitsPerSample = 16; break;
        case DEVCAP_OUT_24BIT: a->mci_mix.ulBitsPerSample = 24; break;
        case DEVCAP_OUT_32BIT: a->mci_mix.ulBitsPerSample = 32; break;
      }
      break;
  }

  // Setup the mixer for playback of a wave data.
  a->mci_mix.ulFormatMode = MCI_PLAY;
  a->mci_mix.ulDeviceType = MCI_DEVTYPE_WAVEFORM_AUDIO;
  a->mci_mix.pmixEvent    = dart_event;

  // Playing the PCM data can use some tricks, that allows provide
  // better sound on some sound cards.
  if( a->mci_mix.ulFormatTag == WAVE_FORMAT_PCM ) {
    if( a->mci_mix.ulBitsPerSample == 8 ) {
      a->zero = 0x80;
    }
  } else if( a->mci_mix.ulFormatTag == WAVE_FORMAT_DSD ) {
    a->zero = 0x69;
  }

  DEBUGLOG(( "os2audio: setup a mixer to %ld bit audio format 0x%04X, samplerate %ld, %ld channels.\n",
    a->mci_mix.ulBitsPerSample, a->mci_mix.ulFormatTag, a->mci_mix.ulSamplesPerSec, a->mci_mix.ulChannels ));

  rc = mciSendCommand( a->mci_device_id, MCI_MIXSETUP, MCI_WAIT | MCI_MIXSETUP_INIT, &a->mci_mix, 0 );

  if( LOUSHORT( rc ) != MCIERR_SUCCESS ) {
    DEBUGLOG(( "os2audio: unable to setup a mixer.\n" ));
    goto end;
  }

  DEBUGLOG(( "os2audio: recommended buffers is %d, size is %d\n",
              a->mci_mix.ulNumBuffers, a->mci_mix.ulBufferSize ));

  // For fast reaction of the player we allocate the buffer in
  // length approximately in 100 milliseconds.
  samplesize = a->mci_mix.ulChannels * ( a->mci_mix.ulBitsPerSample / 8 );
  a->buffersize = samplesize * a->mci_mix.ulSamplesPerSec / 10;

  // Buffers are limited to 64K on Intel machines. But UsbAudio stops play
  // if buffer size is bigger than 61440.
  if( a->buffersize > 61440 ) {
    a->buffersize = 61440;
  }

  // The buffer size must be an integer product of the number of
  // channels and of the number of bits in the sample. And buffers must
  // be 32-bit aligned.
  a->buffersize = a->buffersize / ( samplesize * 4 ) * ( samplesize * 4 );

  // Set up the MCI_BUFFER_PARMS data structure and allocate
  // device buffers from the mixer.
  a->numbuffers    = limit2( a->numbuffers, 16, 200 );
  a->mci_buffers   = calloc( a->numbuffers, sizeof( *a->mci_buffers   ));
  a->mci_buff_info = calloc( a->numbuffers, sizeof( *a->mci_buff_info ));

  if( !a->mci_buffers || !a->mci_buff_info ) {
    DEBUGLOG(( "os2audio: not enough memory.\n" ));
    rc = MCIERR_OUT_OF_MEMORY;
    goto end;
  }

  a->mci_buf_parms.ulNumBuffers = a->numbuffers;
  a->mci_buf_parms.ulBufferSize = a->buffersize;
  a->mci_buf_parms.pBufList     = a->mci_buffers;

  rc = mciSendCommand( a->mci_device_id, MCI_BUFFER,
                       MCI_WAIT | MCI_ALLOCATE_MEMORY, (PVOID)&a->mci_buf_parms, 0 );

  if( LOUSHORT(rc) != MCIERR_SUCCESS ) {
    DEBUGLOG(( "os2audio: unable to allocate %d buffers with size %d.\n", a->numbuffers, a->buffersize ));
    a->mci_buf_parms.ulNumBuffers = 0;
    goto end;
  }

  DEBUGLOG(( "os2audio: suggested buffers is %d, allocated %d\n",
              a->numbuffers, a->mci_buf_parms.ulNumBuffers ));
  DEBUGLOG(( "os2audio: suggested buffer size is %d, allocated %d\n",
              a->buffersize, a->mci_buf_parms.ulBufferSize ));

  // The WM_OUTPUT_OUTOFDATA must be sent before the end of the stream.
  a->nearend = samplesize * a->mci_mix.ulSamplesPerSec / a->mci_buf_parms.ulBufferSize * OUTPUT_OUTOFDATA + 1;
  DEBUGLOG(( "os2audio: near to end buffer count is %d\n", a->nearend ));

  for( i = 0; i < a->mci_buf_parms.ulNumBuffers; i++ )
  {
    a->mci_buffers[i].ulFlags        = 0;
    a->mci_buffers[i].ulBufferLength = a->mci_buf_parms.ulBufferSize;
    a->mci_buffers[i].ulUserParm     = (ULONG)&a->mci_buff_info[i];

    memset( a->mci_buffers[i].pBuffer, a->zero, a->mci_buf_parms.ulBufferSize );

    a->mci_buff_info[i].a    = a;
    a->mci_buff_info[i].next = &a->mci_buffers[i+1];
  }

  a->mci_buff_info[i-1].next = &a->mci_buffers[0];

  a->mci_is_play = &a->mci_buffers[0];
  a->mci_to_fill = &a->mci_buffers[2];

  if( !output_init_conversion( a )) {
    rc = MCIERR_CANNOT_LOAD_DSP_MOD;
    goto end;
  } else {
    recalc_replay_gain( a );
  }

  output_set_volume( a, a->volume, a->amplifier );

  // Write buffers to kick off the amp mixer.
  rc = a->mci_mix.pmixWrite( a->mci_mix.ulMixHandle, a->mci_is_play, a->mci_buf_parms.ulNumBuffers );

  if( LOUSHORT(rc) != MCIERR_SUCCESS ) {
    goto end;
  }

  // Current time of the device is placed at end of
  // playing the buffer, but we require this already now.
  output_position( a, &a->mci_is_play->ulTime );

end:

  if( LOUSHORT(rc) != MCIERR_SUCCESS )
  {
    output_error( a, rc );
    DEBUGLOG(( "os2audio: output device opening is failed.\n" ));

    if( a->mci_device_id ) {
      a->status = DEVICE_FAILED;
      output_close( a );
    } else {
      a->status = DEVICE_CLOSED;
    }
  } else {
    a->status = DEVICE_OPENED;
    DEBUGLOG(( "os2audio: output device is successfully opened.\n" ));
  }

  DosReleaseMutexSem( a->mutex );
  return rc;
}

/* This function is called by the decoder or last in chain filter plug-in
   to play samples. */
int DLLENTRY
output_play_samples( void* A, FORMAT_INFO* format, char* buf, int len, int posmarker )
{
  OS2AUDIO* a = (OS2AUDIO*)A;

  ULONG resetcount;
  ULONG size = len;
  ULONG done = 0;
  ULONG offset;
  ULONG bytes;

  DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );

  if( !a->drivethread )
  {
    PTIB ptib;

    if( DosGetInfoBlocks( &ptib, NULL ) == NO_ERROR ) {
      a->drivethread = ptib->tib_ptib2->tib2_ultid;
      output_boost_priority( a );
    }
  }

  if( a->mastervol != cfg.mastervol ) {
    // React to changes of the volume settings.
    a->mastervol = cfg.mastervol;
    output_set_volume( a, a->volume, a->amplifier );
  }

  if( a->rg_type         != cfg.rg_type         ||
      a->rg_preamp_other != cfg.rg_preamp_other ||
      a->rg_preamp       != cfg.rg_preamp       )
  {
    // React to changes of the Replay Gain settings.
    a->rg_type         = cfg.rg_type;
    a->rg_preamp_other = cfg.rg_preamp_other;
    a->rg_preamp       = cfg.rg_preamp;
    recalc_replay_gain( a );
  }

  // Set the new format structure before re-opening.
  if( memcmp( format, &a->params.formatinfo, sizeof( FORMAT_INFO )) != 0 || a->status == DEVICE_CLOSED )
  {
    DosReleaseMutexSem( a->mutex );

    DEBUGLOG(( "os2audio: old format: size %d, samplerate: %d, channels: %d, bits: %d, id: 0x%04X\n",

        a->params.formatinfo.size,
        a->params.formatinfo.samplerate,
        a->params.formatinfo.channels,
        a->params.formatinfo.bits,
        a->params.formatinfo.format ));

    DEBUGLOG(( "os2audio: new format: size %d, samplerate: %d, channels: %d, bits: %d, id: 0x%04X\n",

        format->size,
        format->samplerate,
        format->channels,
        format->bits,
        format->format ));

    a->params.formatinfo = *format;

    if( output_close(a) != 0 ) {
      return 0;
    }
    if( output_open( a, a->playingpos ) != 0 ) {
      return 0;
    }
  } else {
    DosReleaseMutexSem( a->mutex );
  }

  if( a->dsp ) {
    int samples = len / a->params.formatinfo.channels / ( a->params.formatinfo.bits / 8 );
    samples = dsp_convert( a->dsp, buf, a->dsp_buffer, samples );

    if( samples > 0 ) {
      buf  = a->dsp_buffer;
      size = samples * a->mci_mix.ulChannels * ( a->mci_mix.ulBitsPerSample / 8 );
    } else {
      return 0;
    }
  }

  while( done < size )
  {
    // If we're too quick, let's wait.
    while( a->mci_to_fill == a->mci_is_play && !a->trashed ) {
      DEBUGLOG2(( "os2audio: wait of next free buffer\n" ));
      DosWaitEventSem ( a->dataplayed, SEM_INDEFINITE_WAIT );
      DosResetEventSem( a->dataplayed, &resetcount );
    }

    if( a->status != DEVICE_OPENED ) {
      return 0;
    }

    if( a->trashed ) {
      DEBUGLOG(( "os2audio: trash a portion of samples\n" ));
      a->trashed = FALSE;
      return len;
    }

    DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );

    if( INFO( a->mci_to_fill )->offset == 0 ) {
      INFO( a->mci_to_fill )->playingpos =
        posmarker + ((float)done / a->mci_mix.ulBitsPerSample * 8
                                 / a->mci_mix.ulSamplesPerSec * 1000
                                 / a->mci_mix.ulChannels );
    }

    DEBUGLOG2(( "os2audio: fill of buffer %03d [%05d] at %lu ms\n",
                BUFFID( a->mci_to_fill ), INFO( a->mci_to_fill )->offset, INFO( a->mci_to_fill )->playingpos ));

    offset = INFO( a->mci_to_fill )->offset;
    bytes  = size - done;

    if( bytes > a->mci_buf_parms.ulBufferSize - offset ) {
        bytes = a->mci_buf_parms.ulBufferSize - offset;
    }

    memcpy((char*)a->mci_to_fill->pBuffer + offset, buf, bytes );

    done += bytes;
    buf  += bytes;

    INFO( a->mci_to_fill )->offset += bytes;

    if( INFO( a->mci_to_fill )->offset >= a->mci_buf_parms.ulBufferSize ) {
      if( a->equalize_samples && a->filled < EQ_SKIP_BUFFERS )
      {
        FORMAT_INFO format = { sizeof( FORMAT_INFO )};

        format.samplerate = a->mci_mix.ulSamplesPerSec;
        format.channels   = a->mci_mix.ulChannels;
        format.bits       = a->mci_mix.ulBitsPerSample;
        format.format     = a->mci_mix.ulFormatTag;

        INFO( a->mci_to_fill )->eq = TRUE;
        a->equalize_samples( &format, a->mci_to_fill->pBuffer,
                                      a->mci_buf_parms.ulBufferSize );
      }

      a->mci_to_fill = INFO( a->mci_to_fill )->next;
      a->filled++;
    }

    // If we're out of the water, let's reduce the
    // driver thread priority.
    if( a->filled > OUTPUT_IS_SATED ) {
      output_normal_priority( a );
    }

    a->nomoredata = FALSE;
    a->trashed    = FALSE;

    DosReleaseMutexSem( a->mutex );
  }

  return len;
}

/* Returns the posmarker from the buffer that the user
   currently hears. */
ULONG DLLENTRY
output_playing_pos( void* A )
{
  DEBUGLOG2(( "os2audio: current playing pos is %d\n", ((OS2AUDIO*)A)->playingpos ));
  return ((OS2AUDIO*)A)->playingpos;
}

/* This function is used by visual plug-ins so the user can visualize
   what is currently being played. */
ULONG DLLENTRY
output_playing_samples( void* A, FORMAT_INFO* info, char* buf, int len )
{
  OS2AUDIO* a = (OS2AUDIO*)A;

  DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );

  if( !a->mci_is_play || a->status == DEVICE_CLOSED ) {
    DosReleaseMutexSem( a->mutex );
    return PLUGIN_FAILED;
  }

  info->bits       = a->mci_mix.ulBitsPerSample;
  info->samplerate = a->mci_mix.ulSamplesPerSec;
  info->channels   = a->mci_mix.ulChannels;
  info->format     = a->mci_mix.ulFormatTag;

  if( buf && len )
  {
    MCI_MIX_BUFFER* mci_buffer = a->mci_is_play;
    ULONG position = 0;
    LONG  offsetof = 0;

    if( LOUSHORT( output_position( a, &position )) != MCIERR_SUCCESS ) {
      DosReleaseMutexSem( a->mutex );
      return PLUGIN_FAILED;
    }

    offsetof = ( position - a->mci_is_play->ulTime ) * a->mci_mix.ulSamplesPerSec / 1000 *
                                                       a->mci_mix.ulChannels *
                                                       a->mci_mix.ulBitsPerSample / 8;

    while( INFO(mci_buffer)->next != a->mci_to_fill && offsetof > mci_buffer->ulBufferLength ) {
      offsetof  -= mci_buffer->ulBufferLength;
      mci_buffer = INFO(mci_buffer)->next;
    }

    while( len && INFO(mci_buffer)->next != a->mci_to_fill )
    {
      int chunk_size = mci_buffer->ulBufferLength - offsetof;

      if( chunk_size > len ) {
          chunk_size = len;
      }

      memcpy( buf, (char*)mci_buffer->pBuffer + offsetof, chunk_size );

      buf += chunk_size;
      len -= chunk_size;

      offsetof   = 0;
      mci_buffer = INFO(mci_buffer)->next;
    }

    if( len ) {
      memset( buf, a->zero, len );
    }
  }

  DosReleaseMutexSem( a->mutex );
  return PLUGIN_OK;
}

/* Trashes all audio data received till this time. */
void DLLENTRY
output_trash_buffers( void* A, ULONG temp_playingpos )
{
  OS2AUDIO* a = (OS2AUDIO*)A;
  int i;

  DEBUGLOG(( "os2audio: trashing audio buffers.\n" ));
  DosRequestMutexSem( a->mutex, SEM_INDEFINITE_WAIT );

  if(!( a->status & DEVICE_OPENED )) {
    DosReleaseMutexSem( a->mutex );
    return;
  }

  if( a->dsp ) {
    dsp_conv_reset( a->dsp );
  }

  for( i = 0; i < a->mci_buf_parms.ulNumBuffers; i++ )
  {
    MCI_MIX_BUFFER* pNext = a->mci_buffers+i;

    memset( pNext->pBuffer, a->zero, a->mci_buf_parms.ulBufferSize );
    INFO( pNext )->offset = 0;
    INFO( pNext )->playingpos = temp_playingpos;
    INFO( pNext )->eq = FALSE;
  }

  a->playingpos = temp_playingpos;
  a->nomoredata = FALSE;
  a->trashed    = TRUE;
  a->filled     = 0;

  // A following buffer is already cashed by the audio device. Therefore
  // it is necessary to skip it and to fill the second buffer from current.
  a->mci_to_fill = INFO(a->mci_is_play)->next;
  INFO(a->mci_to_fill)->playingpos = temp_playingpos;
  a->mci_to_fill = INFO(a->mci_to_fill)->next;

  DEBUGLOG(( "os2audio: audio buffers is successfully trashed.\n" ));
  DosReleaseMutexSem( a->mutex );
}

/* This function is called when the user requests
   the use of output plug-in. */
ULONG DLLENTRY
output_init( void** A )
{
  OS2AUDIO* a;

 *A = calloc( sizeof( OS2AUDIO ), 1 );
  a = (OS2AUDIO*)*A;

  a->numbuffers  = 128;
  a->volume      = 100;
  a->amplifier   = 1.0;
  a->status      = DEVICE_CLOSED;
  a->nomoredata  = TRUE;

  DosCreateEventSem( NULL, &a->dataplayed, 0, FALSE );
  DosCreateMutexSem( NULL, &a->mutex, 0, FALSE );
  return PLUGIN_OK;
}

/* This function is called when another output plug-in
   is request by the user. */
ULONG DLLENTRY
output_uninit( void* A )
{
  OS2AUDIO* a = (OS2AUDIO*)A;

  DosCloseEventSem( a->dataplayed );
  DosCloseMutexSem( a->mutex );

  free( a );
  return PLUGIN_OK;
}

/* Returns TRUE if the output plug-in still has some buffers to play. */
BOOL DLLENTRY
output_playing_data( void* A ) {
  return !((OS2AUDIO*)A)->nomoredata;
}

/* Queries name of the specified sound device. Returns a number
   of installed sound devices. */
static ULONG DLLENTRY
output_get_devices( char* name, int size, ULONG deviceid )
{
  char buffer[MAX_DEVICE_NAME];
  MCI_SYSINFO_PARMS mip;
  ULONG rc;

  if( name && size ) {
    *name = 0;

    if( deviceid )
    {
      mip.pszReturn    = buffer;
      mip.ulRetSize    = sizeof( buffer );
      mip.usDeviceType = MCI_DEVTYPE_AUDIO_AMPMIX;
      mip.ulNumber     = deviceid;

      rc = mciSendCommand( 0, MCI_SYSINFO, MCI_WAIT | MCI_SYSINFO_INSTALLNAME, &mip, 0 );
      if( LOUSHORT(rc) == MCIERR_SUCCESS )
      {
        MCI_SYSINFO_LOGDEVICE mid;

        mip.ulItem = MCI_SYSINFO_QUERY_DRIVER;
        mip.pSysInfoParm = &mid;
        strcpy( mid.szInstallName, buffer );

        rc = mciSendCommand( 0, MCI_SYSINFO, MCI_WAIT | MCI_SYSINFO_ITEM, &mip, 0 );
        if( LOUSHORT(rc) == MCIERR_SUCCESS ) {
          #if SHOW_DEVICE_VERSION
            snprintf( name, size, "%s (%s)", mid.szProductInfo, mid.szVersionNumber );
          #else
            strlcpy( name, mid.szProductInfo, size );
          #endif
        }
      }
    } else {
      strlcpy( name, "Default", size );
    }
  }

  mip.pszReturn    = buffer;
  mip.ulRetSize    = sizeof(buffer);
  mip.usDeviceType = MCI_DEVTYPE_AUDIO_AMPMIX;

  mciSendCommand( 0, MCI_SYSINFO, MCI_WAIT | MCI_SYSINFO_QUANTITY, &mip, 0 );
  return atoi( mip.pszReturn );
}

/* Queries capabilities of the specified sound device. */
static ULONG
output_device_caps( HWND howner, ULONG device )
{
  ULONG rc;
  ULONG caps = DEVCAP_DEFAULT & DEVCAP_OUT_DEPTH;
  int   i;

  MCI_AMP_OPEN_PARMS mci_aop = {0};
  MCI_GENERIC_PARMS  mci_mgp = {0};
  MCI_MIXSETUP_PARMS mci_mix = {0};

  // Open the mixer device.
  mci_aop.pszDeviceType = (PSZ)MAKEULONG( MCI_DEVTYPE_AUDIO_AMPMIX, device );
  rc = mciSendCommand( 0, MCI_OPEN, MCI_WAIT | MCI_OPEN_TYPE_ID, &mci_aop, 0 );

  if( LOUSHORT(rc) != MCIERR_SUCCESS ) {
    DEBUGLOG(( "os2audio: unable to open a mixer to enumerate formats.\n" ));
    if( howner ) {
      WinMessageBox( HWND_DESKTOP, howner,
                     "Unable to open audio device. You may need to stop the player before using this function."  ,
                     "DART Audio", 0, MB_APPLMODAL | MB_ERROR | MB_OK | MB_MOVEABLE );
    }
    return DEVCAP_DEFAULT;
  }

  for( i = 0; i < ARRAY_SIZE( pcm_rates ); i++ )
  {
    mci_mix.ulFormatMode    = MCI_PLAY;
    mci_mix.ulDeviceType    = MCI_DEVTYPE_WAVEFORM_AUDIO;
    mci_mix.ulFormatTag     = WAVE_FORMAT_PCM;
    mci_mix.ulChannels      = 2;
    mci_mix.ulSamplesPerSec = pcm_rates[i][1];
    mci_mix.ulBitsPerSample = 16;

    rc = mciSendCommand( mci_aop.usDeviceID, MCI_MIXSETUP,
                         MCI_WAIT | MCI_MIXSETUP_QUERYMODE, &mci_mix, 0 );

    DEBUGLOG(( "os2audio: PCM %7d Hz: %s\n",
                pcm_rates[i][1], LOUSHORT(rc) == MCIERR_SUCCESS ? "+" : "-" ));

    if( LOUSHORT(rc) == MCIERR_SUCCESS ) {
      caps |= pcm_rates[i][0];
    }
  }

  for( i = 0; i < ARRAY_SIZE( dsd_rates ); i++ )
  {
    mci_mix.ulFormatMode    = MCI_PLAY;
    mci_mix.ulDeviceType    = MCI_DEVTYPE_WAVEFORM_AUDIO;
    mci_mix.ulFormatTag     = WAVE_FORMAT_DSD;
    mci_mix.ulChannels      = 2;
    mci_mix.ulSamplesPerSec = dsd_rates[i][1];
    mci_mix.ulBitsPerSample = 32;

    rc = mciSendCommand( mci_aop.usDeviceID, MCI_MIXSETUP,
                         MCI_WAIT | MCI_MIXSETUP_QUERYMODE, &mci_mix, 0 );

    DEBUGLOG(( "os2audio: DSD %7d Hz: %s\n",
                dsd_rates[i][1], LOUSHORT(rc) == MCIERR_SUCCESS ? "+" : "-" ));

    if( LOUSHORT(rc) == MCIERR_SUCCESS ) {
      caps |= dsd_rates[i][0];
    }
  }

  mciSendCommand( mci_aop.usDeviceID, MCI_CLOSE, MCI_WAIT, &mci_mgp, 0 );
  DEBUGLOG(( "os2audio: requested device %d capabilittes: %08lX.\n", device, caps ));
  return caps;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   OUTPUT_PARAMS in the output_plug.h file. */
ULONG DLLENTRY
output_command( void* A, ULONG msg, OUTPUT_PARAMS* params )
{
  OS2AUDIO* a = (OS2AUDIO*)A;

  switch( msg )
  {
    case OUTPUT_OPEN:
      return output_open( a, params->temp_playingpos );

    case OUTPUT_PAUSE:
      return output_pause( a, params->pause );

    case OUTPUT_CLOSE:
      return output_close( a );

    case OUTPUT_VOLUME:
      if( params->volume != a->volume || params->amplifier != a->amplifier ) {
        DEBUGLOG(( "os2audio: received command OUTPUT_VOLUME %d, %f\n",
                    params->volume, params->amplifier ));
        return output_set_volume( a, params->volume, params->amplifier );
      } else {
        DEBUGLOG(( "os2audio: ignored repeated command OUTPUT_VOLUME %d, %f\n",
                    params->volume, params->amplifier ));
        return 0;
      }

    case OUTPUT_SETUP:
      if( a->status != DEVICE_OPENED ) {
        // Otherwise, information on the current session are modified.
        a->params = *params;
      }

      if( HAVE_FIELD( params, info ) && HAVE_FIELD( params->info, album_gain ))
      {
        a->album_gain = params->info->album_gain;
        a->album_peak = params->info->album_peak;
        a->track_gain = params->info->track_gain;
        a->track_peak = params->info->track_peak;
        recalc_replay_gain( a );
      }

      if( HAVE_FIELD( params, equalize_samples )) {
        a->equalize_samples = params->equalize_samples;
      }

      params->always_hungry = FALSE;
      return 0;

    case OUTPUT_TRASH_BUFFERS:
      output_trash_buffers( a, params->temp_playingpos );
      return 0;
  }

  return MCIERR_UNSUPPORTED_FUNCTION;
}

/* Loads capabilities of the specified sound device. */
static void
save_device_caps( ULONG device, ULONG devcaps )
{
  HINI hini;
  char devname[64];

  if(( hini = open_module_ini()) != NULLHANDLE ) {
    output_get_devices( devname, sizeof( devname ), device );
    if( *devname ) {
      strlcat( devname, "_CAPS", sizeof( devname ));
      PrfWriteProfileData( hini, INI_SECTION, devname, &devcaps, sizeof( devcaps ));
    }
    close_ini( hini );
  }
}

/* Saves plug-in's settings. */
static void
save_ini( void )
{
  HINI hini;

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, cfg.device );
    save_ini_value( hini, cfg.lockdevice );
    save_ini_value( hini, cfg.mastervol );
    save_ini_value( hini, cfg.numbuffers );
    save_ini_value( hini, cfg.dither );
    save_ini_value( hini, cfg.convert_hq );
    save_ini_value( hini, cfg.dsd_type );
    save_ini_value( hini, cfg.dsd_bits );
    save_ini_value( hini, cfg.dsd_ratio );
    save_ini_value( hini, cfg.dsd_volume );
    save_ini_value( hini, cfg.rg_enable );
    save_ini_value( hini, cfg.rg_type );
    save_ini_value( hini, cfg.rg_preamp );
    save_ini_value( hini, cfg.rg_preamp_other );

    save_device_caps( cfg.device, cfg.devcaps );
    close_ini( hini );
  }
}

/* Loads capabilities of the specified sound device. */
static ULONG
load_device_caps( HWND howner, ULONG device )
{
  HINI  hini;
  char  devname[64];
  ULONG devcaps = 0;
  ULONG size = sizeof( devcaps );

  if(( hini = open_module_ini()) != NULLHANDLE ) {
    output_get_devices( devname, sizeof( devname ), device );
    if( *devname )
    {
      strlcat( devname, "_CAPS", sizeof( devname ));
      PrfQueryProfileData( hini, INI_SECTION, devname, &devcaps, &size );

      if( size != sizeof( devcaps ) || devcaps == 0 ) {
        devcaps = output_device_caps( howner, device );
        PrfWriteProfileData( hini, INI_SECTION, devname, &devcaps, sizeof( devcaps ));
      }
    }
    close_ini( hini );
  }

  return devcaps;
}

/* Loads plug-in's settings. */
static void
load_ini( void )
{
  HINI  hini;
  ULONG numdevices = output_get_devices( NULL, 0, 0 );

  cfg.device          = 0;
  cfg.lockdevice      = 0;
  cfg.mastervol       = 0;
  cfg.devcaps         = DEVCAP_DEFAULT;
  cfg.numbuffers      = 128;
  cfg.dither          = 0;
  cfg.convert_hq      = 0;
  cfg.dsd_type        = DSP_DSDPCM_MULTISTAGE;
  cfg.dsd_bits        = 16;
  cfg.dsd_ratio       = 32;
  cfg.dsd_volume      = 0;
  cfg.rg_enable       = 0;
  cfg.rg_type         = RG_PREFER_ALBUM;
  cfg.rg_preamp       = 3;
  cfg.rg_preamp_other = 0;

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, cfg.device );
    load_ini_value( hini, cfg.lockdevice );
    load_ini_value( hini, cfg.mastervol );
    load_ini_value( hini, cfg.numbuffers );
    load_ini_value( hini, cfg.dither );
    load_ini_value( hini, cfg.convert_hq );
    load_ini_value( hini, cfg.dsd_type );
    load_ini_value( hini, cfg.dsd_bits );
    load_ini_value( hini, cfg.dsd_ratio );
    load_ini_value( hini, cfg.dsd_volume );
    load_ini_value( hini, cfg.rg_enable );
    load_ini_value( hini, cfg.rg_type );
    load_ini_value( hini, cfg.rg_preamp );
    load_ini_value( hini, cfg.rg_preamp_other );

    if( cfg.device > numdevices ) {
      cfg.device = 0;
    }

    cfg.devcaps = load_device_caps( NULLHANDLE, cfg.device );
    close_ini( hini );
  }
}

/* Processes messages of the adjust sound device dialog. */
static MRESULT EXPENTRY
adjust_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
      do_warpsans( hwnd );

      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Multistage (64fp)", DSP_DSDPCM_MULTISTAGE | DSP_DSDPCM_FP64 );
      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Multistage (32fp)", DSP_DSDPCM_MULTISTAGE );
      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Direct (64fp)",     DSP_DSDPCM_DIRECT | DSP_DSDPCM_FP64 );
      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Direct (32fp)",     DSP_DSDPCM_DIRECT );

      lb_add_with_handle( hwnd, CB_DSD_BITS, "08 bits",  8 );
      lb_add_with_handle( hwnd, CB_DSD_BITS, "16 bits", 16 );
      lb_add_with_handle( hwnd, CB_DSD_BITS, "24 bits", 24 );
      lb_add_with_handle( hwnd, CB_DSD_BITS, "32 bits", 32 );

      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/8",   8 );
      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/16", 16 );
      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/32", 32 );
      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/64", 64 );

      lb_add_item( hwnd, CB_DSD_VOLUME,  "0 dB" );
      lb_add_item( hwnd, CB_DSD_VOLUME, "+1 dB" );
      lb_add_item( hwnd, CB_DSD_VOLUME, "+2 dB" );
      lb_add_item( hwnd, CB_DSD_VOLUME, "+3 dB" );
      lb_add_item( hwnd, CB_DSD_VOLUME, "+4 dB" );
      lb_add_item( hwnd, CB_DSD_VOLUME, "+5 dB" );
      lb_add_item( hwnd, CB_DSD_VOLUME, "+6 dB" );

      WinCheckButton( hwnd, CB_HQ_CONVERT, cfg.convert_hq );

      if( !lb_select_by_handle( hwnd, CB_DSD_TYPE, cfg.dsd_type )) {
        lb_select_by_handle( hwnd, CB_DSD_TYPE, DSP_DSDPCM_MULTISTAGE );
      }
      if( !lb_select_by_handle( hwnd, CB_DSD_BITS, cfg.dsd_bits )) {
        lb_select_by_handle( hwnd, CB_DSD_BITS, 16 );
      }
      if( !lb_select_by_handle( hwnd, CB_DSD_RATIO, cfg.dsd_ratio )) {
        lb_select_by_handle( hwnd, CB_DSD_RATIO, 32 );
      }

      lb_select( hwnd, CB_DSD_VOLUME, cfg.dsd_volume );
      break;

    case WM_UPDATE_CONTROLS:
      WinSendMsg( hwnd, WM_SET_DEVCAPS, mp1, 0 );
      WinSetWindowULong( hwnd, QWL_USER, LONGFROMMP( mp1 ));
      return 0;

    case WM_GET_DEVCAPS:
    {
      ULONG devcaps = cfg.devcaps & DEVCAP_OUT_DEPTH;

      if( WinQueryButtonCheckstate( hwnd, CB_PCM_8000   )) { devcaps |= DEVCAP_PCM_8000;   }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_11025  )) { devcaps |= DEVCAP_PCM_11025;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_12000  )) { devcaps |= DEVCAP_PCM_12000;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_16000  )) { devcaps |= DEVCAP_PCM_16000;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_22050  )) { devcaps |= DEVCAP_PCM_22050;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_24000  )) { devcaps |= DEVCAP_PCM_24000;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_32000  )) { devcaps |= DEVCAP_PCM_32000;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_44100  )) { devcaps |= DEVCAP_PCM_44100;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_48000  )) { devcaps |= DEVCAP_PCM_48000;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_88200  )) { devcaps |= DEVCAP_PCM_88200;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_96000  )) { devcaps |= DEVCAP_PCM_96000;  }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_176400 )) { devcaps |= DEVCAP_PCM_176400; }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_192000 )) { devcaps |= DEVCAP_PCM_192000; }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_352800 )) { devcaps |= DEVCAP_PCM_352800; }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_384000 )) { devcaps |= DEVCAP_PCM_384000; }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_705600 )) { devcaps |= DEVCAP_PCM_705600; }
      if( WinQueryButtonCheckstate( hwnd, CB_PCM_768000 )) { devcaps |= DEVCAP_PCM_768000; }

      if( WinQueryButtonCheckstate( hwnd, CB_DSD_64   )) { devcaps |= DEVCAP_DSD_64;   }
      if( WinQueryButtonCheckstate( hwnd, CB_DSD_128  )) { devcaps |= DEVCAP_DSD_128;  }
      if( WinQueryButtonCheckstate( hwnd, CB_DSD_256  )) { devcaps |= DEVCAP_DSD_256;  }
      if( WinQueryButtonCheckstate( hwnd, CB_DSD_512  )) { devcaps |= DEVCAP_DSD_512;  }
      if( WinQueryButtonCheckstate( hwnd, CB_DSD_1024 )) { devcaps |= DEVCAP_DSD_1024; }

      return MRFROMLONG( devcaps );
    }

    case WM_SET_DEVCAPS:
    {
      ULONG devcaps = LONGFROMMP( mp1 );
      DEBUGLOG(( "os2audio: WM_SET_DEVCAPS devcaps=%08X\n", devcaps ));

      WinCheckButton( hwnd, CB_PCM_8000,   ( devcaps & DEVCAP_PCM_8000   ) != 0 );
      WinCheckButton( hwnd, CB_PCM_11025,  ( devcaps & DEVCAP_PCM_11025  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_12000,  ( devcaps & DEVCAP_PCM_12000  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_16000,  ( devcaps & DEVCAP_PCM_16000  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_22050,  ( devcaps & DEVCAP_PCM_22050  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_24000,  ( devcaps & DEVCAP_PCM_24000  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_32000,  ( devcaps & DEVCAP_PCM_32000  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_44100,  ( devcaps & DEVCAP_PCM_44100  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_48000,  ( devcaps & DEVCAP_PCM_48000  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_88200,  ( devcaps & DEVCAP_PCM_88200  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_96000,  ( devcaps & DEVCAP_PCM_96000  ) != 0 );
      WinCheckButton( hwnd, CB_PCM_176400, ( devcaps & DEVCAP_PCM_176400 ) != 0 );
      WinCheckButton( hwnd, CB_PCM_192000, ( devcaps & DEVCAP_PCM_192000 ) != 0 );
      WinCheckButton( hwnd, CB_PCM_352800, ( devcaps & DEVCAP_PCM_352800 ) != 0 );
      WinCheckButton( hwnd, CB_PCM_384000, ( devcaps & DEVCAP_PCM_384000 ) != 0 );
      WinCheckButton( hwnd, CB_PCM_705600, ( devcaps & DEVCAP_PCM_705600 ) != 0 );
      WinCheckButton( hwnd, CB_PCM_768000, ( devcaps & DEVCAP_PCM_768000 ) != 0 );

      WinCheckButton( hwnd, CB_DSD_64,   ( devcaps & DEVCAP_DSD_64   ) != 0 );
      WinCheckButton( hwnd, CB_DSD_128,  ( devcaps & DEVCAP_DSD_128  ) != 0 );
      WinCheckButton( hwnd, CB_DSD_256,  ( devcaps & DEVCAP_DSD_256  ) != 0 );
      WinCheckButton( hwnd, CB_DSD_512,  ( devcaps & DEVCAP_DSD_512  ) != 0 );
      WinCheckButton( hwnd, CB_DSD_1024, ( devcaps & DEVCAP_DSD_1024 ) != 0 );

      return 0;
    }

    case WM_SYSCOMMAND:
      if( SHORT1FROMMP( mp1 ) == SC_CLOSE ) {
        ULONG devcaps = LONGFROMMR( WinSendMsg( hwnd, WM_GET_DEVCAPS, 0, 0 ));
        WinSetWindowULong( hwnd, QWL_USER, devcaps );
        WinDismissDlg( hwnd, 0 );
        return 0;
      }
      break;

    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case PB_UNDO:
        {
          ULONG devcaps = WinQueryWindowULong( hwnd, QWL_USER );
          WinSendMsg( hwnd, WM_SET_DEVCAPS, MPFROMLONG( devcaps ), 0 );
          WinCheckButton( hwnd, CB_HQ_CONVERT, cfg.convert_hq );
          lb_select_by_handle( hwnd, CB_DSD_TYPE, cfg.dsd_type );
          lb_select_by_handle( hwnd, CB_DSD_BITS, cfg.dsd_bits );
          lb_select_by_handle( hwnd, CB_DSD_RATIO, cfg.dsd_ratio );
          lb_select( hwnd, CB_DSD_VOLUME, cfg.dsd_volume );
          return 0;
        }

        case PB_DEFAULT:
        {
          HWND  howner = WinQueryWindow( hwnd, QW_OWNER );
          ULONG device = lb_selected( howner, CB_DEVICE, LIT_FIRST );

          if( device != LIT_NONE ) {
            ULONG devcaps = output_device_caps( hwnd, device );
            WinSendMsg( hwnd, WM_SET_DEVCAPS, MPFROMLONG( devcaps ), 0 );
          }

          WinCheckButton( hwnd, CB_HQ_CONVERT, FALSE );
          lb_select_by_handle( hwnd, CB_DSD_TYPE, DSP_DSDPCM_MULTISTAGE );
          lb_select_by_handle( hwnd, CB_DSD_BITS, 16 );
          lb_select_by_handle( hwnd, CB_DSD_RATIO, 32 );
          lb_select( hwnd, CB_DSD_VOLUME, 0 );
          return 0;
        }
      }
      return 0;

    case WM_DESTROY:
    {
      int i;

      cfg.devcaps = LONGFROMMR( WinSendMsg( hwnd, WM_GET_DEVCAPS, 0, 0 ));
      cfg.convert_hq = WinQueryButtonCheckstate( hwnd, CB_HQ_CONVERT );

      if(( i = lb_cursored( hwnd, CB_DSD_TYPE )) != LIT_NONE ) {
        cfg.dsd_type = lb_get_handle( hwnd, CB_DSD_TYPE, i );
      }
      if(( i = lb_cursored( hwnd, CB_DSD_BITS )) != LIT_NONE ) {
        cfg.dsd_bits = lb_get_handle( hwnd, CB_DSD_BITS, i );
      }
      if(( i = lb_cursored( hwnd, CB_DSD_RATIO )) != LIT_NONE ) {
        cfg.dsd_ratio = lb_get_handle( hwnd, CB_DSD_RATIO, i );
      }

      cfg.dsd_volume = lb_selected( hwnd, CB_DSD_VOLUME, LIT_FIRST );
      break;
    }
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the configuration dialog. */
static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static HWND hadjust;

  switch( msg ) {
    case WM_INITDLG:
    {
      int     i;
      int     numdevice = output_get_devices( NULL, 0, 0 );
      char    modname[CCHMAXPATH];
      HMODULE hmodule;

      getModule( &hmodule, modname, sizeof( modname ));
      hadjust = WinLoadDlg( HWND_DESKTOP, hwnd, adjust_dlg_proc, hmodule, DLG_ADJUST, NULL );

      do_warpsans( hwnd );

      for( i = 0; i <= numdevice; i++ ) {
        char devname[64];
        output_get_devices( devname, sizeof( devname ), i );
        lb_add_item( hwnd, CB_DEVICE, devname );
      }

      lb_add_item( hwnd, CB_RG_TYPE, "Prefer album gain" );
      lb_add_item( hwnd, CB_RG_TYPE, "Prefer album gain and prevent clipping" );
      lb_add_item( hwnd, CB_RG_TYPE, "Prefer track gain" );
      lb_add_item( hwnd, CB_RG_TYPE, "Prefer track gain and prevent clipping" );

      WinSendDlgItemMsg( hwnd, SB_BUFFERS,   SPBM_SETLIMITS, MPFROMLONG( 200 ),  MPFROMLONG(   5 ));
      WinSendDlgItemMsg( hwnd, SB_RG_PREAMP, SPBM_SETARRAY,  MPFROMP( &dBList ), MPFROMLONG( 241 ));
      WinSendDlgItemMsg( hwnd, SB_PREAMP,    SPBM_SETARRAY,  MPFROMP( &dBList ), MPFROMLONG( 241 ));

      lb_add_with_handle( hwnd, CB_FORMAT, "As is",   DEVCAP_OUT_AS_IS );
      lb_add_with_handle( hwnd, CB_FORMAT, "08 bits", DEVCAP_OUT_08BIT );
      lb_add_with_handle( hwnd, CB_FORMAT, "16 bits", DEVCAP_OUT_16BIT );
      lb_add_with_handle( hwnd, CB_FORMAT, "24 bits", DEVCAP_OUT_24BIT );
      lb_add_with_handle( hwnd, CB_FORMAT, "32 bits", DEVCAP_OUT_32BIT );

      lb_add_with_handle( hwnd, CB_DITHER, "None",        DSP_DITHER_NONE );
      lb_add_with_handle( hwnd, CB_DITHER, "Rectangular", DSP_DITHER_RECTANGULAR );
      lb_add_with_handle( hwnd, CB_DITHER, "Triangular",  DSP_DITHER_TRIANGULAR );
      lb_add_with_handle( hwnd, CB_DITHER, "Shaped",      DSP_DITHER_SHAPED );

      // continue to WM_UPDATE_CONTROLS...
    }

    case WM_UPDATE_CONTROLS:
      lb_select( hwnd, CB_DEVICE, cfg.device );

      WinCheckButton( hwnd, CB_SHARED,   !cfg.lockdevice );
      WinCheckButton( hwnd, CB_MASTERVOL, cfg.mastervol  );
      WinCheckButton( hwnd, CB_RG_ENABLE, cfg.rg_enable  );

      if( !lb_select_by_handle( hwnd, CB_FORMAT, cfg.devcaps & DEVCAP_OUT_DEPTH )) {
        lb_select_by_handle( hwnd, CB_FORMAT, DEVCAP_DEFAULT & DEVCAP_OUT_DEPTH );
      }

      WinEnableControl( hwnd, ST_DITHER, ( cfg.devcaps & DEVCAP_OUT_DEPTH ) != DEVCAP_OUT_AS_IS );
      WinEnableControl( hwnd, CB_DITHER, ( cfg.devcaps & DEVCAP_OUT_DEPTH ) != DEVCAP_OUT_AS_IS );

      if( !lb_select_by_handle( hwnd, CB_DITHER, cfg.dither )) {
        lb_select_by_handle( hwnd, CB_DITHER, DSP_DITHER_NONE );
      }

      lb_select( hwnd, CB_RG_TYPE, cfg.rg_type );

      WinEnableControl( hwnd, CB_RG_TYPE,   cfg.rg_enable );
      WinEnableControl( hwnd, SB_RG_PREAMP, cfg.rg_enable );
      WinEnableControl( hwnd, ST_RG_PREAMP, cfg.rg_enable );
      WinEnableControl( hwnd, SB_PREAMP,    cfg.rg_enable );
      WinEnableControl( hwnd, ST_PREAMP,    cfg.rg_enable );

      WinSendDlgItemMsg( hwnd, SB_BUFFERS,   SPBM_SETCURRENTVALUE, MPFROMLONG( cfg.numbuffers ), 0 );
      WinSendDlgItemMsg( hwnd, SB_RG_PREAMP, SPBM_SETCURRENTVALUE, MPFROMLONG( cfg.rg_preamp * 10 + 120 ), 0 );
      WinSendDlgItemMsg( hwnd, SB_PREAMP,    SPBM_SETCURRENTVALUE, MPFROMLONG( cfg.rg_preamp_other * 10 + 120 ), 0 );

      WinSendMsg( hadjust, WM_UPDATE_CONTROLS, MPFROMLONG( cfg.devcaps ), 0 );
      return 0;

    case WM_DESTROY:
    {
      OS2AUDIO_SETTINGS old_cfg = cfg;
      int i;

      cfg.device = lb_selected( hwnd, CB_DEVICE, LIT_FIRST );
      WinDestroyWindow( hadjust );

      cfg.lockdevice = !WinQueryButtonCheckstate( hwnd, CB_SHARED );
      cfg.mastervol  =  WinQueryButtonCheckstate( hwnd, CB_MASTERVOL );
      cfg.rg_enable  =  WinQueryButtonCheckstate( hwnd, CB_RG_ENABLE );

      if(( i = lb_cursored( hwnd, CB_FORMAT )) != LIT_NONE ) {
        cfg.devcaps = ( cfg.devcaps & ~DEVCAP_OUT_DEPTH ) | lb_get_handle( hwnd, CB_FORMAT, i );
      }
      if(( i = lb_cursored( hwnd, CB_DITHER )) != LIT_NONE ) {
        cfg.dither = lb_get_handle( hwnd, CB_DITHER, i );
      }

      cfg.rg_type = lb_selected( hwnd, CB_RG_TYPE, LIT_FIRST );

      WinSendDlgItemMsg( hwnd, SB_BUFFERS, SPBM_QUERYVALUE,
                         MPFROMP( &cfg.numbuffers ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));

      WinSendDlgItemMsg( hwnd, SB_RG_PREAMP, SPBM_QUERYVALUE,
                         MPFROMP( &i ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
      cfg.rg_preamp = i / 10.0 - 12;

      WinSendDlgItemMsg( hwnd, SB_PREAMP, SPBM_QUERYVALUE,
                         MPFROMP( &i ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
      cfg.rg_preamp_other = i / 10.0 - 12;
      save_ini();

      if( old_cfg.device     != cfg.device     ||
          old_cfg.lockdevice != cfg.lockdevice ||
          old_cfg.rg_enable  != cfg.rg_enable  ||
          old_cfg.devcaps    != cfg.devcaps    ||
          old_cfg.dither     != cfg.dither     ||
          old_cfg.numbuffers != cfg.numbuffers ||
          old_cfg.convert_hq != cfg.convert_hq ||
          old_cfg.dsd_type   != cfg.dsd_type   ||
          old_cfg.dsd_bits   != cfg.dsd_bits   ||
          old_cfg.dsd_ratio  != cfg.dsd_ratio  ||
          old_cfg.dsd_volume != cfg.dsd_volume )
      {
        if( opened ) {
          WinMessageBox( HWND_DESKTOP, hwnd,
              "Some settings will be applied after the current song is stopped.",
              "DART Output", 0, MB_WARNING | MB_OK | MB_MOVEABLE );
        }
      }

      break;
    }

    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case PB_UNDO:
          WinSendMsg( hwnd, WM_UPDATE_CONTROLS, 0, 0 );
          return 0;

        case PB_DEFAULT:
          lb_select( hwnd, CB_DEVICE, 0 );

          WinCheckButton( hwnd, CB_SHARED,    TRUE  );
          WinCheckButton( hwnd, CB_MASTERVOL, FALSE );
          WinCheckButton( hwnd, CB_RG_ENABLE, FALSE );

          WinSendDlgItemMsg( hwnd, SB_BUFFERS, SPBM_SETCURRENTVALUE, MPFROMLONG( 128 ), 0 );

          lb_select_by_handle( hwnd, CB_FORMAT, DEVCAP_DEFAULT & DEVCAP_OUT_DEPTH );
          lb_select_by_handle( hwnd, CB_DITHER, DSP_DITHER_NONE );
          lb_select( hwnd, CB_RG_TYPE, RG_PREFER_ALBUM );

          WinEnableControl( hwnd, CB_RG_TYPE,   FALSE );
          WinEnableControl( hwnd, SB_RG_PREAMP, FALSE );
          WinEnableControl( hwnd, ST_RG_PREAMP, FALSE );
          WinEnableControl( hwnd, SB_PREAMP,    FALSE );
          WinEnableControl( hwnd, ST_PREAMP,    FALSE );

          WinSendDlgItemMsg( hwnd, SB_RG_PREAMP, SPBM_SETCURRENTVALUE, MPFROMLONG( 150 ), 0 );
          WinSendDlgItemMsg( hwnd, SB_PREAMP,    SPBM_SETCURRENTVALUE, MPFROMLONG( 120 ), 0 );
          return 0;

        case PB_ADJUST:
          // Insure WinProcessDlg does not return immediately.
          WinSetWindowUShort( hadjust, QWS_FLAGS, (USHORT)( WinQueryWindowUShort( hadjust, QWS_FLAGS ) & ~FF_DLGDISMISSED ));
          WinSetWindowPos( hadjust, HWND_TOP, 0, 0, 0, 0, SWP_SHOW | SWP_ZORDER | SWP_ACTIVATE );
          WinProcessDlg( hadjust );
          return 0;
      }
      break;

    case WM_CONTROL:
      switch( SHORT1FROMMP(mp1)) {
        case CB_MASTERVOL:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED ) {
            cfg.mastervol = WinQueryButtonCheckstate( hwnd, CB_MASTERVOL );
          }
          break;

        case CB_DEVICE:
          if( SHORT2FROMMP(mp1) == CBN_EFCHANGE ) {
            ULONG device  = lb_selected( hwnd, CB_DEVICE, LIT_FIRST );
            ULONG devcaps = load_device_caps( hwnd, device );

            if( !lb_select_by_handle( hwnd, CB_FORMAT, devcaps & DEVCAP_OUT_DEPTH )) {
              lb_select_by_handle( hwnd, CB_FORMAT, DEVCAP_OUT_16BIT );
            }

            WinSendMsg( hadjust, WM_UPDATE_CONTROLS, MPFROMLONG( devcaps ), 0 );
          }
          break;

        case CB_FORMAT:
          if( SHORT2FROMMP(mp1) == CBN_EFCHANGE )
          {
            int i, out_depth;

            if(( i = lb_cursored( hwnd, CB_FORMAT )) != LIT_NONE ) {
              out_depth = lb_get_handle( hwnd, CB_FORMAT, i );

              WinEnableControl( hwnd, ST_DITHER, out_depth != DEVCAP_OUT_AS_IS );
              WinEnableControl( hwnd, CB_DITHER, out_depth != DEVCAP_OUT_AS_IS );

              WinEnableControl( hadjust, CB_DSD_BITS,
                                out_depth == DEVCAP_OUT_AS_IS );
            }
          }
          break;

        case CB_RG_ENABLE:
          if( SHORT2FROMMP(mp1) == BN_CLICKED || SHORT2FROMMP(mp1) == BN_DBLCLICKED )
          {
            int rg_enable = WinQueryButtonCheckstate( hwnd, CB_RG_ENABLE );

            WinEnableControl( hwnd, CB_RG_TYPE,   rg_enable );
            WinEnableControl( hwnd, SB_RG_PREAMP, rg_enable );
            WinEnableControl( hwnd, ST_RG_PREAMP, rg_enable );
            WinEnableControl( hwnd, SB_PREAMP,    rg_enable );
            WinEnableControl( hwnd, ST_PREAMP,    rg_enable );
            return 0;
          }
          break;

        case CB_RG_TYPE:
          if( SHORT2FROMMP(mp1) == CBN_EFCHANGE ) {
            cfg.rg_type = lb_selected( hwnd, CB_RG_TYPE, LIT_FIRST );
          }
          break;

        case SB_RG_PREAMP:
          if( SHORT2FROMMP(mp1) == SPBN_ENDSPIN )
          {
            int dB;

            WinSendDlgItemMsg( hwnd, SB_RG_PREAMP, SPBM_QUERYVALUE,
                               MPFROMP( &dB ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));

            cfg.rg_preamp = dB / 10.0f - 12;
          }
          break;

        case SB_PREAMP:
          if( SHORT2FROMMP(mp1) == SPBN_ENDSPIN )
          {
            int dB;

            WinSendDlgItemMsg( hwnd, SB_PREAMP, SPBM_QUERYVALUE,
                               MPFROMP( &dB ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));

            cfg.rg_preamp_other = dB / 10.0f - 12;
          }
          break;
      }
      break;
  }

  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in. */
void DLLENTRY
plugin_configure( HWND howner, HMODULE module )
{
  HWND  hwnd;
  char  buffer[64];
  int   i;
  float dB;

  for( dB = -12.0, i = 0; dB < 12.1; dB += 0.1, i++ ) {
    sprintf( buffer, "%+.1f dB", dB );
    dBList[i] = strdup( buffer );
  }

  hwnd = WinLoadDlg( HWND_DESKTOP, howner, cfg_dlg_proc, module, DLG_CONFIGURE, NULL );

  if( hwnd ) {
    WinProcessDlg( hwnd );
    WinDestroyWindow( hwnd );
  }

  for( dB = -12.0, i = 0; dB < 12.1; dB += 0.1, i++ ) {
    free( dBList[i] );
  }
}

void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_OUTPUT;
  param->author       = "Samuel Audet, Dmitry Steklenev";
  param->desc         = "DART Output " VER_STRING;
  param->configurable = TRUE;

  load_ini();
}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    _CRT_term();
  }
  return 1UL;
}
#endif
