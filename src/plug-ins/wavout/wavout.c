/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2006-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * PM123 WAVE Output plug-in.
 */

#define  INCL_PM
#define  INCL_DOS
#include <os2.h>
#include <os2me.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include <utilfct.h>
#include <format.h>
#include <output_plug.h>
#include <decoder_plug.h>
#include <plugin.h>
#include <debuglog.h>
#include <dsp.h>
#include "wavout.h"

static  WAVOUT_SETTINGS cfg;
static  int opened = 0;

#define WM_UPDATE_CONTROLS ( WM_USER + 1000  )

/* Closes a output file. */
static ULONG
output_close( void* A )
{
  WAVOUT* a = (WAVOUT*)A;
  ULONG resetcount;

  if( a->file )
  {
    if( fseek( a->file, 0, SEEK_SET ) == 0 ) {
      fwrite( &a->header, 1, sizeof( a->header ), a->file );
    }

    DEBUGLOG(( "wavout: close %s\n", a->fullpath ));
    fclose( a->file );
    a->file = NULL;

    DosResetEventSem( a->pause, &resetcount );

    free( a->buffer );
    a->buffer = NULL;

    if( a->dsp ) {
      dsp_conv_cleanup( a->dsp );
      a->dsp = NULL;
    }

    if( opened ) {
      --opened;
    }
  }
  return 0;
}

/* Opens a output file. */
static ULONG
output_open( WAVOUT* a )
{
  char  errorbuf[1024];
  ULONG rc = PLUGIN_OK;

  // New filename, even if we didn't explicity get a close!
  output_close( a );

  a->playingpos = 0;

  DosPostEventSem( a->pause );
  strcpy( a->fullpath, cfg.outpath );

  if( stricmp( a->fullpath, "nul" ) != 0 ) {
    if( *a->fullpath ) {
      if( mkcomplexdir( a->fullpath ) != 0 ) {
        strlcpy( errorbuf, "Could not create output directory:\n", sizeof( errorbuf ));
        strlcat( errorbuf, a->fullpath, sizeof( errorbuf ));
        strlcat( errorbuf, "\n", sizeof( errorbuf ));
        strlcat( errorbuf, strerror( errno ), sizeof( errorbuf ));

        a->params.error_display( errorbuf );
        return errno;
      }
    }
    if( *a->fullpath && !is_root( a->fullpath )) {
      strlcat( a->fullpath, "\\", sizeof( a->fullpath ));
      strlcat( a->fullpath, a->filename, sizeof( a->fullpath ));
    } else {
      strlcat( a->fullpath, a->filename, sizeof( a->fullpath ));
    }
  }

  // Check format of the input stream.
  switch( a->params.formatinfo.format ) {
    case WAVE_FORMAT_PCM:
      if( a->params.formatinfo.channels > 2  ||
          a->params.formatinfo.bits % 8 != 0 ||
          a->params.formatinfo.bits > 32 )
      {
        rc = EINVAL;
      }
      break;

    case WAVE_FORMAT_DSD:
      if( a->params.formatinfo.channels > 2  ||
          a->params.formatinfo.bits != 32 )
      {
        rc = EINVAL;
      }
      break;

    default:
      rc = EINVAL;
      break;
  }

  if( rc != PLUGIN_OK )
  {
    strlcpy( errorbuf, "Could not write output data to file:\n" , sizeof( errorbuf ));
    strlcat( errorbuf, a->fullpath, sizeof( errorbuf ));
    strlcat( errorbuf, "\n", sizeof( errorbuf ));
    strlcat( errorbuf, "Unsupported format of the input stream.", sizeof( errorbuf ));

    a->params.error_display( errorbuf );
    return rc;
  }

  // Open the output file.
  if(( a->file = fopen( a->fullpath, "wb" )) == NULL )
  {
    strlcpy( errorbuf, "Could not open file to output data:\n", sizeof( errorbuf ));
    strlcat( errorbuf, a->fullpath, sizeof( errorbuf ));
    strlcat( errorbuf, "\n", sizeof( errorbuf ));
    strlcat( errorbuf, strerror( errno ), sizeof( errorbuf ));

    a->params.error_display( errorbuf );
    return errno;
  }

  //  Write WAVE file header.
  memcpy( a->header.riff.id_riff, "RIFF", 4 );
  memcpy( a->header.riff.id_wave, "WAVE", 4 );
  a->header.riff.len = sizeof( a->header ) - 8;

  memcpy( a->header.format_header.id, "fmt ", 4 );
  a->header.format_header.len = sizeof( a->header.format_header ) +
                                sizeof( a->header.format ) - 8;

  a->header.format.FormatTag      = WAVE_FORMAT_PCM;
  a->header.format.Channels       = a->params.formatinfo.channels;
  a->header.format.SamplesPerSec  = a->params.formatinfo.samplerate;
  a->header.format.BitsPerSample  = a->params.formatinfo.bits;

  switch( a->params.formatinfo.format ) {
    case WAVE_FORMAT_DSD:
      if( cfg.samplerate == -1 ) {
        a->header.format.SamplesPerSec =
          a->params.formatinfo.samplerate * a->params.formatinfo.bits / cfg.dsd_ratio;

        if( a->header.format.SamplesPerSec > 352800 ) {
          a->header.format.SamplesPerSec = 352800;
        }
      } else {
        a->header.format.SamplesPerSec = cfg.samplerate;
      }

      if( cfg.bps == -1 ) {
        a->header.format.BitsPerSample = cfg.dsd_bits;
      } else {
        a->header.format.BitsPerSample = cfg.bps;
      }
      break;

    case WAVE_FORMAT_PCM:
      if( cfg.samplerate != -1 ) {
        a->header.format.SamplesPerSec = cfg.samplerate;
      }
      if( cfg.bps != -1 ) {
        a->header.format.BitsPerSample = cfg.bps;
      }
      break;
  }

  a->header.format.AvgBytesPerSec = a->header.format.Channels *
                                    a->header.format.SamplesPerSec *
                                    a->header.format.BitsPerSample / 8;
  a->header.format.BlockAlign     = a->header.format.Channels *
                                    a->header.format.BitsPerSample / 8;

  memcpy( a->header.data_header.id, "data", 4 );
  a->header.data_header.len = 0;

  DEBUGLOG(( "wavout: output file have %ld bit audio format 0x%04X, samplerate %ld, %ld channels.\n",
    a->header.format.BitsPerSample, a->header.format.FormatTag, a->header.format.SamplesPerSec, a->header.format.Channels ));

  if( fwrite( &a->header, 1, sizeof( a->header ), a->file ) != sizeof( a->header ))
  {
    strlcpy( errorbuf, "Could not write output data to file:\n", sizeof( errorbuf ));
    strlcat( errorbuf, a->fullpath, sizeof( errorbuf ));
    strlcat( errorbuf, "\n", sizeof( errorbuf ));
    strlcat( errorbuf, strerror( errno ), sizeof( errorbuf ));

    a->params.error_display( errorbuf );

    fclose( a->file );
    return errno;
  }

  if( a->header.format.BitsPerSample == 8 ) {
    a->zero = 128;
  } else {
    a->zero = 0;
  }

  // Initialize DSP library if necessary.
  if( a->header.format.FormatTag     != a->params.formatinfo.format     ||
      a->header.format.SamplesPerSec != a->params.formatinfo.samplerate ||
      a->header.format.BitsPerSample != a->params.formatinfo.bits       )
  {
    int samples = a->params.buffersize / a->params.formatinfo.channels / ( a->params.formatinfo.bits / 8 );
    int max_out = ceil((double)( samples + 1 ) * a->header.format.SamplesPerSec / a->params.formatinfo.samplerate );
    int options = cfg.dsd_type;
    int bufsize = max_out * ( a->header.format.BitsPerSample / 8 ) * a->header.format.Channels;

    FORMAT_INFO frm_out = { sizeof( FORMAT_INFO )};

    DEBUGLOG(( "wavout: maximal input/output samples according the buffer size is %d/%d\n", samples, max_out ));
    a->buffer = malloc( bufsize );
    a->buffersize = bufsize;

    if( !a->buffer ) {
      strlcpy( errorbuf, "Not enough memory\n", sizeof( errorbuf ));
      a->params.error_display( errorbuf );
      output_close( a );
      return errno;
    }

    frm_out.format     = a->header.format.FormatTag;
    frm_out.samplerate = a->header.format.SamplesPerSec;
    frm_out.channels   = a->header.format.Channels;
    frm_out.bits       = a->header.format.BitsPerSample;

    if( cfg.bps != -1 ) {
      options |= cfg.dither;
    }

    a->dsp = dsp_conv_init( &a->params.formatinfo, &frm_out, samples, 1.0, options );

    if( !a->dsp ) {
      strlcpy( errorbuf, "Unable initialize the DSP library.\n", sizeof( errorbuf ));
      a->params.error_display( errorbuf );
      output_close( a );
      return errno;
    }
  } else {
    a->buffer = (char*)malloc( a->params.buffersize );
    a->buffersize = a->params.buffersize;
  }

  ++opened;
  memset( a->buffer, a->zero, a->buffersize );

  return PLUGIN_OK;
}

/* Pauses a output file. */
static ULONG
output_pause( void* A, BOOL pause )
{
  WAVOUT* a = (WAVOUT*)A;
  ULONG   resetcount;

  if( pause ) {
    return DosResetEventSem( a->pause, &resetcount );
  } else {
    return DosPostEventSem( a->pause );
  }
}

/* Processing of a command messages. */
ULONG DLLENTRY
output_command( void* A, ULONG msg, OUTPUT_PARAMS* info )
{
  WAVOUT* a = (WAVOUT*)A;

  switch( msg ) {
    case OUTPUT_OPEN:
    {
      sfname( a->filename, info->filename, sizeof( a->filename ));
      if( !*a->filename ) {
        strcpy( a->filename, "wavout" );
      }
      if( is_url( info->filename )) {
        sdecode( a->filename, a->filename, sizeof( a->filename ));
      }
      if( is_cuesheet( info->filename ))
      {
        int len = strlen( a->filename );
        snprintf( a->filename + len, sizeof( a->filename ) - len, ",%02d", strack( info->filename ));
      }
      strlcat( a->filename, ".wav", sizeof( a->filename ));
      return output_open( a );
    }

    case OUTPUT_CLOSE:
      return output_close( a );

    case OUTPUT_VOLUME:
      return 0;

    case OUTPUT_PAUSE:
      return output_pause( a, info->pause );

    case OUTPUT_SETUP:
      info->always_hungry = TRUE;
      a->params = *info;
      return 0;

    case OUTPUT_TRASH_BUFFERS:
      return 0;

    case OUTPUT_NOBUFFERMODE:
      return 0;
  }

  return MCIERR_UNSUPPORTED_FUNCTION;
}

/* This function is called by the decoder or last in chain
   filter plug-in to play samples. */
int DLLENTRY
output_play_samples( void* A, FORMAT_INFO* format, char* buf, int len, int posmarker )
{
  WAVOUT *a = (WAVOUT*)A;
  int size;

  DosWaitEventSem( a->pause, SEM_INDEFINITE_WAIT );

  a->playingpos = posmarker;

  if( a->dsp ) {
    int samples = len / a->params.formatinfo.channels / ( a->params.formatinfo.bits / 8 );
    samples = dsp_convert( a->dsp, buf, a->buffer, samples );

    if( samples > 0 ) {
      buf  = a->buffer;
      size = samples * a->header.format.Channels * ( a->header.format.BitsPerSample / 8 );
    } else {
      return 0;
    }
  } else {
    memcpy( a->buffer, buf, len );
    size = len;
  }

  if( fwrite( buf, 1, size, a->file ) != size )
  {
    char errorbuf[1024];

    strlcpy( errorbuf, "Could not write output data  to file:\n", sizeof( errorbuf ));
    strlcat( errorbuf, a->fullpath, sizeof( errorbuf ));
    strlcat( errorbuf, "\n", sizeof( errorbuf ));
    strlcat( errorbuf, strerror( errno ), sizeof( errorbuf ));

    DEBUGLOG(( "%s\n", errorbuf ));
    a->params.error_display( errorbuf );
    output_close( a );
    return 0;
  }

  a->header.riff.len += size;
  a->header.data_header.len += size;
  a->buffersize = size;

  return len;
}

/* This function is used by visual plug-ins so the user can
   visualize what is currently being played. */
ULONG DLLENTRY
output_playing_samples( void* A, FORMAT_INFO* info, char* buf, int len )
{
  WAVOUT* a = (WAVOUT*)A;

  info->bits       = a->header.format.BitsPerSample;
  info->samplerate = a->header.format.SamplesPerSec;
  info->channels   = a->header.format.Channels;
  info->format     = a->header.format.FormatTag;

  if( buf ) {
    if( len > a->buffersize ) {
      memcpy( buf, a->buffer, a->buffersize );
      memset( buf + a->buffersize, a->zero, len - a->buffersize );
    } else {
      memcpy( buf, a->buffer, len );
    }
  }

  return PLUGIN_OK;
}

/* Returns the playing position. */
ULONG DLLENTRY
output_playing_pos( void* A )
{
  return ((WAVOUT*)A)->playingpos;
}

/* Returns TRUE if the output plug-in still has some buffers to play. */
BOOL DLLENTRY
output_playing_data( void* A )
{
  return FALSE;
}

/* Initialize the output plug-in. */
ULONG DLLENTRY
output_init( void** A )
{
  WAVOUT* a;

 *A = calloc( sizeof( WAVOUT ), 1 );
  a = (WAVOUT*)*A;

  DosCreateEventSem( NULL, &a->pause, 0, TRUE );
  return PLUGIN_OK;
}

/* Uninitialize the output plug-in. */
ULONG DLLENTRY
output_uninit( void* A )
{
  WAVOUT* a = (WAVOUT*)A;

  DosCloseEventSem( a->pause );
  free( a );

  return PLUGIN_OK;
}

/* Saves plug-in's settings. */
static void
save_ini( void )
{
  HINI hini;

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, cfg.outpath );
    save_ini_value( hini, cfg.bps );
    save_ini_value( hini, cfg.dither );
    save_ini_value( hini, cfg.samplerate );
    save_ini_value( hini, cfg.dsd_type );
    save_ini_value( hini, cfg.dsd_bits );
    save_ini_value( hini, cfg.dsd_ratio );

    close_ini( hini );
  }
}

/* Loads plug-in's settings. */
static void
load_ini( void )
{
  HINI hini;

 *cfg.outpath    =  0;
  cfg.bps        = -1;
  cfg.dither     = DSP_DITHER_NONE;
  cfg.samplerate = -1;
  cfg.dsd_type   = DSP_DSDPCM_MULTISTAGE;
  cfg.dsd_bits   = 16;
  cfg.dsd_ratio  = 32;

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, cfg.outpath );
    load_ini_value( hini, cfg.bps );
    load_ini_value( hini, cfg.dither );
    load_ini_value( hini, cfg.samplerate );
    load_ini_value( hini, cfg.dsd_type );
    load_ini_value( hini, cfg.dsd_bits );
    load_ini_value( hini, cfg.dsd_ratio );

    close_ini( hini );
  }
}

/* Default dialog procedure for the directorys browse dialog. */
MRESULT EXPENTRY
cfg_file_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  FILEDLG* filedialog =
    (FILEDLG*)WinQueryWindowULong( hwnd, QWL_USER );

  switch( msg )
  {
    case WM_INITDLG:
      WinEnableControl( hwnd, DID_OK, TRUE  );
      do_warpsans( hwnd );
      break;

    case WM_CONTROL:
      if( SHORT1FROMMP(mp1) == DID_FILENAME_ED && SHORT2FROMMP(mp1) == EN_CHANGE ) {
        // Prevents DID_OK from being greyed out.
        return 0;
      }
      break;

    case WM_COMMAND:
      if( SHORT1FROMMP(mp1) == DID_OK )
      {
        if( !is_root( filedialog->szFullFile )) {
          filedialog->szFullFile[strlen(filedialog->szFullFile)-1] = 0;
        }

        filedialog->lReturn    = DID_OK;
        filedialog->ulFQFCount = 1;

        WinDismissDlg( hwnd, DID_OK );
        return 0;
      }
      break;
  }
  return WinDefFileDlgProc( hwnd, msg, mp1, mp2 );
}

/* Processes messages of the configuration dialog. */
MRESULT EXPENTRY cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static HMODULE module;

  switch( msg ) {
    case WM_INITDLG:
      module = (HMODULE)mp2;
      do_warpsans( hwnd );

      lb_add_with_handle( hwnd, CB_BITS, "As is",   -1 );
      lb_add_with_handle( hwnd, CB_BITS, "08 bits",  8 );
      lb_add_with_handle( hwnd, CB_BITS, "16 bits", 16 );
      lb_add_with_handle( hwnd, CB_BITS, "24 bits", 24 );
      lb_add_with_handle( hwnd, CB_BITS, "32 bits", 32 );

      lb_add_with_handle( hwnd, CB_DITHER, "None",        DSP_DITHER_NONE );
      lb_add_with_handle( hwnd, CB_DITHER, "Rectangular", DSP_DITHER_RECTANGULAR );
      lb_add_with_handle( hwnd, CB_DITHER, "Triangular",  DSP_DITHER_TRIANGULAR );
      lb_add_with_handle( hwnd, CB_DITHER, "Shaped",      DSP_DITHER_SHAPED );

      lb_add_with_handle( hwnd, CB_SAMPLERATE, "As is",       -1 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "8000",     8000 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "11025",   11025 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "16000",   16000 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "22050",   22050 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "24000",   24000 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "32000",   32000 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "44100",   44100 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "48000",   48000 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "88200",   88200 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE,  "96000",   96000 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE, "176400",  176400 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE, "192000",  192400 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE, "352800",  352800 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE, "384000",  384000 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE, "705600",  705600 );
      lb_add_with_handle( hwnd, CB_SAMPLERATE, "768000",  768000 );

      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Multistage (64fp)", DSP_DSDPCM_MULTISTAGE | DSP_DSDPCM_FP64 );
      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Multistage (32fp)", DSP_DSDPCM_MULTISTAGE );
      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Direct (64fp)",     DSP_DSDPCM_DIRECT | DSP_DSDPCM_FP64 );
      lb_add_with_handle( hwnd, CB_DSD_TYPE, "Direct (32fp)",     DSP_DSDPCM_DIRECT );

      lb_add_with_handle( hwnd, CB_DSD_BITS, "08 bits",  8 );
      lb_add_with_handle( hwnd, CB_DSD_BITS, "16 bits", 16 );
      lb_add_with_handle( hwnd, CB_DSD_BITS, "24 bits", 24 );
      lb_add_with_handle( hwnd, CB_DSD_BITS, "32 bits", 32 );

      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/8",   8 );
      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/16", 16 );
      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/32", 32 );
      lb_add_with_handle( hwnd, CB_DSD_RATIO, "1/64", 64 );

      // continue to WM_UPDATE_CONTROLS...

    case WM_UPDATE_CONTROLS:
      WinSetDlgItemText( hwnd, EF_FILENAME, cfg.outpath );

      if( !lb_select_by_handle( hwnd, CB_BITS, cfg.bps )) {
        lb_select_by_handle( hwnd, CB_BITS, -1 );
      }
      if( !lb_select_by_handle( hwnd, CB_DITHER, cfg.dither )) {
        lb_select_by_handle( hwnd, CB_DITHER, DSP_DITHER_NONE );
      }
      if( !lb_select_by_handle( hwnd, CB_SAMPLERATE, cfg.samplerate )) {
        lb_select_by_handle( hwnd, CB_SAMPLERATE, -1 );
      }
      if( !lb_select_by_handle( hwnd, CB_DSD_TYPE, cfg.dsd_type )) {
        lb_select_by_handle( hwnd, CB_DSD_TYPE, DSP_DSDPCM_MULTISTAGE );
      }
      if( !lb_select_by_handle( hwnd, CB_DSD_BITS, cfg.dsd_bits )) {
        lb_select_by_handle( hwnd, CB_DSD_BITS, 16 );
      }
      if( !lb_select_by_handle( hwnd, CB_DSD_RATIO, cfg.dsd_ratio )) {
        lb_select_by_handle( hwnd, CB_DSD_RATIO, 32 );
      }

      WinEnableControl( hwnd, ST_DITHER,    cfg.bps != -1 );
      WinEnableControl( hwnd, CB_DITHER,    cfg.bps != -1 );
      WinEnableControl( hwnd, CB_DSD_BITS,  cfg.bps == -1 );
      WinEnableControl( hwnd, ST_DSD_RATIO, cfg.samplerate == -1 );
      WinEnableControl( hwnd, CB_DSD_RATIO, cfg.samplerate == -1 );
      return 0;

    case WM_DESTROY:
    {
      int i;
      WAVOUT_SETTINGS old_cfg = cfg;

      WinQueryDlgItemText( hwnd, EF_FILENAME, sizeof( cfg.outpath ), cfg.outpath );

      if( *cfg.outpath && cfg.outpath[ strlen( cfg.outpath ) - 1 ] == '\\' && !is_root( cfg.outpath )) {
        cfg.outpath[ strlen( cfg.outpath ) - 1 ] = 0;
      }

      if(( i = lb_cursored( hwnd, CB_BITS )) != LIT_NONE ) {
        cfg.bps = lb_get_handle( hwnd, CB_BITS, i );
      }
      if(( i = lb_cursored( hwnd, CB_DITHER )) != LIT_NONE ) {
        cfg.dither = lb_get_handle( hwnd, CB_DITHER, i );
      }
      if(( i = lb_cursored( hwnd, CB_SAMPLERATE )) != LIT_NONE ) {
        cfg.samplerate = lb_get_handle( hwnd, CB_SAMPLERATE, i );
      }
      if(( i = lb_cursored( hwnd, CB_DSD_TYPE )) != LIT_NONE ) {
        cfg.dsd_type = lb_get_handle( hwnd, CB_DSD_TYPE, i );
      }
      if(( i = lb_cursored( hwnd, CB_DSD_BITS )) != LIT_NONE ) {
        cfg.dsd_bits = lb_get_handle( hwnd, CB_DSD_BITS, i );
      }
      if(( i = lb_cursored( hwnd, CB_DSD_RATIO )) != LIT_NONE ) {
        cfg.dsd_ratio = lb_get_handle( hwnd, CB_DSD_RATIO, i );
      }

      save_ini();

      if( old_cfg.bps        != cfg.bps        ||
          old_cfg.dither     != cfg.dither     ||
          old_cfg.samplerate != cfg.samplerate ||
          old_cfg.dsd_type   != cfg.dsd_type   ||
          old_cfg.dsd_bits   != cfg.dsd_bits   ||
          old_cfg.dsd_ratio  != cfg.dsd_ratio  ||
          strcmp( old_cfg.outpath, cfg.outpath ))
      {
        if( opened ) {
          WinMessageBox( HWND_DESKTOP, hwnd,
              "Some settings will be applied after the current song is stopped.",
              "WAVE Output", 0, MB_WARNING | MB_OK | MB_MOVEABLE );
        }
      }

      break;
    }

    case WM_COMMAND:
      switch( SHORT1FROMMP( mp1 )) {
        case PB_BROWSE:
        {
          FILEDLG filedialog;

          memset( &filedialog, 0, sizeof( FILEDLG ));
          filedialog.cbSize     = sizeof( FILEDLG );
          filedialog.fl         = FDS_CENTER | FDS_OPEN_DIALOG | FDS_CUSTOM;
          filedialog.pszTitle   = "Output directory";
          filedialog.hMod       = module;
          filedialog.usDlgId    = DLG_BROWSE;
          filedialog.pfnDlgProc = cfg_file_dlg_proc;

          WinQueryDlgItemText( hwnd, EF_FILENAME,
                               sizeof( filedialog.szFullFile ), filedialog.szFullFile );

          if( *filedialog.szFullFile &&
               filedialog.szFullFile[ strlen( filedialog.szFullFile ) - 1 ] != '\\' )
          {
            strcat( filedialog.szFullFile, "\\" );
          }

          WinFileDlg( HWND_DESKTOP, hwnd, &filedialog );

          if( filedialog.lReturn == DID_OK ) {
            WinSetDlgItemText( hwnd, EF_FILENAME, filedialog.szFullFile );
          }
          return 0;
        }

        case PB_UNDO:
          WinSendMsg( hwnd, WM_UPDATE_CONTROLS, 0, 0 );
          return 0;

        case PB_DEFAULT:
          WinSetDlgItemText( hwnd, EF_FILENAME, "" );
          lb_select_by_handle( hwnd, CB_BITS, -1 );
          lb_select_by_handle( hwnd, CB_DITHER, DSP_DITHER_NONE );
          lb_select_by_handle( hwnd, CB_SAMPLERATE, -1 );
          lb_select_by_handle( hwnd, CB_DSD_TYPE, DSP_DSDPCM_MULTISTAGE );
          lb_select_by_handle( hwnd, CB_DSD_BITS, 16 );
          lb_select_by_handle( hwnd, CB_DSD_RATIO, 32 );
          return 0;
      }
      break;

    case WM_CONTROL:
      switch( SHORT1FROMMP(mp1)) {
        case CB_BITS:
          if( SHORT2FROMMP(mp1) == CBN_EFCHANGE )
          {
            int i, bps;

            if(( i = lb_cursored( hwnd, CB_BITS )) != LIT_NONE ) {
              bps = lb_get_handle( hwnd, CB_BITS, i );

              WinEnableControl( hwnd, ST_DITHER,   bps != -1 );
              WinEnableControl( hwnd, CB_DITHER,   bps != -1 );
              WinEnableControl( hwnd, CB_DSD_BITS, bps == -1 );
            }
          }
          break;

        case CB_SAMPLERATE:
          if( SHORT2FROMMP(mp1) == CBN_EFCHANGE )
          {
            int i, samplerate;

            if(( i = lb_cursored( hwnd, CB_SAMPLERATE )) != LIT_NONE ) {
              samplerate = lb_get_handle( hwnd, CB_SAMPLERATE, i );
              WinEnableControl( hwnd, ST_DSD_RATIO, samplerate == -1 );
              WinEnableControl( hwnd, CB_DSD_RATIO, samplerate == -1 );
            }
          }
          break;
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in. */
void DLLENTRY
plugin_configure( HWND hwnd, HMODULE module ) {
  WinDlgBox( HWND_DESKTOP, hwnd, cfg_dlg_proc, module, DLG_CONFIGURE, (PVOID)module );
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* query )
{
  query->type         = PLUGIN_OUTPUT;
  query->author       = "Samuel Audet, Dmitry Steklenev ";
  query->desc         = "WAVE Output " VER_STRING;
  query->configurable = TRUE;

  load_ini();
}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    _CRT_term();
  }
  return 1UL;
}
#endif
