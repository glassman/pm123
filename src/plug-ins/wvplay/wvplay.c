/*
 * Copyright 2013-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_WIN
#define  INCL_ERRORS
#include <os2.h>
#include <process.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <decoder_plug.h>
#include <plugin.h>
#include <debuglog.h>
#include <charset.h>
#include <utilfct.h>
#include <wavpack.h>
#include "wvplay.h"

static DECODER_STRUCT** instances = NULL;
static int  instances_count = 0;
static HMTX mutex;

#define MAX_ERRMSG  128

/* Reads up to count bytes from the input file and stores them in the
   given buffer. The position in the file increases by the number of
   bytes read. Returns the number of bytes successfully read, which can
   be less than count if an error occurs or if the end-of-file is met
   before reaching count. */
static int32_t
vio_read_bytes( void* x, void* data, int32_t count ) {
  return xio_fread( data, 1, count, x );
}

/* Finds the current position of the file. Returns the current file
   position. On error, returns -1L and sets errno to a nonzero value. */
static uint32_t
vio_get_pos( void* x ) {
  return xio_ftell( x );
}

/* Changes the current file position to a new location within the file.
   Returns 0 if it successfully moves the pointer. A nonzero return
   value indicates an error. On devices that cannot seek the return
   value is nonzero. */
static int
vio_set_pos_abs( void* x, uint32_t pos ) {
  return xio_fseek( x, pos, XIO_SEEK_SET );
}

static int
vio_set_pos_rel( void* x, int32_t delta, int mode ) {
  return xio_fseek( x, delta, mode );
}

/* Pushes the byte c back onto the given input stream. However, only
   one sequential byte is guaranteed to be pushed back onto the input
   stream if you call ungetc consecutively. */
static int
vio_push_back_byte( void* x, int c ) {
  return xio_ungetc( c, x );
}

/* Returns the size of the file. */
static uint32_t
vio_get_length( void* x ) {
  uint32_t len = xio_fsize( x );

  if( len == -1 ) {
    len = 0;
  }

  return len;
}

/* Returns 0 on streams incapable of seeking, 1 on streams
   capable of seeking. */
static int
vio_can_seek( void* x ) {
  return ( xio_can_seek( x ) >= XIO_CAN_SEEK );
}

/* Writes up to bcount bytes from buffer to the output file. Returns
   the number of bytes successfully written, which can be fewer than
   bcount if an error occurs. */
static int32_t
vio_write_bytes( void* x, void *data, int32_t bcount ) {
  return xio_fwrite( data, 1, bcount, x );
}

/* Closes a pointed file. */
static void DLLENTRY
vio_close( DECODER_STRUCT* w )
{
  if( w->file ) {
    xio_fclose( w->file );
    w->file = NULL;
  }
  if( w->file_wvc ) {
    xio_fclose( w->file_wvc );
    w->file_wvc = NULL;
  }
}

static BOOL
wv_flush_output( DECODER_STRUCT* w )
{
  int bitrate = (int)WavpackGetInstantBitrate( w->wpc) / 1000;

  if( bitrate > 0 && w->bitrate != bitrate ) {
    WinPostMsg( w->hwnd, WM_CHANGEBR, MPFROMLONG( bitrate ), 0 );
    w->bitrate = bitrate;
  }

  if( w->output_play_samples( w->a, &w->output_format, w->buffer, w->bufpos, w->posmarker ) != w->bufpos ) {
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    return FALSE;
  }

  w->bufpos = 0;
  return TRUE;
}

/* Opens WavPack file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static ULONG
wv_open( DECODER_STRUCT* w, int flags, char* error )
{
  char ext[_MAX_EXT];

  static WavpackStreamReader freader = {
    vio_read_bytes, vio_get_pos,  vio_set_pos_abs, vio_set_pos_rel, vio_push_back_byte,
    vio_get_length, vio_can_seek, vio_write_bytes
  };

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if(( w->file = xio_fopen( w->filename, (flags & OPEN_EDIT_TAGS) ? "r+b" : "rb" )) == NULL ) {
    DosReleaseMutexSem( w->mutex );
    DEBUGLOG(( "wvplay: unable to open %s\n", w->filename ));
    strlcpy( error, xio_strerror( xio_errno()), MAX_ERRMSG );
    return xio_errno();
  }

  if(( flags & OPEN_WVC ) && stricmp( sfext( ext, w->filename, sizeof( ext )), ".WV" ) == 0 )
  {
    char* wvc_filename = malloc( strlen( w->filename ) + 10 );

    strcpy( wvc_filename, w->filename );
    strcat( wvc_filename, "c" );
    w->file_wvc = xio_fopen( wvc_filename, "rb" );
    DEBUGLOG(( "wvplay: try to open %s, rc=%08X\n", wvc_filename, w->file_wvc == NULL ));
  }

  w->wpc = WavpackOpenFileInputEx( &freader, w->file, w->file_wvc, error, flags, 0 );

  if( !w->wpc ) {
    vio_close( w );
    DEBUGLOG2(( "wvplay: unable to recognize %s, because %s\n", w->filename, error ));
    DosReleaseMutexSem( w->mutex );
    return -1;
  }

  w->output_format.size       = sizeof( w->output_format );
  w->output_format.format     = WAVE_FORMAT_PCM;
  w->output_format.channels   = WavpackGetNumChannels( w->wpc );
  w->output_format.samplerate = WavpackGetSampleRate( w->wpc );

  w->songlength = samples2ms( WavpackGetNumSamples( w->wpc ));
  w->bitrate    = WavpackGetAverageBitrate( w->wpc, flags & OPEN_WVC ? 1 : 0 ) / 1000;
  w->bps        = WavpackGetBitsPerSample( w->wpc );
  w->seekable   = xio_can_seek( w->file );

  if( w->bps <= 8 ) {
    w->output_format.bits = 8;
  } else if( w->bps <= 16 ) {
    w->output_format.bits = 16;
  } else if( w->bps <= 24 ) {
    w->output_format.bits = 24;
  } else {
    w->output_format.bits = 32;
  }

  DosReleaseMutexSem( w->mutex );
  return 0;
}

/* Closes WavPack file. */
static void
wv_close( DECODER_STRUCT* w )
{
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  WavpackCloseFile( w->wpc );
  vio_close( w );
  DosReleaseMutexSem( w->mutex );
}

/* Returns a specified tag item. */
static char*
wv_get_string( DECODER_STRUCT* w, char* target, char* type, int size )
{
  char* buffer = malloc( size * 5 );
  int rc;

 *target = 0;

  if( buffer ) {
    DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

    if(( rc = WavpackGetTagItem( w->wpc, type, buffer, size * 5 )) > 0 ) {
      ch_convert( CH_UTF_8, buffer, CH_DEFAULT, target, size );
    }

    DosReleaseMutexSem( w->mutex );
    free( buffer );
  }
  return target;
}

/* Sets a specified tag item. */
static void
wv_set_string( DECODER_STRUCT* w, char* source, char* type )
{
  int   size   = strlen( source ) * 4 + 1;
  char* string = (char*)malloc( size );

  if( string ) {
    ch_convert( CH_DEFAULT, source, CH_UTF_8, string, size );
    WavpackAppendTagItem( w->wpc, type, string, strlen( string ));
    free( string );
  }
}

/* Notify an error. */
static void
wv_show_error( DECODER_STRUCT* w, int style, const char* msg, const char* what, int rc )
{
  char buff[1024];

  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );


  if( what && *what ) {
    strlcat( buff, what, sizeof( buff ));
  } else if( rc == -1 ) {
    strlcat( buff, "Unsupported format of the file.", sizeof( buff ));
  } else if( xio_errno()) {
    strlcat( buff, xio_strerror( xio_errno()), sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  #define READ_SAMPLES_AT_ONCE 4096

  ULONG    rc;
  char     error[MAX_ERRMSG] = "";
  int32_t* samples = NULL;
  ULONG    resetcount;
  int      pos;
  uint32_t read;
  uint32_t done;
  int      ch, i;
  int      channels;
  int      mdfloat;
  int      bytespersample;
  uint32_t scale;

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  w->frew = FALSE;
  w->ffwd = FALSE;

  if(( rc = wv_open( w, OPEN_WVC | OPEN_NORMALIZE, error )) != 0 ) {
    wv_show_error( w, MB_ERROR, "Unable open file", error, rc );
    goto end;
  }

  scale = 1 << ( w->output_format.bits - w->bps );
  bytespersample = w->output_format.bits / 8;

  // After opening a new file we so are in its beginning.
  if( w->jumptopos == 0 && w->startpos == 0 ) {
    w->jumptopos = -1;
  }

  if(( w->buffer = (char*)malloc( w->audio_buffersize )) == NULL ) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }
  if(( samples = (int32_t*)malloc( w->output_format.channels * sizeof( int32_t ) * READ_SAMPLES_AT_ONCE )) == NULL ) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  channels = w->output_format.channels;
  mdfloat  = WavpackGetMode( w->wpc ) & MODE_FLOAT;

  DEBUGLOG(( "wvplay: start playing %d channels, bps = %d, float = %d\n", w->output_format.channels, w->bps, mdfloat != 0 ));

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    w->bufpos = 0;

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop ) {
      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

      if( w->jumptopos >= 0 )
      {
        uint32_t pos = ms2samples( w->jumptopos + w->startpos );
        DEBUGLOG(( "wvplay: jumps to %d ms (%u samples)\n", w->jumptopos + w->startpos, pos ));
        rc = WavpackSeekSample( w->wpc, pos );
        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        if( !rc ) {
          wv_show_error( w, MB_WARNING, "Unable seek in file", NULL, xio_errno());
        }

        w->jumptopos = -1;
      }
      else if( w->ffwd || w->frew )
      {
        // The skip of READ_SAMPLES_AT_ONCE * 10 interval will force the decoder to be
        // accelerated approximately in 10 times.
        uint32_t pos = WavpackGetSampleIndex( w->wpc );

        if( w->frew ) {
          if( pos < READ_SAMPLES_AT_ONCE * 11 ||
              pos - READ_SAMPLES_AT_ONCE * 11 < ms2samples( w->startpos ) ||
             !WavpackSeekSample( w->wpc, pos - READ_SAMPLES_AT_ONCE * 11 ))
          {
            break;
          }
        } else if( w->ffwd ) {
          if( !WavpackSeekSample( w->wpc, pos + READ_SAMPLES_AT_ONCE * 9  )) {
            break;
          }
        }
      }

      pos  = samples2ms( WavpackGetSampleIndex( w->wpc ));
      read = READ_SAMPLES_AT_ONCE;

      if( w->endpos ) {
        if( pos >= w->endpos ) {
          break;
        }
        if(( read = ms2samples( w->endpos - pos )) > READ_SAMPLES_AT_ONCE ) {
          read = READ_SAMPLES_AT_ONCE;
        }
      }

      done = WavpackUnpackSamples( w->wpc, (int32_t*)samples, read );

      for( i = 0; i < done; i++ )
      {
        if( !w->bufpos ) {
          w->posmarker = pos - w->startpos;
        }

        for( ch = 0; ch < channels; ch++ )
        {
          int32_t sample;

          if( mdfloat )
          {
            float fd = *((float*)samples + i * channels + ch );

            if( fd >= 1.0 ) {
              sample = INT32_MAX;
            } else if ( fd <= -1.0 ) {
              sample = INT32_MIN;
            } else {
              sample = floor( fd * INT32_MAX );
            }
          } else {
            sample = *( samples + channels * i + ch ) * scale;
          }
          switch( w->output_format.bits ) {
            case  8: PUTSMPL8U( w->buffer + w->bufpos, sample ); break;
            case 16: PUTSMPL16( w->buffer + w->bufpos, sample ); break;
            case 24: PUTSMPL24( w->buffer + w->bufpos, sample ); break;
            case 32: PUTSMPL32( w->buffer + w->bufpos, sample ); break;
          }

          w->bufpos += bytespersample;

          if( w->bufpos == w->audio_buffersize ) {
            wv_flush_output( w );
          }
        }
      }

      DosReleaseMutexSem( w->mutex );

      if( done < read ) {
        if( xio_ferror( w->file ) || ( w->file_wvc && xio_ferror( w->file_wvc )))
        {
          wv_show_error( w, MB_ERROR, "Unable read file", NULL, xio_errno());
          goto end;
        } else {
          DEBUGLOG(( "wvplay: break at %d ms because end of file is reached\n",
                     samples2ms( WavpackGetSampleIndex( w->wpc )), WavpackGetNumErrors( w->wpc )));
          break;
        }
      }
    }

    wv_flush_output( w );
    w->status = DECODER_STOPPED;
    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
  }

  w->status = DECODER_STOPPED;

end:

  wv_close( w );
  free( w->buffer );
  free( samples );
  w->buffer = NULL;

  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );
  DosCreateMutexSem( NULL, &w->mutex, 0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;
  w->file = NULL;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  instances = realloc( instances, ++instances_count * sizeof( DECODER_STRUCT* ));
  instances[ instances_count - 1 ] = w;
  DosReleaseMutexSem( mutex );

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than this is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  int i;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  decoder_command( w, DECODER_STOP, NULL );

  for( i = 0; i < instances_count; i++ ) {
    if( instances[i] == w ) {
      if( i < instances_count - 1 ) {
        memmove( instances + i, instances + i + 1,
               ( instances_count - i - 1 ) * sizeof( DECODER_STRUCT* ));
      }
      instances = realloc( instances, --instances_count * sizeof( DECODER_STRUCT* ));
    }
  }

  DosReleaseMutexSem( mutex  );
  DosCloseEventSem( w->play  );
  DosCloseMutexSem( w->mutex );

  free( w );
  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = arg;

  switch( msg )
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
      strlcpy( w->filename, info->filename, sizeof( w->filename ));
      DosReleaseMutexSem( w->mutex );

      w->startpos   = info->start;
      w->endpos     = info->end;
      w->jumptopos  = info->jumpto;
      w->status     = DECODER_STARTING;
      w->stop       = FALSE;
      w->songlength = 0;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;

      if( w->file ) {
        xio_fabort( w->file );
      }

      DosPostEventSem( w->play  );
      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      if( w->status == DECODER_STOPPED ) {
        DosPostEventSem( w->play );
      }
      break;

    case DECODER_FFWD:
      if( info->ffwd ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->ffwd = info->ffwd;
      break;

    case DECODER_REW:
      if( info->rew ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->frew = info->rew;
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }

  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = arg;

  if( w->seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else {
      return w->songlength;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

/* Guess image mimetype based on image header. */
static void
guess_mimetype( char* mimetype, char* data, int size )
{
  char sigs[5][2][16] = {{{ 3, 0x49, 0x49, 0x2a }, "image/tiff" },
                         {{ 2, 0x42, 0x4D       }, "image/bmp"  },
                         {{ 2, 0x42, 0x41       }, "image/bmp"  },
                         {{ 3, 0x47, 0x49, 0x46 }, "image/gif"  },
                         {{ 3, 0x89, 0x50, 0x4E }, "image/png"  }};
  int i, j;

  for( i = 0; i < 5; i++ ) {
    if( sigs[i][0][0] <= size )
    {
      BOOL found = TRUE;

      for( j = 1; j <= sigs[i][0][0]; j++ ) {
        if( data[j-1] != sigs[i][0][j] ) {
          found = FALSE;
          break;
        }
      }

      if( found ) {
        strcpy( mimetype, sigs[i][1] );
        return;
      }
    }
  }

  strcpy( mimetype, "image/jpeg" );
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  DECODER_STRUCT* w;
  char error[128];
  int  rc;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  strlcpy( w->filename, filename, sizeof( w->filename ));
  DosReleaseMutexSem( w->mutex );

  if(( rc = wv_open( w, OPEN_WVC | OPEN_TAGS, error )) != 0 ) {
    decoder_uninit( w );
    return rc == -1 ? PLUGIN_NO_PLAY : PLUGIN_NO_READ;
  }

  info->format     = w->output_format;
  info->songlength = w->songlength;
  info->mode       = w->output_format.channels == 1 ? 3 : 0;
  info->bitrate    = w->bitrate;

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = xio_fsize64( w->file );
  }

  snprintf( info->tech_info, sizeof( info->tech_info ), "%u kbs, %.1f kHz, %d bits, %s",
            info->bitrate, ( info->format.samplerate / 1000.0 ), w->bps,
            info->format.channels == 1 ? "Mono" : "Stereo" );

  wv_get_string( w, info->artist,  "Artist",  sizeof( info->artist  ));
  wv_get_string( w, info->album,   "Album",   sizeof( info->album   ));
  wv_get_string( w, info->title,   "Title",   sizeof( info->title   ));
  wv_get_string( w, info->genre,   "Genre",   sizeof( info->genre   ));
  wv_get_string( w, info->year,    "Year" ,   sizeof( info->year    ));
  wv_get_string( w, info->comment, "Comment", sizeof( info->comment ));

  if( HAVE_FIELD( info, track ))
  {
    char buffer[128];

    wv_get_string( w, info->copyright, "Copyright",  sizeof( info->copyright ));
    wv_get_string( w, info->track,     "Track",      sizeof( info->track     ));
    wv_get_string( w, info->disc,      "DiscNumber", sizeof( info->disc      ));

    wv_get_string( w, buffer, "replaygain_album_gain", sizeof( buffer ));
    info->album_gain = atof( buffer );
    wv_get_string( w, buffer, "replaygain_album_peak", sizeof( buffer ));
    info->album_peak = atof( buffer );
    wv_get_string( w, buffer, "replaygain_track_gain", sizeof( buffer ));
    info->track_gain = atof( buffer );
    wv_get_string( w, buffer, "replaygain_track_peak", sizeof( buffer ));
    info->track_peak = atof( buffer );

    info->saveinfo = is_file( filename );
    info->haveinfo = DECODER_HAVE_TITLE      |
                     DECODER_HAVE_ARTIST     |
                     DECODER_HAVE_ALBUM      |
                     DECODER_HAVE_YEAR       |
                     DECODER_HAVE_GENRE      |
                     DECODER_HAVE_TRACK      |
                     DECODER_HAVE_DISC       |
                     DECODER_HAVE_COMMENT    |
                     DECODER_HAVE_COPYRIGHT  |
                     DECODER_HAVE_TRACK_GAIN |
                     DECODER_HAVE_TRACK_PEAK |
                     DECODER_HAVE_ALBUM_GAIN |
                     DECODER_HAVE_ALBUM_PEAK |
                     DECODER_HAVE_PICS       |
                     DECODER_PICS_COVERFRONT |
                     DECODER_PICS_COVERBACK;

    info->codepage = ch_default();
  }

  if( HAVE_FIELD( info, pics_count ))
  {
    int  i, count;
    char tag[128];

    DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

    for( i = 0, count = 0; WavpackGetBinaryTagItemIndexed( w->wpc, i, tag, sizeof(tag)); i++ ) {
      if( stricmp( tag, "Cover Art (front)" ) == 0 ||
          stricmp( tag, "Cover Art (back)"  ) == 0 )
      {
        ++count;
      }
    }

    if( count &&  ( options & DECODER_LOADPICS )) {
      if(( info->pics = (APIC**)info->memalloc( sizeof( APIC* ) * count )) != NULL ) {
        for( i = 0, count = 0; WavpackGetBinaryTagItemIndexed( w->wpc, i, tag, sizeof(tag)); i++ ) {
          if( stricmp( tag, "Cover Art (front)" ) == 0 ||
              stricmp( tag, "Cover Art (back)"  ) == 0 )
          {
            APIC* pic;
            int   pic_size = WavpackGetBinaryTagValueIndexed( w->wpc, i, NULL, 0 );
            char* pic_data = malloc( pic_size );
            int   desc_len;

            if( pic_data )
            {
              const char* p = pic_data;

              WavpackGetBinaryTagValueIndexed( w->wpc, i, pic_data, pic_size );
              desc_len = strlen( p );
              pic_size = pic_size - ( desc_len + 1 ) + sizeof( APIC );

              if(( info->pics[count] = (APIC*)info->memalloc( pic_size )) == NULL ) {
                break;
              }

              pic = info->pics[count];

              pic->size = pic_size;
              pic->type = stricmp( tag, "Cover Art (front)" ) == 0 ? PIC_COVERFRONT : PIC_COVERBACK;

              guess_mimetype( pic->mimetype, (CHAR*)(p + desc_len + 1), pic_size - sizeof( APIC ));
              ch_convert( CH_UTF_8, p, CH_DEFAULT, pic->desc, sizeof( pic->desc ));

              DEBUGLOG(( "wvplay: load APIC type %d, %s, '%s', size %d\n",
                          pic->type, pic->mimetype, pic->desc,
                          pic_size + ( desc_len + 1 ) - sizeof( APIC )));

              memcpy((char*)pic + sizeof( APIC ), (CHAR*)(p + desc_len + 1), pic_size - sizeof( APIC ));
              free( pic_data );
              ++count;
            }
          }
        }
      }
    }

    info->pics_count = count;
    DEBUGLOG(( "wvplay: found %d attached pictures\n", info->pics_count ));
    DosReleaseMutexSem( w->mutex );
  }

  wv_close( w );
  decoder_uninit( w );
  return PLUGIN_OK;
}

/* It is called if it is necessary to change the meta information
   for the specified file. */
ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  int  rc = PLUGIN_OK;
  char error[128];
  char buffer[64];
  int  i;

  DECODER_STRUCT* w;

  if( !*filename ) {
    return EINVAL;
  }

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return errno;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  strlcpy( w->filename, filename, sizeof( w->filename ));
  rc = wv_open( w, OPEN_TAGS | OPEN_EDIT_TAGS, error );
  DosReleaseMutexSem( w->mutex );

  if( rc != 0 ) {
    DEBUGLOG(( "wvplay: unable open file for saving %s, rc=%d\n", filename, rc ));
    decoder_uninit( w );
    return rc;
  }

  wv_set_string( w, info->artist,    "Artist"     );
  wv_set_string( w, info->album,     "Album"      );
  wv_set_string( w, info->title,     "Title"      );
  wv_set_string( w, info->genre,     "Genre"      );
  wv_set_string( w, info->year,      "Year"       );
  wv_set_string( w, info->track,     "Track"      );
  wv_set_string( w, info->disc,      "DiscNumber" );
  wv_set_string( w, info->copyright, "Copyright"  );
  wv_set_string( w, info->comment,   "Comment"    );

  if( info->album_gain ) {
    sprintf( buffer, "%.2f dB", info->album_gain );
    wv_set_string( w, buffer, "replaygain_album_gain" );
    wv_set_string( w, buffer, "Replay Gain (album)" );
  } else {
    WavpackDeleteTagItem( w->wpc, "replaygain_album_gain" );
    WavpackDeleteTagItem( w->wpc, "Replay Gain (album)" );
  }

  if( info->album_peak ) {
    sprintf( buffer, "%.6f", info->album_peak );
    wv_set_string( w, buffer, "replaygain_album_peak" );
  } else {
    WavpackDeleteTagItem( w->wpc, "replaygain_album_peak" );
  }

  if( info->track_gain ) {
    sprintf( buffer, "%.2f dB", info->track_gain );
    wv_set_string( w, buffer, "replaygain_track_gain" );
    wv_set_string( w, buffer, "Replay Gain (radio)" );
  } else {
    WavpackDeleteTagItem( w->wpc, "replaygain_track_gain" );
    WavpackDeleteTagItem( w->wpc, "Replay Gain (radio)" );
  }

  if( info->track_peak ) {
    sprintf( buffer, "%.6f", info->track_peak );
    wv_set_string( w, buffer, "replaygain_track_peak" );
  } else {
    WavpackDeleteTagItem( w->wpc, "replaygain_track_peak" );
  }

  if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS )) {
    while( WavpackDeleteTagItem( w->wpc, "Cover Art (front)" )) {}
    while( WavpackDeleteTagItem( w->wpc, "Cover Art (back)"  )) {}

    for( i = 0; i < info->pics_count; i++ )
    {
      APIC* pic = info->pics[i];
      char* data;
      char  desc[sizeof(pic->desc)*4];
      int   desc_len;
      int   pict_len;
      int   size;

      ch_convert( CH_DEFAULT, pic->desc, CH_UTF_8, desc, sizeof( desc ));
      desc_len = strlen( desc );
      pict_len = pic->size - sizeof( APIC );
      size = desc_len + 1 + pict_len;

      if(( data = (char*)malloc( size )) != NULL )
      {
        char* p = data;

        strcpy( p, desc );
        p += desc_len + 1;
        memcpy( p, (char*)pic + sizeof( APIC ), pict_len );

        WavpackAppendBinaryTagItem(
          w->wpc, pic->type == PIC_COVERFRONT ? "Cover Art (front)" : "Cover Art (back)", data, size );

        free( data );
      }
    }
  }

  if( !WavpackWriteTag( w->wpc )) {
    rc = xio_errno();
  }

  return rc;
}

/* Returns information about specified track. */
ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

/* Returns information about a disc inserted to the specified drive. */
ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

/* What can be played via the decoder. */
ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 2 ) {
      strcpy( ext[0], "*.WV" );
      strcpy( ext[1], "audio/x-wavpack" );
    }
    *size = 2;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_SAVEINFO;
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "WavPack Play " VER_STRING;
  param->configurable = FALSE;
}

int INIT_ATTRIBUTE
__dll_initialize( void )
{
  if( DosCreateMutexSem( NULL, &mutex, 0, FALSE ) != NO_ERROR ) {
    return 0;
  }

  return 1;
}

int TERM_ATTRIBUTE
__dll_terminate( void )
{
  free( instances );
  DosCloseMutexSem( mutex );
  return 1;
}

#if defined(__IBMC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag       )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    return __dll_initialize();
  } else if( flag == DLL_PROCESS_DETACH ) {
    __dll_terminate();
    #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
    #endif
    _CRT_term();
  }
  return 1UL;
}
#endif

