/*
 * Copyright 2011-2021 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "All.h"
#include "CharacterHelper.h"
#include "macxio.h"
#include <debuglog.h>

/* Constructs the XIO object. */
CXIO::CXIO()
{
  memset( m_cFileName, 0, sizeof( m_cFileName ));
  m_pFile = NULL;
}

/* Destructs. */
CXIO::~CXIO() {
  Close();
}

/* Returns the file handle currently associated with stream.
   If the methos fails, the return value is -1. */
int CXIO::GetHandle() {
  return xio_fileno( m_pFile );
}

/* Opens a file for reading. */
int CXIO::Open( const wchar_t* pName, BOOL modify )
{
  str_ansi* pAnsiName = CAPECharacterHelper::GetANSIFromUTF16( pName );

  Close();

  m_pFile = xio_fopen( pAnsiName, modify ? "r+b" : "rb" );

  delete pAnsiName;

  if( !m_pFile ) {
    return -1;
  }

  if( !xio_can_seek( m_pFile ) && !xio_buffer_size()) {
    DEBUGLOG(( "macplay: Unable to open file that doesn't support movement of the read pointer\n" ));
    Close();
    return -1;
  }

  wcscpy( m_cFileName, pName );
  return 0;
}

/* Closes the file. Returns 0 if it successfully closes
   the file, or -1 if any errors were detected. */
int CXIO::Close()
{
  int rc = -1;

  if( m_pFile != NULL )
  {
    rc = xio_fclose( m_pFile );
    m_pFile = NULL;
  }

  return rc;
}

/* Reads up to nBytesToRead bytes from the input file and
   stores them in the given buffer. */
int CXIO::Read( void* pBuffer, unsigned int nBytesToRead, unsigned int* pBytesRead )
{
  DEBUGLOG(( "CXIO::Read %d\n", nBytesToRead ));
  *pBytesRead = xio_fread( pBuffer, 1, nBytesToRead, m_pFile );
  return xio_ferror( m_pFile ) ? ERROR_IO_READ : 0;
}

/* Writes up to nBytesToWrite bytes from buffer to the output file. */
int CXIO::Write( const void* pBuffer, unsigned int nBytesToWrite, unsigned int* pBytesWritten )
{
  *pBytesWritten = xio_fwrite( pBuffer, 1, nBytesToWrite, m_pFile );
  return (xio_ferror( m_pFile ) || ( *pBytesWritten != nBytesToWrite )) ? ERROR_IO_WRITE : 0;
}
/* Changes the current file position to a new location within the file. */
int CXIO::Seek( int nDistance, unsigned int nMoveMode )
{
  #if SEEK_SET != XIO_SEEK_SET || SEEK_CUR != XIO_SEEK_CUR || SEEK_END != XIO_SEEK_END

  switch( nMoveMode ) {
    case SEEK_SET: nMoveMode = XIO_SEEK_SET; break;
    case SEEK_CUR: nMoveMode = XIO_SEEK_CUR; break;
    case SEEK_END: nMoveMode = XIO_SEEK_END; break;
  }

  #endif

  DEBUGLOG(( "CXIO::Seek %d %d\n", nDistance, nMoveMode ));
  return xio_fseek( m_pFile, nDistance, nMoveMode );
}

/* Cuts off the file. */
int CXIO::SetEOF() {
  return xio_ftruncate( m_pFile, GetPosition());
}

/* Finds the current position of the file. */
int CXIO::GetPosition()
{
    xio_fpos_t fPosition;

    memset( &fPosition, 0, sizeof( fPosition ));
    xio_fgetpos( m_pFile, &fPosition );
    return fPosition;
}

/* Returns the size of the file. A return value of -1L indicates an
   error or an unknown size. */
int CXIO::GetSize() {
  return xio_fsize( m_pFile );
}

/* Returns the file name. */
int CXIO::GetName( wchar_t* pBuffer )
{
  wcscpy(pBuffer, m_cFileName);
  return 0;
}

/* Opens a file for writing. */
int CXIO::Create( const wchar_t* pName )
{
  str_ansi* pAnsiName = CAPECharacterHelper::GetANSIFromUTF16( pName );

  Close();
  m_pFile = xio_fopen( pAnsiName, "wb" );

  delete pAnsiName;

  if( !m_pFile ) {
    return -1;
  }

  wcscpy( m_cFileName, pName );
  return 0;
}

/* Deletes the file. */
int CXIO::Delete()
{
    Close();

    str_ansi* pAnsiName = CAPECharacterHelper::GetANSIFromUTF16( m_cFileName );
    int rc = remove( pAnsiName );
    delete pAnsiName;
    return rc;
}

/* Waits the read-ahead buffer filling according to current XIO
   library setting. */
void CXIO::Wait() {
  xio_fwait( m_pFile );
}

/* Causes an abnormal termination of all current read and write
   operations of the file. All current and subsequent calls can
   raise an error. */
void CXIO::Abort() {
  xio_fabort( m_pFile );
}

/* Returns XIO_NOT_SEEK (0) on streams incapable of seeking,
   XIO_CAN_SEEK (1) on streams capable of seeking and returns
   XIO_CAN_SEEK_FAST (2) on streams capable of fast seeking. */
int CXIO::CanSeek() {
  return xio_can_seek( m_pFile );
}

