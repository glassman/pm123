/*
 * Copyright 2011-2018 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PM123_MACXIO_H
#define PM123_MACXIO_H

#include "CIO.h"
#include <xio.h>

class CXIO : public CIO
{
  public:

    // construction / destruction
    CXIO();
   ~CXIO();

    // open / close
    int Open( const wchar_t* pName, BOOL modify );
    int Close();

    int Open( const wchar_t* pName ) {
      return Open( pName, FALSE );
    }

    // read / write
    int Read( void* pBuffer, unsigned int nBytesToRead, unsigned int* pBytesRead );
    int Write( const void* pBuffer, unsigned int nBytesToWrite, unsigned int* pBytesWritten );

    // seek
    int Seek( int nDistance, unsigned int nMoveMode);

    // other functions
    int SetEOF();

    // creation / destruction
    int Create( const wchar_t* pName );
    int Delete();

    // attributes
    int GetPosition();
    int GetSize();
    int GetName( wchar_t* pBuffer );
    int GetHandle();

    // additional methods
    void Wait();
    int  CanSeek();
    void Abort();

  private:

    wchar_t m_cFileName[_MAX_URL];
    XFILE*  m_pFile;
};

#endif /* PM123_MACXIO_H */
