/*
 * Copyright 2011-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_WIN
#define  INCL_ERRORS
#include <os2.h>
#include <process.h>
#include <string.h>
#include <errno.h>

#include <decoder_plug.h>
#include <plugin.h>
#include <utilfct.h>
#include <debuglog.h>

#include "All.h"
#include "CharacterHelper.h"
#include "MACLib.h"
#include "APETag.h"
#include "macplay.h"

static DECODER_STRUCT** instances = NULL;
static int  instances_count = 0;
static HMTX mutex;

#define WM_UPDATE_CONTROLS ( WM_USER + 1 )

/* Returns a specified field of the given comment. */
static char*
mac_get_string( DECODER_STRUCT* w, char* target, const wchar_t* type, int size )
{
  *target = 0;

  if( w->pAPETag )
  {
    CAPETagField* pAPETagField = w->pAPETag->GetTagField( type );

    if( pAPETagField ) {
      ch_convert( CH_UTF_8, pAPETagField->GetFieldValue(), CH_DEFAULT, target, size );
    }
  }

  return target;
}

/* Sets a specified field of the given comment. */
static void
mac_set_string( DECODER_STRUCT* w, char* source, const wchar_t* type )
{
  if( w->pAPETag )
  {
    int   size   = strlen( source ) * 4 + 1;
    char* string = (char*)malloc( size );

    if( string ) {
      ch_convert( CH_DEFAULT, source, CH_UTF_8, string, size );
      w->pAPETag->SetFieldString( type, string, TRUE );
      free( string );
    }
  }
}

/* Removes a specified field. */
static void
mac_remove_string( DECODER_STRUCT* w, const wchar_t* type )
{
  if( w->pAPETag ) {
    w->pAPETag->RemoveField( type );
  }
}

/* Maps the error number in errnum to an Monkey's audio
   error message string. */
static const char*
mac_strerror( int errnum )
{
  struct _errorexp {
    int errnum;
    const char* explanation;
  } errorexp[] = { ERROR_EXPLANATION };

  unsigned int i;

  for( i = 0; i < sizeof( errorexp ) / sizeof( errorexp[0] ); i++ ) {
    if( errorexp[i].errnum == errnum ) {
      return errorexp[i].explanation;
    }
  }

  return NULL;
}

static BOOL
mac_flush_output( DECODER_STRUCT* w )
{
  int bitrate = w->pAPEDecompress->GetInfo( APE_DECOMPRESS_CURRENT_BITRATE );

  if( bitrate > 0 && w->bitrate != bitrate ) {
    WinPostMsg( w->hwnd, WM_CHANGEBR, MPFROMLONG( bitrate ), 0 );
    w->bitrate = bitrate;
  }

  if( w->output_play_samples( w->a, &w->output_format, w->buffer, w->bufpos, w->posmarker ) != w->bufpos ) {
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    return FALSE;
  }

  w->bufpos = 0;
  return TRUE;
}

/* Opens Monkey's audio file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. */
static int
mac_open( DECODER_STRUCT* w, BOOL modify )
{
  wchar_t* spInput;
  int rc;

  spInput = CAPECharacterHelper::GetUTF16FromANSI( w->filename );
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  w->pXIO = new CXIO();
  w->pXIO->Open( spInput, modify );
  w->pAPEDecompress = CreateIAPEDecompressEx( w->pXIO, &rc );

  if( w->pAPEDecompress )
  {
    w->songlength = w->pAPEDecompress->GetInfo( APE_INFO_LENGTH_MS );
    w->bitrate    = w->pAPEDecompress->GetInfo( APE_INFO_AVERAGE_BITRATE );
    w->samplesize = w->pAPEDecompress->GetInfo( APE_INFO_BLOCK_ALIGN );
    w->bps        = w->pAPEDecompress->GetInfo( APE_INFO_BITS_PER_SAMPLE );
    w->seekable   = w->pXIO->CanSeek();

    w->output_format.size       = sizeof( w->output_format );
    w->output_format.format     = WAVE_FORMAT_PCM;
    w->output_format.bits       = w->bps;
    w->output_format.channels   = w->pAPEDecompress->GetInfo( APE_INFO_CHANNELS );
    w->output_format.samplerate = w->pAPEDecompress->GetInfo( APE_INFO_SAMPLE_RATE );

    // The Monkey's audio encoder limited bits per sample as 8, 16, 24 and 32.
    // But anyway we apply checks here because other values can produce
    // big problems.
    if( w->bps > 32 || w->bps % 8 != 0 )
    {
      delete w->pAPEDecompress;
      w->pAPEDecompress = NULL;
      rc = ERROR_INPUT_FILE_UNSUPPORTED_BIT_DEPTH;
    } else {
      w->pAPETag = (CAPETag*)w->pAPEDecompress->GetInfo( APE_INFO_TAG );
    }
  }

  delete spInput;
  DosReleaseMutexSem( w->mutex );
  return rc;
}

/* Closes Monkey's audio file. */
static void
mac_close( DECODER_STRUCT* w )
{
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->pAPEDecompress ) {
    delete w->pAPEDecompress;
    w->pAPEDecompress = NULL;
  }
  if( w->pXIO ) {
    delete w->pXIO;
    w->pXIO = NULL;
  }

  DosReleaseMutexSem( w->mutex );
}

/* Notify an error. */
static void
mac_show_error( DECODER_STRUCT* w, int style, const char* msg, int rc )
{
  char buff[1024];
  const char* what = mac_strerror( rc );

  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );

  if( what ) {
    strlcat( buff, what, sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  #define READ_SAMPLES_AT_ONCE 1024

  int   rc;
  unsigned char* samples = NULL;
  ULONG resetcount;
  int   pos;
  int   read, readed;
  int   done;

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  if(( rc = mac_open( w, FALSE )) != 0 ) {
    mac_show_error( w, MB_ERROR, "Unable open file", rc );
    goto end;
  }

  // After opening a new file we so are in its beginning.
  if( w->jumptopos == 0 && w->startpos == 0 ) {
    w->jumptopos = -1;
  }

  if(( w->buffer = (char*)malloc( w->audio_buffersize )) == NULL ) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }
  if(( samples = (unsigned char*)malloc( w->samplesize * READ_SAMPLES_AT_ONCE )) == NULL ) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  DEBUGLOG(( "macplay: play %d aligned blocks, ch = %d, bps = %d, blocks per frame = %d\n",
              w->samplesize, w->output_format.channels, w->bps,
              w->pAPEDecompress->GetInfo( APE_INFO_BLOCKS_PER_FRAME )));
  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    w->bufpos = 0;

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    w->pXIO->Wait();

    while( !w->stop ) {
      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

      if( w->jumptopos >= 0 )
      {
        pos = (int)(((float)w->jumptopos + w->startpos) * w->output_format.samplerate / 1000 );
        DEBUGLOG(( "macplay: jumps to %d ms (%d blocks)\n", w->jumptopos + w->startpos, pos ));
        rc = w->pAPEDecompress->Seek( pos );
        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        if( rc != ERROR_SUCCESS ) {
          mac_show_error( w, MB_WARNING, "Unable seek in file", rc );
        }

        w->jumptopos = -1;
      }

      pos  = w->pAPEDecompress->GetInfo( APE_DECOMPRESS_CURRENT_MS );
      read = READ_SAMPLES_AT_ONCE;

      if( w->endpos ) {
        if( pos >= w->endpos ) {
          DEBUGLOG(( "macplay: break at %d ms because end of fragment is reached\n", pos ));
          DosReleaseMutexSem( w->mutex );
          break;
        }
        if(( read = w->endpos - pos ) > READ_SAMPLES_AT_ONCE ) {
          read = READ_SAMPLES_AT_ONCE;
        }
      }

      rc = w->pAPEDecompress->GetData((char*)samples, read, &readed );

      if( rc != ERROR_SUCCESS ) {
        mac_show_error( w, MB_ERROR, "Unable read file", rc );
        DosReleaseMutexSem( w->mutex );
        goto end;
      }

      readed *= w->samplesize;

      for( done = 0; done < readed; )
      {
        int bytes = readed - done;

        if( w->bufpos == 0 ) {
          w->posmarker = pos - w->startpos;
        }
        if( bytes > w->audio_buffersize - w->bufpos ) {
          bytes = w->audio_buffersize - w->bufpos;
        }

        memcpy((char*)w->buffer + w->bufpos, samples + done, bytes );

        w->bufpos += bytes;
        done += bytes;

        if( w->bufpos == w->audio_buffersize ) {
          mac_flush_output( w );
        }
      }

      DosReleaseMutexSem( w->mutex );

      if( readed < read ) {
        DEBUGLOG(( "macplay: break at %d ms because end of file is reached\n",
                   w->pAPEDecompress->GetInfo( APE_DECOMPRESS_CURRENT_MS )));
        break;
      }
    }

    mac_flush_output( w );
    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

  w->status = DECODER_STOPPED;

end:

  mac_close( w );
  free( w->buffer );
  free( samples );
  w->buffer = NULL;

  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );
  DosCreateMutexSem( NULL, &w->mutex, 0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  instances = (DECODER_STRUCT**)realloc( instances, ++instances_count * sizeof( DECODER_STRUCT* ));
  instances[ instances_count - 1 ] = w;
  DosReleaseMutexSem( mutex );

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than this is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  int i;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  decoder_command( w, DECODER_STOP, NULL );

  for( i = 0; i < instances_count; i++ ) {
    if( instances[i] == w ) {
      if( i < instances_count - 1 ) {
        memmove( instances + i, instances + i + 1,
               ( instances_count - i - 1 ) * sizeof( DECODER_STRUCT* ));
      }
      instances = (DECODER_STRUCT**)realloc( instances, --instances_count * sizeof( DECODER_STRUCT* ));
    }
  }

  DosReleaseMutexSem( mutex  );
  DosCloseEventSem( w->play  );
  DosCloseMutexSem( w->mutex );

  free( w );
  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  switch( msg )
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      w->metadata_buff       = info->metadata_buffer;
      w->metadata_buff[0]    = 0;
      w->metadata_size       = info->metadata_size;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
      strlcpy( w->filename, info->filename, sizeof( w->filename ));
      DosReleaseMutexSem( w->mutex );

      w->startpos   = info->start;
      w->endpos     = info->end;
      w->jumptopos  = info->jumpto;
      w->status     = DECODER_STARTING;
      w->stop       = FALSE;
      w->songlength = 0;
      w->seekable   = TRUE;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;

      if( w->pXIO ) {
        w->pXIO->Abort();
      }

      DosPostEventSem( w->play  );
      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_FFWD:
    case DECODER_REW:
      // Because seeking on Monkey's audio file is too CPU
      // intensive we disable fast forward and fast rewind.
      return PLUGIN_UNSUPPORTED;

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      if( w->status == DECODER_STOPPED ) {
        DosPostEventSem( w->play );
      }
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }

  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  if( w->seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else {
      return w->songlength;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

/* Guess image mimetype based on image header. */
static void
guess_mimetype( char* mimetype, char* data, int size )
{
  unsigned char sigs[5][2][16] = {{{ 3, 0x49, 0x49, 0x2a }, "image/tiff" },
                                  {{ 2, 0x42, 0x4D       }, "image/bmp"  },
                                  {{ 2, 0x42, 0x41       }, "image/bmp"  },
                                  {{ 3, 0x47, 0x49, 0x46 }, "image/gif"  },
                                  {{ 3, 0x89, 0x50, 0x4E }, "image/png"  }};
  int i, j;

  for( i = 0; i < 5; i++ ) {
    if( sigs[i][0][0] <= size )
    {
      BOOL found = TRUE;

      for( j = 1; j <= sigs[i][0][0]; j++ ) {
        if( data[j-1] != sigs[i][0][j] ) {
          found = FALSE;
          break;
        }
      }

      if( found ) {
        strcpy( mimetype, (char*)sigs[i][1] );
        return;
      }
    }
  }

  strcpy( mimetype, "image/jpeg" );
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  DECODER_STRUCT* w;
  int rc;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  strlcpy( w->filename, filename, sizeof( w->filename ));

  if(( rc = mac_open( w, FALSE )) != 0 ) {
    DosReleaseMutexSem( w->mutex );
    decoder_uninit( w );
    return PLUGIN_NO_PLAY;
  }

  info->format     = w->output_format;
  info->songlength = w->songlength;
  info->mode       = w->output_format.channels == 1 ? 3 : 0;
  info->bitrate    = w->bitrate;

  DosReleaseMutexSem( w->mutex );

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = w->pAPEDecompress->GetInfo( APE_INFO_APE_TOTAL_BYTES );
  }

  snprintf( info->tech_info, sizeof( info->tech_info ), "%u kbs, %.1f kHz, %d bits, %s",
            info->bitrate, ( info->format.samplerate / 1000.0 ), w->bps,
            info->format.channels == 1 ? "Mono" : "Stereo" );

  if( w->pAPETag )
  {
    char buffer[64];

    mac_get_string( w, info->artist,  APE_TAG_FIELD_ARTIST,  sizeof( info->artist ));
    mac_get_string( w, info->album,   APE_TAG_FIELD_ALBUM,   sizeof( info->album   ));
    mac_get_string( w, info->title,   APE_TAG_FIELD_TITLE,   sizeof( info->title   ));
    mac_get_string( w, info->genre,   APE_TAG_FIELD_GENRE,   sizeof( info->genre   ));
    mac_get_string( w, info->year,    APE_TAG_FIELD_YEAR,    sizeof( info->year    ));
    mac_get_string( w, info->comment, APE_TAG_FIELD_COMMENT, sizeof( info->comment ));

    // Disable Open Watcom C++ "&array may not produce intended result"
    #ifdef __WATCOMC__
    #pragma disable_message(7)
    #endif

    if( HAVE_FIELD( info, track ))
    {
      mac_get_string( w, info->copyright, APE_TAG_FIELD_COPYRIGHT, sizeof( info->copyright ));
      mac_get_string( w, info->track,     APE_TAG_FIELD_TRACK,     sizeof( info->track     ));
      mac_get_string( w, info->disc,      L"DiscNumber",           sizeof( info->disc      ));

      if( *mac_get_string( w, buffer, L"replaygain_album_gain", sizeof( buffer )) ||
          *mac_get_string( w, buffer, APE_TAG_FIELD_REPLAY_GAIN_ALBUM, sizeof( buffer )))
      {
        info->album_gain = atof( buffer );
      }
      if( *mac_get_string( w, buffer, L"replaygain_track_gain", sizeof( buffer )) ||
          *mac_get_string( w, buffer, APE_TAG_FIELD_REPLAY_GAIN_RADIO, sizeof( buffer )))
      {
        info->track_gain = atof( buffer );
      }
      if( *mac_get_string( w, buffer, L"replaygain_track_peak", sizeof( buffer )) ||
          *mac_get_string( w, buffer, APE_TAG_FIELD_PEAK_LEVEL, sizeof( buffer )))
      {
        info->track_peak = atof( buffer );
      }
      if( *mac_get_string( w, buffer, L"replaygain_album_peak", sizeof( buffer ))) {
        info->album_peak = atof( buffer );
      } else {
        info->album_peak = info->track_peak;
      }

      info->saveinfo = is_file( filename );
      info->haveinfo = DECODER_HAVE_TITLE      |
                       DECODER_HAVE_ARTIST     |
                       DECODER_HAVE_ALBUM      |
                       DECODER_HAVE_YEAR       |
                       DECODER_HAVE_GENRE      |
                       DECODER_HAVE_TRACK      |
                       DECODER_HAVE_DISC       |
                       DECODER_HAVE_COMMENT    |
                       DECODER_HAVE_COPYRIGHT  |
                       DECODER_HAVE_TRACK_GAIN |
                       DECODER_HAVE_TRACK_PEAK |
                       DECODER_HAVE_ALBUM_GAIN |
                       DECODER_HAVE_ALBUM_PEAK |
                       DECODER_HAVE_PICS       |
                       DECODER_PICS_COVERFRONT |
                       DECODER_PICS_COVERBACK;

      info->codepage = ch_default();
    }

    if( HAVE_FIELD( info, pics_count ))
    {
      CAPETagField* tagf;
      int i, count;

      for( i = 0, count = 0; ( tagf = w->pAPETag->GetTagField( i )) != NULL; i++ ) {
        if( tagf->GetFieldFlags() | TAG_FIELD_FLAG_DATA_TYPE_BINARY ) {
          if( wcsicmp( tagf->GetFieldName(), APE_TAG_FIELD_COVER_ART_FRONT ) == 0 ||
              wcsicmp( tagf->GetFieldName(), APE_TAG_FIELD_COVER_ART_BACK  ) == 0 )
          {
            ++count;
          }
        }
      }

      if( count && ( options & DECODER_LOADPICS )) {
        if(( info->pics = (APIC**)info->memalloc( sizeof( APIC* ) * count )) != NULL ) {
          for( i = 0, count = 0; ( tagf = w->pAPETag->GetTagField( i )) != NULL; i++ ) {
            if( tagf->GetFieldFlags() | TAG_FIELD_FLAG_DATA_TYPE_BINARY ) {
              if( wcsicmp( tagf->GetFieldName(), APE_TAG_FIELD_COVER_ART_FRONT ) == 0 ||
                  wcsicmp( tagf->GetFieldName(), APE_TAG_FIELD_COVER_ART_BACK  ) == 0 )
              {
                APIC* pic;
                int   pic_size = tagf->GetFieldValueSize();
                const char* p  = tagf->GetFieldValue();
                int   desc_len = strlen( p );


                pic_size = pic_size - ( desc_len + 1 ) + sizeof( APIC );

                if(( info->pics[count] = (APIC*)info->memalloc( pic_size )) == NULL ) {
                  break;
                }

                pic = info->pics[count];

                pic->size = pic_size;
                pic->type = wcsicmp( tagf->GetFieldName(), APE_TAG_FIELD_COVER_ART_FRONT ) == 0 ?
                                     PIC_COVERFRONT : PIC_COVERBACK;

                guess_mimetype( pic->mimetype, (CHAR*)(p + desc_len + 1), pic_size - sizeof( APIC ));
                ch_convert( CH_UTF_8, p, CH_DEFAULT, pic->desc, sizeof( pic->desc ));

                DEBUGLOG(( "macplay: load APIC type %d, %s, '%s', size %d\n",
                            pic->type, pic->mimetype, pic->desc, tagf->GetFieldValueSize()));

                memcpy((char*)pic + sizeof( APIC ), (CHAR*)(p + desc_len + 1), pic_size - sizeof( APIC ));
                ++count;
              }
            }
          }
        }
      }
      info->pics_count = count;
      DEBUGLOG(( "macplay: found %d attached pictures\n", info->pics_count ));
    }
  }

  mac_close( w );
  decoder_uninit( w );
  return PLUGIN_OK;
}

/* It is called if it is necessary to change the meta information
   for the specified file. */
ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  int  rc = PLUGIN_OK;
  char buffer[64];
  int  i;

  DECODER_STRUCT* w;

  if( !*filename ) {
    return EINVAL;
  }

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return errno;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  strlcpy( w->filename, filename, sizeof( w->filename ));
  rc = mac_open( w, TRUE );
  DosReleaseMutexSem( w->mutex );

  if( rc != 0 ) {
    DEBUGLOG(( "macplay: unable open file for saving %s, rc=%d\n", filename, rc ));
    decoder_uninit( w );
    return rc;
  }

  mac_set_string( w, info->artist,    APE_TAG_FIELD_ARTIST    );
  mac_set_string( w, info->album,     APE_TAG_FIELD_ALBUM     );
  mac_set_string( w, info->title,     APE_TAG_FIELD_TITLE     );
  mac_set_string( w, info->genre,     APE_TAG_FIELD_GENRE     );
  mac_set_string( w, info->year,      APE_TAG_FIELD_YEAR      );
  mac_set_string( w, info->track,     APE_TAG_FIELD_TRACK     );
  mac_set_string( w, info->disc,      L"DiscNumber"           );
  mac_set_string( w, info->copyright, APE_TAG_FIELD_COPYRIGHT );
  mac_set_string( w, info->comment,   APE_TAG_FIELD_COMMENT   );

  if( info->album_gain ) {
    sprintf( buffer, "%.2f dB", info->album_gain );
    mac_set_string( w, buffer, L"replaygain_album_gain" );
    mac_set_string( w, buffer, APE_TAG_FIELD_REPLAY_GAIN_ALBUM );
  } else {
    mac_remove_string( w, L"replaygain_album_gain" );
    mac_remove_string( w, APE_TAG_FIELD_REPLAY_GAIN_ALBUM );
  }

  if( info->album_peak ) {
    sprintf( buffer, "%.6f", info->album_peak );
    mac_set_string( w, buffer, L"replaygain_album_peak" );
  } else {
    mac_remove_string( w, L"replaygain_album_peak" );
  }

  if( info->track_gain ) {
    sprintf( buffer, "%.2f dB", info->track_gain );
    mac_set_string( w, buffer, L"replaygain_track_gain" );
    mac_set_string( w, buffer, APE_TAG_FIELD_REPLAY_GAIN_RADIO );
  } else {
    mac_remove_string( w, L"replaygain_track_gain" );
    mac_remove_string( w, APE_TAG_FIELD_REPLAY_GAIN_RADIO );
  }

  if( info->track_peak ) {
    sprintf( buffer, "%.6f", info->track_peak );
    mac_set_string( w, buffer, L"replaygain_track_peak" );
  } else {
    mac_remove_string( w, L"replaygain_track_peak" );
  }

  if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS )) {
    if( w->pAPETag ) {
      while( w->pAPETag->RemoveField( APE_TAG_FIELD_COVER_ART_FRONT ) == ERROR_SUCCESS ) {}
      while( w->pAPETag->RemoveField( APE_TAG_FIELD_COVER_ART_BACK  ) == ERROR_SUCCESS ) {}

      for( i = 0; i < info->pics_count; i++ )
      {
        APIC* pic = info->pics[i];
        char* data;
        char  desc[sizeof(pic->desc)*4];
        int   desc_len;
        int   pict_len;
        int   size;

        ch_convert( CH_DEFAULT, pic->desc, CH_UTF_8, desc, sizeof( desc ));
        desc_len = strlen( desc );
        pict_len = pic->size - sizeof( APIC );
        size = desc_len + 1 + pict_len;

        if(( data = (char*)malloc( size )) != NULL )
        {
          char* p = data;

          strcpy( p, desc );
          p += desc_len + 1;
          memcpy( p, (char*)pic + sizeof( APIC ), pict_len );

          w->pAPETag->SetFieldBinary( pic->type == PIC_COVERBACK ? APE_TAG_FIELD_COVER_ART_BACK : APE_TAG_FIELD_COVER_ART_FRONT,
                                      data, size, TAG_FIELD_FLAG_DATA_TYPE_BINARY );
          free( data );
        }
      }
    }
  }

  if( w->pAPETag ) {
    rc = w->pAPETag->Save();
  } else {
    rc = EINVAL;
  }

  mac_close( w );
  decoder_uninit( w );
  return rc;
}

/* Returns information about specified track. */
ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

/* Returns information about a disc inserted to the specified drive. */
ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

/* What can be played via the decoder. */
ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 2 ) {
      strcpy( ext[0], "*.APE" );
      strcpy( ext[1], "audio/x-ape" );
    }
    *size = 2;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_SAVEINFO;
}

static void
save_ini( void )
{
  HINI hini;
  int  tag_read_charset = CAPECharacterHelper::GetReadCharset();

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, tag_read_charset );
    close_ini( hini );
  }
}

static void
load_ini( void )
{
  HINI hini;
  int  tag_read_charset;

  if( ch_default() == CH_CYR_OS2 ) {
    tag_read_charset = CH_CYR_WIN;
  } else {
    tag_read_charset = CH_DEFAULT;
  }

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, tag_read_charset );
    close_ini( hini );
  }

  CAPECharacterHelper::SetReadCharset( tag_read_charset );
}

/* Processes messages of the configuration dialog. */
static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_INITDLG:
    {
      int  i;
      for( i = 0; i < ch_list_size; i++ ) {
        lb_add_with_handle( hwnd, CB_V1_RDCH, ch_list[i].name, ch_list[i].id );
      }

      do_warpsans( hwnd );
      // continue to WM_UPDATE_CONTROLS...
    }

    case WM_UPDATE_CONTROLS:
      lb_select_by_handle( hwnd, CB_V1_RDCH, CAPECharacterHelper::GetReadCharset());
      return 0;

    case WM_DESTROY:
    {
      int  i;
      if(( i = lb_cursored( hwnd, CB_V1_RDCH )) != LIT_NONE ) {
        CAPECharacterHelper::SetReadCharset((int)lb_get_handle( hwnd, CB_V1_RDCH, i ));
      }
      save_ini();
      break;
    }
    case WM_COMMAND:

      switch( SHORT1FROMMP( mp1 )) {
        case PB_UNDO:
          WinSendMsg( hwnd, WM_UPDATE_CONTROLS, 0, 0 );
          return 0;

        case PB_DEFAULT:
          if( ch_default() == CH_CYR_OS2 ) {
            lb_select_by_handle( hwnd, CB_V1_RDCH, CH_CYR_WIN  );
          } else {
            lb_select_by_handle( hwnd, CB_V1_RDCH, CH_DEFAULT  );
          }
          return 0;
      }
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in. */
void DLLENTRY
plugin_configure( HWND hwnd, HMODULE module ) {
  WinDlgBox( HWND_DESKTOP, hwnd, cfg_dlg_proc, module, DLG_CONFIGURE, NULL );
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "Monkey's Audio Play " VER_STRING;
  param->configurable = TRUE;

  load_ini();
}

int INIT_ATTRIBUTE
__dll_initialize( void )
{
  if( DosCreateMutexSem( NULL, &mutex, 0, FALSE ) != NO_ERROR ) {
    return 0;
  }

  return 1;
}

int TERM_ATTRIBUTE
__dll_terminate( void )
{
  free( instances );
  DosCloseMutexSem( mutex );
  return 1;
}

#if defined(__IBMC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag       )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    __ctordtorInit();
    return __dll_initialize();
  } else if( flag == DLL_PROCESS_DETACH ) {
    __dll_terminate();
    #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
    #endif
    __ctordtorTerm();
    _CRT_term();
  }
  return 1UL;
}
#endif

