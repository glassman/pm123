#ifndef _WIN32

#ifdef  HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef APE_NOWINDOWS_H
#define APE_NOWINDOWS_H

#define NEAR
#define FAR

typedef unsigned int        uint32;
typedef int                 int32;
typedef unsigned short      uint16;
typedef short               int16;
typedef unsigned char       uint8;
typedef char                int8;
typedef char                str_ansi;
typedef unsigned char       str_utf8;
typedef wchar_t             str_utf16;

typedef unsigned long       DWORD;
typedef unsigned short      WORD;
typedef float               FLOAT;
typedef void *              HANDLE;
typedef unsigned int        UINT;
typedef unsigned int        WPARAM;
typedef long                LPARAM;
typedef const char *        LPCSTR;
typedef char *              LPSTR;
typedef long                LRESULT;
typedef const wchar_t *     LPCTSTR;
typedef const wchar_t *     LPCWSTR;
typedef unsigned char       UCHAR;

#define ZeroMemory(POINTER, BYTES) memset(POINTER, 0, BYTES);
#define max(a,b)    (((a) > (b)) ? (a) : (b))
#define min(a,b)    (((a) < (b)) ? (a) : (b))

#define CALLBACK

#ifdef  __OS2__
#define  INCL_BASE
#include <os2.h>
#include <charset.h>
#include <unidef.h>

#define  wcsnicmp(s1,s2,n) UniStrncmpi(NULL,(UniChar*)s1,(UniChar*)s2,n)
#define  wcsicmp(s1,s2) UniStrcmpi(NULL,(UniChar*)s1,(UniChar*)s2)
#define _wtoi(c) wcstol( c, NULL, 10 )

#define  _wcsicmp  wcsicmp
#define  _wcsnicmp wcsnicmp
#define  _strnicmp strnicmp
#define  _stricmp  stricmp

#ifdef  __IBMCPP__
#define _FPOSOFF(fp) (fp.__fpos_elem[0])
#else
#define _FPOSOFF(fp) (fp)
#endif
#else
typedef int           BOOL;
typedef unsigned char BYTE;

#define FALSE  0
#define TRUE   1

#define  __stdcall
#define  _stricmp  strcasecmp
#define  _strnicmp strncasecmp

#define _FPOSOFF(fp) ((long)(fp).__pos)
#endif

#define MAX_PATH    260

#ifndef _WAVEFORMATEX_
#define _WAVEFORMATEX_

typedef struct tWAVEFORMATEX
{
    WORD        wFormatTag;         /* format type */
    WORD        nChannels;          /* number of channels (i.e. mono, stereo...) */
    DWORD       nSamplesPerSec;     /* sample rate */
    DWORD       nAvgBytesPerSec;    /* for buffer estimation */
    WORD        nBlockAlign;        /* block size of data */
    WORD        wBitsPerSample;     /* number of bits per sample of mono data */
    WORD        cbSize;             /* the count in bytes of the size of */
                    /* extra information (after cbSize) */
} WAVEFORMATEX, *PWAVEFORMATEX, NEAR *NPWAVEFORMATEX, FAR *LPWAVEFORMATEX;
typedef const WAVEFORMATEX FAR *LPCWAVEFORMATEX;

#endif // #ifndef _WAVEFORMATEX_

#endif // #ifndef APE_NOWINDOWS_H

#endif // #ifndef _WIN32
