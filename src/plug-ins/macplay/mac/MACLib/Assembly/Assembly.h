#ifndef APE_ASSEMBLY_H
#define APE_ASSEMBLY_H

extern "C" 
{
    void CDECL Adapt(short * pM, const short * pAdapt, int nDirection, int nOrder);
    int  CDECL CalculateDotProduct(const short * pA, const short * pB, int nOrder);
    BOOL CDECL GetMMXAvailable();
};

#endif // #ifndef APE_ASSEMBLY_H

