/*
 * Copyright 2006-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_WIN
#include <os2.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <process.h>
#include <math.h>

#include "wavplay.h"
#include <decoder_plug.h>
#include <plugin.h>
#include <utilfct.h>
#include <debuglog.h>

static sf_count_t DLLENTRY
vio_fsize( void* x ) {
  return xio_fsize64((XFILE*)x );
}

static sf_count_t DLLENTRY
vio_seek( sf_count_t offset, int whence, void* x )
{
  sf_count_t pos = 0;

  switch( whence )
  {
    case SEEK_SET: pos = offset; break;
    case SEEK_CUR: pos = xio_ftell64((XFILE*)x) + offset; break;
    case SEEK_END: pos = xio_fsize64((XFILE*)x) + offset; break;
    default:
      return -1;
  }

  if( xio_fseek64((XFILE*)x, pos, XIO_SEEK_SET ) == 0 ) {
    return pos;
  } else {
    return -1;
  }
}

static sf_count_t DLLENTRY
vio_read( void* ptr, sf_count_t count, void* x ) {
  return xio_fread( ptr, 1, count, (XFILE*)x );
}

static sf_count_t DLLENTRY
vio_write( const void* ptr, sf_count_t count, void* x ) {
  return xio_fwrite((void*)ptr, 1, count, (XFILE*)x );
}

static sf_count_t DLLENTRY
vio_tell( void *x ) {
  return xio_ftell64((XFILE*)x );
}

static int DLLENTRY
vio_is_pipe( void *x ) {
  return !xio_can_seek((XFILE*)x );
}

/* Opens a file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static ULONG
snd_open( DECODER_STRUCT* w, int mode )
{
  SF_VIRTUAL_IO vio;

  #if DEBUG
  char buf[8192];
  #endif

  if(( w->file = xio_fopen( w->filename, mode == SFM_READ ? "rb" : "r+b" )) == NULL ) {
    w->sndfile = NULL;
    return xio_errno();
  }

  w->seekable = xio_can_seek( w->file );

  vio.read        = vio_read;
  vio.write       = vio_write;
  vio.get_filelen = vio_fsize;
  vio.seek        = vio_seek;
  vio.tell        = vio_tell;
  vio.is_pipe     = vio_is_pipe;

  w->sndfile = sf_open_virtual( &vio, mode, &w->sfinfo, w->file );

  #if DEBUG
  DEBUGLOG(( "wavplay: opening %s, FORMAT=%04X\n", w->filename, w->sfinfo.format ));
  sf_command( w->sndfile, SFC_GET_LOG_INFO, &buf, sizeof( buf ));
  DEBUGLOG(( "wavplay: %s\n%s\n", sf_strerror( w->sndfile ), buf ));
  #endif

  if( w->sndfile == NULL ) {
    xio_fclose( w->file );
    w->file = NULL;
    return -1;
  }

  switch( w->sfinfo.format & ( SF_FORMAT_SUBMASK | SF_FORMAT_TYPEMASK ))
  {
    case SF_FORMAT_WAV  | SF_FORMAT_PCM_U8:
    case SF_FORMAT_AIFF | SF_FORMAT_PCM_U8:
      w->bps = 8;
      break;

    case SF_FORMAT_WAV  | SF_FORMAT_PCM_24:
    case SF_FORMAT_AIFF | SF_FORMAT_PCM_24:
      w->bps = 24;
      break;

    case SF_FORMAT_WAV  | SF_FORMAT_PCM_32:
    case SF_FORMAT_WAV  | SF_FORMAT_FLOAT:
    case SF_FORMAT_WAV  | SF_FORMAT_DOUBLE:
    case SF_FORMAT_AIFF | SF_FORMAT_PCM_32:
    case SF_FORMAT_AIFF | SF_FORMAT_FLOAT:
    case SF_FORMAT_AIFF | SF_FORMAT_DOUBLE:
      w->bps = 32;
      break;

    default:
      w->bps = 16;
  }

  return 0;
}

static ULONG
snd_close( DECODER_STRUCT* w )
{
  if( w->sndfile ) {
    sf_close( w->sndfile );
    w->sndfile = NULL;
  }
  if( w->file ) {
    xio_fclose( w->file );
    w->file = NULL;
  }
  return 0;
}

/* Notify an error. */
static void
snd_show_error( DECODER_STRUCT* w, int style, const char* msg, int rc )
{
  char buff[1024];

  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );

  if( rc == -1 ) {
    strlcat( buff, "Unsupported format of the file.", sizeof( buff ));
  } else if( sf_error( w->sndfile )) {
    strlcat( buff, sf_strerror( w->sndfile ), sizeof( buff ));
  } else if( xio_errno()) {
    strlcat( buff, xio_strerror( xio_errno()), sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}

/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  ULONG resetcount;
  ULONG markerpos;
  ULONG rc;

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  char* buffer = NULL;

  w->frew = FALSE;
  w->ffwd = FALSE;
  w->stop = FALSE;

  if(( rc = snd_open( w, SFM_READ )) != 0 ) {
    snd_show_error( w, MB_ERROR, "Unable open file", xio_errno());
    goto end;
  }

  if(( w->sfinfo.format & SF_FORMAT_SUBMASK ) == SF_FORMAT_FLOAT ||
     ( w->sfinfo.format & SF_FORMAT_SUBMASK ) == SF_FORMAT_DOUBLE )
  {
    float scale = 1.0;
    sf_command( w->sndfile, SFC_SET_SCALE_FLOAT_INT_READ, &scale, SF_TRUE );
  }

  // After opening a new file we so are in its beginning.
  if( w->jumptopos == 0 && w->startpos == 0 ) {
      w->jumptopos = -1;
  }

  if(!( buffer = (char*)malloc( w->audio_buffersize ))) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  w->output_format.size       = sizeof( w->output_format );
  w->output_format.format     = WAVE_FORMAT_PCM;
  w->output_format.bits       = w->bps;
  w->output_format.channels   = w->sfinfo.channels;
  w->output_format.samplerate = w->sfinfo.samplerate;

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop )
    {
      int read;
      int write;

      if( w->jumptopos >= 0 )
      {
        sf_count_t jumptoframe = ceil(( w->jumptopos + w->startpos ) / 1000.0 * w->sfinfo.samplerate );
        DEBUGLOG(( "wavplay: jumps to %ld ms is %lld (%f) frame\n", w->jumptopos + w->startpos, jumptoframe,
                 ( w->jumptopos + w->startpos ) / 1000.0 * w->sfinfo.samplerate ));

        rc = sf_seek( w->sndfile, jumptoframe, SEEK_SET );
        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        if( rc == -1 ) {
          snd_show_error( w, MB_WARNING, "Unable seek in file", xio_errno());
        }

        w->jumptopos = -1;
      }

      if( w->frew || w->ffwd )
      {
        // The skip of this interval will force the decoder to be
        // accelerated approximately in 10 times.
        int skip = w->audio_buffersize /
                   w->output_format.channels / ( w->output_format.bits / 8 );

        if( skip ) {
          if( w->frew ) {
            if( sf_seek( w->sndfile, -11 * skip, SEEK_CUR ) < 0 ) {
              break;
            }
          } else if( w->ffwd ) {
            if( sf_seek( w->sndfile,   9 * skip, SEEK_CUR ) < 0 ) {
              break;
            }
          }
        }
      }

      markerpos = 1000.0 * sf_seek( w->sndfile, 0, SEEK_CUR ) / w->sfinfo.samplerate;

      if( w->endpos )
      {
        read = ( w->endpos / 1000.0 * w->sfinfo.samplerate - sf_seek( w->sndfile, 0, SEEK_CUR ))
               * w->sfinfo.channels * sizeof(short);

        if( read > w->audio_buffersize ) {
            read = w->audio_buffersize;
        }
      } else {
        read = w->audio_buffersize;
      }

      DEBUGLOG2(( "wavplay: read %d bytes from %f ms (%lld frame) [%d,%d]\n", read,
                  1000.0 * sf_seek( w->sndfile, 0, SEEK_CUR ) / w->sfinfo.samplerate,
                  sf_seek( w->sndfile, 0, SEEK_CUR ), w->startpos, w->endpos ));

      if(( w->startpos && markerpos <  w->startpos ) ||
         ( w->endpos   && markerpos >= w->endpos   ))
      {
        break;
      }

      switch( w->sfinfo.format & ( SF_FORMAT_SUBMASK | SF_FORMAT_TYPEMASK ))
      {
        case SF_FORMAT_WAV  | SF_FORMAT_PCM_U8:
        case SF_FORMAT_AIFF | SF_FORMAT_PCM_U8:
        case SF_FORMAT_WAV  | SF_FORMAT_PCM_16:
        case SF_FORMAT_AIFF | SF_FORMAT_PCM_16:
        case SF_FORMAT_WAV  | SF_FORMAT_PCM_24:
        case SF_FORMAT_AIFF | SF_FORMAT_PCM_24:
        case SF_FORMAT_WAV  | SF_FORMAT_PCM_32:
        case SF_FORMAT_AIFF | SF_FORMAT_PCM_32:
          if(( read = sf_read_raw( w->sndfile, (void*)buffer, read )) > 0 )
          {
            if(( w->sfinfo.format & SF_FORMAT_TYPEMASK ) == SF_FORMAT_AIFF &&
               ( w->sfinfo.format & SF_FORMAT_ENDMASK  ) != SF_ENDIAN_LITTLE )
            {
              uint8_t b1, b2, b3, b4;
              int     i;

              switch( w->sfinfo.format & SF_FORMAT_SUBMASK ) {
                case SF_FORMAT_PCM_16:
                  for( i = 0; i < read; i += 2 ) {
                    b1 = buffer[i]; b2 = buffer[i+1];
                    buffer[i] = b2; buffer[i+1] = b1;
                  }
                  break;

                case SF_FORMAT_PCM_24:
                  for( i = 0; i < read; i += 3 ) {
                    b1 = buffer[i]; b3 = buffer[i+2];
                    buffer[i] = b3; buffer[i+2] = b1;
                  }
                  break;

                case SF_FORMAT_PCM_32:
                  for( i = 0; i < read; i += 4 ) {
                    b1 = buffer[i]; b2 = buffer[i+1]; b3 = buffer[i+2]; b4 = buffer[i+3];
                    buffer[i] = b4; buffer[i+1] = b3; buffer[i+2] = b2; buffer[i+3] = b1;
                  }
                  break;
              }
            }
            write = w->output_play_samples( w->a, &w->output_format, buffer, read, markerpos - w->startpos );
          }
          break;

        default:
          if( w->bps == 32 ) {
            if(( read = sf_read_int( w->sndfile, (int*)buffer, read / sizeof( int ))) > 0 ) {
              read *= sizeof( int );
              write = w->output_play_samples( w->a, &w->output_format, buffer, read, markerpos - w->startpos );
            }
          } else {
            if(( read = sf_read_short( w->sndfile, (short*)buffer, read / sizeof( short ))) > 0 ) {
              read *= sizeof( short );
              write = w->output_play_samples( w->a, &w->output_format, buffer, read, markerpos - w->startpos );
            }
          }
          break;
      }

      if( read <= 0 ) {
        if( sf_error( w->sndfile )) {
          snd_show_error( w, MB_ERROR, "Unable read file", sf_error( w->sndfile ));
          goto end;
        } else {
          break;
        }
      }

      if( write != read ) {
        WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
        w->status = DECODER_ERROR;
        goto end;
      }
    }

    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

  w->status = DECODER_STOPPED;

end:

  snd_close( w );
  free( buffer );

  w->decodertid = -1;
  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;
  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than yours is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = arg;

  decoder_command ( w, DECODER_STOP, NULL );
  DosCloseEventSem( w->play  );
  free( w );
  return TRUE;
}

ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = arg;

  switch(msg)
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      strlcpy( w->filename, info->filename, sizeof( w->filename ));

      w->startpos   = info->start;
      w->endpos     = info->end;
      w->jumptopos  = info->jumpto;
      w->status     = DECODER_STARTING;
      w->seekable   = TRUE;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;

      if( w->file ) {
        xio_fabort( w->file );
      }
      DosPostEventSem( w->play );
      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_FFWD:
      if( info->ffwd ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->ffwd = info->ffwd;
      break;

    case DECODER_REW:
      if( info->rew ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->frew = info->rew;
      break;

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      DosPostEventSem( w->play );
      break;

    default:
      return PLUGIN_UNSUPPORTED;
   }

   return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = arg;

  if( w->seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else if( w->sfinfo.samplerate ) {
      return 1000.0 * w->sfinfo.frames / w->sfinfo.samplerate;
    } else {
      return 0;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

static void
copy_string( SNDFILE* file, char* target, int type, int size )
{
  const char* string;

  if(( string = sf_get_string( file, type )) != NULL ) {
    strlcpy( target, string, size );
  }
}

ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  DECODER_STRUCT w = { 0 };
  SF_FORMAT_INFO format_info;
  SF_FORMAT_INFO format_more;
  ULONG rc;

  strlcpy( w.filename, filename, sizeof( w.filename ));
  if(( rc = snd_open( &w, SFM_READ )) == 0 )
  {
    info->format.size       = sizeof( info->format );
    info->format.format     = WAVE_FORMAT_PCM;
    info->format.bits       = w.bps;
    info->format.channels   = w.sfinfo.channels;
    info->format.samplerate = w.sfinfo.samplerate;
    info->mode              = info->format.channels == 1 ? 3 : 0;
    info->songlength        = 1000.0 * w.sfinfo.frames / w.sfinfo.samplerate;

    if( HAVE_FIELD( info, filesize )) {
      info->filesize = xio_fsize64( w.file );
    }

    if( w.sfinfo.frames ) {
      info->bitrate = 8.0 * info->filesize * w.sfinfo.samplerate / w.sfinfo.frames / 1000;
    }

    format_info.format = w.sfinfo.format & SF_FORMAT_TYPEMASK;
    sf_command( w.sndfile, SFC_GET_FORMAT_INFO, &format_info, sizeof( format_info ));
    format_more.format = w.sfinfo.format & SF_FORMAT_SUBMASK;
    sf_command( w.sndfile, SFC_GET_FORMAT_INFO, &format_more, sizeof( format_more ));

    snprintf( info->tech_info, sizeof( info->tech_info ), "%s, %s, %.1f kHz, %s",
              format_info.name, format_more.name, (float)info->format.samplerate / 1000,
              info->format.channels == 1 ? "Mono" : "Stereo" );

    copy_string( w.sndfile, info->title,   SF_STR_TITLE,   sizeof( info->title   ));
    copy_string( w.sndfile, info->artist,  SF_STR_ARTIST,  sizeof( info->artist  ));
    copy_string( w.sndfile, info->year,    SF_STR_DATE,    sizeof( info->year    ));
    copy_string( w.sndfile, info->comment, SF_STR_COMMENT, sizeof( info->comment ));

    if( HAVE_FIELD( info, copyright ))
    {
      copy_string( w.sndfile, info->copyright, SF_STR_COPYRIGHT, sizeof( info->copyright ));

      info->saveinfo = FALSE;
      info->haveinfo = DECODER_HAVE_TITLE    |
                       DECODER_HAVE_ARTIST   |
                       DECODER_HAVE_YEAR     |
                       DECODER_HAVE_COMMENT  |
                       DECODER_HAVE_COPYRIGHT;
    }
    snd_close( &w );
  }

  if( rc == -1 ) {
    return PLUGIN_NO_PLAY;
  } else if( rc == 0 ) {
    return PLUGIN_OK;
  } else {
    return PLUGIN_NO_READ;
  }
}

ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 19 )
    {
      strcpy( ext[ 0], "*.WAV"  );
      strcpy( ext[ 1], "*.AIF"  );
      strcpy( ext[ 2], "*.AIFF" );
      strcpy( ext[ 3], "*.AU"   );
      strcpy( ext[ 4], "*.AVR"  );
      strcpy( ext[ 5], "*.CAF"  );
      strcpy( ext[ 6], "*.IFF"  );
      strcpy( ext[ 7], "*.MAT"  );
      strcpy( ext[ 8], "*.PAF"  );
      strcpy( ext[ 9], "*.PVF"  );
      strcpy( ext[10], "*.SD2"  );
      strcpy( ext[11], "*.SDS"  );
      strcpy( ext[12], "*.SF"   );
      strcpy( ext[13], "*.VOC"  );
      strcpy( ext[14], "*.W64"  );
      strcpy( ext[15], "*.XI"   );

      strcpy( ext[16], "audio/basic" );
      strcpy( ext[17], "audio/wav"   );
      strcpy( ext[18], "audio/aiff"  );
    }
    *size = 19;
  }

  return DECODER_FILENAME | DECODER_URL;
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "WAV Play " VER_STRING;
  param->configurable = FALSE;
}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    _CRT_term();
  }
  return 1UL;
}
#endif

