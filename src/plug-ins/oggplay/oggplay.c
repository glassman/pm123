/*
 * Copyright 2007-2022 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_DOS
#define  INCL_WIN
#define  INCL_ERRORS
#include <os2.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <process.h>
#include <errno.h>

#define  OV_EXCLUDE_STATIC_CALLBACKS
#include <vorbis/vorbisfile.h>
#include <decoder_plug.h>
#include <plugin.h>
#include <utilfct.h>
#include <debuglog.h>
#include <b64.h>

#include "oggplay.h"
#include "vcedit.h"

static DECODER_STRUCT** instances = NULL;
static int  instances_count = 0;
static HMTX mutex;

#define BE32(x) (((unsigned char *)&x)[0] << 24 | \
                 ((unsigned char *)&x)[1] << 16 | \
                 ((unsigned char *)&x)[2] <<  8 | \
                 ((unsigned char *)&x)[3] )

/* Changes the current file position to a new location within the file.
   Returns 0 if it successfully moves the pointer. A nonzero return
   value indicates an error. On devices that cannot seek the return
   value is nonzero. */
static int DLLENTRY
vio_seek( void* w, ogg_int64_t offset, int whence )
{
  int    pos = 0;
  XFILE* x   = ((DECODER_STRUCT*)w)->file;

  if( !xio_can_seek(x)) {
    return -1;
  }

  switch( whence )
  {
    case SEEK_SET: pos = offset; break;
    case SEEK_CUR: pos = xio_ftell(x) + offset; break;
    case SEEK_END: pos = xio_fsize(x) + offset; break;
    default:
      return -1;
  }

  if( xio_fseek( x, pos, XIO_SEEK_SET ) == 0 ) {
    return pos;
  } else {
    return -1;
  }
}

/* Finds the current position of the file. Returns the current file
   position. On error, returns -1L and sets errno to a nonzero value. */
static long DLLENTRY
vio_tell( void* w ) {
  return xio_ftell(((DECODER_STRUCT*)w)->file );
}

/* Reads up to count items of size length from the input file and
   stores them in the given buffer. The position in the file increases
   by the number of bytes read. Returns the number of full items
   successfully read, which can be less than count if an error occurs
   or if the end-of-file is met before reaching count. */
static size_t DLLENTRY
vio_read( void* ptr, size_t size, size_t count, void* w ) {
  return xio_fread( ptr, size, count, ((DECODER_STRUCT*)w)->file );
}

/* Closes a pointed file. Returns 0 if it successfully closes
   the file, or -1 if any errors were detected. */
static int DLLENTRY
vio_close( void* w )
{
  int rc = xio_fclose(((DECODER_STRUCT*)w)->file );
  ((DECODER_STRUCT*)w)->file = NULL;
  return rc;
}

/* Opens Ogg Vorbis file. Returns 0 if it successfully opens the file.
   A nonzero return value indicates an error. A -1 return value
   indicates an unsupported format of the file. */
static ULONG
ogg_open( DECODER_STRUCT* w )
{
  ov_callbacks callbacks;
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  w->comment = NULL;
  w->vrbinfo = NULL;

  if(( w->file = xio_fopen( w->filename, "rb" )) == NULL ) {
    DosReleaseMutexSem( w->mutex );
    return xio_errno();
  }

  callbacks.read_func  = vio_read;
  callbacks.seek_func  = vio_seek;
  callbacks.tell_func  = vio_tell;
  callbacks.close_func = vio_close;

  // The ov_open_callbacks() function performs full stream detection and machine
  // initialization. If it returns 0, the stream *is* Vorbis and we're
  // fully ready to decode.
  if( ov_open_callbacks( w, &w->vrbfile, NULL, 0, callbacks ) < 0 ) {
    vio_close( w );
    DosReleaseMutexSem( w->mutex );
    return -1;
  }

  if(( w->vrbinfo = ov_info( &w->vrbfile, -1 )) == NULL ) {
    ov_clear( &w->vrbfile );
    DosReleaseMutexSem( w->mutex );
    return -1;
  }

  w->songlength  = ov_time_total( &w->vrbfile, -1 ) * 1000.0;
  w->comment     = ov_comment( &w->vrbfile, -1 );
  w->bitrate     = ov_bitrate( &w->vrbfile, -1 ) / 1000;
  w->played_pcms = 0;
  w->played_secs = 0;
  w->seekable    = xio_can_seek( w->file );

  w->output_format.size       = sizeof( w->output_format );
  w->output_format.format     = WAVE_FORMAT_PCM;
  w->output_format.bits       = 16;
  w->output_format.channels   = w->vrbinfo->channels;
  w->output_format.samplerate = w->vrbinfo->rate;

  if( w->metadata_buff && w->metadata_size && w->hwnd ) {
    xio_set_observer( w->file, w->hwnd, w->metadata_buff, w->metadata_size );
  }

  DosReleaseMutexSem( w->mutex );
  return 0;
}

/* Reads up to count pcm bytes from the Ogg Vorbis stream and
   stores them in the given buffer. */
static int
ogg_read( DECODER_STRUCT* w, char* buffer, int count )
{
  int done;
  int read    =  0;
  int section = -1;
  int resync  =  0;

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->file ) {
    while( read < count )
    {
      done = ov_read( &w->vrbfile, buffer + read, count - read, 0, 2, 1, &section );

      if( done == OV_HOLE ) {
        if( resync++ < MAXRESYNC ) {
          continue;
        }
      }
      if( done <= 0 ) {
        break;
      } else {
        read += done;
      }
    }

    w->played_pcms = ov_pcm_tell ( &w->vrbfile );
    w->played_secs = ov_time_tell( &w->vrbfile );
  } else {
    read = -1;
  }

  DosReleaseMutexSem( w->mutex );
  return read;
}

/* Closes Ogg Vorbis file. */
static void
ogg_close( DECODER_STRUCT* w )
{
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->file ) {
    ov_clear( &w->vrbfile );
  }
  DosReleaseMutexSem( w->mutex );
}

#define OGG_SEEK_SET 0
#define OGG_SEEK_CUR 1

/* Changes the current file position to a new time within the file.
   Returns 0 if it successfully moves the pointer. A nonzero return
   value indicates an error. On devices that cannot seek the return
   value is nonzero. */
static int
ogg_seek( DECODER_STRUCT* w, double secs, int origin )
{
  int rc;
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->file ) {
    switch( origin ) {
      case OGG_SEEK_SET:
        rc = ov_time_seek( &w->vrbfile, secs );
        break;
      case OGG_SEEK_CUR:
        rc = ov_time_seek( &w->vrbfile, w->played_secs + secs );
        break;
      default:
        rc = -1;
    }

    if( rc == 0 ) {
      w->played_pcms = ov_pcm_tell ( &w->vrbfile );
      w->played_secs = ov_time_tell( &w->vrbfile );
    }
  } else {
    rc = -1;
  }

  DosReleaseMutexSem( w->mutex );
  return rc;
}

/* Returns a specified field of the given Ogg Vorbis comment. */
static char*
ogg_get_string( DECODER_STRUCT* w, char* target, char* type, int size )
{
  const char* string;
  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

  if( w->comment && ( string = vorbis_comment_query( w->comment, type, 0 )) != NULL ) {
    ch_convert( CH_UTF_8, string, CH_DEFAULT, target, size );
  } else if( size ) {
    *target = 0;
  }

  DosReleaseMutexSem( w->mutex );
  return target;
}

/* Sets a specified field of the given Ogg Vorbis comment. */
static void
ogg_set_string( vorbis_comment* comment, char* source, char* type )
{
  char string[128*4];

  ch_convert( CH_DEFAULT, source, CH_UTF_8, string, sizeof( string ));
  vorbis_comment_add_tag( comment, type, string );
}

/* Notify an error. */
static void
ogg_show_error( DECODER_STRUCT* w, int style, const char* msg, int rc )
{
  char buff[1024];

  snprintf( buff, sizeof( buff ), "%s:\n%s\n", msg, w->filename );

  if( rc == -1 ) {
    strlcat( buff, "Unsupported format of the file.", sizeof( buff ));
  } else if( xio_errno()) {
    strlcat( buff, xio_strerror( xio_errno()), sizeof( buff ));
  }

  if( style == MB_ERROR ) {
    w->error_display( buff );
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
  } else {
    w->info_display( buff );
  }
}


/* Decoding thread. */
static void TFNENTRY
decoder_thread( void* arg )
{
  ULONG resetcount;
  int   posmarker;
  ULONG rc;
  int   bitrate;

  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;

  char* buffer = NULL;
  char  last_artist[256] = "";
  char  curr_artist[256];
  char  last_title [256] = "";
  char  curr_title [256];
  char  last_album [256] = "";
  char  curr_album [256];

  w->frew = FALSE;
  w->ffwd = FALSE;

  if(( rc = ogg_open( w )) != 0 ) {
    ogg_show_error( w, MB_ERROR, "Unable open file", rc );
    goto end;
  }

  // After opening a new file we so are in its beginning.
  if( w->jumptopos == 0 && w->startpos == 0 ) {
      w->jumptopos = -1;
  }

  if(!( buffer = (char*)malloc( w->audio_buffersize ))) {
    w->status = DECODER_ERROR;
    WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
    goto end;
  }

  for(;;)
  {
    DosWaitEventSem ( w->play, SEM_INDEFINITE_WAIT );
    DosResetEventSem( w->play, &resetcount );

    if( w->stop ) {
      break;
    }

    w->status = DECODER_PLAYING;
    xio_fwait( w->file );

    while( !w->stop )
    {
      int read, write;

      if( w->jumptopos >= 0 ) {
        DEBUGLOG(( "oggplay: jumps to %d ms (%f sec)\n", w->jumptopos + w->startpos, ((double)w->jumptopos + w->startpos) / 1000 ));
        rc = ogg_seek( w, ((double)w->jumptopos + w->startpos) / 1000, OGG_SEEK_SET );
        DosResetEventSem( w->play, &resetcount );
        WinPostMsg( w->hwnd, WM_SEEKSTOP, 0, 0 );

        if( rc != 0 ) {
          ogg_show_error( w, MB_WARNING, "Unable seek in file", xio_errno());
        }

        w->jumptopos = -1;
      }

      if( w->frew || w->ffwd )
      {
        // The skip of this interval will force the decoder to be
        // accelerated approximately in 10 times.
        float skip = (float)w->audio_buffersize  /
                     w->output_format.samplerate /
                     w->output_format.channels   / ( w->output_format.bits / 8 );

        if( skip ) {
          if( w->frew ) {
            if( ogg_seek( w, -11 * skip, OGG_SEEK_CUR ) != 0 ) {
              break;
            }
          } else if( w->ffwd ) {
            ogg_seek( w, 9 * skip, OGG_SEEK_CUR );
          }
        }
      }

      posmarker = 1000 * ov_time_tell( &w->vrbfile ) + 0.5;
      DEBUGLOG2(( "oggplay: played pos is %d ms (%f sec)\n", posmarker, ov_time_tell( &w->vrbfile )));

      if(( w->startpos && posmarker <  w->startpos ) ||
         ( w->endpos   && posmarker >= w->endpos   )  )
      {
        DEBUGLOG(( "oggplay: break at %d because edge of fragment is reached\n", posmarker ));
        break;
      }

      if( w->endpos )
      {
        read = ( w->endpos / 1000.0 * w->output_format.samplerate - ov_pcm_tell( &w->vrbfile ))
               * w->output_format.channels * sizeof(short);

        if( read > w->audio_buffersize ) {
            read = w->audio_buffersize;
        }
      } else {
        read = w->audio_buffersize;
      }

      read = ogg_read( w, buffer, read );

      if( read <= 0 ) {
        if( xio_ferror( w->file )) {
          ogg_show_error( w, MB_ERROR, "Unable read file", xio_errno());
          goto end;
        } else {
          break;
        }
      }

      write = w->output_play_samples( w->a, &w->output_format, buffer, read, posmarker - w->startpos );

      if( write != read ) {
        w->status = DECODER_ERROR;
        WinPostMsg( w->hwnd, WM_PLAYERROR, 0, 0 );
        goto end;
      }

      bitrate = ov_bitrate_instant( &w->vrbfile ) / 1000;
      if( bitrate > 0 && w->bitrate != bitrate ) {
        WinPostMsg( w->hwnd, WM_CHANGEBR, MPFROMLONG( bitrate ), 0 );
        w->bitrate = bitrate;
      }

      if( w->metadata_buff && w->metadata_size && w->hwnd )
      {
        ogg_get_string( w, curr_title,  "TITLE",  sizeof( curr_title  ));
        ogg_get_string( w, curr_artist, "ARTIST", sizeof( curr_artist ));
        ogg_get_string( w, curr_album,  "ALBUM",  sizeof( curr_album  ));

        if( strcmp( curr_title,  last_title  ) != 0 ||
            strcmp( curr_artist, last_artist ) != 0 ||
            strcmp( curr_album,  last_album  ) != 0 )
        {
          strcpy( last_title,  curr_title  );
          strcpy( last_artist, curr_artist );
          strcpy( last_album,  curr_album  );

          snprintf( w->metadata_buff, w->metadata_size,
                    "StreamTitle='%s';StreamArtist='%s';StreamAlbum='%s';",
                    last_title, last_artist, last_album );

          WinPostMsg( w->hwnd, WM_METADATA, MPFROMP( w->metadata_buff ), 0 );
        }
      }
    }

    WinPostMsg( w->hwnd, WM_PLAYSTOP, 0, 0 );
    w->status = DECODER_STOPPED;
  }

  w->status = DECODER_STOPPED;

end:

  ogg_close( w );
  free( buffer );

  _endthread();
}

/* Init function is called when PM123 needs the specified decoder to play
   the stream demanded by the user. */
int DLLENTRY
decoder_init( void** returnw )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)calloc( sizeof(*w), 1 );
  *returnw = w;

  DosCreateEventSem( NULL, &w->play,  0, FALSE );
  DosCreateMutexSem( NULL, &w->mutex, 0, FALSE );

  w->decodertid = -1;
  w->status = DECODER_STOPPED;
  w->file = NULL;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  instances = realloc( instances, ++instances_count * sizeof( DECODER_STRUCT* ));
  instances[ instances_count - 1 ] = w;
  DosReleaseMutexSem( mutex );

  return PLUGIN_OK;
}

/* Uninit function is called when another decoder than this is needed. */
BOOL DLLENTRY
decoder_uninit( void* arg )
{
  DECODER_STRUCT* w = (DECODER_STRUCT*)arg;
  int i;

  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );
  decoder_command( w, DECODER_STOP, NULL );

  for( i = 0; i < instances_count; i++ ) {
    if( instances[i] == w ) {
      if( i < instances_count - 1 ) {
        memmove( instances + i, instances + i + 1,
               ( instances_count - i - 1 ) * sizeof( DECODER_STRUCT* ));
      }
      instances = realloc( instances, --instances_count * sizeof( DECODER_STRUCT* ));
    }
  }

  DosReleaseMutexSem( mutex  );
  DosCloseEventSem( w->play  );
  DosCloseMutexSem( w->mutex );

  free( w );
  return TRUE;
}

/* There is a lot of commands to implement for this function. Parameters
   needed for each of the are described in the definition of the structure
   in the decoder_plug.h file. */
ULONG DLLENTRY
decoder_command( void* arg, ULONG msg, DECODER_PARAMS* info )
{
  DECODER_STRUCT* w = arg;

  switch( msg )
  {
    case DECODER_SETUP:
      w->output_play_samples = info->output_play_samples;
      w->hwnd                = info->hwnd;
      w->error_display       = info->error_display;
      w->info_display        = info->info_display;
      w->a                   = info->a;
      w->audio_buffersize    = info->audio_buffersize;
      w->metadata_buff       = info->metadata_buffer;
      w->metadata_buff[0]    = 0;
      w->metadata_size       = info->metadata_size;
      break;

    case DECODER_PLAY:
    {
      if( w->decodertid != -1 ) {
        if( w->status == DECODER_STOPPED || w->status == DECODER_ERROR ) {
          decoder_command( w, DECODER_STOP, NULL );
        } else {
          return PLUGIN_GO_ALREADY;
        }
      }

      DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
      strlcpy( w->filename, info->filename, sizeof( w->filename ));
      DosReleaseMutexSem( w->mutex );

      w->startpos   = info->start;
      w->endpos     = info->end;
      w->jumptopos  = info->jumpto;
      w->status     = DECODER_STARTING;
      w->stop       = FALSE;
      w->songlength = 0;
      w->seekable   = TRUE;
      w->decodertid = _beginthread( decoder_thread, 0, 65535, (void*)w );

      DosPostEventSem( w->play  );
      break;
    }

    case DECODER_STOP:
    {
      if( w->decodertid == -1 ) {
        return PLUGIN_GO_ALREADY;
      }

      w->stop = TRUE;

      if( w->file ) {
        xio_fabort( w->file );
      }

      DosPostEventSem( w->play  );
      wait_thread( w->decodertid, 5000 );
      w->status = DECODER_STOPPED;
      w->decodertid = -1;
      break;
    }

    case DECODER_FFWD:
      if( info->ffwd ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->ffwd = info->ffwd;
      break;

    case DECODER_REW:
      if( info->rew ) {
        if( w->decodertid == -1 || xio_can_seek( w->file ) != XIO_CAN_SEEK_FAST ) {
          return PLUGIN_UNSUPPORTED;
        }
      }
      w->frew = info->rew;
      break;

    case DECODER_JUMPTO:
      w->jumptopos = info->jumpto;
      if( w->status == DECODER_STOPPED ) {
        DosPostEventSem( w->play );
      }
      break;

    default:
      return PLUGIN_UNSUPPORTED;
  }

  return PLUGIN_OK;
}

/* Returns current status of the decoder. */
ULONG DLLENTRY
decoder_status( void* arg ) {
  return ((DECODER_STRUCT*)arg)->status;
}

/* Returns number of milliseconds the stream lasts. */
ULONG DLLENTRY
decoder_length( void* arg )
{
  DECODER_STRUCT* w = arg;

  if( w->seekable ) {
    if( w->endpos ) {
      return w->endpos - w->startpos;
    } else {
      return w->songlength;
    }
  } else {
    // Zero value disable PM123 seeking features.
    return 0;
  }
}

/* Returns information about specified file. */
ULONG DLLENTRY
decoder_fileinfo( char* filename, DECODER_INFO* info, int options )
{
  DECODER_STRUCT* w;
  char* data;

  int rc;
  int count = 0;
  int i;

  if( decoder_init((void**)(void*)&w ) != PLUGIN_OK ) {
    return PLUGIN_NO_PLAY;
  }

  DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
  strlcpy( w->filename, filename, sizeof( w->filename ));

  if(( rc = ogg_open( w )) != 0 ) {
    DosReleaseMutexSem( w->mutex );
    return rc == -1 ? PLUGIN_NO_PLAY : PLUGIN_NO_READ;
  }

  info->format     = w->output_format;
  info->songlength = w->songlength;
  info->mode       = w->output_format.channels == 1 ? 3 : 0;
  info->bitrate    = w->bitrate;

  if( HAVE_FIELD( info, filesize )) {
    info->filesize = xio_fsize64( w->file );
  }

  DosReleaseMutexSem( w->mutex );

  if( info->songlength <= 0 ) {
    info->songlength = -1;
  }

  snprintf( info->tech_info, sizeof( info->tech_info ), "%u kbs, %.1f kHz, %s",
            info->bitrate, ( info->format.samplerate / 1000.0 ),
            info->format.channels == 1 ? "Mono" : "Stereo" );

  ogg_get_string( w, info->artist,  "ARTIST",  sizeof( info->artist  ));
  ogg_get_string( w, info->album,   "ALBUM",   sizeof( info->album   ));
  ogg_get_string( w, info->title,   "TITLE",   sizeof( info->title   ));
  ogg_get_string( w, info->genre,   "GENRE",   sizeof( info->genre   ));
  ogg_get_string( w, info->year,    "YEAR" ,   sizeof( info->year    ));
  ogg_get_string( w, info->comment, "COMMENT", sizeof( info->comment ));

  if( !*info->year ) {
    ogg_get_string( w, info->year, "DATE", sizeof( info->year ));
  }

  if( HAVE_FIELD( info, track ))
  {
    char buffer[128];

    ogg_get_string( w, info->copyright, "COPYRIGHT",   sizeof( info->copyright ));
    ogg_get_string( w, info->disc,      "DISCNUMBER",  sizeof( info->disc      ));
    ogg_get_string( w, info->track,     "TRACKNUMBER", sizeof( info->track     ));

    ogg_get_string( w, buffer, "replaygain_album_gain", sizeof( buffer ));
    info->album_gain = atof( buffer );
    ogg_get_string( w, buffer, "replaygain_album_peak", sizeof( buffer ));
    info->album_peak = atof( buffer );
    ogg_get_string( w, buffer, "replaygain_track_gain", sizeof( buffer ));
    info->track_gain = atof( buffer );
    ogg_get_string( w, buffer, "replaygain_track_peak", sizeof( buffer ));
    info->track_peak = atof( buffer );

    info->saveinfo = is_file( filename );
    info->haveinfo = DECODER_HAVE_TITLE      |
                     DECODER_HAVE_ARTIST     |
                     DECODER_HAVE_ALBUM      |
                     DECODER_HAVE_YEAR       |
                     DECODER_HAVE_GENRE      |
                     DECODER_HAVE_TRACK      |
                     DECODER_HAVE_DISC       |
                     DECODER_HAVE_COMMENT    |
                     DECODER_HAVE_COPYRIGHT  |
                     DECODER_HAVE_TRACK_GAIN |
                     DECODER_HAVE_TRACK_PEAK |
                     DECODER_HAVE_ALBUM_GAIN |
                     DECODER_HAVE_ALBUM_PEAK |
                     DECODER_HAVE_PICS       |
                     DECODER_PICS_ANYTYPE;

    info->codepage = ch_default();
  }

  if( HAVE_FIELD( info, pics_count ))
  {
    DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );
    count = vorbis_comment_query_count( w->comment, "METADATA_BLOCK_PICTURE" );

    DEBUGLOG(( "oggplay: pics count is %d\n", count ));

    if( count && ( options & DECODER_LOADPICS )) {
      if(( info->pics = info->memalloc( sizeof( APIC* ) * count )) != NULL ) {
        for( i = 0; ( data = vorbis_comment_query( w->comment, "METADATA_BLOCK_PICTURE", i )) != NULL; i++ )
        {
          APIC*  pic;
          int    pic_size;
          size_t size;

          if(( data = (char*)b64_decode_ex( data, strlen( data ), &size )) != NULL )
          {
            ogg_int32_t* p = (ogg_int32_t*)data;

            // <32>  The picture type according to the ID3v2 APIC frame.
            // <32>  The length of the MIME type string in bytes.
            // <n*8> The MIME type string, in printable ASCII characters 0x20-0x7e.
            // <32>  The length of the description string in bytes.
            // <n*8> The description of the picture, in UTF-8.
            // <32>  The width of the picture in pixels.
            // <32>  The height of the picture in pixels.
            // <32>  The color depth of the picture in bits-per-pixel.
            // <32>  For indexed-color pictures (e.g. GIF), the number of colors used, or 0 for non-indexed pictures.
            // <32>  The length of the picture data in bytes.
            // <n*8> The binary picture data.

            // skip mime type string
            p = (ogg_int32_t*)((char*)(p+2) + BE32(p[1]));
            if((char*)p >= (char*)data + size ) {
              count = i;
              break;
            }

            // skip description string
            p = (ogg_int32_t*)((char*)(p+1) + BE32(p[0]));
            if((char*)p >= (char*)data + size ) {
              count = i;
              break;
            }

            pic_size = BE32(p[4]) + sizeof( APIC );
            if((char*)(p+5) + BE32(p[4]) > (char*)data + size ) {
              count = i;
              break;
            }

            DEBUGLOG(( "oggplay: load attached picture %d with dimensions %dx%d, color depth is %d bps, %d colors, data size %d\n",
                        i, BE32(p[0]), BE32(p[1]), BE32(p[2]), BE32(p[3]), pic_size -  sizeof( APIC )));

            if(( info->pics[i] = info->memalloc( pic_size )) == NULL ) {
              count = i;
              break;
            }

            pic = info->pics[i];
            p = (ogg_int32_t*)data;

            pic->size = pic_size;
            pic->type = BE32(p[0]);

            strlcpy( pic->mimetype, (char*)(p+2), BE32(p[1]) + 1 );
            DEBUGLOG(( "oggplay: mime type '%s' length is %d bytes\n", pic->mimetype, BE32(p[1])));
            p = (ogg_int32_t*)((char*)(p+2) + BE32(p[1]));

            // Because we do not need the width of the picture, we can store \0 there
            // so that we can safe use the ch_convert.
            *((char*)(p+1) + BE32(p[0])) = 0;
            ch_convert( CH_UTF_8, (char*)(p+1), CH_DEFAULT, pic->desc, sizeof( pic->desc ));
            DEBUGLOG(( "oggplay: description '%s' length is %d bytes\n", pic->desc, BE32(p[0])));
            p = (ogg_int32_t*)((char*)(p+1) + BE32(p[0]));

            memcpy((char*)pic + sizeof( APIC ), (CHAR*)(p + 5), BE32(p[4]));
            free( data );
          } else {
            count = i;
            break;
          }
        }
      }
    }

    info->pics_count = count;
    DEBUGLOG(( "oggplay: found %d attached pictures\n", info->pics_count ));

    DosReleaseMutexSem( w->mutex );
  }

  ogg_close( w );
  decoder_uninit( w );
  return PLUGIN_OK;
}

static size_t
vce_read( void* ptr, size_t size, size_t count, void* file ) {
  return xio_fread( ptr, size, count, (XFILE*)file );
}
static size_t
vce_write( void* ptr, size_t size, size_t count, void* file ) {
  return xio_fwrite( ptr, size, count, (XFILE*)file );
}

/* It is called if it is necessary to change the meta information
   for the specified file. */
ULONG DLLENTRY
decoder_saveinfo( char* filename, DECODER_INFO* info, int options )
{
  XFILE* file = NULL;
  XFILE* save = NULL;
  int    rc = PLUGIN_OK;
  char   savename[_MAX_PATH];
  int    i;
  char   buffer[64];

  vcedit_state*   state = NULL;
  vorbis_comment* vc;
  DECODER_STRUCT* w;

  if( !*filename ) {
    return EINVAL;
  }

  strlcpy( savename, filename, sizeof( savename ));
  savename[ strlen( savename ) - 1 ] = '~';

  for(;;)
  {
    if(( file = xio_fopen( filename, "rb" )) == NULL ) {
      rc = xio_errno();
      break;
    }
    if(( save = xio_fopen( savename, "wb" )) == NULL ) {
      rc = xio_errno();
      break;
    }
    if(( state = vcedit_new_state()) == NULL ) {
      rc = errno;
      break;
    }

    if( vcedit_open_callbacks( state, file, vce_read, vce_write ) < 0 )
    {
      rc = EINVAL;
      break;
    }

    vc = vcedit_comments( state );
    vorbis_comment_clear( vc );
    vorbis_comment_init ( vc );

    ogg_set_string( vc, info->artist,    "ARTIST"      );
    ogg_set_string( vc, info->album,     "ALBUM"       );
    ogg_set_string( vc, info->title,     "TITLE"       );
    ogg_set_string( vc, info->genre,     "GENRE"       );
    ogg_set_string( vc, info->year,      "YEAR"        );
    ogg_set_string( vc, info->year,      "DATE"        );
    ogg_set_string( vc, info->track,     "TRACKNUMBER" );
    ogg_set_string( vc, info->disc,      "DISCNUMBER"  );
    ogg_set_string( vc, info->copyright, "COPYRIGHT"   );
    ogg_set_string( vc, info->comment,   "COMMENT"     );

    if( info->album_gain ) {
      sprintf( buffer, "%.2f dB", info->album_gain );
      ogg_set_string( vc, buffer, "replaygain_album_gain" );
    }
    if( info->album_peak ) {
      sprintf( buffer, "%.6f", info->album_peak );
      ogg_set_string( vc, buffer, "replaygain_album_peak" );
    }
    if( info->track_gain ) {
      sprintf( buffer, "%.2f dB", info->track_gain );
      ogg_set_string( vc, buffer, "replaygain_track_gain" );
    }
    if( info->track_peak ) {
      sprintf( buffer, "%.6f", info->track_peak );
      ogg_set_string( vc, buffer, "replaygain_track_peak" );
    }

    if( HAVE_FIELD( info, pics_count ) && ( options & DECODER_SAVEPICS )) {
      for( i = 0; i < info->pics_count; i++ )
      {
        APIC* pic = info->pics[i];
        char* data;
        char  desc[sizeof(pic->desc)*4];
        int   desc_len;
        int   mime_len;
        int   pict_len;
        int   size;

        // <32>  The picture type according to the ID3v2 APIC frame.
        // <32>  The length of the MIME type string in bytes.
        // <n*8> The MIME type string, in printable ASCII characters 0x20-0x7e.
        // <32>  The length of the description string in bytes.
        // <n*8> The description of the picture, in UTF-8.
        // <32>  The width of the picture in pixels.
        // <32>  The height of the picture in pixels.
        // <32>  The color depth of the picture in bits-per-pixel.
        // <32>  For indexed-color pictures (e.g. GIF), the number of colors used, or 0 for non-indexed pictures.
        // <32>  The length of the picture data in bytes.
        // <n*8> The binary picture data.

        ch_convert( CH_DEFAULT, pic->desc, CH_UTF_8, desc, sizeof( desc ));
        desc_len = strlen( desc );
        mime_len = strlen( pic->mimetype );
        pict_len = pic->size - sizeof( APIC );

        size = 8 + mime_len + 4 + desc_len + 20 + pict_len;
        if(( data = malloc( size )) != NULL )
        {
          ogg_int32_t* p = (ogg_int32_t*)data;
          char* encoded;

          p[0] = pic->type;
          p[1] = BE32( mime_len );
          memcpy((char*)(p+2), pic->mimetype, mime_len );
          p = (ogg_int32_t*)((char*)(p+2) + mime_len );
          p[0] = BE32( desc_len );
          memcpy((char*)(p+1), desc, desc_len );
          p = (ogg_int32_t*)((char*)(p+1) + desc_len );
          p[0] = 0;
          p[1] = 0;
          p[2] = 0;
          p[3] = 0;
          p[4] = BE32( pict_len );
          memcpy((char*)(p+5), (char*)pic + sizeof( APIC ), pict_len );

          if(( encoded = b64_encode((unsigned char*)data, size )) != NULL ) {
            vorbis_comment_add_tag( vc, "METADATA_BLOCK_PICTURE", encoded );
            free( encoded );
          }
          free( data );
        }
      }
    }

    if( vcedit_write( state, save ) < 0 ) {
      rc = errno;
    }
    break;
  }

  if( state ) {
    vcedit_clear( state );
  }
  if( file  ) {
    xio_fclose( file );
  }
  if( save  ) {
    xio_fclose( save );
  }

  if( rc != PLUGIN_OK ) {
    return rc;
  }

  // Suspend all active instances of the updated file.
  DosRequestMutexSem( mutex, SEM_INDEFINITE_WAIT );

  for( i = 0; i < instances_count; i++ )
  {
    w = instances[i];
    DosRequestMutexSem( w->mutex, SEM_INDEFINITE_WAIT );

    if( nlstricmp( w->filename, filename ) == 0 && w->file ) {
      DEBUGLOG(( "oggplay: suspend currently used file: %s\n", w->filename ));
      w->resume_pcms = w->played_pcms;
      ogg_close( w );
    } else {
      w->resume_pcms = -1;
    }
  }

  // Preserve EAs.
  eacopy( filename, savename );

  // Replace file.
  if( remove( filename ) == 0 ) {
    if( rename( savename, filename ) != 0 ) {
      rc = errno;
    }
  } else {
    rc = errno;
    remove( savename );
  }

  // Resume all suspended instances of the updated file.
  for( i = 0; i < instances_count; i++ )
  {
    w = instances[i];
    if( w->resume_pcms != -1 ) {
      DEBUGLOG(( "oggplay: resumes currently used file: %s\n", w->filename ));
      if( ogg_open( w ) == PLUGIN_OK ) {
        ov_pcm_seek( &w->vrbfile, w->resume_pcms );
      }
    }
    DosReleaseMutexSem( w->mutex );
  }

  DosReleaseMutexSem( mutex );
  return rc;
}

/* Returns information about specified track. */
ULONG DLLENTRY
decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options ) {
  return PLUGIN_NO_PLAY;
}

/* Returns information about a disc inserted to the specified drive. */
ULONG DLLENTRY
decoder_cdinfo( const char* drive, DECODER_CDINFO* info ) {
  return PLUGIN_NO_READ;
}

/* What can be played via the decoder. */
ULONG DLLENTRY
decoder_support( char* ext[], int* size )
{
  if( size ) {
    if( ext != NULL && *size >= 4 ) {
      strcpy( ext[0], "*.OGG" );
      strcpy( ext[1], "*.OGA" );
      strcpy( ext[2], "audio/ogg" );
      strcpy( ext[3], "application/ogg" );

    }
    *size = 4;
  }

  return DECODER_FILENAME | DECODER_URL | DECODER_SAVEINFO;
}

/* Returns information about plug-in. */
void DLLENTRY
plugin_query( PLUGIN_QUERYPARAM* param )
{
  param->type         = PLUGIN_DECODER;
  param->author       = "Dmitry Steklenev";
  param->desc         = "OGG Play " VER_STRING;
  param->configurable = FALSE;
}

int INIT_ATTRIBUTE
__dll_initialize( void )
{
  if( DosCreateMutexSem( NULL, &mutex, 0, FALSE ) != NO_ERROR ) {
    return 0;
  }

  return 1;
}

int TERM_ATTRIBUTE
__dll_terminate( void )
{
  free( instances );
  DosCloseMutexSem( mutex );
  return 1;
}

#if defined(__IBMC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag       )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init() == -1 ) {
      return 0UL;
    }
    return __dll_initialize();
  } else if( flag == DLL_PROCESS_DETACH ) {
    __dll_terminate();
    #ifdef __DEBUG_ALLOC__
    _dump_allocated(0);
    #endif
    _CRT_term();
  }
  return 1UL;
}
#endif

