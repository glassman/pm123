/*
 * Copyright 2009 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_WIN
#define  INCL_GPI
#define  INCL_OS2MM
#include <os2.h>
#include <os2me.h>
#include <stdlib.h>
#include <dive.h>
#include <fourcc.h>
#include <malloc.h>

#include <decoder_plug.h>
#include <visual_plug.h>
#include <plugin.h>

#include "analyzer.h"

extern analyzer_cfg  cfg;
extern VISPLUGININIT plug;

static HDIVE        dive;
static ULONG        dive_image_id;
static PBITMAPINFO2 header;
static PBYTE        bitmap;
static ULONG        bitmap_cx;
static ULONG        bitmap_cy;
static BOOL         hidden;

/* This function opens a display engine instance and
 * allocates a buffer to contain an image.
 */

BOOL
dv_open( HWND hwnd, ULONG cx, ULONG cy, PBYTE palette, ULONG palette_size )
{
  if( cfg.use_dive ) {
    // Open up DIVE
    if( DiveOpen( &dive, FALSE, 0 ) != DIVE_SUCCESS ) {
      return FALSE;
    }

    dive_image_id = 0;

    // Allocate image buffer (256-color)
    if( DiveAllocImageBuffer( dive, &dive_image_id, FOURCC_LUT8, cx, cy, 0, NULL ) != DIVE_SUCCESS ) {
      DiveClose( dive );
      dive = NULLHANDLE;
      return FALSE;
    }

    DiveSetSourcePalette( dive, 0, palette_size, palette );
    WinSetVisibleRegionNotify( hwnd, TRUE );

    // Apparently the WM_VRNENABLED message not necessarily automatically
    // follows WinSetVisibleRegionNotify call. We posts it manually.
    WinPostMsg( hwnd, WM_VRNENABLED, MPFROMLONG( TRUE ), 0 );
  }
  else
  {
    ULONG i;
    PBYTE header_palette;

    if(( header = calloc( 16 + sizeof( RGB2 ) * 256, 1 )) == NULL ) {
      return FALSE;
    }

    header->cbFix     = 16;
    header->cx        = cx;
    header->cy        = cy;
    header->cPlanes   = 1;
    header->cBitCount = 8;

    bitmap_cy = cy;
    bitmap_cx = cx;

    // All scan lines must be padded at the end, so that
    // each one begins on a ULONG boundary.
    while( bitmap_cx % 4 ) {
      bitmap_cx++;
    }

    if(( bitmap = calloc( bitmap_cx * bitmap_cy, 1 )) == NULL ) {
      free( header );
      header = NULL;
      return FALSE;
    }

    header_palette = ((PBYTE)header) + 16;
    i = palette_size * sizeof( RGB2 );

    while( i-- ) {
      *header_palette++ = *palette++;
    }

    dv_hide( hwnd );
  }

  return TRUE;
}

/* This function closes a display engine instance and
 * deallocates an image buffer.
 */

BOOL
dv_close( HWND hwnd )
{
  if( cfg.use_dive ) {
    if( dive ) {
      DiveFreeImageBuffer( dive, dive_image_id );
      DiveClose( dive );
      dive = NULLHANDLE;
      WinSetVisibleRegionNotify( hwnd, FALSE );
    }
  } else {
    if( header ) {
      free( header );
    }
    if( bitmap ) {
      free( bitmap );
    }

    header = NULL;
    bitmap = NULL;
  }

  return TRUE;
}

/* This function must be called before reading or
 * writing data in a image buffer.
 */

PBYTE
dv_begin_paint( HWND hwnd, ULONG* cx, ULONG* cy )
{
  if( cfg.use_dive ) {
    PBYTE image;
    DiveBeginImageBufferAccess( dive, dive_image_id, &image, cx, cy );
    return image;
  } else {
    *cx = bitmap_cx;
    *cy = bitmap_cy;
    return bitmap;
  }
}

/* This function must be called after reading or writing
 * data in a image buffer.
 */

BOOL
dv_end_paint( HWND hwnd )
{
  if( cfg.use_dive ) {
    DiveEndImageBufferAccess( dive, dive_image_id );
    DiveBlitImage( dive, dive_image_id, DIVE_BUFFER_SCREEN );
  } else {
    HPS hps;
    POINTL points[4] = {{ 0 }};
    int row, col;

    points[1].x = plug.cx - 1;
    points[1].y = plug.cy - 1;
    points[3].x = plug.cx;
    points[3].y = plug.cy;

    // Flip image as requered by OS/2.
    for( row = 0; row < bitmap_cy / 2; row++ ) {
      for( col = 0; col < bitmap_cx; col++ )
      {
        BYTE* upper = bitmap + row * bitmap_cx + col;
        BYTE* lower = bitmap + ( bitmap_cy - row - 1 ) * bitmap_cx + col;
        BYTE  b = *upper;

        *upper = *lower;
        *lower = b;
      }
    }

    hps = WinGetPS( hwnd );
    GpiDrawBits ( hps, bitmap, header, 4, points, ROP_SRCCOPY, 0 );
    WinReleasePS( hps );
    hidden = FALSE;
  }

  return TRUE;
}

/* Processes messages related to an image buffer.
 */

MRESULT EXPENTRY
dv_win_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg ) {
    case WM_PAINT:
      if( !cfg.use_dive && header && bitmap && !hidden )
      {
        POINTL points[4] = {{ 0 }};
        HPS hps = WinBeginPaint( hwnd, NULLHANDLE, NULL );

        points[1].x = plug.cx - 1;
        points[1].y = plug.cy - 1;
        points[3].x = plug.cx;
        points[3].y = plug.cy;

        GpiDrawBits( hps, bitmap, header, 4, points, ROP_SRCCOPY, 0 );
        WinEndPaint( hps );
        return 0;
      }
      break;

    case WM_VRNENABLED:
      if( cfg.use_dive )
      {
        HPS  hps  = WinGetPS( hwnd );
        HRGN hrgn = GpiCreateRegion( hps, 0, NULL );

        RGNRECT rgn_control;
        RECTL   rgn_rectangles[64];
        SWP     swp;
        POINTL  pos;

        if( hrgn )
        {
          WinQueryVisibleRegion( hwnd, hrgn );

          rgn_control.ircStart    = 0;
          rgn_control.crc         = sizeof( rgn_rectangles ) / sizeof( RECTL );
          rgn_control.ulDirection = RECTDIR_LFRT_TOPBOT;

          if( GpiQueryRegionRects( hps, hrgn, NULL, &rgn_control, rgn_rectangles ))
          {
            SETUP_BLITTER setup;

            WinQueryWindowPos( plug.hwnd, &swp );
            pos.x = swp.x + plug.x;
            pos.y = swp.y + plug.y;
            WinMapWindowPoints( plug.hwnd, HWND_DESKTOP, &pos, 1 );

            setup.ulStructLen       = sizeof(SETUP_BLITTER);
            setup.fInvert           = FALSE;
            setup.fccSrcColorFormat = FOURCC_LUT8;
            setup.ulSrcWidth        = plug.cx;
            setup.ulSrcHeight       = plug.cy;
            setup.ulSrcPosX         = 0;
            setup.ulSrcPosY         = 0;
            setup.ulDitherType      = 0;
            setup.fccDstColorFormat = FOURCC_SCRN;
            setup.ulDstWidth        = plug.cx;
            setup.ulDstHeight       = plug.cy;
            setup.lDstPosX          = 0;
            setup.lDstPosY          = 0;
            setup.lScreenPosX       = pos.x;
            setup.lScreenPosY       = pos.y;
            setup.ulNumDstRects     = rgn_control.crcReturned;
            setup.pVisDstRects      = rgn_rectangles;

            DiveSetupBlitter( dive, &setup );
          }

          GpiDestroyRegion( hps, hrgn );
        }

        WinReleasePS ( hps );
        return 0;
      }
      break;

    case WM_VRNDISABLED:
      if( cfg.use_dive ) {
        DiveSetupBlitter( dive, 0 );
        return 0;
      }
      break;

    case WM_DESTROY:
      if( cfg.use_dive ) {
        WinSetVisibleRegionNotify( hwnd, FALSE );
      }
      break;
  }

  return WinDefWindowProc( hwnd, msg, mp1, mp2 );
}

/* Stops diplaying an image buffer until next dv_end_paint call,
 */

BOOL
dv_hide( HWND hwnd )
{
  SWP   swp;
  RECTL rect;

  if( cfg.use_dive ) {
    DiveBlitImage( dive, 0, 0 );
  } else {
    hidden = TRUE;
  }

  WinQueryWindowPos( hwnd, &swp );

  rect.xLeft   = swp.x;
  rect.xRight  = swp.x + swp.cx;
  rect.yBottom = swp.y;
  rect.yTop    = swp.y + swp.cy;

  WinInvalidateRect( WinQueryWindow( hwnd, QW_PARENT ), &rect, TRUE );
  return TRUE;
}

