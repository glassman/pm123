/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2005-2018 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ANALYZER_H
#define ANALYZER_H

#define DLG_CONFIGURE    100
#define GB_SETTINGS      101
#define SB_FREQUENCY     102
#define ST_FREQUENCY     103
#define ST_DISPLAY       112
#define SB_PERCENT       111
#define ST_PERCENT       113
#define CB_FALLOFF       115
#define SB_FALLOFF       114
#define ST_PIXELS        116
#define ST_DEFAULT       109
#define RB_ANALYZER      104
#define RB_BARS          105
#define RB_OSCILLOSCOPE  106
#define RB_DISABLED      107
#define ST_ABOUT         110
#define ST_AUTHOR        120
#define CB_USEDIVE       121
#define PB_UNDO          122
#define PB_DEFAULT       123
#define GB_DISPLAY       124

#define SHOW_ANALYZER      0
#define SHOW_BARS          1
#define SHOW_OSCILLOSCOPE  2
#define SHOW_DISABLED      3

#define CLR_BGR_BLACK      0
#define CLR_BGR_GREY       1
#define CLR_ANA_BOTTOM     2
#define CLR_ANA_TOP       17
#define CLR_OSC_DIMMEST   18
#define CLR_OSC_BRIGHT    22
#define CLR_ANA_BARS      23

#define BARS_CY            3
#define BARS_SPACE         1

#define M_PI 3.14159265358979323846

#define WINDOW_HAMMING( n, N )  ( 0.54 - 0.46 * cos( 2 * M_PI * n / N ))
#define WINDOW_HANN( n, N )     ( 0.50 - 0.50 * cos( 2 * M_PI * n / N ))
#define WINDOW_BLACKMAN( n, N ) ( 0.42 - 0.50 * cos( 2 * M_PI * n / N )\
                                       + 0.08 * cos( 4 * M_PI * n / N ))

#define WINDOW_USED( n, N ) WINDOW_HAMMING( n, N )

typedef struct {

  ULONG update_freq;
  int   default_mode;
  int   falloff;
  int   falloff_speed;
  int   display_percent;
  int   use_dive;

} analyzer_cfg;

#ifdef __cplusplus
extern "C" {
#endif

/* This function opens a display engine instance and
 * allocates a buffer to contain an image.
 */

BOOL  dv_open( HWND hwnd, ULONG cx, ULONG cy, PBYTE palette, ULONG palette_size );

/* This function closes a display engine instance and
 * deallocates an image buffer.
 */

BOOL  dv_close( HWND hwnd );

/* This function must be called before reading or
 * writing data in a image buffer.
 */

PBYTE dv_begin_paint( HWND hwnd, ULONG* cx, ULONG* cy );

/* This function must be called after reading or writing
 * data in a image buffer.
 */

BOOL  dv_end_paint( HWND hwnd );

/* Stops diplaying an image buffer until next dv_end_paint call.
 */

BOOL  dv_hide( HWND hwnd );

/* Processes messages related to an image buffer.
 */

MRESULT EXPENTRY dv_win_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 );

#ifdef __cplusplus
}
#endif
#endif /* ANALYZER_H */
