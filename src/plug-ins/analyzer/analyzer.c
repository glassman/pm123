/*
 * Copyright 1997-2003 Samuel Audet <guardia@step.polymtl.ca>
 *                     Taneli Lepp� <rosmo@sektori.com>
 *
 * Copyright 2005-2020 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The very first PM123 visual plugin - the spectrum analyzer
 */

#define  INCL_DOS
#define  INCL_WIN
#define  INCL_GPI
#include <os2.h>
#include <stdlib.h>
#include <fourcc.h>
#include <math.h>
#include <malloc.h>

#include <utilfct.h>
#include <format.h>
#include <decoder_plug.h>
#include <visual_plug.h>
#include <plugin.h>
#include <fftw3.h>

#include "analyzer.h"
#define  TID_UPDATE          ( TID_USERMAX - 1 )
#define  WMU_UPDATE_CONTROLS ( WM_USER + 1000  )

analyzer_cfg  cfg;
VISPLUGININIT plug;

static  HAB    hab;
static  HWND   hconfigure = NULLHANDLE;
static  HWND   hanalyzer  = NULLHANDLE;
static  RGB2   palette[24];

static  float* amps;
static  int    amps_count;
static  int*   bars;
static  int    bars_count;
static  int*   scale;
static  BOOL   is_stopped;
static  BOOL   is_hidden;
static  int    numsamples;

static  fftwf_plan     plan;
static  float*         win;
static  float*         in;
static  fftwf_complex* out;
static  float          amp_out;
static  float          amp_in;

static  ULONG (DLLENTRYP decoderPlayingSamples)( FORMAT_INFO *info, char *buf, int len );
static  BOOL  (DLLENTRYP decoderPlaying)( void );

/* Removes comments.
 */

static char*
uncomment_slash( char *something )
{
  int  i = 0;
  BOOL inquotes = FALSE;

  while( something[i] )
  {
    if( something[i] == '\"' ) {
      inquotes = !inquotes;
    } else if( something[i] == '/' && something[i+1] == '/' && !inquotes ) {
      something[i] = 0;
      break;
    }
    i++;
  }

  return something;
}

/* Reads RGB colors from specified file.
 */

static BOOL
read_color( FILE* file, RGB2* color )
{
  char line[256];
  int  r,g,b;

  if( fgets( line, sizeof( line ), file )) {
    if( color ) {
      sscanf( uncomment_slash( line ), "%d,%d,%d", &r, &g, &b );
      color->bRed   = r;
      color->bGreen = g;
      color->bBlue  = b;
    }
    return TRUE;
  } else {
    return FALSE;
  }
}

/* Initializes spectrum analyzer bands.
 */

static int
init_bands( void )
{
  float step, z;
  int   i;

  numsamples = ( plug.cx * 2 ) * 100 / cfg.display_percent;

  fftwf_free( win );
  fftwf_free( in  );
  fftwf_free( out );

  win = NULL;
  in  = NULL;
  out = NULL;

  if( !( win = fftwf_malloc( sizeof( win[0] ) * numsamples )) ||
      !( in  = fftwf_malloc( sizeof( in [0] ) * numsamples )) ||
      !( out = fftwf_malloc( sizeof( out[0] ) * numsamples ))  )
  {
    return 0;
  }

  fftwf_destroy_plan( plan );
  plan = fftwf_plan_dft_r2c_1d( numsamples, in, out, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT );

  if( !plan ) {
    return 0;
  }

  for( i = 0; i < numsamples; i++ ) {
    win[i] = WINDOW_USED( i, numsamples - 1 );
  }

  amps_count = numsamples / 2 + 1;
  bars_count = plug.cx / ( BARS_CY + BARS_SPACE );

  if( plug.cx - bars_count * ( BARS_CY + BARS_SPACE ) >= BARS_CY ) {
    ++bars_count;
  }

  amps  = realloc( amps,  sizeof( amps [0] ) * amps_count );
  bars  = realloc( bars,  sizeof( bars [0] ) * plug.cx );
  scale = realloc( scale, sizeof( scale[0] ) * ( bars_count + 1 ));

  memset( amps, 0, sizeof( amps[0] ) * amps_count );
  memset( bars, 0, sizeof( bars[0] ) * plug.cx );

  step     = log( amps_count ) / bars_count;
  z        = 0;
  scale[0] = 1;

  for( i = 1; i < bars_count; i++ )
  {
    z += step;
    scale[i] = exp( z );

    if( i > 0 && scale[i] <= scale[i-1] ) {
      scale[i] = scale[i-1] + 1;
    }
  }

  scale[bars_count] = amps_count;
  return amps_count;
}

/* Free memory used by spectrum analyzer bands.
 */

static void
free_bands( void )
{
  fftwf_free( win );
  fftwf_free( in  );
  fftwf_free( out );

  fftwf_destroy_plan( plan );

  free( amps  );
  free( bars  );
  free( scale );

  win        = NULL;
  in         = NULL;
  out        = NULL;
  plan       = NULL;
  numsamples = 0;
  amps_count = 0;
  bars_count = 0;
  amps       = NULL;
  bars       = NULL;
  scale      = NULL;
}

/* Calculate analyzer bands.
 */

ULONG
do_bands( void )
{
  FORMAT_INFO bufferinfo;
  int   i, ch, len, bytespersample;
  char* samples;
  char* p;

  memset( in, 0, sizeof( in[0] ) * numsamples );
  amp_out = 0;
  amp_in  = 0;

  if( decoderPlayingSamples( &bufferinfo, NULL, 0 ) != 0 ) {
    return 0;
  }

  if( bufferinfo.bits > 32 || bufferinfo.bits % 8 != 0 ) {
    return 0;
  }

  bytespersample = bufferinfo.bits / 8;
  len = bufferinfo.channels * numsamples * bytespersample;
  samples = alloca( len );

  if( !samples || decoderPlayingSamples( &bufferinfo, samples, len ) != 0 ) {
    return 0;
  }

  for( i = 0, p = samples; i < numsamples; i++ ) {
    for( ch = 0; ch < bufferinfo.channels; ch++ ) {
      switch( bufferinfo.bits ) {
        case  8: in[i] += GETSMPL8U(p); break;
        case 16: in[i] += GETSMPL16(p); break;
        case 24: in[i] += GETSMPL24(p); break;
        case 32: in[i] += GETSMPL32(p); break;
      }
      p += bytespersample;
    }
    in[i] /= bufferinfo.channels;
    amp_in = max( amp_in, fabs( in[i] ));
  }

  // To reduce spectral leakage, the samples are multipled with a window.
  for( i = 0; i < numsamples; i++ ) {
    in[i] *= win[i];
  }

  fftwf_execute( plan );

  for( i = 0; i < numsamples / 2 + 1; i++ ) {
    amps[i] = sqrt( out[i][0]*out[i][0] + out[i][1]*out[i][1] );
    amp_out = max( amp_out, amps[i] );
  }

  // max amplitude
  return 0xFFFFFFFFUL >> ( 32 - bufferinfo.bits );
}

/* Draws grid.
 */

static void
draw_grid( PBYTE image, ULONG image_cx, ULONG image_cy )
{
  int x, y;

  memset( image, 0, image_cx * image_cy );

  for( y = 0; y < plug.cy; y += 2 ) {
    for( x = 0; x < plug.cx; x += 2 ) {
      image[( y * image_cx ) + x ] = 1;
    }
  }
}

/* Clears the analyzer window.
 */

static void
clear( void )
{
  ULONG image_cx, image_cy;

  if( !is_hidden ) {
    PBYTE image = dv_begin_paint( hanalyzer, &image_cx, &image_cy );
    draw_grid( image, image_cx, image_cy );
    dv_end_paint( hanalyzer );
  }

  memset( bars, 0, plug.cx * sizeof( *bars ));
}

/* Draws oscilloscope.
 */

static void
draw_oscilloscope( PBYTE image, ULONG image_cx, ULONG image_cy )
{
  FORMAT_INFO bufferinfo;
  BYTE* samples;
  int   len, x, y, ch, scale, bytespersample;
  ULONG max;
  BYTE  color;
  int   half_cy = plug.cy / 2 - ( plug.cy % 2 ? 0 : 1 );
  int   prev = half_cy;

  decoderPlayingSamples( &bufferinfo, NULL, 0 );

  if( bufferinfo.bits > 32 || bufferinfo.bits % 8 != 0 ) {
    return;
  }

  scale = bufferinfo.samplerate / 8000;
  if( !scale ) {
    scale = 1;
  }

  bytespersample = bufferinfo.bits / 8;
  len = bufferinfo.channels * plug.cx * bytespersample * scale;
  max = 0x80000000UL >> ( 32 - bufferinfo.bits );

  samples = alloca( len );
  decoderPlayingSamples( &bufferinfo, samples, len );

  for( x = 0; x < plug.cx; x++ )
  {
    float z = 0;
    char* p = samples + x * scale * bufferinfo.channels * bytespersample;

    for( ch = 0; ch < bufferinfo.channels; ch++ ) {
      switch( bufferinfo.bits ) {
        case  8: z += GETSMPL8U(p); break;
        case 16: z += GETSMPL16(p); break;
        case 24: z += GETSMPL24(p); break;
        case 32: z += GETSMPL32(p); break;
      }
      p += bytespersample;
    }

    z /= bufferinfo.channels;
    y = half_cy + half_cy * z / max;
    color = ( CLR_OSC_BRIGHT - CLR_OSC_DIMMEST ) * abs( y - half_cy ) / half_cy + CLR_OSC_DIMMEST;
    image[ y * image_cx + x ] = color;

    // Connect the previous graph point to the current point.
    if( x && prev != y )
    {
      int m;
      if( prev > y ) {
        for( m = prev - 1; m > y; m-- ) {
          color = ( CLR_OSC_BRIGHT - CLR_OSC_DIMMEST ) * abs( m - half_cy ) / half_cy + CLR_OSC_DIMMEST;
          image[ m * image_cx + x - 1 ] = color;
        }
      } else {
        for( m = prev + 1; m < y; m++ ) {
          color = ( CLR_OSC_BRIGHT - CLR_OSC_DIMMEST ) * abs( m - half_cy ) / half_cy + CLR_OSC_DIMMEST;
          image[ m * image_cx + x - 1 ] = color;
        }
      }
    }
    prev = y;
  }
}

/* Draws analyzer.
 */

static void
draw_analyzer( PBYTE image, ULONG image_cx, ULONG image_cy )
{
  ULONG max;
  int   x, y, z;
  int   color;

  if(( max = do_bands()) == 0 ) {
    return;
  }

  if( amp_out )
  {
    float scale = amp_in * 2 / amp_out;

    for( x = 1; x < plug.cx + 1; x++ ) {
      amps[x] *= scale;
    }
  }

  for( x = 0; x < plug.cx; x++ )
  {
    z = plug.cy * sqrt( amps[ x + 1 ] / max );

    if( cfg.falloff && bars[x] - cfg.falloff_speed > z  ) {
      bars[x] -= cfg.falloff_speed;
    } else {
      bars[x] = z;
    }

    for( y = 0; y < bars[x]; y++ )
    {
      color = ( CLR_ANA_TOP - CLR_ANA_BOTTOM ) * y / ( plug.cy - bars[x] + 1 ) + CLR_ANA_BOTTOM;

      if( color > CLR_ANA_TOP ) {
        color = CLR_ANA_TOP;
      }
      image[( plug.cy - y - 1 ) * image_cx + x ] = (BYTE)color;
    }
  }
}

/* Draws analyzer bars.
 */

static void
draw_bars( PBYTE image, ULONG image_cx, ULONG image_cy )
{
  ULONG max;
  int   x, y, i, z;
  BYTE  color;

  if(( max = do_bands()) == 0 ) {
    return;
  }

  for( x = 0; x < bars_count; x++ )
  {
    float a = 0;
    for( i = scale[x]; i < scale[ x + 1 ]; i++ ) {
      a += amps[i];
    }
    amps[ x + 1 ] = a;
    amp_out = max( amp_out, a );
  }

  if( amp_out )
  {
    float scale = amp_in * 2 / amp_out;
    for( x = 0; x < bars_count; x++ ) {
      amps[ x + 1 ] *= scale;
    }
  }

  for( x = 0; x < bars_count; x++ ) {
    z = plug.cy * sqrt( amps[ x + 1 ] / max );

    if( cfg.falloff && bars[x] - cfg.falloff_speed > z  ) {
      bars[x] -= cfg.falloff_speed;
    } else {
      bars[x] = z;
    }

    for( y = 0; y < bars[x]; y++ ) {
      color = ( CLR_ANA_TOP - CLR_ANA_BOTTOM + 1 ) * y / plug.cy + CLR_ANA_BOTTOM;
      for( i = 0; i < BARS_CY; i++ ) {
        image[( plug.cy - y - 1 ) * image_cx + ( BARS_CY + BARS_SPACE ) * x + i ] = color;
      }
    }
  }
}

/* Returns information about plug-in.
 */

void DLLENTRY
plugin_query( PPLUGIN_QUERYPARAM query )
{
  query->type         = PLUGIN_VISUAL;
  query->author       = "Samuel Audet, Dmitry Steklenev ";
  query->desc         = "Spectrum Analyzer " VER_STRING;
  query->configurable = 1;
}

/* Processes messages of the configuration dialog.
 */

static MRESULT EXPENTRY
cfg_dlg_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  static analyzer_cfg old_cfg;
  static BOOL update = FALSE;
  LONG   value;

  switch( msg ) {
    case WM_INITDLG:
      do_warpsans( hwnd );
      hconfigure = hwnd;
      old_cfg = cfg;
      // continue to WMU_UPDATE_CONTROLS...

    case WMU_UPDATE_CONTROLS:
      update = TRUE;
      WinCheckButton( hwnd, RB_ANALYZER + cfg.default_mode, TRUE );
      WinCheckButton( hwnd, CB_FALLOFF, cfg.falloff  );
      WinCheckButton( hwnd, CB_USEDIVE, cfg.use_dive );

      WinSendDlgItemMsg( hwnd, SB_PERCENT,   SPBM_SETLIMITS,
                         MPFROMLONG( 100 ), MPFROMLONG( 1 ));
      WinSendDlgItemMsg( hwnd, SB_PERCENT,   SPBM_SETCURRENTVALUE,
                         MPFROMLONG( cfg.display_percent ), 0 );
      WinSendDlgItemMsg( hwnd, SB_FREQUENCY, SPBM_SETLIMITS,
                         MPFROMLONG( 200 ), MPFROMLONG( 1 ));
      WinSendDlgItemMsg( hwnd, SB_FREQUENCY, SPBM_SETCURRENTVALUE,
                         MPFROMLONG( cfg.update_freq ), 0 );
      WinSendDlgItemMsg( hwnd, SB_FALLOFF,   SPBM_SETLIMITS,
                         MPFROMLONG( plug.cy ), MPFROMLONG( 1 ));
      WinSendDlgItemMsg( hwnd, SB_FALLOFF,   SPBM_SETCURRENTVALUE,
                         MPFROMLONG( cfg.falloff_speed ), 0 );
      update = FALSE;
      return 0;

    case WM_CONTROL:
      if( !update ) {
        switch( SHORT1FROMMP(mp1)) {
          case RB_ANALYZER:
          case RB_BARS:
          case RB_OSCILLOSCOPE:
          case RB_DISABLED:
            if( SHORT2FROMMP(mp1) == BN_CLICKED ) {
              cfg.default_mode = LONGFROMMR( WinSendDlgItemMsg( hwnd, RB_ANALYZER,
                                                                BM_QUERYCHECKINDEX, 0, 0 ));
              if( cfg.default_mode != SHOW_DISABLED ) {
                clear();
              }
            }
            break;

          case SB_FREQUENCY:
            if( SHORT2FROMMP(mp1) == SPBN_CHANGE ) {
              WinSendDlgItemMsg( hwnd, SB_FREQUENCY, SPBM_QUERYVALUE,
                                 MPFROMP( &value ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
              cfg.update_freq = limit2( value, 1, 200 );
              WinStartTimer( hab, hanalyzer, TID_UPDATE, 1000 / cfg.update_freq );
            }
            break;

          case CB_FALLOFF:
            if( SHORT2FROMMP(mp1) == BN_CLICKED ) {
              cfg.falloff = WinQueryButtonCheckstate( hwnd, CB_FALLOFF );
            }
            break;

          case SB_FALLOFF:
            if( SHORT2FROMMP(mp1) == SPBN_CHANGE ) {
              WinSendDlgItemMsg( hwnd, SB_FALLOFF, SPBM_QUERYVALUE,
                                 MPFROMP( &value ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
              cfg.falloff_speed = limit2( value, 1, plug.cy );
            }
            break;

          case SB_PERCENT:
            if( SHORT2FROMMP(mp1) == SPBN_CHANGE ) {
               WinSendDlgItemMsg( hwnd, SB_PERCENT, SPBM_QUERYVALUE,
                                  MPFROMP( &value ), MPFROM2SHORT( 0, SPBQ_DONOTUPDATE ));
               cfg.display_percent = limit2( value, 1, 100 );
               init_bands();
            }
            break;

          case CB_USEDIVE:
            if( SHORT2FROMMP(mp1) == BN_CLICKED ) {
              value = WinQueryButtonCheckstate( hwnd, CB_USEDIVE );
              if( value != cfg.use_dive ) {
                dv_close( hanalyzer );
                cfg.use_dive = value;
                dv_open( hanalyzer, plug.cx, plug.cy, (PBYTE)&palette, sizeof( palette ) / sizeof( palette[0] ));
              }
            }
            break;

        }
      }
      break;

    case WM_COMMAND:
      switch( SHORT1FROMMP(mp1)) {
        case PB_UNDO:
          dv_close( hanalyzer );
          cfg = old_cfg;
          WinSendMsg( hwnd, WMU_UPDATE_CONTROLS, 0, 0 );
          break;
        case PB_DEFAULT:
          dv_close( hanalyzer );
          cfg.default_mode    = SHOW_BARS;
          cfg.update_freq     = 32;
          cfg.falloff         = 1;
          cfg.falloff_speed   = 1;
          cfg.display_percent = 80;
          cfg.use_dive        = 0;
          WinSendMsg( hwnd, WMU_UPDATE_CONTROLS, 0, 0 );
          break;
        default:
          return 0;
      }

      dv_open( hanalyzer, plug.cx, plug.cy, (PBYTE)&palette,
               sizeof( palette ) / sizeof( palette[0] ));

      if( cfg.default_mode != SHOW_DISABLED ) {
        clear();
      }

      WinStartTimer( hab, hanalyzer, TID_UPDATE, 1000 / cfg.update_freq );
      init_bands();
      return 0;

    case WM_DESTROY:
      hconfigure = NULLHANDLE;
      break;
  }
  return WinDefDlgProc( hwnd, msg, mp1, mp2 );
}

/* Configure plug-in.
 */

void DLLENTRY
plugin_configure( HWND hwnd, HMODULE module ) {
  WinDlgBox( HWND_DESKTOP, hwnd, cfg_dlg_proc, module, DLG_CONFIGURE, NULL );
}

/* Processes messages of the analyzer window.
 */

static MRESULT EXPENTRY
plg_win_proc( HWND hwnd, ULONG msg, MPARAM mp1, MPARAM mp2 )
{
  switch( msg )
  {
    case DM_DRAGOVER:
    case DM_DROP:
    case 0x041f:
    case 0x041e:
    case WM_CONTEXTMENU:
    case WM_BUTTON2MOTIONSTART:
    case WM_BUTTON1MOTIONSTART:
    case WM_VSCROLL:
      return WinSendMsg( plug.hwnd, msg, mp1, mp2 );

    case WM_BUTTON1DBLCLK:
    case WM_BUTTON1CLICK:
      if( ++cfg.default_mode > SHOW_DISABLED ) {
        cfg.default_mode = SHOW_ANALYZER;
      }
      clear();
      break;

    case WM_TIMER:
    {
      ULONG image_cx, image_cy;
      PBYTE image;

      if( cfg.default_mode == SHOW_DISABLED ) {
        if( !is_hidden ) {
          dv_hide( hwnd );
          is_hidden = TRUE;
        }
      }
      else if( decoderPlaying() == 1 )
      {
        FORMAT_INFO info;

        if( decoderPlayingSamples( &info, NULL, 0 ) == 0 &&
            info.format == WAVE_FORMAT_PCM && info.bits <= 32 && info.bits % 8 == 0 )
        {
          image = dv_begin_paint( hwnd, &image_cx, &image_cy );
          draw_grid( image, image_cx, image_cy );

          switch( cfg.default_mode ) {
            case SHOW_ANALYZER:
              draw_analyzer( image, image_cx, image_cy );
              break;
            case SHOW_BARS:
              draw_bars( image, image_cx, image_cy );
              break;
            case SHOW_OSCILLOSCOPE:
              draw_oscilloscope( image, image_cx, image_cy );
              break;
          }
          dv_end_paint( hwnd );
          is_stopped = FALSE;
          is_hidden  = FALSE;
        } else if( !is_hidden ) {
          dv_hide( hwnd );
          is_hidden  = TRUE;
        }
      }
      else if( !is_stopped && !is_hidden )
      {
        image = dv_begin_paint( hwnd, &image_cx, &image_cy );
        draw_grid( image, image_cx, image_cy );
        dv_end_paint( hwnd );
        is_stopped = TRUE;
        is_hidden  = FALSE;
      }
      break;
    }

    default:
      return dv_win_proc( hwnd, msg, mp1, mp2 );
  }
  return 0;
}

/* Initializes plug-in.
 */

HWND DLLENTRY
vis_init( PVISPLUGININIT init )
{
  FILE* dat;
  HINI  hini;
  int   i;

  memcpy( &plug, init, sizeof( VISPLUGININIT ));

  cfg.update_freq     = 32;
  cfg.default_mode    = SHOW_BARS;
  cfg.falloff         = 1;
  cfg.falloff_speed   = 1;
  cfg.display_percent = 80;
  cfg.use_dive        = FALSE;

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    load_ini_value( hini, cfg.update_freq  );
    load_ini_value( hini, cfg.default_mode );
    load_ini_value( hini, cfg.falloff );
    load_ini_value( hini, cfg.falloff_speed );
    load_ini_value( hini, cfg.display_percent );
    load_ini_value( hini, cfg.use_dive );

    close_ini( hini );
  }

  // First get the routines
  decoderPlayingSamples = init->procs->output_playing_samples;
  decoderPlaying        = init->procs->decoder_playing;

  init_bands();
  is_stopped = FALSE;
  is_hidden  = FALSE;

  // Initialize PM
  hab = WinQueryAnchorBlock( plug.hwnd );
  WinRegisterClass( hab, "Spectrum Analyzer", plg_win_proc, CS_SIZEREDRAW | CS_MOVENOTIFY, 0 );

  hanalyzer = WinCreateWindow( plug.hwnd,
                               "Spectrum Analyzer",
                               "Spectrum Analyzer",
                               WS_VISIBLE,
                               plug.x,
                               plug.y,
                               plug.cx,
                               plug.cy,
                               plug.hwnd,
                               HWND_TOP,
                               plug.id,
                               NULL,
                               NULL  );

  memset( palette, 0, sizeof( palette ));

  if( plug.param && *plug.param && ( dat = fopen( plug.param, "r" )))
  {
    read_color( dat, &palette[CLR_BGR_BLACK] );
    read_color( dat, &palette[CLR_BGR_GREY ] );

    for( i = CLR_ANA_TOP; i >= CLR_ANA_BOTTOM; i-- ) {
      read_color( dat, &palette[i] );
    }

    for( i = CLR_OSC_DIMMEST; i <= CLR_OSC_BRIGHT; i++ ) {
      read_color( dat, &palette[i] );
    }

    if( !read_color( dat, &palette[CLR_ANA_BARS] )) {
      palette[CLR_ANA_BARS] = palette[CLR_ANA_TOP];
    }

    fclose( dat );
  }
  else
  {
    palette[ 2].bRed = 255; palette[ 2].bGreen =   0;
    palette[ 3].bRed = 225; palette[ 3].bGreen =  30;
    palette[ 4].bRed = 195; palette[ 4].bGreen =  85;
    palette[ 5].bRed = 165; palette[ 5].bGreen = 115;
    palette[ 6].bRed = 135; palette[ 6].bGreen = 135;
    palette[ 7].bRed = 115; palette[ 7].bGreen = 165;
    palette[ 8].bRed =  85; palette[ 8].bGreen = 195;
    palette[ 9].bRed =  30; palette[ 9].bGreen = 225;
    palette[10].bRed =   0; palette[10].bGreen = 255;
    palette[11].bRed =   0; palette[11].bGreen = 255;
    palette[12].bRed =   0; palette[12].bGreen = 255;
    palette[13].bRed =   0; palette[13].bGreen = 255;
    palette[14].bRed =   0; palette[14].bGreen = 255;
    palette[15].bRed =   0; palette[15].bGreen = 255;
    palette[16].bRed =   0; palette[16].bGreen = 255;
    palette[17].bRed =   0; palette[17].bGreen = 255;

    palette[18].bGreen = 180;
    palette[19].bGreen = 195;
    palette[20].bGreen = 210;
    palette[21].bGreen = 225;
    palette[22].bGreen = 240;

    palette[CLR_BGR_GREY].bGreen =  90;
    palette[CLR_ANA_BARS].bGreen = 255;
  }

  if( !dv_open( hanalyzer, plug.cx, plug.cy,
                          (PBYTE)&palette, sizeof( palette ) / sizeof( palette[0] )))
  {
    free_bands();
    WinDestroyWindow( hanalyzer );
    hanalyzer = NULLHANDLE;
  }

  // Apparently the WM_VRNENABLED message not necessarily automatically
  // follows WinSetVisibleRegionNotify call. We posts it manually.
  WinPostMsg( hanalyzer, WM_VRNENABLED, MPFROMLONG( TRUE ), 0 );
  WinStartTimer( hab, hanalyzer, TID_UPDATE, 1000 / cfg.update_freq );
  return hanalyzer;
}

/* Terminates plug-in.
 */

int DLLENTRY
plugin_deinit( void )
{
  HINI hini;

  if(( hini = open_module_ini()) != NULLHANDLE )
  {
    save_ini_value( hini, cfg.update_freq );
    save_ini_value( hini, cfg.default_mode );
    save_ini_value( hini, cfg.falloff );
    save_ini_value( hini, cfg.falloff_speed );
    save_ini_value( hini, cfg.display_percent );
    save_ini_value( hini, cfg.use_dive );

    close_ini( hini );
  }

  if( hconfigure != NULLHANDLE ) {
    WinDismissDlg( hconfigure, 0 );
  }

  WinStopTimer( hab, hanalyzer, TID_UPDATE );
  free_bands();
  fftwf_cleanup();
  dv_close( hanalyzer );
  WinDestroyWindow( hanalyzer );

  hanalyzer = NULLHANDLE;
  return PLUGIN_OK;
}

#if defined(__IBMC__) && defined(__DEBUG_ALLOC__)
unsigned long _System _DLL_InitTerm( unsigned long modhandle,
                                     unsigned long flag )
{
  if( flag == DLL_PROCESS_ATTACH ) {
    if( _CRT_init()) == -1 ) {
      return 0UL;
    }
  } else if( flag == DLL_PROCESS_DETACH ) {
    _dump_allocated(0);
    _CRT_term();
  }
  return 1UL;
}
#endif
