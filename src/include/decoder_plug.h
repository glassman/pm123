#ifndef PM123_DECODER_PLUG_H
#define PM123_DECODER_PLUG_H

#include "format.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(4)

int  DLLENTRY decoder_init  ( void** w );
BOOL DLLENTRY decoder_uninit( void*  w );

/* return value:                                               */
/* PLUGIN_OK          -> ok.                                   */
/* PLUGIN_UNSUPPORTED -> command unsupported.                  */
/*                                                             */
/* DECODER_PLAY can return also:                               */
/* PLUGIN_GO_ALREADY  -> already playing.                      */
/* PLUGIN_GO_ERROR    -> error, decoder killed and restarted.  */
/*                                                             */
/* DECODER_STOP can return also:                               */
/* PLUGIN_GO_ALREADY  -> already stopped.                      */
/* PLUGIN_GO_ERROR    -> error, decoder killed and stopped.    */

#define DECODER_PLAY      1
#define DECODER_STOP      2
#define DECODER_FFWD      3
#define DECODER_REW       4
#define DECODER_JUMPTO    5
#define DECODER_SETUP     6
#define DECODER_EQ        7 /* obsolete, don't used anymore */
#define DECODER_BUFFER    8 /* obsolete, don't used anymore */
#define DECODER_SAVEDATA  9

typedef struct _DECODER_PARAMS
{
   int size;

   /* --- DECODER_PLAY, STOP */

   char* filename;
   void* unused1;     /* obsolete, must be NULL */
   char* drive;       /* for CD ie.: "X:" */
   int   track;


   int   start;       /* specifies start and end time in milliseconds */
   int   end;         /* for tracks in cue sheet. */

   void* unused2;     /* obsolete, must be NULL */

   /* --- DECODER_REW, FFWD and JUMPTO */

   int   jumpto;      /* absolute positioning in milliseconds */
   int   ffwd;        /* 1 = start ffwd, 0 = end ffwd */
   int   rew;         /* 1 = start rew,  0 = end rew  */

   /* --- DECODER_SETUP */

   /* specify a function which the decoder should use for output */
   int  (DLLENTRYP output_play_samples)( void* a, FORMAT_INFO* format, char* buf, int len, int posmarker );
   void* a;           /* only to be used with the precedent function */
   int   audio_buffersize;

   char* proxyurl;    /* NULL = none */
   char* httpauth;    /* NULL = none */

   /* error message function the decoder should use */
   void (DLLENTRYP error_display)( char* );

   /* info message function the decoder should use */
   /* this information is always displayed to the user right away */
   void (DLLENTRYP info_display)( char* );

   void* unused3;     /* obsolete, must be NULL */
   HWND  hwnd;        /* commodity for PM interface, decoder must send a few
                         messages to this handle */

   /* values used for streaming inputs by the decoder */
   int   buffersize;  /* read ahead buffer in bytes, 0 = disabled */
   int   bufferwait;  /* block the first read until the buffer is filled */

   char* metadata_buffer; /* the decoder will put streaming metadata in this  */
   int   metadata_size;   /* buffer before posting WM_METADATA */

   int   unused4;     /* obsolete, must be 0 */
   int   unused5;     /* obsolete, must be 0 */
   int   unused6;     /* obsolete, must be 0 */

   /* --- DECODER_SAVEDATA */

   char*  save_filename;

} DECODER_PARAMS;

ULONG DLLENTRY decoder_command( void* w, ULONG msg, DECODER_PARAMS* params );

#define DECODER_STOPPED  0
#define DECODER_PLAYING  1
#define DECODER_STARTING 2
#define DECODER_PAUSED   3
#define DECODER_ERROR    200

ULONG DLLENTRY decoder_status( void* w );

/* WARNING!! this _can_ change in time!!! returns stream length in ms */
/* the decoder should keep in memory a last valid length so the call  */
/* remains valid even if decoder_status() == DECODER_STOPPED          */
ULONG DLLENTRY decoder_length( void* w );

#define DECODER_MODE_STEREO         0
#define DECODER_MODE_JOINT_STEREO   1
#define DECODER_MODE_DUAL_CHANNEL   2
#define DECODER_MODE_SINGLE_CHANNEL 3

#define modes(i) ( i == 0 ? "Stereo"         : \
                 ( i == 1 ? "Joint-Stereo"   : \
                 ( i == 2 ? "Dual-Channel"   : \
                 ( i == 3 ? "Single-Channel" : "" ))))

/* See haveinfo field of the DECODER_INFO structure. */
#define DECODER_HAVE_TITLE      0x00000001UL
#define DECODER_HAVE_ARTIST     0x00000002UL
#define DECODER_HAVE_ALBUM      0x00000004UL
#define DECODER_HAVE_YEAR       0x00000008UL
#define DECODER_HAVE_COMMENT    0x00000010UL
#define DECODER_HAVE_GENRE      0x00000020UL
#define DECODER_HAVE_TRACK      0x00000040UL
#define DECODER_HAVE_COPYRIGHT  0x00000080UL
#define DECODER_HAVE_TRACK_GAIN 0x00000100UL
#define DECODER_HAVE_TRACK_PEAK 0x00000200UL
#define DECODER_HAVE_ALBUM_GAIN 0x00000400UL
#define DECODER_HAVE_ALBUM_PEAK 0x00000800UL
#define DECODER_HAVE_DISC       0x00001000UL
#define DECODER_HAVE_PICS       0x00002000UL

/* You can specify what type of picture is supported. */
#define DECODER_PICS_ANYTYPE    0x00000000UL
#define DECODER_PICS_COVERFRONT 0x80000000UL
#define DECODER_PICS_COVERBACK  0x40000000UL

/* Types of the attached picture. */

#define PIC_OTHER         0x00   /* Other. */
#define PIC_OTHERICON     0x01   /* 32x32 pixels 'file icon' (PNG only). */
#define PIC_ICON          0x02   /* Other file icon. */
#define PIC_COVERFRONT    0x03   /* Cover (front). */
#define PIC_COVERBACK     0x04   /* Cover (back). */
#define PIC_LEAFLET       0x05   /* Leaflet page. */
#define PIC_MEDIA         0x06   /* Media (e.g. label side of CD). */
#define PIC_LEADARTIST    0x07   /* Lead artist/lead performer/soloist. */
#define PIC_ARTIST        0x08   /* Artist/performer. */
#define PIC_CONDUCTOR     0x09   /* Conductor. */
#define PIC_BAND          0x0A   /* Band/Orchestra. */
#define PIC_COMPOSER      0x0B   /* Composer. */
#define PIC_LYRICIST      0x0C   /* Lyricist/text writer. */
#define PIC_LOCATION      0x0D   /* Recording Location. */
#define PIC_RECORDING     0x0E   /* During recording. */
#define PIC_PERFORMANCE   0x0F   /* During performance. */
#define PIC_CAPTURE       0x10   /* Movie/video screen capture. */
#define PIC_FISH          0x11   /* A bright coloured fish. */
#define PIC_ILLUSTRATION  0x12   /* Illustration. */
#define PIC_BANDLOGO      0x13   /* Band/artist logotype. */
#define PIC_PUBLISHERLOGO 0x14   /* Publisher/Studio logotype. */

typedef struct _APIC {

   int   size;            /* size of APIC structure */
   int   type;            /* Picture type. */
   char  mimetype[64];    /* MIME type. */
   char  desc[128];       /* Description. */

} APIC;

/* Determines that the data structure pointed by pInfo contains the specified field. */
#define HAVE_FIELD( pInfo, field ) (pInfo->size >= (int)((int)(&pInfo->field) - (int)(pInfo) + sizeof(pInfo->field)))

/* Obsolete definitions. Use instead HAVE_FIELD macro. */

#define INFO_SIZE_1  976  /* size of the DECODER_INFO structure prior PM123 1.32 */
#define INFO_SIZE_2 1264  /* size of the DECODER_INFO structure since PM123 1.32 */

/* NOTE: the information returned is only based on the FIRST header */
typedef struct _DECODER_INFO
{
   int   size;            /* size of DECODER_INFO structure */

   FORMAT_INFO format;    /* stream format after decoding */

   int   songlength;      /* in milliseconds, smaller than 0 -> unknown */
   int   junklength;      /* bytes of junk before stream start, if < 0 -> unknown */

   /* mpeg stuff */
   int   mpeg;            /* 25 = MPEG 2.5, 10 = MPEG 1.0, 20 = MPEG 2.0, 0 = not an MPEG */
   int   layer;           /* 0 = unknown */
   int   mode;            /* use it on modes(i) */
   int   modext;          /* didn't check what this one does */
   int   bpf;             /* bytes in the mpeg frame including header */
   int   bitrate;         /* in kbit/s */
   int   extention;       /* didn't check what this one does */

   /* not only track stuff */
   int   start;
   int   end;

   /* obsolete stuff */
   int   unused1;         /* obsolete, must be 0 */
   int   unused2;         /* obsolete, must be 0 */
   int   unused3;         /* obsolete, must be 0 */

   /* general technical information string */
   char  tech_info[128];

   /* meta information */
   char  title    [128];
   char  artist   [128];
   char  album    [128];
   char  year     [128];
   char  comment  [128];
   char  genre    [128];

   /* added since PM123 1.32 */
   char  track    [ 64];
   char  disc     [ 64];
   char  copyright[128];
   int   codepage;        /* Code page of the meta info. Must be 0 if the
                             code page is unknown. Don't place here any another
                             value if it is not provided by meta info. */
   int   haveinfo;        /* This flags define what of fields of the block of the
                             meta information are supported by the decoder. Can
                             be 0 if the decoder supports all fields. */
   int   saveinfo;        /* Must be 1 if the decoder can update meta info of
                             this file. Otherwise, must be 0. */
   int   unused4;         /* obsolete, must be 0 */

   float track_gain;      /* Defines Replay Gain values as specified at */
   float track_peak;      /* http://www.replaygain.org/ */
   float album_gain;
   float album_peak;

   /* added since PM123 1.38 */

   int   pics_count;      /* At returns contain the count of attached pictures. */

   /* Functions the decoder should use to allocate memory for */
   /* attached pictures.  */
   void* (DLLENTRYP memalloc)( int size );
   void  (DLLENTRYP memfree)( void* ptr );

   APIC** pics;           /* Pointer to array of the pointers to the
                             loaded attached pictures. */

   /* added since PM123 1.41 */

   long long filesize;    /* Size of the file. */

} DECODER_INFO;

#define DECODER_LOADPICS  0x0001 /* Decoder must load attached pictures. */
#define DECODER_EXTPICS   0x8000 /* Must be ignored by decoder. */

/* returns
      PLUGIN_OK      = everything's perfect, structure is set,
      PLUGIN_NO_READ = error reading file (too small?),
      PLUGIN_NO_PLAY = decoder can't play that,
      other values   = errno, check xio_strerror() for string. */
ULONG DLLENTRY decoder_fileinfo( char* filename, DECODER_INFO* info, int options );
ULONG DLLENTRY decoder_trackinfo( char* drive, int track, DECODER_INFO* info, int options );

#define DECODER_SAVEPICS  0x0001 /* Decoder must save attached pictures. */

/* returns
      PLUGIN_OK      = everything's perfect, structure is saved to filename,
      other values   = errno, check xio_strerror() for string. */
ULONG DLLENTRY decoder_saveinfo( char* filename, DECODER_INFO* info, int options );

typedef struct _DECODER_CDINFO
{
   int sectors;
   int firsttrack;
   int lasttrack;

} DECODER_CDINFO;

/* returns
      PLUGIN_OK      = everything's perfect, structure is set,
      PLUGIN_NO_READ = error reading required info,
      other values   = errno, check xio_strerror() for string. */
ULONG DLLENTRY decoder_cdinfo( const char* drive, DECODER_CDINFO* info );

/* returns ORed values */
#define DECODER_FILENAME  0x0001 /* Decoder can play a regular file. */
#define DECODER_URL       0x0002 /* Decoder can play a file located on an internet server. */
#define DECODER_TRACK     0x0004 /* Decoder can play a CD track. */
#define DECODER_OTHER     0x0008 /* Decoder can play something. */
#define DECODER_STREAM    0x0010 /* Decoder can play an internet stream. */
#define DECODER_SAVEINFO  0x8000 /* Decoder can save a meta info. */
/* size is i/o and is the size of the array.
   each ext should not be bigger than 8bytes */
ULONG DLLENTRY decoder_support( char* fileext[], int* size );

#pragma pack()

#ifdef __cplusplus
}
#endif
#endif /* PM123_DECODER_PLUG_H */

