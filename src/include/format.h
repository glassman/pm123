#ifndef  PM123_FORMAT_H
#define  PM123_FORMAT_H

#include "config.h"
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#define WM_PLAYSTOP         (WM_USER +  69)
#define WM_PLAYERROR        (WM_USER + 100)
#define WM_SEEKSTOP         (WM_USER + 666)
#define WM_METADATA         (WM_USER +  42)
#define WM_CHANGEBR         (WM_USER +  43)
#define WM_OUTPUT_OUTOFDATA (WM_USER + 667)
#define WM_PLUGIN_CONTROL   (WM_USER + 668) /* Plugin notify message */

/*
 * WM_PLUGIN_CONTROL
 *   LONGFROMMP(mp1) = notify code
 *   LONGFROMMP(mp2) = additional information
 *
 * Notify codes:
 */

#define PN_TEXTCHANGED  1   /* Display text changed */

#pragma pack(4)

#define WAVE_FORMAT_PCM 0x0001
#define WAVE_FORMAT_DSD 0x0301

typedef struct _FORMAT_INFO
{
  ULONG size;
  ULONG samplerate;
  ULONG channels;
  ULONG bits;
  ULONG format; /* WAVE_FORMAT_PCM = 1 */

} FORMAT_INFO;

#pragma pack()

/* Definitions that help convert sound samples of different bit depth. */

#define GETSMPL8U(p) (int32_t)(*((uint8_t*)(p)) - 128)
#define GETSMPL16(p) (int32_t)(*((int16_t*)(p)))
#define GETSMPL24(p) (int32_t)(*((int16_t*)((uint8_t*)(p) + 1)) << 8 | *((uint8_t*)(p)))
#define GETSMPL32(p) (int32_t)(*((int32_t*)(p)))

#define PUTSMPL8U(p,s) *((uint8_t*)(p)) = (int32_t)(s) + 128
#define PUTSMPL16(p,s) *((int16_t*)(p)) = (int16_t)(s)
#define PUTSMPL24(p,s) *((uint8_t*)(p)) = (int32_t)(s) & 0xFFUL; \
                       *((int16_t*)((uint8_t*)p+1)) = (uint16_t)(((uint32_t)(s) & 0xFFFF00UL) >> 8)
#define PUTSMPL32(p,s) *((int32_t*)(p)) = (int32_t)(s)

#define SMPL8U_MAX 128.0
#define SMPL16_MAX 32768.0
#define SMPL24_MAX 8388608.0
#define SMPL32_MAX 2147483648.0

#ifdef __cplusplus
}
#endif
#endif /* PM123_FORMAT_H */
