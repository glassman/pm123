#ifndef PM123_VISUAL_PLUG_H
#define PM123_VISUAL_PLUG_H

#include "format.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(4)

/* see decoder_plug.h and output_plug.h for more information
   on some of these functions */
typedef struct _PLUGIN_PROCS
{
  ULONG (DLLENTRYP output_playing_samples)( FORMAT_INFO* info, char* buf, int len );
  BOOL  (DLLENTRYP decoder_playing)( void );
  ULONG (DLLENTRYP output_playing_pos)( void );
  ULONG (DLLENTRYP decoder_status)( void );
  ULONG (DLLENTRYP decoder_command)( ULONG msg, DECODER_PARAMS* params );
  /* name is the DLL filename of the decoder that can play that file */
  ULONG (DLLENTRYP decoder_fileinfo)( const char* filename, DECODER_INFO* info, char* name, int options );

  void* unused1; /* obsolete */
  void* unused2; /* obsolete */

  int   (DLLENTRYP pm123_getstring)( int index, int subindex, int bufsize, char* buf );
  void  (DLLENTRYP pm123_control)( int index, void* param );

  /* name is the DLL filename of the decoder that can play that track */
  ULONG (DLLENTRYP decoder_trackinfo)( char* drive, int track, DECODER_INFO* info, char* name, int options );
  ULONG (DLLENTRYP decoder_cdinfo)( char* drive, DECODER_CDINFO* info );
  ULONG (DLLENTRYP decoder_length)( void );

} PLUGIN_PROCS, *PPLUGIN_PROCS;

typedef struct _VISPLUGININIT
{
  int           x, y, cx, cy;   /* Input        */
  HWND          hwnd;           /* Input/Output */
  PPLUGIN_PROCS procs;          /* Input        */
  int           id;             /* Input        */
  char*         param;          /* Input        */
  HAB           hab;            /* Input        */

} VISPLUGININIT, *PVISPLUGININIT;

HWND DLLENTRY vis_init( PVISPLUGININIT initdata );
int  DLLENTRY plugin_deinit( void );

#pragma pack()

#ifdef __cplusplus
}
#endif
#endif /* PM123_VISUAL_PLUG_H */
