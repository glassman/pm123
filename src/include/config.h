#ifndef PM123_CONFIG_H
#define PM123_CONFIG_H

#include "../config/version.h"

/* Maximum length of URL */
#define _MAX_URL 2048

/* Name of package */
#define PACKAGE_NAME "PM123"
#define PACKAGE PACKAGE_NAME

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "dmitry@5nets.ru"

/* Version number of package */
#define PACKAGE_VERSION VER_STRING
#define VERSION PACKAGE_VERSION

/* Define to the full name and version of this package. */
#define PACKAGE_STRING PACKAGE" "VERSION

#if defined(__GNUC__)
  #include "config_gcc.h"
#elif defined(__WATCOMC__)
  #include "config_wcc.h"
#elif !defined(RC_INVOKED)
  #error Unsupported compiler.
#endif

/* Set to 1 if compiling for OS/2 */
#define OS_IS_OS2 1

/* Target processor clips on positive float to int conversion. */
#define CPU_CLIPS_POSITIVE   1
/* Target processor clips on negative float to int conversion. */
#define CPU_CLIPS_NEGATIVE   0
/* Target processor is big endian. */
#define CPU_IS_BIG_ENDIAN    0
/* Target processor is little endian. */
#define CPU_IS_LITTLE_ENDIAN 1

/* XIO specific configuration */
#define XIO_SERIALIZE_DISK_IO 1

/* FFTW specific configuration */

/* C compiler name and flags */
#define FFTW_CC CCNAME
/* Define to compile in single precision. */
#define FFTW_SINGLE 1
/* Use standard C type functions. */
#define USE_CTYPE
/* extra CFLAGS for codelets */
#define CODELET_OPTIM ""
/* Defines the calling convention used by the library. */
#define FFTEXP DLLENTRY
/* Use FFTW own 16-byte aligned malloc routine. */
#define WITH_OUR_MALLOC16
#define MIN_ALIGNMENT 16

/* libsndfile specific configuration */

/* Set to long if unknown. */
#define TYPEOF_SF_COUNT_T long long
/* Set to maximum allowed value of sf_count_t type. */
#define SF_COUNT_MAX 0x7FFFFFFFFFFFFFFFLL
/* Set to sizeof (long) if unknown. */
#define SIZEOF_SF_COUNT_T 8

/* libflac specific configuration */

#define FLaC__INLINE    INLINE
#define FLAC__CPU_IA32  1
#define FLAC__HAS_NASM  1
#define FLAC__USE_3DNOW 1
#define FLAC__HAS_OGG   1

/* libupnp specific configuration */

#define INCLUDE_CLIENT_APIS 1
#define INCLUDE_DEVICE_APIS 1
#define INTERNAL_WEB_SERVER 1
#define UPNP_HAVE_GENA      1
#define UPNP_HAVE_SOAP      1
#define UPNP_HAVE_SSDP      1
#define UPNP_HAVE_TOOLS     1

#endif /* PM123_CONFIG_H */
