#ifndef PM123_OUTPUT_PLUG_H
#define PM123_OUTPUT_PLUG_H

#include "format.h"
#include "decoder_plug.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(4)

ULONG DLLENTRY output_init  ( void** a );
ULONG DLLENTRY output_uninit( void*  a );

#define OUTPUT_OPEN          1 /* may not be necessary! */
#define OUTPUT_CLOSE         2
#define OUTPUT_VOLUME        3
#define OUTPUT_PAUSE         4
#define OUTPUT_SETUP         5
#define OUTPUT_TRASH_BUFFERS 6
#define OUTPUT_NOBUFFERMODE  7 /* obsolete, don't used anymore */

/* Obsolete definitions. Use instead HAVE_FIELD macro. */

#define OUTPUT_SIZE_1       76 /* size of the OUTPUT_PARAMS structure prior PM123 1.32 */
#define OUTPUT_SIZE_2       80 /* size of the OUTPUT_PARAMS structure since PM123 1.32 */
#define OUTPUT_SIZE_3       84 /* size of the OUTPUT_PARAMS structure since PM123 1.33 */

typedef struct _OUTPUT_PARAMS
{
  /* --- see OUTPUT_SIZE definitions */
  int size;

  /* --- OUTPUT_SETUP */

  FORMAT_INFO formatinfo;

  int buffersize;

  unsigned short boostclass, normalclass;
  signed   short boostdelta, normaldelta;

  void (DLLENTRYP error_display)( char* );

  /* info message function the output plug-in should use */
  /* this information is always displayed to the user right away */
  void (DLLENTRYP info_display)( char* );

  HWND hwnd; /* commodity for PM interface, sends a few messages to this handle. */

  /* --- OUTPUT_VOLUME */

  unsigned char volume;
  float amplifier;

  /* --- OUTPUT_PAUSE */

  BOOL  pause;
  BOOL  unused1;          /* obsolete, must be NULL */

  /* --- OUTPUT_TRASH_BUFFERS */

  ULONG temp_playingpos;  /* used until new buffers come in */

  /* --- OUTPUT_OPEN */

  char* filename;         /* filename, URL or track now being played,
                             useful for disk output */

  /* --- OUTPUT_SETUP */

  BOOL  always_hungry;    /* *OUTPUT* the output plug-in imposes no time restraint
                             it is always WM_OUTPUT_OUTOFDATA */

  DECODER_INFO* info;     /* added since PM123 1.32 */

  /* Added since PM123 1.33. Output plug-in should call this
     function for sound equalization. */

  int (DLLENTRYP equalize_samples)( FORMAT_INFO*, char* buf, int len );

} OUTPUT_PARAMS;

ULONG DLLENTRY output_command( void* a, ULONG msg, OUTPUT_PARAMS* info );
ULONG DLLENTRY output_playing_samples( void* a, FORMAT_INFO* info, char* buf, int len );
int   DLLENTRY output_play_samples( void* a, FORMAT_INFO* format, char* buf, int len, int posmarker );
ULONG DLLENTRY output_playing_pos( void* a );
BOOL  DLLENTRY output_playing_data( void* a );

#pragma pack()

#ifdef __cplusplus
}
#endif
#endif /* PM123_OUTPUT_PLUG_H */
