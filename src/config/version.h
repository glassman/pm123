#ifndef VERSION_H
#define VERSION_H

#define VER_MAJOR  1
#define VER_MINOR  43
#define APP_VENDOR "Dmitry Steklenev"

#define STRVALUE( x ) #x
#define TOSTRING( x ) STRVALUE( x )
#define VER_STRING TOSTRING( VER_MAJOR ) "." TOSTRING( VER_MINOR )

#ifdef __MAKEFILE__
VER_MAJOR  = 1
VER_MINOR  = 43
APP_VENDOR = Dmitry Steklenev
#endif
#endif
