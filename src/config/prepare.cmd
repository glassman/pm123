/*
 * REXX batch file that will replace the build level variables
 * in the module definition file.
 */

parse arg replist

strings.1.template = "DATE"
strings.1.replace  = date( 'E' )
strings.2.template = "TIME"
strings.2.replace  = time()
strings.3.template = "HOSTNAME"
strings.3.replace  = value( "HOSTNAME",, "OS2ENVIRONMENT" )
count = 3

delimiter = substr( replist, 1, 1 )
replist   = substr( replist, 2    )

do while replist \= ""  
   parse value replist with s1 (delimiter) s2 (delimiter) replist
   count = count + 1
   strings.count.template = strip(s1)
   strings.count.replace  = strip(s2)
end

do while lines( stdin ) > 0
   line = linein( stdin )
   do i = 1 to count
      line = replace( line, strings.i.template, strings.i.replace )
   end
   call lineout stdout, line
end
    
return 0

replace: procedure

  parse arg source, string, substitute
  string = translate( string )

  i = pos( string, source )

  do while i \= 0
     source = substr( source, 1, i-1 ) || substitute ||,
              substr( source, i + length( string ))

     i = pos( string, translate( source ), i + length( substitute ))
  end
     
return source
