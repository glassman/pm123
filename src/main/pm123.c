/*
 * Copyright 2007-2018 Dmitry Steklenev <dmitry@5nets.ru>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define  INCL_PM
#define  INCL_BASE
#include <os2.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <direct.h>
#include <utilfct.h>
#include <pipe.h>
#include <debuglog.h>
#include <pm123.h>

/* Creates and displays an error message window. */
static void
show_error( const char* format, ... )
{
  HAB  hab = WinInitialize( 0 );
  HMQ  hmq = WinCreateMsgQueue( hab, 0 );

  char padded_title[60];
  char message[4096];

  va_list args;

  va_start( args, format );
  vsprintf( message, format, args );
  sprintf ( padded_title, "%-59s", "PM123 Error" );

  WinMessageBox( HWND_DESKTOP, HWND_DESKTOP, (PSZ)message,
                 padded_title, 0, MB_ERROR | MB_OK | MB_MOVEABLE );

  WinDestroyMsgQueue( hmq );
  WinTerminate( hab );
}

/* Loads the PM123 main DLL and calls an its entry point. */
static int
call_dll_main( int argc, char* argv[], int files,
               const char* pipename, HPIPE hpipe, int options )
{
  char     exename[_MAX_PATH];
  char     libpath[_MAX_PATH];
  int      libsize;

  char     errmsg[512]       = "";
  char     errobj[_MAX_PATH] = "";
  HMODULE  hmodule;
  APIRET   rc;

  typedef int (DLLENTRYP dll_entry)( int, char*[], int, const char*, HPIPE, int );
  dll_entry entry;

  getExeName( exename, sizeof( exename ));
  DEBUGLOG(( "pm123: getExeName return %s\n", exename ));
  sdrivedir( libpath, exename, sizeof( libpath ));

  libsize = strlen( libpath );
  if( libsize && libpath[ libsize - 1 ] == '\\' && !is_root( libpath )) {
    libpath[ libsize - 1 ] = 0;
  }

  rc = DosSetExtLIBPATH( libpath, BEGIN_LIBPATH );
  DEBUGLOG(( "pm123: DosSetExtLIBPATH for %s, rc=%d\n", libpath, rc ));

  rc = DosLoadModule( errobj, sizeof( errobj ), "pm123.dll", &hmodule );
  DEBUGLOG(( "pm123: DosLoadModule for pm123.dll, rc=%d\n", rc ));

  if( rc != NO_ERROR ) {
    if( !*errobj ) {
      show_error( "Could not load PM123.DLL\n%s",
                 os2_strerror( rc, errmsg, sizeof( errmsg )));
    } else {
      show_error( "Could not load PM123.DLL\n%s\n%s ",
                 os2_strerror( rc, errmsg, sizeof( errmsg )), errobj );
    }
    return 1;
  }

  rc = DosQueryProcAddr( hmodule, 0L, "dll_main", &entry );
  DEBUGLOG(( "pm123: DosQueryProcAddr for dll_main, rc=%d, entry=%08X\n", rc, entry ));

  if( rc != NO_ERROR ) {
    show_error( "Could not load \"dll_main\" from PM123.DLL\n%s",
               os2_strerror( rc, errmsg, sizeof( errmsg )));
    return 1;
  }

  return entry( argc, argv, files, pipename, hpipe, options );
}

/* Create main pipe with only one instance possible since these pipe
   is almost all the time free, it wouldn't make sense having multiple
   intances. */
static HPIPE
pipe_create( int i, char* pipename, HPIPE* phpipe )
{
  ULONG rc;

  if( i > 0 ) {
    sprintf( pipename,"\\PIPE\\PM123_%d", i );
  } else {
    strcpy ( pipename,"\\PIPE\\PM123" );
  }

  rc = DosCreateNPipe( pipename, phpipe,
                       NP_ACCESS_DUPLEX,
                       NP_WAIT | NP_TYPE_BYTE | NP_READMODE_BYTE | 1,
                       2048, 2048, 500 );

  DEBUGLOG(( "pm123: DosCreateNPipe for %s, rc=%d\n", pipename, rc ));

  if( rc != NO_ERROR && rc != ERROR_PIPE_BUSY ) {
    show_error( "Could not create pipe %s, rc = %d.", pipename, rc );
    return NULLHANDLE;
  }

  return rc;
}

/* Parses and processes a commandline arguments. Must be
   called from the main thread. */
static void
process_file_arguments( HPIPE hpipe, int files, int argc, char *argv[] )
{
  char command[1024];
  char curpath[_MAX_PATH];
  char file[_MAX_PATH];
  int  i;

  getcwd( curpath, sizeof( curpath ));

  for( i = 1; i < argc; i++ ) {
    if( *argv[i] != '/' && *argv[i] != '-' ) {
      if( !rel2abs( curpath, argv[i], file, sizeof(file))) {
        strcpy( file, argv[i] );
      }
      if( files == 1 ) {
        snprintf( command, sizeof( command ), "%s", file );
      } else {
        snprintf( command, sizeof( command ), "*add %s", file );
      }
      pipe_write( hpipe, command );
    }
  }
}

/* The main PM123 entry point. */
int main( int argc, char* argv[] )
{
  int   a, i;
  int   options = 0;
  int   files = 0;
  char  pipename[_MAX_PATH] = "\\PIPE\\PM123";
  char  command[1024]= "*";
  HPIPE hpipe = NULLHANDLE;

  for( a = 1; a < argc; a++ )
  {
    DEBUGLOG(( "pm123: argv[%d] = '%s'\n", a, argv[a] ));

    if( *argv[a] == '-' || *argv[a] == '/' ) {
      if( stricmp( argv[a]+1, "shuffle" ) == 0 ) {
        options |= ARG_SHUFFLE;
      } else if( stricmp( argv[a]+1, "smooth" ) == 0 ) {
        // Not supported since 1.32
      } else if( stricmp( argv[a]+1, "cmd" ) == 0 ) {
        if( a+1 < argc ) {
          if( strncmp( argv[a+1], "\\\\", 2 ) == 0 )
          {
            strlcpy( pipename, argv[a+1], sizeof( pipename )); // machine name
            strlcat( pipename, "\\PIPE\\PM123", sizeof( pipename ));
            a += 2;
          } else {
            a += 1;
          }
          for( i = a; i < argc; i++ ) {
            strlcat( command, argv[i], sizeof( command ));
            strlcat( command, " ", sizeof( command ));
          }
          pipe_open_and_write( pipename, command );
          return 0;
        }
      } else if( stricmp( argv[a]+1, "asso:clear" ) == 0 ) {
        return call_dll_main( argc, argv, 0, NULL, NULLHANDLE, ARG_ASSO_CLEAR );
      } else if( stricmp( argv[a]+1, "asso:all" ) == 0 ) {
        return call_dll_main( argc, argv, 0, NULL, NULLHANDLE, ARG_ASSO_ALL );
      } else if( stricmp( argv[a]+1, "asso:on" ) == 0 ) {
        return call_dll_main( argc, argv, 0, NULL, NULLHANDLE, ARG_ASSO_ON );
      } else if( stricmp( argv[a]+1, "asso:off" ) == 0 ) {
        return call_dll_main( argc, argv, 0, NULL, NULLHANDLE, ARG_ASSO_OFF );
      }
    } else {
      ++files;
    }
  }

  if( pipe_create( 0, pipename, &hpipe ) == ERROR_PIPE_BUSY ) {
    if( files > 0 ) {
      if(( hpipe = pipe_open( pipename )) != NULLHANDLE ) {
        process_file_arguments( hpipe, files, argc, argv );
        pipe_close( hpipe );
        return 0;
      }
    }
  }

  if( !hpipe ) {
    for( i = 1; i < 32; i++ ) {
      if( pipe_create( i, pipename, &hpipe ) != ERROR_PIPE_BUSY ) {
        break;
      }
    }
    if( i >= 32 ) {
      show_error( "Too many instances." );
      return 1;
    }
  }

  return call_dll_main( argc, argv, files, pipename, hpipe, options );
}
