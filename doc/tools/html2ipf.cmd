/*--------------------------------------------------------------------*/
/* REXX script to convert a bunch of .html files into os/2 .ipf files */
/*  which can be converted later into .inf files using ipfc compiler  */
/*                                                                    */
/*               Copyright (c) 1997 by FRIENDS software               */
/*                         All Rights Reserved                        */
/*                                                                    */
/* FidoNet: 2:5030/84.5                                               */
/* e-mail:  Andrew Zabolotny <bit@freya.etu.ru>                       */
/*--------------------------------------------------------------------*/

/***************** user-customisable section start ********************/

/* A command to convert any image file into os/2 bmp format           */
/* Global.ImageConvert = 'alchemy.exe -o -O -8 <input> <output> >nul' */
/* Global.ImageConvert = 'gbmbpp.exe <input> <output> >nul'           */
Global.ImageConvert = 'gif2bmp.exe <input> <output> >nul'
/* Executable/description of an external WWW browser to launch when   */
/* user selects an URL link. Normally, you shouldn`t change it (even  */
/* if you have Netscape) since WebEx is found on almost every OS/2    */
/* system, and Navigator is not.                                      */
Global.WWWbrowser   = 'netscape.exe*Netscape Communicator'
/* default book font; use warpsans bold for a nicer-looking books     */
Global.DefaultFont  = ':font facename=default size=0x0.'
/* fonts for headings (1 through 6)                                   */
Global.HeaderFont.1 = ':font facename=''Helv'' size=24x12.'
Global.HeaderFont.2 = ':font facename=''Helv'' size=18x10.'
Global.HeaderFont.3 = ':font facename=''Helv'' size=14x8.'
Global.HeaderFont.4 = ':font facename=''Helv'' size=12x8.'
Global.HeaderFont.5 = ':font facename=''Helv'' size=10x6.'
Global.HeaderFont.6 = ':font facename=''Helv'' size=9x6.'
/* font for url links (which launches WebExplorer)                    */
Global.URLinkFont   = ':font facename=''WarpSans Bold'' size=9x6.'
/* fixed-width font (for <tt>...</tt>                                 */
Global.FixedFont    = ':font facename=''System VIO'' size=11x6.'

/***************** end of user-customisable section *******************/

'@echo off'
call rxFuncAdd 'SysLoadFuncs', 'RexxUtil', 'SysLoadFuncs'
call SysLoadFuncs

/********************** hard-coded variables **************************/

Global.maxLineLength = 80
/* unix end-of-line constant */
Global.EOL = d2c(10)
/* file extensions and name of handler procedures for these; */
/* all other file extensions will be ignored */
Global.TypeHandler = '*.HTML doParseHTML  *.SHTML doParseHTML  *.HTM doParseHTML',
                     '*.HT3  doParseHTML  *.HTM3  doParseHTML  *.TXT doParseText',
                     '*.TEXT doParseText  *.CMD   doParseText  *.BAT doParseText',
                     '*.GIF  doParseImage *.JPG   doParseImage *.PNG doParseImage'

/* Set up some global variables */

Global.Picture.0  = 0   /* keep track of embedded Pictures */
Global.LinkID     = 0   /* total number of external links */
Global.URLinks    = 0   /* keep track of url links */
Global.Title      = ''  /* book Title */
Global.IndexFile  = 1   /* This is an index file */
Global.HREF       = ''  /* Speedup: keep all encountered HREFs and IMG_SRCs in a */
Global.IMGSRC     = ''  /* string so we can use Pos() and WordPos() functions */
Global.SubLinks   = 0   /* This stem keeps track of the SUBLINKS tags */
Global.NoSubLinks = 0   /* This stem keeps track of the NOSUBLINKS tags */

/* Default state for all switches */
Global.optCO = 1        /* COlored output */
Global.optCE = 1        /* enable CEntering */
Global.optCH = 0        /* disable CHecking */
Global.optP  = 1        /* embed Pictures */
Global.optS  = 1        /* Sort links */
Global.optD  = 0        /* Debug log */

/* Physical styles for displaying text */
Global.Style.Italic     = '01'x
Global.Style.Bold       = '02'x
Global.Style.Underscore = '04'x
Global.Style.1          = "hp1."
Global.Style.2          = "hp2."
Global.Style.3          = "hp3."
Global.Style.4          = "hp5."
Global.Style.5          = "hp6."
Global.Style.6          = "hp7."
Global.Style.7          = "hp7."

/* Strings that must be replaced */
Strings.0 = 0

call AnalyseOptions
call DefineQuotes

call ShowHeader
if length( _fName ) == 0 then do
  call ShowHelp
end

Global.oName = _oName
if length( Global.oName ) == 0 then do
  i = lastPos( '.', _fName )
  if i > 0 then
    Global.oName = left( _fName, i ) || 'ipf'
  else do
    Global.oName = _fName||'.ipf'
  end
end

call SetColor "lCyan"
if Global.OptCH then
  say 'Checking the integrity of links for' _fName
else do
  say 'Output goes into' Global.oName
  call SysFileDelete( Global.oName )
end

DateTime = Date(n)', 'Time(c)

call logError ''
call logError '--- ['DateTime'] conversion started: index file '_fName
do i = 1 to Strings.0
   call LogError '--- Replacing "'Strings.i.template'" to "'Strings.i.replace'"'
end

call putline '.* 'copies( '-', 76 )'*'
call putline '.* 'center( 'Converted by HTML2IPF from '_fName' at 'DateTime, 76 )'*'
call putline '.* 'copies( '-', 76 )'*'
call putline ':userdoc.'
call putline ':docprof toc=12345.'

call time 'R'
call ParseFile _fName, 1
do until ResolveLinks(1) = 0
  Global.Sublinks   = 0
  Global.NoSublinks = 0 /* Include all unresolved sublinks */
end
call ConvertPictures
call OutputURLs

call putline ':euserdoc.'

call SetColor "lCyan"
elapsed = time( 'E' )
say 'finished; elapsed time = 'elapsed%3600':'elapsed%60':'trunc( elapsed // 60, 1 )
DateTime = Date(n)', 'Time(c)
call logError '--- ['DateTime'] conversion finished'
exit 0

AnalyseOptions:
  procedure expose Global. _fName _oName Strings.

  _fName = ''
  _oName = ''

  do i = 1
    "@SET HTML2IPFARG=%"i
    nw = value( "HTML2IPFARG",, "OS2ENVIRONMENT" )
    if nw = "" then
       leave

    if left( nw, 1 ) = '-' then do
      if translate( substr( nw, 2, 8 )) == 'REPLACE"' then do
        call AnalyseStrings substr( nw, 9 )
        iterate
      end
      if translate( substr( nw, 2, 2 )) == 'R"' then do
        call AnalyseStrings substr( nw, 3 )
        iterate
      end

      nw = translate( substr( nw, 2 ))
      OptState = pos( right( nw, 1 ), '-+' )
      if OptState > 0 then
        nw = left( nw, length( nw ) - 1 )
      else
        OptState = 2

      OptState = OptState - 1

      select
        when abbrev( 'COLORS', nw, 2 ) then
          Global.OptCO = OptState
        when abbrev( 'CENTER', nw, 2 ) then
          Global.OptCE = OptState
        when abbrev( 'CHECK', nw, 2 ) then
          Global.OptCH = OptState
        when abbrev( 'SORT', nw, 1 ) then
          Global.OptS = OptState
        when abbrev( 'PICTURES', nw, 1 ) then
          Global.OptP = OptState
        when abbrev( 'DEBUG', nw, 1 ) then
          Global.OptD = OptState
        otherwise
          do
            call ShowHeader
            call SetColor "lRed"
            say 'Invalid option in command line:' value( "HTML2IPFARG",, "OS2ENVIRONMENT" )
            call ShowHelp
          end
      end
      end
    else do
      if left(nw,1) == '"' & right(nw,1) == '"' then
         nw = substr( nw, 2, length(nw) - 2 )

      if length( _fName ) == 0 then
        _fName = nw
      else if length( _oName ) == 0 then
        _oName = nw
      else if length( Global.Title ) == 0 then
        Global.Title = nw
      else do
        call ShowHeader
        call SetColor "lRed"
        say 'Extra filename in command line:' value( "HTML2IPFARG",, "OS2ENVIRONMENT" )
        call ShowHelp
      end
    end
  end
return

AnalyseStrings:

  procedure expose Strings.
  parse arg replist

  if left( replist, 1 ) == '"' & right( replist, 1 ) == '"' then
     replist = substr( replist, 2, length(replist) - 2 )

  delimiter = substr( replist, 1, 1 )
  replist   = substr( replist, 2    )
  count     = Strings.0

  do while replist \= ""
     parse value replist with s1 (delimiter) s2 (delimiter) replist
     count = count + 1
     Strings.count.template = strip(s1)
     Strings.count.replace  = strip(s2)
  end

  Strings.0 = count
return

ShowHeader:
  call SetColor "White"
  say '��� HTML2IPF ��� Version 0.2.0 ��� Copyright (c) 1997 by FRIENDS software ���'
return

ShowHelp:
  call SetColor "Yellow"
  say 'Usage: HTML2IPF <IndexFilename.HTML> [<OutputFilename.IPF> [<DocTitle>]] {conversion options}'
  call SetColor "lGreen"
  say '<IndexFilename.HTML>'
  call SetColor "Green"
  say '�Ĵis the "root" .HTML file to start with'
  call SetColor "lGreen"
  say '<OutputFilename.IPF>'
  call SetColor "Green"
  say '�Ĵis the output filename (usually with the .IPF extension)'
  say '<DocTitle>'
  call SetColor "Green"
  say '�ĴProvides a title for the generated document'
  call SetColor "lGreen"
  say '{conversion options}'
  call SetColor "Green"
  say '��´are one or more of the following:'
  say '  ��´-CO{LORS}{+|-}'
  say '   ���use (+) or don`t use (-) ansi [c]olors in output'
  say '   �´-CE{NTER}{+|-}'
  say '   ���enable (+) or disable (-) processing <CENTER> tags'
  say '   �´-CH{ECK}{+|-}'
  say '   ���enable (+) or disable (-) checking files only'
  say '   �´-S{ORT}{+|-}'
  say '   ���sort (+) or don`t sort (-) links alphabetically'
  say '   �´-P{ICTURES}{+|-}'
  say '   ���include (+) or don`t include (-) [p]ictures in .IPF file'
  say '   �´-D{EBUG}{+|-}'
  say '   ���enable (+) or disable (-) [d]ebug logging into HTML2IPF.LOG'
  say '   �´-R{EPLACE}"/s1/s2..."'
  say '    ��Replace s1 to s2 in all processed HTML files'
  call SetColor "lCyan"
  say 'default HTML2IPF options:'
  call SetColor "Cyan"
  say '�Ĵ-COLORS+ -CENTER+ -CHECK- -SORT+ -PICTURES+ -DEBUG-'
  exit 1

ConvertPictures:
  procedure expose Global.

  if \Global.optP | Global.OptCH then
    return

  do i = 1 to Global.Picture.0
    /* get time stamp of destination file */
    tstmp = stream( Global.Picture.i.dst, 'c', 'Query TimeStamp' )
    if ( tstmp = '' ) | ( tstmp < stream( Global.Picture.i.src, 'c', 'Query TimeStamp' )) then
      call RunCmd Global.ImageConvert, Global.Picture.i.src, Global.Picture.i.dst
  end
return

RunCmd:
  procedure expose Global.

  parse arg cmd, in, out

  call SetColor "lGreen"
  ip = pos( '<input>', cmd )
  if ip <> 0 then
    cmd = left( cmd, ip - 1 ) || translate( in,  "/", "\" ) || substr( cmd, ip + 7 )

  op = pos( '<output>', cmd )
  if op <> 0 then
    cmd = left( cmd, op - 1 ) || translate( out, "/", "\" ) || substr( cmd, op + 8 )

  cmd
return

OutputURLs:
  /* make a chapter with links to internet locations */
  if Global.URLinks = 0 then
    return

  /* Sort URLs alphabetically */
  if Global.OptS then
    do i = 1 to Global.URLinks
      ii = Global.URLinks.i
      do j = i + 1 to Global.URLinks
        ji = Global.URLinks.j
        if Global.LinkID.ji < Global.LinkID.ii then do
          tmp = Global.URLinks.i
          Global.URLinks.i = Global.URLinks.j
          Global.URLinks.j = tmp
          ii = ji
        end
      end
    end

  if Global.OptCH then do
    call SetColor "LGreen"
    do i = 1 to Global.URLinks
      j = Global.URLinks.i
      say 'Unresolved link:' Global.LinkID.j.RealName
      call logError '--- Unresolved link:' Global.LinkID.j.RealName
    end
    return
  end

  Global.CurrentDir = ''
  do i = 1 to Global.URLinks
    j = Global.URLinks.i
    call putline ':h2 hide id='GetLinkID( Global.LinkID.j )'.'IPFstring( Global.LinkID.j.RealName )
    call putline Global.DefaultFont
    call putline ':lines align=center.'
    call putline IPFstring( 'The link you selected points to an external resource. Click the ' ||,
                            'URL below to launch 'substr( Global.WWWbrowser, pos( '*', Global.WWWbrowser ) + 1 )".")
    call putline Global.URLinkFont
    call putline ':p.:link reftype=launch object='''left( Global.WWWbrowser, pos( '*', Global.WWWbrowser ) - 1 ),
                 ''' data='''Global.LinkID.j.RealName'''.'
    call putline IPFstring( Global.LinkID.j.RealName )
    call putline ':elink.:elines.'
  end
return

/* Parse a HTML file; called recursively if needed */
ParseFile:
  procedure expose Global. Strings.

  parse arg fName, DeepLevel
  call SetColor "Cyan"
  call charout, 'Parsing 'fName' ...'

  Global.CurrentDir = ''

  id = GetLinkID( fName )
  if id > 0 then
    Global.LinkID.id.Resolved = 1

  tmp = translate( stream( fName, 'c', 'Query Exists' ), '/', '\' )

  if length( tmp ) = 0 then do
    call SetColor "lRed"
    say ' not found'
    call logError '--- file 'fName' not found'
    return
  end

  fName = Shorten( tmp )
  Global.CurrentDir  = fileSpec( 'P', translate( fName, '\', '/' ))
  Global.CurrentFile = fName

  call logError '--- Parsing file "'fName'" ...'

  Global.Article.Title = ''   /* Article Title */
  Global.Article.line.0 = 1   /* count of lines in Article */
  Global.Article.Hidden = 0   /* Is current article hidden from book contents? */
  Global.Article.ResId  = 0   /* Article resource identifier */
  Global.OpenTag.0 = 0        /* keep track of open tags to close at end of chapter */
  Global.RefEndTag = ''       /* end tag to put at next end-of-reference <\a> */
  Global.IsTable = 0          /* We`re inside a <TABLE>...</TABLE> pair? */
  Global.IsCentered = 0       /* We`re inside a <CENTER>...</CENTER> pair? */
  Global.IsOutputEnabled = 1  /* A global switch to enable/disable text output */
  Global.IsPreFormatted = 0   /* set to 1 if text is preformatted */
  Global.AfterBreak = 1       /* set to 1 after .br to avoid empty lines */
  Global.AfterBlank = 1       /* set to 1 after :p. to avoid empty lines */
  Global.NewParagraph = 0
  Global.TextStyle = '00'x

  Global.Article.line.1 = ''  /* initialize output subsystem */
  Global.EOF = 0

  Global.CurFont = Global.DefaultFont
  /* Remember the count of SUBLINKS and NOSUBLINKS to restore it later */
  locSublinks = Global.Sublinks
  locNoSublinks = Global.NoSublinks

  fExt = max( lastPos( '/', fName ), lastPos( '\', fName ))
  if lastPos( '.', fName ) > fExt then
    fExt = translate( substr( fName, lastPos( '.', fName ) + 1 ))
  else
    fExt = ''

  fExt = wordpos( '*.'fExt, Global.TypeHandler )
  if fExt > 0 then
    fExt = word( Global.TypeHandler, fExt + 1 )
  else do
    call SetColor "lRed"
    say ' unknown file type'
    call logError '--- File 'fName': unknown type - ignored'
    return
  end

  Global.FileSize = chars( fName )

  select
    when fExt = 'doParseHTML'  then
      call doParseHTML
    when fExt = 'doParseImage' then
      call doParseImage
    when fExt = 'doParseText'  then
      call doParseText
    otherwise
      call logError 'Unknown file type handler: 'fExt
  end

  call ProgressBar
  call stream Global.CurrentFile, 'c', 'close'

  if length( Global.Article.Title ) = 0 then
    Global.Article.Title = IPFstring( filespec( 'N', translate( fName, '\', '/' )))

  if length( Global.Title ) = 0  then do
    Global.Title = Global.Article.Title
  end
  if Global.IndexFile then do
    call putline ':title.'Global.Title
  end

  call putline '.* Source filename: 'fName

  if id > 0 then do
    if Global.Article.Hidden & Global.IndexFile then do
      i = max( 1, DeepLevel - 1 )
      j = ' hide'
      Global.SubLinks = 1
      Global.Sublinks.1 = '*'
      end
    else do
      i = DeepLevel
      j = ''
    end

    if Global.Article.ResId > 0 then
      call putline ':h'i' res='Global.Article.ResId' id='id || j'.'Global.Article.Title
    else
      call putline ':h'i' id='id || j'.'Global.Article.Title
  end
  Global.IndexFile = 0

  call putline Global.DefaultFont
  do i = 1 to Global.Article.line.0
    call putline Global.Article.line.i
  end
  drop Global.Article.

  call SetColor "Blue"
  call charout, ' done'
  call CRLF

  call ResolveLinks DeepLevel + 1

  /* Restore the SUBLINKS and NOSUBLINKS counter */
  Global.Sublinks = locSublinks
  Global.NoSublinks = locNoSublinks
return

ResolveLinks:
  procedure expose Global. Strings.
  arg DeepLevel

  LinkCount = 0
  Links.0 = 0

  do i = 1 to Global.LinkID
    if \Global.LinkID.i.Resolved then do
      if Global.SubLinks > 0 then do
        do j = 1 to Global.SubLinks
          if Pos( Global.SubLinks.j, translate( Global.LinkID.i.InitialName )) = 1 then do
            j = -1
            leave
          end
        end
        if j \= -1 then
          Iterate
      end
      do j = 1 to Global.NoSubLinks
        if Pos( Global.NoSubLinks.j, translate( Global.LinkID.i.InitialName )) = 1 then do
          j = -1
          leave
        end
      end
      if j = -1 then
        Iterate

      Links.0 = Links.0 + 1
      j = Links.0
      Links.j = Global.LinkID.i.RealName
      Global.LinkID.i.Resolved = 1
    end
  end

  if Global.OptS then
    call SortLinks 1, Links.0

  if DeepLevel > 6 then
    DeepLevel = 6

  do i = 1 to Links.0
    call ParseFile translate( Links.i, '/', '\' ), DeepLevel
    LinkCount = LinkCount + 1
  end

  drop Global.SubLinks.
  drop Global.NoSubLinks.

return LinkCount

SortLinks:
  procedure expose Links.
  arg iLeft, iRight

  Left   = iLeft
  Right  = iRight
  Middle = (Left + Right) % 2
  MidVar = Links.Middle

  do until Left > Right
    do while Links.Left < MidVar
      Left = Left + 1
    end
    do while Links.Right > MidVar
      Right = Right - 1
    end

    if Left <= Right then do
      tmp = Links.Left
      Links.Left = Links.Right
      Links.Right = tmp
      Left = Left + 1
      Right = Right - 1
    end
  end
  if iLeft < Right
    then call SortLinks iLeft, Right
  if Left < iRight
    then call SortLinks Left, iRight

return

doParseHTML:
  Global.FileContents = ''
  call ParseContents 'EMPTY'
return

doParseText:
  /* A plain text file cannot have sublinks */
  Global.SubLinks = 1
  Global.SubLinks.1 = '*'

  /* Draw text using fixed-width font */
  call PutToken ':lines align=left.'
  call SetFont Global.FixedFont

  do while chars( fName ) > 0
    call ProgressBar
    Global.FileContents = charin( fName,, 4096 )

    /* remove all \0x0d Characters from output stream */
    do until i = 0
      i = pos( d2c(13), Global.FileContents )
      if i > 0 then
        Global.FileContents = delstr( Global.FileContents, i, 1 )
    end
    call PutText Global.FileContents
  end
  call PutToken ':elines.'
return

doParseImage:
  _imgBitmap = GetPictureID( fName )
  if \Global.optP | length( _imgBitmap ) <= 1 then do
    if Global.optP then do
      call SetColor "Yellow"
      parse value SysCurPos() with row col
      if col > 0 then
        call CRLF
      say  'Warning: Picture "'Global._imgname'" missing'
      call logError 'Picture "'Global._imgname'" missing'
    end
    call PutText ':lines align=center.'
    call PutText fName
    call PutText ':elines.'
    end
  else do
    Global.Picture.0 = Global.Picture.0 + 1
    i = Global.Picture.0
    Global.Picture.i.dst = left( _imgBitmap, pos( '*', _imgBitmap ) - 1 )
    Global.Picture.i.src = substr( _imgBitmap, pos( '*', _imgBitmap ) + 1 )
    Global.Picture.i.alt = fName
    call PutToken ':artwork name='''Global.Picture.i.dst''' align=center.'
  end
return

ParseContents:
  procedure expose Global. Strings.
  arg TextHandler

  do until length( Global.FileContents ) = 0  & Global.EOF
    Token = GetToken()
    if left( Token, 1 ) = d2c(0) then do
      Token = strip( substr( Token, 2 ))
      /* assume everything starting with <!-- is not important */
      if left( Token, 3 ) = '!--' then
        iterate

      Tag = strip( translate( Token, xrange( 'A', 'Z' )'_!', xrange( 'a', 'z')'-/' ))
      TagBreakPos = pos( ' ', Tag )
      if TagBreakPos > 0 then
        Tag = left( Tag, TagBreakPos - 1 )
      TagBreakPos = 0

      select
        when Tag = 'HTML'   then TagBreakPos = doTagHTML()
        when Tag = '!HTML'  then TagBreakPos = doTag!HTML()
        when Tag = 'HEAD'   then TagBreakPos = doTagHEAD()
        when Tag = '!HEAD'  then TagBreakPos = doTag!HEAD()
        when Tag = 'BODY'   then TagBreakPos = doTagBODY()
        when Tag = '!BODY'  then TagBreakPos = doTag!BODY()
        when Tag = 'META'   then TagBreakPos = doTagMETA()
        when Tag = 'TITLE'  then TagBreakPos = doTagTITLE()
        when Tag = '!TITLE' then TagBreakPos = doTag!TITLE()
        when Tag = 'META'   then TagBreakPos = doTagMETA()
        when Tag = 'A'      then TagBreakPos = doTagA()
        when Tag = '!A'     then TagBreakPos = doTag!A()
        when Tag = 'IMG'    then TagBreakPos = doTagIMG()
        when Tag = 'I'      then TagBreakPos = doTagI()
        when Tag = '!I'     then TagBreakPos = doTag!I()
        when Tag = 'B'      then TagBreakPos = doTagB()
        when Tag = '!B'     then TagBreakPos = doTag!B()
        when Tag = 'U'      then TagBreakPos = doTagU()
        when Tag = '!U'     then TagBreakPos = doTag!U()
        when Tag = 'EM'     then TagBreakPos = doTagEM()
        when Tag = '!EM'    then TagBreakPos = doTag!EM()
        when Tag = 'TT'     then TagBreakPos = doTagTT()
        when Tag = '!TT'    then TagBreakPos = doTag!TT()
        when Tag = 'P'      then TagBreakPos = doTagP()
        when Tag = '!P'     then TagBreakPos = doTag!P()
        when Tag = 'H1'     then TagBreakPos = doTagH1()
        when Tag = '!H1'    then TagBreakPos = doTag!H1()
        when Tag = 'H2'     then TagBreakPos = doTagH2()
        when Tag = '!H2'    then TagBreakPos = doTag!H2()
        when Tag = 'H3'     then TagBreakPos = doTagH3()
        when Tag = '!H3'    then TagBreakPos = doTag!H3()
        when Tag = 'H4'     then TagBreakPos = doTagH4()
        when Tag = '!H4'    then TagBreakPos = doTag!H4()
        when Tag = 'H5'     then TagBreakPos = doTagH5()
        when Tag = '!H5'    then TagBreakPos = doTag!H5()
        when Tag = 'H6'     then TagBreakPos = doTagH6()
        when Tag = '!H6'    then TagBreakPos = doTag!H6()
        when Tag = 'OL'     then TagBreakPos = doTagOL()
        when Tag = '!OL'    then TagBreakPos = doTag!OL()
        when Tag = 'UL'     then TagBreakPos = doTagUL()
        when Tag = '!UL'    then TagBreakPos = doTag!UL()
        when Tag = 'LI'     then TagBreakPos = doTagLI()
        when Tag = 'DL'     then TagBreakPos = doTagDL()
        when Tag = '!DL'    then TagBreakPos = doTag!DL()
        when Tag = 'DT'     then TagBreakPos = doTagDT()
        when Tag = 'DD'     then TagBreakPos = doTagDD()
        when Tag = 'BR'     then TagBreakPos = doTagBR()
        when Tag = 'CITE'   then TagBreakPos = doTagCITE()
        when Tag = '!CITE'  then TagBreakPos = doTag!CITE()
        when Tag = 'CENTER' then TagBreakPos = doTagCENTER()
        when Tag = '!CENTER'  then TagBreakPos = doTag!CENTER()
        when Tag = 'PRE'    then TagBreakPos = doTagPRE()
        when Tag = '!PRE'   then TagBreakPos = doTag!PRE()
        when Tag = 'META'   then TagBreakPos = doTagMETA()
        when Tag = 'MENU'   then TagBreakPos = doTagMENU()
        when Tag = '!MENU'  then TagBreakPos = doTag!MENU()
        when Tag = 'CODE'   then TagBreakPos = doTagCODE()
        when Tag = '!CODE'  then TagBreakPos = doTag!CODE()
        when Tag = 'VAR'    then TagBreakPos = doTagVAR()
        when Tag = '!VAR'   then TagBreakPos = doTag!VAR()
        when Tag = 'STRONG' then TagBreakPos = doTagSTRONG()
        when Tag = '!STRONG'  then TagBreakPos = doTag!STRONG()
        when Tag = 'ADDRESS'  then TagBreakPos = doTagADDRESS()
        when Tag = '!ADDRESS' then TagBreakPos = doTag!ADDRESS()
        when Tag = 'HR'     then TagBreakPos = doTagHR()
        when Tag = 'TABLE'  then TagBreakPos = doTagTABLE()
        when Tag = '!TABLE' then TagBreakPos = doTag!TABLE()
        when Tag = 'TR'     then TagBreakPos = doTagTR()
        when Tag = '!TR'    then TagBreakPos = doTag!TR()
        when Tag = 'TH'     then TagBreakPos = doTagTH()
        when Tag = '!TH'    then TagBreakPos = doTag!TH()
        when Tag = 'TD'     then TagBreakPos = doTagTD()
        when Tag = '!TD'    then TagBreakPos = doTag!TD()
        when Tag = 'BLOCKQUOTE'   then TagBreakPos = doTagBLOCKQUOTE()
        when Tag = '!BLOCKQUOTE'  then TagBreakPos = doTag!BLOCKQUOTE()
        otherwise
          call logError 'Unexpected tag <'Token'>'
      end
      if TagBreakPos then
        leave
      end
    else
      select
        when TextHandler = 'EMPTY' then call doTextEMPTY
        when TextHandler = 'HEAD'  then call doTextHEAD
        when TextHandler = 'BODY'  then call doTextBODY
      end
  end
return

ParseTag:
  procedure expose Global.
  parse arg Tag

  parse var Tag Prefix Tag
  Prefix = translate( Prefix )

  do while length( Tag ) > 0
    parse value translate( Tag, ' ', Global.EOL ) with subTag '=' Tag
    Tag = strip( Tag, 'leading' )
    if left( Tag, 1 ) = '"' then
      parse var Tag '"' subTagValue '"' Tag
    else
      parse var Tag subTagValue Tag

    subTag = translate( strip( subTag ))
    subTagValue = strip( subTagValue )

    select
      when Prefix = 'A' then
        select
          when subTag = 'HREF' then call doTagA_HREF
          when subTag = 'NAME' then call doTagA_NAME
          otherwise
            call logError 'Unexpected subTag 'subTag'="'subTagValue'"'
        end
      when Prefix = 'IMG' then
        select
          when subTag = 'SRC'     then call doTagIMG_SRC
          when subTag = 'ALT'     then call doTagIMG_ALT
          when subTag = 'ALIGN'   then call doTagIMG_ALIGN
          when subTag = 'WIDTH'   then call doTagIMG_WIDTH
          when subTag = 'HEIGHT'  then call doTagIMG_HEIGHT
          otherwise
            call logError 'Unexpected subTag 'subTag'="'subTagValue'"'
        end
      when Prefix = 'HTML' then
        select
          when subTag = 'HIDDEN'      then call doTagHTML_HIDDEN
          when subTag = 'SUBLINKS'    then call doTagHTML_SUBLINKS
          when subTag = 'NOSUBLINKS'  then call doTagHTML_NOSUBLINKS
          when subTag = 'RESID'       then call doTagHTML_RESID
          otherwise
            call logError 'Unexpected subTag 'subTag'="'subTagValue'"'
        end
      when Prefix = 'TABLE' then
        select
          when subTag = 'BORDER'  then call doTagTABLE_BORDER
          otherwise
        end
    end
  end
return

doTagHTML:
  call ParseTag Token
  call ParseContents 'EMPTY'
return 0

doTag!HTML:
return 1

doTagHTML_HIDDEN
  Global.Article.Hidden = 1
return 0

doTagHTML_RESID:
  Global.Article.ResID = SubTagValue
return 0

doTagHTML_SUBLINKS:
  Global.SubLinks = Global.SubLinks + 1
  i = Global.SubLinks
  Global.SubLinks.i = translate( SubTagValue )
return 0

doTagHTML_NOSUBLINKS:
  Global.NoSubLinks = Global.NoSubLinks + 1
  i = Global.NoSubLinks
  Global.NoSubLinks.i = translate( SubTagValue )
return 0

doTagHEAD:
  Global.grabTitle = 0
  call ParseContents 'HEAD'
return 0

doTag!HEAD:
  Global.grabTitle = 0
return 1

doTagBODY:
  Global.grabTitle = 0
  call ParseContents 'BODY'
return 0

doTag!BODY:
return 1

doTagTITLE:
  Global.grabTitle = 1
  Global.Article.Title = ''
return 0

doTag!TITLE:
  Global.grabTitle = 0
return 0

doOpenStyleTag:
  parse arg NewStyle
  if Global.TextStyle \= '00'x then do
    i = c2d( Global.TextStyle )
    call PutToken ':e'Global.Style.i
  end
  Global.TextStyle = bitor( Global.TextStyle, NewStyle )
  i = c2d( Global.TextStyle )
  call PutToken ':'Global.Style.i
return 0

doCloseStyleTag:
  parse arg OldStyle
  OldStyle = bitand( Global.TextStyle, OldStyle )
  if OldStyle \= '00'x then do
    i = c2d( Global.TextStyle )
    call PutToken ':e'Global.Style.i

    Global.TextStyle = bitxor( Global.TextStyle, OldStyle )
    if Global.TextStyle \= '00'x then do
      i = c2d( Global.TextStyle )
      call PutToken ':'Global.Style.i
    end
  end
return 0

doTagCITE:
doTagI:
doTagEM:
doTagVAR:
  call doOpenStyleTag Global.Style.Italic
return 0

doTag!CITE:
doTag!I:
doTag!EM:
doTag!VAR:
  call doCloseStyleTag Global.Style.Italic
return 0

doTagB:
  if \Global.isTable then
    call doOpenStyleTag Global.Style.Bold
return 0

doTag!B:
  if \Global.isTable then
    call doCloseStyleTag Global.Style.Bold
return 0

doTagU:
  call doOpenStyleTag Global.Style.Underscore
return 0

doTag!U:
  call doCloseStyleTag Global.Style.Underscore
return 0

doTagSTRONG:
  call PutToken ':hp8.'
return 0

doTag!STRONG:
  call PutToken ':ehp8.'
return 0

doTagCODE:
doTagTT:
  call SetFont Global.FixedFont
return 0

doTag!CODE:
doTag!TT:
  call SetFont Global.DefaultFont
return 0

doTagBLOCKQUOTE:
  call CloseIMG
  call NewParagraph
  call PutToken ':lm margin=6.'
  call NewLine
return 0

doTag!BLOCKQUOTE:
  call CloseIMG
  call NewLine
  call PutToken ':lm margin=1.'
  call NewParagraph
return 0

doTagP:
  call CloseIMG
  call NewParagraph
return 0

doTag!P:
  Global.NewParagraph = 0
return 0

doTagBR:
  call NewLine
  /* IPFC does not allow .br`s in tables */
  if Global.IsTable then
     call PutToken ':p.'
  else
     call PutToken '.br'
  call NewLine
  Global.AfterBreak = 1
return 0

doTagPRE:
  Global.IsPreformatted = 1
  Global.NewParagraph = 0
  call NewLine
  call PutToken ':cgraphic.'
return 0

doTag!PRE:
  Global.IsPreformatted = 0
  call PutToken ':ecgraphic.'
  call NewLine
return 0

doTagH_begin:
  arg  i
  call NewParagraph
  call SetFont Global.HeaderFont.i
return

doTagH1:
  call doTagH_begin 1
return 0

doTagH2:
  call doTagH_begin 2
return 0

doTagH3:
  call doTagH_begin 3
return 0

doTagH4:
  call doTagH_begin 4
return 0

doTagH5:
  call doTagH_begin 5
return 0

doTagH6:
  call doTagH_begin 6
return 0

doTag!H1:
doTag!H2:
doTag!H3:
doTag!H4:
doTag!H5:
doTag!H6:
  call SetFont Global.DefaultFont
  call NewParagraph
return 0

doTagHR:
  if \Global.AfterBreak then do
    call NewLine
    call PutToken '.br'
  end
  call NewLine
  call PutToken copies('�', 80)
  call NewLine
  call PutToken '.br'
  call NewLine

  Global.AfterBreak = 1
return 0

doTagOL:
  if Global.IsTable then
    return 0
  call doOpenListTag ':ol compact.', ':eol.'
return 0

doTag!OL:
  if Global.IsTable then
    return 0
  call doCloseListTag ':eol.'
return 0

doTagMENU:
doTagUL:
  if Global.IsTable then
    return 0
  call doOpenListTag ':ul compact.', ':eul.'
return 0

doTag!MENU:
doTag!UL:
  if Global.IsTable then
    return 0

  call doCloseListTag ':eul.'
return 0

doTagLI:
  if Global.IsTable then
    return 0
  if doCheckListTag( ':eul.' ) = 0 & doCheckListTag( ':eol.' ) = 0 then
    call doTagUL

  if Global.NewParagraph then do
     Global.NewParagraph = 0

     if \Global.AfterBreak then do
       call NewLine
       call PutToken '.br'
     end
     call NewLine
     Global.AfterBreak = 1
  end

  call NewLine
  call PutToken ':li.'
return 0

doTagDL:
  if Global.IsTable then
    return 0
  call doOpenListTag ':dl compact break=all.', ':edl.'
  Global.DLTermDefined = 0
  Global.DLDescDefined = 0
return 0

doTag!DL:
  if Global.IsTable then
    return 0
  call NewLine
  if \Global.DLDescDefined then
    call doTagDD
  call doCloseListTag ':edl.'
return 0

doTagDT:
  if Global.IsTable then
    return 0
  if doCheckListTag( ':edl.' ) = 0 then
    call doOpenListTag ':dl compact break=all.', ':edl.'
  call NewLine
  call PutToken ':dt.'
  Global.DLTermDefined = 1
  Global.DLDescDefined = 0
return 0

doTagDD:
  if Global.IsTable then
    return 0
  if doCheckListTag( ':edl.' ) = 0 then
    call doOpenListTag ':dl compact break=all.', ':edl.'
  call NewLine
  if \Global.DLTermDefined then
    call doTagDT
  call PutToken ':dd.'
  Global.DLTermDefined = 0
  Global.DLDescDefined = 1
return 0

doTagA:
  call CloseRef
  call ParseTag Token
return 0

doTag!A:
  call CloseRef
return 0

doTagA_HREF:
  i = GetLinkID( subTagValue )
  if i > 0 then do
    call PutToken ':link reftype=hd refid='i'.'
    Global.CurLink = i
    Global.RefEndTag = ':elink.' || Global.RefEndTag
  end
return 0

doTagA_NAME:
return 0  /* ignore */

doTagIMG:
  Global._altName = 'missing Picture'
  Global._imgName = ''

  /* Choose default picture alignment */
  if Global.IsCentered then
    Global._imgAlign = 'center'
  else
    Global._imgAlign = 'left'

  call ParseTag Token
  _imgBitmap = GetPictureID( Global._imgName )
  if \Global.optP | length(_imgBitmap) <= 1 | Global.IsTable then do
    /* Since IPF does not allow pictures in tables :-( */
    if Global.optP & \Global.IsTable then do
      call SetColor "Yellow"
      parse value SysCurPos() with row col
      if col > 0 then
        call CRLF
      say  'Warning: Picture "'Global._imgName'" missing'
      call logError 'Picture "'Global._imgName'" missing'
    end
    call PutText ' 'Global._altName' '
    end
  else do
    if pos( ':elink.', Global.RefEndTag ) > 0 then do
      /* image is a link */
      call PutToken ':elink.'
    end
    Global.Picture.0 = Global.Picture.0 + 1
    i = Global.Picture.0
    Global.Picture.i.dst = left( _imgBitmap, pos( '*', _imgBitmap ) - 1 )
    Global.Picture.i.src = substr( _imgBitmap, pos( '*', _imgBitmap ) + 1 )
    Global.Picture.i.alt = Global._altName
    call PutToken ':artwork name='''Global.Picture.i.dst''' align='Global._imgAlign 'runin.'
    if pos( ':elink.', Global.RefEndTag ) > 0 then do
      /* image is a link */
      call PutToken ':artlink.:link reftype=hd refid='Global.CurLink'.:eartlink.'
      call PutToken ':link reftype=hd refid='Global.CurLink'.'
    end
  end
return 0

doTagIMG_ALIGN:
  if pos( '<'translate(subTagValue)'>', '<LEFT><RIGHT><CENTER>') > 0 then
    Global._imgAlign = subTagValue
return 0

doTagIMG_SRC:
  Global._imgName = subTagValue
return 0

doTagIMG_ALT:
  Global._altName = subTagValue
return 0

doTagIMG_WIDTH:
doTagIMG_HEIGHT:
return 0  /* nop */

doTagADDRESS:
return 0  /* nop */

doTag!ADDRESS:
return 0  /* nop */

doTagMETA:
return 0  /* nop */

doTagCENTER:
  if \Global.OptCE then
    return 0

  Global.IsCentered = 1
  call CloseIMG
  call NewLine
  call PutToken ':lines align=center.'
  call NewLine
return 0

doTag!CENTER:
  if \Global.OptCE then
    return 0
  if Global.IsCentered then do
    Global.IsCentered = 0
    call CloseIMG
    call NewLine
    call PutToken ':elines.'
    call NewLine
  end
return 0

doTagTABLE:
  Global.Table.noborder = 1
  call ParseTag Token
  Global.NewParagraph = 0
  call NewLine
  call PutToken ':table.'
  Global.Table.Begin = Global.Article.Line.0
  call NewLine
  Global.Table.TotalCols.0 = 0
  Global.Table.Cols.0 = 0
  Global.IsTable = 1
  Global.IsOutputEnabled = 0
return 0

doTagTABLE_BORDER:
  if subTagValue then
    Global.Table.noborder = 0
  else
    Global.Table.noborder = 1
return 0

doTag!TABLE:
  call NewLine
  if Global.IsTable then do
    i = Global.Table.Begin
    if Global.Table.TotalCols.0 > 0 then
      ColWidth = ( 79 - Global.Table.TotalCols.0 ) % Global.Table.TotalCols.0
    else
      ColWidth = 78

    tableCols = ''
    do j = 1 to Global.Table.TotalCols.0
      if Global.Table.TotalCols.j < ColWidth then do
        tableCols = tableCols' 'Global.Table.TotalCols.j
        if j < Global.Table.TotalCols.0 then do
          ColWidth = ColWidth + ( ColWidth - Global.Table.TotalCols.j ) %,
                                ( Global.Table.TotalCols.0 - j )
        end
        end
      else
        tableCols = tableCols' 'ColWidth
    end
    if \Global.OptCH then do
      if Global.Table.noborder then
        Global.Article.Line.i = ':table rules=none frame=none cols='''substr(tableCols, 2)'''.'
      else
        Global.Article.Line.i = ':table cols='''substr(tableCols, 2)'''.'
    end
    call PutToken ':etable.'
    call NewLine
    Global.AfterBlank = 1
    Global.AfterBreak = 1
  end

  Global.Table.Begin = 0
  Global.IsTable = 0
  Global.IsOutputEnabled = 1
return 0

doTagTR:
  call NewLine
  call PutToken ':row.'
  call NewLine
  Global.IsOutputEnabled = 0
return 0

doTag!TR:
  call CloseRef
  if Global.Table.Cols.0 > Global.Table.TotalCols.0 then do
    do i = Global.Table.TotalCols.0 + 1 to Global.Table.Cols.0
      Global.Table.TotalCols.i = 0
    end
    Global.Table.TotalCols.0 = Global.Table.Cols.0
  end

  do i = 1 to Global.Table.Cols.0
    Global.Table.TotalCols.i = max( Global.Table.TotalCols.i, Global.Table.Cols.i )
  end

  Global.Table.Cols.0 = 0
return 0

doTagTH:
  Global.IsOutputEnabled = 1
  Global.Table.Cols.0 = Global.Table.Cols.0 + 1
  i = Global.Table.Cols.0
  Global.Table.Cols.i = 0
  call NewLine
  call PutToken ':c.'
  call doTagU
  call NewLine
return 0

doTag!TH:
  call CloseRef
  call doTag!U
return 0

doTagTD:
  Global.IsOutputEnabled = 1
  Global.Table.Cols.0 = Global.Table.Cols.0 + 1
  i = Global.Table.Cols.0
  Global.Table.Cols.i = 0
  call NewLine
  call PutToken ':c.'
  call NewLine
return 0

doTag!TD:
  call CloseRef
return 0

doTextEMPTY:
 Token = translate( Token, ' ', xrange( d2c(0), d2c(31)))
 if length( strip( Token )) > 0 then
    call logError 'Unexpected text 'Token
return

doTextHEAD:
  if Global.grabTitle = 1 then
    Global.Article.Title = Global.Article.Title || IPFstring( translate( Token, '  ', d2c(9)d2c(10)))
  else
    call dotextempty
return

doTextBODY:
 call PutText Token
return

CloseRef:
  call PutToken Global.RefEndTag
  Global.RefEndTag = ''
return

/* recursive Tags management */
doOpenListTag:
  parse arg ot, ct

  if Global.OpenTag.0 == 0 then
    Global.NewParagraph = 0

  call NewLine
  call PutToken ot
  Global.OpenTag.0 = Global.OpenTag.0 + 1
  i = Global.OpenTag.0
  Global.OpenTag.i = ct
  Global.OpenTag.i.open = ot
return

doCloseListTag:
  parse arg bottom
  if length(bottom) = 0 then
    i = 1
  else
    do i = Global.OpenTag.0 to 0 by -1
      if bottom = Global.OpenTag.i then
        leave
    end

  if i > 0 then do
    call NewLine
    do j = Global.OpenTag.0 to i by -1
      call PutToken Global.OpenTag.j
      call NewLine
    end
    Global.OpenTag.0 = i - 1

    if Global.OpenTag.0 == 0 then do
      call NewParagraph
    end
    return 1
  end
return 0

doCheckListTag:
  parse arg SearchArg
  do i = Global.OpenTag.0 to 1 by -1
    if pos( SearchArg, Global.OpenTag.i ) > 0 then
      return 1
  end
return 0

/* Set the current font in output stream */
SetFont:
  parse arg Font
  if Global.IsTable then
    return
  if Global.CurFont = Font then
    return

  Global.CurFont = Font
  call PutToken Font
return

CloseIMG:
  i = Global.Article.line.0
  if right( strip( Global.Article.line.i ), 6 ) = "runin." then do
    call doTagBR
  end
return

/* Get id number depending of link value (<A HREF=...>) */
/* Returns 0 if link belongs to same page (alas, IPF doesn`t permit this...) */
GetLinkID:
  procedure expose Global.
  parse arg link

  InitialLink = link
  if pos( '#', link ) > 0 then
    link = left( link, pos( '#', link ) - 1 )
  if length( link ) = 0 then
    return 0

  link = FindFile( link )
  ulink = translate( link )
  i = wordpos( ulink, Global.HREF )
  if i > 0
    then return i

  Global.LinkID = Global.LinkID + 1
  i = Global.LinkID
  Global.LinkID.i = ulink
  Global.LinkID.i.RealName = link
  Global.LinkID.i.InitialName = InitialLink
  Global.HREF = Global.HREF || ulink || ' '

  if length( stream( link, 'c', 'query exists' )) = 0 then do
    Global.LinkID.i.Resolved = 1
    Global.URLinks = Global.URLinks + 1
    j = Global.URLinks
    Global.URLinks.j = i
    parse var link prot ':' location
    if length( location ) = 0 | pos( '/', prot ) > 0 then
      Global.LinkID.i.RealName = filespec( 'N', translate( link, '\', '/' ))
    end
  else
    Global.LinkID.i.Resolved = 0
return i

/* transform image extension into .bmp */
GetPictureID:
  procedure expose Global.
  parse arg PictName

  PictName = FindFile( PictName )
  if length( stream( PictName, 'c', 'query exists' )) > 0 then do
    tmp = PictName
    i = lastPos( '.', tmp )
    if i > 0 then
      PictName = left( tmp, i ) || 'bmp'
    else
      PictName = tmp || '.bmp'
    end
  else do
    tmp = ''
    PictName = ''
  end
return PictName || '*' || tmp

/* Actively search for file on all possible paths */
FindFile:
  parse arg fName

  /* Skips full qualified URLs. */
  if pos( "://", InitialLink ) > 0 then
    return fName

  ifName = fName
  parse var fName prot ':' location
  if length( location ) > 0 & pos( '/', prot ) = 0 then
    fName = location
  tmp = ''
  do while length( fName ) > 0
    do while pos( left( fName, 1 ), '/\' ) > 0
      fName = substr(fName, 2)
    end
    if length( fName ) = 0 then
      leave
    tmp = stream( fName, 'c', 'query exists' )
    if length( tmp ) > 0 then
      return Shorten( tmp )
    tmp = stream( Global.CurrentDir || fName, 'c', 'query exists' )
    if length( tmp ) > 0 then
      return Shorten( tmp )
    tmp = verify( fName, '/\', 'M' )
    if tmp > 0 then
      fName = substr( fName, tmp )
    else
      fName = ''
  end
return ifName

Replace: procedure

  parse arg source, string, substitute
  i = pos( string, source )

  do while i \= 0
     source = substr( source, 1, i-1 ) || substitute ||,
              substr( source, i + length( string ))

     i = pos( string, source, i + length( substitute ))
  end

return source

/* return next Token (a Tag or a text string) from input stream */
GetToken:
  procedure expose Global. Strings.

  if length( Global.FileContents ) < 512 & \Global.EOF then
GetData:
    do
      /* read next chunk of file */
      Global.FileContents = Global.FileContents || charin( Global.CurrentFile,, 1024 )
      call ProgressBar
      /* remove all \0x0d Characters from input stream */
      do until i = 0
        i = pos( '0D'x, Global.FileContents )
        if i > 0 then
          Global.FileContents = delstr( Global.FileContents, i, 1 )
      end
      /* replace all strings specified in arguments */
      do i = 1 to Strings.0
         Global.FileContents = Replace( Global.FileContents, Strings.i.template, Strings.i.replace )
      end

      Global.EOF = ( chars( Global.CurrentFile ) = 0 )
    end

  i = pos( '<', Global.FileContents )
  if i = 0 then do
    if \Global.EOF then
      signal GetData
    else do
      i = length( Global.FileContents ) + 1
      if i = 1 then
        return ''
    end
  end
  if i = 1 then do
    if left( Global.FileContents, 4 ) == '<!--' then
       endtag = '-->'
    else
       endtag = '>'
    j = pos( endtag, Global.FileContents )
    if j = 0 then
      if \Global.EOF then
        signal GetData
      else
        j = length( Global.FileContents ) + 1

      Token = '00'x || substr( Global.FileContents, 2, j - 2 )
      Global.FileContents = substr( Global.FileContents, j + length(endtag))
    end
  else do
    Token = NoQuotes( left( Global.FileContents, i - 1 ))
    Global.FileContents = substr( Global.FileContents, i )
  end
return Token

PutLine:
  procedure expose Global.
  parse arg str
  if \Global.OptCH then
    call lineout Global.oName, str
return

NewLine:
  procedure expose Global.

  i = Global.Article.line.0
  Global.Article.line.i = strip( Global.Article.line.i, 'trailing' )
  if length( Global.Article.line.i ) > 0 then do
    Global.Article.line.0 = Global.Article.line.0 + 1
    i = Global.Article.line.0
    Global.Article.line.i = ''
    return 1
  end
return 0

NewParagraph:
  Global.NewParagraph = 1
return 0

PutParagraph:
  procedure expose Global.

  if Global.NewParagraph then do
    Global.NewParagraph = 0
    if \Global.AfterBlank then do
      call NewLine
      call PutToken ':p.'
      call NewLine
      Global.AfterBlank = 1
    end
  end
return

/* put an IPF Token into output stream */
PutToken:
  procedure expose Global.
  parse arg token

  if Global.OptCH then
    return

  call PutParagraph
  i = Global.Article.line.0
  Global.Article.line.i = Global.Article.line.i || token
  call Reformat
  Global.AfterBreak = 0
  Global.AfterBlank = 0
return

/* Put an text string into output stream */
PutText:
  procedure expose Global.
  parse arg text

  if Global.OptCH then
    return

  /* Skip everything out of :c. ... :c. or :row. tags */
  if Global.IsTable & \Global.IsOutputEnabled
    then return

  if Global.IsPreformatted then do
    call PutParagraph
    do while length( text ) > 0
      EOLpos = pos( Global.EOL, text )
      if EOLpos > 0 then do
        c = ExpandTabs( left( text, EOLpos - 1 ))
        i = Global.Article.line.0
        Global.Article.line.i = Global.Article.line.i || IPFstring( c )
        Global.Article.line.0 = Global.Article.line.0 + 1
        i = Global.Article.line.0
        Global.Article.line.i = ''
        text = substr( text, EOLpos + 1 )
        end
      else do
        c = ExpandTabs( text )
        i = Global.Article.line.0
        Global.Article.line.i = Global.Article.line.i || IPFstring( c )
        text = ''
      end
      if Global.isTable & Global.Table.Cols.0 > 0 then do
         i = Global.Table.Cols.0
         Global.Table.Cols.i = Global.Table.Cols.i + length( c )
      end
    end
    Global.AfterBreak = 0
    Global.AfterBlank = 0
    return
  end

  text = ChangeStr( Global.EOL, text, " " )
  text = changeStr( d2c(9), text, " " )

  /* Condense blanks */

  curpos = 1
  do forever
    curpos = pos( '  ', text, curpos )
    if curpos = 0 then
      leave
    endpos = verify( text, ' ', 'N', curpos + 2 )
    if endpos = 0 then
      text = left( text, curpos )
    else
      text = left( text, curpos ) || substr( text, endpos )
  end

  i = Global.Article.line.0

  if length( Global.Article.line.i ) == 0 | Global.NewParagraph then
    text = strip( text, 'leading' )

  if text == '' then
    return

  call PutParagraph
  i = Global.Article.line.0
  Global.Article.line.i = Global.Article.line.i || IPFstring( text )
  call Reformat

  if Global.isTable & Global.Table.Cols.0 > 0 then do
     i = Global.Table.Cols.0
     Global.Table.Cols.i = Global.Table.Cols.i + length( text )
  end

  Global.AfterBreak = 0
  Global.AfterBlank = 0
return

ExpandTabs:
  procedure expose Global.
  parse arg text

  tabpos = pos( d2c(9), text )
  do while tabpos > 0
    text = left( text, tabpos - 1 ) || copies( ' ', 8 - tabpos // 8 ) || substr( text, tabpos + 1 )
    tabpos = pos( d2c(9), text )
  end
return text

Reformat:
  procedure expose Global.

  if Global.IsPreformatted then
    return

  i = Global.Article.line.0

  do while length( Global.Article.line.i ) > Global.maxLineLength
    text = Global.Article.line.i
    EOLpos = Global.maxLineLength
    do while EOLpos > 0
      if c2d( substr( text, EOLpos, 1 )) <= 32 then
        leave
      EOLpos = EOLpos - 1
    end
    if EOLpos = 0 then do
      EOLpos = Global.maxLineLength
      do while EOLpos < length( text )
        if c2d( substr( text, EOLpos, 1 )) <= 32 then
          leave
        EOLpos = EOLpos + 1
      end
    end
    if EOLpos > 256 then do
      tail = substr( text, Global.maxLineLength + 1 )
      Global.Article.line.i = left( text, Global.maxLineLength )
      call NewLine
      i = Global.Article.line.0
      Global.Article.line.i = tail
      end
    else if c2d( substr( text, EOLpos, 1 )) <= 32 then do
      tail = substr( text, EOLpos + 1 )
      Global.Article.line.i = left( text, EOLpos - 1 )
      call NewLine
      i = Global.Article.line.0
      Global.Article.line.i = tail
      end
    else do
      leave
    end
  end
return

IPFstring:
  procedure expose Global.
  parse arg ins

  return ChangeStr( d2c(0),,
          ChangeStr( ':',,
            ChangeStr( '&',,
              ChangeStr( '.', ins, d2c(0)'per.' ),,
            '&amp.' ),,
          '&colon.' ),,
          '&')

ChangeStr:
  procedure expose Global.
  parse arg src,var,trg
  curpos = 1
  do forever
    curpos = pos( src, var, curpos )
    if curpos = 0 then
      leave
    var = left( var, curpos - 1 ) || trg || substr( var, curpos + 1 )
    curpos = curpos + length( trg )
  end
return var

Shorten:
  parse arg fName
  fName = translate( stream( fName, 'c', 'query exists' ), '/', '\' )
  tmp = translate( Directory(), '/', '\' )
  if Pos( tmp, fName ) = 1 then
    return substr( fName, length(tmp) + 2 )
  if substr( fName, 2, 1 ) = ':' then
    return substr( fName, 3 )
return fName

logError:
  procedure expose Global.

  if Global.optD then do
    parse arg line
    call lineout 'HTML2IPF.log', line
  end
return

CRLF:
  parse value SysTextScreenSize() with maxrow maxcol
  parse value SysCurPos() with row col
  call charout, copies(' ', maxcol - col )
return

ProgressBar:
  parse value SysTextScreenSize() with maxrow maxcol
  parse value SysCurPos() with row col
  if col > maxcol - 19 then
    say ''

  Rest = (( Global.FileSize - chars( Global.CurrentFile )) * 16 ) % Global.FileSize
  call setcolor "lCyan"
  call charOut, '['
  call setcolor "White"
  call charOut, copies('�', Rest)copies('�', 16-Rest)
  call setcolor "lCyan"
  call charOut, ']'copies('08'x, 18)
return

SetColor:
  arg col

  if \Global.optCO then
    return

  col = ColorNo( col )

  if col = -1 then
    return -1
  if col >  7 then
    col = '1;3'col-8
  else
    col = '0;3'col

  call Charout, d2c(27)'['col'm'
return 0

ColorNo:
  arg colname
  if substr( colname, 1, 1 ) = 'L' then do
    colname = right( colname, length(colname) - 1 )
    light = 8
    end
  else
    light = 0

  select
    when abbrev( 'BLACK',   colname, 3 ) then return light + 0
    when abbrev( 'BLUE',    colname, 3 ) then return light + 4
    when abbrev( 'GREEN',   colname, 3 ) then return light + 2
    when abbrev( 'CYAN',    colname, 3 ) then return light + 6
    when abbrev( 'RED',     colname, 3 ) then return light + 1
    when abbrev( 'MAGENTA', colname, 3 ) then return light + 5
    when abbrev( 'BROWN',   colname, 3 ) then return light + 3
    when abbrev( 'GRAY',    colname, 3 ) then return light + 7
    when abbrev( 'DGRAY',   colname, 3 ) then return 8
    when abbrev( 'YELLOW',  colname, 3 ) then return 11
    when abbrev( 'WHITE',   colname, 3 ) then return 15
  end
return -1

DefineQuotes:
  Global.Quotes = ,
    "amp    0x26",
    "lt     0x3C",
    "gt     0x3E",
    "nbsp   0x20",
    "iexcl  0xAD",
    "cent   0xBD",
    "pound  0x9C",
    "curren 0xCF",
    "yen    0xBE",
    "brvbar 0xDD",
    "sect   0x15",
    "uml    0xF9",
    "copy   0xB8",
    "quot   0x22",
    "ordf   0xA6",
    "laquo  0xAE",
    "not    0xAA",
    "reg    0xA9",
    "macr   0xEE",
    "deg    0xF8",
    "plusmn 0xF1",
    "sup1   0xFB",
    "sup2   0xFD",
    "sup3   0xFC",
    "acute  0xEF",
    "micro  0xE6",
    "para   0xF4",
    "middot 0xFA",
    "ccedil 0x87",
    "ordm   0xA7",
    "frac14 0xAC",
    "frac12 0xAB",
    "frac34 0xF3",
    "iquest 0xA8",
    "Agrave 0xB7",
    "Aacute 0xB5",
    "Acirc  0xB6",
    "Atilde 0xC7",
    "Auml   0x8E",
    "Aring  0x8F",
    "AElig  0x92",
    "Ccedil 0x80",
    "Egrave 0xD4",
    "Eacute 0x90",
    "Ecirc  0xD2",
    "Euml   0xD3",
    "Igrave 0xDE",
    "Iacute 0xD6",
    "Icirc  0xD7",
    "Iuml   0xD8",
    "ETH    0xD1",
    "Ntilde 0xA5",
    "Ograve 0xE3",
    "Oacute 0xE0",
    "Ocirc  0xE2",
    "Otilde 0xE5",
    "Ouml   0x99",
    "times  0x9E",
    "Oslash 0x9D",
    "Ugrave 0xEB",
    "Uacute 0xE9",
    "Ucirc  0xEA",
    "Uuml   0x9A",
    "Yacute 0xED",
    "THORN  0xE8",
    "Yuml   0x98",
    "szlig  0xE1",
    "agrave 0x85",
    "aacute 0xA0",
    "acirc  0x83",
    "atilde 0xC6",
    "auml   0x84",
    "aring  0x86",
    "aelig  0x91",
    "egrave 0x8A",
    "eacute 0x82",
    "ecirc  0x88",
    "euml   0x89",
    "igrave 0x8D",
    "iacute 0xA1",
    "icirc  0x8C",
    "iuml   0x8B",
    "eth    0xD0",
    "ntilde 0xA4",
    "ograve 0x95",
    "oacute 0xA2",
    "ocirc  0x93",
    "otilde 0xE4",
    "ouml   0x94",
    "divide 0xF6",
    "oslash 0x9B",
    "ugrave 0x97",
    "uacute 0xA3",
    "ucirc  0x96",
    "uuml   0x81",
    "yacute 0xEC",
    "thorn  0xE7",
    "yuml   0x98"
return

/* substitute quoted Characters */
NoQuotes:
  parse arg text

  sPos = 1
  do forever
    qPos = pos( '&', text, sPos )
    if qPos == 0 then
      leave
    if pos( ';', text, qPos + 1 ) == 0 then
      leave
    parse var text _head '&' Token ';' _tail

    wordN = wordpos( Token, Global.Quotes )
    if wordN = 0 then do
      if left( Token, 1 )='#' & datatype( substr( Token, 2 ), 'num' ) then do
        Token = substr( Token, 2 )
        Token = d2c( Token )
        end
      else do
        Token = d2c(0) || Token || ';'
      end
      end
    else do
      Token = word( Global.Quotes, wordN + 1 )
      if left( Token, 2 ) = "0x" then
        Token = x2c( substr( Token, 3 ))
    end
    sPos = qPos + 1
    text = _head || Token || _tail
  end
return translate( text, '&', d2c(0))

